/**
 *  dxf_file.h
 *
 */


#ifndef x_dxf_file_h_x
#define x_dxf_file_h_x 1


#include "dxf_model.h"

namespace dxf
{

//

/**
 * DXF_BINARY_SENTINEL is the character sequence at the beginning
 * of binary dxf files including the null at the end.
 */
#define DXF_BINARY_SENTINEL "AutoCAD Binary DXF\r\n\x1A"


// ======== BasicReadFile ==============================================
/**
 * This is the basic file read interface for a dxf file.
 *
 */

class DXF_EXPORT BasicReadFile
  : public at::PtrTarget_MT
{
    public:

    enum ReadResult
    {
        Failed,
        EndOfFile,
        Success
    };

    // ======== Read ==================================================
    /**
     * Read will return a new model from reading the file.  If there are
     * errors they are reported using the status report.
     *
     * @param i_data The location of where to append the data. If a null
     *              pointer is given then create a buffer as well.
     * @param i_len  The size of the block to attempt to read
     * @param i_report A place to report failure.
     *
     * @return ReadResult indicating the result of the read operation
     */

    virtual ReadResult Read(
        at::Ptr< at::Buffer * >   & io_data,
        at::SizeMem                 i_len,
        at::StatusReport          * i_report = 0
    ) = 0;

    // ======== Length ================================================
    /**
     *	Return the length of the file.
     *
     * @return nothing
     */

    virtual at::SizeMem Length() = 0;
    
};


// ======== StdIstreamReadFile ========================================
/**
 * StdIstreamReadFile implements the file interface.
 *
 */

class DXF_EXPORT StdIstreamReadFile
  : public BasicReadFile
{
    public:

    /**
     * StdIstreamReadFile
     *
     * @param i_ostream The std::istream used to read from the file.
     */
    StdIstreamReadFile( std::istream & i_ostream );

    virtual ReadResult Read(
        at::Ptr< at::Buffer * >   & io_data,
        at::SizeMem                 i_len,
        at::StatusReport          * i_report = 0
    );

    virtual at::SizeMem Length();
    
    std::istream                & m_istream;
};

// ======== ReadFile ==================================================
/**
 * ReadFile is used to read a model object from a dxf file.
 *
 */

class DXF_EXPORT ReadFile
{
    public:

    virtual ~ReadFile() {}

    /**
     * ReadFile basic constructor.
     * @param i_file A BasicReadFile interface.
     *
     */
    ReadFile( at::PtrDelegate< BasicReadFile * >  i_file );


    // ======== ReadFile ==============================================
    /**
     * ReadFile basic constructor.
     * @param i_file A BasicReadFile interface.
     *
     */
    ReadFile( std::istream & i_file );

    // ======== Read ==================================================
    /**
     * Read converts the dxf file to a dxf::Model object.
     *
     *
     * @return The model 
     */

    virtual at::PtrDelegate< Model * > Read( at::StatusReport * );


    // ======== ProcessData ===========================================
    /**
     * ProcessData takes a buffer of data and converts it to a model
     *
     * @param i_region  The data to be processed
     * @param i_report  Report errors to this
     * @return A conversion result
     */

    virtual Converter::ConversionResult ProcessData(
        const at::Buffer::t_ConstRegion & l_region,
        at::SizeMem                     & i_offset,
        ConversionReport                & i_report
    );

    /**
     * m_file is the basic read file interface used to get data
     * from  file.
     */
    at::Ptr< BasicReadFile * >              m_file;

    GroupCode::ConverterType                m_type;

    at::PtrDelegate< Model * >              m_model;

    const GroupCode                       * m_last_code;

    /**
     * The buffer size to use.
     */
    at::SizeMem                             m_bufsz;
};


// ======== StdOstreamWriteFile =======================================
/**
 * Write to a std::ostream file.
 *
 */

class DXF_EXPORT StdOstreamWriteFile
  : public WriteFile
{
    public:

    /**
     * StdOstreamWriteFile is constructed from a std::ostream
     *
     */
    StdOstreamWriteFile(
        std::ostream    & i_stdostream
    );

    // ======== Write =================================================
    /**
     *	Write data
     *
     *
     * @param i_mem     The pointer to memory to write
     * @param i_len     The length to write
     * @return true if all is well
     */

    virtual bool Write(
        const char              * i_mem,
        at::SizeMem               i_len,
        at::StatusReport        * i_report = 0
    );


    // ======== Flush =================================================
    /**
     * Flush bits to disk.
     *
     * @return True if all is well
     */

    virtual bool Flush( at::StatusReport * i_report = 0 );

    /**
     * m_stdostream is the stream to write to
     */
    std::ostream            & m_stdostream;

};


} // end namespace


#endif // x_dxf_file_h_x


