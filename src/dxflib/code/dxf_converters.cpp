

#include "dxf_group_codes.h"
#include "dxf_converters.h"
#include "at_assert.h"

#include <sstream>
#include <iomanip>
#include <typeinfo>
#include <cmath>

namespace dxf_impl
{

//


// ======== DxfUnEscapeString ===========================================
/**
 *	Unescape a string
 *
 * @param i_str The stringf to escape
 * @return nothing
 */

std::string DxfUnEscapeString( const std::string & i_str )
{
    std::string::const_iterator l_itr = i_str.begin();
    std::string::const_iterator const l_end = i_str.end();
    std::string             l_result;

    while ( l_itr != l_end )
    {
        if ( '^' == * l_itr )
        {
            ++ l_itr;
            if ( l_itr == l_end )
            {
                return l_result;
            }

            if ( ' ' ==  * l_itr )
            {
                l_result += "^";
            }
            else
            {
                l_result += char( * l_itr - '@' );
            }
        }
        else
        {
            l_result += * l_itr;
        }
            
        ++ l_itr;
    }

    return l_result;
}


// ======== DxfEscapeString ===========================================
/**
 * Use "^" to escape a string
 *
 * @param i_str The stringf to un-escape
 * @return nothing
 */

std::string DxfEscapeString( const std::string & i_str )
{
    std::string::const_iterator l_itr = i_str.begin();
    std::string::const_iterator const l_end = i_str.end();
    std::string             l_result;

    while ( l_itr != l_end )
    {
        if ( '^' == * l_itr )
        {
            l_result += "^ ";
        }
        else if ( ! ((~0x1f) & (* l_itr) ) )
        {
            l_result += '^';
            l_result += char( * l_itr + '@' );
        }
        else
        {
            l_result += * l_itr;
        }
        ++ l_itr;
    }

    return l_result;
}


// ======== ReportError ===============================================
/**
 * Call the status reporter if there is one
 *
 *
 * @param i_report is the report parameteter
 * @param i_str is the message to report.
 * @return nothing
 */

void ReportError(
    at::StatusReport                      * i_report,
    const std::string                     & i_str
) {

    if ( i_report )
    {
        i_report->ReportStatus(
            at::StatusReport::ErrorStatus,
            0,
            i_str
        );
    }
}


// ======== AsciiConverter ============================================
/**
 * 
 *
 */

class DXF_EXPORT AsciiConverter
  : public dxf::Converter
{
    public:

};


// ======== BinaryConverter ===========================================
/**
 * 
 *
 */

class DXF_EXPORT BinaryConverter
  : public dxf::Converter
{
    public:

};


// ======== BinaryConverter_Special ===========================================
/**
 * BinaryConverter_Special convert the binary types.
 *
 */

template <typename w_ExternalType, typename w_InternalType = w_ExternalType>
class BinaryConverter_Basic
  : public BinaryConverter
{
    public:

    typedef at::Buffer::t_ConstRegion::value_type   value_type;

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {
        
        value_type   * l_str = i_buffer.m_mem + io_offset;
        value_type   * const l_end = i_buffer.m_mem + i_buffer.m_allocated;

        if ( l_end <= ( l_str + sizeof( w_ExternalType ) ) )
        {
            return NeedMoreBytes;
        }

        w_ExternalType l_value;
        
        if ( at::OSTraitsBase::IsBigEndian() )
        {
            // big endian conversion
            at::Buffer::t_ConstRegion::t_non_const_value_type * l_local =
                reinterpret_cast<at::Buffer::t_ConstRegion::t_non_const_value_type *>( & l_value );

            if ( sizeof( w_ExternalType ) == 1 )
            {
                l_local[0] = l_str[0];
            }
            else if ( sizeof( w_ExternalType ) == 2 )
            {
                l_local[0] = l_str[1];
                l_local[1] = l_str[0];
            }
            else if ( sizeof( w_ExternalType ) == 4 )
            {
                l_local[0] = l_str[3];
                l_local[1] = l_str[2];
                l_local[2] = l_str[1];
                l_local[3] = l_str[0];
            }
            else if ( sizeof( w_ExternalType ) == 8 )
            {
                l_local[0] = l_str[7];
                l_local[1] = l_str[6];
                l_local[2] = l_str[5];
                l_local[3] = l_str[4];
                l_local[4] = l_str[3];
                l_local[5] = l_str[2];
                l_local[6] = l_str[1];
                l_local[7] = l_str[0];
            }
            else
            {
                AT_Abort();
            }
                
        }
        else
        {
            // if you get bus errors here you need to do what the code
            // above does without the endian swap.

            l_value = * reinterpret_cast< const w_ExternalType * >( l_str );
        }

        // update byte use
        io_offset += sizeof( w_ExternalType );

        AT_Assert( io_offset <= i_buffer.m_allocated );

        // update report
        io_conv_report.UpdateBytesCount( sizeof( w_ExternalType ) );
                
        o_value = new dxf::ValueType( w_InternalType( l_value ), at::AnyInit );
        return Success;
        
    }

    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {

        // get a reference to the internal value
        w_InternalType & l_internal = i_value->Refer();
        // convert to the external value
        w_ExternalType l_value( l_internal );
        
        if ( at::OSTraitsBase::IsBigEndian() )
        {
            // big endian conversion
            at::Buffer::t_ConstRegion::value_type * l_ext =
                reinterpret_cast<value_type *>( & l_value );

            char        l_local[ sizeof( l_value ) ];

            if ( sizeof( w_ExternalType ) == 1 )
            {
                l_local[0] = l_ext[0];
            }
            else if ( sizeof( w_ExternalType ) == 2 )
            {
                l_local[0] = l_ext[1];
                l_local[1] = l_ext[0];
            }
            else if ( sizeof( w_ExternalType ) == 4 )
            {
                l_local[0] = l_ext[3];
                l_local[1] = l_ext[2];
                l_local[2] = l_ext[1];
                l_local[3] = l_ext[0];
            }
            else if ( sizeof( w_ExternalType ) == 8 )
            {
                l_local[0] = l_ext[7];
                l_local[1] = l_ext[6];
                l_local[2] = l_ext[5];
                l_local[3] = l_ext[4];
                l_local[4] = l_ext[3];
                l_local[5] = l_ext[2];
                l_local[6] = l_ext[1];
                l_local[7] = l_ext[0];
            }
            else
            {
                AT_Abort();
            }

            //
            return i_wfile->Write( l_local, sizeof( w_ExternalType ), i_report );
                
        }
        else
        {
            return i_wfile->Write(
                reinterpret_cast<char *>( & l_value ),
                sizeof( w_ExternalType ),
                i_report
            );
        }
    }
    
};

// specialization for strings
template <>
class BinaryConverter_Basic< std::string, std::string >
  : public BinaryConverter
{
    public:
    typedef at::Buffer::t_ConstRegion::value_type   value_type;

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {

        value_type   * l_str = i_buffer.m_mem + io_offset;
        value_type   * l_begin_str = l_str;
        value_type   * const l_end = i_buffer.m_mem + i_buffer.m_allocated;

        while ( l_end > l_str )
        {
            if ( ! * l_str )
            {
                std::string l_value( l_begin_str, l_str );

                at::SizeMem l_used = 1 + l_str - l_begin_str;
                // update byte use
                io_offset += l_used;
                AT_Assert( io_offset <= i_buffer.m_allocated );
        
                // update report
                io_conv_report.UpdateBytesCount( l_used );
                        
                o_value = new dxf::ValueType( l_value, at::AnyInit );
                return Success;
            }

            ++ l_str;
        }

        return NeedMoreBytes;
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {
        // get a reference to the internal value
        std::string & l_internal = i_value->Refer();

        if ( ! i_wfile->Write( l_internal.data(), l_internal.length(), i_report ) )
        {
            return false;
        }

        return i_wfile->Write( "", 1, i_report );
    }
    
};

// specialization for strings
template <>
class BinaryConverter_Basic< dxf::BinaryXdata, std::string >
  : public BinaryConverter
{
    public:
    typedef at::Buffer::t_ConstRegion::value_type   value_type;

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {

        value_type   * l_str = i_buffer.m_mem + io_offset;
        value_type   * const l_end = i_buffer.m_mem + i_buffer.m_allocated;

        if ( l_end <= l_str )
        {
            return NeedMoreBytes;
        }
        
        unsigned char   l_len = * reinterpret_cast< const unsigned char * >( l_str );

        if ( l_end <= ( l_str + 1 + l_len ) )
        {
            return NeedMoreBytes;
        }

        at::SizeMem l_used = 1 + l_len;
        
        std::string l_value( l_str, l_len );

        // update byte use
        io_offset += l_used;
        AT_Assert( io_offset <= i_buffer.m_allocated );

        // update report
        io_conv_report.UpdateBytesCount( l_used );
                
        o_value = new dxf::ValueType( l_value, at::AnyInit );
        return Success;
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {
        // get a reference to the internal value
        std::string & l_internal = i_value->Refer();

        if ( l_internal.length() > 0xff )
        {
            ReportError( i_report, "XData string is too long" );
            return false;
        }

        char    l_length = l_internal.length();

        if ( ! i_wfile->Write( &l_length, sizeof( l_length ), i_report ) )
        {
            return false;
        }

        return i_wfile->Write( l_internal.data(), l_internal.length(), i_report );
    }
    
};

// specialization for strings
template <>
class BinaryConverter_Basic< dxf::UndefinedValue, std::string >
  : public BinaryConverter
{
    public:
    typedef at::Buffer::t_ConstRegion::value_type   value_type;

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {
        io_conv_report.ReportFailure( "Undefined group code - can't convert binary data" );

        return Failed;
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {

        ReportError( i_report, "Attempting to write an undefined value" );
        return false;
    }
    
};

// ======== Extents ===================================================
/**
 * Discover the extents of the string in the file.
 *
 */

template <bool w_skip_spaces>
class LineExtents
{
    public:

    typedef at::Buffer::t_ConstRegion::value_type   value_type;
    /**
     * LineExtents discovers the beginning and end of a line
     * in a file not including the \r\n.  If the region starts
     * with a \r\n it is skipped (only the first one).  This should
     * work with files written in Unix, DOS/Windows or MacOS line
     * endings.
     */
    LineExtents(
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    )
      : m_str_region( FindExtents( i_buffer, io_offset, io_conv_report ) )
    {
    }

    static inline at::Buffer::t_ConstRegion FindExtents(
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    )
    {
        value_type   * l_str = i_buffer.m_mem + io_offset;
        value_type   * const l_end = i_buffer.m_mem + i_buffer.m_allocated;
        at::SizeMem    l_lines = 0;

        // The prior line's cr/lf still in the buffer (except for the
        // first line).  So we need to skip exatly one end of line.

        if ( l_str >= l_end )
        {
            // Not enough text - need to return an empty
            // region.

            return at::Buffer::t_ConstRegion();
        }

        at::Buffer::t_ConstRegion::t_non_const_value_type  l_char = * l_str;
        if ( '\n' == l_char )
        {
            ++ l_lines;
            l_char = * ++ l_str;

            if ( l_str >= l_end ) return at::Buffer::t_ConstRegion();

            if ( '\r' == l_char )
            {
                ++ l_str;
            }
        }
        else if ( '\r' == l_char )
        {
            ++ l_lines;
            l_char = * ++ l_str;
            if ( l_str >= l_end ) return at::Buffer::t_ConstRegion();
            
            if ( '\n' == l_char )
            {
                ++ l_str;
            }
        }

        if ( l_str >= l_end ) return at::Buffer::t_ConstRegion();

        // we have now removed the first end of line marker
        // (the one for the previous line.  l_str contains the beginning
        // of the line.

        if ( w_skip_spaces )
        {
            while ( l_str < l_end )
            {
                l_char = * l_str;

                if ( ( ' ' == l_char ) || ( '\t' == l_char  ) )
                {
                    ++ l_str;
                }
                else
                {
                    break;
                }
            }
        }

        // now we have the start of the line/string
        value_type   * const l_start = l_str;
        
        // find the beginning of the next line
        
        while ( l_str < l_end )
        {
            l_char = * l_str;

            if ( ( '\r' == l_char ) || ( '\n' == l_char  ) )
            {
                // OK we now have the end of line/string

                at::SizeMem l_bytes_used = l_str - i_buffer.m_mem - io_offset;

                // indicate that the string is now used.
                io_offset = l_str - i_buffer.m_mem;
                
                AT_Assert( io_offset <= i_buffer.m_allocated );

                io_conv_report.UpdateLinesAndByteCount( l_bytes_used, l_lines );
                    
                return at::Buffer::t_ConstRegion(
                    l_start,
                    l_str - l_start,
                    l_str - l_start
                );
                
            }
            else
            {
                ++ l_str;
            }
        }

        // Need to read some more...

        return at::Buffer::t_ConstRegion();
    }


    // ======== Status ================================================
    /**
     * Status returns the a "conversion result" status indicating either
     * success or the result of having read enough data.
     *
     * @return Success on a string being found.
     */

    dxf::Converter::ConversionResult Status() const
    {
        if ( m_str_region.m_mem )
        {
            return dxf::Converter::Success;
        }

        return dxf::Converter::NeedMoreBytes;
    }


    // ======== String ===========================================
    /**
     * String creates a std::string from this
     *
     *
     * @return a std::string
     */

    std::string String() const
    {
        return m_str_region.String();
    }

    /**
     * m_str_region is the region that is the string to convert
     * If it contains a null string then it does not contain
     * any data and more data from the file is needed.
     */
    at::Buffer::t_ConstRegion               m_str_region;

};


// ======== AsciiConverter_Special ======================================
/**
 * This is the basic ascii converter.
 *
 */

template <typename w_ExType, typename w_Type, bool w_skip_spaces>
class AsciiConverter_Special
  : public AsciiConverter
{
    public:

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {
        LineExtents< w_skip_spaces >  l_extents( i_buffer, io_offset, io_conv_report );

        if ( Success != l_extents.Status() )
        {
            return l_extents.Status();
        }

        std::istringstream  i_stream( l_extents.String() );

        // l_value is a w_Type or an at::Int32 if it is bool
        typename at::TypeSelect< at::IsSame<w_Type, bool>::m_value, at::Int32, w_Type >::t_Type l_value;

        if ( i_stream >> l_value )
        {
            o_value = new dxf::ValueType( w_Type( l_value ), at::AnyInit );
            return Success;
        }

        // OK we had a conversion failure - report it and get out.
            
        io_conv_report.ReportFailure( "Conversion failure" );

        return Failed;
        
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {
        // get a reference to the internal value
        w_Type & l_data = i_value->Refer();

        std::ostringstream  l_stream;

//l_stream << typeid(w_ExType).name() << " ";

        if ( at::IsFloating<w_Type>::m_value )
        {
			if ( 
				std::floor(typename at::StaticTypeInfo<w_Type>::t_floating_eqiv(l_data)) 
				== std::ceil(typename at::StaticTypeInfo<w_Type>::t_floating_eqiv(l_data)) 
			)
            {
                long long   l_ival( static_cast<long long>( l_data ) );
                l_stream << l_ival << ".0";
                const std::string & l_str = l_stream.str();
                return i_wfile->Write( l_str.data(), l_str.length(), i_report );
            }
            l_stream << std::setprecision( 16 );
        }
        if ( at::IsSame<w_Type, bool>::m_value )
        {
            l_stream << std::setw( 6 );
            l_stream << int( l_data );
            const std::string & l_str = l_stream.str();
            return i_wfile->Write( l_str.data(), l_str.length(), i_report );
        }
        if ( at::IsSame<w_ExType,at::Int16>::m_value )
        {
            l_stream << std::setw( 6 );
        }
        if ( at::IsSame<w_ExType,at::Int32>::m_value )
        {
            l_stream << std::setw( 9 );
        }

        l_stream << l_data;

        const std::string & l_str = l_stream.str();

        return i_wfile->Write( l_str.data(), l_str.length(), i_report );
        
    }
    
};

template <bool w_skip_spaces>
class AsciiConverter_Special<std::string, std::string, w_skip_spaces>
  : public AsciiConverter
{
    public:

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {
        LineExtents< w_skip_spaces >  l_extents( i_buffer, io_offset, io_conv_report );

        if ( Success != l_extents.Status() )
        {
            return l_extents.Status();
        }

        o_value = new dxf::ValueType( DxfUnEscapeString( l_extents.String() ), at::AnyInit );
        return Success;
        
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {
        // get a reference to the internal value
        std::string & l_internal = i_value->Refer();
        std::string l_val = DxfEscapeString( l_internal );

        return i_wfile->Write( l_val.data(), l_val.length(), i_report );
    }
    
};


template <bool w_skip_spaces>
class AsciiConverter_Special<dxf::BinaryXdata, std::string, w_skip_spaces>
  : public AsciiConverter
{
    public:
    typedef at::Buffer::t_ConstRegion::value_type   value_type;

    static unsigned HexBits( value_type & i_val )
    {
        if ( i_val >= '0' && i_val <= '9' )
        {
            return unsigned( i_val ) - unsigned( '0' );
        }

        return unsigned( i_val ) - unsigned( 'A' ) + 10;
    }

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {
        LineExtents< w_skip_spaces >  l_extents( i_buffer, io_offset, io_conv_report );

        if ( Success != l_extents.Status() )
        {
            return l_extents.Status();
        }

        value_type   * l_str = l_extents.m_str_region.m_mem;
        value_type   * const l_end = l_extents.m_str_region.m_mem + l_extents.m_str_region.m_allocated;

        std::string     l_val;
        
        while ( l_end > ( 1 + l_str ) )
        {
            char l_ch = ( HexBits( l_str[ 0 ] ) << 4 ) | HexBits( l_str[ 1 ] );
            l_val.push_back( l_ch );
            l_str += 2;
        }

        o_value = new dxf::ValueType( l_val, at::AnyInit );
        return Success;
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {
        // get a reference to the internal value
        std::string & l_internal = i_value->Refer();

        std::string::iterator       l_itr = l_internal.begin();
        const std::string::iterator l_end = l_internal.end();

        while ( l_end != l_itr )
        {
            static const char l_map[] = "0123456789ABCDEF";

            unsigned char l_ch( * l_itr );

            // a little inefficient - should buffer before Write
            if ( ! i_wfile->Write( l_map + (l_ch>>4), 1, i_report ) )
            {
                return false;
            }
            
            if ( ! i_wfile->Write( l_map + (l_ch&0xf), 1, i_report ) )
            {
                return false;
            }
            ++ l_itr;
        }
        return true;
    }
    
};


template <bool w_skip_spaces>
class AsciiConverter_Special<dxf::UndefinedValue, std::string, w_skip_spaces>
  : public AsciiConverter
{
    public:

    virtual ConversionResult Convert(
        at::Ptr< dxf::ValueType * >         & o_value,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const
    {
        LineExtents< w_skip_spaces >  l_extents( i_buffer, io_offset, io_conv_report );

        if ( Success != l_extents.Status() )
        {
            return l_extents.Status();
        }

        o_value = new dxf::ValueType( l_extents.String(), at::AnyInit );
        return Success;
        
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The value to print
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >         i_wfile,
        at::PtrView< dxf::ValueType * >         i_value,        
        at::StatusReport                      * i_report
    ) const {
        // get a reference to the internal value
        ReportError( i_report, "Attempting to write an undefined value" );
        return false;
    }
    
};



// ======== GroupCodeConverters =======================================
/**
 * GroupCodeConverters provides the conversion facilities for
 * group codes.  This can be shared among multiple converters.
 */

class DXF_EXPORT GroupCodeConverters
{
    public:

    virtual const dxf::Converter * GetConverter(
        dxf::GroupCode::ConverterType       i_converter
    ) const = 0;

};


// ======== GroupCodeConverters_Basic =================================
/**
 * The basic converter
 *
 */

template <typename w_External, typename w_Internal, bool w_skip_white>
class GroupCodeConverters_Basic
  : public GroupCodeConverters
{

    AsciiConverter_Special< w_External, w_Internal, w_skip_white >  m_ascii;

    BinaryConverter_Basic< w_External, w_Internal >     m_binary;

    public:
    
    virtual const dxf::Converter * GetConverter(
        dxf::GroupCode::ConverterType       i_converter
    ) const {

        switch ( i_converter )
        {
            case dxf::GroupCode::DxfBinary :
            case dxf::GroupCode::DxfBinary16 :
            {
                return & m_binary;
            }
            case dxf::GroupCode::DxfAscii :
            {
                return & m_ascii;
            }
            default:
            {
                AT_Abort();
            }
        }
    }

    static const GroupCodeConverters_Basic s_converter;

};

template <typename w_External, typename w_Internal, bool w_skip_white>
const GroupCodeConverters_Basic<w_External, w_Internal, w_skip_white> GroupCodeConverters_Basic<w_External, w_Internal, w_skip_white>::s_converter;

// ======== GroupConverterByCode ============================================
/**
 * Short-hand to get the converter for this group code.
 *
 */

template <int w_GroupCode>
class GroupConverterByCode
{
    public:

    typedef typename dxf::GroupTypeData<w_GroupCode>::t_Traits    t_Traits;
    typedef typename t_Traits::t_ExternalType   t_ExternalType;
    typedef typename t_Traits::t_InternalType   t_InternalType;
    enum { SkipWhiteSpace = t_Traits::SkipWhiteSpace };
    
    typedef GroupCodeConverters_Basic< t_ExternalType, t_InternalType, SkipWhiteSpace > t_Converter;

};

// ======== GroupCodeConverter_Ascii =================================
/**
 * 
 *
 */

class DXF_EXPORT GroupCodeConverter_Ascii
  : public dxf::GroupCodeConverter
{
    public:


    virtual dxf::Converter::ConversionResult Convert(
        const dxf::GroupCode              * & o_value ,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const {
        
        LineExtents< true >  l_extents( i_buffer, io_offset, io_conv_report );

        if ( dxf::Converter::Success != l_extents.Status() )
        {
            return l_extents.Status();
        }

        std::istringstream  i_stream( l_extents.String() );

        int l_value;

        if ( i_stream >> l_value )
        {
            const dxf::GroupCode & l_gc = dxf::GroupCode::GetGroupCode( l_value );

            if ( l_gc != dxf::GroupCode::s_error_code )
            {
                o_value = & l_gc;
                return dxf::Converter::Success;
            }
        }

        // OK we had a conversion failure - report it and get out.
            
        io_conv_report.ReportFailure( "Conversion failure" );

        return dxf::Converter::Failed;
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The group code to write
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >     i_wfile,
        const dxf::GroupCode              & i_value,
        at::StatusReport                  * i_report
    ) const {

        std::ostringstream      l_stream;
        l_stream << std::setw(3) << i_value.m_group_code;
        const std::string   & l_str = l_stream.str();
        return i_wfile->Write( l_str.data(), l_str.length(), i_report );
    }
    
    /**
     * s_converter is the static version of this converter
     */
    static const GroupCodeConverter_Ascii      s_converter;

};

const GroupCodeConverter_Ascii      GroupCodeConverter_Ascii::s_converter;


// ======== GroupCodeConverter_Binary =================================
/**
 * 
 *
 */

class DXF_EXPORT GroupCodeConverter_Binary
  : public dxf::GroupCodeConverter
{
    public:

    typedef at::Buffer::t_ConstRegion::value_type   value_type;

    virtual dxf::Converter::ConversionResult Convert(
        const dxf::GroupCode              * & o_value ,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const {

        value_type   * l_str = i_buffer.m_mem + io_offset;
        value_type   * const l_end = i_buffer.m_mem + i_buffer.m_allocated;

        if ( l_end <= ( l_str + sizeof( char ) ) )
        {
            return dxf::Converter::NeedMoreBytes;
        }

        short     l_val;
        int l_used;

        if ( 0xff != * reinterpret_cast<const unsigned char *>( l_str ) )
        {
            l_val = * reinterpret_cast<const unsigned char *>( l_str );
            l_used = 1;
        }
        else
        {
            // check to make sure we have enough data in the buffer
            if ( l_end <= ( l_str + 3 * sizeof( char ) ) )
            {
                return dxf::Converter::NeedMoreBytes;
            }

            if ( at::OSTraitsBase::IsBigEndian() )
            {
                at::Buffer::t_ConstRegion::t_non_const_value_type * l_pval =
                    reinterpret_cast< at::Buffer::t_ConstRegion::t_non_const_value_type * >( & l_val );

                l_pval[0] = l_str[2];
                l_pval[1] = l_str[1];
            }
            else
            {
                // if you get a bus error here - you need to change it to the
                // above code except no swapping ...
                l_val = * reinterpret_cast< const short * >( 1 + l_str );
            }
            l_used = 3;
        }
        
        const dxf::GroupCode & l_gc = dxf::GroupCode::GetGroupCode( l_val );

        if ( l_gc != dxf::GroupCode::s_error_code )
        {
            o_value = & l_gc;

            // update byte use
            io_offset += l_used;
            AT_Assert( io_offset <= i_buffer.m_allocated );
        
            // update report
            io_conv_report.UpdateBytesCount( l_used );
            
            return dxf::Converter::Success;
        }

        return dxf::Converter::Failed;

    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The group code to write
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >     i_wfile,
        const dxf::GroupCode                & i_value,
        at::StatusReport                  * i_report
    ) const {

        if ( unsigned( i_value.m_group_code ) >= 255 )
        {
            // do the 3 byte thing
            unsigned char l_data[ 3 ];

            l_data[0] = 255;

            at::Int16 l_code = i_value.m_group_code;
            unsigned char * l_str = reinterpret_cast<unsigned char *>( & l_code );
            if ( at::OSTraitsBase::IsBigEndian() )
            {
                l_data[1] = l_str[1];
                l_data[2] = l_str[0];
            }
            else
            {
                l_data[1] = l_str[0];
                l_data[2] = l_str[1];
            }

            return i_wfile->Write( reinterpret_cast<char *>( l_data ), 3, i_report );
        }
        else
        {
            // do the one byte thing.
            char        l_data = i_value.m_group_code;

            return i_wfile->Write( & l_data, 1, i_report );
        }
        
    }
    
    /**
     * s_converter is the static version of this converter
     */
    static const GroupCodeConverter_Binary      s_converter;

};

const GroupCodeConverter_Binary      GroupCodeConverter_Binary::s_converter;


// ======== GroupCodeConverter_Binary16 =================================
/**
 * 
 *
 */

class DXF_EXPORT GroupCodeConverter_Binary16
  : public dxf::GroupCodeConverter
{
    public:

    typedef at::Buffer::t_ConstRegion::value_type   value_type;

    virtual dxf::Converter::ConversionResult Convert(
        const dxf::GroupCode              * & o_value ,
        const at::Buffer::t_ConstRegion     & i_buffer,
        at::SizeMem                         & io_offset,
        dxf::ConversionReport               & io_conv_report
    ) const {

        value_type   * l_str = i_buffer.m_mem + io_offset;
        value_type   * const l_end = i_buffer.m_mem + i_buffer.m_allocated;

        if ( l_end <= ( l_str + 2 * sizeof( char ) ) )
        {
            return dxf::Converter::NeedMoreBytes;
        }

        short     l_val;
        
        if ( at::OSTraitsBase::IsBigEndian() )
        {
            at::Buffer::t_ConstRegion::t_non_const_value_type * l_pval =
                reinterpret_cast< at::Buffer::t_ConstRegion::t_non_const_value_type * >( & l_val );

            l_pval[0] = l_str[1];
            l_pval[1] = l_str[0];
        }
        else
        {
            // if you get a bus error here - you need to change it to the
            // above code except no swapping ...
            l_val = * reinterpret_cast< const short * >( l_str );
        }
        
        int l_used = 2;
        
        const dxf::GroupCode & l_gc = dxf::GroupCode::GetGroupCode( l_val );

        if ( l_gc != dxf::GroupCode::s_error_code )
        {
            o_value = & l_gc;

            // update byte use
            io_offset += l_used;
            AT_Assert( io_offset <= i_buffer.m_allocated );
                
            // update report
            io_conv_report.UpdateBytesCount( l_used );
            
            return dxf::Converter::Success;
        }

        return dxf::Converter::Failed;
        
    }
    
    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The group code to write
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< dxf::WriteFile * >     i_wfile,
        const dxf::GroupCode              & i_value,
        at::StatusReport                  * i_report
    ) const {

        at::Int16 l_code = i_value.m_group_code;
        
        if ( at::OSTraitsBase::IsBigEndian() )
        {
            unsigned char l_data[ 2 ];
            unsigned char * l_str = reinterpret_cast<unsigned char *>( & l_code );
            l_data[0] = l_str[1];
            l_data[1] = l_str[0];
            return i_wfile->Write( reinterpret_cast<char *>( l_data ), 2, i_report );
        }
        else
        {
            return i_wfile->Write( reinterpret_cast<char *>( & l_code ), 2, i_report );
        }
        
    }
    
    /**
     * s_converter is the static version of this converter
     */
    static const GroupCodeConverter_Binary16      s_converter;

};

const GroupCodeConverter_Binary16      GroupCodeConverter_Binary16::s_converter;


} //namespace dxf_impl

namespace dxf
{

//

using namespace dxf_impl;


// ======== GroupCode_Basic ===========================================
/**
 * Create a basic groupcode class
 *
 */

class DXF_EXPORT GroupCode_Basic
  : public GroupCode
{
    public:

    GroupCode_Basic( int i_group_code, GroupCodeSemantics i_semantic )
      : GroupCode( i_group_code, i_semantic )
    {
    }

};

GroupCode::GroupCode( int i_group_code, GroupCodeSemantics i_semantic )
  : m_group_code( i_group_code ),
    m_semantic( i_semantic )
{
}


// ======== GroupCode_Dxf ===========================================
/**
 * Create a basic groupcode class
 *
 */

class DXF_EXPORT GroupCode_Dxf
  : public GroupCode_Basic
{
    public:

    const GroupCodeConverters           & m_converter;

    GroupCode_Dxf( int i_group_code, GroupCodeSemantics i_semantic, const GroupCodeConverters & i_converter )
      : GroupCode_Basic( i_group_code, i_semantic ),
        m_converter( i_converter )
    {
    }

    virtual const Converter * GetConverter(
        ConverterType       i_converter
    ) const {
        return m_converter.GetConverter( i_converter );
    }
    
};

#define FillIn1(I) \
    GroupCode_Dxf( \
        MinGroupCode + (I), \
        GroupTypeData< MinGroupCode + (I) >::t_SemanticTraits::m_semantic, \
        GroupConverterByCode<MinGroupCode + (I)>::t_Converter::s_converter ), \
//end macro
    
#define FillIn10(J) \
    FillIn1(0+(J)) \
    FillIn1(1+(J)) \
    FillIn1(2+(J)) \
    FillIn1(3+(J)) \
    FillIn1(4+(J)) \
    FillIn1(5+(J)) \
    FillIn1(6+(J)) \
    FillIn1(7+(J)) \
    FillIn1(8+(J)) \
    FillIn1(9+(J)) \
// end macro

#define FillIn100(J) \
    FillIn10(0+(J)) \
    FillIn10(10+(J)) \
    FillIn10(20+(J)) \
    FillIn10(30+(J)) \
    FillIn10(40+(J)) \
    FillIn10(50+(J)) \
    FillIn10(60+(J)) \
    FillIn10(70+(J)) \
    FillIn10(80+(J)) \
    FillIn10(90+(J)) \
// end macro

#define FillIn1000(J) \
    FillIn100(0+(J)) \
    FillIn100(100+(J)) \
    FillIn100(200+(J)) \
    FillIn100(300+(J)) \
    FillIn100(400+(J)) \
    FillIn100(500+(J)) \
    FillIn100(600+(J)) \
    FillIn100(700+(J)) \
    FillIn100(800+(J)) \
    FillIn100(900+(J)) \
// end macro

#define FillIn70(J) \
    FillIn10(0+(J)) \
    FillIn10(10+(J)) \
    FillIn10(20+(J)) \
    FillIn10(30+(J)) \
    FillIn10(40+(J)) \
    FillIn10(50+(J)) \
    FillIn10(60+(J)) \
// end macro

#define FillIn7(J) \
    FillIn1(0+(J)) \
    FillIn1(1+(J)) \
    FillIn1(2+(J)) \
    FillIn1(3+(J)) \
    FillIn1(4+(J)) \
    FillIn1(5+(J)) \
    FillIn1(6+(J)) \
// end macro

// Initialize the array for all the GroupCodes.
const GroupCode_Dxf     g_group_codes[MaxGroupCode+1-MinGroupCode] =
{
    FillIn1000(0)
    FillIn70(1000)
    FillIn7(1070)
    // Currently there are 1077 group codes
    // if this changes then an adjustment to the "FillIn" code in needed.
};

GroupCode_Dxf     g_error_code( MinGroupCode -1, SpecialSemantic, GroupConverterByCode<MaxGroupCode+1>::t_Converter::s_converter );

// ======== GroupCode::GetGroupCode ===================================
// defined in header - implemented here
const GroupCode & GroupCode::GetGroupCode( int i_groupcode )
{
    if ( ( i_groupcode < MinGroupCode ) || ( i_groupcode > MaxGroupCode ) )
    {
        return g_error_code;
    }
    
    return g_group_codes[ i_groupcode - MinGroupCode ];
}


// ======== GroupCode_Record ===========================================
/**
 * Create a basic groupcode class
 *
 */

class DXF_EXPORT GroupCode_Record
  : public GroupCode_Basic
{
    public:

    GroupCode_Record( int i_group_code )
      : GroupCode_Basic( i_group_code, SpecialSemantic )
    {
    }

    virtual const Converter * GetConverter(
        ConverterType       i_converter
    ) const {
        // this converter must never be called.
        AT_Abort();
        return 0;
    }
    
};

// ======== GetGroupCodeConverter =================================

const GroupCodeConverter * GroupCode::GetGroupCodeConverter(
    GroupCode::ConverterType    i_conv_type
) {
    switch ( i_conv_type )
    {
        case dxf::GroupCode::DxfBinary :
        {
            return & GroupCodeConverter_Binary::s_converter;
        }
        
        case dxf::GroupCode::DxfBinary16 :
        {
            return & GroupCodeConverter_Binary16::s_converter;
        }
        
        case dxf::GroupCode::DxfAscii :
        {
            return & GroupCodeConverter_Ascii::s_converter;
        }
        default:
        {
            AT_Abort();
        }
    }
    
}
    


const GroupCode      & GroupCode::s_record = GroupCode_Record( MaxGroupCode + 2 );
const GroupCode      & GroupCode::s_entity = g_group_codes[ 0 - MinGroupCode ];
const GroupCode      & GroupCode::s_name =  g_group_codes[ 2 - MinGroupCode ];
const GroupCode      & GroupCode::s_handle =  g_group_codes[ 5 - MinGroupCode ];
const GroupCode      & GroupCode::s_comment = g_group_codes[ 999 - MinGroupCode ];
const GroupCode      & GroupCode::s_error_code = g_error_code;

} // namespace dxf


