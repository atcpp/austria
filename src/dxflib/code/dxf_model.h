/**
 *  dxf_model.h
 *
 */


#ifndef x_dxf_model_h_x
#define x_dxf_model_h_x 1

#include "dxf_converters.h"

#include <list>
#include <map>
#include <set>

namespace dxf
{
//


// ======== Nest ======================================================
/**
 * Nest describes the nesting attributes of dxf entities.  Unfortunately
 * the dxf format nesting of entities i semantic, e.g. SECTION/ENDSEC
 * POLYLINE/ENDSEQ etc.  Nest allows use to dynamically define the
 * nesting structure and keep the dxf parser relatively clean.
 *
 * The Nest class automatically registers itself with the internal
 * registry.  Simply creating a Nest object will register the nesting.
 * The lifetime of the nest object is irrelevant.  All strings passed to the
 * Nest functions must have infinite lifetime.
 *
 * Example usage:
 *
 *  Nest( "SECTION", "ENDSEC", Nest::NestAny() )
 *  Nest( "POLYLINE", "SEQEND", Nest::NestList() | "VERTEX" | "TEXT" )
 * 
 */

class DXF_EXPORT Nest
{
    public:

    Nest & operator|( const Nest & )
    {
        return * this;
    }

    // ======== NestList ==============================================
    /**
     * NestList contains a list of words.
     *
     */

    struct StrCmp
    {
        bool operator()( const char * i_lhs, const char * i_rhs ) const
        {
            return std::strcmp( i_lhs, i_rhs ) < 0;
        }
    };

    class NestList
    {
        public:

        // ======== NestListInternal ==================================
        /**
         * The basic internal interface of a "NestList"
         *
         */

        class NestListInternal
          : public at::PtrTarget_MT
        {
            public:


            // ======== HasString =====================================
            /**
             * Ask this nestlist if the string passed in is one of the
             * allowed strings.
             *
             *
             * @param i_str The string to seach for.
             * @return True if the string is in the NestList
             */

            virtual bool HasString(
                const std::string & i_str
            ) const = 0;


            // ======== InsertString ==================================
            /**
             * This inserts a string in the NestList
             *
             *
             * @param i_str The string to insert
             * @return nothing
             */

            virtual void InsertString( const char * i_str ) = 0;
            
        };


        // ======== NestListBasic_Strings =============================
        /**
         * This implements the nestlist with strings
         *
         */

        class NestListBasic_Strings
          : public NestListInternal
        {
            public:

            std::set< const char *, StrCmp >    m_strset;
            
            virtual bool HasString(
                const std::string & i_str
            ) const {
                return m_strset.find( i_str.c_str() ) != m_strset.end();
            }


            virtual void InsertString( const char * i_str )
            {
                m_strset.insert( i_str );
            }
        
        };


        // ======== NestListBasic_Any =================================
        /**
         * This is a "do anything" NestList
         *
         */

        class NestListBasic_Any
          : public NestListInternal
        {
            public:
            
            virtual bool HasString(
                const std::string & // unused
            ) const {
                return true;
            }


            virtual void InsertString( const char * i_str )
            {
                AT_Abort(); // never should be called.
            }

        };
        
        // ======== HasString =====================================
        /**
         * Ask this nestlist if the string passed in is one of the
         * allowed strings.
         *
         *
         * @param i_str The string to seach for.
         * @return True if the string is in the NestList
         */
        
        bool HasString(
            const std::string & i_str
        ) const {
            return m_nestlist->HasString( i_str );
        }

        /**
         * NestList constructor
         *
         */
        NestList( at::PtrDelegate< NestListInternal * > i_nl )
          : m_nestlist( i_nl )
        {
        }


        std::Ptr< NestListInternal * >  m_nestlist;

    };


    // ======== NestListStrings =======================================
    /**
     * NestListStrings allows the collection of various strings
     *
     */

    class NestListStrings
      : public NestList
    {
        public:

        NestListStrings()
          : NestList( new NestList::NestListBasic_Strings() )
        {
        }

        NestListStrings operator|( const char * i_str )
        {
            m_nestlist->InsertString( i_str );
            return * this;
        }
    };


    // ======== NestListAny ===========================================
    /**
     * This allows any entities to be nested
     *
     */

    class NestListAny
      : public NestList
    {
        public:

        /**
         * NestListAny
         *
         */
        NestListAny()
          : NestList( new NestList::NestListBasic_Any() )
        {
        }

    };
        
    

    // ======== Nest_Impl =============================================
    /**
     * This implements the Nest record
     *
     */

    class Nest_Impl
      : public at::PtrTarget_MT
    {
        public:

        /**
         * Nest_Impl
         *
         */
        Nest_Impl(
            const char      * i_begin_name,
            const char      * i_end_name,
            const NestList  & i_nestable_entities
        )
          : m_begin_name( i_begin_name ),
            m_end_name( i_end_name ),
            m_nestable_entities( i_nestable_entities )
        {
        }

        const char      * m_begin_name;
        const char      * m_end_name;
        const NestList    m_nestable_entities;


        // ======== IsEndItem =========================================
        /**
         * This checks if the passed in item is an "end" to the nest
         *
         * @param i_name
         * @return True to terminate the nest
         */

        bool IsEndItem( const std::string & i_name ) const
        {
            return i_name == m_end_name;
        }


        // ======== IsNestable ========================================
        /**
         * Returns true if the name is in the nestable list
         *
         * @param i_name
         * @return True if nesting of element is allowed
         */

        bool IsNestable( const std::string & i_name ) const
        {
            return m_nestable_entities.HasString( i_name );
        }
        
    };


    typedef std::map< const char *, at::Ptr< const Nest_Impl * >, StrCmp >    t_map;


    // ======== GetMap ================================================
    /**
     * Retrieve the map - make sure it is constructed before returning.
     *
     *
     * @return A reference to the map.
     */

    static t_map & GetMap();

    /**
     * Nest constructor
     *
     */
    Nest(
        const char * i_begin_name,
        const char * i_end_name,
        const NestList & i_nestable_entities
    )
    {
        t_map & l_map = GetMap();

        l_map[ i_begin_name ] = new Nest_Impl( i_begin_name, i_end_name, i_nestable_entities );        
    }



    // ======== FindNestInfo ==========================================
    /**
     * Find a nest item (if it exists)
     *
     * @param i_name
     * @return pointer to nest info or null.
     */

    static at::PtrDelegate<const Nest_Impl *> FindNestInfo(
        const std::string & i_name
    ) {
        const t_map & l_map = GetMap();

        t_map::const_iterator l_itr = l_map.find( i_name.c_str() );

        if ( l_itr == l_map.end() )
        {
            return 0;
        }
        
        return l_itr->second;
    }

};

typedef Nest::NestListAny       NestAny;
typedef Nest::NestListStrings   NestList;
typedef Nest::Nest_Impl         NestInfo;

// ======== Record ====================================================
/**
 * This is a recursive data structure.  Record can contain Record items.
 *
 */

class DXF_EXPORT Record
  : public at::PtrTarget_MT
{
    public:


    typedef std::list<Item> Items;

    /**
     * m_items is the collection of items for this
     * record.
     */
    Items               m_items;

    /**
     * This would be the type of object.
     */
    std::string         m_object_type;

    /**
     * This would be the object handle.
     */
    std::string         m_object_handle;


    /**
     * This is the value of the object's Group 2 record.
     */
    std::string         m_object_name;


    // ======== Append ================================================
    /**
     *	Append an item to this Record.  This also looks out for
     *  object type.
     *
     *
     * @param i_item The item to append
     * @return nothing
     */

    void Append( const Item & i_item )
    {
        m_items.push_back( i_item );

        // if we have no object type - we fill it in
        if ( m_object_type.empty() )
        {
            if ( i_item.GetCode() == GroupCode::s_entity )
            {
                // this will throw if the entity is question is
                // not a std::string value
                ( * i_item.m_value ).Read( m_object_type );
            }
        }
        
        // if we have no object name - we fill it in now
        if ( m_object_name.empty() )
        {
            if ( i_item.GetCode() == GroupCode::s_name )
            {
                // this will throw if the entity is question is
                // not a std::string value
                ( * i_item.m_value ).Read( m_object_name );
            }
        }
        
        // if we have no object name - we fill it in now
        if ( m_object_handle.empty() )
        {
            if ( i_item.GetCode() == GroupCode::s_handle )
            {
                // this will throw if the entity is question is
                // not a std::string value
                ( * i_item.m_value ).Read( m_object_handle );
            }
        }
        
    }

    // ======== FindItem ==============================================
    /**
     * FindItem will find the first item of a particular type.
     *
     *
     * @param i_group_code
     * @return nothing
     */

    Item & FindItem(
        int i_group_code
    );


    // ======== Find ==================================================
    /**
     * Find a corresponding item
     *
     *
     * @param i_X
     * @return nothing
     */

    template <int w_CmpGroupCode, int w_OutGroupCode, typename w_CmpType, typename w_OutType, typename w_DefaultType>
    bool Find(
        const w_CmpType     & i_cmp_val,
        w_OutType           & o_out_val,
        w_DefaultType         i_default
    ) {

        Items::iterator l_itr = m_items.begin();
        const Items::iterator l_end = m_items.end();

        for ( ; l_itr != l_end; ++ l_itr )
        {
            if ( l_itr->GetCode() == GroupCodeRef<w_CmpGroupCode>::s_code )
            {
                // compare values
                const w_CmpType & l_cmp_val = l_itr->m_value->Refer();

                if ( l_cmp_val == i_cmp_val )
                {
                    ++ l_itr;
                
                    for ( ; l_itr != l_end; ++ l_itr )
                    {
                        
                        if ( l_itr->GetCode() == GroupCodeRef<w_OutGroupCode>::s_code )
                        {
                            const std::string   & l_val = l_itr->m_value->Refer();
                        
                            o_out_val = l_val;
                            return true;
                        }
                    }
                    
                    o_out_val = i_default;
                    return false;
                }
                
            }
        }

        o_out_val = i_default;
        return false;
    }
    
};


// ======== TraverseContext ===========================================
/**
 * TraverseContext is used to traverse the model
 *
 */

class DXF_EXPORT TraverseContext
{
    public:


    // ======== PushItem ==============================================
    /**
     * 
     *
     *
     * @param i_record
     */

    virtual void PushRecord( at::PtrView< Record * >  i_record ) = 0;


    // ======== PopRecord =============================================
    /**
     * A record is complete
     *
     */

    virtual void PopRecord() = 0;


    // ======== PushItem ==============================================
    /**
     * Pushes an item into the context
     *
     * @param i_item
     */

    virtual void PushItem( const Item & i_item ) = 0;
    

};


// ======== Model =====================================================
/**
 * 
 *
 */

class DXF_EXPORT Model
  : public at::PtrTarget_MT
{
    public:

    Model()
      : m_data( new Record ),
        m_state( Initial ),
        m_trace( 0 ),
        m_out_trace( false )
    {
        m_stack.push_front( StackItem( m_data, 0, true ) );
    }

    at::Ptr< Record * >                 m_data;


    // ======== StackItem =============================================
    /**
     * Stack item contains the state of the parser at theat point in
     * the parse.
     */

    class StackItem
    {
        public:

        // ======== StackItem =========================================
        /**
         * StackItem constructor.  This defines the "stakability"
         *
         *
         * @param i_record
         * @param i_nest_info
         * @param i_nest_all
         * @return nothing
         */

        StackItem(
            at::PtrView< Record * >             i_record,
            at::PtrView< const NestInfo * >     i_nest_info,
            bool                                i_nest_all = false
        )
          : m_nestall( i_nest_all ),
            m_record( i_record ),
            m_nest_info( i_nest_info )
        {
        }


        enum Action
        {
            DoPush,
            DoPop,
            DoAppendPop
        };


        bool                            m_nestall;
        
        at::Ptr< Record * >             m_record;

        at::Ptr< const NestInfo * >     m_nest_info;
    };
    
    
    typedef std::list< StackItem >      t_Stack;
    t_Stack                             m_stack;

    enum ReadState
    {
        Initial,
        InSection,
    };

    ReadState                             m_state;

    // set this to a valid ostream for read debug tracing
    std::ostream                        * m_trace;

    // to to true for tracing of output
    bool                                  m_out_trace;

    // ======== NestAction ========================================
    /**
     * NestAction determines what to perform for this nest
     * given the item that follows.
     *
     * @param i_item An s_entity group item
     * @return Action
     */

    void NestAction(
        const Item & i_item
    ) {
        if ( m_stack.front().m_nestall )
        {
            PushRecord( i_item );
            return;
        }

        t_Stack::iterator                 l_itr = m_stack.begin();
        const t_Stack::iterator           l_end = m_stack.end();
        const std::string               & l_name = i_item.m_value->Refer();
        
        while ( l_itr != l_end )
        {
            if ( l_itr->m_nest_info )
            {
                if ( l_itr->m_nest_info->IsEndItem( l_name ) )
                {
                    while ( m_stack.begin() != l_itr )
                    {
                        PopRecord();
                    }
                    Append( i_item );
                    PopRecord();
                    return;
                }
            }
            ++ l_itr;
        }
        
        if ( ! m_stack.front().m_nest_info )
        {
            PopRecord();
            PushRecord( i_item );
            return;
        }

        if ( m_stack.front().m_nest_info->IsNestable( l_name ) )
        {
            PushRecord( i_item );
            return;
        }

        PopRecord();
        PushRecord( i_item );
        return;
    }
        

    // ======== PushRecord ============================================
    /**
     * PushRecord will create a new record and "PUSH" it onto the stack.
     *
     *
     * @param i_item
     * @return nothing
     */

    void PushRecord( const Item & i_push_item )
    {
        Item    l_item( GroupCode::s_record );

        at::Ptr< Record * >     l_record = new Record();

        l_item.m_value = new ValueType( l_record, at::AnyInit );

        m_stack.front().m_record->Append( l_item );

        m_stack.push_front( StackItem( l_record, Nest::FindNestInfo( i_push_item.m_value->Refer() ) ) );

        l_record->Append( i_push_item );
    }


    // ======== PopRecord =============================================
    /**
     * Pop the record off the stack.
     *
     * @return nothing
     */

    void PopRecord()
    {
        m_stack.pop_front();
    }


    // ======== DumpStack =============================================
    /**
     * DumpStack prints the contents of the stack
     *
     *
     * @param o_ostream
     * @return nothing
     */

    std::ostream & DumpStack( std::ostream & o_ostream )
    {
        t_Stack::const_iterator           l_itr = m_stack.begin();
        const t_Stack::const_iterator     l_end = m_stack.end();

        const char  * l_prefix = "";

        while ( l_itr != l_end )
        {
            o_ostream << l_prefix << l_itr->m_record->m_object_type << "(" << l_itr->m_record->m_object_name << ")";
            ++ l_itr;
            l_prefix = " ";
        }

        return o_ostream;
    }


    // ======== Append ================================================
    /**
     * Append a record to the current record
     *
     *
     * @param i_item The item to append
     * @return nothing
     */

    void Append( const Item & i_item )
    {
        m_stack.front().m_record->Append( i_item );
    }

    // ======== InsertItem ============================================
    /**
     * InsertItem places a new item into a record.
     *
     * @param i_data
     * @return nothing
     */

    virtual void	InsertItem( const Item & i_item )
    {
        // insert comments exactly where you get them
        if ( i_item.GetCode() == GroupCode::s_comment )
        {
            // insert this puppy in the current record

            Append( i_item );
        }
        else
        {

            if ( i_item.GetCode() == GroupCode::s_entity )
            {
                // are we tracing ?
                if ( m_trace )
                {
                    DumpStack( * m_trace ) << " Insert " << i_item << "\n";
                }
                
                NestAction( i_item );

                // are we tracing ?
                if ( m_trace )
                {
                    DumpStack( std::cerr ) << "\n";
                }
            }
            else
            {
                Append( i_item );
            }
        }
    }


    // ======== Write =================================================
    /**
     * Write the contents of this to a "File"
     *
     * @param i_wfile The file to write to
     * @param i_mode The type of DXF file to write
     * @param i_report Place where errors are reported
     * @return True if all is well
     */

    bool Write(
        at::Ptr< WriteFile * >        i_wfile,
        GroupCode::ConverterType      i_converter_type,
        at::StatusReport            * i_report
    );


    // ======== Traverse ==============================================
    /**
     *	
     *
     *
     * @param i_context - The context for this traversal.
     */

    virtual void Traverse( TraverseContext * i_context );
    
};


} // end namespace


#endif // x_dxf_model_h_x



