
#include "dxf_file.h"

// ======== ReportError ===============================================
/**
 * Call the status reporter if there is one
 *
 *
 * @param i_report is the report parameteter
 * @param i_str is the message to report.
 * @return nothing
 */

namespace
{

void ReportError(
    at::StatusReport                      * i_report,
    const std::string                     & i_str
) {

    if ( i_report )
    {
        i_report->ReportStatus(
            at::StatusReport::ErrorStatus,
            0,
            i_str
        );
    }
}

} // anon namespace

namespace dxf
{
//

ReadFile::ReadFile( at::PtrDelegate< BasicReadFile * >  i_file )
  : m_type( GroupCode::DxfAscii ),
    m_last_code(),
    m_file( i_file ),
    m_bufsz( 1<< 16 )
{
}


ReadFile::ReadFile( std::istream & i_file )
  : m_type( GroupCode::DxfAscii ),
    m_last_code(),
    m_file( new StdIstreamReadFile( i_file ) ),
    m_bufsz( 1<< 16 )
{
}

Converter::ConversionResult ReadFile::ProcessData(
    const at::Buffer::t_ConstRegion & l_region,
    at::SizeMem                     & i_offset,
    ConversionReport                & i_report
) {

    // get the current groupcode converter
    Converter::ConversionResult     l_result;

    const GroupCodeConverter * l_gcconv = GroupCode::GetGroupCodeConverter( m_type );

    AT_Assert( l_gcconv );
    
    while (true)
    {
        while ( ! m_last_code )
        {

            l_result = l_gcconv->Convert( m_last_code, l_region, i_offset, i_report );

            AT_Assert( i_offset <= l_region.m_allocated );
            
            if ( Converter::Success == l_result )
            {
                break;
            }

            return l_result;
        }

        const Converter * l_converter = m_last_code->GetConverter( m_type );

        Item    l_item( * m_last_code );
        
        l_result = l_converter->Convert( l_item.m_value, l_region, i_offset, i_report );

        AT_Assert( i_offset <= l_region.m_allocated );

        if ( Converter::Success != l_result )
        {
            return l_result;
        }

        m_model->InsertItem( l_item );
        m_last_code = 0;
        
    }

    return l_result;
    
}

at::PtrDelegate< Model * > ReadFile::Read( at::StatusReport * i_report )
{
    at::Ptr< at::Buffer * >     l_data;

    // start with a clean model
    m_model = new Model;
    m_last_code = 0;

    // first thing is to discover what type of conversion to use.
    m_type = GroupCode::DxfAscii;

    const at::SizeMem   l_minlen = ( 3 + at::SizeMem( sizeof( DXF_BINARY_SENTINEL ) ) );
    BasicReadFile::ReadResult  l_read_rez;

    while ( BasicReadFile::Success == ( l_read_rez = m_file->Read( l_data, m_bufsz, i_report ) ) )
    {
        if ( l_data->Region().m_allocated >= l_minlen )
        {
            break;
        }
    }

    if ( BasicReadFile::Success != l_read_rez )
    {
        // nope - we have errors
        return 0;
    }

    // OK - we now have enough data

    // check to see if this is a binary file.
    {
        const at::Buffer::t_ConstRegion & l_region = l_data->Region();
        
        if ( ! std::memcmp( DXF_BINARY_SENTINEL, l_region.m_mem, sizeof( DXF_BINARY_SENTINEL ) ) )
        {

            if ( l_region.m_mem[ sizeof( DXF_BINARY_SENTINEL ) +1 ] )
            {
                m_type = GroupCode::DxfBinary;
            }
            else
            {
                m_type = GroupCode::DxfBinary16;
            }
            
            l_data->Erase( 0, sizeof( DXF_BINARY_SENTINEL ) );
        }
    }

    ConversionReport        l_creport( i_report );

    
    // read the bits-n-pieces.
    do {

        at::SizeMem l_offset = 0;

        Converter::ConversionResult l_result = ProcessData(
            l_data->Region(),
            l_offset,
            l_creport
        );

        if ( Converter::Failed == l_result )
        {
            return 0;
        }

        // remove that portion we have used.
        l_data->Erase( 0, l_offset );
        
    } while ( BasicReadFile::Success == ( l_read_rez = m_file->Read( l_data, m_bufsz, i_report ) ) );

    if ( BasicReadFile::Failed == l_read_rez )
    {
        return 0;
    }

    return m_model;
}

StdIstreamReadFile::StdIstreamReadFile( std::istream & i_ostream )
  : m_istream( i_ostream )
{
}


StdIstreamReadFile::ReadResult StdIstreamReadFile::Read(
    at::Ptr< at::Buffer * >   & io_data,
    at::SizeMem                 i_len,
    at::StatusReport          * i_report
) {

    if ( m_istream.eof() )
    {
        return EndOfFile;
    }

    if ( m_istream.bad() )
    {
        return Failed;
    }

    if ( ! io_data )
    {
        io_data = at::NewBuffer();
    }

    const at::Buffer::t_Region & l_region = io_data->RegionExtend( i_len, i_len /2 + 1 );

    AT_Assert( ( l_region.m_max_available - l_region.m_allocated ) >= i_len );

    std::streambuf * l_pbuf = m_istream.rdbuf();
    
    std::size_t l_len_read = l_pbuf->sgetn( l_region.m_mem + l_region.m_allocated, i_len );

    if ( l_len_read )
    {
        io_data->SetAllocated( l_region.m_allocated + l_len_read );
        return Success;
    }

    if ( m_istream.eof() )
    {
        return EndOfFile;
    }

    if ( m_istream.bad() )
    {
        return Failed;
    }

    return EndOfFile;;

}

at::SizeMem StdIstreamReadFile::Length()
{
    std::istream::pos_type   l_curpos = m_istream.tellg();
    m_istream.seekg( 0, std::ios::end );
    std::istream::pos_type   l_length = m_istream.tellg();
    m_istream.seekg( l_curpos, std::ios::beg );
    return l_length;
}


// ======== WriteRecord ===============================================
/**
 *	WriteRecord is a recursive function that writes "records"
 *
 *
 */

struct OnEnd
{
    OnEnd( 
        const std::string               & i_str,
        at::Ptr< WriteFile * >          i_wfile
    )
      : m_str( i_str ),
        m_wfile( i_wfile )
    {
    }

    ~OnEnd()
    {
        if ( m_str.length() )
        {
            m_wfile->Write( m_str.data(), m_str.length(), 0 );
        }
    }

    std::string                     m_str;
    at::Ptr< WriteFile * >          m_wfile;
    
};

bool WriteRecord(
    at::Ptr< Record * >             i_record,
    at::Ptr< WriteFile * >          i_wfile,
    const GroupCode::ConverterType  i_converter_type,
    bool                            i_do_trace,
    at::StatusReport              * i_report
) {

    Record::Items::iterator l_itr = i_record->m_items.begin();
    const Record::Items::iterator l_end = i_record->m_items.end();

    const GroupCodeConverter    * l_gcc = GroupCode::GetGroupCodeConverter( i_converter_type );

    static const char  s_crlf[] = "\r\n";

    OnEnd   l_onend( "", i_wfile );
    if ( i_do_trace )
    {
        {
            std::ostringstream l_msg;
            l_msg << "*** begin write of record " << i_record->m_object_type << "(" << i_record->m_object_name << ") r" << (&*i_record) << " ***\n";
            OnEnd( l_msg.str(), i_wfile );
        }
        {
            std::ostringstream l_msg;
            l_msg << "*** finish write of record " << i_record->m_object_type << "(" << i_record->m_object_name << ") r" << (&*i_record) << " ***\n";
            l_onend.m_str = l_msg.str();
        }
    }

    while ( l_itr != l_end )
    {
        Item    & l_curitrm = * ( l_itr ++ );

        const GroupCode & l_gc = l_curitrm.GetCode();

        if ( l_gc == GroupCode::s_record )
        {
            at::Ptr< Record * > l_record = l_curitrm.m_value->Refer();

            if ( ! WriteRecord( l_record, i_wfile, i_converter_type, i_do_trace, i_report ) )
            {
                return false;
            }
        }
        else
        {
            if ( ! l_gcc->Write( i_wfile, l_gc, i_report ) )
            {
                return false;
            }

            if ( i_converter_type == GroupCode::DxfAscii )
            {
                if ( ! i_wfile->Write( s_crlf, 2, i_report ) )
                {
                    return false;
                }
            }
            
            if ( ! l_gc.GetConverter( i_converter_type )->Write( i_wfile, l_curitrm.m_value, i_report ) )
            {
                return false;
            }
            
            if ( i_converter_type == GroupCode::DxfAscii )
            {
                if ( ! i_wfile->Write( s_crlf, 2, i_report ) )
                {
                    return false;
                }
            }
            
        }
    }

    return true;
}

// ======== Model::Write ==============================================

bool Model::Write(
    at::Ptr< WriteFile * >        i_wfile,
    GroupCode::ConverterType      i_converter_type,
    at::StatusReport            * i_report
) {

    if ( i_converter_type != GroupCode::DxfAscii )
    {
        if ( ! i_wfile->Write( DXF_BINARY_SENTINEL, sizeof( DXF_BINARY_SENTINEL ), i_report ) )
        {
            return false;
        }
    }

    return WriteRecord( m_data, i_wfile, i_converter_type, m_out_trace, i_report );
}
    
StdOstreamWriteFile::StdOstreamWriteFile(
    std::ostream    & i_stdostream
)
  : m_stdostream( i_stdostream )
{
}

bool StdOstreamWriteFile::Write(
    const char              * i_mem,
    at::SizeMem               i_len,
    at::StatusReport        * i_report
)
{
    if ( m_stdostream.bad() )
    {
        
        return false;
    }

    if ( !m_stdostream.write( i_mem, i_len ) )
    {
        ReportError( i_report, "ostream write failed" );
        return false;
    }

    return true;
}

bool StdOstreamWriteFile::Flush( at::StatusReport * i_report )
{
    if ( m_stdostream.bad() )
    {
        ReportError( i_report, "ostream object is bad" );
        return false;
    }

    if ( ! m_stdostream.flush() )
    {
        ReportError( i_report, "ostream flush failed" );
        return false;
    }

    return true;
}
    
} // namespace
