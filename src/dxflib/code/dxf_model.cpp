

#include "dxf_model.h"


namespace dxf_model
{
//
using namespace dxf;
    
Nest s_nest =
    Nest( "SECTION", "ENDSEC", NestAny() )
    | Nest( "BLOCK", "ENDBLK", NestAny() )
    | Nest( "POLYLINE", "SEQEND", NestList() | "VERTEX" )
    | Nest( "INSERT", "SEQEND", NestList() | "ATTRIB" )
    | Nest( "TABLE", "ENDTAB", NestAny() );



} // end namespace

namespace dxf
{
//

Nest::t_map & Nest::GetMap()
{
    static t_map   s_map;

    return s_map;
}



void TraverseRecord(
    at::PtrView< Model * >              i_model,
    at::Ptr< Record * >                 i_record,
    TraverseContext                   * i_context
) {

    i_context->PushRecord( i_record );
    
    Record::Items::iterator l_itr = i_record->m_items.begin();
    const Record::Items::iterator l_end = i_record->m_items.end();


    while ( l_itr != l_end )
    {
        Item    & l_curitrm = * ( l_itr ++ );

        const GroupCode & l_gc = l_curitrm.GetCode();

        if ( l_gc == GroupCode::s_record )
        {
            at::Ptr< Record * > l_record = l_curitrm.m_value->Refer();

            TraverseRecord( i_model, l_record, i_context );
        }
        else
        {
            i_context->PushItem( l_curitrm );
        }
    }

    i_context->PopRecord();

}

// ======== Model::Traverse ==============================================

void Model::Traverse(
    TraverseContext *           i_context
) {


    return TraverseRecord( this, m_data, i_context );
}

} // end namespace
