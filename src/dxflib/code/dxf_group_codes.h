/**
 *  dxf_group_codes.h
 *
 */


#ifndef x_dxf_group_codes_h_x
#define x_dxf_group_codes_h_x 1


#include "dxf_exports.h"
#include <string>
#include "at_types.h"

namespace dxf
{
//

/**
 * GroupCodeSemantics correspond to the different "type" of value
 * indicated in the Autodesk dxf documentation section:
 *      "Group Codes in Numerical Order"
 */

enum GroupCodeSemantics
{
    Error = 1,
    SpecialSemantic,
    angle,
    arbitrary_object_handle,
    binary_chunk,
    boolean_flag_value,
    color_number,
    comment,
    conditional_operator,
    dimvar_object_handle,
    entities_follow_flag,
    entity_handle,
    entity_name_never_saved,
    entity_name_reference,
    entity_type,
    entity_visibility,
    extended_data_control_string,
    extended_data_layer_name,
    extended_data_sentinel,
    extrusion_direction,
    floating_value,
    hard_owner,
    hard_pointer,
    integer_value,
    layer_name,
    linetype_name,
    lineweight,
    model_or_paper_space_flag,
    name_value,
    objectarx_subclass_markers,
    plotstylename,
    plotstylename_handle_value,
    point_value,
    primary_text_value,
    reactor_chain,
    registered_application_name,
    scale_factors_misc,
    soft_owner,
    soft_pointer,
    subclass_data_marker,
    text_style_name,
    text_value,
    thickness,
    ucs_origin,
    variable_name_identifier,
    viewport_active_flag,
    viewport_identification,
};


// ======== SemanticsTraits ===========================================
/**
 * SemanticsTraits is the template for semantics traits and
 * can be extended.
 */

template < GroupCodeSemantics w_GCSemantic >
class SemanticsTraits
{
    public:

    static const GroupCodeSemantics m_semantic = w_GCSemantic;
};


// ======== SemanticsTraitsUndefined ==================================
/**
 * SemanticsTraitsUndefined is for filling in the blanks
 *
 *
 */

class SemanticsTraitsUndefined
{
    public:

    static const GroupCodeSemantics m_semantic = Error;
};


// ======== GroupSemanticRange ===================================
/**
 * This is for specializations of GroupSemanticData that contain actual data.
 *
 */

template <
    int                 w_From,
    int                 w_To,
    GroupCodeSemantics  w_GCSemantic
>
class GroupSemanticRange
{
    public:

    enum {
        e_From = w_From,
        e_To = w_To
    };

    typedef SemanticsTraits<w_GCSemantic>   t_SemanticTraits;

};


// ======== GroupSemanticData =============================================
/**
 * GroupSemanticData scans recursively through the types looking for
 * the specialization that corresponds to the w_GroupCode parameter.
 * This is done so that the specifications for the group type data
 * correspond in a 1:1 fashion with the dxf spec.  This makes the
 * compiler fill in all the blanks and hence less tedious or
 * error prone.
 */

template <int w_GroupCode>
class GroupSemanticData
{
    public:

    enum {
        e_From = GroupSemanticData<w_GroupCode-1>::e_From,
        e_To = GroupSemanticData<w_GroupCode-1>::e_To
    };

    typedef
        typename at::TypeSelect<
            (e_To < w_GroupCode),
            SemanticsTraitsUndefined,
            typename GroupSemanticData<w_GroupCode-1>::t_SemanticTraits
        >::t_Type
        t_SemanticTraits;
};

template <> class GroupSemanticData< -5 > : public GroupSemanticRange< -5, -5, reactor_chain > {};
template <> class GroupSemanticData< -4 > : public GroupSemanticRange< -4, -4, conditional_operator > {};
template <> class GroupSemanticData< -3 > : public GroupSemanticRange< -3, -3, extended_data_sentinel > {};
template <> class GroupSemanticData< -2 > : public GroupSemanticRange< -2, -2, entity_name_reference > {};
template <> class GroupSemanticData< -1 > : public GroupSemanticRange< -1, -1, entity_name_never_saved > {};
template <> class GroupSemanticData< 0 > : public GroupSemanticRange< 0, 0, entity_type > {};
template <> class GroupSemanticData< 1 > : public GroupSemanticRange< 1, 1, primary_text_value > {};
template <> class GroupSemanticData< 2 > : public GroupSemanticRange< 2, 2, name_value > {};
template <> class GroupSemanticData< 3 > : public GroupSemanticRange< 3, 4, text_value > {};
template <> class GroupSemanticData< 5 > : public GroupSemanticRange< 5, 5, entity_handle > {};
template <> class GroupSemanticData< 6 > : public GroupSemanticRange< 6, 6, linetype_name > {};
template <> class GroupSemanticData< 7 > : public GroupSemanticRange< 7, 7, text_style_name > {};
template <> class GroupSemanticData< 8 > : public GroupSemanticRange< 8, 8, layer_name > {};
template <> class GroupSemanticData< 9 > : public GroupSemanticRange< 9, 9, variable_name_identifier > {};
template <> class GroupSemanticData< 10 > : public GroupSemanticRange< 10, 18, point_value > {};
template <> class GroupSemanticData< 39 > : public GroupSemanticRange< 39, 39, thickness > {};
template <> class GroupSemanticData< 40 > : public GroupSemanticRange< 40, 49, scale_factors_misc > {};
template <> class GroupSemanticData< 50 > : public GroupSemanticRange< 50, 58, angle > {};
template <> class GroupSemanticData< 60 > : public GroupSemanticRange< 60, 60, entity_visibility > {};
template <> class GroupSemanticData< 62 > : public GroupSemanticRange< 62, 62, color_number > {};
template <> class GroupSemanticData< 66 > : public GroupSemanticRange< 66, 66, entities_follow_flag > {};
template <> class GroupSemanticData< 67 > : public GroupSemanticRange< 67, 67, model_or_paper_space_flag > {};
template <> class GroupSemanticData< 68 > : public GroupSemanticRange< 68, 68, viewport_active_flag > {};
template <> class GroupSemanticData< 69 > : public GroupSemanticRange< 69, 69, viewport_identification > {};
template <> class GroupSemanticData< 70 > : public GroupSemanticRange< 70, 78, integer_value > {};
template <> class GroupSemanticData< 90 > : public GroupSemanticRange< 90, 99, integer_value > {};
template <> class GroupSemanticData< 100 > : public GroupSemanticRange< 100, 100, subclass_data_marker > {};
template <> class GroupSemanticData< 102 > : public GroupSemanticRange< 102, 102, objectarx_subclass_markers > {};
template <> class GroupSemanticData< 105 > : public GroupSemanticRange< 105, 105, dimvar_object_handle > {};
template <> class GroupSemanticData< 110 > : public GroupSemanticRange< 110, 110, ucs_origin > {};
template <> class GroupSemanticData< 140 > : public GroupSemanticRange< 140, 149, floating_value > {};
template <> class GroupSemanticData< 170 > : public GroupSemanticRange< 170, 179, integer_value > {};
template <> class GroupSemanticData< 210 > : public GroupSemanticRange< 210, 210, extrusion_direction > {};
template <> class GroupSemanticData< 270 > : public GroupSemanticRange< 270, 279, integer_value > {};
template <> class GroupSemanticData< 280 > : public GroupSemanticRange< 280, 289, integer_value > {};
template <> class GroupSemanticData< 290 > : public GroupSemanticRange< 290, 299, boolean_flag_value > {};
template <> class GroupSemanticData< 300 > : public GroupSemanticRange< 300, 309, text_value > {};
template <> class GroupSemanticData< 310 > : public GroupSemanticRange< 310, 319, binary_chunk > {};
template <> class GroupSemanticData< 320 > : public GroupSemanticRange< 320, 329, arbitrary_object_handle > {};
template <> class GroupSemanticData< 330 > : public GroupSemanticRange< 330, 339, soft_pointer > {};
template <> class GroupSemanticData< 340 > : public GroupSemanticRange< 340, 349, hard_pointer > {};
template <> class GroupSemanticData< 350 > : public GroupSemanticRange< 350, 359, soft_owner > {};
template <> class GroupSemanticData< 360 > : public GroupSemanticRange< 360, 369, hard_owner > {};
template <> class GroupSemanticData< 370 > : public GroupSemanticRange< 370, 379, lineweight > {};
template <> class GroupSemanticData< 380 > : public GroupSemanticRange< 380, 389, plotstylename > {};
template <> class GroupSemanticData< 390 > : public GroupSemanticRange< 390, 399, plotstylename_handle_value > {};
template <> class GroupSemanticData< 400 > : public GroupSemanticRange< 400, 409, integer_value > {};
template <> class GroupSemanticData< 410 > : public GroupSemanticRange< 410, 419, text_value > {};
template <> class GroupSemanticData< 420 > : public GroupSemanticRange< 420, 427, integer_value > {};
template <> class GroupSemanticData< 430 > : public GroupSemanticRange< 430, 437, text_value > {};
template <> class GroupSemanticData< 440 > : public GroupSemanticRange< 440, 447, integer_value > {};
template <> class GroupSemanticData< 450 > : public GroupSemanticRange< 450, 459, integer_value > {};
template <> class GroupSemanticData< 460 > : public GroupSemanticRange< 460, 469, floating_value > {};
template <> class GroupSemanticData< 470 > : public GroupSemanticRange< 470, 479, text_value > {};
template <> class GroupSemanticData< 999 > : public GroupSemanticRange< 999, 999, comment > {};
template <> class GroupSemanticData< 1000 > : public GroupSemanticRange< 1000, 1000, text_value > {};
template <> class GroupSemanticData< 1001 > : public GroupSemanticRange< 1001, 1001, registered_application_name > {};
template <> class GroupSemanticData< 1002 > : public GroupSemanticRange< 1002, 1002, extended_data_control_string > {};
template <> class GroupSemanticData< 1003 > : public GroupSemanticRange< 1003, 1003, extended_data_layer_name > {};
template <> class GroupSemanticData< 1004 > : public GroupSemanticRange< 1004, 1004, binary_chunk > {};
template <> class GroupSemanticData< 1005 > : public GroupSemanticRange< 1005, 1005, entity_handle > {};
template <> class GroupSemanticData< 1010 > : public GroupSemanticRange< 1010, 1013, point_value > {};
template <> class GroupSemanticData< 1040 > : public GroupSemanticRange< 1040, 1040, floating_value > {};
template <> class GroupSemanticData< 1041 > : public GroupSemanticRange< 1041, 1041, floating_value > {};
template <> class GroupSemanticData< 1042 > : public GroupSemanticRange< 1042, 1042, floating_value > {};
template <> class GroupSemanticData< 1070 > : public GroupSemanticRange< 1070, 1070, integer_value > {};
template <> class GroupSemanticData< 1071 > : public GroupSemanticRange< 1071, 1071, integer_value > {};


/**
 * This file embodies the group specifications from the
 * DXF Reference documentation.
 */

// ======== UndefinedValue ============================================
/**
 * Special "undefined" value type.  Trying to convert to or from this
 * type should be an error.
 */

class DXF_EXPORT UndefinedValue
{
    private:
    UndefinedValue();
    UndefinedValue( const UndefinedValue & );
    UndefinedValue & operator=( const UndefinedValue & );
};


// ======== BinaryXdata ===============================================
/**
 * BinaryXdata is for reading data from "Xdata" sections of the file.
 *
 */

class DXF_EXPORT BinaryXdata
{
    private:
    BinaryXdata();
    BinaryXdata( const BinaryXdata & );
    BinaryXdata & operator=( const BinaryXdata & );
};


// ======== GroupTraits_Undefined =====================================
/**
 * Define the "undefined" group traits.  For Ascii conversion,
 * it should simply read in the value write it back out exactly as it was
 * found. For binary data, since it can't be read, there is little we can
 * do except abort.
 *
 */

class GroupTraits_Undefined
{
    public:

    enum { SkipWhiteSpace = false };
    typedef UndefinedValue  t_ExternalType;
    typedef std::string     t_InternalType;
};



// ======== GroupRangeDefine ==========================================
/**
 * This is used in the specializations of the GroupTypeData template
 * below.  It contains the specification of the beginning of the
 * range as specified in the dxf file.
 */

template <
    int         w_From,
    int         w_To,
    typename    w_ExternalType,
    typename    w_InternalType = w_ExternalType,
    bool        w_skip_white = false
>
class GroupRangeDefine
{
    public:

    enum {
        e_From = w_From,
        e_To = w_To
    };


    // ======== Traits ================================================
    /**
     * This defines the traits for this group.
     *
     */

    class Traits
    {
        public:
        enum { SkipWhiteSpace = false };
        typedef w_ExternalType  t_ExternalType;
        typedef w_InternalType  t_InternalType;
    };
    

    typedef Traits  t_Traits;
    
    typedef typename GroupSemanticData<w_From>::t_SemanticTraits         t_SemanticTraits;
};



// ======== GroupTypeData =============================================
/**
 * GroupTypeData scans recursively through the types looking for
 * the specialization that corresponds to the w_GroupCode parameter.
 * This is done so that the specifications for the group type data
 * correspond in a 1:1 fashion with the dxf spec.  This makes the
 * compiler fill in all the blanks and hence less tedious or
 * error prone.
 */

template <int w_GroupCode>
class GroupTypeData
{
    public:

    enum {
        e_From = GroupTypeData<w_GroupCode-1>::e_From,
        e_To = GroupTypeData<w_GroupCode-1>::e_To
    };

    typedef
        typename at::TypeSelect<
            (e_To < w_GroupCode),
            GroupTraits_Undefined,
            typename GroupTypeData<w_GroupCode-1>::t_Traits
        >::t_Type
        t_Traits;

    typedef
        typename GroupSemanticData<w_GroupCode>::t_SemanticTraits
        t_SemanticTraits;
};


// specializations based on dxf documentation for "AutoCAD 2007 - March 2006"
template <> class GroupTypeData< 0 > : public GroupRangeDefine< 0, 9, std::string > {}; // String 
template <> class GroupTypeData< 10 > : public GroupRangeDefine< 10, 39, double > {}; // Double precision 3D point value
template <> class GroupTypeData< 40 > : public GroupRangeDefine< 40, 59, double > {}; // Double-precision floating-point value
template <> class GroupTypeData< 60 > : public GroupRangeDefine< 60, 79, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 90 > : public GroupRangeDefine< 90, 99, at::Int32 > {}; // 32-bit integer value
template <> class GroupTypeData< 100 > : public GroupRangeDefine< 100, 100, std::string > {}; // String (255-character maximum; less for Unicode strings)
template <> class GroupTypeData< 102 > : public GroupRangeDefine< 102, 102, std::string > {}; // String (255-character maximum; less for Unicode strings)
template <> class GroupTypeData< 105 > : public GroupRangeDefine< 105, 105, std::string > {}; // String representing hexadecimal (hex) handle value
template <> class GroupTypeData< 110 > : public GroupRangeDefine< 110, 119, double > {}; // Double precision floating-point value
template <> class GroupTypeData< 120 > : public GroupRangeDefine< 120, 129, double > {}; // Double precision floating-point value
template <> class GroupTypeData< 130 > : public GroupRangeDefine< 130, 139, double > {}; // Double precision floating-point value
template <> class GroupTypeData< 140 > : public GroupRangeDefine< 140, 149, double > {}; // Double precision scalar floating-point value
template <> class GroupTypeData< 170 > : public GroupRangeDefine< 170, 179, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 210 > : public GroupRangeDefine< 210, 239, double > {}; // Double-precision floating-point value
template <> class GroupTypeData< 270 > : public GroupRangeDefine< 270, 279, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 280 > : public GroupRangeDefine< 280, 289, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 290 > : public GroupRangeDefine< 290, 299, at::Int8, bool > {}; // Boolean flag value
template <> class GroupTypeData< 300 > : public GroupRangeDefine< 300, 309, std::string > {}; // Arbitrary text string
template <> class GroupTypeData< 310 > : public GroupRangeDefine< 310, 319, BinaryXdata, std::string > {}; // String representing hex value of binary chunk
template <> class GroupTypeData< 320 > : public GroupRangeDefine< 320, 329, BinaryXdata, std::string > {}; // String representing hex handle value
template <> class GroupTypeData< 330 > : public GroupRangeDefine< 330, 369, std::string > {}; // String representing hex object IDs
template <> class GroupTypeData< 370 > : public GroupRangeDefine< 370, 379, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 380 > : public GroupRangeDefine< 380, 389, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 390 > : public GroupRangeDefine< 390, 399, std::string > {}; // String representing hex handle value
template <> class GroupTypeData< 400 > : public GroupRangeDefine< 400, 409, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 410 > : public GroupRangeDefine< 410, 419, std::string > {}; // String
template <> class GroupTypeData< 420 > : public GroupRangeDefine< 420, 429, at::Int32 > {}; // 32-bit integer value
template <> class GroupTypeData< 430 > : public GroupRangeDefine< 430, 439, std::string > {}; // String
template <> class GroupTypeData< 440 > : public GroupRangeDefine< 440, 449, at::Int32 > {}; // 32-bit integer value
template <> class GroupTypeData< 450 > : public GroupRangeDefine< 450, 459, at::Int32 > {}; // Long
template <> class GroupTypeData< 460 > : public GroupRangeDefine< 460, 469, double > {}; // Double-precision floating-point value
template <> class GroupTypeData< 470 > : public GroupRangeDefine< 470, 479, std::string > {}; // String
template <> class GroupTypeData< 999 > : public GroupRangeDefine< 999, 999, std::string > {}; // Comment (string)
template <> class GroupTypeData< 1000 > : public GroupRangeDefine< 1000, 1009, std::string > {}; // String (same limits as indicated with 0-9 code range)
template <> class GroupTypeData< 1010 > : public GroupRangeDefine< 1010, 1059, double > {}; // Double-precision floating-point value
template <> class GroupTypeData< 1060 > : public GroupRangeDefine< 1060, 1070, at::Int16 > {}; // 16-bit integer value
template <> class GroupTypeData< 1071 > : public GroupRangeDefine< 1071, 1071, at::Int32 > {}; // 32-bit integer value
template <> class GroupTypeData< -5 > : public GroupRangeDefine< -5, -5, std::string > {}; // APP: persistent reactor chain
template <> class GroupTypeData< -4 > : public GroupRangeDefine< -4, -4, std::string > {}; // APP: conditional operator (used only with ssget)
template <> class GroupTypeData< -3 > : public GroupRangeDefine< -3, -3, BinaryXdata, std::string > {}; // APP: extended data (XDATA) sentinel (fixed)
template <> class GroupTypeData< -2 > : public GroupRangeDefine< -2, -2, std::string > {}; // APP: entity name reference (fixed)


// This must accurately reflect the use
enum { MinGroupCode = -5, MaxGroupCode = 1071 };


} // end namespace

#endif // x_dxf_group_codes_h_x



