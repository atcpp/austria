/**
 *  dxf_converters.h
 *
 */


#ifndef x_dxf_converters_h_x
#define x_dxf_converters_h_x 1

#include "at_buffer.h"
#include "at_any.h"
#include "at_lifetime.h"
#include "at_lifetime_mt.h"
#include "at_status_report.h"

#include "dxf_exports.h"
#include "dxf_group_codes.h"

#include <string>
#include <list>

namespace dxf
{

//

typedef at::Any< at::PtrTarget_MT > ValueType;

// ======== WriteFile ============================================
/**
 * This contains the interface to "write" a file.
 *
 */

class WriteFile
  : public at::PtrTarget_MT
{
    public:


    // ======== Write =================================================
    /**
     *	Write data
     *
     *
     * @param i_mem     The pointer to memory to write
     * @param i_len     The length to write
     * @return true if all is well
     */

    virtual bool Write(
        const char              * i_mem,
        at::SizeMem               i_len,
        at::StatusReport        * i_report
    ) = 0;


    // ======== Flush =================================================
    /**
     * Flush bits to disk.
     *
     * @return True if all is well
     */

    virtual bool Flush( at::StatusReport * i_report ) = 0;

    

};

// ======== ConversionReport ==========================================
/**
 * ConversionReport contains status information about the conversion
 * process.
 */

class DXF_EXPORT ConversionReport
{
    public:

    /**
     * ConversionReport
     *
     */
    ConversionReport(
        at::StatusReport                    * i_report
    )
      : m_report( i_report ),
        m_linecount(),
        m_bytecount()
    {
    }


    // ======== UpdateBytesCount ===========================================
    /**
     * This should be called by the converter whenever bytes are consumed.
     *
     * @param i_bytes The number of bytes consumed.
     * @return nothing
     */

    virtual void UpdateBytesCount(
        at::SizeMem         i_bytes
    )
    {
        m_bytecount += i_bytes;
    }


    // ======== UpdateLinesAndByteCount ===============================
    /**
     * This should be called by the converter whenever an ascii file
     * is parsed.
     *
     *
     * @param i_bytes The number of bytes consumed.
     * @param i_lines The number of lines consumed
     * @return nothing
     */

    virtual void UpdateLinesAndByteCount(
        at::SizeMem         i_bytes,
        at::SizeMem         i_lines
    ) {
        m_linecount += i_lines;
        UpdateBytesCount( i_bytes );
    }


    // ======== ReportFailure =========================================
    /**
     *	
     *
     * @param i_err_str String reporting the error
     * @return nothing
     */

    virtual void	ReportFailure( const std::string & i_err_str )
    {
        if ( m_report )
        {
            m_report->ReportStatus( at::StatusReport::ErrorStatus, 0, i_err_str );
        }
    }

    /**
     * m_report contains the basic reporter
     */
    at::StatusReport                        * m_report;

    /**
     * m_linecount is updated whenever a line is parsed.
     */
    at::SizeMem                               m_linecount;

    /**
     * m_bytecount is the number of bytes currently parsed.
     */
    at::SizeMem                               m_bytecount;
};



// ======== Converter =================================================
/**
 * This converts data from a buffer into the data type specified by the
 * converter.
 *
 *
 */

class DXF_EXPORT Converter
{
    public:

    enum ConversionResult
    {
        /**
         * NeedMoreBytes is returned when more data is
         * needed.
         */
        NeedMoreBytes,

        /**
         * Failed indicated that the conversion has failed.
         */

        Failed,
        /**
         * Success indicates that the conversion succeeded
         */
        Success
    };


    // ======== Convert ===============================================
    /**
     * Convert performs the conversion.
     *
     * @param o_value A pointer to the value being created.
     * @param i_buffer The buffer that is being used to supply the data.
     * @param io_offset The offset into the buffer.  Must be updated once
     *                  the buffer is read.
     * @param io_conv_report Manages the reporting of location and errors.
     * @return ConversionResult indicating the result of the conversion.
     */

    virtual ConversionResult Convert(
        at::Ptr< ValueType * >              & o_value,
        const at::Buffer::t_ConstRegion     & o_buffer,
        at::SizeMem                         & io_offset,
        ConversionReport                    & io_conv_report
    ) const = 0;


    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< WriteFile * >          i_wfile,
        at::PtrView< ValueType * >          i_value,        
        at::StatusReport                  * i_report
    ) const = 0;
    

};


// ======== GroupCodeConverter ========================================
/**
 * Basic interface to retrieve a group code
 *
 */
class GroupCode;

class DXF_EXPORT GroupCodeConverter
{
    public:

    // ======== Convert ===============================================
    /**
     * Convert performs the conversion.
     *
     * @param o_value A place to put the converted group code
     * @param i_buffer The buffer that is being used to supply the data.
     * @param io_offset The offset into the buffer.  Must be updated once
     *                  the buffer is read.
     * @param io_conv_report Manages the reporting of location and errors.
     * @return ConversionResult indicating the result of the conversion.
     */

    virtual Converter::ConversionResult Convert(
        const GroupCode                   * & o_value ,
        const at::Buffer::t_ConstRegion     & o_buffer,
        at::SizeMem                         & io_offset,
        ConversionReport                    & io_conv_report
    ) const = 0;

    // ======== Write =================================================
    /**
     * Write data to file.
     *
     * @param i_wfile The file to write to
     * @param i_value The group code to write
     * @param i_report Status report to tell about errors
     * @return nothing
     */

    virtual bool Write(
        at::PtrView< WriteFile * >          i_wfile,
        const GroupCode                   & i_value,
        at::StatusReport                  * i_report
    ) const = 0;
    
};


// ======== GroupCode =================================================
/**
 * This describes a "group code"
 *
 */

class DXF_EXPORT GroupCode
{
    friend class GroupCode_Basic;

    // ======== GroupCode =============================================
    /**
     * Constructs a basic group code.
     * (The constructor is private).
     *
     * @param i_group_code The group code number
     * @param i_semantic The semantic of this code
     */

    GroupCode( int i_group_code, GroupCodeSemantics i_semantic );

    public:

    const int                   m_group_code;
    const GroupCodeSemantics    m_semantic;

    // ======== ConverterType =========================================
    /**
     * ConverterType defines the type of converter required.
     *
     */

    enum ConverterType
    {
        /**
         * DxfAscii converter
         */
        DxfAscii,

        /**
         * DxfBinary type converter
         */
        DxfBinary,
        
        /**
         * DxfBinary type converter with 16 bit group codes
         */
        DxfBinary16
    };

    // ======== GetConverter ==========================================
    /**
     * Retrieve a converter for this GroupCode.
     *
     * @param i_converter_type  The converter type to get
     * @return A pointer to the converter.
     */

    virtual const Converter * GetConverter(
        ConverterType       i_converter
    ) const = 0;

    public:

    bool operator==( const GroupCode & i_rhs ) const
    {
        return this == & i_rhs;
    }

    bool operator!=( const GroupCode & i_rhs ) const
    {
        return this != & i_rhs;
    }

    bool operator<( const GroupCode & i_rhs ) const
    {
        return this < & i_rhs;
    }

    // ======== GetGroupCode ==========================================
    /**
     * GetGroupCode pulls a groupcode object.
     *
     * @param i_groupcode The groupcode item to get
     * @return nothing
     */

    static const GroupCode & GetGroupCode(
        int         i_groupcode
    );


    // ======== GetGroupCodeConverter =================================
    /**
     * Get the group code converter by type.
     *
     * @param i_conv_type
     * @return A group code converter.
     */

    static const GroupCodeConverter * GetGroupCodeConverter(
        ConverterType    i_conv_type
    );
    

    /**
     * Special group that pertains only to
     * dxflib.
     */
    static const GroupCode      & s_record;

    /**
     * dxf group 0 - should be the same as GetGroupCode(0)
     */
    static const GroupCode      & s_entity;

    /**
     * dxf group 2
     */
    static const GroupCode      & s_name;

    /**
     * dxf group 5
     */
    static const GroupCode      & s_handle;

    /**
     * dxf group 999
     */
    static const GroupCode      & s_comment;
    
    /**
     * special group code that indicates failure - returned by
     * GetConverter where the requested code makes no sense.
     */
    static const GroupCode      & s_error_code;

    
};


// ======== GroupCodeRef ==============================================
/**
 * 
 *
 */
template <int w_code>
class DXF_EXPORT GroupCodeRef
{
    public:


    static const GroupCode             & s_code;
};

template <int w_code>
const GroupCode & GroupCodeRef<w_code>::s_code = GroupCode::GetGroupCode(w_code);


// ======== Item ======================================================
/**
 * This stores a GroupCode ValueType pair.
 *
 */

class DXF_EXPORT Item
{
    public:

    /**
     * Item constructor
     *
     */
    Item(
        const GroupCode     & i_code
    )
      : m_code( & i_code )
    {
    }

    // ======== GetCode ===============================================
    /**
     * Return the code as a const reference.
     */

    const GroupCode & GetCode() const
    {
        return * m_code;
    }
    
    
    const GroupCode               * m_code;

    /**
     * m_value is the value of this item.
     */
    at::Ptr< ValueType * >          m_value;

};


} // end namespace


#include <ostream>

namespace std {

template<
    typename        w_char_type,
    class           w_traits
>
basic_ostream<w_char_type, w_traits>& operator << (
    basic_ostream<w_char_type, w_traits>        & o_ostream,
    const dxf::Item                             & i_item
) {
    const std::string & l_name = i_item.m_value->Refer();
    return o_ostream << i_item.GetCode().m_group_code << ':' << l_name;
} // end basic_ostream<w_char_type, w_traits>& operator <<


} // namespace std


#endif // x_dxf_converters_h_x


