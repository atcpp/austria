

#include "dxf_file.h"

#include "at_unit_test.h"
#include "at_status_report_basic.h"

AT_TestArea( DXF_ReaderWriter, "Basic DXF Read/Write Test" );

#include <iostream>
#include <fstream>

AT_DefineTest( Ascii, DXF_ReaderWriter, "Ascii DXF Read/Write Test" )
{
	void Run()
	{
        std::ifstream       l_file( "test.dxf", std::ios::binary );
        
        dxf::ReadFile       l_read_file( l_file );

        l_read_file.m_bufsz = 2; // pedantically small

        at::StatusReport_Basic      l_status;

        at::Ptr< dxf::Model * >     l_model = l_read_file.Read( & l_status );

        std::ofstream       l_ostream( "testwrite.dxf", std::ios::binary );

        at::Ptr< dxf::WriteFile * > l_wfile = new dxf::StdOstreamWriteFile( l_ostream );

        at::StatusReport_Basic  l_report;

        if ( ! l_model->Write( l_wfile, dxf::GroupCode::DxfAscii, & l_report ) )
        {
            AT_TCAssert( false, l_report.m_description );
        }

#if 0
//        dxf::ModelIterator l_iter = l_model->Search( Selector( dxf::Group(10), dxf::Equal, double( 0 ) ) );

//        at::WriteFile   l_write_file( std::cout );

        for ( ; ! l_iter.end(); l_iter.next() )
        {

            l_write_file << * l_iter;

        }
#endif

	}
};

AT_RegisterTest( Ascii, DXF_ReaderWriter );



AT_DefineTest( Binary, DXF_ReaderWriter, "Binary DXF Read/Write Test" )
{
	void Run()
	{
        std::ifstream       l_file( "testb.dxf", std::ios::binary );
        
        dxf::ReadFile       l_read_file( l_file );

        l_read_file.m_bufsz = 2; // pedantically small

        at::StatusReport_Basic      l_status;

        at::Ptr< dxf::Model * >     l_model = l_read_file.Read( & l_status );

        AT_TCAssert( l_model, l_status.m_description );

        std::ofstream       l_ostream( "testbwrite.dxf", std::ios::binary );

        at::Ptr< dxf::WriteFile * > l_wfile = new dxf::StdOstreamWriteFile( l_ostream );

        at::StatusReport_Basic  l_report;

        if ( ! l_model->Write( l_wfile, dxf::GroupCode::DxfBinary16, & l_report ) )
        {
            AT_TCAssert( false, l_report.m_description );
        }

#if 0
//        dxf::ModelIterator l_iter = l_model->Search( Selector( dxf::Group(10), dxf::Equal, double( 0 ) ) );

//        at::WriteFile   l_write_file( std::cout );

        for ( ; ! l_iter.end(); l_iter.next() )
        {

            l_write_file << * l_iter;

        }
#endif

	}
};

AT_RegisterTest( Binary, DXF_ReaderWriter );


AT_DefineTest( BinaryToAscii, DXF_ReaderWriter, "BinaryToAscii DXF Read/Write Test" )
{
	void Run()
	{
        std::ifstream       l_file( "testb.dxf", std::ios::binary );
        
        dxf::ReadFile       l_read_file( l_file );

        l_read_file.m_bufsz = 2; // pedantically small

        at::StatusReport_Basic      l_status;

        at::Ptr< dxf::Model * >     l_model = l_read_file.Read( & l_status );

        AT_TCAssert( l_model, l_status.m_description );

        std::ofstream       l_ostream( "testbtoawrite.dxf", std::ios::binary );

        at::Ptr< dxf::WriteFile * > l_wfile = new dxf::StdOstreamWriteFile( l_ostream );

        at::StatusReport_Basic  l_report;

        if ( ! l_model->Write( l_wfile, dxf::GroupCode::DxfAscii, & l_report ) )
        {
            AT_TCAssert( false, l_report.m_description );
        }

#if 0
//        dxf::ModelIterator l_iter = l_model->Search( Selector( dxf::Group(10), dxf::Equal, double( 0 ) ) );

//        at::WriteFile   l_write_file( std::cout );

        for ( ; ! l_iter.end(); l_iter.next() )
        {

            l_write_file << * l_iter;

        }
#endif

	}
};

AT_RegisterTest( BinaryToAscii, DXF_ReaderWriter );



