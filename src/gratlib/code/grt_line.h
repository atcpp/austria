//
// The Graphics Template Library (grt) is copyright (c) Gianni Mariani 2006
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * grt_line.h
 *
 */

#ifndef x_grt_line_h_x
#define x_grt_line_h_x 1

#include "grt_base.h"

namespace grt
{
//


// ======== line ======================================================
/**
 * A line is represented as 2 vectors.
 *
 *
 */

template <
    typename w_Type,
    unsigned w_Dim,
    typename w_Base = NullBase
>
class line
{
    public:

    
};



}; // namespace

#endif // x_grt_line_h_x



