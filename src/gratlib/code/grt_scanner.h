//
// The Graphics Template Library (grt) is copyright (c) Gianni Mariani 2006
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * grt_scanner.h
 *
 */

#ifndef x_grt_scanner_h_x
#define x_grt_scanner_h_x 1

#include "at_types.h"
#include "at_pointers.h"
#include "at_assert.h"
#include "at_lifetime_mt.h"
#include <map>
#include <vector>

#include "grt_exports.h"

namespace grt
{
//



// ======== IteratorTranverser =======================================
/**
 * IteratorTranverser is a template class that iterates through
 * a pointer or iterator.  The pointers passed to it must be valid
 * for the life-time of this object.
 */

template <typename Itr>
struct IteratorTranverser
{

    Itr                       m_from;
    const Itr                 m_end;

    typedef typename at::NonConst< typename at::PointerTo<Itr>::value_type >::t_Type value_type;

    IteratorTranverser( const Itr & i_from, const Itr & i_end )
      : m_from( i_from ),
        m_end( i_end )
    {
    }


    bool GetToken( value_type & l_val )
    {
        if ( m_from != m_end )
        {
            l_val = * ( m_from ++ );
            return true;
        }

        return false;
    }

    bool HasInput( bool i_wait )
    {
        return m_from != m_end;
    }

};


// ======== CombiningTraverser ========================================
/**
 *
 *
 */

template <typename TraverserTypeFirst, typename TraverserTypeSecond>
struct CombiningTraverser
{

    TraverserTypeFirst              & m_first;
    TraverserTypeSecond             & m_second;
    bool                              m_use_second;
        
    CombiningTraverser(
        TraverserTypeFirst          & io_first,
        TraverserTypeSecond         & io_second
    )
      : m_first( io_first ),
        m_second( io_second ),
        m_use_second( false )
    {
    }

    typedef typename at::NonConst< typename at::PointerTo<TraverserTypeFirst>::value_type >::t_Type value_type;

    enum {
        m_issame = 
            at::IsSame<
                typename TraverserTypeFirst::value_type,
                typename TraverserTypeSecond::value_type
            >::m_value
    };
    
    AT_StaticAssert(
        m_issame,
        Traversers_Must_Be_Same_Element_Type
    );

    bool GetToken( value_type & l_val )
    {
        if ( ! m_use_second )
        {
            if ( m_first.GetToken( l_val ) )
            {
                return true;
            }
            m_use_second = true;
        }

        return m_second.GetToken( l_val );
    }

    bool HasInput( bool i_wait )
    {
        if ( ! m_use_second )
        {
            if ( m_first.HasInput( i_wait ) )
            {
                return true;
            }
            m_use_second = true;
        }

        return m_second.HasInput( i_wait );
    }

};



// ======== DFAMachineObjectToTokenConvert ============================
/**
 * DFAMachineObjectToTokenConvert can be overloaded to manage
 * the conversion of an "object type" to a "token type" which
 * usually means just referencing a member of the object.
 *
 */

template <typename w_Type>
class DFAMachineObjectToTokenConvert
{
    public:

    w_Type                  & m_object;

    /**
     * DFAMachineObjectToTokenConvert
     *
     */
    DFAMachineObjectToTokenConvert(
        w_Type                  & i_object
    )
      : m_object( i_object )
    {
    }

    template <typename w_Otype>
    operator w_Otype()
    {
        return m_object;
    }

};


// ======== DFAMachine ================================================
/**
 * Simple DFA state machine
 *
 */

template <typename w_InTokType, typename w_ResultType>
class DFAMachine
  : public at::PtrTarget_MT
{
    public:

    // ======== DFAState ==================================================
    /**
     * This is a simplistic DFA state
     *
     */

    class DFAState
      : public at::PtrTarget_MT
    {
        public:

        // to avoid circular references, there are only weak pointers
        // between states.
        typedef std::map<w_InTokType, at::PtrView<DFAState *> >     t_map_type;
        typedef typename t_map_type::iterator                       t_iterator;
        typedef typename t_map_type::const_iterator                 t_const_iterator;
        
        t_map_type                                          m_transitions;

        w_ResultType                                        m_terminal;

        DFAState()
          : m_terminal()
        {
            AT_Assert( w_ResultType() == m_terminal );
        }

        bool FindOrInsertTransition(
            w_InTokType                             i_token,
            std::vector< at::Ptr<DFAState *> > &    o_state_collection,
            at::PtrView<DFAState *>               & o_end_state
        ) {
            std::pair<t_iterator, bool> l_insert_result =
                m_transitions.insert( typename t_map_type::value_type( i_token, 0 ) );

            if ( ! l_insert_result.second )
            {
                if ( o_end_state )
                {
                    o_end_state = l_insert_result.first->second;
                    return false;
                }
                o_end_state = l_insert_result.first->second;
                return true;
            }

            if ( o_end_state )
            {
                l_insert_result.first->second = o_end_state;
                return true;
            }
            // create a state by first adding it to the vector of states.
            // the vector maintains ownership of states.
            o_state_collection.push_back( new DFAState );
            o_end_state = l_insert_result.first->second = o_state_collection.back();
            return true;
        }


        at::PtrView<DFAState *> FindTransition( w_InTokType i_token ) const
        {
            t_const_iterator l_insert_result =
                m_transitions.find( i_token );

            if ( l_insert_result != m_transitions.end() )
            {
                return l_insert_result->second;
            }

            return 0;
        }

    }; // DFAState



    // ======== Scanner ===============================================
    /**
     * This is an instace of a scanner that scans against the state
     * machine.
     */

    template <typename w_InObjectType, typename w_ObjectContainer>
    class Scanner
    {
        public:
        
        at::Ptr<const DFAMachine *>         m_machine;
        at::PtrView<DFAState *>             m_current_state;
        at::PtrView<DFAState *>             m_last_accept_state;
        w_ObjectContainer                   m_stack;
        w_ObjectContainer                   m_acceptstack;

        /**
         * Scanner
         *
         */
        Scanner(
            at::PtrDelegate< const DFAMachine * >   i_machine
        )
          : m_machine( i_machine ),
            m_current_state( m_machine->m_all_states.front() ),
            m_stack(),
            m_acceptstack()
        {
        }

        /**
         * NextToken will traverse the state machine with the next
         * character and return the terminal KeyDef if one exists.
         * If i_token does not make a valid transition, o_valid
         * is set to false.
         */
        w_ResultType NextToken( w_InObjectType i_object, bool & o_valid )
        {

            w_InTokType l_token = DFAMachineObjectToTokenConvert<w_InObjectType>( i_object );
                
            m_stack.push_back( i_object );
            
            at::PtrView<DFAState *> l_next_state = m_current_state->FindTransition( l_token );

            if ( l_next_state )
            {
                m_current_state = l_next_state;

                o_valid = true;
                // If there is an accepting state then we
                // can roll back the push-back buffer.
                if ( l_next_state->m_terminal )
                {
                    m_last_accept_state = l_next_state;

                    return l_next_state->m_terminal;
                }

                return w_ResultType();
            }

            m_current_state = m_machine->m_all_states.front();
            o_valid = false;
            return w_ResultType();
        }

        template <typename Traverser>
        w_ResultType ScanStream( Traverser & io_traverser, w_ObjectContainer & o_result )
        {
            w_InObjectType              l_object;

            while ( io_traverser.GetToken( l_object ) )
            {
                bool        i_valid;

                w_ResultType l_result = NextToken( l_object, i_valid );

                if ( w_ResultType() != l_result )
                {
                    // we have a new accept state
                    // push the contents of the stack onto the accept stack

                    m_acceptstack.insert( m_acceptstack.end(), m_stack.begin(), m_stack.end() );
                    m_stack.clear();
                    
                }

                at::PtrView<DFAState *> l_last_accept_state = m_last_accept_state;

                // If there are no more transitions or the last
                if ( ( ! i_valid ) || ( m_current_state->m_transitions.size() == 0 ) )
                {
                    o_result.swap( m_acceptstack );
                    m_acceptstack.clear();

                    if ( l_last_accept_state )
                    {
                        m_last_accept_state = 0;
                        m_current_state = m_machine->m_all_states.front();
                        return l_last_accept_state->m_terminal;
                    }
                    return w_ResultType();
                }

                // There are transitions ...
                AT_Assert( m_current_state->m_transitions.size() != 0 );

                // If there are transitions (true here) and this is an interactive
                // scan (waiting for user input) then wait a little longer, if there
                // are no accept states - wait forever (which means calling GetToken).

                if ( l_last_accept_state )
                {
                    if ( ! io_traverser.HasInput( true ) )
                    {
                        // there is no longer any pending input. We're done.
                        o_result.swap( m_acceptstack );
                        m_acceptstack.clear();

                        m_last_accept_state = 0;
                        m_current_state = m_machine->m_all_states.front();

                        return l_last_accept_state->m_terminal;
                    }
                }
            }

            return w_ResultType();
        }


        template <typename TraverserType>
        w_ResultType ScanBuffered( TraverserType & io_traverser, w_ObjectContainer & o_result )
        {

            if ( m_stack.size() != 0 )
            {
                w_ObjectContainer           l_objcont;
                l_objcont.swap( m_stack );

                IteratorTranverser< typename w_ObjectContainer::iterator >    l_tvsr(
                    l_objcont.begin(),
                    l_objcont.end()
                );

                CombiningTraverser<
                    IteratorTranverser< typename w_ObjectContainer::iterator >,
                    TraverserType
                >       l_combined( l_tvsr, io_traverser );

                w_ResultType l_kd = ScanStream( l_combined, o_result );

                m_stack.insert( m_stack.end(), l_tvsr.m_from, l_tvsr.m_end );

                return l_kd;
            }
            else
            {
                w_ResultType l_kd = ScanStream( io_traverser, & o_result );

                return l_kd;
            }
       }

        template <typename TraverserType, typename w_OutputProcessor>
        void ScanAll(
            TraverserType       & io_traverser,
            w_OutputProcessor   & o_outproc,
            bool                  i_eof,
            bool                  i_wait = true
        )
        {

            w_ObjectContainer       l_result;

            while ( io_traverser.HasInput( i_wait ) || ( i_eof && ( m_stack.size() != 0 ) ) )
            {
                w_ResultType l_kd;

                if ( m_stack.size() != 0 )
                {
                    w_ObjectContainer           l_objcont;
                    
                    l_objcont.swap( m_stack );
    
                    IteratorTranverser< typename w_ObjectContainer::iterator >    l_tvsr(
                        l_objcont.begin(),
                        l_objcont.end()
                    );
    
                    CombiningTraverser<
                        IteratorTranverser< typename w_ObjectContainer::iterator >,
                        TraverserType
                    >       l_combined( l_tvsr, io_traverser );
    
                    l_kd = ScanStream( l_combined, l_result );

                    // place the local items back n thr stack.
                    m_stack.insert( m_stack.end(), l_tvsr.m_from, l_tvsr.m_end );
    
                }
                else
                {
                    l_kd = ScanStream( io_traverser, l_result );
                }

                if ( w_ResultType() == l_kd )
                {
                    // pop one off the stack and give it back

                    if ( ! m_stack.empty() )
                    {
                        o_outproc.SkippedToken( m_stack.front() );
                        m_stack.erase( m_stack.begin() );
                    }
                }
                else
                {
                    // OK - found one - let's push it thru

                    o_outproc.Scanned( l_kd, l_result );

                    l_result.clear();
                }
            }
        }

    };


    // ======== MakeScanner ===========================================
    /**
     * This constructs a new scanner.
     *
     * @return A scanner - constructed in place.
     */

    template <typename w_InObjectType, typename w_ObjectContainer>
    Scanner<w_InObjectType, w_ObjectContainer> MakeScanner() const
    {
        return Scanner<w_InObjectType, w_ObjectContainer>( at::PtrView< const DFAMachine * >( this ) );
    }


    // ======== AddTerminal ===========================================
    /**
     * AddTerminal will add a new terminal node to the state machine
     * that will match the exact sequence given
     *
     * @param i_spec_begin  Iterator for beginning list of tokens.
     * @param i_spec_end    for end of list of tokens
     * @return A default result object if all is well, otherwise 
     *  the colliding terminal token if insertion failed.
     */

    template <typename w_SpecifierIterator>
    w_ResultType AddTerminal(
        const w_SpecifierIterator       & i_spec_begin,
        const w_SpecifierIterator       & i_spec_end,
        w_ResultType                      i_result,
        const at::PtrView<DFAState *>   * i_start_state = 0,
        at::PtrView<DFAState *>         * o_end_state = 0
    ) {

        w_SpecifierIterator         l_begin( i_spec_begin );
        
        at::PtrView<DFAState *>     l_curr_state_default = m_all_states.front();
        
        at::PtrView<DFAState *>     l_curr_state = i_start_state ? * i_start_state : l_curr_state_default;
        

        bool l_has_collision = false;
        
        while ( l_begin != i_spec_end )
        {
            w_SpecifierIterator         l_cur( l_begin ++ );

            at::PtrView<DFAState *>     l_next_state;
            // choose a pointer to where to write the next state
            at::PtrView<DFAState *>   * l_next_ptr_state = ( ( !o_end_state) || ( l_begin != i_spec_end ) ? & l_next_state : o_end_state );
        
            l_has_collision = ! l_curr_state->FindOrInsertTransition(
                * l_cur,
                m_all_states,
                * l_next_ptr_state
            );

            l_curr_state = * l_next_ptr_state;
        }

        if ( l_has_collision )
        {
            return i_result;
        }

        if ( w_ResultType() != l_curr_state->m_terminal )
        {
            // We have a collision !
            return l_curr_state->m_terminal;
        }

        l_curr_state->m_terminal = i_result;

        // we have a collision of the final state transition
        if ( l_has_collision )
        {
            return i_result;
        }

        // test the scanner to make sure that we decode what we expect
        // to decode
        Scanner<w_InTokType, std::vector<w_InTokType> > l_scanner = MakeScanner<w_InTokType, std::vector<w_InTokType> >();

        IteratorTranverser< w_SpecifierIterator >    l_tvsr( i_spec_begin, i_spec_end );

        std::vector<w_InTokType>    l_val;
        
        w_ResultType l_kd2 = l_scanner.ScanStream( l_tvsr, l_val );

        AT_Assert( l_kd2 == i_result );

        return w_ResultType();

    }

    DFAMachine()
      : m_all_states( 1, new DFAState )
    {
        
    }

//    at::Ptr<DFAState *>                             m_initial_state;
    std::vector< at::Ptr<DFAState *> >              m_all_states;

    private:
    // no copying
    DFAMachine( const DFAMachine & );
    DFAMachine operator=( const DFAMachine & );
    
}; // DFAMachine



}; // namespace

#endif // x_grt_scanner_h_x



