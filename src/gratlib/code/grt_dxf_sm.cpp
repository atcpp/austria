//
// grt_dxf_sm.cpp contains the state machine
// definition for dxf->grt scene graph conversion
//



#if 0
20, 10, ErrorType, ErrorAccessor
30, 10, grt::vec2d, Index<2,vec2d>
40, 10, grt::vec3d, Index<3,vec3d>


10 : 10 20 vec2d
10 : 10 20 30 vec3d

// ======== Serializer ================================================
/**
 *	
 *
 *
 * @param i_ctxt Contains the context of this conversion
 * @return nothing
 */

void Serializer(
    at::PtrView< SerializerContext * >      i_ctxt,
    vec3d                                 & io_val,
    int                                     i_offset
) {

    i_ctxt->Exchange( io_val[ 0 ], 10 + i_offset, 0 );
    i_ctxt->Exchange( io_val[ 1 ], 20 + i_offset, 0 );
    i_ctxt->Exchange( io_val[ 2 ], 30 + i_offset, 0 );
}


#endif

