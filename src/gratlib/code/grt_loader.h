//
// The Graphics Template Library (grt) is copyright (c) Gianni Mariani 2006
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * grt_loader.h
 *
 */

#ifndef x_grt_loader_h_x
#define x_grt_loader_h_x 1

#include "grt_entity.h"
#include "dxf_file.h"

namespace grt
{
//



// ======== Loader ====================================================
/**
 * This class handles the loading of a DXF file
 *
 */

class GRT_EXPORT Loader
{
    public:

    /**
     * Loader
     *
     */
    Loader()
    {
    }
    


    // ======== LoadFile ==============================================
    /**
     * Loads a dxf file into memory
     *
     * @param i_stream  The std::istream to read the file from
     * @return True if successful
     */

    bool LoadFile(
        std::istream                & i_file,
        at::StatusReport            * i_status = 0
    ) {

        dxf::ReadFile               l_read_file( i_file );
        
        at::PtrDelegate< dxf::Model * >     l_model = l_read_file.Read( i_status );

        return ParseModel( l_model );

    }


    // ======== ParseModel ============================================
    /**
     * ParseModel takes a dxfmodel and 
     *
     *
     * @param i_X
     * @return nothing
     */

    bool ParseModel(
        at::PtrDelegate< dxf::Model * >     i_model
    ) {

        m_model = i_model;

        if ( !m_model )
        {
            return false;
        }

//        m_model->Traverse( this );

        return false;
    }


    /**
     * The dxf file model
     */
    at::Ptr< dxf::Model * >         m_model;

};



}; // namespace

#endif // x_grt_loader_h_x



