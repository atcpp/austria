//
// The Graphics Template Library (grt) is copyright (c) Gianni Mariani 2006
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * grt_base.h
 *
 */

#ifndef x_grt_base_h_x
#define x_grt_base_h_x 1

#include "grt_exports.h"
#include "at_assert.h"
#include "at_types.h"

namespace grt
{
//


// ======== UnitItem ==================================================
/**
 * 
 *
 */

template <typename w_Type>
struct UnitItem
{
    public:

    static const w_Type    s_value;
};

template <typename w_Type>
const w_Type  UnitItem<w_Type>::s_value( 1 );


// ======== NullBase ==================================================
/**
 * Null - do nothing base class
 *
 */

class GRT_EXPORT NullBase
{
    public:

    // allow the vector to be normalized
    enum { m_allow_normalization = 1 };
    enum { m_allow_extension = 1 };
};


// ======== vec =======================================================
/**
 * Basic vector
 *
 */

template < typename w_Type, unsigned w_Dim, typename w_Base = NullBase>
class GRT_EXPORT vec
  : public w_Base
{
    public:

    typedef w_Base          t_base_type;

    typedef w_Type          value_type;

    /**
     * m_dim in the number of elements in the dimension
     */
    static const int m_dim = w_Dim;

    value_type              m_value[ m_dim ];

    /**
     * vec default constructor
     */
    vec()
      : m_value()
    {
    }

    value_type & operator[]( int i_index )
    {
        return m_value[ i_index ];
    }
    
    const value_type & operator[]( int i_index ) const
    {
        return m_value[ i_index ];
    }

    vec( const w_Type & i_v0 )
    {
        AT_StaticAssert( w_Dim == 1, Check_for_correct_dimension );
        m_value[ 0 ] = i_v0;
    }

    vec( const w_Type & i_v0, const w_Type & i_v1 )
    {
        AT_StaticAssert( w_Dim == 2, Check_for_correct_dimension );
        m_value[ 0 ] = i_v0;
        m_value[ 1 ] = i_v1;
    }

    vec( const w_Type & i_v0, const w_Type & i_v1, const w_Type & i_v2 )
    {
        AT_StaticAssert( w_Dim == 3, Check_for_correct_dimension );
        m_value[ 0 ] = i_v0;
        m_value[ 1 ] = i_v1;
        m_value[ 2 ] = i_v2;
    }

    vec( const w_Type & i_v0, const w_Type & i_v1, const w_Type & i_v2, const w_Type & i_v3 )
    {
        AT_StaticAssert( w_Dim == 4, Check_for_correct_dimension );
        m_value[ 0 ] = i_v0;
        m_value[ 1 ] = i_v1;
        m_value[ 2 ] = i_v2;
        m_value[ 3 ] = i_v3;
    }


    // ======== operator= ==============================================
    /**
     * Assign from another vec
     *
     * @return nothing
     */

    template < typename w_RhsType, typename w_RhsBase>
    vec & operator=(
        const vec< w_RhsType, w_Dim, w_RhsBase >    & i_rhs
    ) {
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
        return * this;
    }

    // ======== operator= ==============================================
    /**
     * Assign from another vec
     *
     * @return nothing
     */

    template < typename w_RhsType, typename w_RhsBase>
    vec & operator=(
        const vec< w_RhsType, w_Dim + 1, w_RhsBase >    & i_rhs
    ) {
        AT_StaticAssert( t_base_type::m_allow_normalization, Check_for_allowable_operation );
        w_RhsType   l_val = i_rhs[ w_Dim ];
        
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ] / l_val;
        }
        return * this;
    }

    // ======== operator= ==============================================
    /**
     * Assign from another vec
     *
     * @return nothing
     */

    template < typename w_RhsType, typename w_RhsBase>
    vec & operator=(
        const vec< w_RhsType, w_Dim - 1, w_RhsBase >    & i_rhs
    ) {
        AT_StaticAssert( t_base_type::m_allow_extension, Check_for_allowable_operation );
        
        for ( unsigned i = 0; i < w_Dim -1; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
        m_value[ w_Dim - 1 ] = UnitItem<w_Type>::s_value;
        return * this;
    }

    // ======== operator= ==============================================
    /**
     * Assign from another vec
     *
     * @return nothing
     */

    template < typename w_RhsType >
    vec & operator=(
        const w_RhsType         ( & i_rhs )[ w_Dim ]
    ) {
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
        return * this;
    }


    // ======== operator= ==============================================
    /**
     * Assign from another vec
     *
     * @return nothing
     */

    template < typename w_RhsType >
    vec & operator=(
        const w_RhsType         ( & i_rhs )[ w_Dim + 1 ]
    ) {
        AT_StaticAssert( t_base_type::m_allow_normalization, Check_for_allowable_operation );
        w_RhsType   l_val = i_rhs[ w_Dim ];
        
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ] / l_val;
        }
        return * this;
    }

    // ======== vec ===================================================
    /**
     * Assign one vec from another vec
     *
     * @param i_rhs - Item being constructed from
     * @return nothing
     */

    template < typename w_RhsType >
    vec & operator=(
        const w_RhsType         ( & i_rhs )[ w_Dim - 1 ]
    ) {
        AT_StaticAssert( t_base_type::m_allow_extension, Check_for_allowable_operation );
        
        for ( unsigned i = 0; i < w_Dim -1; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
        m_value[ w_Dim - 1 ] = UnitItem<w_Type>::s_value;
        return * this;
    }


    // ======== vec ===================================================
    /**
     * Assign one vec from another vec
     *
     * @param i_rhs - Item being constructed from
     * @return nothing
     */

    template < typename w_RhsType, typename w_RhsBase>
    vec(
        const vec< w_RhsType, w_Dim, w_RhsBase >    & i_rhs
    ) {
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
    }

    // ======== vec ===================================================
    /**
     * Assign one vec from another vec
     *
     * @param i_rhs - Item being constructed from
     * @return nothing
     */

    template < typename w_RhsType, typename w_RhsBase>
    explicit vec(
        const vec< w_RhsType, w_Dim + 1, w_RhsBase >    & i_rhs
    ) {
        AT_StaticAssert( t_base_type::m_allow_normalization, Check_for_allowable_operation );
        w_RhsType   l_val = i_rhs[ w_Dim ];
        
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ] / l_val;
        }
    }

    // ======== vec ===================================================
    /**
     * Assign one vec from another vec
     *
     * @param i_rhs - Item being constructed from
     * @return nothing
     */

    template < typename w_RhsType, typename w_RhsBase>
    vec(
        const vec< w_RhsType, w_Dim - 1, w_RhsBase >    & i_rhs
    ) {
        AT_StaticAssert( t_base_type::m_allow_extension, Check_for_allowable_operation );
        
        for ( unsigned i = 0; i < w_Dim -1; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
        m_value[ w_Dim - 1 ] = UnitItem<w_Type>::s_value;
    }

    
    // ======== vec ===================================================
    /**
     * Assign one vec from another vec
     *
     * @param i_rhs - Item being constructed from
     * @return nothing
     */

    template < typename w_RhsType >
    vec(
        const w_RhsType         ( & i_rhs )[ w_Dim ]
    ) {
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
    }

    // ======== vec ===================================================
    /**
     * Assign one vec from another vec
     *
     * @param i_rhs - Item being constructed from
     * @return nothing
     */

    template < typename w_RhsType >
    vec(
        const w_RhsType         ( & i_rhs )[ w_Dim + 1 ]
    ) {
        AT_StaticAssert( t_base_type::m_allow_normalization, Check_for_allowable_operation );
        w_RhsType   l_val = i_rhs[ w_Dim ];
        
        for ( unsigned i = 0; i < w_Dim; ++i )
        {
            m_value[ i ] = i_rhs[ i ] / l_val;
        }
    }

    // ======== vec ===================================================
    /**
     * Assign one vec from another vec
     *
     * @param i_rhs - Item being constructed from
     * @return nothing
     */

    template < typename w_RhsType >
    vec(
        const w_RhsType         ( & i_rhs )[ w_Dim - 1 ]
    ) {
        AT_StaticAssert( t_base_type::m_allow_extension, Check_for_allowable_operation );
        
        for ( unsigned i = 0; i < w_Dim -1; ++i )
        {
            m_value[ i ] = i_rhs[ i ];
        }
        m_value[ w_Dim - 1 ] = UnitItem<w_Type>::s_value;
    }

    
};

// don't define 
template < typename w_Type, typename w_Base>
class GRT_EXPORT vec<w_Type,1,w_Base>
{
private:
	vec( const vec & );
	vec & operator=( const vec & );
};

// ======== operator^ ==================================================
/**
 * Cross product - works with 3 and 4 size vec objects.
 *
 *
 * @return nothing
 */

template <
    typename    w_LhsType,
    unsigned    w_LhsDim,
    typename    w_RhsType,
    unsigned    w_RhsDim,
    typename    w_LhsBase,
    typename    w_RhsBase
>
vec< typename at::Promote<w_LhsType,w_RhsType>::type, (w_LhsDim>w_RhsDim ? w_LhsDim : w_RhsDim), w_LhsBase>
operator^(
    const vec< w_LhsType, w_LhsDim, w_LhsBase >        & i_lhs,
    const vec< w_RhsType, w_RhsDim, w_RhsBase >        & i_rhs
) {
    typedef typename at::Promote<w_LhsType,w_RhsType>::type                             t_scalar_type;
    typedef vec< t_scalar_type, (w_LhsDim>w_RhsDim ? w_LhsDim : w_RhsDim), w_LhsBase>   t_result_type;

    AT_StaticAssert( w_LhsDim == 3 || w_LhsDim == 4, Dimension_check );
    AT_StaticAssert( w_RhsDim == 3 || w_RhsDim == 4, Dimension_check );

    const t_scalar_type l_divisor(
        ( ( w_LhsDim == 3 ) && ( w_LhsDim == 3 ) ) ? t_scalar_type(1) :
        ( ( w_LhsDim == 4 ) && ( w_LhsDim == 3 ) ) ? t_scalar_type(1)/i_lhs[3] :
        ( ( w_LhsDim == 3 ) && ( w_LhsDim == 4 ) ) ? t_scalar_type(1)/i_rhs[3] :
        t_scalar_type(1)/(i_lhs[3] * i_rhs[3])
    );

    t_scalar_type   l_vals[3] = {
        i_lhs[1] * i_rhs[2] - i_lhs[2] * i_rhs[1],
        - i_lhs[0] * i_rhs[2] + i_lhs[2] * i_rhs[0],
        i_lhs[0] * i_rhs[1] - i_lhs[1] * i_rhs[0]
    };

    if ( ( w_LhsDim == 3 ) && ( w_LhsDim == 3 ) )
    {
        return t_result_type( l_vals );
    }

    l_vals[ 0 ] *= l_divisor;
    l_vals[ 1 ] *= l_divisor;
    l_vals[ 2 ] *= l_divisor;

    return t_result_type( l_vals );
}

// ======== operator* ==================================================
/**
 * Dot product
 *
 *
 * @return nothing
 */

template <
    typename    w_LhsType,
    unsigned    w_Dim,
    typename    w_RhsType,
    typename    w_LhsBase,
    typename    w_RhsBase
>
typename at::Promote<w_LhsType,w_RhsType>::type
operator*(
    const vec< w_LhsType, w_Dim, w_LhsBase >        & i_lhs,
    const vec< w_RhsType, w_Dim, w_RhsBase >        & i_rhs
) {
    typedef typename at::Promote<w_LhsType,w_RhsType>::type t_result_type;

    t_result_type   l_result( i_lhs[0] * i_rhs[0] );

    for ( unsigned i = 0; i < w_Dim; ++i )
    {
        l_result += i_lhs[1] * i_rhs[1];
    }

    return l_result;
}

#if 0
// ======== operator^ ==================================================
/**
 * Cross product
 *
 *
 * @return nothing
 */

template <
    typename    w_vecType,
    typename    w_vecType,
    unsigned    w_ColDim,
    unsigned    w_RowDim,
    typename    w_ColBase = NullBase,
    typename    w_RowBase = NullBase
>
vec< w_Type, w_ColDim, w_ColBase > operator^(
    const vec<
        vec< w_Type, w_ColDim, w_ColBase >,
        w_RowDim,
        w_RowBase
    >                                               & i_lhs,
    const vec< w_Type, w_ColDim, w_ColBase >        & i_rhs
) {
}
#endif


// ======== mat =======================================================
/**
 * Basic 2D matrix
 *
 */

template <
    typename    w_Type,
    unsigned    w_ColDim,
    unsigned    w_RowDim,
    typename    w_ColBase = NullBase,
    typename    w_RowBase = NullBase
>
class mat_traits
{
    public:
    
    typedef vec<
        vec< w_Type, w_ColDim, w_ColBase >,
        w_RowDim,
        w_RowBase
    >   mat;

};


typedef vec< double, 4 >                    vec4d;
typedef mat_traits< double, 4, 4 >::mat     mat4d;
typedef vec< float, 4 >                     vec4f;
typedef mat_traits< float, 4, 4 >::mat      mat4f;
typedef vec< double, 3 >                    vec3d;
typedef mat_traits< double, 3, 3 >::mat     mat3d;
typedef vec< float, 3 >                     vec3f;
typedef mat_traits< float, 3, 3 >::mat      mat3f;


}; // namespace

#endif // x_grt_base_h_x


