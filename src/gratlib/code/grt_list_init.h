//
// The Graphics Template Library (grt) is copyright (c) Gianni Mariani 2006
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * grt_list_init.h
 *
 */

#ifndef x_grt_list_init_h_x
#define x_grt_list_init_h_x 1

#include <vector>

#include "grt_exports.h"

namespace grt
{
//



// ======== w_ItemReference ========================================
/**
 * This simply stores a reference to a group code.
 *
 */

template <typename w_Item>
class ItemRef
{
    
    public:

    typedef w_Item          t_ItemType;

    ItemRef(
        const w_Item       & i_ref
    )
      : m_ref( & i_ref )
    {
    }

    bool operator<( const ItemRef & i_rhs ) const
    {
        return this->m_ref < i_rhs.m_ref;
    }

    ItemRef()
      : m_ref()
    {
    }
    const w_Item           * m_ref;

};


// ======== ItemContainerSelector =====================================
/**
 * This must be specialized to defined the typedef t_Container
 * to somthing that contains ItemRef<w_Item> if somthing other
 * than vector is required.
 */

template <typename w_Item>
class ItemContainerSelector
{
    public:

    typedef std::vector< ItemRef<w_Item> >  t_Container;
};


// ======== ItemSequence =============================================
/**
 * Stores a sequence of groupcodes...
 *
 */

template <typename w_Item>
class ItemSequence
{
    public:

    typedef typename ItemContainerSelector<w_Item>::t_Container t_Container;

    t_Container     m_sequence;

    public:
    
    ItemSequence() {}
    
    ItemSequence(
            const w_Item       & i_groupcode
    )
      : m_sequence( 1, i_groupcode )
    {
    }

    ItemSequence(
            const w_Item       & i_lhs,
            const w_Item       & i_rhs
    )
      : m_sequence( 1, i_lhs )
    {
        m_sequence.push_back( i_rhs );
    }

    ItemSequence(
            const ItemSequence     & i_lhs,
            const w_Item    & i_rhs
    )
      : m_sequence( i_lhs.m_sequence )
    {
        m_sequence.push_back( i_rhs );
    }

    ItemSequence(
            const w_Item    & i_lhs,
            const ItemSequence     & i_rhs
    )
      : m_sequence( 1, i_lhs )
    {
        m_sequence.insert( m_sequence.end(), i_rhs.m_sequence.begin(), i_rhs.m_sequence.end() );
    }

    ItemSequence(
            const ItemSequence     & i_lhs,
            const ItemSequence     & i_rhs
    )
      : m_sequence( i_lhs.m_sequence )
    {
        m_sequence.insert( m_sequence.end(), i_lhs.m_sequence.begin(), i_lhs.m_sequence.end() );
    }

};

/** This must be overloaded for the type in question
template <typename w_Item>
ItemSequence<w_Item> operator|( const w_Item & i_lhs, const w_Item & i_rhs )
{
    return ItemSequence<w_Item>( i_lhs, i_rhs );
}
*/

template <typename w_Item>
ItemSequence<w_Item> operator|( const ItemSequence<w_Item> & i_lhs, const w_Item & i_rhs )
{
    return ItemSequence<w_Item>( i_lhs, i_rhs );
}

template <typename w_Item>
ItemSequence<w_Item> operator|( const w_Item & i_lhs, const ItemSequence<w_Item> & i_rhs )
{
    return ItemSequence<w_Item>( i_lhs, i_rhs );
}

template <typename w_Item>
ItemSequence<w_Item> operator|( const ItemSequence<w_Item> & i_lhs, const ItemSequence<w_Item> & i_rhs )
{
    return ItemSequence<w_Item>( i_lhs, i_rhs );
}




}; // namespace

#endif // x_grt_list_init_h_x



