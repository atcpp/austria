
#include "dxf_model.h"
#include "grt_entity.h"
#include "grt_list_init.h"
#include "grt_scanner.h"
#include <algorithm>
#include <typeinfo>
//#include <>

#include <vector>

namespace grt_impl
{
//

using namespace grt;
using dxf::GroupCode;

typedef ItemSequence<GroupCode> GroupSequence;
typedef ItemRef<GroupCode>      GroupCodeRef;

GroupSequence operator|( const GroupCode & i_lhs, const GroupCode & i_rhs )
{
    return GroupSequence( i_lhs, i_rhs );
}

// ======== Serializer ================================================
/**
 * 
 *
 */

class GRT_EXPORT Serializer
  : public at::PtrTarget_MT
{
    public:

};

extern const Serializer       & g_2dvec_srl;
extern const Serializer       & g_3dvec_srl;
extern const Serializer       & g_ignore;


// ======== ParserSpec_Base ===========================================
/**
 * The parser specification line
 *
 */

class GRT_EXPORT ParserSpec_Base
{
    public:

    ParserSpec_Base(
        const Serializer                    & i_serializer,
        int                                   i_repeat_count,
        const GroupSequence                 & i_sequence,
        const GroupSequence                 & i_repeat_seq
    )
      : m_sequence( i_sequence ),
        m_repeat_seq( i_repeat_seq ),
        m_repeat_count( i_repeat_count ),
        m_serializer( i_serializer )
    {
    }

    /**
     * m_sequence is the beginning sequence of the pattern
     *  to be matched.
     */
    const GroupSequence                     m_sequence;
    const GroupSequence                     m_repeat_seq;
    int                                     m_repeat_count;
    const Serializer                      & m_serializer;

};


// ======== ParserSpec ================================================
/**
 * ParserSpec specifies sequences of elements that can be used to
 * pre-parse the input stream into components that are more easily digested
 * by the deserializer.
 */

class GRT_EXPORT ParserSpec
  : public ParserSpec_Base
{
    public:

    /**
     * ParserSpec
     *
     */
    ParserSpec(
        const Serializer                    & i_serializer,
        const GroupSequence                 & i_sequence,
        const GroupSequence                 & i_repeat_seq = GroupSequence()
    )
      : ParserSpec_Base( i_serializer, 1, i_sequence, i_repeat_seq )
    {
    }

    ParserSpec(
        const Serializer                    & i_serializer,
        int                                   i_repeat_count,
        const GroupSequence                 & i_sequence,
        const GroupSequence                 & i_repeat_seq = GroupSequence()
    )
      : ParserSpec_Base( i_serializer, i_repeat_count, i_sequence, i_repeat_seq )
    {
    }

};

/**
 * s_parser_spec defines the sequences that will
 * be scanned prior to scanning each record.
 */
static const ParserSpec  s_parser_spec[] = {
    ParserSpec( g_2dvec_srl, 9, GroupCode::GetGroupCode(10) | GroupCode::GetGroupCode(20) ),
    ParserSpec( g_3dvec_srl, 9, GroupCode::GetGroupCode(10) | GroupCode::GetGroupCode(20) | GroupCode::GetGroupCode(30) ),
    ParserSpec( g_ignore, GroupCode::s_comment ), // drop comments
    ParserSpec( g_ignore, GroupCode::s_record ), // drop records - they get pushed
};

typedef grt::DFAMachine< GroupCodeRef, const Serializer * > GrtScanner;


// ======== OffsetCode ================================================
/**
 * A transform functor
 *
 */

class GRT_EXPORT OffsetCode
{
    public:

    /**
     * OffsetCode
     *
     */
    OffsetCode(
        int                     i_offset
    )
      : m_offset( i_offset )
    {
    }


    GroupCodeRef operator()( const GroupCodeRef & i_code )
    {
        return GroupCode::GetGroupCode( m_offset + i_code.m_ref->m_group_code );
    }

    /**
     * m_offsetcode
     */
    int                         m_offset;


};


// ======== NewScanner ================================================
/**
 * This will create a new scanner
 *
 *
 * @return nothing
 */

at::PtrDelegate<GrtScanner *> NewScanner()
{
    at::PtrDelegate<GrtScanner *> l_scanner = new GrtScanner;

    for ( const ParserSpec * l_pitr = s_parser_spec; l_pitr < at::ArrayEnd( s_parser_spec ); ++ l_pitr )
    {
        for ( int i = l_pitr->m_repeat_count; i --; )
        {
            at::PtrView< GrtScanner::DFAState *>    l_state_res;

            std::vector<GroupCodeRef>   l_newseq( sizeof( l_pitr->m_sequence.m_sequence.size() ) );

            std::transform(
                l_pitr->m_sequence.m_sequence.begin(),
                l_pitr->m_sequence.m_sequence.end(),
                l_newseq.begin(),
                OffsetCode( i )
            );
            
            
            const Serializer * l_serializer = (* l_scanner ).AddTerminal(
                l_newseq.begin(),
                l_newseq.end(),
                &( l_pitr->m_serializer ),
                ( const at::PtrView<GrtScanner::DFAState *> * ) 0,
                & l_state_res
            );

            if ( ! l_pitr->m_repeat_seq.m_sequence.size() )
            {
                if ( l_serializer )
                {
                    // possible collision
                    if ( &( l_pitr->m_serializer ) == l_serializer )
                    {
                        break;
                    }

                    throw "Scanner has collision\n";
                }
                break; // all is good
            }

            // OK now we have a repeated sequence

            l_newseq.resize( sizeof( l_pitr->m_repeat_seq.m_sequence.size() ) );
            
            std::transform(
                l_pitr->m_repeat_seq.m_sequence.begin(),
                l_pitr->m_repeat_seq.m_sequence.end(),
                l_newseq.begin(),
                OffsetCode( i )
            );

            const Serializer * l_serializer1 = l_scanner->AddTerminal(
                l_newseq.begin(),
                l_newseq.end(),
                &( l_pitr->m_serializer ),
                & l_state_res,
                & l_state_res
            );

            // ok
        }
    }

    return l_scanner;
}


// ======== GetScanner ================================================
/**
 *	GetScanner will return a scanner built for scanning a dxf input stream
 *
 * @return A new scanner
 */

at::PtrView<const GrtScanner *> GetScanner()
{
    // this is done once
    static at::Ptr<GrtScanner *> s_machine = NewScanner();
    return s_machine;
}


} // namespace grt_impl

namespace grt
{
    
template <>
class DFAMachineObjectToTokenConvert<const dxf::Item>
{
    public:

    typedef const dxf::Item       w_Type;

    w_Type                      & m_object;

    /**
     * DFAMachineObjectToTokenConvert
     *
     */
    DFAMachineObjectToTokenConvert(
        w_Type                  & i_object
    )
      : m_object( i_object )
    {
    }

    operator const dxf::GroupCode & ()
    {
        return m_object.GetCode();
    }

};

} // namespace grt

namespace grt_impl
{

// ======== TraverseContext_Grt =======================================
/**
 * This is called back by the dxflib upon every item in the file.
 *
 */

class GRT_EXPORT TraverseContext_Grt
  : public dxf::TraverseContext
{
    public:

    enum TraverseState
    {
        Start,
        HaveSceneGraph,
    };

    typedef std::list<dxf::Item>                t_ScannerResultType;


    /**
     * is a copy of the scanner
     */
    GrtScanner::Scanner<dxf::Item, t_ScannerResultType >     m_scanner;
    

    TraverseContext_Grt()
      : m_scanner( GetScanner()->MakeScanner<dxf::Item, std::list<dxf::Item> >( ) )
    {
    }



    // ======== DeSerializerState =====================================
    /**
     * This contains the deserialization state of a single record.
     *
     */

    class DeSerializerState
    {
        public:

        /**
         * DeSerializerState
         *
         */
        DeSerializerState(
            at::PtrView< dxf::Record * >      i_record
        )
          : m_record( i_record )
        {
        }

        /**
         * m_record is the state of the object being deserialized
         */
        at::Ptr< dxf::Record * >                m_record;

        /**
         * m_tokens is the list of parsed tokens.
         */
        TokenList                               m_items;
    };
    

    typedef std::list< DeSerializerState >              t_Stack;
    t_Stack                                             m_stack;

    // ======== PushRecord ==============================================
    /**
     * 
     *
     *
     * @param i_record
     */

    void PushRecord( at::PtrView< dxf::Record * >  i_record )
    {
        m_stack.push_back( i_record );
    }


    // ======== PopRecord =============================================
    /**
     * A record is complete
     *
     */

    void PopRecord()
    {

        
        m_stack.pop_back();
    }


    // ======== PushItem ==============================================
    /**
     * Pushes an item into the context
     *
     * @param i_item
     */

    void PushItem( const dxf::Item & i_item )
    {
    }

    at::Ptr< grt::SceneGraph * >            m_scene_graph;
    TraverseState                           m_state;

    
};


} // namespace grt_impl
