//
// The Graphics Template Library (grt) is copyright (c) Gianni Mariani 2006
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * grt_entity.h
 *
 */

#ifndef x_grt_entity_h_x
#define x_grt_entity_h_x 1

#include "grt_base.h"

#include "dxf_model.h"

#include "at_lifetime_mt.h"

#include "at_list.h"

#include <typeinfo>

namespace grt
{
//

class HandleBase;
class HandleResolverNode;


  
class ResolverListTraits
  : public at::Default_List_Traits
{
    public:

    typedef at::PtrTarget_MT        Root_Base;

    template <
        typename    w_container_wrapper
    >
    class Entry_1_Traits
      : public at::List_RefCountTraits< w_container_wrapper, at::ListNothing_Basic >
    {
    };

    public:
    template <
        typename        w_inheritor
    >
    class Payload
        : public w_inheritor
    {
     
        public:
        // ======== ResolveReference ======================================
        /**
         * This passes in the pointer to resolve the reference.
         *
         * @return nothing
         */
    
        virtual bool ResolveReference(
            at::PtrView<HandleBase *>         i_ptr,
            const std::type_info            & i_typeinfo
        ) = 0;
  
    };
     
};

// make some more semantically interesting names for the list types
//
typedef at::List2<ResolverListTraits>   ResolverListDefs;
typedef ResolverListDefs::list_1        ResolverListHead_HandleBase;
typedef ResolverListDefs::list_2        ResolverListHead_ResolverNode;

// ======== ReferenceResolver =========================================
/**
 * ReferenceResolver defines the components to resolve a reference
 * to a handle object.  Different kinds of these are created
 * depending on the type of pointer
 */

class GRT_EXPORT ReferenceResolver
  : public ResolverListDefs::Apogee
{
    public:



};

// ======== HandleBase ================================================
/**
 * All objects that can be referenced in a DXF file using a "handle"
 * need to inherit from this.
 */

class GRT_EXPORT HandleBase
  : public at::PtrTarget_MT
{
    public:

    /**
     * m_resolvernode points to an object that is
     * used to contain the links (to and from) this
     * node.
     */
    at::Ptr<HandleResolverNode *>           m_resolvernode;

    // A list of all the resolvers for this handle is here
    // adding resolvers to this list should increment their
    // reference count - hence deleting the HandleBase
    // will cause all the nodes that point to it to
    // also either be deleted or have their refcount
    // reduced.  In theory we should delete the
    // resolver list contents FIRST in the destruction process
    // but it should make no difference in a single
    // threaded program.
    ResolverListHead_HandleBase             m_resolver_list;


    // ======== ResolutionFailed ======================================
    /**
     * ResolutionFailed is called when a member has failed resolutuon.
     *
     *
     * @param i_name The name given by the deserializer
     * @return nothing
     */

    virtual void ResolutionFailed(
        const char          * i_name
    ) = 0;

};



// ======== HandleResolverNode ========================================
/**
 * Each handle corresponds to a HandleResolverNode.  When objects are
 * deserialized, a link is made to the handle resolver for them and when
 * the node is found, it is added
 *
 */

class GRT_EXPORT HandleResolverNode
  : public at::PtrTarget_MT,
    public ResolverListHead_ResolverNode // allows us to find the HandleResolverNode by down cast
{
    public:

    HandleResolverNode(
        const std::string           & i_handle,
        const std::type_info        & i_typeinfo
    )
      : m_handle( i_handle ),
        m_typeinfo( & i_typeinfo )
    {
    }

    // ======== HandleResolved_Worker ========================================
    /**
     * This is called once the handle has been resolved.
     *
     * @param i_ptr The base class of handle resolution
     * @param i_typeinfo The typeid of the derived class
     * @return nothing
     */

    bool HandleResolved_Worker(
        at::PtrView<HandleBase *>     i_ptr,
        const std::type_info              & i_typeinfo
    ) {
        if ( i_typeinfo != * m_typeinfo )
        {
            return false; // pretty serious - should probably throw
        }

        // OK run through the handle references and assign them.

        iterator    l_itr = this->begin();
        const iterator  l_end = this->end();
        
        while ( l_itr != l_end )
        {
            l_itr->ResolveReference( i_ptr, i_typeinfo );
            
            ++ l_itr;
        }
        return true;
    }

    // ======== HandleResolved ========================================
    /**
     * Template that does all the type specific stuff.
     *
     * @param i_ptr Is the pointer value for this handle resolver.
     * @return True if the resolution was successful (same type)
     */

    template <typename w_PtrType>
    bool HandleResolved(
        w_PtrType           * i_ptr
    ) {
        return HandleResolved_Worker(
            static_cast<HandleBase *>( i_ptr ),
            typeid( w_PtrType )
        );
    }
    
    // ======== ResolveReference ======================================
    /**
     * This adds a reference resolver to this HandleResolverNode
     *
     * @param i_handle  The handle for the object whose member
     *                  is referenced by the resolver.
     * @param i_resolver The resolver that referes to a member of
     *                      the i_handle_base
     * @return nothing
     */

    bool ResolveReference(
        at::PtrView< HandleBase * >         i_handle_base,
        at::PtrView< ReferenceResolver * >  i_resolver
    ) {
        // Add the reference resolver to the two lists.
        // this should increment the refcount on the ReferenceResolver
        i_handle_base->m_resolver_list.push_back( * i_resolver );        
        this->push_back( * i_resolver );

        // If the object is currently known, then the resolve can happen
        // right away.

        if ( m_handle_ptr )
        {
            // woo hoo - resolve this now
            return i_resolver->ResolveReference( m_handle_ptr, * m_typeinfo );
        }
        
        return true;
    }    

    
    const std::string                 m_handle;
    const std::type_info            * m_typeinfo;
    at::PtrView<HandleBase *>         m_handle_ptr;
};



// ======== HandleResolver ============================================
/**
 * HandleResolver provides the funtionality to resolve dxf handles.
 * This is somewhat generic.
 */

class HandleResolver
  : public at::PtrTarget_MT
{
    public:

    /**
     * HandleResolver
     *
     */
    HandleResolver()
    {}


    // ======== GetHandleResolverNode ====================================
    /**
     * HandleResolverNode is used to find a new resolver.
     *
     * @param i_handle The "string" handle
     * @return A handle resolver node
     */

    at::PtrView< HandleResolverNode * > GetHandleResolverNode(
        const std::string       & i_handle,
        const std::type_info          & i_typeinfo
    ) {

        std::pair<t_Map::iterator, bool> l_insert_result =
            m_map.insert( t_Map::value_type( i_handle, 0 ) );

        if ( ! l_insert_result.second )
        {
            // the item was already there

            if ( l_insert_result.first->second )
            {
                // the types may not be compatible but
                // we'll resolve that later
                return l_insert_result.first->second;
            }
        }

        l_insert_result.first->second = new HandleResolverNode( l_insert_result.first->first, i_typeinfo );

        // woo hoo, we have a new resolver
        return l_insert_result.first->second;
    }
    

    typedef std::map< std::string, at::Ptr<HandleResolverNode *> >  t_Map;

    /**
     * m_map is a link to all the resolver nodes
     * mapped by handle
     */
    t_Map                               m_map;

    /**
     * m_idgen is used to generate new ids.
     */
    unsigned                            m_idgen;
};


// ======== TypeInfoOfDeref ===========================================
/**
 * This returns a std::type_info of the passed in type.
 *
 * @param a never referenced object
 * @return nothing
 */

template <typename w_Type>
inline const std::type_info TypeInfoOfDeref( const w_Type & i_val )
{
    return typeid( i_val );
}


// ======== DerefTypeAssign ===========================================
/**
 * This discovers the type of the dereferenced pointer and then assigns
 * a statically downcasted version of the other.
 *
 * @return nothing
 */

template <typename w_Type, typename w_DerefType, typename w_Parent >
inline bool DerefTypeAssign(
    w_Type          & o_dest,
    w_DerefType     * i_ignored,
    w_Parent        * i_parent_ptr
) {

    w_DerefType * l_newp = dynamic_cast< w_DerefType * >( i_parent_ptr );

    if ( ! l_newp )
    {
        return false;
    }

    o_dest = at::PtrView< w_DerefType * >( l_newp );

    return true;
}

// ======== ReferenceResolver_Basic ===================================
/**
 * This implements a basic resolver.
 *
 */

template <typename w_Type>
class GRT_EXPORT ReferenceResolver_Basic
  : public ReferenceResolver
{
    public:

    /**
     * ReferenceResolver_Basic
     * @param i_reference refers to a various "POINTER" types.  Especially
     * ones that can have their "value_type" discovered with an deref (*v).
     */
    ReferenceResolver_Basic(
        at::PtrView<HandleBase *>       m_handle,
        w_Type                        & i_reference,
        const char                    * i_name
    )
      : m_thisrefhandle( m_handle ),
        m_reference( i_reference ),
        m_typeinfo( TypeInfoOfDeref( * i_reference ) ),
        m_name( i_name )
    {
    }

    /**
     * m_reference is the location where the handle
     * needs to be written.
     */
    at::PtrView<HandleBase *>     m_thisrefhandle;
    w_Type                      & m_reference;
    const std::type_info        & m_typeinfo;
    const char                  * const m_name;

    // ======== ResolveReference ======================================
    /**
     * This passes in the pointer to resolve the reference.
     *
     * @return nothing
     */

    virtual bool ResolveReference(
        at::PtrView<HandleBase *>         i_ptr,
        const std::type_info            & i_typeinfo
    ) {
        if ( DerefTypeAssign( m_reference, &* m_reference, &* i_ptr ) )
        {
            return true;
        }

        // notify the handle that resolution failed.
        m_thisrefhandle->ResolutionFailed( m_name );

        return false;
    }

};


// ======== DxfType ===================================================
/**
 * This maps to the group code type
 *
 */

template <int w_Code>
class GRT_EXPORT DxfType
{
    public:

    typedef typename dxf::GroupTypeData< 0 >::t_Traits::t_InternalType value_type;
    
};


// ======== SerializerBase ============================================
/**
 * This is the base class for serializers.  In particular, this class
 * allows for the serializer to store unrecognizable parts of the 
 * serialization stream.
 */

class GRT_EXPORT SerializerBase
{
    public:

    virtual ~SerializerBase(){}

    at::Any<>               m_serializer_unused;

};


// ======== RCSerializerBase ==========================================
/**
 * Reference counted version of the serializer
 *
 */

class GRT_EXPORT RCSerializerBase
  : public at::PtrTarget_MT,
    public SerializerBase
{
};



// ============== Entity List Classes ==================

class Entity;
  
class EntityListTraits
  : public at::Default_List_Traits
{
    public:

    typedef HandleBase          Root_Base;

    template <
        typename    w_container_wrapper
    >
    class Entry_1_Traits
      : public at::List_RefCountTraits< w_container_wrapper, at::ListNothing_Basic >
    {
    };

    public:
    template <
        typename        w_inheritor
    >
    class Payload
        : public w_inheritor
    {
     
        public:

        virtual Entity & GetEntity() = 0;
        virtual const Entity & GetEntity() const = 0;
  
    };
     
};

// make some more semantically interesting names for the list types
//
typedef at::List2<EntityListTraits>     EntityListDefs;
typedef EntityListDefs::list_1          EntityList_Owner; // elements in this list type are owned
typedef EntityListDefs::list_2          EntityList_Layer;
typedef EntityListDefs::Apogee          Entity_Base;


// ======== Entity =================================================
/**
 * The Entity container makes it easier to locate objects.
 *
 */


class GRT_EXPORT Entity
  : public Entity_Base
{
    public:

    // 0 Entity type not omitted
    DxfType<0>::value_type                              m_entity_type;
    
    // 5 Handle not omitted
    DxfType<5>::value_type                              m_handle;
    
    // 102 Start of application-defined group no default
    // Common Group Codes for Entities | 53
    // Group codes that apply to all graphical objects
    // If omitted,
    // defaults to
    // Group code Description
    // {application_name (optional)
    // application-defined Codes and values within the 102 groups are application-defined (optional) no default
    // codes
    // 102 End of group, } (optional) no default
    // {ACAD_REACTORS indicates the start of the AutoCAD persistent reactors no default
    // group. This group exists only if persistent reactors have been attached to
    // this object (optional)
    // 102
    
    // 330 Soft-pointer ID/handle to owner dictionary (optional) no default
    DxfType<330>::value_type                            m_dictionary_owner_handle;
    
    // 102 End of group, } (optional) no default
    // {ACAD_XDICTIONARY indicates the start of an extension dictionary no default
    // group. This group exists only if an extension dictionary has been attached
    // to the object (optional)
    // 102
    
    // 360 Hard-owner ID/handle to owner dictionary (optional) no default
    DxfType<360>::value_type                            m_hard_owner;
    
    // 102 End of group, } (optional) no default
    // 330 Soft-pointer ID/handle to owner BLOCK_RECORD object not omitted
    DxfType<330>::value_type                            m_block_owner_handle;
    
    // 100 Subclass marker (AcDbEntity) not omitted
    
    // 67 Absent or zero indicates entity is in model space. 1 indicates entity is in 0
    // paper space (optional).
    DxfType<67>::value_type                             m_model_space;
    
    // 410 APP: layout tab name not omitted
    DxfType<410>::value_type                            m_layout_tab;
    
    // 8 Layer name not omitted
    DxfType<8>::value_type                              m_layer;
    
    // 6 Linetype name (present if not BYLAYER). The special name BYBLOCK BYLAYER
    // indicates a floating linetype (optional)
    DxfType<6>::value_type                              m_linetype;
    

    // 347 Hard-pointer ID/handle to material object (present if not BYLAYER) BYLAYER
    DxfType<347>::value_type                            m_material_handle;
    
    // 62 Color number (present if not BYLAYER); zero indicates the BYBLOCK BYLAYER
    // (floating) color; 256 indicates BYLAYER; a negative value indicates that the
    // layer is turned off (optional)
    DxfType<62>::value_type                             m_color;
    
    // 370 Lineweight enum value. Stored and moved around as a 16-bit integer. not omitted
    DxfType<370>::value_type                            m_lineweight;
    
    // 48 Linetype scale (optional) 1.0
    DxfType<48>::value_type                             m_linetype_scale;
    
    // 60 Object visibility (optional): 0 = Visible; 1 = Invisible 0
    DxfType<60>::value_type                             m_visible;
    
    // 92 Number of bytes in the proxy entity graphics represented in the subsequent no default
    //DxfType<92>::value_type                             m_proxy_entity_bytes;
    
    // 310 groups, which are binary chunk records (optional)
    //DxfType<310>::value_type                            m_;
    
    // 310 Proxy entity graphics data (multiple lines; 256 characters max. per line) no default
    // (optional)
    //DxfType<310>::value_type                             m_;
    
    // 420 A 24-bit color value that should be dealt with in terms of bytes with values no default
    // of 0 to 255. The lowest byte is the blue value, the middle byte is the green
    // value, and the third byte is the red value. The top byte is always 0. The
    // group code cannot be used by custom entities for their own data because
    // the group code is reserved for AcDbEntity, class-level color data and
    // AcDbEntity, class-level transparency data
    DxfType<420>::value_type                            m_color24;
    
    // 430 Color name. The group code cannot be used by custom entities for their no default
    // own data because the group code is reserved for AcDbEntity, class-level
    // color data and AcDbEntity, class-level transparency data
    DxfType<430>::value_type                            m_color_name;
    
    // 440 Transparency value. The group code cannot be used by custom entities for no default
    // their own data because the group code is reserved for AcDbEntity, class-level
    // color data and AcDbEntity, class-level transparency data
    DxfType<440>::value_type                            m_transparency;
    
    // 390 Hard-pointer ID/handle to the plot style object no default
    DxfType<390>::value_type                            m_plot_style_handle;
    
    // 284 Shadow mode no default
    //  0 = Casts and receives shadows
    //  1 = Casts shadows
    //  2 = Receives shadows
    //  3 = Ignores shadows
    DxfType<284>::value_type                            m_shadow_mode;
    
};



// ======== ToDxfConverter ============================================
/**
 * ToDxfConverter contains the basic conversion methods.
 *
 */
class TokenizedItem;

class DXF_EXPORT ToDxfConverter
{
    public:


    // ======== ToDxfConvert ==========================================
    /**
     * Convert to DXF lib model format
     *
     *
     * @param i_tokitem  The tokenized item
     * @return true if conversion was successful
     */

    virtual bool ToDxfConvert(
        const TokenizedItem         & i_tokitem,
        dxf::Record::Items          & o_out
    ) = 0;

};


// ======== TokenizedItem =========================================
/**
 * Tokenized item is a token that has been pushed through the scanner
 * an potentially translated to a new list of tokens that
 * are related.  For example a 10,20,30 sequence of tokens
 * will be presented in a TokenizedItem once it is passed
 * through the converter and then presented as an a 10 with the
 * appropriate context.
 */

class TokenizedItem
{
    public:

    /**
     * TokenizedItem
     *
     */
    TokenizedItem(
        const dxf::GroupCode    & i_code,
        const ToDxfConverter    & i_todxf
    )
        : m_code( & i_code ),
          m_todxf( & i_todxf )
    {
    }

    // ======== GetCode ===============================================
    /**
     * Return the code as a const reference.
     */

    const dxf::GroupCode & GetCode() const
    {
        return * m_code;
    }

    const dxf::GroupCode            * m_code;

    /**
     * The conversion functions for this data type.
     */
    const ToDxfConverter            * m_todxf;

    /**
     * Pointer to the "converted" data - generic "Any" container.
     */
    at::Ptr< dxf::ValueType * >       m_value;

};
    
typedef std::list<TokenizedItem>       TokenList;



// ======== AcadLWPOLYLINE ============================================
/**
 * 
 *
 */

class GRT_EXPORT AcadLWPOLYLINE
//  : public AcadEntity
{
    public:

    

};


// ======== Layer =====================================================
/**
 * A Layer entity contains a list
 *
 */

class GRT_EXPORT Layer
  : public Entity,          // Layer IS an Entity AND
    public EntityList_Layer // Layer IS a list of entities
{
    public:


};


// ======== SceneGraph ================================================
/**
 * A SceneGraph corresponds to the contents of a dxf file.
 *
 */

class DXF_EXPORT SceneGraph
  : public at::PtrTarget_MT
{
    public:

//    std::map<std::string, ???>                  m_header;
    

};


}; // namespace

#endif // x_grt_entity_h_x



#if 0
Layer 

SceneGraph 0 Header Reference m_header

Header 9 HeaderItem

Header 10 Point PointContext 

SceneGraph 0 Entity m_entities

#endif


#if 0

{
// 3DFACE
// The following group codes apply to 3dface entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 3dface group codes
// Group code Description

// 100 Subclass marker (AcDbFace)

// 10 First corner (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of first corner (in WCS)

// 11 Second corner (in WCS)
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of second corner (in WCS)

// 12 Third corner (in WCS)
// DXF: X value; APP: 3D point
// - 22, 32 DXF: Y and Z values of third corner (in WCS)

// 13 Fourth corner (in WCS). If only three corners are entered, this is the same as the third corner
// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of fourth corner (in WCS)

// 70 Invisible edge flags (optional; default = 0):
//  1 = First edge is invisible
//  2 = Second edge is invisible
//  4 = Third edge is invisible
//  8 = Fourth edge is invisible

{
// 3DSOLID
// The following group codes apply to 3dsolid entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// �"Formatting Conventions in This Reference"�h on page 2.

// 100 Subclass marker (AcDbModelerGeometry)

// 70 Modeler format version number (currently = 1)

// 1 Proprietary data (multiple lines < 255 characters each)
// 3 Additional lines of proprietary data (if previous group 1 string is greater than 255 characters)
// (optional)

// 100 Subclass marker (AcDb3dSolid
)
// 350 Soft-owner ID/handle to history object

{
// ACAD_PROXY_ENTITY
// The following group codes apply to proxy entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.

// 100 DXF: AcDbProxyEntity

// 90 DXF: Proxy entity class ID (always 498)
// 91 DXF: Application entity's class ID. Class IDs are based on the order of the class in the CLASSES
// section. The first class is given the ID of 500, the next is 501, and so on

// 92 DXF: Size of graphics data in bytes

// 310 DXF: Binary graphics data (multiple entries can appear) (optional)

// 93 DXF: Size of entity data in bits

// 310 DXF: Binary entity data (multiple entries can appear) (optional)

// 330 or
// 340 DXF: An object ID (multiple entries can appear) (optional)
// 350
// 360

// 94 DXF: 0 (indicates end of object ID section)

// 95 DXF: Object drawing format when it becomes a proxy (a 32-bit unsigned integer):
// Low word is AcDbDwgVersion
// High word is MaintenanceReleaseVersion

// 70 DXF: Original custom object data format:
// 0 = DWG format
// 1 = DXF format

{
// ARC
// The following group codes apply to arc entities. In addition to the group codes
// described here, see gCommon Group Codes for Entitiesh on page 53. For
// information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.

// 100 Subclass marker (AcDbCircle)

// 39 Thickness (optional; default = 0)

// 10 Center point (in OCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of center point (in OCS)

// 40 Radius

// 100 Subclass marker (AcDbArc)

// 50 Start angle

// 51 End angle

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

{
// ATTDEF
// The following group codes apply to attdef (attribute definition) entities. In
// addition to the group codes described here, see "Common Group Codes for
// Entities"�h on page 53. For information about abbreviations and formatting
// used in this table, see gFormatting Conventions in This Referenceh on page
// 2.

// 100 Subclass marker (AcDbText)

// 39 Thickness (optional; default = 0)

// 10 First alignment point (in OCS)
// DXF: X value; APP: 3D point
// 20, 30 DXF: Y and Z values of text start point (in OCS)

// 40 Text height

// 1 Default value (string)

// 100 Subclass marker (AcDbAttributeDefinition)

// 50 Text rotation (optional; default = 0)

// 41 Relative X scale factor (width) (optional; default = 1). This value is also adjusted when fit-type
// text is used

// 51 Oblique angle (optional; default = 0)

// 7 Text style name (optional; default = STANDARD)

// 71 Text generation flags (optional; default = 0); see TEXT group codes

// 72 Horizontal text justification type (optional; default = 0); see TEXT group codes

// 11 Second alignment point (in OCS) (optional)
// DXF: X value; APP: 3D point
// Meaningful only if 72 or 74 group values are nonzero
// - 21, 31 DXF: Y and Z values of second alignment point (in OCS) (optional)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction

// 100 Subclass marker (AcDbAttributeDefinition)

// 3 Prompt string

// 2 Tag string (cannot contain spaces)

// 70 Attribute flags:
//  1 = Attribute is invisible (does not appear)
//  2 = This is a constant attribute
//  4 = Verification is required on input of this attribute
//  8 = Attribute is preset (no prompt during insertion)

// 73 Field length (optional; default = 0) (not currently used)

// 74 Vertical text justification type (optional, default = 0); see group code 73 inTEXT

// 280 Lock position flag. Locks the position of the attribute within the block reference
// If group 72 and/or 74 values are nonzero then the first alignment point values
// are ignored and new values are calculated by AutoCAD, based on the second
// alignment point and the length and height of the text string itself (after
// applying the text style). If the 72 and 74 values are zero or missing, then the
// second alignment point is meaningless.

{
// ATTRIB
// The following group codes apply to attrib (attribute) entities. In addition to
// the group codes described here, see gCommon Group Codes for Entitiesh on
// page 53. For information about abbreviations and formatting used in this
// table, see gFormatting Conventions in This Referenceh on page 2.

// 100 Subclass marker (AcDbText)

// 39 Thickness (optional; default = 0)

// 10 Text start point (in OCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of text start point (in OCS)

// 40 Text height

// 1 Default value (string)

// 100 Subclass marker (AcDbAttribute)

// 2 Attribute tag (string; cannot contain spaces)

// 70 Attribute flags:
//  1 = Attribute is invisible (does not appear)
//  2 = This is a constant attribute
//  4 = Verification is required on input of this attribute
//  8 = Attribute is preset (no prompt during insertion)

// 73 Field length (optional; default = 0) (not currently used)

// 50 Text rotation (optional; default = 0)

// 41 Relative X scale factor (width) (optional; default = 1). This value is also adjusted when fit-type
// text is used

// 51 Oblique angle (optional; default = 0)

// 7 Text style name (optional; default = STANDARD)

// 71 Text generation flags (optional; default = 0). See TEXT group codes

// 72 Horizontal text justification type (optional; default = 0). See TEXT group codes

// 74 Vertical text justification type (optional; default = 0). See group code 73 inTEXT

// 11 Alignment point (in OCS) (optional)
// DXF: X value; APP: 3D point
// Present only if 72 or 74 group is present and nonzero
// - 21, 31 DXF: Y and Z values of alignment point (in OCS) (optional)

// 210 Extrusion direction. Present only if the entity's extrusion direction is not parallel to the WCS Z
// axis (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 280 Lock position flag. Locks the position of the attribute within the block reference
// If group 72 and/or 74 values are nonzero then the text insertion point values
// are ignored, and new values are calculated by AutoCAD based on the text
// alignment point and the length of the text string itself (after applying the text
// style). If the 72 and 74 values are zero or missing, then the text alignment
// point is ignored and recalculated based on the text insertion point and the
// length of the text string itself (after applying the text style).

{
// BODY
// The following group codes apply to body entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.

// 100 Subclass marker (AcDbModelerGeometry)

// 70 Modeler format version number (currently = 1)

// 1 Proprietary data (multiple lines < 255 characters each)
// 3 Additional lines of proprietary data (if previous group 1 string is greater than 255 characters)
// (optional)

{
// CIRCLE
// The following group codes apply to circle entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.

// 100 Subclass marker (AcDbCircle)

// 39 Thickness (optional; default = 0)

// 10 Center point (in OCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of center point (in OCS)

// 40 Radius

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// DIMENSION
// Dimension entity definitions consist of group codes that are common to all
// dimension types, followed by codes specific to the type.
// Common Dimension Group Codes

// 100 Subclass marker (AcDbDimension)

// 2 Name of the block that contains the entities that make up the dimension picture

// 10 Definition point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of definition point (in WCS)

// 11 Middle point of dimension text (in OCS)
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of middle point of dimension text (in OCS)

// 70 Dimension type:
// Values 0-6 are integer values that represent the dimension type. Values 32, 64, and 128 are
// bit values, which are added to the integer values (value 32 is always set in R13 and later
// releases)
//  0 = Rotated, horizontal, or vertical; 1 = Aligned
//  2 = Angular; 3 = Diameter; 4 = Radius
//  5 = Angular 3 point; 6 = Ordinate
//  32 = Indicates that the block reference (group code 2) is referenced by this dimension only
//  64 = Ordinate type. This is a bit value (bit 7) used only with integer value 6. If set, ordinate
// is X-type; if not set, ordinate is Y-type
// 128 = This is a bit value (bit 8) added to the other group 70 values if the dimension text
// has been positioned at a user-defined location rather than at the default location

// 71 Attachment point:
//  1 = Top left; 2 = Top center; 3 = Top right
//  4 = Middle left; 5 = Middle center; 6 = Middle right
//  7 = Bottom left; 8 = Bottom center; 9 = Bottom right

// 72 Dimension text line-spacing style (optional):
//  1 (or missing) = At least (taller characters will override)
//  2 = Exact (taller characters will not override)

// 41 Dimension text-line spacing factor (optional):
// Percentage of default (3-on-5) line spacing to be applied. Valid values range from 0.25
// to 4.00

// 42 Actual measurement (optional; read-only value)

// 1 Dimension text explicitly entered by the user. Optional; default is the measurement. If null
// or g<>h, the dimension measurement is drawn as the text, if gg (one blank space), the text
// is suppressed. Anything else is drawn as the text


// 53 The optional group code 53 is the rotation angle of the dimension text away from its default
// orientation (the direction of the dimension line) (optional)

// 51 All dimension types have an optional 51 group code, which indicates the horizontal direction
// for the dimension entity. The dimension entity determines the orientation of dimension text
// and lines for horizontal, vertical, and rotated linear dimensions
// This group value is the negative of the angle between the OCS X axis and the UCS X axis.
// It is always in the XY plane of the OCS

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 3 Dimension style name
// Xdata belonging to the application ID "ACAD" follows a dimension entity if
// any dimension overrides have been applied to this entity. See gDimension
// Style Overridesh on page 69.
// For all dimension types, the following group codes represent 3D WCS points:
// ¡ (10, 20, 30)
// ¡ (13, 23, 33)
// ¡ (14, 24, 34)
// ¡ (15, 25, 35)
// For all dimension types, the following group codes represent 3D OCS points:
// ¡ (11, 21, 31)
// ¡ (12, 22, 32)
// ¡ (16, 26,
//
//
// Aligned Dimension Group Codes
// The following group codes apply to aligned dimensions. In addition to the
// group codes described here, those listed in gCommon Group Codes for Entitiesh
// on page 53 and gCommon Dimension Group Codesh on page 62 can also be
// present. For information about abbreviations and formatting used in this table,
// see gFormatting Conventions in This Referenceh on page 2.
// Aligned dimension group codes
// Group code Description

// 100 Subclass marker (AcDbAlignedDimension)

// 12 Insertion point for clones of a dimension.Baseline and Continue (in OCS)
// DXF: X value; APP: 3D point
// - 22, 32 DXF: Y and Z values of insertion point for clones of a dimension.Baseline and Continue (in OCS)

// 13 Definition point for linear and angular dimensions (in WCS)
// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// 14 Definition point for linear and angular dimensions (in WCS)
// DXF: X value; APP: 3D point
// - 24, 34 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// The point (13,23,33) specifies the start point of the first extension line and
// the point (14,24,34) specifies the start point of the second extension line.
// Point (10,20,30) specifies the dimension line location. The point (11,21,31)
// specifies the midpoint of the dimension text.
// Linear and Rotated Dimension Group Codes
// The following group codes apply to linear and rotated dimensions (note that
// linear and rotated dimensions are part of the AcDbAlignedDimension subclass).
// In addition to the group codes described here, those listed in gCommon Group
// Codes for Entitiesh on page 53 and gCommon Dimension Group Codesh on
// page 62 can also be present. For information about abbreviations and
// formatting used in this table, see gFormatting Conventions in This Referenceh
// on page 2.
//
// 100 Subclass marker (AcDbAlignedDimension)

// 12 Insertion point for clones of a dimension.Baseline and Continue (in OCS)
// DXF: X value; APP: 3D point
// - 22, 32 DXF: Y and Z values of insertion point for clones of a dimension.Baseline and Continue (in OCS)

// 13 Definition point for linear and angular dimensions (in WCS)
// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// 14 Definition point for linear and angular dimensions (in WCS)
// DXF: X value; APP: 3D point
// - 24, 34 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// 50 Angle of rotated, horizontal, or vertical dimensions
// 52 Linear dimension types with an oblique angle have an optional group code 52. When added to
// the rotation angle of the linear dimension (group code 50), it gives the angle of the extension
// lines

// 100 Subclass marker (AcDbRotatedDimension)

//
//
// The following group codes apply to radial and diameter dimensions. In
// addition to the group codes described here, those listed in gCommon Group
// Codes for Entitiesh on page 53 and gCommon Dimension Group Codesh on
// page 62 can also be present. For information about abbreviations and
// formatting used in this table, see gFormatting Conventions in This Referenceh
// on page 2.

// 100 Subclass marker (AcDbRadialDimension or AcDbDiametricDimension)
// 15 Definition point for diameter, radius, and angular dimensions (in WCS)

// DXF: X value; APP: 3D point
// DXF: Y and Z values of definition point for diameter, radius, and angular dimensions (in
// WCS)
// - 25, 35

// 40 Leader length for radius and diameter dimensions

//
// Angular Dimension Group Codes
// The following group codes apply to angular dimensions. In addition to the
// group codes described here, those listed in gCommon Group Codes for Entitiesh
// on page 53 and gCommon Dimension Group Codesh on page 62 can also be
// present. For information about abbreviations and formatting used in this table,
// see gFormatting Conventions in This Referenceh on page 2.
// Angular dimension group codes
// Group code Description
// 100 Subclass marker (AcDb3PointAngularDimension)

// 13 Definition point for linear and angular dimensions (in WCS)

// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// 14 Definition point for linear and angular dimensions (in WCS)
// DXF: X value; APP: 3D point
// - 24, 34 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// Definition point for diameter, radius, and angular dimensions (in WCS) DXF: X value; APP: 3D
// point
// 15
// - 25, 35 DXF: Y and Z values of definition point for diameter, radius, and angular dimensions (in WCS)

// 16 Point defining dimension arc for angular dimensions (in OCS)
// DXF: X value; APP: 3D point
// - 26, 36 DXF: Y and Z values of point defining dimension arc for angular dimensions (in OCS)

// The points (13,23,33) and (14,24,34) specify the endpoints of the line used
// to determine the first extension line. Points (10,20,30) and (15,25,35) specify
// the endpoints of the line used to determine the second extension line. Point
// (16,26,36) specifies the location of the dimension line arc. The point (11,21,31)
// specifies the midpoint of the dimension text.
// The point (15,25,35) specifies the vertex of the angle. The points (13,23,33)
// and (14,24,34) specify the endpoints of the extension lines. The point
// (10,20,30) specifies the location of the dimension line arc and the point
// (11,21,31) specifies the midpoint of the dimension text.


// Ordinate Dimension Group Codes
// The following group codes apply to ordinate dimensions. In addition to the
// group codes described here, those listed in gCommon Group Codes for Entitiesh
// on page 53 and gCommon Dimension Group Codesh on page 62 can also be
// present. For information about abbreviations and formatting used in this table,
// see gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbOrdinateDimension)

// 13 Definition point for linear and angular dimensions (in WCS)
// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// 14 Definition point for linear and angular dimensions (in WCS)
// DXF: X value; APP: 3D point
// - 24, 34 DXF: Y and Z values of definition point for linear and angular dimensions (in WCS)

// The point (13,23,33) specifies the feature location and the point (14,24,34)
// specifies the leader endpoint. The point (11,21,31) specifies the midpoint of
// the dimension text. Point (10,20,30) is placed at the origin of the UCS that is
// current when the dimension is created.


// Dimension Style Overrides
// Dimension style overrides can be applied to dimension, leader, and tolerance
// entities. Any overrides applied to these entities are stored in the entity as
// xdata. The overridden dimension variable group codes and the related values
// are contained within group 1002 control strings. The following example shows
// the xdata of a dimension entity where the DIMTOL and DIMCLRE variables
// have been overridden.
// (setq diment (car (entsel))) ; Select dimension entity
// (setq elst (entget diment '("ACAD"))) ; Get entity definition list
// (assoc -3 elst) ; Extract xdata only
// This code returns the following:
// (-3 ("ACAD" Start of the ACAD APPID section of xdata
// (1000 . "DSTYLE") (1002 . "{") Beginning of the dimstyle subsection
// (1070 . 177) (1070 . 3) The DIMCLRE (code 177) override + value (3)
// (1070 . 71) (1070 . 1) The DIMTOL (code 71) override + value (1)
// (1002 . "}") )) End dimstyle subsection and ACAD section

{
// ELLIPSE
// The following group codes apply to ellipse entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.

// 100 Subclass marker (AcDbEllipse)

// 10 Center point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of center point (in WCS)

// 11 Endpoint of major axis, relative to the center (in WCS)
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of endpoint of major axis, relative to the center (in WCS)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 40 Ratio of minor axis to major axis

// 41 Start parameter (this value is 0.0 for a full ellipse)

// 42 End parameter (this value is 2pi for a full ellipse)

{
// HATCH
// The following group codes apply to hatch and MPolygon entities. In addition
// to the group codes described here, see gCommon Group Codes for Entitiesh
// on page 53. For information about abbreviations and formatting used in this
// table, see gFormatting Conventions in This Referenceh on page 2.
// Hatch group codes
// Group code Description
// 100 Subclass marker (AcDbHatch)

// 10 Elevation point (in OCS)
// DXF: X value = 0; APP: 3D point (X and Y always equal 0, Z represents the elevation)
// - 20, 30 DXF: Y and Z values of elevation point (in OCS)

// Y value = 0, Z represents the elevation
// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction

// 2 Hatch pattern name

// 70 Solid fill flag (solid fill = 1; pattern fill = 0); for MPolygon, the version of MPolygon

// 63 For MPolygon, pattern fill color as the ACI

// 71 Associativity flag (associative = 1; non-associative = 0); for MPolygon, solid-fill flag (has solid fill
// = 1; lacks solid fill = 0)

// 91 Number of boundary paths (loops)

// 9999 Boundary path data. Repeats number of times specified by code 91. See gBoundary Path Datah
// on page 72
// varies

// 75 Hatch style:
// - 0 = Hatch godd parityh area (Normal style)
// - 1 = Hatch outermost area only (Outer style)
// - 2 = Hatch through entire area (Ignore style)

// 76 Hatch pattern type:
// - 0 = User-defined; 1 = Predefined; 2 = Custom

// 52 Hatch pattern angle (pattern fill only)

// 41 Hatch pattern scale or spacing (pattern fill only)
// 73 For MPolygon, boundary annotation flag (boundary is an annotated boundary = 1; boundary is
// not an annotated boundary = 0)

// 77 Hatch pattern double flag (pattern fill only):
//  0 = not double; 1 = double

// 78 Number of pattern definition lines

// 9999 varies Pattern line data. Repeats number of times specified by code 78. See gPattern Datah on page 75
// 47 Pixel size used to determine the density to perform various intersection and ray casting operations
// in hatch pattern computation for associative hatches and hatches created with the Flood method
// of hatching

// 98 Number of seed points

// 11 For MPolygon, offset vector

// 99 For MPolygon, number of degenerate boundary paths (loops), where a degenerate boundary
// path is a border that is ignored by the hatch

// 10 Seed point (in OCS)
// DXF: X value; APP: 2D point (multiple entries)
// - 20 DXF: Y value of seed point (in OCS); (multiple entries)

// 450 Indicates solid hatch or gradient; if solid hatch, the values for the remaining codes are ignored
// but must be present. Optional; if code 450 is in the file, then the following codes must be in the
// file: 451, 452, 453, 460, 461, 462, and 470. If code 450 is not in the file, then the following
// codes must not be in the file: 451, 452, 453, 460, 461, 462, and 470
//  0 = Solid hatch
//  1 = Gradient

// 451 Zero is reserved for future use

// 452 Records how colors were defined and is used only by dialog code:
//  0 = Two-color gradient
//  1 = Single-color gradient

// 453 Number of colors:
//  0 = Solid hatch
//  2 = Gradient

// 460 Rotation angle in radians for gradients (default = 0, 0)

// 461 Gradient definition; corresponds to the Centered option on the Gradient Tab of the Boundary
// Hatch and Fill dialog box. Each gradient has two definitions, shifted and unshifted. A Shift value

// 462 describes the blend of the two definitions that should be used. A value of 0.0 means only the
// unshifted version should be used, and a value of 1.0 means that only the shifted version should
// be used.
// Color tint value used by dialog code (default = 0, 0; range is 0.0 to 1.0). The color tint value is a
// gradient color and controls the degree of tint in the dialog when the Hatch group code 452 is
// set to 1.

// 463 Reserved for future use:
// - 0 = First value
// - 1 = Second value

// 470 String (default = LINEAR)

// Boundary Path Data
// The boundary of each hatch object is defined by a path (or loop) that consists
// of one or more segments. Path segment data varies depending on the entity
// type (or types) that make up the path. Each path segment is defined by its
// own set of group codes. For information about abbreviations and formatting
// used in this table, see gFormatting Conventions in This Referenceh on page
// 2.

// 92 Boundary path type flag (bit coded):
//  0 = Default; 1 = External; 2 = Polyline
//  4 = Derived; 8 = Textbox; 16 = Outermost
// varies Polyline boundary type data (only if boundary = polyline). See Polyline boundary data table below

// 93 Number of edges in this boundary path (only if boundary is not a polyline)

// 72 Edge type (only if boundary is not a polyline):
//  1 = Line; 2 = Circular arc; 3 = Elliptic arc; 4 = Spline
// varies Edge type data (only if boundary is not a polyline). See appropriate Edge data table below

// 97 Number of source boundary objects

// 330 Reference to source boundary objects (multiple entries)
// Polyline boundary data group codes
// Group code Description

// 72 Has bulge flag

// 73 Is closed flag

// 93 Number of polyline vertices

// 10 Vertex location (in OCS)
// DXF: X value; APP: 2D point (multiple entries)
// - 20 DXF: Y value of vertex location (in OCS) (multiple entries)

// 42 Bulge (optional, default = 0)
// Line edge data group codes
// Group code Description
// 10 Start point (in OCS)
// DXF: X value; APP: 2D point
// - 20 DXF: Y value of start point (in OCS)

// 11 Endpoint (in OCS)
// DXF: X value; APP: 2D point
// - 21 DXF: Y value of endpoint (in OCS)

// Arc edge data group codes
// Group code Description
// 10 Center point (in OCS)
// DXF: X value; APP: 2D point
// - 20 DXF: Y value of center point (in OCS)

// 40 Radius

// 50 Start angle

// 51 End angle

// 73 Is counterclockwise flag
// Ellipse edge data group codes
// Group code Description

// 10 Center point (in OCS)
// DXF: X value; APP: 2D point
// - 20 DXF: Y value of center point (in OCS)

// 11 Endpoint of major axis relative to center point (in OCS)
// DXF: X value; APP: 2D point
// - 21 DXF: Y value of endpoint of major axis (in OCS)

// 40 Length of minor axis (percentage of major axis length)

// 50 Start angle

// 51 End angle

// 73 Is counterclockwise flag

// Spline edge data group codes
// Group code Description
// 94 Degree

// 73 Rational

// 74 Periodic

// 95 Number of knots

// 96 Number of control points

// 40 Knot values (multiple entries)

// 10 Control point (in OCS)
// DXF: X value; APP: 2D point
// - 20 DXF: Y value of control point (in OCS)

// 42 Weights (optional, default = 1)


// Pattern Data
// The following pattern data codes repeat for each pattern definition line. For
// information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Hatch pattern data group codes
// Group code Description
// 53 Pattern line angle

// 43 Pattern line base point, X component

// 44 Pattern line base point, Y component

// 45 Pattern line offset, X component

// 46 Pattern line offset, Y component

// 79 Number of dash length items

// 49 Dash length (multiple entries)

{
// HELIX
// The following group codes apply to helix entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Spline data
// 100 Subclass marker (AcDbHelix)

// 90 Major release number

// 91 Maintainance release number

// 10, 20, 30 Axis base point

// 11, 21, 31 Start point

// 12, 22, 32 Axis vector

// 40 Radius

// 41 Number of turns

// 42 Turn height

// 290 Handedness; 0 = left, 1 = right
// 280 Constrain type
//  0 = Constrain turn height
//  1 = Constrain turns
//  2 = Constrain height

// IMAGE
// The following group codes apply to image entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbRasterImage)

// 90 Class version

// 10 Insertion point (in WCS)
// DXF: X value; APP: 3D point
// 20, 30 DXF: Y and Z values of insertion point (in WCS)

// 11 U-vector of a single pixel (points along the visual bottom of the image, starting at the insertion
// point) (in WCS)
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values U-vector (in WCS)

// 12 V-vector of a single pixel (points along the visual left side of the image, starting at the insertion
// point) (in WCS)
// DXF: X value; APP: 3D point
// - 22, 32 DXF: Y and Z values of V-vector (in WCS)

// 13 Image size in pixels
// DXF: U value; APP: 2D point (U and V values)
// - 23 DXF: V value of image size in pixels

// 340 Hard reference to imagedef object

// 70 Image display properties:
//  1 = Show image
//  2 = Show image when not aligned with screen
//  4 = Use clipping boundary
//  8 = Transparency is on

// 280 Clipping state: 0 = Off; 1 = On

// 281 Brightness value (0-100; default = 50)

// 282 Contrast value (0-100; default = 50)

// 283 Fade value (0-100; default = 0)

// 360 Hard reference to imagedef_reactor object

// 71 Clipping boundary type. 1 = Rectangular; 2 = Polygonal

// 91 Number of clip boundary vertices that follow

// 14 Clip boundary vertex (in OCS)
// DXF: X value; APP: 2D point (multiple entries)
// NOTE 1) For rectangular clip boundary type, two opposite corners must be specified. Default is
// (-0.5,-0.5), (size.x-0.5, size.y-0.5). 2) For polygonal clip boundary type, three or more vertices
// must be specified. Polygonal vertices must be listed sequentially
// - 24 DXF: Y value of clip boundary vertex (in OCS) (multiple entries)

{
// INSERT
// The following group codes apply to insert (block reference) entities.
// 100 Subclass marker (AcDbBlockReference)

// 66 Variable attributes-follow flag (optional; default = 0); if the value of attributes-follow flag is 1, a
// series of attribute entities is expected to follow the insert, terminated by a seqend entity

// 2 Block name

// 10 Insertion point (in OCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of insertion point (in OCS)

// 41 X scale factor (optional; default = 1)

// 42 Y scale factor (optional; default = 1)

// 43 Z scale factor (optional; default = 1)

// 50 Rotation angle (optional; default = 0)

// 70 Column count (optional; default = 1)

// 71 Row count (optional; default = 1)

// 44 Column spacing (optional; default = 0)

// 45 Row spacing (optional; default = 0)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// LEADER
// The following group codes apply to leader entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Leader group codes
// Group code Description
// 100 Subclass marker (AcDbLeader)

// 3 Dimension style name

// 71 Arrowhead flag: 0 = Disabled; 1 = Enabled

// 72 Leader path type: 0 = Straight line segments; 1 = Spline

// 73 Leader creation flag (default = 3):
//  0 = Created with text annotation
//  1 = Created with tolerance annotation
//  2 = Created with block reference annotation
//  3 = Created without any annotation

// 74 Hookline direction flag:
//  0 = Hookline (or end of tangent for a splined leader) is the opposite direction from the horizontal
// vector
//  1 = Hookline (or end of tangent for a splined leader) is the same direction as horizontal vector
// (see code 75)

// 75 Hookline flag: 0 = No hookline; 1 = Has a hookline

// 40 Text annotation height

// 41 Text annotation width

// 76 Number of vertices in leader (ignored for OPEN)

// 10 Vertex coordinates (one entry for each vertex)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of vertex coordinates

// 77 Color to use if leader's DIMCLRD = BYBLOCK

// 340 Hard reference to associated annotation (mtext, tolerance, or insert entity)

// 210 Normal vector
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of normal vector

// 211 gHorizontalh direction for leader
// DXF: X value; APP: 3D vector
// - 221, 231 DXF: Y and Z values of ghorizontalh direction for leader

// 212 Offset of last leader vertex from block reference insertion point
// DXF: X value; APP: 3D vector
// - 222, 232 DXF: Y and Z values of offset

// 213 Offset of last leader vertex from annotation placement point
// DXF: X value; APP: 3D vector
// - 223, 233 DXF: Y and Z values of offset
// Xdata belonging to the application ID "ACAD" follows a leader entity if any
// dimension overrides have been applied to this entity. See "�gDimension Style
// Overrides"� on page 69.

{
// LIGHT
// The following group codes apply to light entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Light group codes
// Group code Description
// 100 Subclass marker (AcDbLight)

// 90 Version number

// 1 Light name

// 70 Light type (distant = 1; point = 2; spot = 3)

// 290 Status

// 291 Plot glyph

// 40 Intensity

// 10 Light Position
// DXF: X value; APP: 3D point
// - 20, 30 DXF: X, Y, and Z values of the light position

// 11 Target location
// DXF: X value; APP: 3D point
// - 21, 31 DXF: X, Y, and Z values of the target location

// 72 Attenuation type
//  0 = None
//  1 = Inverse Linear
//  2 = Inverse Square

// 292 Use attenuation limits

// 41 Attenuation start limit

// 42 Attenuation end limit

// 50 Hotspot angle

// 51 Falloff angle

// 293 Cast shadows

// 73 Shadow Type
//  0 = Ray traced shadows
//  1 = Shadow maps

// 91 Shadow map size

// 280 Shadow map softness

{
// LINE
// The following group codes apply to line entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Line group codes
// Group code Description
// 100 Subclass marker (AcDbLine)

// 39 Thickness (optional; default = 0)

// 10 Start point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of start point (in WCS)

// 11 Endpoint (in WCS)
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of endpoint (in WCS)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// LWPOLYLINE
// The following group codes apply to lwpolyline entities. In addition to the
// group codes described here, see gCommon Group Codes for Entitiesh on page
// 53. For information about abbreviations and formatting used in this table,
// see gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbPolyline)

// 90 Number of vertices

// 70 Polyline flag (bit-coded); default is 0:
//  1 = Closed; 128 = Plinegen

// 43 Constant width (optional; default = 0). Not used if variable width (codes 40 and/or 41) is set

// 38 Elevation (optional; default = 0)

// 39 Thickness (optional; default = 0)

// 10 Vertex coordinates (in OCS), multiple entries; one entry for each vertex
// DXF: X value; APP: 2D point
// - 20 DXF: Y value of vertex coordinates (in OCS), multiple entries; one entry for each vertex

// 40 Starting width (multiple entries; one entry for each vertex) (optional; default = 0; multiple entries).
// Not used if constant width (code 43) is set

// 41 End width (multiple entries; one entry for each vertex) (optional; default = 0; multiple entries).
// Not used if constant width (code 43) is set

// 42 Bulge (multiple entries; one entry for each vertex) (optional; default = 0)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

{
// MLINE
// The following group codes apply to mline entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Mline group codes
// Group code Description
// 100 Subclass marker (AcDbMline)
// 2 String of up to 32 characters. The name of the style used for this mline. An entry for this style
// must exist in the MLINESTYLE dictionary.
// Do not modify this field without also updating the associated entry in the MLINESTYLE dictionary

// 340 Pointer-handle/ID of MLINESTYLE object

// 40 Scale factor

// 70 Justification: 0 = Top; 1 = Zero; 2 = Bottom

// 71 Flags (bit-coded values):
//  1 = Has at least one vertex (code 72 is greater than 0)
//  2 = Closed
//  4 = Suppress start caps
//  8 = Suppress end caps

// 72 Number of vertices

// 73 Number of elements in MLINESTYLE definition

// 10 Start point (in WCS)
// DXF: X value; APP: 3D point
// -20, 30 DXF: Y and Z values of start point (in WCS)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 11 Vertex coordinates (multiple entries; one entry for each vertex) DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of vertex coordinates

// 12 Direction vector of segment starting at this vertex (multiple entries; one for each vertex)
// DXF: X value; APP: 3D vector
// - 22, 32 DXF: Y and Z values of direction vector of segment starting at this vertex

// 13 Direction vector of miter at this vertex (multiple entries: one for each vertex)
// DXF: X value; APP: 3D vector
// - 23, 33 DXF: Y and Z values of direction vector of miter

// 74 Number of parameters for this element (repeats for each element in segment)

// 41 Element parameters (repeats based on previous code 74)

// 75 Number of area fill parameters for this element (repeats for each element in segment)

// 42 Area fill parameters (repeats based on previous code 75)

// The group code 41 parameterization is a list of real values, one real per group
// code 41. The list may contain zero or more items. The first group code 41
// value is the distance from the segment vertex along the miter vector to the
// point where the line element's path intersects the miter vector. The next group
// code 41 value is the distance along the line element's path from the point
// defined by the first group 41 to the actual start of the line element. The next
// is the distance from the start of the line element to the first break (or cut) in
// the line element. The successive group code 41 values continue to list the start
// and stop points of the line element in this segment of the mline. Linetypes
// do not affect group 41 lists.
// The group code 42 parameterization is also a list of real values. Similar to the
// -41 parameterization, it describes the parameterization of the fill area for this
// mline segment. The values are interpreted identically to the 41 parameters
// and when taken as a whole for all line elements in the mline segment, they
// define the boundary of the fill area for the mline segment.
// A common example of the use of the group code 42 mechanism is when an
// unfilled mline crosses over a filled mline and mledit is used to cause the filled
// mline to appear unfilled in the crossing area. This would result in two group
// -42s for each line element in the affected mline segment; one for the fill stop
// and one for the fill start.
// The 2 group codes in mline entities and mlinestyle objects are redundant
// fields. These groups should not be modified under any circumstances, although
// it is safe to read them and use their values. The correct fields to modify are as
// follows:
// Mline
// The 340 group in the same object, which indicates the proper MLINESTYLE
// object.
// Mlinestyle
// The 3 group value in the MLINESTYLE dictionary, which precedes the 350
// group that has the handle or entity name of the current mlinestyle.

{
// MTEXT
// The following group codes apply to mtext entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbMText)

// 10 Insertion point
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of insertion point

// 40 Nominal (initial) text height

// 41 Reference rectangle width

// 71 Attachment point:
// - 1 = Top left; 2 = Top center; 3 = Top right
// - 4 = Middle left; 5 = Middle center; 6 = Middle right
// - 7 = Bottom left; 8 = Bottom center; 9 = Bottom right

// 72 Drawing direction:
// - 1 = Left to right
// - 3 = Top to bottom
// - 5 = By style (the flow direction is inherited from the associated text style)
// 1 Text string. If the text string is less than 250 characters, all characters appear in group 1. If the
// text string is greater than 250 characters, the string is divided into 250-character chunks, which
// appear in one or more group 3 codes. If group 3 codes are used, the last group is a group 1 and
// has fewer than 250 characters

// 3 Additional text (always in 250-character chunks) (optional)

// 7 Text style name (STANDARD if not provided) (optional)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 11 X-axis direction vector (in WCS)
// DXF: X value; APP: 3D vector
// A group code 50 (rotation angle in radians) passed as DXF input is converted to the equivalent
// direction vector (if both a code 50 and codes 11, 21, 31 are passed, the last one wins). This is
// provided as a convenience for conversions from text objects
// - 21, 31 DXF: Y and Z values of X-axis direction vector (in WCS)

// 42 Horizontal width of the characters that make up the mtext entity. This value will always be equal
// to or less than the value of group code 41 (read-only, ignored if supplied)

// 43 Vertical height of the mtext entity (read-only, ignored if supplied)

// 50 Rotation angle in radians

// 73 Mtext line spacing style (optional):
//  1 = At least (taller characters will override)
//  2 = Exact (taller characters will not override)

// 44 Mtext line spacing factor (optional):
// Percentage of default (3-on-5) line spacing to be applied. Valid values range from 0.25 to 4.00

// 90 Background fill setting:
//  0 = Background fill off
//  1 = Use background fill color
//  2 = Use drawing window color as background fill color

// 63 Background color (if color index number)

// 420 - 429 Background color (if RGB color)

// 430 - 439 Background color (if color name)

// 45 Fill box scale (optional):
// Determines how much border there is around the text.

// 63 Background fill color (optional):
// Color to use for background fill when group code 90 is 1.

// 441 Transparency of background fill color (not implemented)

// Xdata with the "DCO15" application ID may follow an mtext entity. This
// contains information related to the dbConnect feature.

{
// OLEFRAME
// The following group codes apply to oleframe entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Oleframe group codes
// Group code Description
// 100 Subclass marker (AcDbOleFrame)

// 70 OLE version number

// 90 Length of binary data

// 310 Binary data (multiple lines)

// 1 End of OLE data (the string "OLE"�)

{
// OLE2FRAME
// The following group codes apply to ole2frame entities. This information is
// read-only. During OPEN, the values are ignored because they are part of the
// OLE binary object, and are obtained via access functions. In addition to the
// group codes described here, see gCommon Group Codes for Entitiesh on page
// 53. For information about abbreviations and formatting used in this table,
// see gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbOle2Frame)

// 70 OLE version number

// 3 Length of binary data

// 10 Upper-left corner (WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of upper-left corner (in WCS)

// 11 Lower-right corner (WCS)
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of lower-right corner (in WCS)

// 71 OLE object type, 1 = Link; 2 = Embedded; 3 = Static

// 72 Tile mode descriptor:
//  0 = Object resides in model space
//  1 = Object resides in paper space

// 90 Length of binary data

// 310 Binary data (multiple lines)

// 1 End of OLE data (the string "OLE")

// - Sample DXF output:
// - OLE2FRAME
// - 5
// - 2D
// - 100
// - AcDbEntity
// - 67
// - 180
// - OLE2FRAME | 87
// - 100
// - AcDbOle2Frame
// - 70
// - 23
// - Paintbrush Picture
// - 10
// - 4.43116
// - 20
// - 5.665992
// - 30
// - 0.0
// - 11
// - 6.4188
// - 21
// - 4.244939
// - 31
// - 0.0
// - 71
// - 2
// - 72
// - 1
// - 90
// - 23680
// - 310 0155764BD60082B91140114B08C8F9A916400000000000000000506DC0D0D9AC
// - 310
// - 1940114B08C8F9A916400000000000000000506DC0D0D9AC194002303E5CD1FA
// - 310
// - 10400000000000000000764BD60082B9114002303E5CD1FA1040000000000000
// - ...
// - ...
// - AutoLISP entnext function sample output:
// - Command: (setq e (entget e3))
// - ((-1 . <Entity name: 7d50428>) (0 . "OLE2FRAME") (5 . "2D")
// - (100 . "AcDbEntity") (67 . 1) (8 . "0") (100 . "AcDbOle2Frame")
// - (70 . 2) (3 "Paintbrush Picture") (10 4.43116 5.66599 0.0)
// - (11 6.4188 4.24494 0.0) (71 . 2) (72 . 1))

{
// POINT
// The following group codes apply to point entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbPoint)

// 10 Point location (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of point location (in WCS)
// 39 Thickness (optional; default = 0)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 50 Angle of the X axis for the UCS in effect when the point was drawn (optional, default = 0); used
// when PDMODE is nonzero

{
// POLYLINE
// The following group codes apply to polyline entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Polyline group codes
// Group code Description
// 100 Subclass marker (AcDb2dPolyline or AcDb3dPolyline)

// 66 Obsolete; formerly an gentities follow flagh (optional; ignore if present)

// 10 DXF: always 0
// APP: a gdummyh point; the X and Y values are always 0, and the Z value is the polyline's
// elevation (in OCS when 2D, WCS when 3D)
// - 20 DXF: always 0
// - 30 DXF: polyline's elevation (in OCS when 2D; WCS when 3D)

// 39 Thickness (optional; default = 0)

// 70 Polyline flag (bit-coded; default = 0):
//  1 = This is a closed polyline (or a polygon mesh closed in the M direction)
//  2 = Curve-fit vertices have been added
//  4 = Spline-fit vertices have been added
//  8 = This is a 3D polyline
// 16 = This is a 3D polygon mesh
// 32 = The polygon mesh is closed in the N direction
// 64 = The polyline is a polyface mesh
// 128 = The linetype pattern is generated continuously around the vertices of this polyline

// 40 Default start width (optional; default = 0)

// 41 Default end width (optional; default = 0)

// 71 Polygon mesh M vertex count (optional; default = 0)

// 72 Polygon mesh N vertex count (optional; default = 0)

// 73 Smooth surface M density (optional; default = 0)

// 74 Smooth surface N density (optional; default = 0)

// 75 Curves and smooth surface type (optional; default = 0); integer codes, not bit-coded:
//  0 = No smooth surface fitted
//  5 = Quadratic B-spline surface
//  6 = Cubic B-spline surface
//  8 = Bezier surface

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// Xdata with the "AUTOCAD_POSTSCRIPT_FIGURE" application ID may follow a
// polyline entity. This contains information related to PostScript images and
// PostScript fill information.
// Polyface Meshes
// A polyface mesh is represented in DXF as a variant of a polyline entity. The
// polyline header is identified as introducing a polyface mesh by the presence
// of the 64 bit in the polyline flags (70) group. The 71 group specifies the number
// of vertices in the mesh, and the 72 group specifies the number of faces.
// Although these counts are correct for all meshes created with the PFACE
// command, applications are not required to place correct values in these fields.
// Following the polyline header is a sequence of vertex entities that specify the
// vertex coordinates, followed by faces that compose the mesh.
// The AutoCAD entity structure imposes a limit on the number of vertices that
// a given face entity can specify. You can represent more complex polygons by
// decomposing them into triangular wedges. Their edges should be made
// 90 | Chapter 6 ENTITIES Section
// invisible to prevent visible artifacts of this subdivision from being drawn. The
// PFACE command performs this subdivision automatically, but when
// applications generate polyface meshes directly, the applications must do this
// themselves. The number of vertices per face is the key parameter in this
// subdivision process. The PFACEVMAX system variable provides an application
// with the number of vertices per face entity. This value is read-only and is set
// to 4.
// Polyface meshes created with the PFACE command are always generated with
// all the vertex coordinate entities first, followed by the face definition entities.
// The code within AutoCAD that processes polyface meshes requires this
// ordering. Programs that generate polyface meshes in DXF should generate all
// the vertices, and then all the faces. However, programs that read polyface
// meshes from DXF should be tolerant of odd vertex and face ordering.

{
// RAY
// The following group codes apply to ray entities. In addition to the group codes
// described here, see gCommon Group Codes for Entitiesh on page 53. For
// information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Ray group codes
// Group code Description
// 100 Subclass marker (AcDbRay)

// 10 Start point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of start point (in WCS)

// 11 Unit direction vector (in WCS)
// DXF: X value; APP: 3D vector
// - 21, 31 DXF: Y and Z values of unit direction vector (in WCS)

// REGION
// The following group codes apply to region entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Region group codes
// Group code Description
// 100 Subclass marker (AcDbModelerGeometry)

// 70 Modeler format version number (currently = 1)

// 1 Proprietary data (multiple lines < 255 characters each)
// 3 Additional lines of proprietary data (if previous group 1 string is greater than 255 characters)
// (optional)

{
// SECTION
// The following group codes apply to section entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Section group codes
// Group code Description
// 100 Subclass marker (AcDbSection)

// 90 Section state

// 91 Section flags

// 1 Name

// 10, 20, 30 Vertical direction

// 40 Top height

// 41 Bottom height

// 70 Indicator transparency

// 63, 411 Indicator color

// 92 Number of vertices

// 11, 21, 31 Vertex (repeats for number of vertices)

// 93 Number of back line vertices

// 12, 22, 32 Back line vertex (repeats for number of back line vertices)

// 360 Hard-pointer ID/handle to geometry settings object

// 92 | Chapter 6 ENTITIES Section

// SEQEND
// The following group codes apply to seqend entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// - -2 APP: name of entity that began the sequence. This entity marks the end of vertex (vertex
// type name) for a polyline, or the end of attribute entities (attrib type name) for an insert
// entity that has attributes (indicated by 66 group present and nonzero in insert entity). This
// code is not saved in a DXF file

{
// SHAPE
// The following group codes apply to shape entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbShape)

// 39 Thickness (optional; default = 0)

// 10 Insertion point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of insertion point (in WCS)

// 40 Size

// 2 Shape name

// 50 Rotation angle (optional; default = 0)

// 41 Relative X scale factor (optional; default = 1)

// 51 Oblique angle (optional; default = 0)
// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// -220, 230 DXF: Y and Z values of extrusion direction (optional)

{
// SOLID
// The following group codes apply to solid entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbTrace)

// 10 First corner
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of first corner

// 11 Second corner
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of second corner

// 12 Third corner
// XF: X value; APP: 3D point
// - 22, 32 DXF: Y and Z values of third corner

// 13 Fourth corner. If only three corners are entered to define the SOLID, then the fourth corner
// coordinate is the same as the third.
// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of fourth corner

// 39 Thickness (optional; default = 0)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

{
// SPLINE
// The following group codes apply to spline entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbSpline)

// 210 Normal vector (omitted if the spline is nonplanar)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of normal vector (optional)

// 70 Spline flag (bit coded):
//  1 = Closed spline
//  2 = Periodic spline
//  4 = Rational spline
//  8 = Planar
//  16 = Linear (planar bit is also set)

// 71 Degree of the spline curve

// 72 Number of knots

// 73 Number of control points

// 74 Number of fit points (if any)

// 42 Knot tolerance (default = 0.0000001)

// 43 Control-point tolerance (default = 0.0000001)

// 44 Fit tolerance (default = 0.0000000001)

// 12 Start tangent.may be omitted (in WCS)
// DXF: X value; APP: 3D point
// - 22, 32 DXF: Y and Z values of start tangent.may be omitted (in WCS)

// 13 End tangent.may be omitted (in WCS)
// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of end tangent.may be omitted (in WCS)

// 40 Knot value (one entry per knot)

// 41 Weight (if not 1); with multiple group pairs, they are present if all are not 1

// 10 Control points (in WCS); one entry per control point
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of control points (in WCS); one entry per control point

// 11 Fit points (in WCS); one entry per fit point
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of fit points (in WCS); one entry per fit point

// SUN
// The following group codes apply to the sun entity. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbSun)

// 90 Version number

// 290 Status

// 63 Color

// 40 Intensity

// 291 Shadows

// 91 Julian day

// 92 Time (in seconds past midnight)

// 292 Daylight savings time

// 70 Shadow type
//  0 = Ray traced shadows
//  1 = Shadow maps

// 71 Shadow map size

// 280 Shadow softness

{
// SURFACE
// Surface entity definitions consist of group codes that are common to all surface
// types, followed by codes specific to the type.
// 100 Subclass marker (AcDbModelerGeometry)

// 70 Modeler format version number (currently = 1)

// 1 Proprietary data (multiple lines < 255 characters each)
// 3 Additional lines of proprietary data (if previous group 1 string is greater than 255 characters)
// (optional)

// 100 Subclass markar (AcDbSurface)

// 71 Number of U isolines

// 72 Number of V isolines

// Extruded Surface
// The following group codes apply to extruded surfaces. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Extruded Surface group codes
// Group code Description
// 100 Subclass markar (AcDbExtrudedSurface)

// 90 Class ID

// 90 Size of binary data

// 310 Binary data

// 10, 20, 30 Sweep vector

// 40 Transform matrix of extruded entity (16 reals; row major format; default = identity matrix)

// 42 Draft angle (in radians)

// 43 Draft start distance

// 44 Draft end distance

// 45 Twist angle

// 48 Scale factor

// 49 Align angle (in radians)

// 46 Transform matrix of sweep entity (16 reals; row major format; default = identity matrix)

// 47 Transform matrix of path entity (16 reals; row major format; default = identity matrix)

// 290 Solid flag

// 70 Sweep alignment option
//  0 = No alignment
//  1 = Align sweep entity to path
//  2 = Translate sweep entity to path
//  3 = Translate path to sweep entity

// 292 Align start flag

// 293 Bank flag

// 294 Base point set flag

// 295 Sweep entity transform computed flag

// 296 Path entity transform computed flag

// 11, 21, 31 Reference vector for controlling twist

// Lofted Surface
// The following group codes apply to lofted surfaces. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbLoftedSurface)

// 40 Transform matrix of loft entity (16 reals; row major format; default = identity matrix)
// Entity data for cross sections
// Entity data for guide curves
// Entity data for path curves

// 70 Plane normal lofting type

// 41 Start draft angle (in radians)

// 42 End draft angle (in radians)

// 43 Start draft magnitude

// 44 End draft magnitude

// 290 Arc length parametrization flag

// 291 No twist flag

// 292 Align direction flag

// 293 Create simple surfaces flag

// 294 Create closed surface flag

// 295 Solid flag

// 296 Create ruled surface flag

// 297 Virtual guide flag

// Revolved Surface
// The following group codes apply to revolved surfaces. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass markar (AcDbRevolvedSurface)

// 90 ID of revolve entity

// 90 Size of binary data

// 310 Binary data

// 10, 20, 30 Axis point

// 11, 21, 31 Axis vector

// 40 Revolve angle (in radians)

// 41 Start angle (in radians)

// 42 Transform matrix of revolved entity (16 reals; row major format; default = identity matrix)

// 43 Draft angle (in radians)

// 44 Start draft distance

// 45 End draft distance

// 46 Twist angle (in radians)

// 290 Solid flag

// 291 Close to axis flag

// Swept Surface
// The following group codes apply to swept surfaces. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass markar (AcDbSweptSurface)

// 90 ID of sweep entity

// 90 Size of binary data

// 310 Binary data

// 90 ID of path entity

// 90 Size of binary data

// 310 Proprietary data

// 40 Transform matrix of sweep entity (16 reals; row major format; default = identity matrix)

// 41 Transform matrix of path entity (16 reals; row major format; default = identity matrix)

// 42 Draft angle (in radians)

// 43 Draft start distance

// 44 Draft end distance

// 45 Twist angle

// 48 Scale factor

// 49 Align angle (in radians)

// 46 Transform matrix of sweep entity (16 reals; row major format; default = identity matrix)

// 47 Transform matrix of path entity (16 reals; row major format; default = identity matrix)

// 290 Solid flag

// 70 Sweep alignment option
//  0 = No alignment
//  1 = Align sweep entity to path
//  2 = Translate sweep entity to path
//  3 = Translate path to sweep entity

// 292 Align start flag

// 293 Bank flag

// 294 Base point set flag

// 295 Sweep entity transform computed flag

// 296 Path entity transform computed flag

// 11, 21, 31 Reference vector for controlling twist

{
// TABLE
// The following group codes apply to table entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 0 Entity name (ACAD_TABLE)

// 5 Entity handle

// 330 Soft-pointer ID to the owner dictionary
// 100 Subclass marker. There are three subclass markers, in the following order: AcDbEntity,
// AcDbBlockReference, AcDbTable

// 92 Number of bytes in the proxy entity graphics

// 310 Data for proxy entity graphics (multiple lines; 256-character maximum per line)

// 2 Block name; an anonymous block begins with a *T value

// 10,20,30 Insertion point

// 342 Hard pointer ID of the TABLESTYLE object

// 343 Hard pointer ID of the owning BLOCK record

// 11,21,31 Horizontal direction vector

// 90 Flag for table value (unsigned integer)

// 91 Number of rows

// 92 Number of columns

// 93 Flag for an override

// 94 Flag for an override of border color

// 95 Flag for an override of border lineweight

// 96 Flag for an override of border visibility

// 141 Row height; this value is repeated, 1 value per row

// 142 Column height; this value is repeated, 1 value per row

// 171 Cell type; this value is repeated, 1 value per cell:
//  1 = text type
//  2 = block type

// 172 Cell flag value; this value is repeated, 1 value per cell

// 173 Cell merged value; this value is repeated, 1 value per cell
// 174 Boolean flag indicating if the autofit option is set for the cell; this value is repeated, 1 value per
// cell

// 175 Cell border width (applicable only for merged cells); this value is repeated, 1 value per cell

// 176 Cell border height ( applicable for merged cells); this value is repeated, 1 value per cell

// 177 Cell override flag; this value is repeated, 1 value per cell

// 178 Flag value for a virtual edge

// 145 Rotation value (real; applicable for a block-type cell and a text-type cell)

// 344 Hard pointer ID of the FIELD object. This applies only to a text-type cell. If the text in the cell
// contains one or more fields, only the ID of the FIELD object is saved. The text string (group codes
// - 1 and 3) is ignored

// 1 Text string in a cell. If the string is shorter than 250 characters, all characters appear in code 1.
// If the string is longer than 250 characters, it is divided into chunks of 250 characters. The chunks
// are contained in one or more code 3 codes. If code 3 codes are used, the last group is a code 1
// and is shorter than 250 characters. This value applies only to text-type cells and is repeated, 1
// value per cell

// 3 Text string in a cell, in 250-character chunks; optional. This value applies only to text-type cells
// and is repeated, 1 value per cell

// 340 Hard-pointer ID of the block table record. This value applies only to block-type cells and is repeated,
// 1 value per cell

// 144 Block scale (real). This value applies only to block-type cells and is repeated, 1 value per cell

// 179 Number of attribute definitions in the block table record (applicable only to a block-type cell)

// 331 Soft pointer ID of the attribute definition in the block table record, referenced by group code
// - 179 (applicable only for a block-type cell). This value is repeated once per attribute definition

// 300 Text string value for an attribute definition, repeated once per attribute definition and applicable
// only for a block-type cell

// 7 Text style name (string); override applied at the cell level

// 140 Text height value; override applied at the cell level

// 170 Cell alignment value; override applied at the cell level

// 64 Value for the color of cell content; override applied at the cell level

// 63 Value for the background (fill) color of cell content; override applied at the cell level

// 69 True color value for the top border of the cell; override applied at the cell level

// 65 True color value for the right border of the cell; override applied at the cell level

// 66 True color value for the bottom border of the cell; override applied at the cell level

// 68 True color value for the left border of the cell; override applied at the cell level

// 279 Lineweight for the top border of the cell; override applied at the cell level

// 275 Lineweight for the right border of the cell; override applied at the cell level

// 276 Lineweight for the bottom border of the cell; override applied at the cell level

// 278 Lineweight for the left border of the cell; override applied at the cell level

// 283 Boolean flag for whether the fill color is on; override applied at the cell level

// 289 Boolean flag for the visibility of the top border of the cell; override applied at the cell level

// 285 Boolean flag for the visibility of the right border of the cell; override applied at the cell level

// 286 Boolean flag for the visibility of the bottom border of the cell; override applied at the cell level

// 288 Boolean flag for the visibility of the left border of the cell; override applied at the cell level

// 70 Flow direction; override applied at the table entity level

// 40 Horizontal cell margin; override applied at the table entity level

// 41 Vertical cell margin; override applied at the table entity level

// 280 Flag for whether the title is suppressed; override applied at the table entity level

// 281 Flag for whether the header row is suppressed; override applied at the table entity level

// 7 Text style name (string); override applied at the table entity level. There may be one entry for
// each cell type

// 140 Text height (real); override applied at the table entity level. There may be one entry for each cell
// type

// 170 Cell alignment (integer); override applied at the table entity level. There may be one entry for
// each cell type

// 63 Color value for cell background or for the vertical, left border of the table; override applied at the
// table entity level. There may be one entry for each cell type

// 64 Color value for cell content or for the horizontal, top border of the table; override applied at the
// table entity level. There may be one entry for each cell type

// 65 Color value for the horizontal, inside border lines; override applied at the table entity level

// 66 Color value for the horizontal, bottom border lines; override applied at the table entity level

// 68 Color value for the vertical, inside border lines; override applied at the table entity level

// 69 Color value for the vertical, right border lines; override applied at the table entity level
// 283 Flag for whether background color is enabled (default = 0); override applied at the table entity
// level. There may be one entry for each cell type:
//  0 = Disabled
//  1 = Enabled
// * 274-279 Lineweight for each border type of the cell (default = kLnWtByBlock); override applied at the
// table entity level. There may be one group for each cell type

// 284-289 Flag for visibility of each border type of the cell (default = 1); override applied at the table entity
// level. There may be one group for each cell type:
//  0 = Invisible
//  1 = Visible

// Group code 178 is a flag value for a virtual edge. A virtual edge is used when
// a grid line is shared by two cells. For example, if a table contains one row and
// two columns and it contains cell A and cell B, the central grid line contains
// the right edge of cell A and the left edge of cell B. One edge is real, and the
// other edge is virtual. The virtual edge points to the real edge; both edges have
// the same set of properties, including color, lineweight, and visibility.

{
// TEXT
// The following group codes apply to text entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// 100 Subclass marker (AcDbText)

// 39 Thickness (optional; default = 0)

// 10 First alignment point (in OCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of first alignment point (in OCS)

// 40 Text height

// 1 Default value (the string itself)

// 50 Text rotation (optional; default = 0)

// 41 Relative X scale factor.width (optional; default = 1)
// This value is also adjusted when fit-type text is used

// 51 Oblique angle (optional; default = 0)

// 7 Text style name (optional, default = STANDARD)

// 71 Text generation flags (optional, default = 0):
//  2 = Text is backward (mirrored in X)
//  4 = Text is upside down (mirrored in Y)

// 72 Horizontal text justification type (optional, default = 0) integer codes (not bit-coded)
//  0 = Left; 1= Center; 2 = Right
//  3 = Aligned (if vertical alignment = 0)
//  4 = Middle (if vertical alignment = 0)
//  5 = Fit (if vertical alignment = 0)
// See the Group 72 and 73 integer codes table for clarification

// 11 Second alignment point (in OCS) (optional)
// DXF: X value; APP: 3D point
// This value is meaningful only if the value of a 72 or 73 group is nonzero (if the justification is
// anything other than baseline/left)
// - 21, 31 DXF: Y and Z values of second alignment point (in OCS) (optional)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 100 Subclass marker (AcDbText)

// 73 Vertical text justification type (optional, default = 0): integer codes (not bit-coded):
// 0 = Baseline; 1 = Bottom; 2 = Middle; 3 = Top
// See the Group 72 and 73 integer codes table for clarification
// The following table describes the group codes 72 (horizontal alignment) and

// 73 (vertical alignment) in greater detail.

// If group 72 and/or 73 values are nonzero then the first alignment point values
// are ignored and AutoCAD calculates new values based on the second alignment
// point and the length and height of the text string itself (after applying the
// text style). If the 72 and 73 values are zero or missing, then the second
// alignment point is meaningless.

{
// TOLERANCE
// The following group codes apply to tolerance entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Tolerance group codes
// Group code Description
// 100 Subclass marker (AcDbFcf)

// 3 Dimension style name

// 10 Insertion point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of insertion point (in WCS)

// 1 String representing the visual representation of the tolerance

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

// 11 X-axis direction vector (in WCS)
// DXF: X value; APP: 3D vector
// - 21, 31 DXF: Y and Z values of X-axis direction vector (in WCS)

// The following group codes apply to trace entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// 100 Subclass marker (AcDbTrace)

// 10 First corner (in OCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of first corner (in OCS)

// 11 Second corner (in OCS)
// DXF: X value; APP: 3D point
// - 21, 31 DXF: Y and Z values of second corner (in OCS)

// 12 Third corner (in OCS)
// DXF: X value; APP: 3D point
// - 22, 32 DXF: Y and Z values of third corner (in OCS)

// 13 Fourth corner (in OCS)
// DXF: X value; APP: 3D point
// - 23, 33 DXF: Y and Z values of fourth corner (in OCS)

// 39 Thickness (optional; default = 0)

// 210 Extrusion direction (optional; default = 0, 0, 1)
// DXF: X value; APP: 3D vector
// - 220, 230 DXF: Y and Z values of extrusion direction (optional)

{
// UNDERLAY
// The following group codes apply to underlays. In addition to the group codes
// described here, see gCommon Group Codes for Entitiesh on page 53. For
// information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
/// 100 Subclass marker (AcDbUnderlayReference)

// 340 The ID of the AcDbUnderlayDefinition object
// The X,Y, and Z coordinates of the insertion point of the underlay. These
// are OCS/ECS coordinates

// 10,20,30

// 41,42,43 DXF: X, Y, and Z scale factors

// 50 Rotation Angle (in OCS/ECS. CCW from the coordinate system X axis
// and around the Z axis)

// 210,220,230 Normal vector (in WCS)

// 280 Flags
//  1 = Clipping is on
//  2 = Underlay is on
//  4 = Monochrome
//  8 = Adjust for background

// 281 Contrast (value between 20 and 100)

// 282 Fade (value between 0 and 80)

// 11, 21 Repeating: 2d points in OCS/ECS. If only two, then they are the lower
//  left and upper right corner points of a clip rectangle. If more than two,
//  then they are the vertices of a clipping polygon

// VERTEX
// The following group codes apply to vertex entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// 100 Subclass marker (AcDbVertex)
// 100 Subclass marker (AcDb2dVertex or AcDb3dPolylineVertex)

// 10 Location point (in OCS when 2D, and WCS when 3D)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of location point (in OCS when 2D, and WCS when 3D)

// 40 Starting width (optional; default is 0)

// 41 Ending width (optional; default is 0)
// 42 Bulge (optional; default is 0). The bulge is the tangent of one fourth the included angle for an
// arc segment, made negative if the arc goes clockwise from the start point to the endpoint. A
// bulge of 0 indicates a straight segment, and a bulge of 1 is a semicircle

// 70 Vertex flags:
//  1 = Extra vertex created by curve-fitting
//  2 = Curve-fit tangent defined for this vertex. A curve-fit tangent direction of 0 may be omitted
//  from DXF output but is significant if this bit is set
//  4 = Not used
//  8 = Spline vertex created by spline-fitting
//  16 = Spline frame control point
//  32 = 3D polyline vertex
//  64 = 3D polygon mesh
//  128 = Polyface mesh vertex

// 50 Curve fit tangent direction

// 71 Polyface mesh vertex index (optional; present only if nonzero)

// 72 Polyface mesh vertex index (optional; present only if nonzero)

// 73 Polyface mesh vertex index (optional; present only if nonzero)

// 74 Polyface mesh vertex index (optional; present only if nonzero)

// Every vertex that is part of a polyface mesh has its vertex flag 128 bit set. If
// the entity supplies the coordinate of a vertex of the mesh, its 64 bit is set as
// well, and the 10, 20, 30 groups give the vertex coordinate. The vertex index
// values are determined by the order in which the vertex entities appear within
// the polyline, with the first being numbered 1.
// If the vertex defines a face of the mesh, its vertex flags group has the 128 bit
// set but not the 64 bit. In this case, the 10, 20, 30 (location) groups of the face
// entity are irrelevant and are always written as 0 in a DXF file. The vertex
// indexes that define the mesh are given by 71, 72, 73, and 74 group codes, the
// values of which specify one of the previously defined vertexes by index. If the
// index is negative, the edge that begins with that vertex is invisible. The first 0
// vertex marks the end of the vertices of the face.

{
// VIEWPORT
// The following group codes apply to viewport entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// For information about abbreviations and formatting used in this table, see
// gFormatting Conventions in This Referenceh on page 2.
// Viewport group codes
// Group code Description
// 100 Subclass marker (AcDbViewport)

// 10 Center point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of center point (in WCS)

// 40 Width in paper space units

// 41 Height in paper space units

// 68 Viewport status field:

//  -1 = On, but is fully off screen, or is one of the viewports that is not active because the $MAXACTVP
// count is currently being exceeded.
//  0 = Off
//  <positive value > = On and active. The value indicates the order of stacking for the viewports,
//  where 1 is the active viewport, 2 is the next, and so forth

// 69 Viewport ID

// 12 View center point (in DCS)
// DXF: X value; APP: 2D point
// - 22 DXF: View center point Y value (in DCS)

// 13 Snap base point
//  DXF: X value; APP: 2D point
//  23 DXF: Snap base point Y value

// 14 Snap spacing
//  DXF: X value; APP: 2D point
//  24 DXF: Snap spacing Y value

// 15 Grid spacing
//  DXF: X value; APP: 2D point
//  25 DXF: Grid spacing Y value

// 16 View direction vector (in WCS)
// DXF: X value; APP: 3D vector
//  26, 36 DXF: Y and Z values of view direction vector (in WCS)

// 17 View target point (in WCS)
// DXF: X value; APP: 3D vector
//  27, 37 DXF: Y and Z values of view target point (in WCS)

// 42 Perspective lens length

// 43 Front clip plane Z value

// 44 Back clip plane Z value

// 45 View height (in model space units)

// 50 Snap angle

// 51 View twist angle

// 72 Circle zoom percent

// 331 Frozen layer object ID/handle (multiple entries may exist) (optional)

// 90 Viewport status bit-coded flags:
//  1 (0x1) = Enables perspective mode
//  2 (0x2) = Enables front clipping
//  4 (0x4) = Enables back clipping
//  8 (0x8) = Enables UCS follow
//  16 (0x10) = Enables front clip not at eye
//  32 (0x20) = Enables UCS icon visibility
//  64 (0x40) = Enables UCS icon at origin
//  128 (0x80) = Enables fast zoom
//  256 (0x100) = Enables snap mode
//  512 (0x200) = Enables grid mode
//  1024 (0x400) = Enables isometric snap style
//  2048 (0x800) = Enables hide plot mode
//  4096 (0x1000) = kIsoPairTop. If set and kIsoPairRight is not set, then isopair top is enabled. If
//  both kIsoPairTop and kIsoPairRight are set, then isopair left is enabled
//  8192 (0x2000) = kIsoPairRight. If set and kIsoPairTop is not set, then isopair right is enabled
//  16384 (0x4000) = Enables viewport zoom locking
//  32768 (0x8000) = Currently always enabled
//  65536 (0x10000) = Enables non-rectangular clipping
//  131072 (0x20000) = Turns the viewport off
// 340 Hard-pointer ID/handle to entity that serves as the viewport's clipping boundary (only present if
// viewport is non-rectangular)

// 1 Plot style sheet name assigned to this viewport

// 281 Render mode:
//  0 = 2D Optimized (classic 2D)
//  1 = Wireframe
//  2 = Hidden line
//  3 = Flat shaded
//  4 = Gouraud shaded
//  5 = Flat shaded with wireframe
//  6 = Gouraud shaded with wireframe
// All rendering modes other than 2D Optimized engage the new 3D graphics pipeline. These values
// directly correspond to the SHADEMODE command and the
// AcDbAbstractViewTableRecord::RenderMode enum

// 71 UCS per viewport flag:
//  0 = The UCS will not change when this viewport becomes active.
//  1 = This viewport stores its own UCS which will become the current UCS whenever the viewport
//  is activated

// 74 Display UCS icon at UCS origin flag:
//  Controls whether UCS icon represents viewport UCS or current UCS (these will be different if
//  UCSVP is 1 and viewport is not active). However, this field is currently being ignored and the icon
//  always represents the viewport UCS

// 110 UCS origin
//  DXF: X value; APP: 3D point
//  -120, 130 DXF: Y and Z values of UCS origin

// 111 UCS X-axis
//  DXF: X value; APP: 3D vector
//  -121, 131 DXF: Y and Z values of UCS X-axis

// 112 UCS Y-axis
//  DXF: X value; APP: 3D vector
//  - 122, 132 DXF: Y and Z values of UCS Y-axis

// 345 ID/handle of AcDbUCSTableRecord if UCS is a named UCS. If not present, then UCS is unnamed
// 346 ID/handle of AcDbUCSTableRecord of base UCS if UCS is orthographic (79 code is non-zero). If
//  not present and 79 code is non-zero, then base UCS is taken to be WORLD

// 79 Orthographic type of UCS:
//  0 = UCS is not orthographic
//  1 = Top; 2 = Bottom
//  3 = Front; 4 = Back
//  5 = Left; 6 = Right

// 146 Elevation

// 170 ShadePlot mode:
//  0 = As Displayed
//  1 = Wireframe
//  2 = Hidden
//  3 = Rendered

// 61 Frequency of major grid lines compared to minor grid lines

// 332 Background ID/Handle (optional)

// 333 Shade plot ID/Handle (optional)

// 348 Visual style ID/Handle (optional)

// 292 Default lighting flag. On when no user lights are specified.

// 282 Default lighting type:
//  0 = One distant light
//  1 = Two distant lights
// 141 View brightness

// 142 View contrast

// 63,421,431 Ambient light color. Write only if not black color.

// 361 Sun ID/Handle (optional)

// NOTE The ZOOM XP factor is calculated with the following formula: group_41
// / group_45 (or pspace_height / mspace_height).

{
// WIPEOUT
// The following group codes apply to wipeout entities. In addition to the group
// codes described here.
// 100 Subclass marker (AcDbRasterImage)

// 90 Class version

// 10 Insertion point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of insertion point (in WCS)

// 11 U-vector of a single pixel (points along the visual bottom of the image, starting at the insertion
// point) (in WCS)
// DXF: X value; APP: 3D point
// 21, 31 DXF: Y and Z values U-vector (in WCS)

// 12 V-vector of a single pixel (points along the visual left side of the image, starting at the insertion
// point) (in WCS)
// DXF: X value; APP: 3D point
// 22, 32 DXF: Y and Z values of V-vector (in WCS)

// 13 Image size in pixels
// DXF: U value; APP: 2D point (U and V values)
// - 23 DXF: V value of image size in pixels

// 340 Hard reference to imagedef object

// 70 Image display properties:
//  1 = Show image
//  2 = Show image when not aligned with screen
//  4 = Use clipping boundary
//  8 = Transparency is on
// 280 Clipping state: 0 = Off; 1 = On

// 281 Brightness value (0-100; default = 50)

// 282 Contrast value (0-100; default = 50)

// 283 Fade value (0-100; default = 0)

// 360 Hard reference to imagedef_reactor object

// 71 Clipping boundary type. 1 = Rectangular; 2 = Polygonal

// 91 Number of clip boundary vertices that follow

// 14 Clip boundary vertex (in OCS)
// DXF: X value; APP: 2D point (multiple entries)
// NOTE 1) For rectangular clip boundary type, two opposite corners must be specified. Default is
// (-0.5,-0.5), (size.x-0.5, size.y-0.5). 2) For polygonal clip boundary type, three or more vertices
// must be specified. Polygonal vertices must be listed sequentially
// - 24 DXF: Y value of clip boundary vertex (in OCS) (multiple entries)

{
// XLINE
// The following group codes apply to xline entities. In addition to the group
// codes described here, see gCommon Group Codes for Entitiesh on page 53.
// 100 Subclass marker (AcDbXline)

// 10 First point (in WCS)
// DXF: X value; APP: 3D point
// - 20, 30 DXF: Y and Z values of first point (in WCS)

// 11 Unit direction vector (in WCS)
//  DXF: X value; APP: 3D vector
// - 21, 31 DXF: Y and Z values of unit direction vector (in WCS)


#endif

