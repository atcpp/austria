

#include "grt_base.h"

#include "at_unit_test.h"
#include "at_status_report_basic.h"

AT_TestArea( GRT, "Graphics Geometry Template Library" );

#include <iostream>
#include <fstream>

AT_DefineTest( Basic, GRT, "Basic vector stuff" )
{
	void Run()
	{

        double  v1[4] = { 1, 2, 3, 4 };

        grt::vec4d    vc1( v1 );
        grt::vec3f    vc2( vc1 );
        grt::vec4f    vc3( vc2 );
        
        double  m1[4][4] = {
            { 1, 2, 3, 4 },
            { 1, 2, 3, 4 },
            { 1, 2, 3, 4 },
            { 1, 2, 3, 4 },
        };
        
        grt::mat4d    mc1( m1 );
        grt::mat4f    mc2( m1 );

        const grt::vec4d & rv1 = vc1 ^ vc2;

        const double & rv2 = vc1 * vc3;
	}
};

AT_RegisterTest( Basic, GRT );
