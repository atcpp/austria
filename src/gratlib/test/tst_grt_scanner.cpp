

#include "grt_scanner.h"

#include "at_any.h"

#include <list>
#include <algorithm>

#include "at_unit_test.h"
//#include "at_status_report_basic.h"

AT_TestArea( GRTScanner, "Scanner" );

#include <iostream>
#include <fstream>
#include <iterator>

struct TestObject
{
    char        m_token;

    int         m_value;

    TestObject()
      : m_token(),
        m_value()
    {
    }

    TestObject( 
        const char  i_token,
        int         i_value
    )
      : m_token( i_token ),
        m_value( i_value )
    {
        AT_Assert( i_token );
    }

    operator char ()
    {
        return m_token;
    }

    bool operator<( const TestObject & i_rhs ) const
    {
        return m_token < i_rhs.m_token;
    }
    bool operator==( const TestObject & i_rhs ) const
    {
        return m_token == i_rhs.m_token;
    }
};


template<
    typename        w_char_type,
    class           w_traits
>
std::basic_ostream<w_char_type, w_traits>& operator << (
    std::basic_ostream<w_char_type, w_traits>        & o_ostream,
    const TestObject                            & i_value
) {
    o_ostream << "'" << i_value.m_token << "':" << i_value.m_value;
    return o_ostream;
   
} // end basic_ostream<w_char_type, w_traits>& operator <<




TestObject  testseq[] = {
    TestObject( 'a', 0 ),
    TestObject( 'a', -1 ),
    TestObject( 'c', 1 ),
    TestObject( 'a', -1 ),
    TestObject( 'x', 2 ),
    TestObject( 'a', -3 ),
    TestObject( 'c', -4 ),
    TestObject( 'a', -6 ),
    TestObject( 'q', -5 ),
    TestObject( 'a', -6 ),
    TestObject( 'a', -1 ),
    TestObject( 'x', 2 ),
    TestObject( 'a', -3 ),
    TestObject( 'y', -4 ),
    TestObject( 'y', -4 ),
    TestObject( 'z', -4 ),
    TestObject( 'z', -4 ),
    TestObject( 'a', -4 ),
    TestObject( 'c', -4 ),
    TestObject( 'z', -7 ),
    TestObject( 'a', -7 ),
    TestObject( 'c', -7 ),
    TestObject( 'a', -7 ),
    TestObject( 'c', -7 ),
    TestObject( 'a', -7 ),
    TestObject( 'c', -7 ),
    TestObject( 'a', -7 ),
    TestObject( 'c', -7 ),
    TestObject( 'a', -7 ),
    TestObject( 'c', -7 ),
    TestObject( 'c', -8 ),
};

struct ResultCode
{
    const char          * m_val;
    
    explicit ResultCode( const char * i_val )
      : m_val( i_val )
    {
    }

    static const ResultCode      l_codes[];
    static const ResultCode      l_repeat[];
    
    private:
    ResultCode( const ResultCode & );
    ResultCode operator=( const ResultCode & );
};

const ResultCode      ResultCode::l_codes[] = {
    ResultCode( "a" ),
    ResultCode( "z" ),
    ResultCode( "ac" ),
    ResultCode( "ax" ),
    ResultCode( "axat" ),
    ResultCode( "axay" ),
};

const ResultCode      ResultCode::l_repeat[] = {
    ResultCode( 0 ),
    ResultCode( "ac" ),
};


template<
    typename        w_char_type,
    class           w_traits
>
std::basic_ostream<w_char_type, w_traits>& operator << (
    std::basic_ostream<w_char_type, w_traits>        & o_ostream,
    const std::vector<TestObject>                    & i_value
) {
    o_ostream << '"';

    for (
        typename std::vector<TestObject>::const_iterator i = i_value.begin();
        i != i_value.end();
        ++ i
    ) {
        o_ostream << *i;
    }
    o_ostream << '"';
    return o_ostream;
   
} // end basic_ostream<w_char_type, w_traits>& operator <<

template<
    typename        w_char_type,
    class           w_traits
>
std::basic_ostream<w_char_type, w_traits>& operator << (
    std::basic_ostream<w_char_type, w_traits>                       & o_ostream,
    const std::pair<const ResultCode *, std::vector<TestObject> >   & i_val
) {
    o_ostream << "(" << i_val.first->m_val << ":" << i_val.second << ")";

    return o_ostream;
   
} // end basic_ostream<w_char_type, w_traits>& operator <<



class Consumer
{
    public:
    
    typedef std::list< at::Any<> >  t_List;
    t_List                          l_list;
    
    void SkippedToken( const TestObject & i_skip )
    {
        // skipped token
        l_list.push_back( at::ToAny( i_skip ) );

        std::cout << i_skip;
    }

    void Scanned( const ResultCode * i_result, std::vector<TestObject> & io_objects )
    {
        std::cout << "{" << i_result->m_val << ":" << io_objects << "}";
        // objects successfully scanned
        l_list.push_back( at::ToAny( std::pair<const ResultCode *, std::vector<TestObject> >( i_result, io_objects ) ) );
    }
};




AT_DefineTest( BasicScanner, GRTScanner, "Basic test" )
{
	void Run()
	{
        typedef grt::DFAMachine< char, const ResultCode *>   t_DFA;
        at::Ptr<t_DFA *>   l_machine = new t_DFA;

        t_DFA::Scanner< TestObject, std::vector<TestObject> >  l_scanner = l_machine->MakeScanner< TestObject, std::vector<TestObject> >();

        const ResultCode    * l_rc = ResultCode::l_codes;
        const ResultCode    * const l_rcend = ResultCode::l_codes + at::CountElements( ResultCode::l_codes );
        const ResultCode    * l_rprc = ResultCode::l_repeat;
        const ResultCode    * const l_rprcend = ResultCode::l_repeat + at::CountElements( ResultCode::l_repeat );

        while ( l_rcend != l_rc )
        {
            at::PtrView< t_DFA::DFAState * >    l_state;
            
            const ResultCode * l_collision = l_machine->AddTerminal( l_rc->m_val, l_rc->m_val + strlen( l_rc->m_val ), l_rc, 0, & l_state );

            AT_TCAssert( !l_collision, std::string("Collision between ") + l_rc->m_val + " and " + ( l_collision? l_collision->m_val : "" ) )

            if ( l_rprc != l_rprcend )
            {
                if ( l_rprc->m_val )
                {
                    const ResultCode * l_collision = l_machine->AddTerminal( l_rprc->m_val, l_rprc->m_val + strlen( l_rprc->m_val ), l_rc, & l_state, & l_state );
                }
                
                ++ l_rprc;
            }

            ++ l_rc;
        }

        grt::IteratorTranverser< const TestObject * >     l_trav( testseq, testseq + at::CountElements( testseq ) );

        Consumer    l_consumer;

        l_scanner.ScanAll( l_trav, l_consumer, true );

        for (
            Consumer::t_List::iterator i = l_consumer.l_list.begin();
            i != l_consumer.l_list.end();
            ++ i
        ) {
            std::cout << * i << ", ";
        }

        std::cout << std::endl;
        
	}
};

AT_RegisterTest( BasicScanner, GRTScanner );
