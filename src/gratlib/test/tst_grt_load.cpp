

#include "grt_loader.h"

#include "at_unit_test.h"
#include "at_status_report_basic.h"

AT_TestArea( GrtLoad, "Graphics Library Loader" );

#include <iostream>
#include <fstream>

AT_DefineTest( LoadBasic, GrtLoad, "Basic File Loader" )
{
	void Run()
	{
        std::ifstream       l_file( "test.dxf", std::ios::binary );

        grt::Loader         l_loader;

        l_loader.LoadFile( l_file );
	}
};

AT_RegisterTest( LoadBasic, GrtLoad );
