#include <list>
#include "at_activity.h"
#include "at_file.h"

#include "hptlink.h"

class Tlink_Base;
class DirScannerResult_Tlink;

// ======== MutexStream ===============================================
/**
 * MutexStream is used to create stream that can be written to
 * without having interleaving threads creating interleaving output
 * on the stream.
 */

class MutexStream
{
    public:

    /**
     * MutexStream
     *
     */
    MutexStream(
        std::ostream                & o_stream,
        at::Mutex                   & i_mutex
    )
      : m_stream( o_stream ),
        m_lock( i_mutex )
    {
    }

    std::ostream                        & m_stream;
    at::Lock<at::Mutex>                   m_lock;

    template <typename w_type>
    inline std::ostream & operator<<( const w_type & i_value )
    {
        return m_stream << i_value;
    }
};

// ======== DirScannerContext =========================================
/**
 * DirScannerContext_Basic contains the context of the tlink operation.
 *
 */

class DirScannerContext_Basic
{
    public:

    typedef std::list< at::Ptr< Tlink_Base * > >                t_Tlinks;
    typedef std::list< at::Ptr< DirScannerResult_Tlink * > >    t_ScanneResultList;

    
    /**
     * DirScannerContext_Basic
     *
     */
    DirScannerContext_Basic(
        unsigned                              i_scanner_threads,
        unsigned                              i_linker_threads,
        const at::FilePath                  & i_root_dir,
        const at::FilePath                  & i_tlink_dir,
        const std::vector< std::string >    & i_exclude,
        std::ostream                        & o_cout = std::cout,
        std::ostream                        & o_cerr = std::cerr
    )
      : m_exclude( i_exclude ),
        m_al_scanner( new at::ActivityList_Pool( i_scanner_threads ) ),
        m_al_linker( new at::ActivityList_Pool( i_linker_threads ) ),
        m_root_dir( i_root_dir ),
        m_tlink_dir( i_tlink_dir ),
        m_cout( o_cout ),
        m_cerr( o_cerr ),
        m_has_errors( false )
    {
    }


    // ======== Wait ==================================================
    /**
     * Wait for completion of all the operations.
     *
     * @return nothing
     */

    virtual void Wait();
    
    at::Mutex                             m_mutex;
    at::Mutex                             m_stream_mutex;
    const std::vector< std::string >    & m_exclude;
    at::Ptr<at::ActivityList *>           m_al_scanner;
    at::Ptr<at::ActivityList *>           m_al_linker;
    const at::FilePath                    m_root_dir;
    const at::FilePath                    m_tlink_dir;
    t_Tlinks                              m_tlinks;
    t_ScanneResultList                    m_scanners;

    std::ostream                        & m_cout;
    std::ostream                        & m_cerr;

    bool                                  m_has_errors;
};


// ======== CerrStream ================================================
/**
 * CerrStream uses the DirScannerContext_Basic's m_cerr as the
 * output stream object.
 */

class CerrStream
  : public MutexStream
{
    public:

    /**
     * CerrStream
     *
     */
    CerrStream(
        DirScannerContext_Basic     & i_context
    )
      : MutexStream( i_context.m_cerr, i_context.m_stream_mutex )
    {
    }

};


// ======== CoutStream ================================================
/**
 * CoutStream uses the DirScannerContext_Basic's m_cout as the
 * output stream object;
 */

class CoutStream
  : public MutexStream
{
    public:

    /**
     * CoutStream
     *
     */
    CoutStream(
        DirScannerContext_Basic     & i_context
    )
      : MutexStream( i_context.m_cout, i_context.m_stream_mutex )
    {
    }

};


// ======== Tlink_Base ================================================
/**
 * Tlink_Base encapsulates the base tlink 
 *
 */

class Tlink_Base
  : public at::PtrTarget_MT,
    public at::Activity
{
    public:

    /**
     * Tlink_Base
     *
     */
    Tlink_Base(
        DirScannerContext_Basic                     & io_context,
        at::PtrDelegate<Tlink_Base *>                 i_parent,
        const at::FilePath                          & i_src_dir,
        const at::FilePath                          & i_dst_dir,
        const at::FilePath                          & i_name
    )
      : at::Activity( io_context.m_al_linker ),
        m_context( io_context ),
        m_parent( i_parent ),
        m_src_dir( i_src_dir ),
        m_dst_dir( i_dst_dir ),
        m_name( i_name ),
        m_result( false )
    {
    }

    // Called by activity thread after an Enqueue()
    void Perform()
    {
        // if we have a parent - make sure it's done before we
        // do the real thing.
        if ( m_parent )
        {
            if ( ! m_parent->WaitForCompletion() )
            {
                return;
            }
        }
        
        Perform( m_dst_dir / m_name );
    }

    virtual void Perform( const at::FilePath & i_dst_path ) = 0;

    virtual bool WaitForCompletion()
    {
        // default implementation is - nothing to do.
        // this only needs to be implemented for directories.
        return m_result;
    }

    DirScannerContext_Basic                         & m_context;
    at::Ptr<Tlink_Base *>                             m_parent;
    const at::FilePath                                m_src_dir;
    const at::FilePath                                m_dst_dir;
    const at::FilePath                                m_name;
    bool                                              m_result;
};


// ======== Tlink_Directory ===========================================
/**
 * Tlink_Directory performs the "tlink" of a directory, which really
 * is just creating the destrination directory.
 *
 */

class Tlink_Directory
  : public Tlink_Base
{
    public:

    /**
     * Tlink_Base
     *
     */
    Tlink_Directory(
        DirScannerContext_Basic                     & io_context,
        at::PtrDelegate<Tlink_Base *>                 i_parent,
        const at::FilePath                          & i_src_dir,
        const at::FilePath                          & i_dst_dir,
        const at::FilePath                          & i_name
    )
      : Tlink_Base(
            io_context,
            i_parent,
            i_src_dir,
            i_dst_dir,
            i_name
        ),
        m_iscomplete( false )
    {
    }

    virtual void Perform( const at::FilePath & i_dst_path )
    {

        try
        {
            m_result = at::Directory::Create( i_dst_path );
        } catch ( at::FileErrorBasic & i_err )
        {
            m_result = false;
            m_context.m_has_errors = true;
            CerrStream( m_context ) << "Failed to create directory \"" << i_dst_path << "\" : " << i_err.What() << std::endl;
            return;
        }
        if ( ! m_result )
        {
            CerrStream( m_context ) << "Failed to create directory \"" << i_dst_path << "\"" << std::endl;
            m_context.m_has_errors = true;
        }
        
    }

    virtual bool WaitForCompletion()
    {
        Wait();
        
        return m_result;
    }

    ~Tlink_Directory()
    {
        Wait();
    }

    /**
     * m_iscomplete indicates the completion.
     */
    bool                                        m_iscomplete;
};


// ======== Tlink_File ================================================
/**
 * This performs a link on the file.
 *
 */

class Tlink_File
  : public Tlink_Base
{
    public:

    /**
     * Tlink_File
     *
     */
    Tlink_File(
        DirScannerContext_Basic                     & io_context,
        at::PtrDelegate<Tlink_Base *>                 i_parent,
        const at::FilePath                          & i_src_dir,
        const at::FilePath                          & i_dst_dir,
        const at::FilePath                          & i_name
    )
      : Tlink_Base(
            io_context,
            i_parent,
            i_src_dir,
            i_dst_dir,
            i_name
        )
    {
    }
    
    virtual void Perform( const at::FilePath & i_dst_path )
    {
        const at::FilePath l_src_path = m_src_dir / m_name;

        m_result = at::BaseFile::HardLink( l_src_path, i_dst_path );
        if ( ! m_result )
        {
            CerrStream( m_context ) << "Failed to create link from \"" << l_src_path << "\" to \"" << i_dst_path << "\"" << std::endl;
            m_context.m_has_errors = true;
        }

    }

    virtual bool WaitForCompletion()
    {
        Wait();
        
        return m_result;
    }

    ~Tlink_File()
    {
        Wait();
    }

};




// ======== DirScannerResult_Tlink ==========================================
/**
 * DirScannerResult_Tlink implements a tlink (gnu::cp -rl) like function.
 * Directory scanning is also done in a multithreaded way (more for experimentation
 * than because it actually improves performance.)  This may be also dependant on
 * the performance of the underlying file system.
 */

class DirScannerResult_Tlink
  : public at::PtrTarget_MT,
    public DirScannerResult,
    public at::Activity
{
    public:

    DirScannerResult_Tlink(
        DirScannerContext_Basic             & io_context,
        at::PtrDelegate<Tlink_Base *>         i_parent,
        const at::FilePath                  & i_src_dir,
        const at::FilePath                  & i_dst_dir
    )
      : at::Activity( io_context.m_al_scanner ),
        m_context( io_context ),
        m_parent( i_parent ),
        m_src_dir( i_src_dir ),
        m_dst_dir( i_dst_dir )
    {
    }

    ~DirScannerResult_Tlink()
    {
        Wait();
    }

    // ======== DoDirectory ===========================================
    /**
     * DoDirectory is called whenever a directory is scanned.
     *
     * @param i_path The directory path.
     * @return nothing
     */

    virtual void	DoDirectory(
        const at::FilePath                  & i_sub_dir,
        const at::FilePath                  & i_path
    ) {

//CerrStream( m_context ) << "DoDirectory : " << i_path << std::endl;

        at::Ptr<Tlink_Base *> i_newdir = new Tlink_Directory(
            m_context,
            m_parent,
            m_src_dir,
            m_dst_dir,
            i_path
        );

        // Enqueue this as well
        i_newdir->Enqueue();

        m_local_dir_list.push_back( i_newdir );

        at::Ptr< DirScannerResult_Tlink * > l_scanner = new DirScannerResult_Tlink(
            m_context,
            i_newdir,           // new parent
            m_src_dir / i_path,
            m_dst_dir / i_path
        );

        // Start scanning the next directory
        l_scanner->Enqueue();

        m_local_scanners.push_back( l_scanner );
    }

    // ======== DoFile ================================================
    /**
     * DoFile is called whenever a file is scanned
     *
     *
     * @param i_path The path to the file
     * @return nothing
     */

    virtual void	DoFile(
        const at::FilePath                  & i_sub_dir,
        const at::FilePath                  & i_path
    ) {

        at::Ptr<Tlink_Base *> l_newfile = new Tlink_File(
            m_context,
            m_parent,
            m_src_dir,
            m_dst_dir,
            i_path
        );

        m_local_list.push_back( l_newfile );

    }

    // ======== WaitForCompletion =====================================
    /**
     *	WaitForCompletion will wait for this DirScannerResult_Tlink to complete
     *  it's operation.
     *
     * @return True if the operation completed successfully
     */

    virtual bool WaitForCompletion()
    {
        Wait();
        return true;
    }
    
    // ======== DoEnd =================================================
    /**
     * DoEnd is called when no further files are found.
     *
     *
     * @return nothing
     */

    virtual void DoEnd()
    {
        at::Lock<at::Mutex>             l_lock( m_context.m_mutex );

        m_context.m_tlinks.splice(
            m_context.m_tlinks.end(),
            m_local_dir_list
        );
        
        for (
            DirScannerContext_Basic::t_Tlinks::iterator l_itr = m_local_list.begin();
            m_local_list.end() != l_itr;
            ++ l_itr
        ) {
            ( * l_itr )->Enqueue();
        }
        
        m_context.m_tlinks.splice(
            m_context.m_tlinks.end(),
            m_local_list
        );
        m_context.m_scanners.splice(
            m_context.m_scanners.end(),
            m_local_scanners
        );
    }
    
    // Called by activity thread after an Enqueue()
    void Perform()
    {
        DirScanner      l_scanner( m_src_dir, at::FilePath(""), m_context.m_exclude );

        l_scanner.DoScan( * this );
    }

    DirScannerContext_Basic                   & m_context;
    at::Ptr<Tlink_Base *>                       m_parent;
    const at::FilePath                          m_src_dir;
    const at::FilePath                          m_dst_dir;
    
    DirScannerContext_Basic::t_Tlinks           m_local_dir_list;
    DirScannerContext_Basic::t_Tlinks           m_local_list;
    DirScannerContext_Basic::t_ScanneResultList m_local_scanners;
};


// ======== DirScannerContext_Basic::Wait =============================

inline void DirScannerContext_Basic::Wait()
{

    do
    {
        
        t_ScanneResultList::iterator  l_itr;

        {
            at::Lock<at::Mutex>     l_lock(m_mutex);
            l_itr = m_scanners.begin();
            
            if ( m_scanners.end() == l_itr )
            {
                break;
            }
        }

        (* l_itr)->Wait();

        {
            at::Lock<at::Mutex>     l_lock(m_mutex);
            m_scanners.pop_front();
        }
    } while (1);

    // this will wait for all linkers to complete.
    m_tlinks.clear();
}

