
#include <iostream>
#include "at_dir.h"


// ======== DirScannerResult ==========================================
/**
 * DirScannerResult is the interface to provide as a result of scanning
 * a directory.
 *
 */

class DirScannerResult
{
    public:

    // virtual destructor
    virtual ~DirScannerResult() {}

    // ======== DoDirectory ===========================================
    /**
     * DoDirectory is called whenever a directory is scanned.
     *
     * @param i_path The directory path.
     * @return nothing
     */

    virtual void	DoDirectory(
        const at::FilePath                  & i_sub_dir,
        const at::FilePath                  & i_path
    ) = 0;

    

    // ======== DoFile ================================================
    /**
     * DoFile is called whenever a file is scanned
     *
     *
     * @param i_path The path to the file
     * @return nothing
     */

    virtual void	DoFile(
        const at::FilePath                  & i_sub_dir,
        const at::FilePath                  & i_path
    ) = 0;


    // ======== DoEnd =================================================
    /**
     * DoEnd is called when no further files are found.
     *
     *
     * @return nothing
     */

    virtual void DoEnd() = 0;
    

    // ======== WaitForCompletion =====================================
    /**
     *	WaitForCompletion will wait for this DirScannerResult to complete
     *  it's operation.
     *
     * @return True if the operation completed successfully
     */

    virtual bool WaitForCompletion() = 0;

    
};


// ======== DirScanner ================================================
/**
 * 
 *
 */

class DirScanner
{
    public:

    static const std::vector< std::string >     s_empty_exclude;

    // ======== DirScanner ============================================
    /**
     *	
     * @param i_dir The scan directory
     */

    DirScanner(
        const at::FilePath                  & i_root_dir,
        const at::FilePath                  & i_sub_dir,
        const std::vector< std::string >    & i_exclude = s_empty_exclude
    )
      : m_root_dir( i_root_dir ),
        m_sub_dir( i_sub_dir ),
        m_exclude( i_exclude )
    {
    }


    // ======== IsExcluded ============================================
    /**
     * IsExcluded returns true if the file is excluded as per the
     * exclusion patterns.
     *
     * @param i_filename
     * @return True if the file is excluded
     */

    bool IsExcluded(
        const at::FilePath          & i_path
    ) {

        const std::vector< std::string >::const_iterator    l_end = m_exclude.end();
        std::vector< std::string >::const_iterator          l_itr = m_exclude.begin();

        while ( l_end != l_itr )
        {
            if ( i_path.Match( * l_itr ) )
            {
                return true;
            }
            
            ++ l_itr;
        }

        return false;
    }

    // ======== DoScan ================================================
    /**
     * DoScan will scan a directory and pass all the scanned contents
     * by calling o_result with directory and file names.
     *
     * @param o_result The result of the scan
     * @return nothing
     */

    void DoScan( DirScannerResult & o_result )
    {
        // want to destruct DirIterator before calling DoEnd()
        {
            bool            l_is_inc_dir = false;

            at::FilePath    l_dir = m_sub_dir.StlString().length() ? m_root_dir / m_sub_dir : m_root_dir;
            
            at::DirIterator l_dir_iter( l_dir );
        
            if ( l_dir_iter.First() )
            {
                do {
                    static at::FilePath     l_dot( "." );
                    static at::FilePath     l_dotdot( ".." );
        
                    const at::FilePath& l_name = l_dir_iter.GetEntry().m_entry_name;
        
                    if ( ( l_name == l_dot ) ||  ( l_name == l_dotdot ) )
                    {
                        continue;
                    }
    
                    if ( IsExcluded( l_name ) )
                    {
                        continue;
                    }
    
                    if ( at::Directory::Exists( l_dir / l_name ) )
                    {
                        o_result.DoDirectory( m_sub_dir, l_name );
                    }
                    else
                    {
                        o_result.DoFile( m_sub_dir, l_name );
                    }
                    
                }
                while ( l_dir_iter.Next() );
            }
        }

        // indicate that there are no more files.
        o_result.DoEnd();
    }
    
    at::FilePath                          m_root_dir;
    at::FilePath                          m_sub_dir;

    const std::vector< std::string >    & m_exclude;

};

// should be in a .cpp file
const std::vector< std::string > DirScanner::s_empty_exclude;

