
#include "cmdoptns.h"

#include <memory>
#include <iostream>
#include <ctime>
#include <sstream>

#include <string.h>

#include "at_string_conv.h"



#include "hptlink_mt.h"

typedef long long t_longlong;

// ================================================================================================
// Help and Debug Options

static ost::CommandOptionNoArg help_opt(
    "help",
    "?",
    "Print help usage.",
    false
);

static ost::CommandOptionNoArg debug_opt(
    "debug",
    "d",
    "Debug mode.",
    false
);

/*
static ost::CommandOptionNoArg parents_opt(
    "parents",
    "p",
    "Create parents of the destination directory",
    false
);
*/

static ost::CommandOptionArg    exclude_opt(
    "exclude",
    "x",
    "Exclude pattern",
    false
);

static ost::CommandOptionArg    scan_threads_opt(
    "scan_threads",
    "z",
    "Number of scanning threads",
    false
);

static ost::CommandOptionArg    link_threads_opt(
    "link_threads",
    "y",
    "Number of linker threads",
    false
);

static ost::CommandOptionRest dirs_opt(
    "dirs",
    "=",
    "Input + output directory",
    true
);



// ======== ParamConv ================================================
/**
 * Convert a command line parameter to value of a particular type
 *
 * @param i_parameter
 * @return nothing
 */

template <typename w_Type>
w_Type ParamConv(
    const ost::CommandOptionArg     & i_param,
    const w_Type                    & i_default,
    const int                       & i_index = 0
) {
    if ( i_param.numValue <= i_index  )
    {
        return i_default;
    }

    return at::ToValue<w_Type>( i_param.values[ i_index ] );
}




// ======== do_tlink ==================================================
/**
 * Peform a linking
 *
 * @return exit value for the command
 */

int do_tlink()
{

    std::vector<std::string>    l_excludes(
        exclude_opt.values,
        exclude_opt.values + exclude_opt.numValue
    );

    at::FilePath    i_dest = at::FilePath( dirs_opt.values[1] );

    DirScannerContext_Basic     l_context(
        ParamConv( scan_threads_opt, 30U ),
        ParamConv( link_threads_opt, 3U ),
        at::FilePath( dirs_opt.values[0] ),
        i_dest,
        l_excludes
    );

    /*
    bool l_result = at::CreatePath(
        i_dest,
        parents_opt.numSet ? at::FilePath() : i_dest.Head()
    );
    */

    if ( ! at::Directory::Exists( i_dest ) )
    {
        bool l_result;
        try
        {
            l_result = at::Directory::Create( i_dest );
        } catch ( at::FileErrorBasic & i_err )
        {
            l_context.m_has_errors = true;
            std::cerr << "Failed to create directory \"" << i_dest << "\" : " << i_err.What() << std::endl;
            return 1;
        }
    
        if ( ! l_result )
        {
            l_context.m_cerr << "Failed to create \"" << i_dest << "\"\n";
            return 1;
        }
    }

    at::Ptr< DirScannerResult_Tlink * > l_scanner = new DirScannerResult_Tlink(
        l_context,
        0, // no parent
        l_context.m_root_dir,
        l_context.m_tlink_dir
    );

    l_scanner->Enqueue();
    
    l_context.m_scanners.push_back( l_scanner );

//    at::Task::Sleep( at::TimeInterval::Secs( 300 ) );
    
    l_context.Wait();

    if ( l_context.m_has_errors )
    {
        return 1;
    }

    return 0;
}


int main( int argc, char** argv )
{
    // parse the command line options
    std::auto_ptr<ost::CommandOptionParse> args(
        ost::makeCommandOptionParse(
            argc, argv,
            "[ options ] <read device> <write device>\n"
            "Copy data from <read device> to <write device> in a high performance way.\n"
            "Options are:\n"
        )
    );

    // If the user requested help then suppress all the usage error messages.
    if ( help_opt.numSet ) {
        std::cerr << args->printUsage();
        return 0;
    }

    if ( dirs_opt.numValue != 2 )
    {
        std::cerr << "Need exactly 2 directories: " << args->printUsage();
        return 0;
    }

    return do_tlink();
};
