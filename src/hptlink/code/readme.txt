
hptlink is equivalent to the SGI "tlink" command or "Tree Link" which is now
available as options to the "cp" command in the GNU file tools (i.e. "cp -rl").

In an automated build system, fresh copies are regularly made of a checked
out tree so avoid hitting the subversion repository for every checkout of the
tree.  The automated build does not (for the most part) require the ".svn"
directories and perforing multiple passes against the tree using the standard
GNU tools appears to make the copy process too slow.

hptlink tries to solve this problem by providing a "--exclude" option so that
the ".svn" (or CVS) directories can be omitted and a multithreaded algorithm
to attempt to extract as much parallelism as possible.

For command line options see:
hptlink --help

With some testing on a Linux 2.6.12 tree with xfs, (without --exclude), hptlink
can be slower than "cp -rl".  With large trees, 1 "scan" thread and "2" link
threads on a 2 CPU machine, the performance seems to be up to 50% better than
a regular "cp -rl".
