// ================================================================================================
// Include Directives

#include <cmdoptns.h>

#include <list>
#include <set>
#include <map>
#include <algorithm>

#include <iostream>
#include <ostream>

#include "mxs_tools.h"

static ost::CommandOption* command_option_list = 0;

// ================================================================================================
// File Path Printer

struct FilePathPrinter : public std::unary_function< const at::FilePath&, void >
{
    FilePathPrinter( std::ostream& i_os ) :
        m_os( i_os ) {}
    FilePathPrinter( std::ostream& i_os, const char* i_head, const char* i_foot ) :
        m_os( i_os ), m_head( i_head ), m_foot( i_foot ) {}
    void operator() ( const at::FilePath& i_path)
        { m_os << m_head << i_path << m_foot; }
    std::ostream& m_os;
    std::string   m_head;
    std::string   m_foot;
};

// ================================================================================================
// Names and Patterns

typedef std::string Name;
typedef std::string Pattern;
typedef std::set< Name > NameSet;
typedef std::set< Pattern > PatternSet;
typedef std::map< std::string, PatternSet > NamePatternMap;

struct FilePathMatch : public std::unary_function< const Pattern &, bool >
{
    FilePathMatch( const at::FilePath& i_path ) : m_path( i_path ) {}
    bool operator() ( const Pattern& i_pattern ) { return m_path.Match( i_pattern ); }
    const at::FilePath & m_path;
};

inline bool path_match( const at::FilePath& i_path, const PatternSet& i_patterns )
{
    return std::find_if( i_patterns.begin(),
                         i_patterns.end(),
                         FilePathMatch( i_path ) ) != i_patterns.end();
}

inline void add_arg( std::set< std::string >& i_set, const char* i_arg )
{
    // i_arg is supposed to be of the form <pattern>,<pattern>,...
    char c;
    const char* l_end;
    do
    {
        l_end = i_arg;
        while ( ( c = *l_end ) != '\0' && c != ',' ) l_end++;
        if ( i_arg != l_end ) i_set.insert( std::string( i_arg, l_end ) );
        i_arg = l_end + 1;
    }
    while ( *l_end == ',' );
}

static void add_arg( std::map< std::string, std::set< std::string > >& i_map, const char* i_arg )
{
    // i_arg is supposed to be of the form <name>:<pattern>,<pattern>,...
    const char* l_end = i_arg - 1;
    do
    {
        char c;
        const char* l_beg = l_end = l_end + 1;
        while ( ( c = *l_end ) != '\0' && c != ':' && c != ';' ) l_end++;
        const std::string l_name( l_beg, l_end );
        PatternSet& l_patterns = i_map[ l_name ];
        if ( *l_end == ':' )
        {
            do
            {
                l_beg = l_end = l_end + 1;
                while ( ( c = *l_end ) != '\0' && c != ',' && c != ';' ) l_end++;
                if ( l_beg != l_end ) l_patterns.insert( std::string( l_beg, l_end ) );
            }
            while ( *l_end == ',' );
        }
        else
        {
            l_patterns.insert( l_name );
        }
    }
    while ( *l_end == ';' );
}

// ================================================================================================
// Help and Debug Options

static ost::CommandOptionNoArg help_opt(
    "help", "?", "Print help usage.",
    false, &command_option_list
    );

#ifndef NDEBUG
static ost::CommandOptionNoArg debug_opt(
    "debug", "d",  "Debug mode.",
    false, &command_option_list
    );
#endif

static ost::CommandOptionNoArg show_config_opt(
    "show-tool-config", "s",  "Show tool configuration and exit immediately.",
    false, &command_option_list
    );

// ================================================================================================
// Input Path Directory Separator

static char input_path_directory_separator = '/';

static ost::CommandOptionArg directory_separator_arg(
    "directory_separator", 0, "The directory separator for input paths. Default is '/'.",
    false, &command_option_list
    );

static void set_directory_separator()
{
    if ( directory_separator_arg.numValue )
    {
        const char* l_argval = directory_separator_arg.values[0];
        if ( strlen( l_argval ) != 1 )
        {
            error_exit( "invalid directory separator", l_argval );
        }
        input_path_directory_separator = l_argval[0];
    }
    at::FilePath::s_directory_separator = '/';
}

// ================================================================================================
// The Root Directory

static at::FilePath root_dir( "." );

static ost::CommandOptionRest root_dir_arg(
    "root", 0, "The root of the directory tree.",
    false, &command_option_list
    );

static void set_root_dir()
{
    if ( root_dir_arg.numValue )
    {
        std::string l_root = root_dir_arg.values[0];
        if ( input_path_directory_separator != at::FilePath::s_directory_separator )
        {
            std::replace( l_root.begin(), l_root.end(), input_path_directory_separator,
                          at::FilePath::s_directory_separator );
        }
        root_dir = l_root;
        root_dir.Clean();
        if ( ! at::Directory::Exists( root_dir ) )
        {
            error_exit( "invalid directory", root_dir.String() );
        }
    }
#ifndef NDEBUG
    if ( debug_opt.numSet )
    {
        std::cerr << "Root Directory: " << root_dir << std::endl;
    }
#endif
}

// ================================================================================================
// Header File Name Patterns

static PatternSet header_file_patterns;

static ost::CommandOptionArg header_file_pattern_arg(
    "header-name-patterns", 0, "Define hearder file name patterns.",
    false, &command_option_list
    );

static ost::CommandOptionNoArg no_dflt_header_file_pattern_opt(
    "no-default-header-name-patterns", 0, "No default hearder file name patterns.",
    false, &command_option_list
    );

static void set_header_file_patterns()
{
    if ( ! no_dflt_header_file_pattern_opt.numSet )
    {
        add_arg( header_file_patterns, "*.h" );
    }
    for ( int i = 0; i < header_file_pattern_arg.numValue; ++i )
    {
        add_arg( header_file_patterns, header_file_pattern_arg.values[i] );
    }
    if ( header_file_patterns.empty() )
    {
        error_exit( "no header file name pattern specified" );
    }
}
    
// ================================================================================================
// Library File Name Patterns

static NamePatternMap lib_file_patterns;

static ost::CommandOptionArg lib_file_pattern_arg(
    "library-name-patterns", 0, "Define library file name patterns.",
    false, &command_option_list
    );

static ost::CommandOptionNoArg no_dflt_lib_file_pattern_opt(
    "no-default-library-name-patterns", 0, "No default library file name patterns.",
    false, &command_option_list
    );

static void set_lib_file_patterns()
{
    if ( ! no_dflt_lib_file_pattern_opt.numSet )
    {
        add_arg( lib_file_patterns, "gx86_32:*.a" );
        add_arg( lib_file_patterns, "gx86_64:*.a" );
        add_arg( lib_file_patterns, "win32:*.lib" );
        add_arg( lib_file_patterns, "mipsl:*.a;mipsb:*.a" );
        add_arg( lib_file_patterns, "ppc:*.a;sspc:*.a" );
        add_arg( lib_file_patterns, "sparc:*.a" );
        add_arg( lib_file_patterns, "darwin:*.a" );
    }
    for ( int i = 0; i < lib_file_pattern_arg.numValue; ++i )
    {
        add_arg( lib_file_patterns, lib_file_pattern_arg.values[i] );
    }
    if ( lib_file_patterns.empty() )
    {
        error_exit( "no library file name pattern specified" );
    }
}

// ================================================================================================
// Directory Exclusion

// The exclusion of a directory depends on:
// - the existence of some exclusion file in the directory
// - the directory path matching any of a set of path patterns

static at::FilePath excl_dir_file_name( "mxs_excl_incl" );
static PatternSet   excl_dir_path_patterns;

static ost::CommandOptionArg excl_dir_file_name_arg(
    "exclude-file-name", 0, "Name of the file to exclude a directory.",
    false, &command_option_list
);

static ost::CommandOptionArg excl_dir_path_arg(
    "excluded-directory-patterns", 0, "Define excluded directory patterns.",
    false, &command_option_list
);

static ost::CommandOptionNoArg no_dflt_excl_dir_opt(
    "no-default-excluded-directory-patterns", 0, "No default excluded directory patterns.",
    false, &command_option_list
);

static void set_excl_dir_opts ()
{
    if ( excl_dir_file_name_arg.numValue )
    {
        excl_dir_file_name = excl_dir_file_name_arg.values[0];
    }
    if ( ! no_dflt_excl_dir_opt.numSet )
    {
        excl_dir_path_patterns.insert( "*/CVS" );
        excl_dir_path_patterns.insert( "*/RCS" );
        excl_dir_path_patterns.insert( "*/.svn" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/apps" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/ASNMP" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/PACE" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/docs" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/examples" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/performance-tests" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/tests" );
        excl_dir_path_patterns.insert( "*ACE_wrappers/netsvcs" );
    }
    for ( int i = 0; i < excl_dir_path_arg.numValue; ++i )
    {
        add_arg( excl_dir_path_patterns, excl_dir_path_arg.values[i] );
    }
}

inline bool is_directory_excluded( const at::FilePath & i_path )
{
    const at::FilePath l_path = root_dir / i_path;
    return at::BaseFile::Exists( l_path / excl_dir_file_name ) || path_match( l_path, excl_dir_path_patterns );
}

// ================================================================================================
// Build Types

static NameSet build_types;

static ost::CommandOptionArg build_type_arg(
    "build-types", 0, "Define the list of build types.",
    false, &command_option_list
    );

static ost::CommandOptionNoArg no_dflt_build_type_opt(
    "no-default-build-types", 0, "No default build types.",
    false, &command_option_list
    );

static void set_build_types()
{
    if ( ! no_dflt_build_type_opt.numSet )
    {
        add_arg( build_types, "work,release" );
    }
    for ( int i = 0; i < build_type_arg.numValue; ++i )
    {
        add_arg( build_types, build_type_arg.values[i] );
    }
    if ( build_types.empty() )
    {
        error_exit( "no build type specified" );
    }
}

// ================================================================================================
// Build Architectures

static NameSet architectures;

static ost::CommandOptionArg build_arch_arg(
    "architectures", 0, "Define the list of architectures.",
    false, &command_option_list
    );

static ost::CommandOptionNoArg no_dflt_arch_opt(
    "no-default-architectures", 0, "No default architectures.",
    false, &command_option_list
    );

static void set_architectures()
{
    if ( ! no_dflt_arch_opt.numSet )
    {
        add_arg( architectures, "gx86_32,gx86_64,win32,sparc,darwin" );
        add_arg( architectures, "mipsl,mipsb,ppc,sspc" );
    }
    for ( int i = 0; i < build_arch_arg.numValue; ++i )
    {
        add_arg( architectures, build_arch_arg.values[i] );
    }
    if ( architectures.empty() )
    {
        error_exit( "no architecture specified" );
    }
}

// ================================================================================================
// Build Information Data Structures

struct BuildInfo
{
    std::string m_key;               // <build_type> '.' <arch_name>
    PatternSet  m_spec_dir_patterns; // patterns of specific directories
    PatternSet  m_lib_name_patterns; // patterns of library file names
    FilePathVec m_inc_dirs;          // the include directories
    FilePathVec m_lib_dirs;          // the lib directories
    FilePathVec m_lib_names;         // the lib names 
};

std::ostream& operator<<( std::ostream& os, const BuildInfo& i_info )
{
    os << i_info.m_key << std::endl
       << "    specific directories  : " << ContForm<PatternSet>( i_info.m_spec_dir_patterns, "," ) << std::endl
       << "    library files pattern : " << ContForm<PatternSet>( i_info.m_lib_name_patterns, "," ) << std::endl;
    const std::string sep( "\n        " );
    if ( ! i_info.m_inc_dirs.empty() )
    {
        os << "    include directories   :" << sep
           << ContForm<FilePathVec>( i_info.m_inc_dirs, sep )
           << std::endl;
    }
    if ( ! i_info.m_lib_dirs.empty() )
    {
        os << "    library directories   :" << sep
           << ContForm<FilePathVec>( i_info.m_lib_dirs, sep )
           << std::endl;
    }
    if ( ! i_info.m_lib_names.empty() )
    {
        os << "    library file names    :" << sep
           << ContForm<FilePathVec>( i_info.m_lib_names, sep )
           << std::endl;
    }
    return os;
}

std::ostream& operator<<( std::ostream& os, const BuildInfo* i_info )
{
    if ( i_info )
    {
        os << *i_info;
    }
    return os;
}

typedef std::map< std::string, BuildInfo > BuildInfoMap;

static BuildInfoMap info_map;

static void create_info_map()
{
    const NameSet::const_iterator l_type_end = build_types.end();
    for ( NameSet::const_iterator l_type_i = build_types.begin(); l_type_i != l_type_end; l_type_i++ )
    {
        const NameSet::const_iterator l_arch_end = architectures.end();
        for ( NameSet::const_iterator l_arch_i = architectures.begin(); l_arch_i != l_arch_end; l_arch_i++ )
        {
            const std::string l_key = *l_type_i + '.' + *l_arch_i;
            BuildInfo& l_info = info_map[ l_key ];
            l_info.m_key = l_key;
            l_info.m_spec_dir_patterns.insert( ( at::FilePath( "*" ) / *l_arch_i ).String() );
            l_info.m_spec_dir_patterns.insert( ( at::FilePath( "*" ) / l_key ).String() );
            l_info.m_lib_name_patterns = lib_file_patterns[ *l_arch_i ];
        }
    }
}

std::ostream& operator<<( std::ostream& os, const BuildInfoMap::value_type& i_key_build_info_pair )
{
    return os << i_key_build_info_pair.second;
}

typedef std::vector< BuildInfo * > BuildInfoPtrVec;

inline void add_inc_dir( const at::FilePath&    i_inc_dir,
                         const BuildInfoPtrVec& i_build_infos )
{
    const BuildInfoPtrVec::const_iterator l_end = i_build_infos.end();
    BuildInfoPtrVec::const_iterator l_iter = i_build_infos.begin();

    while ( l_iter != l_end )
    {
        ( *l_iter++ )->m_inc_dirs.push_back( i_inc_dir );
    }
}
    
inline void add_lib_name( const at::FilePath&    i_lib_dir,
                          const at::FilePath&    i_lib_name,
                          const BuildInfoPtrVec& i_build_infos )
{
    const BuildInfoPtrVec::const_iterator l_end = i_build_infos.end();
    BuildInfoPtrVec::const_iterator l_iter = i_build_infos.begin();

    while ( l_iter != l_end )
    {
        BuildInfo* l_info = *l_iter++;
        if ( path_match( i_lib_name, l_info->m_lib_name_patterns ) )
        {
            l_info->m_lib_names.push_back( i_lib_name );
            if ( l_info->m_lib_dirs.empty() || i_lib_dir != l_info->m_lib_dirs.back() )
            {
                l_info->m_lib_dirs.push_back( i_lib_dir );
            }
        }
    }
}

// ================================================================================================
// Cache Creation

static void create_cache_file( const at::FilePath& i_file, const FilePathVec& i_paths )
{
    std::ofstream l_fos( i_file.String() );
    if ( ! l_fos ) error_exit( "cannot write file", i_file.String() );
    std::for_each( i_paths.begin(), i_paths.end(), FilePathPrinter( l_fos, "", "\n" ) );
}

static void create_cache( const BuildInfo& i_info, const at::FilePath& i_cache_dir )
{
    // Create the build-specific, cache sub-directory if it does not exist yet.
    const at::FilePath l_cache_build_dir = i_cache_dir / i_info.m_key;
    if ( ! at::Directory::Exists( l_cache_build_dir ) )
    {
        try
        {
            at::Directory::Create( l_cache_build_dir );
        }
        catch( at::FileError& fe )
        {
            error_exit( fe.What(), l_cache_build_dir.String() );
        }
    }
    // Create the 3 build-specific cache files.
    create_cache_file( l_cache_build_dir / inc_dirs_cache_file_base_name, i_info.m_inc_dirs );
    create_cache_file( l_cache_build_dir / lib_dirs_cache_file_base_name, i_info.m_lib_dirs );
    create_cache_file( l_cache_build_dir / lib_names_cache_file_base_name, i_info.m_lib_names );
}

static void create_cache( const BuildInfoPtrVec& i_builds )
{
    // Create the cache directory if it does not exist yet.
    const at::FilePath l_cache_dir = root_dir / cache_directory_name;
    if ( ! at::Directory::Exists( l_cache_dir ) )
    {
        try
        {
            at::Directory::Create( l_cache_dir );
        }
        catch( at::FileError& fe )
        {
            error_exit( fe.What(), l_cache_dir.String() );
        }
    }
    // Loop through the build info entries creating caches files.
    const BuildInfoPtrVec::const_iterator l_end = i_builds.end();
    for ( BuildInfoPtrVec::const_iterator l_ptr = i_builds.begin(); l_ptr != l_end; ++l_ptr )
    {
        BuildInfo& l_info = **l_ptr;
        std::sort( l_info.m_inc_dirs.begin(), l_info.m_inc_dirs.end() );
        std::sort( l_info.m_lib_dirs.begin(), l_info.m_lib_dirs.end() );
        std::sort( l_info.m_lib_names.begin(), l_info.m_lib_names.end() );
        create_cache( l_info, l_cache_dir );
    }
}

// ================================================================================================
// Read Cache

inline void read_cache_file( const at::FilePath& i_file, const at::FilePath& i_dir, FilePathVec& i_paths )
{
    std::ifstream l_fis( i_file.String() );
    if ( ! l_fis ) error_exit( "cannot read file", i_file.String() );

    std::string l_str;
    while ( getline( l_fis, l_str ) )
    {
        i_paths.push_back( i_dir / l_str );
    }
}

static void read_cache( const at::FilePath& i_dir, BuildInfoPtrVec& i_builds, BuildInfoPtrVec& o_builds )
{
    BuildInfoPtrVec l_builds;
    const at::FilePath l_cache_dir = root_dir / i_dir / cache_directory_name;
    const BuildInfoPtrVec::const_iterator l_end = i_builds.end();
    for ( BuildInfoPtrVec::const_iterator l_ptr = i_builds.begin(); l_ptr != l_end; ++l_ptr )
    {
        BuildInfo* l_build = *l_ptr;
        const at::FilePath l_build_cache_dir = l_cache_dir / l_build->m_key;
        const at::FilePath l_inc_dirs_file  = l_build_cache_dir / inc_dirs_cache_file_base_name;
        const at::FilePath l_lib_dirs_file  = l_build_cache_dir / lib_dirs_cache_file_base_name;
        const at::FilePath l_lib_names_file = l_build_cache_dir / lib_names_cache_file_base_name;
        if ( at::BaseFile::Exists( l_inc_dirs_file ) &&
             at::BaseFile::Exists( l_lib_dirs_file ) &&
             at::BaseFile::Exists( l_lib_names_file ) )
        {
            read_cache_file( l_inc_dirs_file, i_dir, l_build->m_inc_dirs );
            read_cache_file( l_lib_dirs_file, i_dir, l_build->m_lib_dirs );
            read_cache_file( l_lib_names_file, i_dir, l_build->m_lib_names );
            o_builds.push_back( l_build );
        }
        else
        {
            l_builds.push_back( l_build );
        }
    }
    i_builds = l_builds;
}

inline bool directory_has_cache( const at::FilePath& i_dir )
{
    return at::Directory::Exists( root_dir / i_dir / cache_directory_name );
}

// ================================================================================================
// Filtering out build info entries to cache.

static PatternSet cache_file_patterns;

static ost::CommandOptionArg cache_file_pattern_arg(
    "cache", 0, "Define the patterns to filter out the files to cache.",
    false, &command_option_list
    );

static void set_cache_file_patterns()
{
    for ( int i = 0; i < cache_file_pattern_arg.numValue; ++i )
    {
        add_arg( cache_file_patterns, cache_file_pattern_arg.values[i] );
    }
    if ( cache_file_patterns.empty() )
    {
        add_arg( cache_file_patterns, "*.*" );
    }
}

// ================================================================================================
// Filtering out build info entries specific to a directory.

inline bool is_directory_specific( const at::FilePath&    i_dirpath,
                                   const BuildInfoPtrVec& i_builds )
{
    const BuildInfoPtrVec::const_iterator l_end = i_builds.end();
    for ( BuildInfoPtrVec::const_iterator l_ptr = i_builds.begin(); l_ptr != l_end; ++l_ptr )
    {
        if ( path_match( i_dirpath, ( *l_ptr )->m_spec_dir_patterns ) )
        {
            return true;
        }
    }
    return false;
}

inline bool is_directory_specific( const at::FilePath&    i_dirpath,
                                   const BuildInfoPtrVec& i_builds,
                                   BuildInfoPtrVec&       o_spec_builds,
                                   BuildInfoPtrVec&       o_excl_builds )
{
    const BuildInfoPtrVec::const_iterator l_end = i_builds.end();
    for ( BuildInfoPtrVec::const_iterator l_ptr = i_builds.begin(); l_ptr != l_end; ++l_ptr )
    {
        if ( path_match( i_dirpath, ( *l_ptr )->m_spec_dir_patterns ) )
        {
            o_spec_builds.push_back( *l_ptr );
        }
        else
        {
            o_excl_builds.push_back( *l_ptr );
        }
    }
    return ! o_spec_builds.empty();
}

// ================================================================================================
// Directory Scan

static void scan_directory( const at::FilePath&    i_dirpath,
                            const BuildInfoPtrVec& i_builds,
                            const BuildInfoPtrVec& i_excl_builds )
{
    // Check first if the directory is excluded and return immediately if it is.

    if ( is_directory_excluded( i_dirpath ) )
    {
#ifndef NDEBUG
        if ( debug_opt.numSet )
        {
            std::cerr << root_dir / i_dirpath << " excluded\n";
        }
#endif
        return;
    }

    // Filter out the build info entries which this directory is specific to.
    // If the directory is not specific to any build, use the input vectors. Note that l_builds will
    // end up empty if the directory is only specific to excluded builds.

    BuildInfoPtrVec l_builds;
    BuildInfoPtrVec l_excl_builds = i_excl_builds;

    if ( ! is_directory_specific( i_dirpath, i_builds, l_builds, l_excl_builds ) &&
         ! is_directory_specific( i_dirpath, i_excl_builds ) )
    {
        l_builds = i_builds;
        l_excl_builds = i_excl_builds;
    }

    // Read in cache files if any, updating the l_builds and l_excl_builds vectors.

    if ( directory_has_cache( i_dirpath ) )
    {
#ifndef NDEBUG
        if ( debug_opt.numSet )
        {
            std::cerr << "Found cache in " << root_dir / i_dirpath << std::endl;
        }
#endif
        read_cache( i_dirpath, l_builds, l_excl_builds );
    }

    // If there is no build info left to fill in, just return.

    if ( l_builds.empty() )
    {
#ifndef NDEBUG
        if ( debug_opt.numSet )
        {
            std::cerr << root_dir / i_dirpath << " skipped\n";
        }
#endif
        return;
    }

    // Collect the list of sub-directories, find out if we have an include directory, and,
    // if there are any, collect the list of library files.

    FilePathVec     l_dirs;
    bool            l_is_inc_dir = false;
    at::DirIterator l_dir_iter( root_dir / i_dirpath );

    if ( l_dir_iter.First() )
    {
        do {
            static at::FilePath     l_dot( "." );
            static at::FilePath     l_dotdot( ".." );

            const at::FilePath& l_name = l_dir_iter.GetEntry().m_entry_name;

            if ( ( l_name == l_dot ) ||  ( l_name == l_dotdot ) )
            {
                continue;
            }
            
            const at::FilePath  l_path( i_dirpath / l_name );
            if ( at::Directory::Exists( root_dir / l_path ) )
            {
                l_dirs.push_back( l_path );
            }
            else if ( ! l_is_inc_dir && path_match( l_name, header_file_patterns ) )
            {
                add_inc_dir( i_dirpath, l_builds );
                l_is_inc_dir = true;
            }
            else
            {
                add_lib_name( i_dirpath, l_name, l_builds );
            }
        }
        while ( l_dir_iter.Next() );
    }
#ifndef NDEBUG
    if ( debug_opt.numSet )
    {
        std::cerr << root_dir / i_dirpath << " scanned\n";
    }
#endif
    // Finally scan the collected sub-drectories

    const FilePathVec::const_iterator l_dirs_end = l_dirs.end();
    for ( FilePathVec::const_iterator l_dirs_ptr = l_dirs.begin(); l_dirs_ptr != l_dirs_end; ++l_dirs_ptr )
    {
        scan_directory( *l_dirs_ptr, l_builds, l_excl_builds );
    }
}

// ================================================================================================
// Main Function

int create_cache( int argc, char** argv )
{
    // parse the command line options
    std::auto_ptr<ost::CommandOptionParse> args(
        ost::makeCommandOptionParse(
            argc, argv,
            "[ options ] [ dir ]\n"
            "Traverse a directory tree to find the directories that contain header files or\n"
            "library files, and store the information in cache files.\n"
            "Options are:\n",
            command_option_list
        )
    );

    // If the user requested help then suppress all the usage error messages.
    if ( help_opt.numSet ) {
        std::cerr << args->printUsage();
        return 0;
    }

    // Print usage your way.
    if ( args->argsHaveError() )
    {
        std::cerr << args->printErrors();
        std::cerr << "Get help by --help\n";
        return 1;
    }

    // Go off and run any option specific task.
    args->performTask();

    // Process options and create the info map.
    // Note: always start with the directory separator.
    set_directory_separator();
    set_root_dir();
    set_header_file_patterns();
    set_lib_file_patterns();
    set_excl_dir_opts();
    set_build_types();
    set_architectures();
    set_cache_file_patterns();
    create_info_map();

    if ( show_config_opt.numSet )
    {
        std::cout << "Header file patterns  : " << ContForm<PatternSet>( header_file_patterns, ",") << std::endl
                  << "Exclusion file name   : " << excl_dir_file_name << std::endl
                  << "Excluded directories  : " << ContForm<PatternSet>( excl_dir_path_patterns, ",") << std::endl
                  << std::endl
                  << ContForm<BuildInfoMap>( info_map, "\n" )
                  << std::endl;
        return 0;
    }

    // Build the vector of information, and scan the top directory.
    BuildInfoPtrVec l_info_in;
    BuildInfoPtrVec l_info_out;
    const BuildInfoMap::const_iterator l_map_end = info_map.end();
    for ( BuildInfoMap::iterator l_map_ptr = info_map.begin(); l_map_ptr != l_map_end; ++l_map_ptr )
    {
        if ( path_match( l_map_ptr->first, cache_file_patterns ) )
        {
            l_info_in.push_back( &l_map_ptr->second );
        }
        else
        {
            l_info_out.push_back( &l_map_ptr->second );
        }
    }
    scan_directory( at::FilePath(), l_info_in, l_info_out );

    // Generate the cache files
    create_cache( l_info_in );

#ifndef NDEBUG
    if ( debug_opt.numSet )
    {
        std::cerr << std::endl << ContForm<BuildInfoPtrVec>( l_info_in, "\n" ) << std::endl;
    }
#endif

    return 0;
}
