// ================================================================================================
// Include Directives

#include <string>
#include <iostream>
#include <at_file.h>
#include "mxs_tools.h"

// ================================================================================================
// Global Definitions

const std::string cache_directory_name( "mxs_cache" );
const std::string inc_dirs_cache_file_base_name( "inc_dirs" );
const std::string lib_dirs_cache_file_base_name( "lib_dirs" );
const std::string lib_names_cache_file_base_name( "lib_files" );

void error_exit( const std::string& msg1, const std::string& msg2 )
{
    std::cerr << "mxs_tools: error: "  << msg1;
    if ( ! msg2.empty() )
    {
        std::cerr << ": " << msg2;
    }
    std::cerr << std::endl;
    std::exit( 1 );
}

void warning( const std::string& msg1, const std::string& msg2 )
{
    std::cerr << "mxs_tools: warning: "  << msg1;
    if ( ! msg2.empty() )
    {
        std::cerr << ": " << msg2;
    }
    std::cerr << std::endl;
}

// ================================================================================================
// Main Function

int create_cache( int argc, char** argv );
int create_vcproj( int argc, char** argv );

int main( int argc, char** argv )
{
    const at::FilePath l_cmd_path( argv[0] );
    const std::string  l_cmd_name = l_cmd_path.Tail().String();

    if ( l_cmd_name == "mxs_create_cache" )
    {
        return create_cache( argc, argv );
    }
    if ( l_cmd_name == "mxs_create_vcproj" )
    {
        return create_vcproj( argc, argv );
    }

    if ( argc > 1 )
    {
        if ( strcmp( argv[1], "create_cache" ) == 0 )
        {
            return create_cache( argc - 1, argv + 1 );
        }
        if ( strcmp( argv[1], "create_vcproj" ) == 0 )
        {
            return create_vcproj( argc - 1, argv + 1 );
        }
        if ( strcmp( argv[1], "--help" ) == 0 || strcmp( argv[1], "-h" ) == 0 )
        {
            std::cerr << "Usage:\n    "
                      << l_cmd_name << " create_cache ...\n"
                      << "or\n    "
                      << l_cmd_name << " create_vcproj ...\n";
            return 0;
        }
    }

    
    error_exit( "unknown command, expecting create_cache or create_vcproj", l_cmd_name );
    return 1;
}
