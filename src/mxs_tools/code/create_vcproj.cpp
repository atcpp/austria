// ================================================================================================
// Include Directives

#include <cmdoptns.h>
#include <at_xml_parser.h>
#include <at_xml_printer.h>
#include <at_exception.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>

#include "mxs_tools.h"

// ================================================================================================
// Command Line Options

static ost::CommandOption* command_option_list = 0;

static ost::CommandOptionNoArg theHelpArg(
    "help", "?", "Print help usage.",
    false, &command_option_list
    );

#ifndef NDEBUG
static ost::CommandOptionNoArg theDebugArg(
    "debug", "d",  "Debug mode.",
    false, &command_option_list
    );
#endif

static ost::CommandOptionArg theTemplateFileArg(
    "template", "T", "The template VC++ project file.",
    true, &command_option_list
    );

static ost::CommandOptionArg theFilterTemplateFileArg(
	"filter_template", "F", "The template VC++ filter file.",
	false, &command_option_list
);

static ost::CommandOptionArg theDirectorySeparatorArg(
    "directory_separator", 0, "The directory separator for input paths. Default is '/'.",
    false, &command_option_list
    );

static ost::CommandOptionArg theRootDirArg(
    "root", "R", "The root directory.",
    true, &command_option_list
    );

static ost::CommandOptionArg theSrcDirArg(
    "source", "S", "A source directory.",
    true, &command_option_list
    );

static ost::CommandOptionArg theOutputArg(
    "output", "o", "Place output in a file. Default is stdout.",
    false, &command_option_list
    );

static ost::CommandOptionArg theMacroCancelArg(
    "undef", "U", "Cancel any definition of a built in macro.",
    false, &command_option_list
    );

static ost::CommandOptionArg theMacroDefineArg(
    "define", "D", "Define a macro.",
    false, &command_option_list
    );

static ost::CommandOptionNoArg theListMacroArg(
    "list", "l", "List the built in macros and exit.",
    false, &command_option_list
    );

// ================================================================================================
// The macro definition map

typedef std::map< std::string, std::string >    MacroDefinitionMap;

// Decls.

int create_vcxproj_2008(MacroDefinitionMap& macro_defs);
int create_vcproj_pre2008(MacroDefinitionMap& macro_defs);

inline void define_macro( const std::string& i_def, MacroDefinitionMap& o_map )
{
    const size_t n = i_def.find( '=' );
    if ( n == 0 )
    {
        error_exit( "invalid name in macro definition", i_def );
    }
    if ( n == std::string::npos || n == i_def.length() )
    {
        o_map[ std::string( i_def, 0, n ) ] = "";
    }
    else
    {
        o_map[ std::string( i_def, 0, n ) ] = std::string( i_def, n + 1 );
    }
}

inline void cancel_macro( const std::string& i_name, MacroDefinitionMap& o_map )
{
    if ( ! o_map.erase( i_name ) )
    {
        warning( "unknown built in macro", i_name );
    }
}

inline std::ostream& operator<<( std::ostream& os, const MacroDefinitionMap::value_type& i_macro )
{
    return os << i_macro.first << "=\"" << i_macro.second << "\"";
}

// ================================================================================================
// VC Project Configuration Information

struct ConfigInfo
{
    std::string m_key;               // <build_type> '.' <arch_name>
    std::string m_inc_dirs;          // the include directories
    std::string m_lib_dirs;          // the lib directories
    std::string m_lib_names;         // the lib names

    ConfigInfo( const std::string& i_key ) : m_key( i_key ) {}
};

inline std::istream& mygetline( std::istream& is, std::string& s )
{
    if ( getline( is, s ) )
    {
        if ( s.length() && s[ s.length() - 1 ] == '\r' )
        {
            s.erase( s.length() - 1 );
        }
    }
    return is;
}

typedef void(*line_to_entries_func)(const at::FilePath& i_file, const at::FilePath& i_dir, std::string& i_entries);

inline void read_dir_path( const at::FilePath& i_file, const at::FilePath& i_dir, std::string& i_entries )
{
    std::ifstream l_fis( i_file.String() );
    if ( ! l_fis ) error_exit( "cannot read file", i_file.String() );

    std::string l_str;
    if ( mygetline( l_fis, l_str ) )
    {
        i_entries = ( i_dir / l_str ).String();
        while ( mygetline( l_fis, l_str ) )
        {
            i_entries.push_back( ';' );
            i_entries.append( ( i_dir / l_str ).String() );
        }
    }
}

inline void read_name_list( const at::FilePath& i_file, const at::FilePath& unused, std::string& i_entries )
{
    std::ifstream l_fis( i_file.String() );
    if ( ! l_fis ) error_exit( "cannot read file", i_file.String() );

    if ( mygetline( l_fis, i_entries ) )
    {
        std::string l_str;
        while ( mygetline( l_fis, l_str ) )
        {
            i_entries.push_back( ' ' );
            i_entries.append( l_str );
        }
    }
}

static void read_data_to_string(
	line_to_entries_func func, 
	const at::FilePath & value_file, 
	const at::FilePath& i_prefix_dir,
	std::string& out_str) {
	if (at::BaseFile::Exists(value_file))
	{
		func(value_file, i_prefix_dir, out_str);
	}
	else
	{
		warning("file not found", value_file.String());
	}
}

static void read_cache_info( const at::FilePath& i_cache_dir,
                             const at::FilePath& i_prefix_dir,
                             const std::string&  i_macro_name,
                             MacroDefinitionMap& o_macro_defs )
{
	read_data_to_string(read_dir_path, i_cache_dir / inc_dirs_cache_file_base_name, i_prefix_dir, o_macro_defs[i_macro_name + "INCLPATH"]);
	read_data_to_string(read_dir_path, i_cache_dir / lib_dirs_cache_file_base_name, i_prefix_dir, o_macro_defs[i_macro_name + "LIBPATH"]);
	read_data_to_string(read_name_list, i_cache_dir / lib_names_cache_file_base_name, i_prefix_dir, o_macro_defs[i_macro_name + "LIBRARIES"]);
}

// ================================================================================================
// Updating element attributes by replacing macros by their definitions

struct MacroApplier : public std::unary_function< MacroDefinitionMap::const_reference, void >
{
    MacroApplier( std::string& o_str ) : m_str( o_str ) {}

    void operator() ( MacroDefinitionMap::const_reference i_macro )
        {
            const std::string& l_macro_name = i_macro.first;
            const std::string& l_macro_def = i_macro.second;
            size_t n = m_str.find( l_macro_name );
            while ( n != std::string::npos )
            {
                m_str.replace( n, l_macro_name.length(), l_macro_def );
                n = m_str.find( l_macro_name, n + l_macro_def.length() );
            }
        }

private:

    std::string& m_str;
};

struct ElementAttributeUpdater //: public std::unary_function< at::XmlNodeList::const_reference, void >
{
    ElementAttributeUpdater( const MacroDefinitionMap& i_macro_defs ) : m_macro_defs( i_macro_defs ) {}

    void operator() ( at::XmlNodeList::const_reference i_node )
        {
            if ( at::PtrView< at::XmlElement* > l_elt = i_node->AsElement() )
            {
                at::XmlAttributeMap& l_attrs = l_elt->Attributes();
                std::for_each( l_attrs.begin(), l_attrs.end(), *this );                
            }
        }

    void operator() ( at::XmlAttributeMap::value_type& i_attr )
        {
            std::for_each( m_macro_defs.begin(), m_macro_defs.end(), MacroApplier( i_attr.second ) );
        }

private:

    const MacroDefinitionMap& m_macro_defs;
};

// ================================================================================================
// VC Project Source File Information

typedef std::set< at::PtrView< at::XmlNode* > > NodeSet;
typedef std::map< std::string, NodeSet >        FilterMap;

struct FilterMapBuilder : public std::unary_function< at::XmlNodeList::const_reference, void >
{
    FilterMapBuilder( FilterMap& i_map ) : m_map( i_map ) {}

    void operator() ( at::XmlNodeList::const_reference i_node )
        {
            if ( at::PtrView< at::XmlElement* > l_filter_elt = i_node->AsElement() )
            {
                if ( l_filter_elt->HasAttribute( "Filter" ) )
                {
                    const std::string& l_filter_attr = l_filter_elt->GetAttribute( "Filter" );
                    const std::string::const_iterator l_end = l_filter_attr.end();
                    std::string::const_iterator l_beg = l_filter_attr.begin();
                    if ( l_beg != l_end )
                    {
                        std::string::const_iterator l_sep;
                        while ( ( l_sep = std::find( l_beg, l_end, ';' ) ) != l_end )
                        {
                            m_map[ std::string( l_beg, l_sep ) ].insert( i_node );
                            l_beg = l_sep + 1;
                        }
                        m_map[ std::string( l_beg, l_end ) ].insert( i_node );
                    }
                }
            }
        }

private:

    FilterMap& m_map;
};

struct AddFileElement : public std::unary_function< at::XmlNodeList::const_reference, void >
{
    AddFileElement( const std::string& i_relpath ) :
        m_relpath( i_relpath )
        {
            if ( at::FilePath::s_directory_separator != '\\' )
            {
                std::replace( m_relpath.begin(), m_relpath.end(), at::FilePath::s_directory_separator, '\\' );
            }
        }

    void operator() ( at::XmlNodeList::const_reference i_node )
        {
            at::PtrDelegate< at::XmlElement* > l_file_elt = i_node->OwnerDocument()->CreateElement( "File" );
            l_file_elt->SetAttribute( "RelativePath", m_relpath );
            i_node->AppendChild( l_file_elt );
        }

private:

    std::string m_relpath;
};

static void scan_source_directory( const at::FilePath& i_dirpath, const FilterMap& i_fmap )
{
    const FilterMap::const_iterator l_fmap_end = i_fmap.end();
    at::DirIterator l_diriter( i_dirpath );

    if ( l_diriter.First() )
    {
        do {
            const at::FilePath& l_name = l_diriter.GetEntry().m_entry_name;
            const FilterMap::const_iterator l_fmap_itr = i_fmap.find( l_name.Extension() );
            if ( l_fmap_itr != l_fmap_end )
            {
                const at::FilePath l_path( i_dirpath / l_name );
                if ( ! at::Directory::Exists( l_path ) )
                {
                    const NodeSet& l_filters = l_fmap_itr->second;
                    std::for_each( l_filters.begin(), l_filters.end(), AddFileElement( l_path.String() ) );
                }
            }
        }
        while ( l_diriter.Next() );
    }
}

// ================================================================================================
// Cleaning up elements by removing blank text child nodes.

struct ElementCleaner : public std::unary_function< at::XmlNodeList::const_reference, void >
{
    static bool IsBlankTextNode( at::PtrView< const at::XmlNode* > i_node )
    {
        if ( i_node && i_node->Type() == at::XmlNode::TEXT_NODE )
        {
            const std::string& l_content = i_node->Value();
            const std::string::const_iterator l_end = l_content.end();
            std::string::const_iterator l_itr = l_content.begin();
            while ( l_itr != l_end && isspace( *l_itr ) ) l_itr++;
            return l_itr == l_end;
        }
        return false;
    }

    void operator() ( at::XmlNodeList::const_reference i_node )
        {
            if ( at::PtrView< at::XmlElement* > l_elt = i_node->AsElement() )
            {
                at::PtrView< at::XmlNode* > l_child = l_elt->FirstChild();
                while ( l_child )
                {
                    if ( IsBlankTextNode( l_child ) )
                    {
                        at::PtrView< at::XmlNode* > l_blank = l_child;
                        l_child = l_child->NextSibling();
                        l_elt->RemoveChild( l_blank );
                    }
                    else
                    {
                        l_child = l_child->NextSibling();
                    }
                }
            }
        }
};


// ================================================================================================
// Main Function

int create_vcproj(int argc, char** argv)
{
	// parse the command line options
	std::auto_ptr<ost::CommandOptionParse> args(
		ost::makeCommandOptionParse(
			argc, argv,
			"[ options ]\n\n"
			"\t1. collects information in the <root_directory>/mxs_cache directory\n"
			"\t2. parses the template VC++ project file\n"
			"\t3. performs macro substitutions in all the attributes\n"
			"\t4. scans the specified source directories and updates the Files elements\n"
			"\nOptions are:\n",
			command_option_list
		)
	);

	// If the user requested help then suppress all the usage error messages.
	if (theHelpArg.numSet) {
		std::cerr << args->printUsage();
		return 0;
	}

	// Print usage your way unless user requested the list of built in macros.
	if (theListMacroArg.numSet)
	{
		if (!theRootDirArg.numValue)
		{
			std::cerr << "Value required for option '--root' is missing\n";
			std::cerr << "Get help by --help\n";
			return 1;
		}
	}
	else
	{
		if (args->argsHaveError())
		{
			std::cerr << args->printErrors();
			std::cerr << "Get help by --help\n";
			return 1;
		}
	}

	// Go off and run any option specific task.
	args->performTask();

	// The directory separator used in paths on the command line.
	char input_path_directory_separator = '/';

	if (theDirectorySeparatorArg.numValue)
	{
		const char* l_argval = theDirectorySeparatorArg.values[0];
		if (strlen(l_argval) != 1)
		{
			error_exit("invalid directory separator", l_argval);
		}
		input_path_directory_separator = l_argval[0];
	}
	at::FilePath::s_directory_separator = '/';

	// The root directory
	at::FilePath root_dir(theRootDirArg.values[0]);

	if (input_path_directory_separator != at::FilePath::s_directory_separator)
	{
		std::string l_root = root_dir.String();
		std::replace(l_root.begin(), l_root.end(), input_path_directory_separator,
			at::FilePath::s_directory_separator);
		root_dir = at::FilePath(l_root);
	}
	root_dir.Clean();

	if (!at::Directory::Exists(root_dir))
	{
		error_exit("invalid directory", root_dir.String());
	}

	// Initialize the macro definition map with the cache information
	MacroDefinitionMap macro_defs;
	at::FilePath cache_dir = root_dir / cache_directory_name;

	if (!at::Directory::Exists(cache_dir))
	{
		warning("cache directory does not exist", cache_dir.String());
	}
	else
	{
		read_cache_info(cache_dir / "work.win32", root_dir, "VC_DEVEL_", macro_defs);
		read_cache_info(cache_dir / "release.win32", root_dir, "VC_RELEASE_", macro_defs);
	}
	macro_defs["VC_DEVEL_BUILDDIR"] = "work.win32";
	macro_defs["VC_RELEASE_BUILDDIR"] = "release.win32";
	macro_defs["VC_USE_ATL"] = "2";
	macro_defs["VC_USE_MFC"] = "0";
	macro_defs["VC_USEPRECOMPILEDHEADER"] = "0";

	if (theListMacroArg.numSet)
	{
		std::cerr << ContForm<MacroDefinitionMap>(macro_defs, "\n") << std::endl;
		return 0;
	}

	// Complete the macro definition map with define and undef arguments.
	for (int i = 0; i < theMacroCancelArg.numValue; i++)
	{
		cancel_macro(theMacroCancelArg.values[i], macro_defs);
	}
	for (int i = 0; i < theMacroDefineArg.numValue; i++)
	{
		define_macro(theMacroDefineArg.values[i], macro_defs);
	}
#ifndef NDEBUG
	if (theDebugArg.numSet)
	{
		std::cerr << "Macro Definition Table\n"
			<< "----------------------\n"
			<< ContForm<MacroDefinitionMap>(macro_defs, "\n")
			<< std::endl;
	}
#endif
	if (theFilterTemplateFileArg.numValue) {
		return create_vcxproj_2008(macro_defs);
	}
	else
	{
		return create_vcproj_pre2008(macro_defs);
	}
}

int create_vcxproj_2008(MacroDefinitionMap& macro_defs) {
	// Parse the main project template file.
	at::Ptr< at::XmlDocument* > l_document;
	try
	{
		l_document = at::XmlParseFile(theTemplateFileArg.values[0]);
	}
	catch (at::ExceptionDerivation< at::XmlParserErr > e)
	{
		error_exit("failed to parse the template project file", e.what());
	}

	at::Ptr< at::XmlDocument* > l_filter_document;
	try
	{
		l_filter_document = at::XmlParseFile(theFilterTemplateFileArg.values[0]);
	}
	catch (at::ExceptionDerivation< at::XmlParserErr > e)
	{
		error_exit("failed to parse the filter template project file", e.what());
	}

	return 1;
}

int create_vcproj_pre2008(MacroDefinitionMap& macro_defs) {
    // Parse the template file
    at::Ptr< at::XmlDocument* > l_document;
    try
    {
        l_document = at::XmlParseFile( theTemplateFileArg.values[0] );
    }
    catch ( at::ExceptionDerivation< at::XmlParserErr > e )
    {
        error_exit( "failed to parse the template project file", e.what() );
    }

    // Get a pointer to the root element, i.e. the VisualStudioProject.
    at::PtrView< at::XmlElement* > l_rootElement = l_document->DocumentElement();
    if ( ! l_rootElement || l_rootElement->TagName() != "VisualStudioProject" )
    {
        error_exit( "the template file does not look like a VisualStudioProject XML document" );
    }

    // Collect the list of all the Element nodes and, since we're using the pretty printer,
    // clean them up from blank Text nodes (which basically are formatting components).
    at::XmlNodeList elt_list = l_rootElement->GetElementsByTagName( "*" );
    std::for_each( elt_list.begin(), elt_list.end(), ElementCleaner() );

    // Go again through all the Element nodes and update their attributes by replacing all
    // occurrences of macro names by the corresponding macro definition.
    std::for_each( elt_list.begin(), elt_list.end(), ElementAttributeUpdater( macro_defs ) );

    // Get the Filter elements - they are the children of the Files element - and build the map
    // that gives the list of Filter elements for a given file extension. File extensions for a
    // given Filter element are found in the Filter attribute of the element.
    FilterMap filter_map;
    at::XmlNodeList filter_elt_list = l_rootElement->GetElementsByTagName( "Filter" );
    std::for_each( filter_elt_list.begin(), filter_elt_list.end(), FilterMapBuilder( filter_map ) );

    // Scan the source directories updating the Filter elements.
    for ( int i = 0; i < theSrcDirArg.numValue; i++ )
    {
        at::FilePath l_src_path( theSrcDirArg.values[ i ] );
        l_src_path.Clean();
        scan_source_directory( l_src_path , filter_map );
    }

    // Print the template back.
    std::ofstream output_file_stream;
	bool use_file = theOutputArg.numValue != 0;
	if (use_file)
	{
		output_file_stream.open(theOutputArg.values[0]);
	}

	auto& out_stream = use_file ? output_file_stream : std::cout;

    at::XmlPrettyPrint( out_stream, l_document );
    if ( output_file_stream.is_open() )
    {
        output_file_stream.close();
    }
    return 0;
}
