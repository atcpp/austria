// -*- c++ -*-

/**
 * \file at_xml_node.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_mxs_tools_h_x
#define x_mxs_tools_h_x 1

#include "at_file.h"
#include "at_dir.h"
#include <string>
#include <fstream>

extern const std::string cache_directory_name;
extern const std::string inc_dirs_cache_file_base_name;
extern const std::string lib_dirs_cache_file_base_name;
extern const std::string lib_names_cache_file_base_name;

void error_exit( const std::string& msg1, const std::string& msg2 = "" );
void warning( const std::string& msg1, const std::string& msg2 = "" );

typedef std::vector< at::FilePath > FilePathVec;

template< class w_type >
struct ContForm
{
    const std::string m_item_sep;
    const w_type&     m_container;

    ContForm( const w_type& i_container, const std::string& i_sep ) :
        m_container( i_container ),
        m_item_sep( i_sep )
        {}
};

template< class w_type >
std::ostream& operator<<( std::ostream& os, const ContForm< w_type >& i_contpp )
{
    const typename w_type::const_iterator l_end = i_contpp.m_container.end();
    typename w_type::const_iterator l_iter = i_contpp.m_container.begin();
    if ( l_iter != l_end )
    {
        os << *l_iter++;
        while ( l_iter != l_end )
        {
            os << i_contpp.m_item_sep << *l_iter++;
        }
    }
    return os;
}

#endif // x_mxs_tools_h_x
