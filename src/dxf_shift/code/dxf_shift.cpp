
#include "cmdoptns.h"

#include <memory>
#include <iostream>
#include <ctime>
#include <sstream>
#include <vector>
#include <fstream>

#include <string.h>

#include "at_string_conv.h"
#include "at_file.h"

#include "dxf_file.h"

#include "at_status_report_basic.h"


typedef long long t_longlong;

// ================================================================================================
// Help and Debug Options

static ost::CommandOptionNoArg help_opt(
    "help",
    "?",
    "Print help usage.",
    false
);

static ost::CommandOptionNoArg debug_opt(
    "debug",
    "d",
    "Debug mode.",
    false
);

static ost::CommandOptionNoArg ascii_opt(
    "ascii",
    "a",
    "Output dxf ascii mode.",
    false
);

static ost::CommandOptionNoArg binary_opt(
    "binary",
    "b",
    "Output dxf binary mode.",
    false
);

static ost::CommandOptionArg    prefix_opt(
    "prefix",
    "p",
    "Prefix to append to dxf file layers and blocks",
    true
);

static ost::CommandOptionRest files_opt(
    "files",
    "=",
    "input file name",
    false
);


class ConversionFailure
{
};

// ======== ParamConv ================================================
/**
 * Convert a command line parameter to value of a particular type
 *
 * @param i_parameter
 * @return nothing
 */

template <typename w_Opt, typename w_Type>
w_Type ParamConv(
    const w_Opt                     & i_param,
    const w_Type                    & i_default,
    const int                       & i_index = 0
) {
    if ( i_param.numValue <= i_index  )
    {
        return i_default;
    }

    try
    {
        return at::ToValue<w_Type>( i_param.values[ i_index ] );
    }
    catch ( const at::ExceptionDerivation< at::StringConversionError > & )
    {
        std::cerr
            << "Failed to convert parameter '--"
            << i_param.optionName
            << "' value '"
            << i_param.values[ i_index ]
            << "'\n";
        throw ConversionFailure();
    }
}

template <typename w_Opt>
const char * ParamConv(
    const w_Opt                     & i_param,
    const char                      * i_default,
    const int                       & i_index = 0
) {
    if ( i_param.numValue <= i_index  )
    {
        return i_default;
    }

    return i_param.values[ i_index ];
}




// ======== PrefixLayersAndBlocks =============================================
/**
 * PrefixLayersAndBlocks will read a DXF file and create a dot file suitable for
 * using with graphviz (dot) to show how the various entities relate to
 * each other using the hard and soft links.
 */

class  PrefixLayersAndBlocks
  : public dxf::TraverseContext
{
    public:

    /**
     * PrefixLayersAndBlocks
     *
     */
    PrefixLayersAndBlocks(
        std::istream                & i_infile,
        std::ostream                & o_outfile,
        std::ostream                & o_errfile,
        const std::string           & i_prefix
    )
      : m_infile( i_infile ),
        m_outfile( o_outfile ),
        m_errfile( o_errfile ),
        m_prefix( i_prefix )
    {
    }

    typedef std::map< std::string, at::PtrView< dxf::Record* > >    t_Map;
    

    typedef std::list< at::PtrView< dxf::Record * > >   t_Stack;

    // ======== ReadDxfFile ===========================================
    /**
     * Perform the conversion.
     *
     *
     */

    bool ReadDxfFile()
    {    

        dxf::ReadFile       l_read_file( m_infile );
        
        at::StatusReport_Basic      l_status;

        m_model = l_read_file.Read( & l_status );

        if ( l_status.m_status_type != at::StatusReport::NoStatus )
        {
            if ( l_status.m_status_type == at::StatusReport::WarningStatus )
            {
                m_errfile << "Warning: " << l_status.m_description << std::endl;
            }
            else
            {
                m_errfile << "Error: " << l_status.m_description << std::endl;
                return false;
            }
        }

        return true;
    }

    

    // ======== PushItem ==============================================
    /**
     * 
     *
     *
     * @param i_record
     */

    virtual void PushRecord( at::PtrView< dxf::Record * >  i_record )
    {

        m_stack.push_back( i_record );
    }


    // ======== PopRecord =============================================
    /**
     * A record is complete
     *
     */

    virtual void PopRecord()
    {
        m_stack.pop_back();
    }


    // ======== PushItem ==============================================
    /**
     * Pushes an item into the context
     *
     * @param i_item
     */

    virtual void PushItem( const dxf::Item & i_item )
    {

        const dxf::GroupCode & l_gc = i_item.GetCode();
        
        at::PtrView< dxf::Record * > l_record = m_stack.back();

        switch ( l_gc.m_group_code )
        {
            case 2 :
            case 3 :
            {
                if ( l_record->m_object_type == "LAYER" || l_record->m_object_type == "INSERT" )
                {
                    if ( l_gc.m_group_code == 3 )
                    {
                        break;
                    }

                    std::string   & l_name = i_item.m_value->Refer();

                    l_name = m_prefix + l_name;
                }
                else if ( l_record->m_object_type == "BLOCK_RECORD" || l_record->m_object_type == "BLOCK" )
                {

                    std::string   & l_name = i_item.m_value->Refer();

                    if ( l_name.length() && '*' != l_name[0] )
                    {
                        l_name = m_prefix + l_name;
                    }
                }
                break;
                
                return;
            }
            case 8 :
            {
                std::string   & l_name = i_item.m_value->Refer();

                l_name = m_prefix + l_name;
                
                break;
            }
            default: return;
        }

    }



    // ======== PerformAll ============================================
    /**
     * Perform all the steps
     *
     */

    bool PerformAll( dxf::GroupCode::ConverterType i_outtype = dxf::GroupCode::DxfBinary16 )
    {
        if ( ! ReadDxfFile() )
        {
            return false;
        }

        m_model->Traverse( this );

        at::Ptr< dxf::WriteFile * > l_wfile = new dxf::StdOstreamWriteFile( m_outfile );

        at::StatusReport_Basic  l_report;
                
        if ( ! m_model->Write( l_wfile, i_outtype, & l_report ) )
        {
            m_errfile << "Error: " << l_report.m_description << std::endl;
            return false;
        }

        return true;
    }
    
    std::istream                    & m_infile;
    std::ostream                    & m_outfile;
    std::ostream                    & m_errfile;
    std::string                       m_prefix;

    bool                              m_writelegend;
    
    at::Ptr< dxf::Model * >         m_model;

    t_Map                           m_map;
    t_Stack                         m_stack;
};



// ======== do_dxfprefix ==================================================
/**
 * Peform a linking
 *
 * @return exit value for the command
 */

int do_dxfprefix()
{

    std::istream     * l_infile = & std::cin;
    
    const char * l_infilename = ParamConv( files_opt, static_cast< const char * >( 0 ) );
    
    std::ifstream       l_file;

    std::ostream        * l_outstream;

    std::ofstream           l_out_file;

    std::string             l_outfilename;

    std::string             l_prefix = ParamConv( prefix_opt, std::string( "DefaultPrefix" ) );
        
    if ( l_infilename )
    {
        l_file.open( l_infilename, std::ios::binary );

        if ( ! l_file )
        {
            std::cerr << "failed to open '" << l_infilename << "' for input\n";
            return 1;
        }

        l_infile = & l_file;

        l_outfilename = l_prefix + l_infilename;
        
        l_out_file.open( l_outfilename.c_str(), std::ios_base::binary | std::ios_base::out );

        if ( ! l_out_file )
        {
            std::cerr << "failed to open '" << l_outfilename << "' for output\n";
            return 1;
        }
        
        l_outstream = & l_out_file;
    }
    else
    {
        // set stdin to binary mode
        if ( ! at::SetBinaryMode( 0 ) )
        {
            std::cerr << "failed to set stdin to binary mode - try saving the file to disk\n";
            return -1;
        }
        
        // set stdout to binary mode
        if ( ! at::SetBinaryMode( 1 ) )
        {
            std::cerr << "failed to set stdout to binary mode - try saving the file to disk\n";
            return -1;
        }
        
        l_outstream = & std::cout;
    }

    PrefixLayersAndBlocks   l_workobj(
        * l_infile,
        * l_outstream,
        std::cerr,
        l_prefix
    );

    dxf::GroupCode::ConverterType l_outtype = dxf::GroupCode::DxfBinary16;

    if ( ascii_opt.numSet )
    {
        l_outtype = dxf::GroupCode::DxfAscii;
    }

    if ( binary_opt.numSet )
    {
        l_outtype = dxf::GroupCode::DxfBinary;
    }

    if ( ! l_workobj.PerformAll( l_outtype ) )
    {
        return 1; // some kind of error happened
    }

    return 0;
}


int main( int argc, char** argv )
{
    // parse the command line options
    std::auto_ptr<ost::CommandOptionParse> args(
        ost::makeCommandOptionParse(
            argc, argv,
            "[ options ] <inpt file>\n"
            "Rename all layers and blocks in by adding a prefix:\n"
            "Options are:\n"
        )
    );
    
    if ( ascii_opt.numSet && binary_opt.numSet )
    {
        std::cerr << "Can't set both ascii and binary mode\n";
        return 1;
    }


    // If the user requested help then suppress all the usage error messages.
    if ( help_opt.numSet ) {
        std::cerr << args->printUsage();
        return 1;
    }

    // If the user requested help then suppress all the usage error messages.
    if ( args->argsHaveError() ) {
        std::cerr << args->printErrors();
        return 1;
    }
    

    if ( files_opt.numValue > 1 )
    {
        std::cerr << "Error: Can't convert more than once file at a time\n" << args->printUsage();
        return 1;
    }

    try
    {
        return do_dxfprefix();
    }
    catch ( const ConversionFailure & )
    {
        // some of the command line parameters were broken
    }
    
    return 0;
};
