
#include "at_thread.h"
#include "at_lifetime.h"
#include "at_buffer.h"
#include "at_factory.h"

#include <list>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <iomanip>

// ======== HPReadTask ================================================
/**
 * HPReadTask is simply a task with a different work thread name.
 *
 */

class HPReadTask
  : public at::Task
{
    public:

    virtual void ReadWork() = 0;

    void Work()
    {
        ReadWork();
    }
};


// ======== HPWriteTask ================================================
/**
 * HPWriteTask is simply a task with a different work thread name.
 *
 */

class HPWriteTask
  : public at::Task
{
    public:

    virtual void WriteWork() = 0;

    void Work()
    {
        WriteWork();
    }
};

// ======== HPCompareTask ================================================
/**
 * HPCompateTask is simply a task with a different work thread name.
 *
 */

class HPCompareTask
  : public at::Task
{
    public:

    virtual void CompareWork() = 0;

    void Work()
    {
        CompareWork();
    }
};


// ======== HPPostable ================================================
/**
 * The HPPostable interface is used to post to a class
 *
 */

class HPPostable
{
    public:

    virtual ~HPPostable(){};

    HPPostable()
      : m_destruct(),
        m_queue_cond( m_mutex ),
        m_completion_cond( m_mutex ),
        m_has_errors( false ),
        m_consumer_quit( false ),
        m_pool( at::FactoryRegister< at::Pool, at::DKy >::Get().Create( "Basic" )() )
    {
        AT_Assert( m_pool );
    }

    // ======== Post ==================================================
    /**
     * Post will wake every thread.
     *
     *
     * @return nothing
     */

    virtual void	Post() = 0;

    /**
     * m_mutex is the mutex for all postable conditions.
     */
    at::Mutex                           m_mutex;

    /**
     * m_queue_cond is for waiters for readers (or writers) on
     * m_queue.
     */
    at::Conditional                     m_queue_cond;

    at::Conditional                     m_completion_cond;

    /**
     * m_destruct is the get-out now, destruction is imminent
     * flag
     */
    volatile bool                       m_destruct;
    
    /**
     * m_has_errors indicates that an error has occurred
     */
    bool                                m_has_errors;

    /**
     * m_consumer_quit is set to true when the consumer
     * is no longer available.
     */
    bool                                m_consumer_quit;

    
    at::Ptr< at::Pool * >               m_pool;
    
};


// ======== CompleteHelper ========================================
/**
 * CompleteHelper manages the completion of the read or
 * write task.
 *
 */

struct CompleteHelper
{

    HPPostable              & m_postable;
    volatile bool           & m_completed_flag;
    
    CompleteHelper(
        HPPostable          & i_postable,
        volatile bool       & i_completed_flag
    )
      : m_postable( i_postable ),
        m_completed_flag( i_completed_flag )
    {
    }

    ~CompleteHelper()
    {
        at::Lock< at::Mutex >        l_lock( m_postable.m_mutex );

        m_completed_flag = true;

        m_postable.Post();
    }

};


// ======== HPReader ==================================================
/**
 * HPReader is a high performance reader class.  Must wait in most
 * derived destructor.
 */

class HPReader
  : public HPReadTask,
    virtual public HPPostable
{
    public:

    const std::string             & m_read_file_name;
    
    unsigned                                m_block_size;
    unsigned                                m_max_queue;

    /**
     * m_readfd is the file descriptor for the read file.
     */
    int                                     m_readfd;

    /**
     * m_queue is the read queue;
     */
    
    std::list< at::Ptr< at::Buffer * > >    m_queue;

    /**
     * m_bytes indicates the number of bytes read
     */
    volatile long long                      m_bytes;

    int                                     m_read_errno;
    
    /**
     * the fstat result of the read file
     */
    struct stat64                           m_read_stat;

    /**
     * m_read_completed indicates true when the read has completed.
     */

    volatile bool                           m_read_completed;

    HPReader(
        const std::string       & i_read_file_name,
        unsigned                  i_block_size = 1<<23,
        unsigned                  i_max_queue = 16,
        long long                 i_seek = 0
    )
      : m_read_file_name( i_read_file_name ),
        m_block_size( i_block_size ),
        m_max_queue( i_max_queue ),
        m_bytes(),
        m_read_completed( false ),
        m_read_errno(),
        m_read_stat()
    {

        m_readfd = ::open(
            m_read_file_name.c_str(),
            O_RDONLY|O_LARGEFILE
        );

        if ( m_readfd < 0 )
        {
            m_read_errno = errno;
            m_has_errors = true;
            return;
        }

        if ( fstat64( m_readfd, & m_read_stat ) )
        {
            m_read_errno = errno;
            m_has_errors = true;
            return;
        }

        if ( i_seek != lseek64( m_readfd, i_seek, SEEK_SET ) )
        {
            m_read_errno = errno;
            m_has_errors = true;
            return;
        }
    }



    // ======== EndOfFile =============================================
    /**
     * EndOfFile indicates that there are no more bytes to be had
     *
     *
     */

    bool EndOfFile()
    {
        return ( m_queue.begin() == m_queue.end() ) && m_read_completed;
    }


    // ======== NotReady ============================================
    /**
     * NotReady indicates that there is no data in the queue but the
     * reader is still incomplete.
     *
     * @return Indicates if any data is available and not complete.
     */

    bool NotReady()
    {
        return ( m_queue.begin() == m_queue.end() ) && ! m_read_completed;
    }

    
    // ======== ReadWork ==============================================
    /**
     *	HPCopy reader thread
     *
     *
     * @return nothing
     */

    void ReadWork()
    {
        CompleteHelper  l_completer( * this, m_read_completed );

        if ( m_read_errno )
        {
            return;
        }
        
        while ( 1 )
        {
            if ( m_consumer_quit || m_destruct )
            {
                return;
            }
            
            at::Ptr< at::Buffer * >     l_buf( m_pool->Create() );

            const at::Buffer::t_Region & l_region = l_buf->SetAvailability( m_block_size );

            int l_result = ::read( m_readfd, l_region.m_mem, m_block_size );

            if ( l_result > 0 )
            {
                l_buf->SetAllocated( l_result );

                {
                    at::Lock< at::Mutex >       l_lock( m_mutex );
    
                    m_bytes += l_result;
    
                    m_queue.push_back( l_buf );
    
                    m_queue_cond.PostAll();
    
                    if ( m_consumer_quit || m_destruct )
                    {
                        return;
                    }
            
                    while ( m_queue.size() >= m_max_queue )
                    {
                        m_queue_cond.Wait();

                        if ( m_consumer_quit || m_destruct )
                        {
                            return;
                        }

                    }
                }
            }
            else
            {
                if ( l_result < 0 )
                {
                    m_read_errno = errno;
                    m_has_errors = true;
                    return;
                }
                // l_completer should do everything else
                return;
            }
        }
    }

};


// ======== HPCopy ====================================================
/**
 * Perform a copy from one device to another in a high performance
 * (multithreaded) way
 */

class HPCopy
  : public HPReader,
    public HPWriteTask
{
    public:

    std::string                             m_write_file_name;

    /**
     * m_writefd is the file descriptor for the write file.
     */
    int                                     m_writefd;

    /**
     * m_consumer_quit indicates that there are no more
     * bytes to write.
     */
    volatile bool                           m_consumer_quit;

    int                                     m_write_errno;

    /**
     * the fstat result of the write file
     */
    struct stat                             m_write_stat;

    long long                               m_splitsize;

    HPCopy(
        const std::string       & i_read_file_name,
        const std::string       & i_write_file_name,
        unsigned                  i_block_size = 1<<23,
        unsigned                  i_max_queue = 16,
        long long                 i_seek = 0,
        long long                 i_splitsize = 0
    )
      : HPReader( i_read_file_name, i_block_size, i_max_queue, i_seek ),
        m_write_file_name( i_write_file_name ),
        m_writefd( -1 ),
        m_consumer_quit( false ),
        m_write_errno(),
        m_write_stat(),
        m_splitsize( i_splitsize )
    {

        if ( ! CreateOutputFile( 0 ) )
        {
            return;
        }
        
        if ( i_seek != lseek64( m_writefd, i_seek, SEEK_SET ) )
        {
            m_write_errno = errno;
            m_has_errors = true;
            return;
        }
            
        HPReadTask::Start();
        HPWriteTask::Start();
    }


    // ======== CreateOutputFile ======================================
    /**
     *	
     *
     * @param i_offset
     * @return nothing
     */

    bool CreateOutputFile( unsigned i_num )
    {

        if ( m_writefd != -1 )
        {
            if ( ::close( m_writefd ) == -1 )
            {
                m_writefd = -1;
                m_write_errno = errno;
                m_has_errors = true;
                return false;
            }
                
            m_writefd = -1;
        }
        
        std::string m_filename( m_write_file_name );
        
        if ( m_splitsize > 0 )
        {
            std::ostringstream  l_stream;

            l_stream << "_" << std::setw( 3 ) << std::setfill( '0' ) << i_num;

            m_filename += l_stream.str();
        }

        m_writefd = ::open(
            m_filename.c_str(),
            O_WRONLY|O_CREAT|O_LARGEFILE,
            m_read_stat.st_mode & 0x1ff
        );

        if ( m_writefd < 0 )
        {
            m_write_errno = errno;
            m_has_errors = true;
            return false;
        }
        
        if ( fstat( m_writefd, & m_write_stat ) )
        {
            m_write_errno = errno;
            m_has_errors = true;
            ::close( m_writefd );
            m_writefd = -1;
            return false;
        }
        
        return true;
    }


    ~HPCopy()
    {
        m_destruct = true;

        {
            at::Lock< at::Mutex >   l_lock( m_mutex );

            Post();
        }

        HPReadTask::Wait();
        HPWriteTask::Wait();

    }

    // ======== Post ==================================================
    /**
     * Post will wake every thread.
     *
     *
     * @return nothing
     */

    virtual void Post()
    {
        m_queue_cond.PostAll();
        m_completion_cond.PostAll();
    }


    // ======== WriteWork =============================================
    /**
     * HPCopy writer thread.
     *
     *
     * @return nothing
     */

    void WriteWork()
    {
        long long       l_file_bytes_written = 0;
        int             l_file_number = 0;
        
        CompleteHelper  l_completer( * this, m_consumer_quit );

        if ( m_write_errno )
        {
            return;
        }

        while ( 1 )
        {
            at::Ptr< at::Buffer * >     l_buf;

            {
                at::Lock< at::Mutex >       l_lock( m_mutex );

                // while nothing in the queue - sit tight
                while ( NotReady() )
                {
                    
                    if ( m_destruct )
                    {
                        return;
                    }

                    m_queue_cond.Wait();
                }

                if ( EndOfFile() )
                {
                    // nothing more to write
                    return;
                }
                
                l_buf = m_queue.front();
                m_queue.pop_front();

                // wake up the reader (if the queue was full)
                m_queue_cond.PostAll();
            }
            
            if ( m_destruct )
            {
                return;
            }
            
            const at::Buffer::t_Region & l_region = l_buf->Region();

            if ( m_splitsize )
            {
                int l_block_bytes_written = 0;
    
                while ( l_block_bytes_written < l_region.m_allocated )
                {
    
                    long long l_file_left = m_splitsize - l_file_bytes_written;
    
                    if ( l_file_left <= 0 )
                    {
                        if ( ! CreateOutputFile( ++ l_file_number ) )
                        {
                            return;
                        }
    
                        l_file_bytes_written = 0;

                        l_file_left = m_splitsize;
                    }
    
                    int l_block_bytes_to_write = l_region.m_allocated - l_block_bytes_written;
    
                    if ( l_block_bytes_to_write > l_file_left )
                    {
                        l_block_bytes_to_write = l_file_left;
                    }
                    
                    int l_result = ::write( m_writefd, l_region.m_mem + l_block_bytes_written, l_block_bytes_to_write );
        
                    if ( l_result != l_block_bytes_to_write )
                    {
                        // this would be an error
                        m_write_errno = errno;
                        m_has_errors = true;
                        return;
                    }

                    l_block_bytes_written += l_block_bytes_to_write;
                    l_file_bytes_written += l_block_bytes_to_write;
                
                }
            }
            else
            {

                int l_result = ::write( m_writefd, l_region.m_mem, l_region.m_allocated );
    
                if ( l_result != l_region.m_allocated )
                {
                    // this would be an error
                    m_write_errno = errno;
                    m_has_errors = true;
                    return;
                }
            }
        }
    }


    // ======== WaitProgress ==========================================
    /**
     *	This will wait for any progress.
     *
     * @return the number of bytes currently read
     */

    long long WaitProgress()
    {
        at::Lock< at::Mutex >       l_lock( m_mutex );

        if ( m_has_errors || ( m_read_completed && m_consumer_quit ) )
        {
            return m_bytes;
        }

        m_queue_cond.Wait();

        return m_bytes;
    }


    // ======== WaitComplete ==========================================
    /**
     * WaitComplete will wait until the reader and the writer have completed
     *
     *
     * @return nothing
     */

    void WaitComplete()
    {
        at::Lock< at::Mutex >       l_lock( m_mutex );

        while ( 1 )
        {
            if ( m_has_errors || ( m_read_completed && m_consumer_quit ) )
            {
                return;
            }

            m_completion_cond.Wait();
        }
    }    


    // ======== IsComplete ============================================
    /**
     * 
     *
     * @return true if complted
     */

    bool IsComplete()
    {
        return ( m_has_errors || ( m_read_completed && m_consumer_quit ) );
    }


    // ======== HasErrors =============================================
    /**
     *	
     *
     *
     * @return nothing
     */

    bool HasErrors()
    {
        return m_has_errors;
    }
    
};


// ======== HPReaderA =================================================
/**
 * HPReaderA is simply a reader class
 *
 */

template <int N>
class HPReaderT
  : public HPReader
{
    public:

    HPReaderT(
        const std::string       & i_read_file_name,
        unsigned                  i_block_size = 1<<23,
        unsigned                  i_max_queue = 16,
        long long                 i_seek = 0
    )
      : HPReader(
            i_read_file_name,
            i_block_size,
            i_max_queue,
            i_seek
        )
    {
    }

};

typedef HPReaderT<1>    HPReaderA;
typedef HPReaderT<2>    HPReaderB;


// ======== HPCompare =================================================
/**
 * HPCompare reads 2 files and compares them
 *
 */

class HPCompare
  : public HPReaderA,
    public HPReaderB,
    public HPCompareTask
{
    public:

    enum Result
    {
        Incomplete,
        FileA_EOF,
        FileB_EOF,
        Differ,
        Equal
    };

    volatile Result              m_result;

    HPCompare(
        const std::string       & i_read_file_nameA,
        const std::string       & i_read_file_nameB,
        unsigned                  i_block_size = 1<<23,
        unsigned                  i_max_queue = 16,
        long long                 i_seek = 0
    )
      : HPReaderA( i_read_file_nameA, i_block_size, i_max_queue, i_seek ),
        HPReaderB( i_read_file_nameB, i_block_size, i_max_queue, i_seek ),
        m_result( Incomplete )
    {
        HPReaderA::Start();
        HPReaderB::Start();
        HPCompareTask::Start();
    }

    ~HPCompare()
    {
        m_destruct = true;

        {
            at::Lock< at::Mutex >   l_lock( m_mutex );

            Post();
        }

        HPReaderA::Wait();
        HPReaderB::Wait();
        HPCompareTask::Wait();

    }

    // ======== Post ==================================================
    /**
     * Post will wake every thread.
     *
     *
     * @return nothing
     */

    virtual void Post()
    {
        m_queue_cond.PostAll();
        m_completion_cond .PostAll();
    }


    // ======== CompareWork ===========================================
    /**
     * CompareWork will read the contents of HPReaderA and HPReaderB
     * and compare the files.  It will terminate on the first detected
     * non common bit.
     *
     * @return nothing
     */

    virtual void CompareWork()
    {

        CompleteHelper  l_completer( * this, m_consumer_quit );

        while ( 1 )
        {
            at::Ptr< at::Buffer * >     l_bufA;
            at::Ptr< at::Buffer * >     l_bufB;

            {
                at::Lock< at::Mutex >       l_lock( m_mutex );

                // while nothing in the queue - sit tight
                while ( HPReaderA::NotReady() )
                {
                    
                    if ( m_destruct )
                    {
                        return;
                    }

                    m_queue_cond.Wait();
                }

                if ( HPReaderA::EndOfFile() )
                {
                    while ( HPReaderB::NotReady() )
                    {
                        
                        if ( m_destruct )
                        {
                            return;
                        }
    
                        m_queue_cond.Wait();
                    }
                    
                    if ( HPReaderB::EndOfFile() )
                    {
                        m_result = Equal;
                        return;
                    }

                    m_result = FileA_EOF;
                        
                    return;
                }
                
                l_bufA = HPReaderA::m_queue.front();
                HPReaderA::m_queue.pop_front();

                // wake up the reader (if the queue was full)
                m_queue_cond.PostAll();
                
                // while nothing in the queue - sit tight
                while ( HPReaderB::NotReady() )
                {
                    
                    if ( m_destruct )
                    {
                        return;
                    }

                    m_queue_cond.Wait();
                }

                if ( HPReaderB::EndOfFile() )
                {
                    // We have a premature end on B
                    m_result = FileB_EOF;
                        
                    return;
                }
                
                l_bufB = HPReaderB::m_queue.front();
                HPReaderB::m_queue.pop_front();

                // wake up the reader (if the queue was full)
                m_queue_cond.PostAll();
            }
            
            if ( m_destruct )
            {
                return;
            }

            const at::Buffer::t_Region & l_regionA = l_bufA->Region();
            const at::Buffer::t_Region & l_regionB = l_bufB->Region();

            int l_result = l_regionA.Compare( l_regionB );

            if ( l_result != 0 )
            {
                m_result = Differ;
                return;
            }

        }

    }
    // ======== WaitProgress ==========================================
    /**
     *	This will wait for any progress.
     *
     * @return the number of bytes currently read
     */

    long long WaitProgress()
    {
        at::Lock< at::Mutex >       l_lock( m_mutex );

        if ( ! ( m_has_errors || m_consumer_quit ) )
        {
            m_queue_cond.Wait();
        }

        return HPReaderA::m_bytes < HPReaderB::m_bytes ? HPReaderA::m_bytes : HPReaderB::m_bytes;
    }


    // ======== WaitComplete ==========================================
    /**
     * WaitComplete will wait until the reader and the writer have completed
     *
     *
     * @return nothing
     */

    void WaitComplete()
    {
        at::Lock< at::Mutex >       l_lock( m_mutex );

        while ( 1 )
        {
            if ( m_has_errors || m_consumer_quit )
            {
                return;
            }

            m_completion_cond.Wait();
        }
    }

    // ======== IsComplete ============================================
    /**
     * 
     *
     * @return true if complted
     */

    bool IsComplete()
    {
        return ( m_has_errors || m_consumer_quit );
    }

    const char * Result()
    {
        switch ( m_result )
        {
            case Incomplete : {
                return "Incomplete";
            }
            case FileA_EOF : {
                return "FileA_EOF";
            }
            case FileB_EOF : {
                return "FileB_EOF";
            }
            case Differ : {
                return "Differ";
            }
            case Equal : {
                return "Equal";
            }
        }
        return "Unknown";
    }
    
    // ======== HasErrors =============================================
    /**
     *	
     *
     *
     * @return nothing
     */

    bool HasErrors()
    {
        return m_has_errors;
    }
    
};

