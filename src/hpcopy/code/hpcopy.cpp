
#include "cmdoptns.h"

#include <memory>
#include <iostream>
#include <ctime>
#include <sstream>

#include <string.h>



#include "hpcopy.h"

typedef long long t_longlong;

// ================================================================================================
// Help and Debug Options

static ost::CommandOptionNoArg help_opt(
    "help",
    "?",
    "Print help usage.",
    false
);

static ost::CommandOptionNoArg debug_opt(
    "debug",
    "d",
    "Debug mode.",
    false
);

static ost::CommandOptionArg block_size_opt(
    "blocksize",
    "b",
    "Number of bytes in requested transfers",
    false
);

static ost::CommandOptionArg seek_opt(
    "seek",
    "s",
    "Number of bytes to seek into files before writing (default 0)",
    false
);

static ost::CommandOptionArg operation_opt(
    "operation",
    "t",
    "The operation type (compare) or (copy)",
    true
);

static ost::CommandOptionArg queue_max_opt(
    "queue_max",
    "q",
    "The maximum number of blocks in the read queue",
    false
);

static ost::CommandOptionArg bytes_opt(
    "bytes",
    "z",
    "Split the file being copied into files of size not more than bytes.",
    false
);

static ost::CommandOptionRest files_opt(
    "files",
    "=",
    "Copy file list",
    true
);


int do_copy()
{
    unsigned l_bs = 1 << 23;

    if ( block_size_opt.numValue > 0 )
    {
        std::istringstream  l_strm( block_size_opt.values[ 0 ] );
        if ( ! ( l_strm >> l_bs ) )
        {
            std::cerr << "Error with blocksize option\n";
            return 1;
        }
    }

    unsigned l_queue_max = 16;
    if ( queue_max_opt.numValue > 0 )
    {
        std::istringstream  l_strm( queue_max_opt.values[ 0 ] );
        if ( ! ( l_strm >> l_queue_max ) )
        {
            std::cerr << "Error with queue_max option\n";
            return 1;
        }

        std::cerr << "Max elements in queue " << l_queue_max << "\n";
    }

    t_longlong l_seek = 0;
    if ( seek_opt.numValue > 0 )
    {
        std::istringstream  l_strm( seek_opt.values[ 0 ] );
        if ( ! ( l_strm >> l_seek ) )
        {
            std::cerr << "Error with seek option\n";
            return 1;
        }

        std::cerr << "Seek to " << l_seek << "\n";
    }

    t_longlong l_split_bytes = 0;
    if ( bytes_opt.numValue > 0 )
    {
        std::istringstream  l_strm( bytes_opt.values[ 0 ] );
        if ( ! ( l_strm >> l_split_bytes ) )
        {
            std::cerr << "Error with bytes option\n";
            return 1;
        }

        std::cerr << "Split into files no larger than " << l_split_bytes << "\n";
    }

    std::cerr << "Copy from '" << files_opt.values[ 0 ]
        << "' to '" << files_opt.values[ 1 ] << "'\n";

    HPCopy      l_copy( files_opt.values[ 0 ], files_opt.values[ 1 ], l_bs, l_queue_max, l_seek, l_split_bytes );

    std::clock_t    l_stamp = std::clock();
    
    t_longlong l_bytes;
    t_longlong l_last_bytes = 0;
    while ( 1 )
    {

        l_bytes = l_copy.WaitProgress();

        if ( l_copy.IsComplete() )
        {
            break;
        }

        std::clock_t    l_now = std::clock();
        
        if ( l_now - l_stamp >= CLOCKS_PER_SEC )
        {
            l_stamp = l_now;

            std::cerr << "\rRead " << l_bytes << " last sec " << (l_bytes - l_last_bytes) << "   ";
            std::cerr.flush();
            l_last_bytes = l_bytes;
        }
    }

    l_copy.WaitComplete();

    std::cerr << "\rFinished reading " << l_bytes << "           " << std::endl;

    if ( l_copy.HasErrors() )
    {
        if ( l_copy.m_read_errno )
        {
            std::cerr << "READ ERRORS" << std::endl;
            std::cerr << strerror( l_copy.m_read_errno ) << std::endl;
        }
        
        if ( l_copy.m_write_errno )
        {
            std::cerr << "WRITE ERRORS" << std::endl;
            std::cerr << strerror( l_copy.m_write_errno ) << std::endl;
        }

        return 1;
    }

    return 0;
}


int do_compare()
{
    unsigned l_bs = 1 << 22;

    if ( block_size_opt.numValue > 0 )
    {
        std::istringstream  l_strm( block_size_opt.values[ 0 ] );
        if ( ! ( l_strm >> l_bs ) )
        {
            std::cerr << "Error with blocksize option\n";
            return 1;
        }
    }

    t_longlong l_seek = 0;
    if ( seek_opt.numValue > 0 )
    {
        std::istringstream  l_strm( seek_opt.values[ 0 ] );
        if ( ! ( l_strm >> l_seek ) )
        {
            std::cerr << "Error with seek option\n";
            return 1;
        }

        std::cerr << "Seek to " << l_seek << "\n";
    }

    std::cerr << "Compare '" << files_opt.values[ 0 ]
        << "' and '" << files_opt.values[ 1 ] << "'\n";

    HPCompare      l_compare( files_opt.values[ 0 ], files_opt.values[ 1 ], l_bs, 16, l_seek );

    std::clock_t    l_stamp = std::clock();
    
    t_longlong l_bytes;
    t_longlong l_last_bytes = 0;
    while ( 1 )
    {

        l_bytes = l_compare.WaitProgress();

        if ( l_compare.IsComplete() )
        {
            break;
        }

        std::clock_t    l_now = std::clock();
        
        if ( l_now - l_stamp >= CLOCKS_PER_SEC )
        {

            double l_persec = (l_bytes - l_last_bytes);
            l_persec = ( l_persec * CLOCKS_PER_SEC )/ (l_now - l_stamp);

            t_longlong l_ipersec = t_longlong( l_persec );

            std::cerr << "\rRead " << l_bytes << " last sec " << l_ipersec << "   ";
            std::cerr.flush();
            l_last_bytes = l_bytes;
            
            l_stamp = l_now;
        }
    }

    l_compare.WaitComplete();

    std::cerr << "\rFinished reading " << l_bytes << "           " << std::endl;
    std::cerr << "Result - " << l_compare.Result() << std::endl;

    if ( l_compare.HasErrors() )
    {
        std::cerr << "Failed to open either or both files\n";
        return 1;
    }

    return l_compare.m_result != HPCompare::Equal;
}

int main( int argc, char** argv )
{
    // parse the command line options
    std::auto_ptr<ost::CommandOptionParse> args(
        ost::makeCommandOptionParse(
            argc, argv,
            "[ options ] <read device> <write device>\n"
            "Copy data from <read device> to <write device> in a high performance way.\n"
            "Options are:\n"
        )
    );

    // If the user requested help then suppress all the usage error messages.
    if ( help_opt.numSet ) {
        std::cerr << args->printUsage();
        return 0;
    }

    if ( files_opt.numValue != 2 )
    {
        std::cerr << "Need exactly 2 filenames: " << args->printUsage();
        return 0;
    }

    if ( ( operation_opt.numValue == 0 ) || ( 0 == ::strcmp( operation_opt.values[0], "copy" ) ) )
    {
        return do_copy();
    }
    
    if ( 0 == ::strcmp( operation_opt.values[0], "compare" ) )
    {
        return do_compare();
    }

    std::cerr << "Expected -t copy or -t compare\n";
    std::cerr << args->printUsage();

    return 1;
};
