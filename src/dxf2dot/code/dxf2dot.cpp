
#include "cmdoptns.h"

#include <memory>
#include <iostream>
#include <ctime>
#include <sstream>
#include <vector>
#include <fstream>

#include <string.h>

#include "at_string_conv.h"
#include "at_file.h"

#include "dxf_file.h"

#include "at_status_report_basic.h"


typedef long long t_longlong;

// ================================================================================================
// Help and Debug Options

static ost::CommandOptionNoArg help_opt(
    "help",
    "?",
    "Print help usage.",
    false
);

static ost::CommandOptionNoArg debug_opt(
    "debug",
    "d",
    "Debug mode.",
    false
);

static ost::CommandOptionArg    out_opt(
    "out",
    "o",
    "Dot output file",
    false
);

static ost::CommandOptionArg    width_opt(
    "width",
    "w",
    "Width size parameter",
    false
);

static ost::CommandOptionArg    height_opt(
    "height",
    "h",
    "Height size parameter",
    false
);

static ost::CommandOptionNoArg    legend_opt(
    "legend",
    "l",
    "Output Legend to DOT file",
    false
);

static ost::CommandOptionRest files_opt(
    "files",
    "=",
    "input file name",
    false
);


class ConversionFailure
{
};

// ======== ParamConv ================================================
/**
 * Convert a command line parameter to value of a particular type
 *
 * @param i_parameter
 * @return nothing
 */

template <typename w_Opt, typename w_Type>
w_Type ParamConv(
    const w_Opt                     & i_param,
    const w_Type                    & i_default,
    const int                       & i_index = 0
) {
    if ( i_param.numValue <= i_index  )
    {
        return i_default;
    }

    try
    {
        return at::ToValue<w_Type>( i_param.values[ i_index ] );
    }
    catch ( const at::ExceptionDerivation< at::StringConversionError > & l_except  )
    {
        std::cerr
            << "Failed to convert parameter '--"
            << i_param.optionName
            << "' value '"
            << i_param.values[ i_index ]
            << "'\n";
        throw ConversionFailure();
    }
}

template <typename w_Opt>
const char * ParamConv(
    const w_Opt                     & i_param,
    const char                      * i_default,
    const int                       & i_index = 0
) {
    if ( i_param.numValue <= i_index  )
    {
        return i_default;
    }

    return i_param.values[ i_index ];
}


// ======== LinkType ==================================================
/**
 * 
 *
 */

struct LinkType
{
    const char * const m_desc;
    const char * const m_color;
};

LinkType    g_link_types[] = {
    { "Plot Stylename", "white" },
    { "Soft Owner", "cyan" },
    { "Soft Pointer", "blue" },
    { "Hard Owner", "magenta" },
    { "Hard Pointer", "red" },
    { "Dimvar Object Handle", "0.4  1 0.4" },
    { "Part Of", "yellow", },
};

typedef LinkType * LinkTypes;
typedef LinkType * const CLinkTypes;

CLinkTypes C_plotstylename_handle_value = g_link_types + 0;
CLinkTypes C_soft_owner = g_link_types + 1;
CLinkTypes C_soft_pointer = g_link_types + 2;
CLinkTypes C_hard_owner = g_link_types + 3;
CLinkTypes C_hard_pointer = g_link_types + 4;
CLinkTypes C_dimvar_object_handle = g_link_types + 5;
CLinkTypes C_part_of = g_link_types + 6;

const char * g_bgcolor = "black";

const char s_legend_hdr[] =
"    subgraph cluster_1 {\n"
"        label = \"LEGEND\";\n"
"        node [width=1 height=0.5 label=\"\"];\n"
"        color=blue\n";

const char s_legend_ftr[] =
"    }\n";

const char s_legend_lbl[] = "label = ";



// ======== WriteLegend ===============================================
/**
 *	
 *
 *
 * @param o_str - The stream to write legend to
 * @return nothing
 */

void WriteLegend( std::ostream & o_str )
{
    o_str.write( s_legend_hdr, AT_CountElementsStatic(s_legend_hdr)-1 );
    
    for (
        LinkType * l_link = g_link_types;
        l_link < AT_ArrayEnd(g_link_types);
        ++ l_link
    )
    {

        o_str << "        A" << l_link << "->" << "B" << l_link << " [ label = \"" << l_link->m_desc << "\" fontcolor = \"" << l_link->m_color << "\" color = \"" << l_link->m_color << "\" ]\n";
    }
    
    o_str.write( s_legend_ftr, AT_CountElementsStatic(s_legend_ftr)-1 );
    
}



// ======== GraphADxf_Red =============================================
/**
 * GraphADxf_Red will read a DXF file and create a dot file suitable for
 * using with graphviz (dot) to show how the various entities relate to
 * each other using the hard and soft links.
 */

class  GraphADxf_Red
  : public dxf::TraverseContext
{
    public:

    /**
     * GraphADxf_Red
     *
     */
    GraphADxf_Red(
        std::istream                & i_infile,
        std::ostream                & o_outfile,
        std::ostream                & o_errfile,
        double                        i_width,
        double                        i_height
    )
      : m_infile( i_infile ),
        m_outfile( o_outfile ),
        m_errfile( o_errfile ),
        m_width( i_width ),
        m_height( i_height ),
        m_writelegend( false )
    {
    }

    typedef std::map< std::string, at::PtrView< dxf::Record* > >    t_Map;


    // ======== EdgeItem ==============================================
    /**
     * 
     *
     */

    class EdgeItem
    {
        public:

        /**
         * EdgeItem
         *
         */
        EdgeItem(
            at::PtrView< dxf::Record* >       i_from,
            at::PtrView< dxf::Record* >       i_torec,
            const char                      * i_color
        )
         :  m_from( i_from ),
            m_torec( i_torec ),
            m_color( i_color )
        {
        }

        EdgeItem(
            at::PtrView< dxf::Record* >       i_from,
            const t_Map::iterator           & i_to,
            const char                      * i_color
        )
         :  m_from( i_from ),
            m_to( i_to ),
            m_color( i_color )
        {
        }


        // ======== RecordToStr =======================================
        /**
         *	
         *
         * @return nothing
         */

        std::string RecordToStr(
            at::PtrView< dxf::Record* > i_record
        ) {
            std::string l_second;

            if ( i_record->m_object_type == "LAYOUT" )
            {
                i_record->Find<100,1>( std::string("AcDbLayout"), l_second, "NF" );
            }
            else if ( i_record->m_object_name.empty() )
            {
                l_second = i_record->m_object_handle;
            }
            else
            {
                l_second = i_record->m_object_name;
            }

            return i_record->m_object_type + "_" + l_second;;
        }

        // ======== FromStr ===========================================
        /**
         * Create a string that indicates the record doing the pointing
         *
         * @return A string
         */

        std::string FromStr()
        {
            return RecordToStr( m_from );
        }

        // ======== ToStr =============================================
        /**
         * Create a string that indicates the record being pointed to
         *
         *
         * @return A string
         */

        std::string ToStr()
        {
            if ( m_torec )
            {
                return RecordToStr(  m_torec);
            }
            
            if ( m_to->second )
            {
                return RecordToStr( m_to->second );
            }

            return m_to->first + "*NOT-FOUND";
        }
        
        
        at::PtrView< dxf::Record* >       m_from;
        at::PtrView< dxf::Record* >       m_torec;
        const t_Map::iterator             m_to;
        const char                      * m_color;

    };
    

    typedef std::list< EdgeItem >                       t_List;

    typedef std::list< at::PtrView< dxf::Record * > >   t_Stack;

    // ======== ReadDxfFile ===========================================
    /**
     * Perform the conversion.
     *
     *
     */

    bool ReadDxfFile()
    {    

        dxf::ReadFile       l_read_file( m_infile );
        
        at::StatusReport_Basic      l_status;

        m_model = l_read_file.Read( & l_status );

        if ( l_status.m_status_type != at::StatusReport::NoStatus )
        {
            if ( l_status.m_status_type == at::StatusReport::WarningStatus )
            {
                m_errfile << "Warning: " << l_status.m_description << std::endl;
            }
            else
            {
                m_errfile << "Error: " << l_status.m_description << std::endl;
                return false;
            }
        }

        return true;
    }

    

    // ======== PushItem ==============================================
    /**
     * 
     *
     *
     * @param i_record
     */

    virtual void PushRecord( at::PtrView< dxf::Record * >  i_record )
    {

        // add a link from the new record and the previous one (if it exists)
        if ( ! m_stack.empty() )
        {
            m_list.push_back( EdgeItem( m_stack.back(), i_record, C_part_of->m_color ) );
            
        }
        
        m_stack.push_back( i_record );
    }


    // ======== PopRecord =============================================
    /**
     * A record is complete
     *
     */

    virtual void PopRecord()
    {
        m_stack.pop_back();
    }


    // ======== PushItem ==============================================
    /**
     * Pushes an item into the context
     *
     * @param i_item
     */

    virtual void PushItem( const dxf::Item & i_item )
    {

        LinkTypes  l_color;

        switch ( i_item.GetCode().m_semantic )
        {
            case dxf::entity_handle :
            {
                const std::string   & l_handle = i_item.m_value->Refer();
                at::PtrView< dxf::Record * > l_record = m_stack.back();
                std::pair<t_Map::iterator, bool> l_insert_result = m_map.insert( t_Map::value_type( l_handle, l_record ) );

                if ( l_insert_result.second )
                {
                    return;
                }

                if ( l_insert_result.first->second )
                {
                    // this should not happen
                    m_errfile << "dxf file error: handle describes more than one record: '" << l_handle << "'\n";
                    return;
                }

                l_insert_result.first->second = l_record;
                
                return;
            }
            case dxf::plotstylename_handle_value :
            {
                l_color = C_plotstylename_handle_value;
                break;
            }
            case dxf::soft_owner :
            {
                l_color = C_soft_owner;
                break;
            }
            case dxf::soft_pointer :
            {
                l_color = C_soft_pointer;
                break;
            }
            case dxf::hard_owner :
            {
                l_color = C_hard_owner;
                break;
            }
            case dxf::hard_pointer :
            {
                l_color = C_hard_pointer;
                break;
            }
            case dxf::dimvar_object_handle :
            {
                l_color = C_dimvar_object_handle;
                break;
            }
            default: return;
        }

        at::PtrView< dxf::Record * > l_record = m_stack.back();
        
        const std::string   & l_handle = i_item.m_value->Refer();

        // add a new entry to the list
        std::pair<t_Map::iterator, bool> l_insert_result = m_map.insert( t_Map::value_type( l_handle, 0 ) );

        m_list.push_back( EdgeItem( l_record, l_insert_result.first, l_color->m_color ) );
    }


    // ======== DumpText ==============================================
    /**
     * Write the '.dot' text of the listed edges.
     *
     *
     * @return nothing
     */

    void DumpListText()
    {
        t_List::iterator            l_itr = m_list.begin();
        const t_List::iterator      l_end = m_list.end();

        while ( l_itr != l_end )
        {
            EdgeItem    & l_item = * l_itr;

            m_outfile << "    \"" << l_item.FromStr() << "\" -> \"" << l_item.ToStr() << "\" [color=\"" << l_item.m_color << "\"];\n";

            ++ l_itr;
        }
    }


    // ======== DumpText ==============================================
    /**
     * Dump the '.dot' file
     *
     */

    void DumpText()
    {
        m_outfile <<
            "digraph prof {\n"
            "    graph [bgcolor=\"" << g_bgcolor << "\"];\n"
            "    size=\"" << m_width << "," << m_height << "\"; ratio = fill;\n"
            "    node [style=filled];\n";

        if ( m_writelegend )
        {
            WriteLegend( m_outfile );
        }

        DumpListText();

        m_outfile << "}\n";
    }


    // ======== PerformAll ============================================
    /**
     * Perform all the steps
     *
     */

    bool PerformAll()
    {
        if ( ! ReadDxfFile() )
        {
            return false;
        }

        m_model->Traverse( this );

        DumpText();

        return true;
    }
    
    std::istream                    & m_infile;
    std::ostream                    & m_outfile;
    std::ostream                    & m_errfile;
    double                            m_width;
    double                            m_height;

    bool                              m_writelegend;
    
    at::Ptr< dxf::Model * >         m_model;

    t_Map                           m_map;
    t_List                          m_list;
    t_Stack                         m_stack;
};



// ======== do_dxf2dot ==================================================
/**
 * Peform a linking
 *
 * @return exit value for the command
 */

int do_dxf2dot()
{

    std::istream     * l_infile = & std::cin;
    
    const char * l_infilename = ParamConv( files_opt, static_cast< const char * >( 0 ) );
    
    std::ifstream       l_file;

    if ( l_infilename )
    {
        l_file.open( l_infilename, std::ios::binary );

        if ( ! l_file )
        {
            std::cerr << "failed to open '" << l_infilename << "' for input\n";
            return 1;
        }

        l_infile = & l_file;
    }
    else
    {
        // set stdin to binary mode
        if ( ! at::SetBinaryMode( 0 ) )
        {
            std::cerr << "failed to set stdin to binary mode - try saving the file to disk\n";
            return -1;
        }
    }

    std::ostream        * l_outstream;

    std::ofstream         l_out_file;

    if ( out_opt.numValue == 0 )
    {
        l_outstream = & std::cout;
    }
    else
    {
        l_out_file.open( out_opt.values[ 0 ] );

        if ( ! l_out_file )
        {
            std::cerr << "failed to open '" << out_opt.values[ 0 ] << "' for output\n";
            return 1;
        }
        
        l_outstream = & l_out_file;
    }

    GraphADxf_Red   l_workobj(
        * l_infile,
        * l_outstream,
        std::cerr,
        ParamConv( width_opt, 20.0 ),
        ParamConv( height_opt, 20.0 )
    );

    if ( legend_opt.numSet )
    {
        l_workobj.m_writelegend = true;
    }

    if ( ! l_workobj.PerformAll() )
    {
        return 1; // some kind of error happened
    }

    return 0;
}


int main( int argc, char** argv )
{
    // parse the command line options
    std::auto_ptr<ost::CommandOptionParse> args(
        ost::makeCommandOptionParse(
            argc, argv,
            "[ options ] <inpt file>\n"
            "Convert the dxf file entity relationships to a Graphviz dot format:\n"
            "Options are:\n"
        )
    );

    // If the user requested help then suppress all the usage error messages.
    if ( help_opt.numSet ) {
        std::cerr << args->printUsage();
        return 0;
    }

    // If the user requested help then suppress all the usage error messages.
    if ( args->argsHaveError() ) {
        std::cerr << args->printErrors();
        return 0;
    }
    

    if ( files_opt.numValue > 1 )
    {
        std::cerr << "Error: Can't convert more than once file at a time\n" << args->printUsage();
        return 0;
    }

    try
    {
        return do_dxf2dot();
    }
    catch ( const ConversionFailure & )
    {
        // some of the command line parameters were broken
    }
    
    return 1;
};
