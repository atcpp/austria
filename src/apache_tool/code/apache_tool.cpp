
#include "cmdoptns.h"
#include "at_string_conv.h"
#include "at_file.h"

#include <memory>
#include <iostream>
#include <ctime>
#include <sstream>

#include <string.h>




const char svn_user_default[] = "apache_svn";


// ================================================================================================
// Help and Debug Options

static ost::CommandOptionNoArg help_opt(
    "help",
    "?",
    "Print help usage.",
    false
);

static ost::CommandOptionNoArg restart_opt(
    "restart",
    "r",
    "Restart apache server",
    false
);

static ost::CommandOptionNoArg reload_opt(
    "reload",
    "",
    "Reload config file for apache server",
    false
);

static ost::CommandOptionNoArg start_opt(
    "start",
    "s",
    "Start apache server",
    false
);

static ost::CommandOptionNoArg stop_opt(
    "stop",
    "x",
    "Stop apache server",
    false
);

static ost::CommandOptionArg destination_opt(
    "destination",
    "d",
    "Symbolic link destination",
    false
);

static ost::CommandOptionArg webloc_opt(
    "webloc",
    "w",
    "Web location for symbolic link",
    false
);

static ost::CommandOptionArg svn_update_dir_opt(
    "svn_update_dir",
    "u",
    "Subversion update directory",
    false
);

static ost::CommandOptionArg svn_co_dir_opt(
    "svn_co_dir",
    "",
    "Subversion checkout to directory - must have --svn_co_url",
    false
);

static ost::CommandOptionArg svn_co_url_opt(
    "svn_co_url",
    "",
    "Subversion checkout from URL - must have --svn_co_dir",
    false
);


static ost::CommandOptionNoArg svn_nofork_opt(
    "svn_nofork",
    "",
    "Don't background the update process",
    false
);

static ost::CommandOptionArg svn_user_opt(
    "svn_user",
    "a",
    "svn user to use in svn update",
    false
);

static ost::CommandOptionArg svn_pass_opt(
    "svn_pass",
    "",
    "svn password to use",
    false
);

static ost::CommandOptionArg svn_pause_opt(
    "pause",
    "",
    "Pause time in seconds before running svn update",
    false
);

const char apache_script[] = "/etc/init.d/httpd";

unsigned seconds = 5;

#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <sys/wait.h>
#include <grp.h>

#include <pwd.h>



// ======== ParamConv ================================================
/**
 * Convert a command line parameter to value of a particular type
 *
 * @param i_parameter
 * @return nothing
 */

template <typename w_Type>
w_Type ParamConv(
    const ost::CommandOptionArg     & i_param,   
    const w_Type                    & i_default,
    const int                       & i_index = 0
) {
    if ( i_param.numValue <= i_index  )
    {
        return i_default;
    }

    return at::ToValue<w_Type>( i_param.values[ i_index ] );
}


bool get_uid( const char * i_name, uid_t & o_uid, gid_t & o_gid, std::string & o_home )
{
    passwd              l_passwd = passwd();
    
    char                l_buf[ 1024 ];
    passwd            * l_ppasswd = 0;

    int err = getpwnam_r( i_name, & l_passwd, l_buf, sizeof( l_buf ), & l_ppasswd );

    if ( ! l_ppasswd )
    {
        perror( "getpwnam_r failed" );
        return false;
    }

    o_uid = l_ppasswd->pw_uid;

    o_gid = l_ppasswd->pw_gid;

    o_home = l_ppasswd->pw_dir;

    return true;
}

bool http_fork( const char * i_cmd )
{

    const pid_t l_child = fork();

    if ( l_child < 0 )
    {
        std::perror( "Can't fork" );
        exit( 1 );
    }

    if ( l_child > 0 )
    {
        int     l_status;
        pid_t l_waitid = waitpid( l_child, & l_status, 0 );

        if ( WIFEXITED(l_status) )
        {
            return WEXITSTATUS(l_status) == 0;
        }
        return false;
    }

    if ( false )
    {
        const pid_t l_child2 = fork();
    
        if ( l_child2 < 0 )
        {
            std::perror( "Can't fork" );
            exit( 1 );
        }
    
        if ( l_child2 > 0 )
        {
            exit( 0 );
        }
    }

    execl( apache_script, apache_script, i_cmd, 0 );

    exit( 1 );

    return false;
}
        
void http_stop(  )
{
    http_fork( "stop" );
}

void http_start(  )
{
    http_fork( "start" );
}

void http_restart(  )
{
    http_fork( "restart" );
}

void http_reload(  )
{
    http_fork( "reload" );
}

void dosymlink( const char * i_src, const char * i_dest )
{
    std::string l_dest = std::string( "/var/www/html/" ) + std::string( i_dest );
    
    if ( symlink( i_src, l_dest.c_str() ) < 0 )
    {
        perror( "Symlink failure\n" );
    }
}


// ======== SvnUpdateNegotiate ========================================
/**
 * This allows negotiation of an svn update.
 *
 * This reliably negotiates a process in which two or more processes
 * can reliably allide a number of requests to a time-consuming
 * "svn update" request.
 *
 * This ....Two "lock" are used to negotiate the process.  The first svn update request will
 * attempt to lock the file ".lockfile" and also create ".lockdoit" file and then proceed
 * to wait a specified time after which an svn update is initiated.
 *
 */

class SvnUpdateNegotiate
{
    public:
    
    at::FilePath    m_dirname;
    at::FilePath    m_lockfilename;

    std::string     m_user;
    std::string     m_pass;
    uid_t           m_userid;
    gid_t           m_groupid;
    std::string     m_home;

    at::RWFile      m_lockfile;
    
    pid_t           m_child;

    bool            m_open_success;
    
    /**
     * SvnUpdateNegotiate
     *
     */
    SvnUpdateNegotiate(
        const char          * i_dir,
        const char          * i_user,
        const char          * i_pass,
        uid_t                 i_uid,
        gid_t                 i_gid,
        const std::string   & i_home
    )
      : m_dirname( i_dir ),
        m_lockfilename( m_dirname / ".lockfile" ),
        m_user( i_user ),
        m_pass( i_pass ),
        m_userid( i_uid ),
        m_groupid( i_gid ),
        m_home( i_home ),
        m_child(),
        m_open_success( OpenOrCreateFile( m_lockfilename, m_lockfile ) )
    {
    }


    // ======== OpenOrCreateFile ======================================
    /**
     * This will open a file and create it if it does not already
     * exist.
     *
     * @param i_filename - The file name
     * @param o_file - The at::WFile object to open
     * @return nothing
     */

    static bool OpenOrCreateFile(
        const at::FilePath  & i_filename,
        at::RWFile          & o_file
    ) {
        do {
            if ( ! o_file.Open( i_filename, at::FileAttr::New ) )
            {
                if ( o_file.GetError() == at::FileError::s_already_exists )
                {
                    if ( o_file.Open( i_filename, at::FileAttr::Preserve ) )
                    {
                        break;
                    }
                }
    
                std::cerr << "Failed to create/open file " << i_filename.StlString() << " : " << o_file.GetError().What() << "\n";
    
                return false;
            }
        } while (false);

        return true;
    }


    // ======== WaitForCompletion =====================================
    /**
     * This will wait for the completion of the process.
     *
     *
     */

    int WaitForCompletion()
    {
        int     l_status;
        pid_t l_waitid = waitpid( m_child, & l_status, 0 );

        

        if ( WIFEXITED(l_status) )
        {
            return WEXITSTATUS(l_status);
        }
        return -1;
    }

    

    // ======== DeferredSync ==========================================
    /**
     * DeferredSync will be called by multiple processes at a time.
     * One of the processes will wait for a specified amount of time
     * and return true from DeferredSync.  Others will return false
     * immediatly.
     */

    bool DeferredSync( const at::TimeInterval & i_interval )
    {
        if ( m_child != pid_t() )
        {
            std::cerr << "Can't use this object more than once\n";
            return false;
        }
        
        // do all this in a forked process.
        pid_t m_child = fork();
    
        if ( m_child < 0 )
        {
            std::perror( "Can't fork" );
            return false;
        }
    
        if ( m_child > 0 )
        {
            return true;
        }

        if ( DeferredSyncProcess( i_interval ) )
        {
            exit( 0 );
        }

        exit( 1 );
    }

    // ======== DeferredSyncProcess ===============================
    /**
     *
     *
     * @param i_interval The time to wait before update
     * @return true on success
     */

    bool DeferredSyncProcess( const at::TimeInterval & i_interval )
    {        

        // become the user we want to be
        setgid( m_groupid );
        initgroups( m_user.c_str(), m_groupid );
        setuid( m_userid );
    
        at::TimeStamp               l_last_update( at::ThreadSystemTime() );
        at::TimeStamp               l_ts( l_last_update );

        // make a fake "last update" time in the past
        l_last_update -= at::TimeInterval::Secs(1);
        
        at::TimeStamp               l_ts_file( l_ts );
        at::Buffer::t_Region        l_br_file(
            reinterpret_cast<char*>(& l_ts_file.m_time),
            sizeof( l_ts_file.m_time ),
            sizeof( l_ts_file.m_time )
        );
        
        at::Buffer::t_Region        l_rbr_file(
            reinterpret_cast<char*>(& l_ts_file.m_time),
            0,
            sizeof( l_ts_file.m_time )
        );

        // start by locking the region - this will wait until the lock is attained
        at::FileLockRange   l_filelock1( m_lockfile, 0, sizeof( i_interval.m_timeinterval ), true );
        
        // this is a deferred lock (lock it later)
        at::FileLockRange   l_filelock2( m_lockfile, sizeof( i_interval.m_timeinterval ), sizeof( i_interval.m_timeinterval )+1, false, true );

        if ( ! l_filelock1.IsLocked() )
        {
            std::cerr << "Failed to lock file " << m_lockfilename.StlString() << " : " << m_lockfile.GetError().What() << "\n";
            return false;
        }

        // start the negotiation loop
        do {

            l_rbr_file.m_allocated = 0;
            // read the previous time from the file if any
            m_lockfile.Seek( 0, at::BaseFile::SeekBegin );
            if ( ! m_lockfile.Read( l_rbr_file ) )
            {
                // assume the file is empty
                return false;
            }

            // check to see what we read from the file.
            bool    l_write_new_time = true;
            if ( l_rbr_file.m_allocated == sizeof( l_ts_file.m_time ) )
            {
                if ( l_ts > l_ts_file )
                {
                    l_ts_file = l_ts;

                }
                else
                {
                    // someone wrote a later time to the file
                    l_write_new_time = false;
                }
            }
            else
            {
                l_ts_file = l_ts;
            }

            // write the time stamp to the file if
            // the file has an earlier timestamp
            if ( l_write_new_time )
            {
                l_rbr_file.m_allocated = sizeof( l_ts_file.m_time );
                m_lockfile.Seek( 0, at::BaseFile::SeekBegin );
                if ( ! m_lockfile.Write( l_rbr_file ) )
                {
                    std::cerr << "Failed to write to file " << m_lockfilename.StlString() << " : " << m_lockfile.GetError().What() << "\n";
                    return false;
                }
            }

            // time has been written - now try to lock the second region
            if ( ! l_filelock2.Lock() )
            {
                // OK we have someone else holding onto the lock - cool - we're done.
                // because they're going to read the time out of the file before
                // running the update.
                return false;
            }


            // check to see we have not already done an update.
            // note - both locks are held here
            if ( l_last_update < l_ts_file )
            {
                // now we unlock the first lock and chill here for the prescribed period (if any)
                l_filelock1.Unlock();
            
                at::TimeStamp   l_wait_to = l_ts_file + i_interval;
    
                at::TimeStamp   l_now( at::ThreadSystemTime() );
    
                if ( l_wait_to < l_now )
                {
                    if ( ! PerformUpdate() )
                    {
                        return false;
                    }
                    l_last_update = l_ts_file;
                }
                else
                {
                    // wait here for the specified time
        
                    at::Task::Sleep( l_wait_to );
                }
            
            }
            else
            {
                // update has already been done - we're over
                // this should unlock lock 2 followed by unlocking lock1
                // (must do it in that order!)
                return true;
            }

            // we're done with lock2 - we go and start all over again
            l_filelock2.Unlock();


            // need to wait here to get lock1 again
            l_filelock1.Lock( true );
            
        } while ( true );
        

    }


    // ======== PerformUpdate =========================================
    /**
     *	This performs the appropriate subversion update.
     *
     * @return nothing
     */

    bool PerformUpdate()
    {
        
        // do all this in a forked process.
        pid_t l_child = fork();
    
        if ( l_child < 0 )
        {
            std::perror( "Can't fork" );
            return false;
        }
    
        if ( l_child > 0 )
        {
            int     l_status;
            pid_t l_waitid = waitpid( l_child, & l_status, 0 );
    
            if ( WIFEXITED(l_status) )
            {
                return WEXITSTATUS(l_status) == 0;
            }
            return false;
        }

        if ( 0 != chdir( m_dirname.StlString().c_str() ) )
        {
            std::cerr << "Failed to chdir to '" << m_dirname.StlString() << "'\n";
            return false;
        }

        setenv( "HOME", m_home.c_str(), 1 );

        std::string l_svn_cmd = "svn update";

        if ( m_pass.length() )
        {
            l_svn_cmd = l_svn_cmd + " --password '" + m_pass + "'";
        }

        execl( "/bin/bash", "/bin/bash", "-l", "-c", l_svn_cmd.c_str(), reinterpret_cast<const char *>(0) );

        perror( "failed to run /bin/bash" );

        exit( 1 );
        return true;
    }


};


// ======== dosvnupdate ===============================================
/**
 * Perform an SVN update as a user.  This does an "su -l -c 'cd DIR; svn update'".
 *
 *
 * @param i_dir Directory to be updated
 * @param i_user Username
 * @return nothing
 */

bool dosvnupdate(
    const char        * i_dir,
    const char        * i_name,
    const char        * i_pass,
    uid_t               i_uid,
    gid_t               i_gid,
    const std::string & i_home,
    int                 i_seconds
)
{
    
    SvnUpdateNegotiate  l_svnneg( i_dir, i_name, i_pass, i_uid, i_gid, i_home );

    if ( !l_svnneg.m_open_success )
    {
        return false;
    }

    // This should spawn a new process and return immediatly - hopefully it succeeds !
    //
    if ( ! svn_nofork_opt.numSet )
    {
        return l_svnneg.DeferredSync( at::TimeInterval::Secs( i_seconds ) );
    }
    else
    {
        return l_svnneg.DeferredSyncProcess( at::TimeInterval::Secs( i_seconds ) );
    }
    
}


// ======== dosvncheckout =============================================
/**
 * dosvncheckout performs an svn checkout under a different user (if possible)
 *
 * @return true if successful
 */

bool dosvncheckout(
    const char        * i_url,
    const char        * i_dir,
    const char        * i_name,
    const char        * i_pass,
    uid_t               i_uid,
    gid_t               i_gid,
    const std::string & i_home    
)
{
    
    // do all this in a forked process.
    pid_t l_child = fork();

    if ( l_child < 0 )
    {
        std::perror( "Can't fork" );
        return false;
    }

    if ( l_child > 0 )
    {
        int     l_status;
        pid_t l_waitid = waitpid( l_child, & l_status, 0 );

        if ( WIFEXITED(l_status) )
        {
            return WEXITSTATUS(l_status) == 0;
        }
        return false;
    }

    // become the user we want to be
    setgid( i_gid );
    initgroups( i_name, i_gid );
    setuid( i_uid );
    
    setenv( "HOME", i_home.c_str(), 1 );

    std::string l_svn_cmd = "svn co";

    if ( * i_pass )
    {
        l_svn_cmd = l_svn_cmd + " --password '" + i_pass + "'";
    }

    l_svn_cmd = l_svn_cmd + " '" + i_url + "' '" + i_dir + "'";

    execl( "/bin/bash", "/bin/bash", "-l", "-c", l_svn_cmd.c_str(), reinterpret_cast<const char *>(0) );

    perror( "failed to run /bin/bash" );

    exit( 1 );
    return true;
}



int main( int argc, char** argv )
{
    setgid( 0 );
    setuid( 0 );
    
    // parse the command line options
    std::auto_ptr<ost::CommandOptionParse> args(
        ost::makeCommandOptionParse(
            argc, argv,
            "[ options ]\n"
            "create a sym link and restart apache.\n"
            "Options are:\n"
        )
    );

    // If the user requested help then suppress all the usage error messages.
    if ( help_opt.numSet ) {
        std::cerr << args->printUsage();
        return 0;
    }

    

    if ( destination_opt.numValue != webloc_opt.numValue )
    {
        std::cerr << "Need same number of destination and webloc args " << destination_opt.numValue << "!=" << webloc_opt.numValue << "\n";
        std::cerr << args->printUsage();
        return 1;
    }

    for ( int i = 0; i < destination_opt.numValue; ++i )
    {
        dosymlink( destination_opt.values[0], webloc_opt.values[0] );
    }

    if ( reload_opt.numSet )
    {
        http_reload();
    }
    else if ( restart_opt.numSet )
    {
        http_restart();
    }
    else if ( start_opt.numSet )
    {
        http_start();
    }
    else if ( stop_opt.numSet )
    {
        http_stop();
    }

    if ( svn_update_dir_opt.numValue )
    {

        const char * l_pass = svn_pass_opt.LastValue( "" );
        const char * l_user = svn_user_opt.LastValue( svn_user_default );
        
        uid_t           l_uid;
        gid_t           l_gid;
        std::string     l_home;

        int         l_seconds( ParamConv( svn_pause_opt, 5 ) );
        
        if ( ! get_uid( l_user, l_uid, l_gid, l_home ) )
        {
            std::cerr << "unable to find user '" << l_user << "'\n";
            return 1;
        }
        
        for ( int i = 0; i < svn_update_dir_opt.numValue; ++i )
        {
            dosvnupdate( svn_update_dir_opt.values[i], l_user, l_pass, l_uid, l_gid, l_home, l_seconds );
        }
    }

    if ( svn_co_dir_opt.numValue || svn_co_url_opt.numValue )
    {
        if ( svn_co_dir_opt.numValue != svn_co_url_opt.numValue )
        {
            std::cerr << "Need same number of check-out url's and directories " << svn_co_dir_opt.numValue << "!=" << svn_co_url_opt.numValue << "\n";
            std::cerr << args->printUsage();
            return 1;
        }
    
        const char * l_pass = svn_pass_opt.LastValue( "" );
        const char * l_user = svn_user_opt.LastValue( svn_user_default );
        
        uid_t           l_uid;
        gid_t           l_gid;
        std::string     l_home;

        if ( ! get_uid( l_user, l_uid, l_gid, l_home ) )
        {
            std::cerr << "unable to find user '" << l_user << "'\n";
            return 1;
        }
        
        for ( int i = 0; i < svn_co_dir_opt.numValue; ++i )
        {
            bool l_res = dosvncheckout(
                svn_co_url_opt.values[0],
                svn_co_dir_opt.values[0],
                l_user,
                l_pass,
                l_uid,
                l_gid,
                l_home
            );

            if ( !l_res )
            {
                return 1;
            }
        }
    }

    return 0;
};
