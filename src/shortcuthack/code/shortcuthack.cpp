/**
 * shortcuthack.cpp
 *
 * This creates a Windows "shortcut" for an http DAV server.
 *
 *  Usage: shortcuthack <name> <url> > file.lnk.
 *
 * BUGS: It may be possible to create other types of shortcuts but this code does not produce anything
 * but simple webdav "shortcuts".
 */


#include <vector>
#include <string>
#include <iostream>
#include "at_types.h"


const char g_folder_shortcut_tmpl[] =
{
      76, 0x00, 0x00, 0x00, 0x01, 0x14, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, // 0000000
    0x00, 0x00, 0x00, 0x46, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0000016
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 0000032
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, // 0000048
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x14, 0x00, // 0000064
    0x1f, 0x50, 0xe0, 0x4f, 0xd0, 0x20, 0xea, 0x3a, 0x69, 0x10, 0xa2, 0xd8, 0x08, 0x00, 0x2b, 0x30, // 0000080
    0x30, 0x9d, 0x14, 0x00, 0x2e, 0x00, 0x00, 0xdf, 0xea, 0xbd, 0x65, 0xc2, 0xd0, 0x11, 0xbc, 0xed, // 0000096
    0x00, 0xa0, 0xc9, 0x0a, 0xb5, 0x0f, 0xff, 0xff, 0x4c, 0x50, 0x00, 0x01, 0x42, 0x57, 0x00, 0x00, // 0000112
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, // 0000128
    0x00, 0x00,                                                                                     // 0000144
};



// ======== Guid ======================================================
/**
 * 
 *
 */

struct Guid
{
    char            m_data[ 16 ];
};



// ======== ShortCutHeader ============================================
/**
 * 
 *
 */

struct ShortCutHeader
{

    at::Int32       m_headerlen;
    Guid            m_guid;
    at::Int32       m_flags;
    char            m_fill3[ 8 * 3 ];
    at::Int32       m_fsiz;
    at::Int32       m_icon_no;
    at::Int32       m_run;
    at::Int32       m_hotkey;
    char            m_fill2[ 12 ];
    at::Int16       m_bodylen;
};



// ======== bodgie_string =============================================
/**
 * Makes somthing resembling a BSTR
 *
 */

class bodgie_string
{
    public:

    std::vector<at::Int16>      m_str;

    bodgie_string(
        const std::string       & i_str
    )
      : m_str( 1, at::Int16( i_str.length() ) )
    {
        m_str.insert( m_str.end(), i_str.begin(), i_str.end() );
        m_str.push_back( 0 );
    }

    const char * begin() const
    {
        return reinterpret_cast<const char *>( &m_str[0] );
    }
    
    const char * end() const
    {
        return reinterpret_cast<const char *>( m_str.size() + &m_str[0] );
    }

};



// ======== MakeDavShortCut ===========================================
/**
 * Make a Windows explorer "DAV" shortcut
 *
 *
 * @param i_name    The "NAME" of the link
 * @param i_url     The "URL" of the link
 * @return The shortcut bytes
 */

std::vector<char> MakeDavShortCut(
    const std::string       & i_name,
    const std::string       & i_url
) {

    std::vector<char>       l_result( g_folder_shortcut_tmpl, at::ArrayEnd( g_folder_shortcut_tmpl ) );

    bodgie_string           l_name( i_name );
    bodgie_string           l_url( i_url );

    l_result.insert( l_result.end(), l_name.begin(), l_name.end() );
    l_result.insert( l_result.end(), l_url.begin(), l_url.end() );

    ShortCutHeader  * l_hdr = reinterpret_cast<ShortCutHeader *>( & l_result[ 0 ] );

    l_hdr->m_bodylen = l_result.size() - 72;

    l_result.insert( l_result.end(), 10, char( 0 ) );

    at::Int16 * l_len = reinterpret_cast<at::Int16 *>( 118 + & l_result[ 0 ] );

    * l_len = l_result.size() - 6 - 118;
    
    return l_result;
}


int main( int argc, const char ** argv )
{
    if ( argc != 3 )
    {
        std::cerr << "Need 2 args - usage: shortcuthack LINK-NAME LINK-URL\n";
        return 1;
    }

    std::vector<char>   l_link = MakeDavShortCut( argv[ 1 ], argv[ 2 ] );

    std::cout.write( & l_link[ 0 ], l_link.size() );

    return 0;
}

    
