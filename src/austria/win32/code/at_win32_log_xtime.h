/**
 * \file at_win32_log_xtime.h
 *
 * \author Gianni Mariani
 *
 */


#ifndef x_at_win32_log_xtime_h_x
#define x_at_win32_log_xtime_h_x 1


#include "at_types.h"
#include "at_thread.h"
#include <string>


namespace at
{


    class LogManagerTimePolicy_XTime_win32
    {

    public:

        typedef	TimeStamp		t_time_type;

        static t_time_type GetTime()
		{
			return ThreadSystemTime(); // defined in at_thread.h
		}

    protected:

        inline LogManagerTimePolicy_XTime_win32() {}
        inline ~LogManagerTimePolicy_XTime_win32() {}

    private:

        /* Unimplemented */
        LogManagerTimePolicy_XTime_win32
            ( const LogManagerTimePolicy_XTime_win32 & );
        LogManagerTimePolicy_XTime_win32 & operator=
            ( const LogManagerTimePolicy_XTime_win32 & );

    };


    class LogWriterTimeFormatPolicy_XTime_win32
    {

    public:

        typedef  std::string  t_formatted_time_type;

		static t_formatted_time_type FormatTime( const LogManagerTimePolicy_XTime_win32::t_time_type & i_time );

    protected:

        inline LogWriterTimeFormatPolicy_XTime_win32() {}
        inline ~LogWriterTimeFormatPolicy_XTime_win32() {}

    private:

        /* Unimplemented */
        LogWriterTimeFormatPolicy_XTime_win32
            ( const LogWriterTimeFormatPolicy_XTime_win32 & );
        LogWriterTimeFormatPolicy_XTime_win32 & operator=
            ( const LogWriterTimeFormatPolicy_XTime_win32 & );

    };


}


#endif // ifndef x_at_win32_log_xtime_h_x
