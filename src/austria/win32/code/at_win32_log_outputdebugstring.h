/**
 * \file at_win32_log_outputdebugstring.h
 *
 * \author Bradley Austin
 *
 */


#ifndef x_at_win32_log_outputdebugstring_h_x
#define x_at_win32_log_outputdebugstring_h_x 1

#include <windows.h>

#include "at_log.h"


namespace at
{


    class LogWriterOutputPolicy_OutputDebugString
      : public ThisInBaseInitializerHack<
            LogWriterOutputPolicy_OutputDebugString
        >
    {

    public:

        typedef
            LogWriterOutputPolicy_Stream<
                LogWriterOutputPolicy_OutputDebugString
            >
                t_ostream_type;

    private:

        t_ostream_type m_ostream;
        std::ostringstream m_buffer;

    protected:

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_OutputDebugString
        >::This;

        inline LogWriterOutputPolicy_OutputDebugString()
          : m_ostream( This() )
        {
        }

        inline ~LogWriterOutputPolicy_OutputDebugString() {}

    public:

        template< typename w_type >
        inline void Write( const w_type & i_value )
        {
            m_buffer << i_value;
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        void Flush()
        {
            std::string l_string = m_buffer.str();
            m_buffer.str( std::string() );

            ::OutputDebugString( l_string.c_str() );
        }

    private:

        /* Unimplemented. */
        LogWriterOutputPolicy_OutputDebugString(
            const LogWriterOutputPolicy_OutputDebugString &
        );
        LogWriterOutputPolicy_OutputDebugString & operator=(
            const LogWriterOutputPolicy_OutputDebugString &
        );

    };


}


#endif
