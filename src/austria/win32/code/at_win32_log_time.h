/**
 * \file at_win32_log_time.h
 *
 * \author Bradley Austin
 *
 */


#ifndef x_at_win32_log_time_h_x
#define x_at_win32_log_time_h_x 1


#include "at_win32_log_xtime.h"


namespace at
{

    typedef  LogManagerTimePolicy_XTime_win32  LogManagerTimePolicy_Default;

    typedef  LogWriterTimeFormatPolicy_XTime_win32  LogWriterTimeFormatPolicy_Default;

}


#endif
