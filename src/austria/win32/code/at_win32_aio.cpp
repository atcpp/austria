/*
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 *  Herein lies a minimal wrapper for asynchronous I/O at the OS level
 */

#include "at_aio.h"
#include "at_twinmt_basic.h"

#include <winsock2.h>
#include <errno.h>

#include <memory>
#include <list>
#include <iostream>
#include <vector>

namespace
{

// ======== WS2Startup_Basic ==========================================
/**
 * WS2Startup_Basic is a singleton class that only gets created
 * when WS2Startup_Basic::Startup() is called the first time.
 *
 */

class WS2Startup_Basic
{
    WSAData         m_wsa_data;

    WS2Startup_Basic()
    {
        ::WSAStartup( 0x0002, &m_wsa_data );
    }
    ~WS2Startup_Basic()
    {
        ::WSACleanup();
    }

    WS2Startup_Basic( const WS2Startup_Basic &);
    WS2Startup_Basic operator=( const WS2Startup_Basic &);

    public:

    static WSAData & Startup()
    {
        static WS2Startup_Basic     s_startup_code;

        return s_startup_code.m_wsa_data;
    }

};

} // anon namespace

namespace at
{
// ======== GetHostName ===========================================
/**
 * GetHostName returns the machine name of the local host.
 *
 * @return a std::string containing the host name.
 */

AUSTRIA_EXPORT std::string GetHostName()
{
    WS2Startup_Basic::Startup();

    char l_buf[ 256 ];
    * l_buf = 0;
    gethostname( l_buf, sizeof( l_buf ) );

    return l_buf;
}


struct aio_system_handle {
    WSADATA m_wsData;
};

/**
 * An endpoint (aka file descriptor or socket).  You should treat
 * these as opaque.
 *
 */

struct AsyncSocketContext :
    public AideTwinMT_Basic<aio_monitor_if>,
    public PtrTarget_MT
{
    AsyncSocketContext( AioMonitorLead *monitor , PtrView<MutexRefCount *> i_stateMutex,
                        aio_hub *hub, int stype = SOCK_STREAM, bool accepting = false);
    virtual ~AsyncSocketContext();

    // communications methods
    virtual bool connect(aio_ip4_addr &addr);
    virtual bool listen(aio_ip4_addr &addr, bool isDataGram = false);
    virtual bool accept(PtrView<AsyncSocketContext *> listening_asc, aio_fd *conn_o,aio_ip4_addr *addr);
    virtual bool recv(char *buffer_o, int *sizep_io,aio_ip4_addr *addrp_o, int *err_o);
    virtual bool close(int *err_o);

    // helpers
    virtual bool get_local_address(aio_ip4_addr *addrp_o, int *err_o);
    virtual bool get_remote_address(aio_ip4_addr *addrp_o, int *err_o);

    enum { isDatagram = true, isNotDatagram = false };
    virtual bool send_or_write(Ptr<Buffer *> i_buffer, aio_ip4_addr dest, int *err_o,bool datagram = isNotDatagram);
    virtual bool set_monitor(AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex, int *err_o);

    // accessors/status
    virtual int fd(void) { return m_fdx; }
    virtual HANDLE EventHandle(void) { return m_event; }
    virtual bool NotifyEvent(void);     // return false to delete "this" context

    // socket events
    virtual bool notifyRead(int statusCode);
    virtual bool notifyWrite(int statusCode);
    virtual bool notifyOOB(int statusCode);
    virtual bool notifyAccept(int statusCode);
    virtual bool notifyConnect(int statusCode);
    virtual bool notifyClose(int statusCode);
    virtual bool notifyQOS(int statusCode);
    virtual bool notifyGroupQOS(int statusCode);
    virtual bool notifyRoutingInterfaceChange(int statusCode);
    virtual bool notifyAddressListChange(int statusCode);

    virtual bool tellMonitor( int state, int flags);
    virtual bool sendCompleted(void);

private:
    AsyncSocketContext(const AsyncSocketContext &doesNotCopy);
    AsyncSocketContext& operator=(const AsyncSocketContext &doesNotAssign);

    /** Shared -- The number of endpoints we've created. */
    static AtomicCount m_n;

    // internal class
    struct write_params {
        int     m_sent_size;
        aio_ip4_addr m_dest_addr;
        int     m_err;
        Ptr<Buffer *>   m_buffer;
    };

    /** A number unique to this endpoint. */
    int         m_fdx;

    SOCKET      m_socket;
    WSAEVENT    m_event;
    int         m_type;

    bool        m_closed;

    bool        m_haveLocalAddress;
    int         m_local_addr_len;
    sockaddr_in m_local_addr;

    bool        m_havePeerAddress;
    int         m_peer_addr_len;
    sockaddr_in m_peer_addr;

    aio_hub     *m_hub;

    std::list<write_params> m_queued_write_params;
    // insert new members above here ^^^

    Ptr<MutexRefCount *>    m_stateMutex;

};

AtomicCount AsyncSocketContext::m_n = 0;

/**
*   CallbackTask  - this thread runs in the background. It waits for new contexts
*   and adds them to an event map. When the context completes the thread fires
*   off a completion notification.
*
*   Activities execute in the context of the CallbackTask thread, serially after
*   their associated event fires
*/

// @@ TODO this code is essentially identical to at_file CallbackTask -- reuse would be good

struct aio_hub_internal : private at::Task
{
private:
    ConditionalMutex    m_condWork;         // wait for condition variable when no work pending

    // members to protect with the above mutex
    HANDLE                      m_hQueueEvent;  // aborts the waitformultipleobjects when a new event is queued
    bool                            m_done;     // flag to terminate the work loop
    bool                            m_working;  // flag that the worker thread is active

    std::vector<Ptr<AsyncSocketContext *> > m_asc;  // pending io

public:

    aio_hub_internal () :
        m_condWork(Mutex::Recursive),
        m_hQueueEvent(::CreateEvent(NULL,false,false,NULL)),
        m_done(false),
        m_working(false),
        m_asc()
    {
    }

    // typically does not execute in the thread context of the callback thread
    virtual ~aio_hub_internal()
    {
        Shutdown();
        {
            Lock<ConditionalMutex>  l(m_condWork);
            if (INVALID_HANDLE_VALUE != m_hQueueEvent) {
                ::CloseHandle(m_hQueueEvent);
                m_hQueueEvent = INVALID_HANDLE_VALUE;
            }
        }
    }

 // ======== Start =======================================================
    /**
    *  Starts the thread
    *  typically does not execute in the thread context of the callback thread
    */
    virtual void Start ()
    {
        Task::Start ();
    }

 // ======== Shutdown =======================================================
    /**
    *  Cancels all activities and shuts down the thread,
    *  typically does not execute in the thread context of the callback thread
    *
    * @returns nothing
    */
    virtual void Shutdown()
    {
        Lock<ConditionalMutex>  l(m_condWork);
        m_done = true;
        releaseWorker();
        if (m_working) {
            m_condWork.Wait();
        }
    }

    virtual bool QueueAsyncSocketContext(PtrView<AsyncSocketContext *> ac) {
        Lock<ConditionalMutex>  l(m_condWork);
        if (m_asc.size() >= (MAXIMUM_WAIT_OBJECTS -1)) {
            // we can't handle more due to win32 api limitations
            return false;
        }
        m_asc.push_back(ac);
        releaseWorker();
        return true;
    }

    Ptr<AsyncSocketContext *> GetAsyncSocketContext(aio_fd afd) {
        Lock<ConditionalMutex>  l(m_condWork);
        Ptr<AsyncSocketContext *> retval = NULL;
        for (size_t i = 0; i < m_asc.size(); ++i) {
            if (afd == m_asc[i]->fd()) {
                retval = m_asc[i];
            }
        }
        return retval;
    }

private:
    // ======== Work =======================================================
    //  sleeps until there is work to do
    //  then retrieves and posts it
    virtual void Work ()
    {
        Lock<ConditionalMutex>  l(m_condWork);
        m_working = true;
        do {
            if (m_asc.empty()) {
                m_condWork.Wait();
            } else {
                doWin32EventWait();
            }
        } while (!m_done);
        m_working = false;
        m_condWork.PostAll();
    }

    void releaseWorker(void)
    {
        m_condWork.PostAll();
        if (INVALID_HANDLE_VALUE != m_hQueueEvent) {
            ::SetEvent(m_hQueueEvent);
        }
    }

    void doWin32EventWait(void) {
        HANDLE events[MAXIMUM_WAIT_OBJECTS];
        DWORD dwCount = m_asc.size() + 1;
        if (dwCount > MAXIMUM_WAIT_OBJECTS) {
            dwCount = MAXIMUM_WAIT_OBJECTS;
        }
        events[0] = m_hQueueEvent;          // if we wake on this one, a new io event was queued
        for (unsigned int i=1; i < dwCount; ++i) {   // make a local copy of queued events so that we can unlock the state mutex
            events[i] = m_asc[i-1]->EventHandle();
        }
        DWORD dwResult = WAIT_FAILED;
        {
            Unlock<ConditionalMutex> m_ul(m_condWork);
            dwResult = ::WaitForMultipleObjectsEx(dwCount,events,false,30*1000,true);
        }
        dispatchWin32Event(events,dwCount,dwResult);
    }

    void dispatchWin32Event(HANDLE *events,DWORD dwCount, DWORD dwResult)
    {
        int eventNum = 0;
        if ((dwResult >= WAIT_ABANDONED_0) && (dwResult < (WAIT_ABANDONED_0 + dwCount))) {
            eventNum = dwResult - WAIT_ABANDONED_0;
        } else if ((dwResult >= WAIT_OBJECT_0) && (dwResult < (WAIT_OBJECT_0 + dwCount))) {
            eventNum = dwResult - WAIT_OBJECT_0;
        }
        if (eventNum) {
            // not our private event, therefore a valid async io handle
            HANDLE event = events[eventNum];
            Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(event); // hold an owned reference
            if (ac) {
                bool bKeep = true;
                {
                    Unlock<ConditionalMutex> l_ul(m_condWork);
                    bKeep = ac->NotifyEvent();
                }
                if (!bKeep) {
                    removeAsyncSocketContext(event);
                }
            }
            Unlock<ConditionalMutex> l_ul(m_condWork);
            ac = NULL;
        }
    }

    PtrView<AsyncSocketContext *> getAsyncSocketContext(HANDLE event)
    {
        PtrView<AsyncSocketContext *> retval = NULL;
        for (size_t i = 0; i < m_asc.size(); ++i) {
            if (event == m_asc[i]->EventHandle()) {
                retval = m_asc[i];
            }
        }
        return retval;
    }

    void removeAsyncSocketContext(HANDLE event)
    {
        for (size_t i = 0; i < m_asc.size(); ++i) {
            if (event == m_asc[i]->EventHandle()) {
                m_asc.erase(m_asc.begin() + i);         // does not erase pointee
                break;
            }
        }
        return;
    }
};

} // namespace

using namespace at;

namespace
{
    const int default_events = FD_READ|FD_WRITE|FD_OOB|FD_ACCEPT|FD_CONNECT|FD_CLOSE;

    /** Make an OS-level address structure. */
    void
    to_os_address(aio_ip4_addr addr, sockaddr_in *o)
    {
        o->sin_family = AF_INET;
        o->sin_addr.s_addr = htonl(addr.first);
        o->sin_port = htons(addr.second);
    }

    /** Make an aio-level address structure. */
    void
    to_aio_address(sockaddr_in addr, aio_ip4_addr *o)
    {
        o->first = ntohl(addr.sin_addr.s_addr);
        o->second = ntohs(addr.sin_port);
    }
}

const int aio_flag::data_available = (1 << 0);
const int aio_flag::connection_waiting = (1 << 1);
const int aio_flag::closing = (1 << 2);
const int aio_flag::writeable = (1 << 3);

const int at::aio_state::listening = 1;
const int at::aio_state::connecting = 2;
const int at::aio_state::connected = 3;
const int at::aio_state::datagram = 4;
const int at::aio_state::closed = 5;

const int at::aio_error::unexpected = 1;
const int at::aio_error::loser = 2;
const int at::aio_error::state = 3;
const int at::aio_error::invalid = 4;
const int at::aio_error::closing = 5;
const int at::aio_error::shutdown = 6;
const int at::aio_error::unreachable = 7;
const int at::aio_error::peer_reset = 8;
const int at::aio_error::bind_failure = 9;
const int at::aio_error::invalid_address = 10;

const aio_fd aio_fd::invalid;

AsyncSocketContext::~AsyncSocketContext()
{
    AideCancel();
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (INVALID_HANDLE_VALUE != m_event) {
        ::WSACloseEvent(m_event);
        m_event = INVALID_HANDLE_VALUE;
    }
    if (INVALID_SOCKET != m_socket) {
        ::closesocket(m_socket);
        m_socket = INVALID_SOCKET;
    }
}

const char* at::aio_error::to_string(int e)
{
    switch (e)
    {
    case unexpected:
        return "unexpected";
    case loser:
        return "loser";
    case state:
        return "state";
    case invalid:
        return "invalid";
    case closing:
        return "closing";
    case shutdown:
        return "shutdown";
    case unreachable:
        return "unreachable";
    case peer_reset:
        return "peer_reset";
    default:
        return "<unrecognized error code>";
    }
}

const char* at::aio_state::to_string(int e)
{
    switch (e)
    {
    case aio_state::listening:
        return "listening";
    case aio_state::connecting:
        return "connecting";
    case aio_state::connected:
        return "connected";
    case aio_state::datagram:
        return "datagram";
    case aio_state::closed:
        return "closed";
    default:
        return "<unrecognized state>";
    }
}

std::string at::aio_flag::to_string(int e)
{
    std::string s;
    if (e & aio_flag::data_available) s += "data_available";
    if (e & aio_flag::connection_waiting)
    {
        if (s.size()) s += ",";
        s += "connection_waiting";
    }
    if (e & aio_flag::closing)
    {
        if (s.size()) s += ",";
        s += "closing";
    }
    return s;
}

aio_monitor_if::~aio_monitor_if()
{
}

void
aio_monitor_if::become_monitor(aio_fd fd, aio_hub *source)
{
}

void
aio_monitor_if::state_changed(
    aio_fd fd, aio_hub *source, int state, int flags)
{
}

void
aio_monitor_if::send_completed(
    aio_fd fd,
    aio_hub *source,
    Ptr<Buffer *> i_buffer,
    int i_sent_size, 
    aio_ip4_addr dest,
    int err)
{
}

aio_hub*
aio_hub::new_hub()
{
    // singleton for now @@ TODO
    static aio_hub theHub;
    return &theHub;
}

Ptr<AsyncSocketContext *> aio_hub::getAsyncSocketContext(aio_fd afd)
{
    Lock<Mutex> lock(m_stateMutex);
    Ptr<AsyncSocketContext *> ac;
    for (size_t i=0; i < m_internal_hubs.size(); ++i) {
        // @@ TODO -- currently this is O(N) with number of sockets -- consider O(log(N))
        // data structures and algorithms
        ac = m_internal_hubs[i]->GetAsyncSocketContext(afd);    // try to get context
        if (ac) {
            break;
        }
    }
    return ac;
}

bool aio_hub::queueAsyncSocketContext(PtrView<AsyncSocketContext *> ac)
{
    Lock<Mutex> lock(m_stateMutex);
    bool retval = false;
    for (size_t i=0; i < m_internal_hubs.size(); ++i) {
        retval = m_internal_hubs[i]->QueueAsyncSocketContext(ac);   // try to enqueue request
        if (retval) {
            break;
        }
    }
    if (!retval) {  // all existing hubs are full, create a new one
        aio_hub_internal *ah = new aio_hub_internal();
        retval = ah->QueueAsyncSocketContext(ac);                   // try to enqueue request
        m_internal_hubs.push_back(ah);
        ah->Start();
    }
    return retval;
}

bool
aio_hub::connect(
    aio_ip4_addr dest, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex,
    aio_fd *fd_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    if (monitor || fd_o ) {
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_STREAM);
        if (sock->connect(dest)) {                      // initiate client request on socket
            retval = queueAsyncSocketContext(sock);   // enqueue request
            if (retval) {
                fd_o? (*fd_o = sock->fd()) : false;
                err = 0;
                retval = true;
            } else {
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {
            err = aio_error::unexpected;
        }
    }
    err_o? (*err_o = err) : false;
    return retval;
}

bool
aio_hub::listen(
    aio_ip4_addr addr, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex,
    aio_fd *fd_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    if (monitor || fd_o ) {
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_STREAM);
        if (sock->listen(addr)) {                       // initiate client request on socket
            retval = queueAsyncSocketContext(sock);   // enqueue request
            if (retval) {
                fd_o? (*fd_o = sock->fd()) : false;
                err = 0;
                retval = true;
            } else {
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {
            err = aio_error::bind_failure;
        }
    }
    err_o? (*err_o = err) : false;
    return retval;
}

bool
aio_hub::listen_datagram(
    aio_ip4_addr addr, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex,
    aio_fd *fd_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    if (monitor || fd_o ) {
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_DGRAM);
        if (sock->listen(addr, true)) {                 // initiate client request on socket
            retval = queueAsyncSocketContext(sock);   // enqueue request
            if (retval) {
                fd_o? (*fd_o = sock->fd()) : false;
                err = 0;
                retval = true;
            } else {
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {
            err = aio_error::bind_failure;
        }
    }
    err_o? (*err_o = err) : false;
    return retval;
}

bool
aio_hub::accept(
    aio_fd listening_fd, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex,
    aio_fd *conn_o, aio_ip4_addr *addrp_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    Ptr<AsyncSocketContext *> listening_ac = getAsyncSocketContext(listening_fd);
    if (listening_ac && (monitor || conn_o )) {
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_DGRAM);
        if (sock->accept(listening_ac,conn_o,addrp_o)) {                // initiate client request on socket
            retval = queueAsyncSocketContext(sock);     // enqueue request
            if (retval) {
                err = 0;
                retval = true;
            } else {
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {
            err = aio_error::unexpected;
        }
    }
    if (!retval) {
        err_o? (*err_o = err) : 0;
    }
    return retval;
}

bool
aio_hub::set_monitor(aio_fd fd, AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->set_monitor(monitor,i_stateMutex,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}


bool
aio_hub::get_local_address(aio_fd fd, aio_ip4_addr *addrp_o, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->get_local_address(addrp_o,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::get_remote_address(aio_fd fd, aio_ip4_addr *addrp_o, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->get_remote_address(addrp_o,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::read(aio_fd fd, char *buffer_o, int *sizep_io, int *err_o)
{
    return this->recv(fd, buffer_o, sizep_io, 0, err_o);
}

bool
aio_hub::write(aio_fd fd, Ptr<Buffer *> i_buffer, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->send_or_write(i_buffer, aio_ip4_addr(), err_o, AsyncSocketContext::isNotDatagram);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::recv(aio_fd fd, char *buffer_o, int *sizep_io,aio_ip4_addr *addrp_o, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->recv(buffer_o,sizep_io,addrp_o,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::send(aio_fd fd, Ptr<Buffer *> i_buffer, aio_ip4_addr dest, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->send_or_write(i_buffer, dest, err_o, AsyncSocketContext::isDatagram);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::close(aio_fd fd, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->close(err_o);  // queues an event request
    } else {
        retval = true;
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}



aio_hub::~aio_hub()
{
    Lock<Mutex> lock(m_stateMutex);
    while (!m_internal_hubs.empty()){
        m_internal_hubs[0]->Shutdown();
        delete m_internal_hubs[0];
        m_internal_hubs.erase(m_internal_hubs.begin());
    }
}

aio_hub::aio_hub(void) :
    m_internal_hubs(),
    m_stateMutex(Mutex::Recursive)
{
    WS2Startup_Basic::Startup();
}

// message creation methods
bool AsyncSocketContext::connect(aio_ip4_addr &addr)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    if (m_socket != INVALID_SOCKET) {
        sockaddr_in os_addr;
        to_os_address(addr, &os_addr);
        if (SOCKET_ERROR == ::connect(m_socket,(sockaddr *)(&os_addr),sizeof(os_addr))) {
            // try asynchronous completion proc
            if (WSAEWOULDBLOCK == ::WSAGetLastError()) {
                retval = true;
            }
        } else {
            // synchronous completion
            retval = true;
        }
    }
    return retval;
}

bool AsyncSocketContext::listen(aio_ip4_addr &addr, bool isDataGram)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    if (m_socket != INVALID_SOCKET) {
        sockaddr_in os_addr;
        to_os_address(addr, &os_addr);
        int result = ::bind(m_socket,(sockaddr *)(&os_addr),sizeof(os_addr));
        if (SOCKET_ERROR != result) {
            int result2 = (!isDataGram)? ::listen(m_socket,SOMAXCONN): 0;
            if (SOCKET_ERROR != result2) {
                retval = true;
            } else {
                int result3 = ::WSAGetLastError();
            }
        } else {
        }
    }
    return retval;
}

AsyncSocketContext::AsyncSocketContext(AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex,
                                       aio_hub *hub, int stype, bool accepting) :
    m_fdx(m_n++),
    m_socket(INVALID_SOCKET),
    m_event(INVALID_HANDLE_VALUE),
    m_type(stype),
    m_closed(false),
    m_haveLocalAddress(false),
    m_local_addr_len(sizeof(m_local_addr)),
    m_havePeerAddress(false),
    m_peer_addr_len(sizeof(m_peer_addr)),
    m_hub(hub),
    m_queued_write_params(),
    m_stateMutex(i_stateMutex)
{
    if (m_fdx == -1) {  // disallow reserved invalid value
        m_fdx = m_n++;
    }
    ::ZeroMemory(&m_local_addr,sizeof(m_local_addr));
    ::ZeroMemory(&m_peer_addr,sizeof(m_peer_addr));
    // done static init

    if (monitor) {
        AideAssociate(monitor, m_stateMutex);
        CallLead().VoidCall(&AioMonitorLead::become_monitor,m_fdx,m_hub);
    }

    if (!accepting) {
        m_socket = ::socket(PF_INET,m_type,(m_type == SOCK_STREAM)? IPPROTO_TCP : IPPROTO_UDP);
        if (INVALID_SOCKET == m_socket) {
            return;
        }
    }
    m_event = ::WSACreateEvent();
    if (INVALID_HANDLE_VALUE != m_event) {
        if (!accepting) {
            SOCKET result = ::WSAEventSelect(m_socket,m_event,default_events);
            if (result != INVALID_SOCKET) {
                return;
            }
            ::WSACloseEvent(m_event);
            m_event = INVALID_HANDLE_VALUE;
        }
    }
    if (INVALID_SOCKET != m_socket) {
        ::closesocket(m_socket);
        m_socket = INVALID_SOCKET;
    }
    return;
}

bool AsyncSocketContext::accept(PtrView<AsyncSocketContext *> listening_asc, aio_fd *conn_o,aio_ip4_addr *addr)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    sockaddr_in l_peer_addr;
    int l_peer_addr_len = sizeof(l_peer_addr);

    SOCKET l_socket = ::accept(listening_asc->m_socket,(sockaddr *)&l_peer_addr, &l_peer_addr_len);
    if ((SOCKET_ERROR != l_socket) && (l_peer_addr_len == sizeof(l_peer_addr))) {
        if (INVALID_SOCKET != m_socket) {   //@@ TODO prevent constructor from creating wasted socket
            ::closesocket(m_socket);
            m_socket = INVALID_SOCKET;
        }
        m_socket = l_socket;
        SOCKET result = ::WSAEventSelect(m_socket,m_event,default_events);
        if (result != INVALID_SOCKET) {
            m_peer_addr = l_peer_addr;
            m_peer_addr_len = l_peer_addr_len;
            m_havePeerAddress = true;
            retval = true;
        }
    }
    if (retval) {
        conn_o? (*conn_o = this->fd()): 0;
        addr? to_aio_address(m_peer_addr, addr): 0;
    } else {
        tellMonitor(aio_state::closed,0);
    }
    return retval;
}

bool AsyncSocketContext::recv(char *buffer_o, int *sizep_io,aio_ip4_addr *addrp_o, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    int err = aio_error::unexpected;
    if (buffer_o && sizep_io) {
        // in winsock, os_addr is not returned for connection oriented sockets, so we
        // must initialize it to something meaningful
        sockaddr_in os_addr;
        int os_addr_size = sizeof(os_addr);
        if (m_havePeerAddress) {
            os_addr = m_peer_addr;
        } else {
            // @@ TODO -- this is questionable, but the real question is what is the
            // representation of an "invalid" aio_ip4_addr ...
            ::ZeroMemory(&os_addr,sizeof(os_addr));
        }

        int bytes = ::recvfrom(m_socket,buffer_o,*sizep_io,0,(sockaddr*)&os_addr,&os_addr_size);
        if (SOCKET_ERROR != bytes) {
            addrp_o? to_aio_address(os_addr, addrp_o): 0;
            *sizep_io = bytes;
            retval = true;
        } else {
            int error = ::WSAGetLastError();
            switch (error) {
                case WSAEWOULDBLOCK:
                    err = aio_error::loser;     // @@ TODO -- this is a terrible error name, replace
                    break;
                default:
                // @@ there are certain errors we may want to quantify as a courtesy to the caller
                    break;
            }
        }
    }
    if (!retval) {
        err_o? (*err_o = err): 0;
    }
    return retval;
}

bool AsyncSocketContext::close(int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    int err = 0;
    int result = ::closesocket(m_socket);
    if (SOCKET_ERROR != result) {
        m_closed = true;
        ::WSASetEvent(m_event); // trigger close logic asynchronously on different thread
        retval = true;
    } else {
        int error = ::WSAGetLastError();
        switch (error) {
            case WSAEWOULDBLOCK:
                err = aio_error::loser;     // @@ TODO -- this is a terrible error name, replace
                break;
            case WSAENOTSOCK:   // ignore, it is already closed
                break;
            default:
                // @@ there are certain errors we may want to quantify as a courtesy to the caller
                std::cerr << "close error [" << error << "]" << std::endl;
                break;
        }
        retval = true;
    }
    err_o? (*err_o = err): 0;
    return retval;
}

bool AsyncSocketContext::get_local_address(aio_ip4_addr *addrp_o, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (addrp_o) {
        if (m_havePeerAddress) {
            to_aio_address(m_local_addr, addrp_o);
        } else {
            m_local_addr_len = sizeof(m_local_addr);
            if (SOCKET_ERROR != ::getsockname(m_socket, (sockaddr*)&m_local_addr, &m_local_addr_len)) {
                to_aio_address(m_local_addr, addrp_o);
                m_haveLocalAddress = true;
            }
        }
    }
    (!m_haveLocalAddress && err_o)? (*err_o = aio_error::unexpected): 0;
    return m_haveLocalAddress;
}

bool AsyncSocketContext::get_remote_address(aio_ip4_addr *addrp_o, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (addrp_o) {
        if (m_havePeerAddress) {
            to_aio_address(m_peer_addr, addrp_o);
        } else {
            m_peer_addr_len = sizeof(m_peer_addr);
            if (SOCKET_ERROR != ::getpeername(m_socket, (sockaddr*)&m_peer_addr, &m_peer_addr_len)) {
                to_aio_address(m_peer_addr, addrp_o);
                m_havePeerAddress = true;
            }
        }
    }
    (!m_havePeerAddress && err_o)? (*err_o = aio_error::unexpected): 0;
    return m_havePeerAddress;
}
 


// @@ TODO -- overload for datagram vs. connected sockets
bool AsyncSocketContext::send_or_write(Ptr<Buffer *> i_buffer, aio_ip4_addr dest, int *err_o,bool datagram)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);

    char *buffer = i_buffer->Region().m_mem;
    int size = i_buffer->Region().m_allocated;

    bool retval = false;
    retval &= !size;
    if (!retval) {
        sockaddr_in os_addr;
        to_os_address(dest, &os_addr);
        // sendto, with flags = 0, os_addr ignored on non-datagram sockets
        int result = ::sendto(m_socket, buffer, size, 0, (sockaddr*)&os_addr,sizeof(os_addr));
        if (SOCKET_ERROR != result) {
            write_params l_write_params;

            l_write_params.m_sent_size = result;
            l_write_params.m_dest_addr = dest;
            l_write_params.m_err = (datagram && (result != size))? aio_error::unexpected: 0;
            l_write_params.m_buffer = i_buffer;

            m_queued_write_params.push_back(l_write_params);

            retval = (l_write_params.m_err == 0);

        } else {
            err_o? (*err_o = aio_error::unexpected): 0;
        }
    }
    return retval;
}

bool AsyncSocketContext::set_monitor(AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    AideCancel();
    if (monitor) {
        m_stateMutex = i_stateMutex;
        AideAssociate(monitor, m_stateMutex);
        retval = CallLead().VoidCall(&AioMonitorLead::become_monitor,m_fdx,m_hub);
    }

    err_o? (*err_o = 0): 0;
    return retval;
}

// socket events {return "false" only to terminate this context for final state
/* reference should match aio.h

namespace aio_flag
    extern const int data_available;
    extern const int connection_waiting;
    extern const int closing;
namespace aio_state
    extern const int listening;
    extern const int connecting;
    extern const int connected;
    extern const int datagram;
    extern const int closed;
*/

bool AsyncSocketContext::tellMonitor( int state, int flags)
{
    bool retval = false;
    retval = CallLead().VoidCall(&AioMonitorLead::state_changed, m_fdx, m_hub, state, flags);
    return retval;
}

bool AsyncSocketContext::sendCompleted(void)
{
    bool retval = false;
    write_params l_write_params = m_queued_write_params.front();
    Ptr<Buffer *>   l_buffer = l_write_params.m_buffer;
    aio_ip4_addr dest;
    dest.first = l_write_params.m_dest_addr.first;
    dest.second = l_write_params.m_dest_addr.second;

    if (!l_write_params.m_err) {
        if (l_write_params.m_sent_size == l_buffer->Region().m_allocated) {
            // the buffer was completed, remove and report
            m_queued_write_params.pop_front();

            retval = CallLead().VoidCall(
                &AioMonitorLead::send_completed,
                m_fdx,
                m_hub,
                l_buffer,
                l_write_params.m_sent_size,
                dest,
                l_write_params.m_err
            );

        } else {
            // partial buffer remains, leave on front, try writing more
            char *buffer = l_buffer->Region().m_mem + l_write_params.m_sent_size;
            int size = l_buffer->Region().m_allocated - l_write_params.m_sent_size;
            sockaddr_in os_addr;
            to_os_address(dest, &os_addr);
            // sendto, with flags = 0, os_addr ignored on non-datagram sockets
            int result = ::sendto(m_socket, buffer, size, 0, (sockaddr*)&os_addr,sizeof(os_addr));
            if (SOCKET_ERROR != result) {
                m_queued_write_params.front().m_sent_size += result;
                m_queued_write_params.front().m_err = 0;

                retval = (m_queued_write_params.front().m_err == 0);

            } else {
                m_queued_write_params.front().m_err = aio_error::unexpected;
            }
        }
    } else {
        // there was an error on the previous write, remove and report
        m_queued_write_params.pop_front();
        retval = CallLead().VoidCall(
            &AioMonitorLead::send_completed,
            m_fdx,
            m_hub,
            l_buffer,
            l_write_params.m_sent_size,
            dest,
            l_write_params.m_err
        );
    }
    return retval;
}

bool AsyncSocketContext::notifyRead(int statusCode)
{
    bool retval = false;
    if (!statusCode) {
        retval = true;
        tellMonitor(aio_state::connected, aio_flag::data_available);
    } else {
        tellMonitor(aio_state::closed, 0);
    }
    return retval;
}

bool AsyncSocketContext::notifyWrite(int statusCode)
{
    bool retval = false;
    if (!statusCode) {
        retval = true;
        // in the documentation for WSAEventSelect, it is indicated that the FD_WRITE event
        // means "you can write now", as opposed to "your write has completed"
        // which means that it is also set upon initial "connect/accept" socket functions
        if (!m_queued_write_params.empty()) {
            sendCompleted();
        }
        tellMonitor(aio_state::connected, aio_flag::writeable);
    } else {
        tellMonitor(aio_state::closed, 0);
    }
    return retval;
}

bool AsyncSocketContext::notifyOOB(int statusCode)
{
    bool retval = true;
    ::OutputDebugString("AsyncSocketContext::notifyOOB\n");
    // @@ TODO -- need to eat the incoming data, and need monitor API/state/flags
    return retval;
}

bool AsyncSocketContext::notifyAccept(int statusCode)
{
    bool retval = false;
    switch (statusCode) {
        case 0:
            tellMonitor(aio_state::listening,aio_flag::connection_waiting);
            retval = true;
            break;
        case WSAEAFNOSUPPORT:       // Addresses in the specified family cannot be used with this socket. 
        case WSAENETUNREACH:        // The network cannot be reached from this host at this time. 
        case WSAENOBUFS:            // No buffer space is available. The socket cannot be connected. 
        case WSAETIMEDOUT:          // An attempt to connect timed out without establishing a connection 
        case WSAECONNREFUSED:       // The attempt to connect was forcefully rejected.
        default:
            ::OutputDebugString("AsyncSocketContext::notifyAccept - BAD\n");
            tellMonitor(aio_state::closed,0);
            break;
    }
    return retval;
}

bool AsyncSocketContext::notifyConnect(int statusCode)
{
    bool retval = false;
    switch (statusCode) {
        case 0:
            tellMonitor(aio_state::connected,0);
            retval = true;
            break;
        case WSAEAFNOSUPPORT:       // Addresses in the specified family cannot be used with this socket. 
        case WSAENETUNREACH:        // The network cannot be reached from this host at this time. 
        case WSAENOBUFS:            // No buffer space is available. The socket cannot be connected. 
        case WSAETIMEDOUT:          // An attempt to connect timed out without establishing a connection 
        case WSAECONNREFUSED:       // The attempt to connect was forcefully rejected.
        default:
            ::OutputDebugString("AsyncSocketContext::notifyConnect - BAD\n");
            tellMonitor(aio_state::closed,0);
            break;
    }
    return retval;
}

bool AsyncSocketContext::notifyClose(int statusCode)
{
    bool retval = false;
    m_socket = INVALID_SOCKET;  // this is to prevent an undocumented race condition in some winsock implementations
                                // wherein the WS reissues the same socket number to another client app before we get the
                                // async close notification -- we need to prevent our client app from doing any
                                // further socket operations (such as "close") which invalidate the other client's state.
                                // good idea anyway, since our client should not really be doing anything after being told
                                // that the session is closed
    tellMonitor(aio_state::connected, aio_flag::closing);
    tellMonitor(aio_state::closed, 0);
    return retval;
}

bool AsyncSocketContext::notifyQOS(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyQOS\n");
    return retval;
}

bool AsyncSocketContext::notifyGroupQOS(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyGroupQOS\n");
    return retval;
}

bool AsyncSocketContext::notifyRoutingInterfaceChange(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyRoutingInterfaceChange\n");
    return retval;
}

bool AsyncSocketContext::notifyAddressListChange(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyAddressListChange\n");
    return retval;
}

bool AsyncSocketContext::NotifyEvent(void)  // return false to delete "this" context
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (m_closed) {
        notifyClose(0);
        return false;
    }
    bool retval = false;
    WSANETWORKEVENTS events;
    int result = ::WSAEnumNetworkEvents(m_socket,m_event,&events);
    // if no monitor, or an error, set to delete this context
    if (SOCKET_ERROR != result) {
        retval = true;
        retval &= (events.lNetworkEvents & FD_READ)? notifyRead(events.iErrorCode[FD_READ_BIT]): true;
        retval &= (events.lNetworkEvents & FD_WRITE)? notifyWrite(events.iErrorCode[FD_WRITE_BIT]): true;
        retval &= (events.lNetworkEvents & FD_OOB)? notifyOOB(events.iErrorCode[FD_OOB_BIT]): true;
        retval &= (events.lNetworkEvents & FD_ACCEPT)? notifyAccept(events.iErrorCode[FD_ACCEPT_BIT]): true;
        retval &= (events.lNetworkEvents & FD_CONNECT)? notifyConnect(events.iErrorCode[FD_CONNECT_BIT]): true;
        retval &= (events.lNetworkEvents & FD_CLOSE)? notifyClose(events.iErrorCode[FD_CLOSE_BIT]): true;
        retval &= (events.lNetworkEvents & FD_QOS)? notifyQOS(events.iErrorCode[FD_QOS_BIT]): true;
        retval &= (events.lNetworkEvents & FD_GROUP_QOS)? notifyGroupQOS(events.iErrorCode[FD_GROUP_QOS_BIT]): true;
        retval &= (events.lNetworkEvents & FD_ROUTING_INTERFACE_CHANGE)? notifyRoutingInterfaceChange(events.iErrorCode[FD_ROUTING_INTERFACE_CHANGE_BIT]): true;
        retval &= (events.lNetworkEvents & FD_ADDRESS_LIST_CHANGE)? notifyAddressListChange(events.iErrorCode[FD_ADDRESS_LIST_CHANGE_BIT]): true;
    } else {
        ::OutputDebugString("ac: event -- error processsing\n");
        tellMonitor(aio_state::closed,0);
    }
    return retval;
}
