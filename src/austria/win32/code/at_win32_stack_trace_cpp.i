//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_win32_stack_trace_cpp.i
 *
 */
#pragma warning ( disable : 4250 )

//
// This implements the Windows stack trace

#include "at_stack_trace.h"
#include "at_types.h"
#include "at_lifetime_mt.h"
#include "at_thread.h"

#define _WIN32_WINNT 0x0501
#include <WTypes.h>
#include <WinBase.h>

// This define exposes the old API's
// #define _IMAGEHLP_SOURCE_ 1
#include <dbghelp.h>

#include <vector>
#include <sstream>

namespace {
	at::Mutex g_sync_trace;
};

// Austria namespace
namespace at
{

// ======== StackTrace_Win32 ===========================================

class AUSTRIA_EXPORT StackTrace_Win32
  : virtual public PtrTarget_MTVirtual,
    public StackTrace
{

    public:

    StackTrace_Win32(
        DWORD64                         * i_addresses,
        int                               i_count
    )
      : m_addresses( i_addresses, i_addresses + i_count )
    {
    }

    ~StackTrace_Win32()
    {
    }

#if ErrorInfoSupport
	// This is here for debugging only ...
	// there are old buggy versions of dbghelp.dll
	// and it might be interesting to know what the
	// error message is ...
	std::string ShowLastError() const
	{
		LPVOID lpMsgBuf;
		DWORD dw = GetLastError(); 

		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			dw,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &lpMsgBuf,
			0, NULL
		);
		
		std::string		l_str = ( char * ) lpMsgBuf;
		LocalFree(lpMsgBuf);
		
		return l_str;
	}
#endif

	struct DbgHelpInitializer
	{
		DbgHelpInitializer()
		{
	        HANDLE  l_process = GetCurrentProcess();

			SymSetOptions( SYMOPT_DEFERRED_LOADS | SYMOPT_LOAD_LINES  );
			SymInitialize( l_process, 0, TRUE );
		}
		
		~DbgHelpInitializer()
		{
			HANDLE  l_process = GetCurrentProcess();
			
			SymCleanup( l_process );
		}
	
	};

    // ======== ProcessFrames ==========================================
    /**
     * ProcessFrames will process the stack trace into StackFrame
     * objects.
     *
     * @return nothing
     */

    virtual void ProcessFrames() const
    {
        // if the processing is already done, don't do it again
        
        if ( 0 != m_frames.size() )
        {
            return;
        }

        // create the frames

        m_frames.reserve( m_addresses.size() );
        
        HANDLE  l_process = GetCurrentProcess();
        
        {
			Lock<Mutex>           l_lock( g_sync_trace );
			
			static DbgHelpInitializer	l_initializer;

			typedef IMAGEHLP_SYMBOL64	t_SymbolType;
			typedef DWORD64				t_SymbolAddr;
			
			static t_SymbolType       l_symbol[ 1 + ( sizeof( t_SymbolType   ) * 2 + 2048 ) / sizeof( t_SymbolType ) ];

			l_symbol[0] = t_SymbolType(); // initialize the first struct
			l_symbol->SizeOfStruct = sizeof( * l_symbol );
			l_symbol->MaxNameLength = sizeof( l_symbol ) - sizeof( * l_symbol );
			l_symbol->Name[ 0 ] = 0;

			IMAGEHLP_LINE64 l_line = IMAGEHLP_LINE64();
			l_line.SizeOfStruct = sizeof( l_line );

			static const char l_no_symbol[] = "no symbol found";
	        
			for ( unsigned i = 0; i < m_addresses.size(); ++ i )
			{
				const char * l_value;

				static t_SymbolAddr l_offset = DWORD64();
				
				t_SymbolAddr		l_addr = m_addresses[ i ];

				if ( SymGetSymFromAddr64( l_process, l_addr, & l_offset, l_symbol ) )
				{
					l_value = l_symbol->Name;
				}
				else
				{
//					ShowLastError();
					l_value = l_no_symbol;
				}

				std::ostringstream  l_line_stream;

				l_line_stream << "(" << l_value << ") ";

				DWORD l_displacement;

				if (
					SymGetLineFromAddr64(
						l_process,
						m_addresses[ i ],
						& l_displacement,
						& l_line
					)
				) {

					l_line_stream << l_line.FileName << ":" << l_line.LineNumber;
				}
				else
				{
					l_line_stream << "No file found:0";
				}

				l_line_stream << " [" << reinterpret_cast<void*>( m_addresses[ i ] ) << "]";
	            
				StackFrame      l_frame = {
					reinterpret_cast<void *>( m_addresses[ i ] ),
					l_line_stream.str()
				};
	            
				m_frames.push_back( l_frame );
			}
        

        }
    }

    // ======== Begin =================================================

    virtual PtrDelegate<StackIterator *> Begin() const;

    // ======== operator< =============================================

    virtual bool operator< ( const StackTrace & i_rhs ) const
    {
        const StackTrace_Win32 * l_st_win32 = dynamic_cast< const StackTrace_Win32 * >( & i_rhs );

        AT_Assert( l_st_win32 != 0 );

        return m_addresses < l_st_win32->m_addresses;
    }
    
    // This contains the addresses collected from the
    // backtrace
    std::vector< DWORD64 >                  m_addresses;

    // Place to store all the stack frames
    //
    mutable std::vector< StackFrame >       m_frames;

};

// ======== StackIterator_Win32 ========================================

class AUSTRIA_EXPORT StackIterator_Win32
  : virtual public PtrTarget_MTVirtual,
    public StackIterator
{
    public:

    // ======== StackIterator_Win32 ====================================
    
    StackIterator_Win32(
        at::PtrDelegate< const StackTrace_Win32 * >    i_stace_trace
    )
      : m_stace_trace( i_stace_trace ),
        m_iterator( m_stace_trace->m_frames.begin() )
    {
    }
    

    // ======== Next ==================================================
    virtual bool Next()
    {
        if ( m_iterator == m_stace_trace->m_frames.end() )
        {
            return false;
        }
        
        return m_stace_trace->m_frames.end() != ++ m_iterator;

    }


    // ======== Previous ==============================================
    virtual bool Previous()
    {
        if ( m_iterator == m_stace_trace->m_frames.begin() )
        {
            return false;
        }

        -- m_iterator;
        return true;
    }


    // ======== Last ==================================================
    virtual void Last()
    {
        m_iterator = m_stace_trace->m_frames.end();
    }


    // ======== First =================================================
    virtual void First()
    {
        m_iterator = m_stace_trace->m_frames.begin();
    }


    // ======== IsValid ===============================================
    virtual bool IsValid() const
    {
        return m_stace_trace->m_frames.end() != m_iterator;
    }


    // ======== DeReference ===========================================
    const StackFrame & DeReference() const
    {
        AT_Assert( IsValid() );
        return * m_iterator;
    }
    
    // m_stace_trace points to the StackTrace_Win32 associated with this
    // iterator.
    
    at::Ptr< const StackTrace_Win32 * >      m_stace_trace;

    // m_iterator is the current iterator
    std::vector< StackFrame >::iterator     m_iterator;

};


// ======== StackTrace_Win32::Begin ====================================

PtrDelegate<StackIterator *> StackTrace_Win32::Begin() const
{
    ProcessFrames();
    
    return new StackIterator_Win32( at::PtrView< const StackTrace_Win32 * >( this ) );
}


// ======== NewStackTrace =============================================
/**
 * NewStackTrace is implementation dependant, it will create a new
 * stack trace and record the stack trace at this point.
 *
 * @return A pointer to a new stack trace.
 */

static DWORD s_image_type = 
    ( sizeof( void * ) <= 4 ) ? IMAGE_FILE_MACHINE_I386 : IMAGE_FILE_MACHINE_AMD64;

PtrDelegate< StackTrace * > NewStackTrace()
{
    DWORD64               l_addresses[ 1024 ];
    const int             l_max_count = CountElements( l_addresses );
    int					  l_frame_num = 0;
    
    STACKFRAME64          l_stack_frame = STACKFRAME64();
    CONTEXT               l_context = CONTEXT();

    HANDLE                l_process = GetCurrentProcess();
    HANDLE                l_thread = GetCurrentThread();
    
    // Requesting 
    l_context.ContextFlags = CONTEXT_FULL;

	BOOL l_result = GetThreadContext( l_thread, & l_context );
	
	AT_Assert( l_result );

	// The l_context.Eip value is bogus - grab it from the CPU.
	int l_ebp;
	__asm
	{
		mov l_ebp, ebp		; Get first argument
	}

	// 
	l_stack_frame.AddrPC.Offset = l_context.Eip;
	l_stack_frame.AddrPC.Mode = AddrModeFlat;

	l_stack_frame.AddrFrame.Offset = l_ebp;
	l_stack_frame.AddrFrame.Mode = AddrModeFlat;

	do {
		l_result = StackWalk64(
			s_image_type,               // DWORD MachineType
			l_process,                  // HANDLE hProcess
			l_thread,                   // HANDLE hThread
			& l_stack_frame,            // LPSTACKFRAME64 StackFrame
			& l_context,                // PVOID ContextRecord
			NULL,                       // PREAD_PROCESS_MEMORY_ROUTINE64 ReadMemoryRoutine
			SymFunctionTableAccess64,   // PFUNCTION_TABLE_ACCESS_ROUTINE64 FunctionTableAccessRoutine
			SymGetModuleBase64,         // PGET_MODULE_BASE_ROUTINE64 GetModuleBaseRoutine
			NULL                        // PTRANSLATE_ADDRESS_ROUTINE64 TranslateAddress
		);

		// if we failed, there is no more stack so we're done
		if ( l_result == FALSE )
		{
			break;
		}

		// store away the address
		l_addresses[ l_frame_num ++ ] = l_stack_frame.AddrPC.Offset;
	    
	} while ( l_frame_num < l_max_count );

	Ptr< StackTrace *> retval = new StackTrace_Win32( l_addresses + 1, l_frame_num -1 );
	return retval;
}

}; // namespace at
