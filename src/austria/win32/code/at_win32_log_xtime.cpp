/**
 * \file at_win32_log_xtime.cpp
 *
 * \author Gianni Mariani
 *
 */


#include "at_assert.h"
#include "at_win32_log_xtime.h"
#include <sstream>
#include <iomanip>

#include <windows.h>

namespace at
{

LogWriterTimeFormatPolicy_XTime_win32::t_formatted_time_type LogWriterTimeFormatPolicy_XTime_win32::FormatTime( const LogManagerTimePolicy_XTime_win32::t_time_type & i_time )
{
	const FILETIME * l_filetime = reinterpret_cast<const FILETIME *>( & i_time.m_time );
	FILETIME l_local_filetime;

	if ( ! FileTimeToLocalFileTime( l_filetime, & l_local_filetime ) )
	{
		return "<invalid time>";
	}

	SYSTEMTIME	l_sys_time;

	if ( ! FileTimeToSystemTime( & l_local_filetime, & l_sys_time ) )
	{
		return "<invalid time>";
	}

	std::ostringstream	l_str;

	l_str << std::setw( 2 ) << std::setfill('0') 
		<< l_sys_time.wHour << ":"
		<< std::setw( 2 ) << std::setfill('0')
		<< l_sys_time.wMinute << ":"
		<< std::setw( 2 ) << std::setfill('0')
		<< l_sys_time.wSecond << "."
		<< std::setw( 3 ) << std::setfill('0')
		<< l_sys_time.wMilliseconds << "  "
		<< std::setw( 4 ) << std::setfill('0')
		<< l_sys_time.wYear << "/"
		<< std::setw( 2 ) << std::setfill('0')
		<< l_sys_time.wMonth << "/"
		<< std::setw( 2 ) << std::setfill('0')
		<< l_sys_time.wDay;

	return l_str.str();
}

} // namespace at

