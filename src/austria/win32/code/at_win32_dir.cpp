/*
 *
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *  at_win32_dir.cpp
 *
 *  This contains the Windows implementation of at_dir.h.
 *
 */

#define _WIN32_WINNT 0x0501   
#include <WTypes.h>
#include <WinBase.h>

#include "at_file.h"
#include "at_dir.h"

#include "at_win32_file.h"
#include "at_win32_dir.h"
#include <direct.h>
#include "winerror.h"
//#include "winbase.h"

#include <map>

// Disable Forcing value performance warning
#pragma warning (disable : 4800)

// Austria namespace
namespace at
{
    ////////////////////////////////////////////////////////////// 
    //
    //  DirEntry
    //
    //////////////////////////////////////////////////////////////

    // ======== DirEntry::IsDirectory ================================================
    bool DirEntry::IsDirectory () const
    {
        const char* name = m_entry_name.String ();

        DWORD attributes = ::GetFileAttributes ( name );
        if ( attributes == INVALID_FILE_ATTRIBUTES )
        {
            throw FileErrorBasic ( ::GetLastError () );
        }

        return ( attributes & FILE_ATTRIBUTE_DIRECTORY ) != 0;
    }

    ////////////////////////////////////////////////////////////// 
    //
    //  DirIterator
    //
    //////////////////////////////////////////////////////////////

    // ======== DirIterator =============================================
    DirIterator::DirIterator ( const FilePath& i_dir_name ) : m_dir_name ( i_dir_name ) 
    {
        m_handle.handle = INVALID_HANDLE_VALUE;
    }

    // ======== Next ==================================================
    bool DirIterator::Next ()
    {
        if ( m_handle.handle == INVALID_HANDLE_VALUE )
            return false; 

        WIN32_FIND_DATA data;
        if ( ::FindNextFile ( m_handle.handle, &data ) == 0 )
        {
            DWORD error = ::GetLastError ();
            if ( error != ERROR_NO_MORE_FILES )
            {
                throw FileErrorBasic ( error );
            }
             else 
            { 
                return false; 
            } // end reached
        }
        
        // Save the File found
        m_dir_entry.m_entry_name = std::string ( data.cFileName );
        
        return true;
    }

    // ======== First =================================================
    bool DirIterator::First () 
    {
        WIN32_FIND_DATA data;

        std::string dir_listing ( m_dir_name.String () );
        dir_listing += "/*";

        m_handle.handle = ::FindFirstFileA ( dir_listing.c_str (), &data );

        if ( INVALID_HANDLE_VALUE == m_handle.handle )
        {
            return false; // Probably No files here
        }

        const char *name = data.cFileName;
        // Check for and skip over Dot, Dot Dot
        bool dotdot = false;
        do
        {
            dotdot = name[0] == '.' && ( name[1] == '\0' || (name[1] =='.' && name[2] == '\0' ) );
            if ( dotdot )
            {
                if ( Next () )
                    name = m_dir_entry.m_entry_name.String ();
                else
                    return false; // no files!
            }
        } while ( dotdot );

        // Save the first File
        m_dir_entry.m_entry_name = std::string ( name );

        return true;
    }

    // ======== GetEntry ===========================================
    const DirEntry & DirIterator::GetEntry () const
    {
        return m_dir_entry;
    }

    DirIterator::~DirIterator ()
    {
        if ( m_handle.handle != INVALID_HANDLE_VALUE )
            ::FindClose ( m_handle.handle );
    }

    ////////////////////////////////////////////////////////////// 
    //
    //  Directory
    //
    //////////////////////////////////////////////////////////////

    // ======== Create =================================================
    bool Directory::Create ( const FilePath& i_dir_name )
    {
        return ::CreateDirectory ( i_dir_name.String (), 0 );
    }

    // ======== Remove =================================================
    bool Directory::Remove ( const FilePath& i_dir_name, bool i_delete_if_not_empty )
    {
        Directory dir ( i_dir_name );

        bool isdir = dir.IsDirectory ();
        if ( isdir == false )
            return false;

        if ( true == i_delete_if_not_empty )
        {
            dir.Clear ( true );
        }

        if ( false == ::RemoveDirectory ( i_dir_name.String () ) )
        {
            DWORD error = ::GetLastError ();
            if ( false == i_delete_if_not_empty && ERROR_DIR_NOT_EMPTY == error )
            {
                return false;
            }
            else
            {
                throw FileErrorBasic ( error );
            }
        }
        
        return true; 
    }
    
    // ======== ChangeCurrent =================================================
    bool Directory::ChangeCurrent ( const FilePath& i_dir_name )
    {
        std::cout << _getcwd ( NULL, 0 ) << std::endl;

        return _chdir ( i_dir_name.String () );
    }

    // ======== Rename =================================================
    bool Directory::Rename ( const FilePath& i_dir_old_name, const FilePath& i_dir_new_name )
    {
        return ::MoveFile ( i_dir_old_name.String (), i_dir_new_name.String () );
    }

    // ======== Exists =================================================
    bool Directory::Exists ( const FilePath& i_dir_name )
    {
        DWORD attributes = ::GetFileAttributes ( i_dir_name.String () );
        if ( attributes == INVALID_FILE_ATTRIBUTES )
        {
            return false;
        }

        return ( attributes & FILE_ATTRIBUTE_DIRECTORY ) != 0;
    }

    bool Directory::IsDirectory ()
    {
        return Directory::Exists ( m_dir_name );
    }

    // ======== Directory::Directory==========================================
    Directory::Directory ( const FilePath& i_dir_name ) : m_dir_name ( i_dir_name )
    {
    }

    // ======== IsEmpty =================================================
    bool Directory::IsEmpty ()
    {
        at::PtrDelegate <DirIterator *> di = ListEntries ();

        return !di->First ();
    }

    // ======== Clear =================================================
    bool Directory::Clear  ( bool i_delete_sub_directories )
    {
        at::PtrDelegate <DirIterator *> di = ListEntries ();

        if ( di->First () )
        {
            do
            {
                DirEntry entry = di->GetEntry ();
                std::string file = m_dir_name.String ();
                file += "/";
                file += entry.m_entry_name.String ();
                entry.m_entry_name = file;

                if ( !entry.IsDirectory () )
                {
                    WFile wf ( entry.m_entry_name );
                    wf.SetPermissions ( FilePermission::UserWrite );
                    if ( false == BaseFile::Remove ( entry.m_entry_name ) )
                    {
                        return false;
                    }
                }
                else
                {
                    if ( i_delete_sub_directories )
                    {
                        Remove ( entry.m_entry_name, true );
                    }
                }
            }
            while ( di->Next () );
        }

        return true; // Directory was erased
    }

    // ======== List =================================================
    PtrDelegate<DirIterator *> Directory::ListEntries () const
    {
        PtrDelegate<DirIterator*> di = new DirIterator ( m_dir_name );

        return di;
    }

    // ======== operator == =============================================
    bool Directory::operator == ( const Directory & i_rhs ) const
    {
        AT_Assert ( false ); // Not Supported for 1.0
        return true;
    }

    // ======== ~Directory::Directory =============================================
    Directory::~Directory ()
    {
    }
};
