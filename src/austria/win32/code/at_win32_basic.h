//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_win32_atomic.h
 *
 */

#ifndef x_at_win32_basic_h_x
#define x_at_win32_basic_h_x 1

// Austria namespace
namespace at
{

//
// This provides some basic win32 helper classes.
//


// ======== Win32Struct ===============================================
/**
 * Win32Struct allows the construction of common Win32 struct
 * types which contain a member that containts within it
 *
 */

template <typename w_type, typename w_member_type, w_member_type w_type::* w_member>
struct Win32Struct
{
    w_type              m_obj;

    Win32Struct()
      : m_obj()
    {
        m_obj.*w_member = sizeof( w_type );
    }

    operator w_type * ()
    {
        return & m_obj;
    }

    operator const w_type * () const
    {
        return & m_obj;
    }

    w_type * operator->()
    {
        return & m_obj;
    }

    const w_type * operator->() const
    {
        return & m_obj;
    }
};

template <typename w_type>
struct PlainStruct
{
    w_type              m_obj;

    PlainStruct()
      : m_obj()
    {
    }

    operator w_type * ()
    {
        return & m_obj;
    }

    operator const w_type * () const
    {
        return & m_obj;
    }

    w_type * operator->()
    {
        return & m_obj;
    }

    const w_type * operator->() const
    {
        return & m_obj;
    }
};



}; // namespace

#endif // x_at_win32_basic_h_x



