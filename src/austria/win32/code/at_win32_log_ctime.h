/**
 * \file at_win32_log_ctime.h
 *
 * \author Bradley Austin
 *
 */


#ifndef x_at_win32_log_ctime_h_x
#define x_at_win32_log_ctime_h_x 1


#include <string>


namespace at
{


    class LogManagerTimePolicy_CTime_win32_TimeTypeImplBase
    {

    public:

        virtual const LogManagerTimePolicy_CTime_win32_TimeTypeImplBase * Clone() const = 0;

        virtual ~LogManagerTimePolicy_CTime_win32_TimeTypeImplBase() {}

    };

    class LogManagerTimePolicy_CTime_win32_TimeType
    {

    public:

        const LogManagerTimePolicy_CTime_win32_TimeTypeImplBase * const m_impl;

        inline LogManagerTimePolicy_CTime_win32_TimeType(
            const LogManagerTimePolicy_CTime_win32_TimeType & i_time
        )
          : m_impl( i_time.m_impl->Clone() )
        {
        }

        inline LogManagerTimePolicy_CTime_win32_TimeType(
            const LogManagerTimePolicy_CTime_win32_TimeTypeImplBase & i_impl
        )
          : m_impl( i_impl.Clone() )
        {
        }

        inline ~LogManagerTimePolicy_CTime_win32_TimeType()
        {
            delete m_impl;
        }

    private:

        /* Unimplemented */
        void operator=( const LogManagerTimePolicy_CTime_win32_TimeType & );

    };


    class LogManagerTimePolicy_CTime_win32
    {

    public:

        typedef  LogManagerTimePolicy_CTime_win32_TimeType  t_time_type;

        static t_time_type GetTime();

    protected:

        inline LogManagerTimePolicy_CTime_win32() {}
        inline ~LogManagerTimePolicy_CTime_win32() {}

    private:

        /* Unimplemented */
        LogManagerTimePolicy_CTime_win32
            ( const LogManagerTimePolicy_CTime_win32 & );
        LogManagerTimePolicy_CTime_win32 & operator=
            ( const LogManagerTimePolicy_CTime_win32 & );

    };


    class LogWriterTimeFormatPolicy_CTime_win32
    {

    public:

        typedef  std::string  t_formatted_time_type;

        static t_formatted_time_type FormatTime( const LogManagerTimePolicy_CTime_win32_TimeType & i_time );

    protected:

        inline LogWriterTimeFormatPolicy_CTime_win32() {}
        inline ~LogWriterTimeFormatPolicy_CTime_win32() {}

    private:

        /* Unimplemented */
        LogWriterTimeFormatPolicy_CTime_win32
            ( const LogWriterTimeFormatPolicy_CTime_win32 & );
        LogWriterTimeFormatPolicy_CTime_win32 & operator=
            ( const LogWriterTimeFormatPolicy_CTime_win32 & );

    };


}


#endif
