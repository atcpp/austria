

#include "at_id_generator.h"
#include "at_base64.h"

#include <windows.h>
    
// Austria namespace
namespace at
{

// ======== NewIdent =============================================

std::string NewIdent()
{
    ::UUID        l_uuid;

    ::UuidCreate( & l_uuid );

    BaseNEncoder<6, BaseNEndianLittle>          l_encoder( g_urlsafe_base64_key );

    std::string     l_result;

    std::back_insert_iterator< std::string >    l_inserter( l_result );

    l_encoder.Encode(
        ( char * ) & l_uuid,
        ( sizeof l_uuid ) + ( char * ) & l_uuid,
        l_inserter
    );

    l_encoder.Flush( l_inserter );
    
    return l_result;
}

// ======== BinaryRandomSequence =============================================

AUSTRIA_EXPORT std::string BinaryRandomSequence( unsigned i_length )
{
    ::UUID          l_uuid;

    ::UuidCreate( & l_uuid );

    {
        LARGE_INTEGER   l_val;

        ::QueryPerformanceCounter( & l_val );

        // add a little more entropy on the first one.
        * ( ( at::Uint64 *) & l_uuid ) += ( * ( at::Uint64 *) & l_val );
    }
    
    std::string l_result;
    
    while ( 1 )
    {
        unsigned l_count = ( i_length > sizeof( l_uuid) ) ? sizeof( l_uuid) : i_length;
        
        l_result += std::string( (char*) & l_uuid, l_count );
        i_length -= l_count;

        if ( ! i_length )
        {
            return l_result;
        }
        ::UuidCreate( & l_uuid );
    }
}

} // namespace

