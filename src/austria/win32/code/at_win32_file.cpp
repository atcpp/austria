/*
 *
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *  at_win32_file.cpp
 *
 *  This contains the Windows implementation of at_file.h.
 *
 */

#define I_DONT_WANT_ANNOYING_WARNINGS

// This needs to be defined to Use QueueUserAPC
#define _WIN32_WINNT 0x0501   
#include <WTypes.h>
#include <WinBase.h>
#include <direct.h>

#include "at_file.h"
#include "at_win32_file.h"
#include "at_trace.h"
#include "at_thread.h"
#include "winerror.h"
//#include "winbase.h"

#include <map>

// Disable Forcing value performance warning
#pragma warning (disable : 4800)

// Austria namespace
namespace at
{
    ////////////////////////////////////////////////////////////// 
    //
    //  Fileinitializer 
    //
    //////////////////////////////////////////////////////////////

    // Intialize Initializer Static
    at::CallbackTask *at::FileInitializer::m_call_back_thread;

    // Register the fileinitializer ' Z' so it's called last
    AT_MakeFactory2P( "ZFileInitializer", FileInitializer, Initializer, DKy, int & , const char ** & );

    ////////////////////////////////////////////////////////////// 
    //
    //  FileError for Win32 is implemented as FileErrorBasic
    //
    //////////////////////////////////////////////////////////////

    // ======== std::map<long,FileErrorBasic*> g_errormap ===================================================
    /**
     *      g_errormap contains the map of OS errorcodes to File Errors
     */
    static std::map<long,FileErrorBasic*> s_errormap;

    // Initialize all the Custom Errors with their error messages
    static FileErrorBasic s_success_basic               ( "Success", NO_ERROR ); 
    static FileErrorBasic s_end_of_file_basic           ( "End of File (EOF)", ERROR_HANDLE_EOF );
    static FileErrorBasic s_waiting_read_basic          ( "Waiting for an ASync Read to Finish" );
    static FileErrorBasic s_waiting_write_basic         ( "Waiting for an ASync Write to Finish" );
    static FileErrorBasic s_waiting_open_basic          ( "Waiting for an ASync Open to Complete" );
    static FileErrorBasic s_waiting_close_basic         ( "Waiting for an ASync Close to Complete" );
    static FileErrorBasic s_file_not_found_basic        ( "File not Found", ERROR_FILE_NOT_FOUND );
    static FileErrorBasic s_bad_path_basic              ( "Bad Path", ERROR_PATH_NOT_FOUND );
    static FileErrorBasic s_too_many_open_files_basic   ( "Too many open files", ERROR_TOO_MANY_OPEN_FILES );
    static FileErrorBasic s_access_denied_basic         ( "Access Denied", ERROR_ACCESS_DENIED );
    static FileErrorBasic s_invalid_file_basic          ( "Invalid File", ERROR_INVALID_HANDLE );
    static FileErrorBasic s_remove_current_dir_basic    ( "Remove Current Directory Failed", ERROR_CURRENT_DIRECTORY );
    static FileErrorBasic s_directory_full_basic        ( "Directory is Full" , ERROR_NO_MORE_FILES );
    static FileErrorBasic s_bad_seek_basic              ( "Negative Seek", ERROR_NEGATIVE_SEEK );
    static FileErrorBasic s_hard_io_basic               ( "Hard Disk Error", ERROR_SECTOR_NOT_FOUND );
    static FileErrorBasic s_sharing_violation_basic     ( "Sharing Violation", ERROR_SHARING_VIOLATION );
    static FileErrorBasic s_lock_violation_basic        ( "Lock Violation", ERROR_LOCK_VIOLATION );
    static FileErrorBasic s_disk_full_basic             ( "Disk Full", ERROR_DISK_FULL );   
    static FileErrorBasic s_bad_param_basic             ( "Invalid Parameter", ERROR_INVALID_PARAMETER );
    static FileErrorBasic s_overflow_basic              ( "Buffer Overflow", ERROR_MORE_DATA );
    static FileErrorBasic s_too_many_async_calls_basic  ( "Too many Async Calls" );
    static FileErrorBasic s_user_aborted_basic          ( "Cancelled by User", ERROR_OPERATION_ABORTED );
    static FileErrorBasic s_already_exists_basic        ( "File already exists", ERROR_FILE_EXISTS );
    static FileErrorBasic s_not_supported_basic         ( "Not supported", ERROR_NOT_SUPPORTED );
    static FileErrorBasic s_file_exists					( "File exists", ERROR_ALREADY_EXISTS );
    static FileErrorBasic s_not_implemented             ( "Not implemented" );

    // Assign the Static FileErrorBasic members
    const FileError &FileError::s_success               = s_success_basic;
    const FileError &FileError::s_end_of_file           = s_end_of_file_basic;
    const FileError &FileError::s_waiting_read          = s_waiting_read_basic;
    const FileError &FileError::s_waiting_write         = s_waiting_write_basic;
    const FileError &FileError::s_waiting_open          = s_waiting_open_basic;
    const FileError &FileError::s_waiting_close         = s_waiting_close_basic;
    const FileError &FileError::s_file_not_found        = s_file_not_found_basic;
    const FileError &FileError::s_bad_path              = s_bad_path_basic;
    const FileError &FileError::s_too_many_open_files   = s_too_many_open_files_basic;
    const FileError &FileError::s_access_denied         = s_access_denied_basic;
    const FileError &FileError::s_invalid_file          = s_invalid_file_basic;
    const FileError &FileError::s_remove_current_dir    = s_remove_current_dir_basic;
    const FileError &FileError::s_directory_full        = s_directory_full_basic;
    const FileError &FileError::s_bad_seek              = s_bad_seek_basic;
    const FileError &FileError::s_hard_io               = s_hard_io_basic;
    const FileError &FileError::s_sharing_violation     = s_sharing_violation_basic;
    const FileError &FileError::s_lock_violation        = s_lock_violation_basic;
    const FileError &FileError::s_disk_full             = s_disk_full_basic;
    const FileError &FileError::s_bad_param             = s_bad_param_basic;
    const FileError &FileError::s_overflow              = s_overflow_basic;
    const FileError &FileError::s_too_many_async_calls  = s_too_many_async_calls_basic;
    const FileError &FileError::s_user_aborted          = s_user_aborted_basic;
    const FileError &FileError::s_already_exists        = s_already_exists_basic;
	const FileError &FileError::s_not_supported         = s_not_supported_basic;
	const FileError &FileError::s_file_exists           = s_file_exists;

    // ======== FileErrorBasic FileErrorBasic( const char* i_name, long i_errorno ) ===
    FileErrorBasic::FileErrorBasic ( const char* i_name, long i_errorno ) : m_errortext ( i_name ), m_errorno ( i_errorno )
    {
        if ( i_errorno != -1 )
        {
            s_errormap[i_errorno] = this;
        }
    }

    // ======== FileErrorBasic FileErrorBasic( long i_errorno ) =======================
    FileErrorBasic::FileErrorBasic ( long i_errorno )
    {
        FileErrorBasic* fe = s_errormap[i_errorno];
        if ( fe )
        {
            *this = fe;
        }
        else
        {
            AT_Assert ( false ); // This OS error (i_errorno) needs to be added to the global list above. 
                                 // See Bill. For Win32 look up the number in Winerror.h. For Gx86 see the Man
                                 // Page for errno.
        }
    }

    // ======== FileErrorBasic FileErrorBasic( const FileError& i_fileerror ) ==========
    FileErrorBasic::FileErrorBasic ( const FileError& i_fileerror )
    {
        m_errortext = i_fileerror.What ();
        m_errorno = i_fileerror.ErrorCode ();
    }

    // ======== FileErrorBasic & FileErrorBasic::operator = ( const FileErrorBasic* i_fileerror )================
    const FileErrorBasic & FileErrorBasic::operator = ( const FileErrorBasic* i_fileerror )
    { 
        if (this != i_fileerror) {
            m_errortext = i_fileerror->What ();
            m_errorno = i_fileerror->ErrorCode ();
        }
        return *this; 
    }

    // ======== FileAttr_List_x( AT_StaticVariableDefn ) ========================================
    /**
     *  Defines the static variables needed for attribute handling
     */
    FileAttr_List_x( AT_StaticVariableDefn )

    /////////////////////////////////////////////////////////////////////
    //
    // Win32 Implementation of BaseFile 
    //
    /////////////////////////////////////////////////////////////////////

    /**
     *  Initialize the Seek Constants
     */
    const int BaseFile::SeekBegin = FILE_BEGIN; 
    const int BaseFile::SeekCurrent = FILE_CURRENT;
    const int BaseFile::SeekEnd = FILE_END; 

    /**
     *  m_filelock is used to lock the BaseFile static functions
     */
    at::Mutex BaseFile::m_filelock;

    // ======== BaseFile::Exists ========================================
    bool BaseFile::Exists ( const FilePath & i_file_path )
    {
        //Lock<Mutex> m_l ( m_filelock );
        return INVALID_FILE_ATTRIBUTES != ::GetFileAttributes ( i_file_path.String () );
    }

    // ======== BaseFile::Rename ========================================
    bool BaseFile::Rename ( const FilePath & i_file_pathold, const FilePath & i_file_pathnew )
    {
        //Lock<Mutex> m_l ( m_filelock );
        return ::MoveFileEx ( i_file_pathold.String (), i_file_pathnew.String (),
                              MOVEFILE_REPLACE_EXISTING | MOVEFILE_WRITE_THROUGH );
    }

    // ======== BaseFile::HardLink ======================================
    bool BaseFile::HardLink ( const FilePath & i_file_pathold, const FilePath & i_file_pathnew )
    {
        //Lock<Mutex> m_l ( m_filelock );
        return 0 != ::CreateHardLink ( i_file_pathnew.String (), i_file_pathold.String ( ), NULL );
    }

    // ======== SymLink =============================================

    bool BaseFile::SymLink ( const FilePath & i_file_path, const FilePath & i_link_path )
    {
        throw s_not_implemented;
        return false;
    }

    // ======== ReadSymLink =============================================

    bool BaseFile::ReadSymLink ( const FilePath & i_link_path, std::string & o_symlink_val )
    {
        throw s_not_implemented;
        return false;
    }
    
    // ======== RemoveSymLink =============================================
    
    bool BaseFile::RemoveSymLink ( const FilePath & i_link_path )
    {
        throw s_not_implemented;
        return false;
    }
    
    // ======== BaseFile::Remove ========================================
    bool BaseFile::Remove ( const FilePath & i_file_path )
    {
        //Lock<Mutex> m_l ( m_filelock );
        return ::DeleteFile ( i_file_path.String () );
    }

    // ======== BaseFile::Open ( i_file_path, i_attr ) =======================
    bool BaseFile::Open ( const FilePath & i_file_path, const FileAttr &i_attr )
    {
        // Assert if already open 
        AT_Assert ( m_filehandle.handle == INVALID_HANDLE_VALUE );

        // Adjust the Mode to match this class type
        FileAttr attr = i_attr;
        if ( false == CheckAttributes ( &attr ) )
        {
            return false;
        }

        DWORD access = 0;
        DWORD disp = OPEN_EXISTING;
        DWORD flags = FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN;

        // Read Access
        if ( attr & FileAttr::Read ) 
        {
            access |= GENERIC_READ;
            disp = OPEN_EXISTING;
        }

        // Write Access
        if ( attr & FileAttr::Write ) 
        {
            access |= GENERIC_WRITE;
        }

        // Write Access
        if ( attr & FileAttr::ReadWrite ) 
        {
            access |= ( GENERIC_WRITE | GENERIC_READ );
        }

        // Disposition always Create
        if ( attr & FileAttr::Create )
        {
            disp = CREATE_ALWAYS;
        }

        // Disposition Open Exisiting and Append to End
        if ( attr & FileAttr::Preserve )
        {
            disp = OPEN_EXISTING;
        }

        if ( attr & FileAttr::New )
        {
            disp = CREATE_NEW;
        }

        AT_Assert ( access > 0 );

        if ( attr & FileAttr::Mapped )
        {
            // REV @@ TODO -- mapped files
        }

        if ( attr & FileAttr::DirectIO )
        {
            flags |= FILE_FLAG_NO_BUFFERING;

            DWORD dwSectPerClust;
            DWORD dwBytesPerSect;
            DWORD dwFreeClusters; 
            DWORD dwTotalClusters;

            // REV @@ TODO -- scan drives
            GetDiskFreeSpace ("C:", &dwSectPerClust, &dwBytesPerSect, &dwFreeClusters, &dwTotalClusters );
        }

        if ( attr & FileAttr::Async )
        {
            flags |= FILE_FLAG_OVERLAPPED;
        }

        if ( attr & FileAttr::SyncWrite )
        {
            flags |= FILE_FLAG_WRITE_THROUGH;
        }

        SECURITY_ATTRIBUTES sa;
        sa.nLength = sizeof SECURITY_ATTRIBUTES;
        sa.bInheritHandle = true;
        sa.lpSecurityDescriptor = NULL;
        // Try to Open File with Desired Access / Disposition
        HANDLE hfilehandle;
        hfilehandle = ::CreateFile ( i_file_path.String (),
                access,
                FILE_SHARE_READ | FILE_SHARE_WRITE,
                &sa,
                disp,
                flags,
                NULL );

        // If its a valid handle then the create was successful
        if ( hfilehandle != INVALID_HANDLE_VALUE )
        {
            m_filehandle.handle = hfilehandle;
            m_fullname = i_file_path;
            m_mode = i_attr;
            m_closeondelete = true;

            return true;
        }
        else
        {
            // Otherwise report the error
            long error = ::GetLastError ();

            // Record the Error
            SetError ( error );

#ifdef FILESTRICT
            // Throw an Exception, the file should be able to open
            throw FileErrorBasic ( error );
#endif
        }
        return false;
    }

    // ======== BaseFile::Open ===============================================
    bool BaseFile::Open ()
    {
        return Open ( m_fullname, m_mode ); 
    }

    // ======== Operations: BaseFile::IsOpen =================================
    bool BaseFile::IsOpen ()
    {
        if ( m_filehandle.handle != INVALID_HANDLE_VALUE )
        {
            return true;
        }

        return false;
    }

    // ======== BaseFile::AbortAllBlockingCalls ==================================
    bool BaseFile::AbortAllBlockingCalls ()
    {
		int result = true;
		if ( m_filehandle.handle != INVALID_HANDLE_VALUE ) {

			// This will cancel *all* outstanding Async Calls against the specified handle
			// and allows the overlapped structure and all it owns to be destructed
			result = ::CancelIo ( m_filehandle.handle );
            // REV: TODO remove handle from wait tables
		}
        return result;
    }

    // ======== BaseFile::Flush () ==========================================
    bool BaseFile::Flush ()
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        if ( !::FlushFileBuffers ( m_filehandle.handle ) )
        {
            SetError ( ::GetLastError() );
            return false;
        }
        return true;
    }

    // ======== BaseFile::Close  ()===========================================
    void BaseFile::Close () 
    {

        bool error = false;
        // If we have a valid File handle then Close
        if ( m_filehandle.handle != INVALID_HANDLE_VALUE )
        {
            error = !::CloseHandle ( m_filehandle.handle );
        }

        m_filehandle.handle = INVALID_HANDLE_VALUE;
        m_closeondelete = false;

        // There was an error trying to Close the File
        if ( error )
        {
            throw FileErrorBasic ( ::GetLastError () );
        }
    }

    // ======== BaseFile::GetError () ========================================
    const FileError& BaseFile::GetError () const
    {
        return *m_lasterror;
    }
    
    // ======== BaseFile::SetError ( long i_error ) ==========================
    void BaseFile::SetError ( long i_error )
    {
        if ( m_lasterror )
        {
            delete m_lasterror; 
        }

        m_lasterror = new FileErrorBasic ( i_error );
    }

    // ======== BaseFile::( const FileError& i_error ) =======================
    void BaseFile::SetError ( const FileError& i_error )
    {
        SetError ( i_error.ErrorCode () );
    }

    // ======== BaseFile::( long i_off, int i_from ) ========================
    Int64 BaseFile::Seek ( Int64 i_off, int i_from ) const
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );
        AT_Assert ( i_from == FILE_BEGIN || i_from == FILE_END || i_from == FILE_CURRENT );

        LARGE_INTEGER off;
        LARGE_INTEGER r_off;

        off.QuadPart = i_off;
        bool status = ::SetFilePointerEx ( m_filehandle.handle, off, &r_off, (DWORD)i_from );
        if ( !status )
        {
            long error = ::GetLastError ();
            if ( error != NO_ERROR )
            {
                throw FileErrorBasic ( error );
            }
            r_off.QuadPart = -1;
        }
        return r_off.QuadPart;
    }

    // ======== BaseFile::GetPosition () =====================================
    Int64 BaseFile::GetPosition () const
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        LARGE_INTEGER off;
        LARGE_INTEGER r_off;

        off.QuadPart = 0;
        r_off = off;

        bool status = ::SetFilePointerEx ( m_filehandle.handle, off, &r_off, FILE_CURRENT );
        if ( !status )
        {
            long error = ::GetLastError ();
            if ( error != NO_ERROR )
            {
                throw FileErrorBasic ( error );
            }
        }
        return r_off.QuadPart;
    }

    // ======== BaseFile::SetLength ( Unit64 i_newlen ) ========================
    bool BaseFile::SetLength ( Int64 i_newlen )
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );
        try
        {
            Seek ( i_newlen, FILE_BEGIN );

            if ( 0 == ::SetEndOfFile ( m_filehandle.handle ) )
            {
                long error = ::GetLastError ();
                throw FileErrorBasic ( error );
            }
        }
        catch ( FileErrorBasic & fe )
        {
            SetError ( fe );

            return false;
        }

        return true;
    }

    // ======== BaseFile::GetLength () =======================================
    Int64 BaseFile::GetLength () const
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        LARGE_INTEGER fsize;
        bool status = ::GetFileSizeEx ( m_filehandle.handle, &fsize);
        if ( !status )
        {
            long error = ::GetLastError ();
            if ( error != NO_ERROR )
            {
                throw FileErrorBasic ( error );
            }
        }
        return fsize.QuadPart;
    }

    // ======== BaseFile::GetLength () =======================================
    Int64 BaseFile::GetLength( const FilePath & i_file_path )
    {
        WIN32_FILE_ATTRIBUTE_DATA l_fd;
        ::ZeroMemory( &l_fd, sizeof( WIN32_FILE_ATTRIBUTE_DATA ) );

        if( ! ::GetFileAttributesEx(
                  i_file_path.String(),
                  GetFileExInfoStandard,
                  static_cast< void* >( &l_fd )
              )
          )
        {
            long error = ::GetLastError ();
            throw FileErrorBasic ( error );
        }

        ULARGE_INTEGER l_uli;
        l_uli.LowPart = l_fd.nFileSizeLow;
        l_uli.HighPart = l_fd.nFileSizeHigh;

        return l_uli.QuadPart;
    }


    // ======== BaseFile::LockRange( Int64 i_pos, Int64 i_count ) ==============
    bool BaseFile::LockRange( Int64 i_pos, Int64  i_count, bool i_wait ) 
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        LARGE_INTEGER pos;
        pos.QuadPart = i_pos;

        LARGE_INTEGER count;
        count.QuadPart = i_count;

        OVERLAPPED  l_overlapped;
        l_overlapped.Internal = 0;
        l_overlapped.InternalHigh = 0;
        l_overlapped.Offset = pos.LowPart;
        l_overlapped.OffsetHigh = pos.HighPart;
        l_overlapped.hEvent = 0;
                    
        return ::LockFileEx( 
			m_filehandle.handle, 
			LOCKFILE_EXCLUSIVE_LOCK | ( i_wait ? 0 : LOCKFILE_FAIL_IMMEDIATELY ), 
			0, 
			count.LowPart, 
			count.HighPart, 
			& l_overlapped
		);
    }

    // ======== BaseFile::UnlockRange( long i_pos, long i_count ) ============
    bool BaseFile::UnlockRange( Int64 i_pos, Int64 i_count ) 
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        LARGE_INTEGER pos;
        pos.QuadPart = i_pos;

        LARGE_INTEGER count;
        count.QuadPart = i_count;

        return ::UnlockFile ( m_filehandle.handle, pos.LowPart, pos.HighPart, count.LowPart, count.HighPart);
    }

    // ======== BaseFile::SetAttributes ( int i_permissions ) ===============
    void BaseFile::SetPermissions ( int i_permissions )
    {
        // i_level is ignored
        long attributes = 0;

        if ( FilePermission::UserWrite  & i_permissions ||
             FilePermission::GroupWrite & i_permissions ||
             FilePermission::WorldWrite & i_permissions )
        {
            attributes = FILE_ATTRIBUTE_NORMAL;
        }

        if ( FilePermission::UserRead  & i_permissions ||
             FilePermission::GroupRead & i_permissions || 
             FilePermission::WorldRead & i_permissions )
        {
            // Only set read if Write is not specified
            if ( ! ( FilePermission::UserWrite  & i_permissions ) && 
                 ! ( FilePermission::GroupWrite & i_permissions ) &&
                 ! ( FilePermission::WorldWrite & i_permissions ) )
                attributes = FILE_ATTRIBUTE_READONLY;
        }

        if ( !::SetFileAttributes ( m_fullname.String (), attributes ) )
        {
            long error = ::GetLastError ();
            throw FileErrorBasic ( error );
        }
    }

    // ======== BaseFile::GetPermissions () ===========================
    int BaseFile::GetPermissions()
    {
      long attributes = ::GetFileAttributes( m_fullname.String() );
      if (attributes == INVALID_FILE_ATTRIBUTES)
      {
        long error = ::GetLastError ();
        throw FileErrorBasic ( error );
        return -1;
      }

      int l_permissions = 0;
      if (attributes & FILE_ATTRIBUTE_NORMAL)
      {
        l_permissions = FilePermission::UserWrite |
                        FilePermission::GroupWrite |
                        FilePermission::WorldWrite |
                        FilePermission::UserRead |
                        FilePermission::GroupRead |
                        FilePermission::WorldRead;
      }

      if (attributes & FILE_ATTRIBUTE_READONLY)
      {
        l_permissions = FilePermission::UserRead |
                        FilePermission::GroupRead |
                        FilePermission::WorldRead;
      }

      return l_permissions;
    }

    // CopyTime does a copy win32 FILETIME to an at::TimeStamp
    inline static void CopyTime( const ::FILETIME & i_time, TimeStamp & o_timestamp )
    {
        ULARGE_INTEGER l_uli;
        l_uli.LowPart = i_time.dwLowDateTime;
        l_uli.HighPart = i_time.dwHighDateTime;
        o_timestamp = TimeStamp( l_uli.QuadPart );
    }

    // ======== BaseFile::GetDateTime ( int i_flag, TimeStamp* i_timestamp ) =
    bool BaseFile::GetDateTime ( int i_flag, TimeStamp & o_timestamp ) const
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

	    BOOL bRet = FALSE;
	    WIN32_FILE_ATTRIBUTE_DATA fd;
        ::ZeroMemory( &fd, sizeof ( WIN32_FILE_ATTRIBUTE_DATA ) );

	    if ( m_filehandle.handle ) 
        {
		    if( ::GetFileAttributesEx ( m_fullname.String (), GetFileExInfoStandard, ( void* ) &fd ) ) 
            {
                switch ( i_flag )
                {
                    case LastWrite : CopyTime( fd.ftLastWriteTime, o_timestamp ); break;
                    case Created   : CopyTime( fd.ftCreationTime,  o_timestamp ); break;
                    case Accessed  : CopyTime( fd.ftLastAccessTime, o_timestamp ); break;

                    default: AT_Abort();
                }

			    bRet = TRUE;
		    }
	    }

        return bRet;
    }

    // ======== BaseFile::GetDateTime( const FilePath & i_file_path, int i_flag, TimeStamp* i_timestamp ) =
    bool BaseFile::GetDateTime(
        const FilePath & i_file_path,
        int i_flag,
        TimeStamp & o_timestamp
    )
    {
        bool l_ret = false;
        WIN32_FILE_ATTRIBUTE_DATA l_fd;
        ::ZeroMemory( &l_fd, sizeof( WIN32_FILE_ATTRIBUTE_DATA ) );

        if( ::GetFileAttributesEx(
                i_file_path.String(),
                GetFileExInfoStandard,
                static_cast< void* >( &l_fd )
            )
          )
        {
            FILETIME * l_ft;

            switch( i_flag )
            {
                case LastWrite:
                    l_ft = &( l_fd.ftLastWriteTime );
                    break;

                case Created:
                    l_ft = &( l_fd.ftCreationTime );
                    break;

                case Accessed:
                    l_ft = &( l_fd.ftLastAccessTime );
                    break;

                default:
                    return false;
            }

            CopyTime( *l_ft, o_timestamp );

            l_ret = true;
        }

        return l_ret;
    }


    // ======== BaseFile::~BaseFile () =======================================
    BaseFile::~BaseFile ()
    {
        try
        {
            if ( true == m_closeondelete )
            {
                Close ();
            }

            if ( m_lasterror )
            {
                delete m_lasterror;
                m_lasterror = NULL;
            }
        }
        catch ( const FileErrorBasic& )
        {
            // Not much we can do if there's an error on closing the file.
            // Can't throw an exception, since allowing a destructor to emit
            // exceptions is evil.  No point in recording the error state in
            // the object itself, since it's in the course of being
            // destructed.  And writing a message to stderr or something would
            // be more than a little presumtuous.  So we just drop it on the
            // floor...
        }
    }

    // ======== BaseFile::BaseFile ( const FilePath&, int ) ==================
    BaseFile::BaseFile ( const FilePath & i_file_path, const FileAttr &i_attr ) 
    {
        m_filehandle.handle    = INVALID_HANDLE_VALUE;
        m_fullname      = i_file_path;
        m_mode          = i_attr; // Mode is verified In Open
        m_closeondelete = false;
        m_lasterror     = 0;
        m_locked        = 0;
        // Create a recursive mutex for thread synchronization on lead/aide twins...
        m_mutex = new MutexRefCount( Mutex::Recursive );
    }

    // ======== BaseFile::BaseFile () ========================================
    BaseFile::BaseFile ()
    {
        m_mode          = FileAttr::Read;
        m_filehandle.handle    = INVALID_HANDLE_VALUE;
        m_closeondelete = false;
        m_lasterror     = 0;
        m_locked        = 0;
        // Create a recursive mutex for thread synchronization on lead/aide twins...
        m_mutex = new MutexRefCount( Mutex::Recursive );
    }

    // ======== BaseFile::CheckAttributes ( FileAttr *i_attr ) ===============
    bool BaseFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Should never be called directly so always fail
        return false;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Win32 Implementation of RFile
    //
    /////////////////////////////////////////////////////////////////////

    // ======== RFile::RFile ( const FilePath&, int ) ==============
    RFile::RFile ( const FilePath & i_file_path, const FileAttr& i_attr ) : BaseFile ( i_file_path, i_attr )
    {
    }

    // ======== RFile::RFile () ========================================
    RFile::RFile () : BaseFile ()
    {
    }

    // ======== RFile::Read ( at::PtrView< Buffer* > &i_br ) ===========
    bool RFile::Read ( at::PtrView< Buffer * > i_br )
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        // Zero in Zero out
        if ( i_br.Get()->Region().m_allocated == 0 )
        {
            return true;  
        }

        DWORD bytesread;

        if ( !::ReadFile ( m_filehandle.handle, static_cast<LPVOID>(i_br.Get ()->Region().m_mem), 
                i_br.Get()->Region().m_allocated, static_cast<LPDWORD>(&bytesread), NULL ) )
        {
            SetError ( ::GetLastError() );

            return false;
        }

        i_br.Get()->SetAllocated ( bytesread );

        return true;
    }

    // ======== RFile::Read ( const Buffer::t_Region& i_br ) ===========
    bool RFile::Read ( Buffer::t_Region& i_br )
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        const DWORD l_bytestoread = i_br.m_max_available - i_br.m_allocated;
        if ( l_bytestoread > 0 )
        {
            DWORD l_bytesread;
            if ( ! ::ReadFile ( m_filehandle.handle,
                                static_cast<LPVOID>(i_br.m_mem), 
                                l_bytestoread,
                                static_cast<LPDWORD>(&l_bytesread),
                                NULL ) )
            {
                SetError ( ::GetLastError() );
                return false;
            }
            i_br.m_allocated += static_cast<SizeMem>(l_bytesread);
        }
        return true;
    }

    // ======== RFile::Open ( const FilePath&, const int )================
    bool RFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr  )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== RFile::Open () ===========================================
    bool RFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== RFile::CheckAttributes ( int i_attr ) ====================
    bool RFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we only have Read attributes 

        FileAttr mode = *i_attr;

        // Write not Allowed
        if ( mode & FileAttr::Write ) 
        {
            return false;
        }

        // ReadWrite not Allowed
        if ( mode & FileAttr::ReadWrite ) 
        {
            return false;  

        }

        // Can't Create a read only file
        if ( mode & FileAttr::Create )
        {
            return false;
        }

        if ( mode & FileAttr::New )
        {
            return false;
        }

        if ( mode & FileAttr::SyncWrite )
        {
            return false;
        }

        // Must Have Read Attribute
        if ( !( mode & FileAttr::Read ) )
        {
            mode.Add ( FileAttr::Read );        
        }

        *i_attr = mode;

        return true;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Win32 Implementation of WFile
    //
    /////////////////////////////////////////////////////////////////////

    // ======== WFile::WFile ( const FilePath&, int ) ====================
    WFile::WFile ( const FilePath & i_file_path, const FileAttr& i_attr  ) : BaseFile ( i_file_path, i_attr ) 
    {
    }

    // ======== WFile::WFile () ==========================================
    WFile::WFile () : BaseFile ()
    {
    }

    // ======== WFile::Write ( const Buffer* buffer ) ====================
    long WFile::Write ( at::PtrView< Buffer * > i_br )
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        // Zero bytes to write do nothing!
        if ( i_br.Get()->Region().m_allocated == 0 )
        {
            return 0;   
        }

        DWORD byteswritten;

        // If write fails throw an exception
        if ( !::WriteFile ( m_filehandle.handle, static_cast<LPVOID>(i_br.Get ()->Region().m_mem), 
            i_br.Get()->Region().m_allocated, &byteswritten, NULL ) )
        {
            SetError ( ::GetLastError () );

            return -1;
        }

        // If the number written does not match then the Disk must be full
        if ( byteswritten != i_br.Get()->Region().m_allocated )
        {
            throw FileErrorBasic ( FileError::s_disk_full );
        }

        return byteswritten;
    }

    // ======== WFile::Write ( const Buffer::t_Region& i_br ) ============
    bool WFile::Write ( const Buffer::t_Region& i_br )
    {
        return Write( Buffer::t_ConstRegion( i_br ) );
    }
    
    // ======== WFile::Write ( const Buffer::t_ConstRegion& i_br ) ============
    bool WFile::Write ( const Buffer::t_ConstRegion& i_br )
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );

        if ( i_br.m_allocated > 0 )
        {
            DWORD l_byteswritten;
            if ( !::WriteFile ( m_filehandle.handle,
                                (LPVOID)( i_br.m_mem ),
                                i_br.m_allocated,
                                &l_byteswritten,
                                NULL ) )
            {
                SetError ( ::GetLastError () );
                return false;
            }
            if ( l_byteswritten != i_br.m_allocated )
            {
                SetError( FileErrorBasic ( FileError::s_disk_full ) );
                return false;
            }
        }
        return true;
    }

    // ======== WFile::Open ( const FilePath&, const int )================
    bool WFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== WFile::Open () ===========================================
    bool WFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== WFile::CheckAttributes ( int i_attr ) ====================
    bool WFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we only have Write attributes 

        FileAttr mode = *i_attr;

        // Read Not Allowed
        if ( mode & FileAttr::Read )
        {
            return false;       
        }

        // ReadWrite Not Allowed
        if ( mode & FileAttr::ReadWrite )
        {
            return false;       
        }

        // Create and Preserve not allowed together
        if ( mode & FileAttr::Create && mode & FileAttr::Preserve )
        {
            // Create has presedance over Preserve so remove Preserve
            mode.Remove ( FileAttr::Preserve );
        }

        if ( mode & FileAttr::New && mode & FileAttr::Create )
        {
            return false;
        }

        if ( mode & FileAttr::New && mode & FileAttr::Preserve )
        {
            mode.Remove ( FileAttr::Preserve );
        }

        // Must Have Write
        if ( ! (mode & FileAttr::Write ) )
        {
            mode.Add ( FileAttr::Write );
        }

        *i_attr =  mode;

        return true;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Win32 Implementation of RWFile
    //
    /////////////////////////////////////////////////////////////////////

    // ======== RWFile::RWFile ( const FilePath& ,int ) ==================
    RWFile::RWFile ( const FilePath & i_file_path, const FileAttr& i_attr  ) 
        : BaseFile ( i_file_path, i_attr ), RFile ( i_file_path, i_attr ), WFile ( i_file_path, i_attr )
    {
    }

    // ======== RWFile::RWFile () ========================================
    RWFile::RWFile () : BaseFile (), WFile (), RFile ()
    {
    }

    // ======== RWFile::Open ( const FilePath& , const int ) ============
    bool RWFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== RWFile::Open () =========================================
    bool RWFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== RWFile::CheckAttributes ( int *i_attr ) =================
    bool RWFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we have Read / Write attributes 
        FileAttr mode = *i_attr;

        // Read Not Allowed
        if ( mode & FileAttr::Read )
        {
            return false;       
        }

        // Write Not Allowed
        if ( mode & FileAttr::Write )
        {
            return false;       
        }

        // Create and Preserve not allowed together 
        if ( mode & FileAttr::Create && mode & FileAttr::Preserve )
        {
            // Create takes presedance remove Preserve 
            mode.Remove ( FileAttr::Preserve );
        }

        if ( mode & FileAttr::New && mode & FileAttr::Create )
        {
            return false;
        }

        if ( mode & FileAttr::New && mode & FileAttr::Preserve )
        {
            mode.Remove ( FileAttr::Preserve );
        }

        // Must Have ReadWrite
        if ( ! ( mode & FileAttr::ReadWrite ) )
        {
            mode.Add ( FileAttr::ReadWrite );
        }

        *i_attr = mode;

        return true;
    }

class AsyncFileContext :
    public FileContext,
    private AideTwinMT_Basic<FileNotifyLeadInterface>
{
private:
    OVERLAPPED      ol;

    // mutex for callback interface to client
    Ptr<MutexRefCount *>                        m_twinMutex;

public:

    AsyncFileContext(   PtrDelegate< Buffer * > i_br,
                        AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead,
                        BaseFile &i_file
                     ) :
        m_twinMutex(new MutexRefCount(Mutex::Recursive))
    {
        m_buffer = i_br;
        m_basefile = &i_file;

        this->AideAssociate(i_lead,m_twinMutex);
        ::ZeroMemory(&ol,sizeof(ol));
        ol.Offset = (DWORD)(m_basefile->GetPosition() & 0xffffffff);
        ol.OffsetHigh = (DWORD)((m_basefile->GetPosition() >> 32) & 0xffffffff);
        ol.hEvent = ::CreateEvent(NULL,0,0,NULL);
    }

    virtual ~AsyncFileContext()
    {
        this->AideCancel();
        ::CloseHandle(ol.hEvent);
    }

    virtual OVERLAPPED *GetOverlappedPtr(void)
    {
        return &ol;
    }

    HANDLE EventHandle(void)
    {
        return ol.hEvent;
    }

    void NotifyDone(const FileError &fe)
    {
        this->CallLead().VoidCall ( &FileNotifyLeadInterface::IOCompleted, this, fe);
        this->CallLead().VoidCall ( &FileNotifyLeadInterface::Cancel );
    }

private:
    AsyncFileContext(void); // does not default construct
    AsyncFileContext(const AsyncFileContext &doesNotCopy);
    AsyncFileContext &operator= (const AsyncFileContext &doesNotAssign);
};

/**
*   CallbackTask  - this thread runs in the background. It waits for new contexts
*   and adds them to an event map. When the context completes the thread fires
*   off a completion notification.
*
*   Activities execute in the context of the CallbackTask thread, serially after
*   their associated event fires
*/

class CallbackTask : private at::Task
{
private:
    ConditionalMutex    m_condWork;         // wait for condition variable when no work pending

    // members to protect with the above mutex
    HANDLE                      m_hQueueEvent;  // aborts the waitformultipleobjects when a new event is queued
    bool                            m_done;     // flag to terminate the work loop
    bool                            m_working;  // flag that the worker thread is active
    std::vector<AsyncFileContext *> m_afc;      // pending io


public:

    CallbackTask () :
        m_condWork(Mutex::Recursive),
        m_hQueueEvent(::CreateEvent(NULL,false,false,NULL)),
        m_done(false),
        m_working(false),
        m_afc()
    {
    }

    // typically does not execute in the thread context of the callback thread
    virtual ~CallbackTask ()
    {
        Shutdown();
        {
            Lock<ConditionalMutex>  m_l(m_condWork); 
            if (INVALID_HANDLE_VALUE != m_hQueueEvent) {
                ::CloseHandle(m_hQueueEvent);
                m_hQueueEvent = INVALID_HANDLE_VALUE;
            }
        }
    }

	// ======== Start =======================================================
    /**
     *  Starts the thread
     *  typically does not execute in the thread context of the callback thread
     */
    virtual void Start () 
    { 
        Task::Start (); 
    }

	// ======== Shutdown =======================================================
    /**
     *  Cancels all activities and shuts down the thread,
     *  typically does not execute in the thread context of the callback thread
     *
     * @returns nothing
     */
    virtual void Shutdown() 
    {
        Lock<ConditionalMutex>  m_l(m_condWork); 
        m_done = true;
        releaseWorker();
        if (m_working) {
            m_condWork.Wait();
        }
    }

    virtual bool QueueAsyncFileContext(AsyncFileContext * ac) {
        Lock<ConditionalMutex>  m_l(m_condWork);
        m_afc.push_back(ac);
        releaseWorker();
        return true;
    }
private:
    // ======== Work =======================================================
    //  sleeps until there is work to do
    //  then retrieves and posts it
    virtual void Work ()
    {
        Lock<ConditionalMutex>  m_l(m_condWork);
        m_working = true;
        do {
            if (m_afc.empty()) {
                m_condWork.Wait();
            } else {
                doWin32EventWait();
            }
        } while (!m_done);
        m_working = false;
        m_condWork.PostAll();
    }

    void releaseWorker(void)
    {
        m_condWork.PostAll();
        if (INVALID_HANDLE_VALUE != m_hQueueEvent) {
            ::SetEvent(m_hQueueEvent);
        }
    }

    void doWin32EventWait(void) {
        HANDLE events[MAXIMUM_WAIT_OBJECTS];
        DWORD dwCount = m_afc.size() + 1;
        if (dwCount > MAXIMUM_WAIT_OBJECTS) {
            dwCount = MAXIMUM_WAIT_OBJECTS;
        }
        events[0] = m_hQueueEvent;          // if we wake on this one, a new io event was queued
        for (DWORD i=1; i < dwCount; ++i) {   // make a local copy of queued events so that we can unlock the state mutex
            events[i] = m_afc[i-1]->EventHandle();
        }
        DWORD dwResult = WAIT_FAILED;
        {
            Unlock<ConditionalMutex> m_ul(m_condWork);
            dwResult = ::WaitForMultipleObjectsEx(dwCount,events,false,30*1000,true);
        }
        dispatchWin32Event(events,dwCount,dwResult);
    }

    void dispatchWin32Event(HANDLE *events,DWORD dwCount, DWORD dwResult)
    {
        int eventNum = 0;
        FileErrorBasic fe(FileErrorBasic::s_success);
        if ((dwResult >= WAIT_ABANDONED_0) && (dwResult < (WAIT_ABANDONED_0 + dwCount))) {
            eventNum = dwResult - WAIT_ABANDONED_0;
            fe = FileErrorBasic::s_user_aborted;
        } else if ((dwResult >= WAIT_OBJECT_0) && (dwResult < (WAIT_OBJECT_0 + dwCount))) {
            eventNum = dwResult - WAIT_OBJECT_0;
        }
        if (eventNum) {
            // not our private event, therefore a valid async io handle
            HANDLE event = events[eventNum];
            AsyncFileContext *ac = removeAsyncFileContext(event);
            if (ac) {
                Unlock<ConditionalMutex> l_ul(m_condWork);
                ac->NotifyDone(fe);
                delete ac;
            }
        }
    }

    AsyncFileContext *removeAsyncFileContext(HANDLE event)
    {
        AsyncFileContext *retval = NULL;
        for (size_t i = 0; i < m_afc.size(); ++i) {
            if (event == m_afc[i]->EventHandle()) {
                retval = m_afc[i];
                m_afc.erase(m_afc.begin() + i);         // does not erase pointee
            }
        }
        return retval;
    }
};


    // ======== RFile::ReadAsync =================================
    bool RFile::ReadAsync ( at::PtrDelegate< Buffer * > i_br, 
        AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead, Int64 i_off, int i_from ) 
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );
        AT_Assert ( i_br.Get () != NULL );
        AT_Assert ( i_br.Get()->Region().m_allocated != 0 );
        AT_Assert ( m_mode & FileAttr::Async );

        AsyncFileContext *ac = new AsyncFileContext(i_br, i_lead, *this);

        // If requested perform a seek (-1 means no seek)
        if ( i_off > -1 ) 
        {
            try
            {
                Seek ( i_off, i_from );
            }
            catch ( FileErrorBasic & fe )
            {
                SetError ( fe );

                return false;
            }
        }

        bool result = ::ReadFile(m_filehandle.handle,ac->GetRawBytes(),ac->GetBufferSize(),NULL,ac->GetOverlappedPtr());
        if (!result) {
            DWORD error = ::GetLastError();
            if (ERROR_IO_PENDING != error) {
                SetError(error);
            } else {
                // async read pending.  Queue it on the background thread
                CallbackTask *cb = FileInitializer::GetCallBackThread();
                if (cb) {
                    result = cb->QueueAsyncFileContext(ac);
                    ac = NULL;
                }
            }
            if (!result) {
                // error in reading or queueing
                // @@ TODO -- more specific error codes to the callback, this is too vague
                i_lead->IOCompleted( ac, FileErrorBasic::s_access_denied);
                i_lead->Cancel();
                delete ac;
            }
        } else {
            // read completed synchronously.  Tell the lead
            i_lead->IOCompleted( ac, FileErrorBasic::s_success);
            i_lead->Cancel();
            delete ac;
        }
        return result;
    }

    // ======== WFile::WriteAsync =================================
    bool WFile::WriteAsync ( const at::PtrDelegate< Buffer * > i_br, 
        AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead ) 
    {
        AT_Assert ( m_filehandle.handle != INVALID_HANDLE_VALUE );
        AT_Assert ( i_br.Get () != NULL );
        AT_Assert ( i_br.Get()->Region().m_allocated != 0 );
        AT_Assert ( m_mode & FileAttr::Async );

        AsyncFileContext *ac = new AsyncFileContext(i_br, i_lead, *this);

        bool result = ::WriteFile(m_filehandle.handle,ac->GetRawBytes(),ac->GetBufferSize(),NULL,ac->GetOverlappedPtr());
        if (!result) {
            DWORD error = ::GetLastError();
            if (ERROR_IO_PENDING != error) {
                SetError(error);
            } else {
                // async write pending.  Queue it on the background thread
                CallbackTask *cb = FileInitializer::GetCallBackThread();
                if (cb) {
                    result = cb->QueueAsyncFileContext(ac);
                    ac = NULL;
                }
            }
            if (!result) {
                // error in writing or queueing
                // @@ TODO -- more specific error codes to the callback, this is too vague
                i_lead->IOCompleted( ac, FileErrorBasic::s_access_denied);
                i_lead->Cancel();
                delete ac;
            }
        } else {
            // write completed synchronously.  Tell the lead
            i_lead->IOCompleted( ac, FileErrorBasic::s_success);
            i_lead->Cancel();
            delete ac;
        }
        return result;
    }

    FileInitializer::FileInitializer ( int & argc, const char ** & argv )
    {
        // Start the callback thread
        m_call_back_thread = new CallbackTask ();
        m_call_back_thread->Start ();
    }

    FileInitializer::~FileInitializer ()
    {
        // Very strange conditions can occur on termination.
        // In this case we go ahead and leak - the process is 
        // exiting anyway so this is really not a leak.
		// at::CallbackTask *l_cb = m_call_back_thread;
		// l_cb->Shutdown();
		// m_call_back_thread = NULL;
        // delete l_cb;
    }

    // ======== CurrentDirectory ==========================================
    /**
     * CurrentDirectory returns a filepath of the current directory
     *
     * @return The current directory.
     */
    
    FilePath CurrentDirectory()
    {
    
        char l_buf[ 1024 ];
        char * l_bufp = _getcwd( l_buf, sizeof( l_buf ) );
    
        AT_Assert( l_bufp );
        
        return FilePath( std::string( l_buf ) );
    }
} // namespace at

#include <io.h>
#include <fcntl.h>

namespace at
{

    // ======== SetBinaryMode =========================================

    bool SetBinaryMode( int i_filedes, bool i_set_binary )
    {
        int l_result = _setmode( i_filedes, i_set_binary ? _O_BINARY : _O_TEXT );

        return -1 != l_result;
    }
    
} // namespace at
