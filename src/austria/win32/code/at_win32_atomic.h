//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_win32_atomic.h
 *
 */

#include "at_exports.h"

#ifndef x_at_win32_atomic_h_x
#define x_at_win32_atomic_h_x 1

// Austria namespace
namespace at
{

// ======== AtomicCountType ===========================================
/**
 * AtomicCountType is the fundamental type that is operated on for
 * atomic operations.
 */

typedef long AtomicCountType;



// ======== AtomicIncrement ===========================================
/**
 *	AtomicIncrement will atomically increment the value being pointed
 *  to in the parameter.
 *
 * @param io_val pointer to the value being incremented
 * @return The value contained in the pointer before being incremented.
 */

AUSTRIA_EXPORT AtomicCountType AtomicIncrement(
    volatile AtomicCountType    * io_val
) throw ();

// ======== AtomicDecrement ===========================================
/**
 *	AtomicDecrement will atomically decrement the value being pointed
 *  to in the parameter.
 *
 * @param io_val pointer to the value being decremented
 * @return The value contained in the pointer before being incremented.
 */

AUSTRIA_EXPORT AtomicCountType AtomicDecrement(
    volatile AtomicCountType    * io_val
) throw ();

// ======== AtomicBump ===========================================
/**
 *	AtomicBump will atomically add the value being pointed
 *  to in the parameter with the 
 *
 * @param io_val pointer to the value being decremented
 * @param i_add_val valie to add.
 * @return The value contained in the pointer before being added
 */

AUSTRIA_EXPORT AtomicCountType AtomicBump(
    volatile AtomicCountType    * io_val,
    AtomicCountType                i_add_val
) throw ();



// ======== AtomicExchangeablePointer =================================
/**
 * AtomicExchangeablePointer is the pointer type that is the base for
 * the AtomicExchange_Ptr class.
 *
 */

typedef void * AtomicExchangeablePointer;

// ======== AtomicExchangeableValue =================================
/**
 * AtomicExchangeableValue is the pointer type that is the base for
 * the AtomicExchange_Ptr class.
 *
 */

typedef long AtomicExchangeableValue;


// ======== AtomicExchange =======================================
/**
 *	AtomicExchangeValue will atomically exchange the value passed in
 *  with the one pointed to in memory.
 *
 * @param i_val the value to set the memory location to
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return the previous value pointed to by io_mem_loc
 */

AUSTRIA_EXPORT AtomicExchangeablePointer AtomicExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val
);

AUSTRIA_EXPORT AtomicExchangeableValue AtomicExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val
);


// ======== AtomicCompareExchangeValue ================================
/**
 * AtomicCompareExchangeValue does a compare exchange.  i.e.
 *
 * @param i_val the value to set the memory location to
 * @param i_cmp The value to compare with
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return The original value in the memory location
 */

AUSTRIA_EXPORT AtomicExchangeableValue AtomicCompareExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val,
    AtomicExchangeableValue                 i_cmp
);

// ======== AtomicCompareExchangePointer ==============================
/**
 * AtomicCompareExchangePointer performs a compare/exchange on a
 * pointer value.
 *
 * @param i_val The value to set the memory location to
 * @param i_cmp The value to compare with
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return The original value in the memory location
 */

AUSTRIA_EXPORT AtomicExchangeablePointer AtomicCompareExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val,
    AtomicExchangeablePointer                 i_cmp
);

}; // namespace

#endif // x_at_win32_atomic_h_x


