//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_win32_thread.cpp
 *
 *
 */


//
// This is the win32 version of the threading interface.
//

//
// This employs the PIMPL idiom to hide the Win32 specific headers from
// the implementations.  The local definitions of PrivateMutexContext
// and PrivateConditionalContext are the actual implementations of the
// mutex and conditional variable.
//

#pragma warning ( disable : 4355 )
#pragma comment(lib, "ole32.lib")

#include "at_assert.h"
#include "at_thread.h"
#include "at_start_up.h"
#include "at_factory.h"
#include "at_win32_basic.h"

// win2k or later api support
#define _WIN32_WINNT 0x0500

#include <windows.h>
#include <Winbase.h>
#include <process.h>    /* win32 threading header */
#include <crtdbg.h>
#include <signal.h>
#include <set>

using namespace at;
//
// Tracing support
//
static const bool g_tracing = false;

// at_trace.h must be included after defining g_tracing
#include "at_trace.h"

// ======== GetSystemInfoR =============================================
/**
 * This is like the windows system call except it returns the 
 * GetSystemInfo data.
 *
 * @return a const reference to a statically allocated SYSTEM_INFO
 *  object.
 */

const SYSTEM_INFO & GetSystemInfoR()
{
    struct SysGet
    {
        SYSTEM_INFO     m_system_info;

        SysGet()
        {
            GetSystemInfo( & m_system_info );
        }
    };

    // static initialization means that this is initialized only
    // the first time this is called.
    static SysGet   s_system_info;

    // Return the value
    return s_system_info.m_system_info;
}


// ======== GetCpuCount ===============================================
/**
 * Uniprocessor systems should (in theory) not do too much spinning
 * on spin locks. GetCpuCount will return the count of CPUs.
 */

unsigned GetCpuCount()
{
    // in theory. this is initialized only once.
    static unsigned s_cpu_count( GetSystemInfoR().dwNumberOfProcessors );

    return s_cpu_count;
}


// ======== GetDefaultSpinCount =======================================
/**
 * GetDefaultSpinCount computes the number of spins based on the number of CPUs.
 * Single CPU machines don't need to spin, while SMP machines benefit
 * from spinning at least enough cycles that would allow a cache line
 * to be refectched.  This is of the order of 1000 cycles on most modern
 * systems.
 *
 * @return a default spin count
 */

unsigned GetDefaultSpinCount()
{
    static unsigned s_default_spin_count(
        GetSystemInfoR().dwNumberOfProcessors == 1
            ? 0
            : 2000 + ( GetCpuCount() << 11 )
    );
    return s_default_spin_count;
}

// ======== PrivateMutexContext =======================================
/**
 * PrivateMutexContext contains the private data for a mutex.
 *
 */

class PrivateMutexContext
{
private:
	CRITICAL_SECTION	m_cs;				// lightweight basic spin lock held while updating internal state

	const bool			m_recursive;		// if this was created as a recursive lock
    unsigned			m_recursion_count;	// number of times same thread has locked recursively

	Task::TaskID		m_task_id;			// ID of task which currently holds the lock

	volatile bool m_deleted;	// @@ TODO remove if not debugging

	void doEntryBookkeeping(void)
	{
        ++m_recursion_count;
        if (!m_recursive) {
            AT_Assert(m_recursion_count == 1);
        }
        m_task_id = Task::GetSelfId();
    }

public:
    PrivateMutexContext( Mutex::MutexType i_type ) :
    m_recursive( i_type != Mutex::NonRecursive ),
    m_recursion_count( 0 ),
    m_task_id(::GetCurrentProcessId()) // guaranteed to be invalid as a thread id, since they are in the same namespace
    {
        m_deleted = false;
        ::InitializeCriticalSection(&m_cs);
    }

    virtual ~PrivateMutexContext()
    {
        AT_Assert(!m_deleted);
        if (m_recursion_count) {
            _CrtDbgBreak();
        }
        AT_Assert(m_recursion_count == 0);
        ::DeleteCriticalSection(&m_cs);
        m_deleted = true;
    }

    // ======== Lock ==================================================
    /**
     * lock the mutex.
     *
     * @return nothing
     */

    virtual void Lock()
    {
        AT_Assert(!m_deleted);
        ::EnterCriticalSection(&m_cs);
        doEntryBookkeeping();
        return;
    }


    // ======== TryLock ===============================================
    /**
     * TryLock will attempt to take the lock without waiting
     *
     */

    bool TryLock()
    {
        AT_Assert(!m_deleted);
        bool retval = (TRUE == ::TryEnterCriticalSection(&m_cs));
        if (retval) {
            doEntryBookkeeping();
        }
        return retval;
    }


    // ======== Unlock ================================================
    /**
     * Unlock this lock.
     */

    void Unlock()
    {
        AT_Assert(!m_deleted);
        --m_recursion_count;    // exit bookkeeping
        ::LeaveCriticalSection(&m_cs);
    }

private:
    PrivateMutexContext(const PrivateMutexContext &cantCopyMe);
    PrivateMutexContext &operator=(const PrivateMutexContext &cantAssignMe);
};


// ======== PrivateConditionalContext =================================
/*
 * PrivateConditionalContext contains information pertaining to the
 * Conditional implementation.
 */

class PrivateConditionalContext
{
private:
    PrivateMutexContext *m_callerMutex;  // always hold caller mutex before acquiring working mutex
    at::Mutex   m_workingMutex;
    unsigned int  m_waitSerialNum;

    struct thdControl {
        unsigned int m_waitingForSerialNum;
        HANDLE   m_waitEvent;
        volatile bool m_posted;

        thdControl() :
            m_waitEvent(::CreateEvent(NULL,false,false,NULL)),
            m_posted(false)
        {
            AT_Assert(m_waitEvent != INVALID_HANDLE_VALUE);
        }

        ~thdControl()
        {
            ::CloseHandle(m_waitEvent);
        }
        bool operator< (const thdControl *rhs)
        {
            return this->m_waitingForSerialNum < rhs->m_waitingForSerialNum;
        }
    };

    typedef std::set<thdControl*> thdWaitSet;   // intended to be a reference to a local on the thread's stack, so be careful with lifetime
    thdWaitSet      m_waitingSet;

public:
 // ======== PrivateConditionalContext =============================
    /**
     * Constructor
     *
     */

 PrivateConditionalContext(PrivateMutexContext *i_mutex) :
        m_callerMutex(i_mutex),
        m_workingMutex(),
        m_waitSerialNum(0),
        m_waitingSet()
    {
    }

    virtual ~PrivateConditionalContext()
    {
        AT_Assert(m_waitingSet.empty());
    }

private:
    void deleteWaiting(thdControl *ptc)
    {
        for (thdWaitSet::iterator hsi = m_waitingSet.begin(); hsi != m_waitingSet.end(); ++hsi) {
            if (ptc == *hsi) {
                m_waitingSet.erase(hsi);
                break;
            }
        }
    }

    void releaseThreads(void)  // release all who have been posted
    {
        thdWaitSet m_wakingSet;

        for (thdWaitSet::iterator hsi = m_waitingSet.begin(); hsi != m_waitingSet.end(); ++hsi) {
            thdControl& tc = **hsi;
            if (tc.m_posted) {
                m_wakingSet.insert(&tc);
            }
        }
        while (!m_wakingSet.empty()) {
            thdWaitSet::iterator hsi = m_wakingSet.begin();
            {
                thdControl& tc = **hsi;
                ::SetEvent(tc.m_waitEvent);
                deleteWaiting(&tc);
            }
            m_wakingSet.erase(hsi);
        }
    }

public:
    // ======== Post ==================================================
    /**
     * Post will notify one thread (if one is waiting)
     *
     */

    void Post()
    {
        Lock <Mutex> l_lock(m_workingMutex);
        if (!m_waitingSet.empty()) {
            thdWaitSet::iterator hsi = m_waitingSet.begin(); // @@ TODO may use algorithm which knows thread priorities
            if (hsi != m_waitingSet.end()) {
                thdControl& tc = **hsi;
                tc.m_posted = true;
                releaseThreads();
            }
        }
    }

    
    // ======== PostAll ===============================================
    /**
     * Post a notification to all waiting threads.
     *
     */

    void PostAll()
    {
        Lock <Mutex> l_lock(m_workingMutex);
        if (!m_waitingSet.empty()) {
            for (thdWaitSet::iterator hsi = m_waitingSet.begin(); hsi != m_waitingSet.end(); ++hsi) {
                thdControl& tc = **hsi;
                tc.m_posted = true;
            }
            releaseThreads();
        }
    }

    // ======== Wait ==================================================
    /**
     * Wait will wait until another thread "notifies" this thread
     * (the thread calling Wait()).
     *
     * @return nothing
     */

    void Wait()
    {
        Wait( INFINITE );
    }

    // ======== Wait ==================================================
    /**
     * Wait will wait until another thread "notifies"
     * (the thread calling Wait()).
     *
     * @return true if the condition was signalled, false if it timed out
     */

    bool Wait( const TimeInterval & i_time )
    {
        /*
         *  Don't pass a negative value to Wait( DWORD ), as it gets
         *  interpreted as an unsigned value.
         */
        if ( i_time < TimeInterval( 0 ) )
        {
            return Wait( DWORD( 0 ) );
        }
        else
        {
            return Wait( static_cast<DWORD>( i_time.MilliSecs() ) );
        }
    }

    // ======== Wait ==================================================
    /**
     * Wait will wait until another thread "notifies"
     * (the thread calling Wait()).
     *
     * @return true if the condition was signalled, false if it timed out
     */

    bool Wait( const TimeStamp & i_time )
    {
        return Wait( i_time - ThreadSystemTime() );
    }

    // ======== Wait ==================================================
    /**
     * Wait will wait until another thread "notifies"
     * (the thread calling Wait()). This may also time-out, however,
     * the lock will be re-attained when Wait returns.
     *
     * @return true if the condition was signalled, false if it timed out
     */

    bool Wait( DWORD i_val )
    {
        bool retval = false;

        // be careful with this, it is a local, but the heap points to it. manage the lifetime carefully
        thdControl l_myThdInfo;

        {
            Lock<Mutex> l_lock(m_workingMutex);
            l_myThdInfo.m_waitingForSerialNum =  ++m_waitSerialNum;        // atomic, since protected by working mutex
            m_waitingSet.insert(&l_myThdInfo);
            m_callerMutex->Unlock();
        }
        DWORD eventReturn = ::WaitForSingleObject(l_myThdInfo.m_waitEvent,i_val);
        m_callerMutex->Lock();
        {
            Lock<Mutex> l_lock(m_workingMutex);
            switch (eventReturn) {  // depending on why we exited, there is bookkeeping to do
                case WAIT_OBJECT_0:  // expected result
                    retval = true;
                    break;
                case WAIT_TIMEOUT:      // expected result
                case WAIT_FAILED:       // unexpected
                case WAIT_ABANDONED:    // unexpected
                default:				// unexpected
                if (l_myThdInfo.m_posted) {	// someone did signal us, but we didn't receive it through the Event object
                    retval = true;			// they already removed us from the waiting queue
                } else {
                    deleteWaiting(&l_myThdInfo);
                }
                break;
            }
        }
        return retval;
    }

private:
	PrivateConditionalContext(const PrivateConditionalContext &cantCopyMe);
	PrivateConditionalContext& operator=(const PrivateConditionalContext &cantAssignMe);
};

//
// make sure the sizes of opaque objects are the appropriate size.
// corrent the sizes.
AT_StaticAssert(
    sizeof( at::Mutex::MutexContext ) >= sizeof( PrivateMutexContext ),
    Mutex__MutexContext_size_must_be_greater_or_equal_to_size_of_PrivateMutexContext
);

AT_StaticAssert(
    sizeof( at::Conditional::ConditionalContext ) >= sizeof( PrivateConditionalContext ),
    Conditional__ConditionalContext_size_must_be_greater_or_equal_to_size_of_pthread_cond_t
);

namespace at
{

// ======== TaskContext ===============================================
/**
 * TaskContext is the data contained within this context.
 *
 */

struct TaskContext
{
    /**
     * start_routine is a static method used to hook back from
     * the implementation dependant thread start-up routine
     * to the Work method.
     */
	static unsigned __stdcall win32_start_routine( void * i_task_v );

    TaskContext()
      : m_started( false ),
        m_completed( false ),
        m_is_joined( false ),
		m_is_deleting( false )
    {
    }
    
    /**
     * m_thread_id contains the ID of this thread
     */
    
    Task::TaskID            m_thread_id;

    /**
     * m_thread_handle contains the os handle for managing the
     * thread.
     */

    HANDLE                  m_thread_handle;

    /**
     * m_started indicates wether the thread has been "started"
     */

    volatile bool           m_started;

    /**
     * m_completed indicates wether the thread has finished.
     */
    volatile bool           m_completed;

    /**
     * m_is_joined indicates wether the thread is joined
     */
    volatile bool           m_is_joined;

 /**
  * m_is_deleting indicates that thethread is being deleted prematurely.
  */
 volatile bool   m_is_deleting;

    /**
     * This is to manage this thread.
     */
    ConditionalMutex        m_wait_cond_mutex;
    
};

// ======== Start =================================================
void Task::Start()
{
    // Need any lock here.  Must not start more than once.
    Lock<ConditionalMutex>  l_lock( m_task_context->m_wait_cond_mutex );

    if ( ! m_task_context->m_started )
    {
        m_task_context->m_started = true;

        ::ResumeThread( m_task_context->m_thread_handle );
    }
}

// ======== Wait ==================================================
void Task::Wait()
{
    // Wait here to be started
    Lock<ConditionalMutex>  l_lock( m_task_context->m_wait_cond_mutex );

    if ( ! m_task_context->m_started )
    {
        m_task_context->m_started = true;
        ::ResumeThread( m_task_context->m_thread_handle );
    }

    while ( ! m_task_context->m_completed )
    {
        l_lock.Wait();
    }
}


// ======== Wait ==================================================
bool Task::Wait( const TimeInterval & i_time )
{
    // Wait here to be started
    Lock<ConditionalMutex>  l_lock( m_task_context->m_wait_cond_mutex );

    if ( ! m_task_context->m_started )
    {
        m_task_context->m_started = true;
        ::ResumeThread( m_task_context->m_thread_handle );
    }

    TimeStamp l_tst_start = ThreadSystemTime (); 
    TimeStamp l_tst = l_tst_start;
    while ( ! m_task_context->m_completed && ( l_tst-l_tst_start < i_time  ) )
    {
        l_lock.Wait( i_time - (l_tst-l_tst_start) );
        l_tst = ThreadSystemTime (); 
    }

    return m_task_context->m_completed;
}


// ======== Sleep =================================================
bool Task::Sleep( const TimeInterval & i_time )
{
    DWORD l_ms;
    if ( i_time < TimeInterval( 0 ) )
    {
        l_ms = 0;
    }
    else
    {
        l_ms = DWORD( i_time.MilliSecs() );
    }
    bool retval = ::WaitForSingleObjectEx(::GetCurrentProcess(),l_ms, true ) == WAIT_IO_COMPLETION ? true : false; 
    return retval;
}

// ======== Sleep =================================================
bool Task::Sleep( const TimeStamp & i_time )
{
    return Sleep( i_time - ThreadSystemTime() );
}

// ======== GetThisId =============================================

Task::TaskID Task::GetThisId()
{
    return m_task_context->m_thread_id;
}


#pragma warning(push)
#pragma warning(disable : 4311)
// ======== GetSelfId =============================================

Task::TaskID Task::GetSelfId()
{
 return (Task::TaskID)(::GetCurrentThreadId());
}
#pragma warning(pop)


// ======== start_routine =============================================

unsigned __stdcall TaskContext::win32_start_routine( void * i_task_v )
{
    // Need to do per thread COM things
    ::CoInitialize( NULL );

    Task * l_this_task = static_cast< Task * >( i_task_v );

    TaskContext   & l_task_context = * l_this_task->m_task_context;

    // do the work ...
    if ( l_task_context.m_is_deleting == false)
    {
        l_this_task->Work();
    }

    {
        // Wake all the waiters.
        Lock<ConditionalMutex>  l_lock( l_task_context.m_wait_cond_mutex );

        l_this_task->m_task_context->m_completed = true;
        l_lock.PostAll();
    }

    ::CoUninitialize();
    
    return 0;
}


// ======== Task ======================================================

Task::Task()
  : m_task_context( new TaskContext() )
{
    // Windows will start the thread an suspend it immediatly
    // which means we don't need any start synchronization.
    //
    m_task_context->m_thread_handle = (HANDLE) _beginthreadex(
        NULL,                               // security
        0,                                  // stack_size
        & TaskContext::win32_start_routine,
        static_cast<void *>( this ),
        CREATE_SUSPENDED,
        & m_task_context->m_thread_id
    );

    if ( INVALID_HANDLE_VALUE == m_task_context->m_thread_handle )
    {
        AT_Abort();
    }
}

// ======== ~Task =================================================
Task::~Task()
{
    {
        Lock<ConditionalMutex>  l_lock( m_task_context->m_wait_cond_mutex );

        m_task_context->m_is_deleting = true;
        // must not try to delete thread that is still executing
        while ( ! m_task_context->m_completed )
        {
            l_lock.Wait();
        }
    }
    delete m_task_context;
}


#define ReferenceMutexContext(i_name)                                   \
    PrivateMutexContext * i_name = reinterpret_cast<PrivateMutexContext *>( & m_mutex_context )



// ======== Mutex =================================================
Mutex::Mutex( MutexType i_type )
{
    ReferenceMutexContext( l_mutex );

    // perform the contructor using placement new
    l_mutex = new ( static_cast<void *>( l_mutex ) ) PrivateMutexContext( i_type );
}


// ======== Mutex =================================================

Mutex::~Mutex()
{
    ReferenceMutexContext( l_mutex );

    // call the destructor
    l_mutex->~PrivateMutexContext();

}

// ======== Lock ==================================================

void Mutex::Lock()
{
    ReferenceMutexContext( l_mutex );

    l_mutex->Lock();
}


// ======== TryLock ===============================================

bool Mutex::TryLock()
{
    ReferenceMutexContext( l_mutex );

    return l_mutex->TryLock();
}


// ======== Unlock ================================================

void Mutex::Unlock()
{
    ReferenceMutexContext( l_mutex );

    l_mutex->Unlock();
}


// ======== Conditional ===============================================

#define PointerToConditionalContext(i_name)                             \
    PrivateConditionalContext * i_name =                                \
        reinterpret_cast<PrivateConditionalContext *>( & m_conditional_context )


// ======== Conditional ===========================================

Conditional::Conditional( Mutex & i_mutex )
{
    PrivateMutexContext * l_mutex = reinterpret_cast<PrivateMutexContext *>( & i_mutex.m_mutex_context );
    PointerToConditionalContext( l_conditional );

    // placement new - construct the PrivateConditionalContext in place
    l_conditional = new ( static_cast<void *>( l_conditional ) ) PrivateConditionalContext(l_mutex);
}

// ======== ~Conditional ==========================================

Conditional::~Conditional()
{
    PointerToConditionalContext( l_conditional );

    l_conditional->~PrivateConditionalContext();
}


// ======== Post ==================================================
void Conditional::Post()
{
    PointerToConditionalContext( l_conditional );

    l_conditional->Post();
}


// ======== PostAll ===============================================
void Conditional::PostAll()
{
    PointerToConditionalContext( l_conditional );
    
    l_conditional->PostAll();
}

// ======== Wait ==================================================
void Conditional::Wait()
{
    PointerToConditionalContext( l_conditional );

    l_conditional->Wait();
}

// ======== Wait ==================================================
bool Conditional::Wait( const TimeInterval & i_time )
{
    PointerToConditionalContext( l_conditional );

    return l_conditional->Wait( i_time );
}

// ======== Wait ==================================================
bool Conditional::Wait( const TimeStamp & i_time )
{
    PointerToConditionalContext( l_conditional );

    return l_conditional->Wait( i_time );
}


// ======== ConditionalMutex ======================================
ConditionalMutex::ConditionalMutex( MutexType i_type )
  : Mutex( i_type ),
    Conditional( * static_cast< Mutex * >( this ) )
{
}

ConditionalMutexRefCount::ConditionalMutexRefCount( MutexType i_type)
  : MutexRefCount( i_type ),
    Conditional( * static_cast< Mutex * >( this ) )
{
}

class CRTInitializer : public Initializer
{
public:

    CRTInitializer ( int & argc, const char ** & argv );

    virtual ~CRTInitializer ();
};

void sigHandler(int signal)
{
    // @@ TODO -- should not ship code this way, not enough diagnostics
    ::OutputDebugString("Signal caught\n");
    ::ExitProcess(UINT(-2));
}


// Register the CRTinitializer ' !' so it's called first
AT_MakeFactory2P( "!CRTInitializer", CRTInitializer, Initializer, DKy, int & , const char ** & );

CRTInitializer::CRTInitializer ( int & argc, const char ** & argv )
{
    _CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_FILE | _CRTDBG_MODE_DEBUG);
    _CrtSetReportMode( _CRT_WARN,   _CRTDBG_MODE_FILE | _CRTDBG_MODE_DEBUG);
    _CrtSetReportMode( _CRT_ERROR,  _CRTDBG_MODE_FILE | _CRTDBG_MODE_DEBUG);

    _CrtSetReportFile( _CRT_ASSERT, _CRTDBG_FILE_STDERR );
    _CrtSetReportFile( _CRT_WARN,   _CRTDBG_FILE_STDERR );
    _CrtSetReportFile( _CRT_ERROR,  _CRTDBG_FILE_STDERR );

    signal(SIGINT,sigHandler);
    signal(SIGILL,sigHandler);
    signal(SIGFPE,sigHandler);
    signal(SIGTERM,sigHandler);
    signal(SIGBREAK,SIG_IGN);
    signal(SIGABRT,sigHandler);
}

CRTInitializer::~CRTInitializer ()
{
}


TimeStamp ThreadSystemTime()
{
    FILETIME l_system_time;
    GetSystemTimeAsFileTime( &l_system_time );

    ULARGE_INTEGER l_uli;
    l_uli.LowPart = l_system_time.dwLowDateTime;
    l_uli.HighPart = l_system_time.dwHighDateTime;
    return TimeStamp( l_uli.QuadPart );
}

#ifndef INVALID_HANDLE_VALUE
#define INVALID_HANDLE_VALUE		((HANDLE)(-1))
#endif

// ======== SystemCommandContext ======================================
/**
 * This contains the system specific system command context.
 *
 */

struct SystemCommandContext
{
    public:

    SystemCommandContext(
        const char                  * i_command_name,
        const char                  * const i_command_argv[],
        const SystemCommandAttr     & i_attr = SystemCommandAttr()
    )
      : m_pid(),
        m_errno(),
        m_state( SystemCommand::Running ),
        m_wait_status()
    {
        // build the command line
        int l_index = 1;
        std::string l_cmd_line = "\"";
        l_cmd_line = l_cmd_line + i_command_argv[0] + "\"";

        while ( i_command_argv[l_index] )
        {
            l_cmd_line = l_cmd_line + " \"" + i_command_argv[l_index++] + "\"";
        }

        Win32Struct<STARTUPINFO,DWORD,&STARTUPINFO::cb> l_startupInfo;
        PlainStruct<PROCESS_INFORMATION>                l_procInfo;

        // No Hourglass cursor and we want to hide the window -- TODO Make this a param
        //l_startupInfo.dwFlags = STARTF_USESHOWWINDOW | STARTF_FORCEOFFFEEDBACK;
        //l_startupInfo.wShowWindow = SW_HIDE; 

        // Create The Process
        if ( 
            CreateProcess ( 
                i_command_name, 
                (char*)l_cmd_line.c_str (),
                NULL,
                NULL,
                TRUE,
                DETACHED_PROCESS | HIGH_PRIORITY_CLASS, // On it's own and high priority
                NULL,
                NULL,
                l_startupInfo,
                l_procInfo
            ) 
        )
        {
            // Store the Handle
            m_pid = l_procInfo->hProcess;
            CloseHandle(l_procInfo->hThread);
        }
        else
        {
            // if failed 
            m_state = SystemCommand::Failed;
            m_errno = GetLastError ();
            m_pid = INVALID_HANDLE_VALUE;
        }
    }
    
    ~SystemCommandContext()
    {
        if ( INVALID_HANDLE_VALUE != m_pid )
        {
            CloseHandle ( m_pid );
        }
        // TODO BAD - in theory we need to wait here but we
        // can't because it can cause a deadlock
    }

    // ======== InitStatus ============================================

    bool InitStatus( std::string * o_error )
    {
        if ( INVALID_HANDLE_VALUE == m_pid )
        {
            if ( o_error )
            {
                * o_error = strerror( m_errno );
            }
            return false;
        }
        return true;
    }


    // ======== ExitStatus ============================================

    SystemCommand::ProcessState ExitStatus(
        int             & o_info,
        bool              i_wait
    ) {
        
        switch ( m_state )
        {
            case SystemCommand::Failed :
            {
                return SystemCommand::Failed;
            }

            case SystemCommand::Stopped :
            case SystemCommand::Running :
            {
                DWORD l_retval = WaitForSingleObject( m_pid, i_wait ? INFINITE : 0 );

                switch ( l_retval )
                {
                    case WAIT_OBJECT_0 :
                    {
                        break;
                    }

                    case WAIT_TIMEOUT :
                    {
                        return SystemCommand::Running;
                    }
                }
                
                if ( GetExitCodeProcess( m_pid, &m_wait_status ) )
                {
                    o_info = int( m_wait_status );
                    return m_state = SystemCommand::Exited;
                }
                else
                {
                    m_errno = GetLastError();
                    return SystemCommand::Failed;
                }
            }

            case SystemCommand::Interrupted :
            case SystemCommand::Exited :
            {
                o_info = int( m_wait_status );
                return m_state;
            }
        }

    }

    typedef HANDLE pid_t;
    pid_t                                   m_pid;
    int                                     m_errno;
    SystemCommand::ProcessState             m_state;
    DWORD                                   m_wait_status;

};

SystemCommand::SystemCommand(
    const char                  * i_command_name,
    const char                  * const i_command_argv[],
    const SystemCommandAttr     & i_attr
)
  : m_command_context(
        new SystemCommandContext( i_command_name, i_command_argv, i_attr )
    )
{}

SystemCommand::~SystemCommand()
{
    delete m_command_context;
}

// ======== InitStatus ============================================

bool SystemCommand::InitStatus( std::string * o_error )
{
    return m_command_context->InitStatus( o_error );
}


// ======== ExitStatus ============================================

SystemCommand::ProcessState SystemCommand::ExitStatus(
    int             & o_info,
    bool              i_wait
) {
    return m_command_context->ExitStatus( o_info, i_wait );
}
}; // namespace

