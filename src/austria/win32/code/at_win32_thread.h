//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_win32_thread.h
 *
 */

#ifndef x_at_win32_thread_h_x
#define x_at_win32_thread_h_x 1

// Austria namespace
namespace at
{
    
//
// These types are system specific.
//

typedef unsigned int SYS_TaskID;


/**
 * SystemMutexContextType basically contains a pthread_mutex_t
 * This should be made large enough for holding at least a
 * pthread_mutex_t.  Don't include pthread.h here.
 */

typedef void * SystemMutexContextType[
    sizeof( void * ) == 4
        ? 6 * sizeof( void * )
        : 5 * sizeof( void * )
];

/**
 * SystemConditionalContextType basically contains a pthread_condattr_t
 * and a pointer to a pthread_mutex_t.
 * This should be made large enough for holding at least a
 * pthread_condattr_t and a pointer.  Don't include pthread.h here.
 */

typedef void * SystemConditionalContextType[
    sizeof( void * ) == 4
        ? 13 * sizeof( void * )
        : 7 * sizeof( void * )
];


}; // namespace

#endif // x_at_win32_thread_h_x



