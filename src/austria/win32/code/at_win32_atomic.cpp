//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_win32_atomic.cpp
 *
 */
#pragma warning ( disable : 4311 4312 )

#include <windows.h>
#include "at_os.h"
#include "at_win32_atomic.h"

// Austria namespace
namespace at
{


// ======== AtomicIncrement ===========================================

AUSTRIA_EXPORT AtomicCountType AtomicIncrement(
    volatile AtomicCountType    * io_val
) throw ()
{
    return InterlockedIncrement( (long int*) io_val ) - 1;
}

// ======== AtomicDecrement ===========================================

AUSTRIA_EXPORT AtomicCountType AtomicDecrement(
    volatile AtomicCountType    * io_val
) throw ()
{
    return InterlockedDecrement( (long int*) io_val ) + 1;
}

// ======== AtomicBump ===========================================

AUSTRIA_EXPORT AtomicCountType AtomicBump(
    volatile AtomicCountType    * io_val,
    AtomicCountType                i_add_val
) {
    return InterlockedExchangeAdd( io_val, i_add_val );
}


// ======== AtomicExchange =======================================

AUSTRIA_EXPORT AtomicExchangeablePointer AtomicExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val
) {

    return InterlockedExchangePointer( io_mem_loc, i_val );
}

AUSTRIA_EXPORT AtomicExchangeableValue AtomicExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val
) {

    return InterlockedExchange( io_mem_loc, i_val );
}

// ======== AtomicCompareExchangeValue ================================

AUSTRIA_EXPORT AtomicExchangeableValue AtomicCompareExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val,
    AtomicExchangeableValue                 i_cmp
) {
    return InterlockedCompareExchange( io_mem_loc, i_val, i_cmp );
}

// ======== AtomicCompareExchangePointer ==============================

AUSTRIA_EXPORT AtomicExchangeablePointer AtomicCompareExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val,
    AtomicExchangeablePointer                 i_cmp
) {
    return InterlockedCompareExchangePointer( io_mem_loc, i_val, i_cmp );
}

}; // namespace


