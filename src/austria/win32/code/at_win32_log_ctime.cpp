/**
 * \file at_win32_log_ctime.cpp
 *
 * \author Bradley Austin
 *
 */


#include "at_win32_log_ctime.h"

//#include <afx.h>
#include <ctime>

namespace at
{

#if 0
	class CTime
	{

		std::tm		m_time;
	};

    class LogManagerTimePolicy_CTime_win32_TimeTypeImpl
      : public LogManagerTimePolicy_CTime_win32_TimeTypeImplBase
    {

    public:

        const CTime m_ctime;

        inline LogManagerTimePolicy_CTime_win32_TimeTypeImpl(
            const LogManagerTimePolicy_CTime_win32_TimeTypeImpl & i_time
        )
          : m_ctime( i_time.m_ctime )
        {
        }

        inline LogManagerTimePolicy_CTime_win32_TimeTypeImpl(
            const CTime & i_ctime
        )
          : m_ctime( i_ctime )
        {
        }

    private:

        virtual const LogManagerTimePolicy_CTime_win32_TimeTypeImplBase * Clone() const
        {
            return new LogManagerTimePolicy_CTime_win32_TimeTypeImpl( *this );
        }

        /* Unimplemented */
        LogManagerTimePolicy_CTime_win32_TimeTypeImpl & operator=
            ( const LogManagerTimePolicy_CTime_win32_TimeTypeImpl & );

    };


    LogManagerTimePolicy_CTime_win32_TimeType
        LogManagerTimePolicy_CTime_win32::GetTime()
    {
        return LogManagerTimePolicy_CTime_win32_TimeType(
            LogManagerTimePolicy_CTime_win32_TimeTypeImpl(
                CTime::GetCurrentTime()
            )
        );
    }


    std::string LogWriterTimeFormatPolicy_CTime_win32::FormatTime(
        const LogManagerTimePolicy_CTime_win32_TimeType & i_time
    )
    {
        return
            (LPCTSTR)
                (static_cast< const LogManagerTimePolicy_CTime_win32_TimeTypeImpl * >(
                     i_time.m_impl
                 )->m_ctime.Format( "%H:%M:%S  %a %b %d %Y" )
                );
    }

#endif
}
