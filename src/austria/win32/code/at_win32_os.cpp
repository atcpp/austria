//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_gx86_os.cpp.i
 *
 */


#include <windows.h>
#include <process.h>    /* win32 threading header */
#include <crtdbg.h>
#include <signal.h>
#include <set>


// ======== ProcessorCount ============================================
/**
 * Return the number of available CPUs
 *
 *
 */

static unsigned ProcessorCount()
{
	SYSTEM_INFO     m_system_info;

	::GetSystemInfo( & m_system_info );
    return m_system_info.dwNumberOfProcessors;
}


namespace at
{
    
// ======== SchedulerYield =======================================
void OSTraitsBase::SchedulerYield()
{
    ::SleepEx( 0, 0 );
}

// ======== s_cpu_count ==========================================
const unsigned OSTraitsBase::s_cpu_count = ProcessorCount();


// ======== s_smp_memory_latency =================================
const unsigned OSTraitsBase::s_smp_memory_latency =
    OSTraitsBase::s_cpu_count > 1 ? 400 : 0;



} // namespace
