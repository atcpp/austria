/*
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 */
#include "at_net.h"
#include "at_net_ipv4.h"
#include "at_net_helpers.h"
#include "at_unit_test.h"
#include "at_twinmt_basic.h"

#include <stdlib.h>
#include <time.h>
#include <list>
#include <set>

using namespace at;

#define MAX_NET_ITER 1000

const unsigned int maxTestSeconds = 10;
const TimeInterval maxTestWait (maxTestSeconds*TimeInterval::PerSec);

namespace
{
    struct DatagramEvent
    {
        bool receiveFailure;
        bool sendFailure;
        bool dataReady;
        bool closed;
        const NetDatagramError *error;
        Ptr<const Buffer*> buffer;
        DatagramEvent() : receiveFailure(false),
                          sendFailure(false),
                          dataReady(false),
                          closed(false),
                          error(0),
                          buffer(0)
        { }
    };

    class DatagramRecorder :
        public LeadTwinMT_Basic<NetDatagramChannelResponder>
    {
    public:
        std::list<DatagramEvent>    events;
        Ptr<MutexRefCount *>        m_mutex;
        Conditional *               m_newEvent;

        DatagramRecorder() :
            events(),
            m_mutex(new MutexRefCount(Mutex::Recursive)),
            m_newEvent(new Conditional(*m_mutex))
        {
        }

        virtual ~DatagramRecorder()
        {
            LeadCancel();
            {
                Lock<Ptr<MutexRefCount *> > l(m_mutex);
                delete m_newEvent;
                m_newEvent = NULL;
            }
            m_mutex = NULL;
        }

    public: // NetDatagramChannelResponder
        virtual void ReceiveFailure(const NetDatagramError &i_error)
        {
            DatagramEvent call = DatagramEvent();
            call.receiveFailure = true;
            call.error = &i_error;
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            events.push_back(call);
            m_newEvent->Post();
        }

        virtual void SendCompleted(PtrDelegate<const Buffer*> i_buffer)
        {
            // TODO: Record these.
        }

        virtual void SendFailure(
            PtrDelegate<const Buffer*> i_buffer,
            const NetDatagramError &i_error)
        {
            DatagramEvent call = DatagramEvent();
            call.sendFailure = true;
            call.buffer = i_buffer;
            call.error = &i_error;
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            events.push_back(call);
            m_newEvent->Post();
        }

        virtual void DataReady()
        {
            DatagramEvent call = DatagramEvent();
            call.dataReady = true;
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            events.push_back(call);
            m_newEvent->Post();
        }

        void dump()
        {
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            std::list<DatagramEvent>::iterator it;
            std::list<DatagramEvent>::iterator end;
            it = events.begin();
            end = events.end();
            for (; it != end; ++it)
            {
                if (it->receiveFailure)
                    std::cout << " receiveFailure ";
                else if (it->sendFailure)
                    std::cout << " sendFailure ";
                else if (it->dataReady)
                    std::cout << " dataReady ";
                else if (it->closed)
                    std::cout << " closed ";
                else
                    AT_Assert(false);
            }
            std::cout << std::endl;
        }

    public: // AppLeadTwinMT_ForwardTraits (from LeadTwinMT_Basic)
        virtual void AppLeadCompleted(TwinTraits::TwinCode i_completion_code)
        {
            DatagramEvent call = DatagramEvent();
            call.closed = true;
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            events.push_back(call);
            m_newEvent->Post();
        }
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_mutex)
        {
            // catch a copy of the mutex, which is the entire purpose of this override
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            delete m_newEvent;
            m_newEvent = new Conditional(*i_mutex);
            m_mutex = i_mutex;
            return LeadTwinMT_Basic<NetDatagramChannelResponder>::LeadAssociate(i_aide, i_mutex);
        }

    };

    // RandomNumber
    //
    // Returns a random number in the range 1 to i_n.
    int RandomNumber(int i_n)
    {
        return 1 + (int)(rand() * double(i_n) / (1.0 + RAND_MAX));
    }

    // RandomString
    //
    // Returns a random string consisting of up to i_max_length lowercase
    // letters.
    std::string RandomString(int i_max_length)
    {
        const std::string l_letters("abcdefghijklmnopqrstuvwxyz");

        int l_length(RandomNumber(i_max_length));

        std::string l_str;
        for (int l_pos(0); l_pos < l_length; ++l_pos)
        {
            l_str += l_letters[RandomNumber(26) - 1];
        }

        return l_str;
    }

    class TCPServerResponder : public at::LeadTwinMT_Basic<at::NetConnectionResponderIf>, public at::PtrTarget_MT
    {
    public:
        TCPServerResponder(at::PtrDelegate<at::NetConnection*> io_endpoint):
            m_endpoint(io_endpoint),
            m_mutex(new MutexRefCount(Mutex::Recursive)),
            m_newEvent(new Conditional(*m_mutex))
          {
              std::cout << "TCPServerResponder constructor called ..." << std::endl;

              m_endpoint->ConnectionNotify(*this);
          }

          virtual ~TCPServerResponder()
          {
            std::cout << "TCPServerResponder destructor called ..." << std::endl;

            LeadCancel();
            std::cout << "TCPServerResponder destructor [cancelled lead]" << std::endl;

            m_endpoint = NULL;
            delete m_newEvent;
            m_newEvent = NULL;
            m_mutex = NULL;
            std::cout << "TCPServerResponder destructor ... exited" << std::endl;
          }

          // NetConnectionResponderIf interface
          virtual void ReceiveFailure(const at::NetConnectionError &i_error)
          {
              std::cout << "TCPServerResponder::ReceiveFailure called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_mutex);
          }

          virtual void SendCompleted(at::PtrDelegate<const at::Buffer*> i_buffer, const char *i_end)
          {
              std::cout << "TCPServerResponder::SendCompleted called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_mutex);
          }

          virtual void SendFailure(at::PtrDelegate<const at::Buffer*> i_buffer, const char *i_begin, const at::NetConnectionError &i_error)
          {
              std::cout << "TCPServerResponder::SendFailure called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_mutex);
          }

          virtual void DataReady()
          {
              std::cout << "TCPServerResponder::DataReady called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_mutex);

              at::Ptr<at::Buffer*> l_message_buffer(NewBuffer());
              l_message_buffer->reserve(40000);

              // If this buffer fills up we have a serious problem as this while loop
              // will never complete.  As this is just a test we can make sure that
              // the buffer is big enough to contain all of the messages that are to
              // be received here.
              while (m_endpoint->Receive(l_message_buffer));

              std::string l_message_string = l_message_buffer->Region().String(); 
              std::cout << "Returning " <<  l_message_string << std::endl;

              if (!l_message_string.empty())
              {
                // Just send the message back to where it came from
                m_endpoint->Send(l_message_buffer);
              }
          }

          virtual void OutOfBandDataReady()
          {
              std::cout << "TCPServerResponder::OutOfBandDataReady called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_mutex);
          }

          virtual void ReceivedPropertiesNotification(bool i_complete, bool i_props_in_queue)
          {
              std::cout << "TCPServerResponder::ReceivedPropertiesNotification called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_mutex);
          }

    public: // AppLeadTwinMT_ForwardTraits (from LeadTwinMT_Basic)
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_mutex)
        {
            // catch a copy of the mutex, which is the entire purpose of this override
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            delete m_newEvent;
            m_newEvent = new Conditional(*i_mutex);
            m_mutex = i_mutex;
            return LeadTwinMT_Basic<NetConnectionResponderIf>::LeadAssociate(i_aide, i_mutex);
        }
        virtual void AppLeadCompleted(TwinTraits::TwinCode i_completion_code)
        {
            // wake up all waiters if there is no possibility of future events
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            m_newEvent->Post();
        }

    private:
        at::Ptr<at::NetConnection*> m_endpoint;
        Ptr<MutexRefCount *>        m_mutex;
        Conditional *               m_newEvent;
    };

    class TCPServer : public at::LeadTwinMT_Basic<at::NetConnectLeadIf>
    {
    public:
        TCPServer():
          m_stateMutex(new MutexRefCount(Mutex::Recursive)),
          m_net(FactoryRegister<at::Net, DKy>::Get().Create("Net")()),
          m_responders(),
          m_listening(false)
          {
              std::cout << "TCPServer constructor called ..." << std::endl;
          }

          virtual ~TCPServer()
          {
              std::cout << "TCPServer destructor called ..." << std::endl;

              LeadCancel();
              std::cout << "TCPServer destructor [cancelled lead]" << std::endl;

              m_responders.clear();
              m_net = NULL;
              std::cout << "TCPServer destructor ... exited" << std::endl;
          }

          bool Listen(at::PtrDelegate<at::NetAddress*> i_address)
          {
              std::cout << "TCPServer::Listen called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_stateMutex);

              // If the call to Listen returns without Failed having been
              // called then it was a successful call.
              m_listening = true;

              at::Ptr<at::NetParameters*> l_parameters;
              m_net->Listen(this, i_address, l_parameters);

              return m_listening;
          }

          // NetConnectLeadIf interface
          virtual void Failed(const at::NetError &i_err)
          {
              std::cout << "TCPServer::Failed called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_stateMutex);

              m_listening = false;
          }

          virtual void Established(at::PtrDelegate<at::NetConnection*> io_endpoint)
          {
              std::cout << "TCPServer::Established called ..." << std::endl;
              Lock<Ptr<MutexRefCount *> > l(m_stateMutex);

              at::Ptr<TCPServerResponder*> l_responder(new TCPServerResponder(io_endpoint));
              m_responders.push_back(l_responder);
          }

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectLeadIf>::LeadAssociate(i_aide, i_sharedMutex);
        }

    private:
        Ptr<MutexRefCount *>    m_stateMutex;
        at::Ptr<at::Net*> m_net;
        std::list<at::Ptr<TCPServerResponder*> > m_responders;
        bool m_listening;
    };

    class TCPClient : public at::LeadTwinMT_Basic<at::NetConnectionResponderIf>
    {
    public:
        TCPClient():
              m_clientConnector(*this),
              m_stateMutexResponderLead(new MutexRefCount(Mutex::Recursive)),
              m_responseCompleteEvent(new Conditional(*m_stateMutexResponderLead)),
              m_sent_messages(),
              m_waiting_for_responses(false),
              m_messages()
          {
              std::cout << "TCPClient constructor called ..." << std::endl;
          }

          virtual ~TCPClient()
          {
              std::cout << "TCPClient destructor called ..." << std::endl;

              LeadCancel();
              std::cout << "TCPClient destructor [cancelled lead]" << std::endl;

              m_endpoint = NULL;
              m_net = NULL;
              delete m_responseCompleteEvent;
              m_responseCompleteEvent = NULL;
              m_stateMutexResponderLead = NULL;
          }

          bool Connect(at::PtrDelegate<at::NetAddress*> i_address)
          {
              std::cout << "TCPClient Connect called ..." << std::endl;
              at::Ptr<at::NetConnection*> l_endpoint;

              bool retval = m_clientConnector.Connect(i_address,l_endpoint);
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);
              m_endpoint = l_endpoint;
              return retval;
          }

          void Send(std::string i_message)
          {
              std::cout << "TCPClient::Send called with the message " << i_message << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);

              // Remember this message so that we know to wait for the server's
              // response
              m_sent_messages.insert(i_message);

              // Add the terminating character and send the message
              at::Ptr<at::Buffer*> l_buffer(NewBuffer());
              l_buffer->Set(i_message + s_terminator);

              m_endpoint->Send(l_buffer);
              m_waiting_for_responses = true;
          }

          void WaitUntilFinished()
          {
              std::cout << "TCPClient::WaitUntilFinished called ..." << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);

              while (m_waiting_for_responses)
              {
                  m_responseCompleteEvent->Wait(::maxTestWait);
              }
          }

          // NetConnectionResponderIf interface
          virtual void ReceiveFailure(const at::NetConnectionError &i_error)
          {
              std::cout << "TCPClient::ReceiveFailure called ..." << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);
          }

          virtual void SendCompleted(at::PtrDelegate<const at::Buffer*> i_buffer, const char *i_end)
          {
              std::cout << "TCPClient::SendCompleted called ..." << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);
          }

          virtual void SendFailure(at::PtrDelegate<const at::Buffer*> i_buffer, const char *i_begin, const at::NetConnectionError &i_error)
          {
              std::cout << "TCPClient::SendFailure called ..." << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);
          }

          virtual void DataReady()
          {
              std::cout << "TCPClient::DataReady called ..." << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);

              at::Ptr<at::Buffer*> l_message_buffer(NewBuffer());
              l_message_buffer->reserve(50000);

              // If this buffer fills up we have a serious problem as this while loop
              // will never complete.  As this is just a test we can make sure that
              // the buffer is big enough to contain all of the messages that are to
              // be received here.
              while (m_endpoint->Receive(l_message_buffer));

              std::cout << "Received " << l_message_buffer->Region().String() << std::endl;
              // Pull out all of the messages in the buffer.
              m_messages += l_message_buffer->Region().String();

              // Look for a terminator.
              std::string::size_type l_position(m_messages.find_first_of(s_terminator));

              while (l_position != std::string::npos)
              {
                  // We have found a terminator, so there is at least one complete
                  // message in the buffer.

                  // Get the message ...
                  std::string l_message(m_messages.substr(0, l_position));
                  m_messages = m_messages.substr(++l_position);

                  // ... and check that it matches one of the messages that we sent.
                  {
                      std::multiset<std::string>::iterator l_position_in_set(m_sent_messages.find(l_message));
                      AT_TCAssert(l_position_in_set != m_sent_messages.end(), "A message has been corrupted");
                      m_sent_messages.erase(l_position_in_set);
                  }

                  // Look for another terminator
                  l_position = m_messages.find_first_of(s_terminator);
              }

              if (m_sent_messages.empty())
              {
                  // We aren't waiting for any responses so we wake any threads
                  // that were waiting for us to finish.
                  m_waiting_for_responses = false;
                  m_responseCompleteEvent->Post();
              }
              else
              {
                  m_waiting_for_responses = true;
              }
          }

          virtual void OutOfBandDataReady()
          {
              std::cout << "TCPClient::OutOfBandDataReady called ..." << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);
          }

          virtual void ReceivedPropertiesNotification(bool i_complete, bool i_props_in_queue)
          {
              std::cout << "TCPClient::ReceivedPropertiesNotification called ..." << std::endl;
              at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexResponderLead);
          }

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            std::cout << "TCPClient::at::NetConnectionResponder::LeadAssociate..." << std::endl;
            Lock<Ptr<MutexRefCount *> > l(m_stateMutexResponderLead);
            m_responseCompleteEvent->PostAll();
            delete m_responseCompleteEvent;
            m_responseCompleteEvent = new Conditional(*i_sharedMutex);
            m_stateMutexResponderLead = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectionResponderIf>::LeadAssociate(i_aide, i_sharedMutex);
        }

    private:
        // aggregate a connection object
        class TCPClientConnector : public at::LeadTwinMT_Basic<at::NetConnectLeadIf> {
        public:
            TCPClientConnector (at::NetConnectionResponder &i_responder):
                m_stateMutexConnectLead(new MutexRefCount(Mutex::Recursive)),
                m_newConnectionEvent(new Conditional(*m_stateMutexConnectLead)),
                m_state(waiting),
                m_responder(i_responder),
                m_net(FactoryRegister<at::Net, DKy>::Get().Create("Net")())
            {
                std::cout << "TCPClientConnector constructor called ..." << std::endl;
            }

            virtual ~TCPClientConnector()
            {
                std::cout << "TCPClientConnector destructor called ..." << std::endl;

                LeadCancel();
                std::cout << "TCPClientConnectordestructor [cancelled lead]" << std::endl;

                m_endpoint = NULL;
                std::cout << "TCPClientConnectordestructor [disconnected endpoint]" << std::endl;
                m_net = NULL;
                std::cout << "TCPClientConnectordestructor [disconnected net]" << std::endl;
                delete m_newConnectionEvent;
                std::cout << "TCPClientConnectordestructor [deleted connectionevent]" << std::endl;
                m_newConnectionEvent = NULL;
                std::cout << "TCPClientConnectordestructor [nulled connectionevent]" << std::endl;
                m_stateMutexConnectLead = NULL;
                std::cout << "TCPClientConnector destructor ... exited" << std::endl;
            }

            bool Connect(at::PtrDelegate<at::NetAddress*> i_address,at::Ptr<at::NetConnection*> & i_endpoint)
            {
                std::cout << "TCPClientConnector::Connect called ..." << std::endl;

                at::Ptr<at::NetParameters*> l_parameters;
                m_net->Connect(this, i_address, l_parameters);
                at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexConnectLead);
                while (m_state == waiting)
                {
                    m_newConnectionEvent->Wait(::maxTestWait);
                }
                if (m_state == established) {
                    i_endpoint = m_endpoint;
                }
                return m_state == established;
            }
        protected:
            // NetConnectLeadIf interface
            virtual void Failed(const at::NetError &i_err)
            {
                std::cout << "TCPClientConnector::Failed called ..." << std::endl;
                at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexConnectLead);

                m_state = failed;
                m_newConnectionEvent->Post();
            }

            virtual void Established(at::PtrDelegate<at::NetConnection*> io_endpoint)
            {
                std::cout << "TCPClientConnector::Established called ..." << std::endl;
                at::Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutexConnectLead);

                m_state = established;

                io_endpoint->ConnectionNotify(m_responder);
                m_endpoint = io_endpoint;

                m_newConnectionEvent->Post();
            }

        protected:
            virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
            {
                std::cout << "TCPClientConnector::at::NetConnectLead::LeadAssociate..." << std::endl;
                Lock<Ptr<MutexRefCount *> > l(m_stateMutexConnectLead);
                m_newConnectionEvent->PostAll();
                delete m_newConnectionEvent;
                m_newConnectionEvent = new Conditional(*i_sharedMutex);
                m_stateMutexConnectLead = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
                return LeadTwinMT_Basic<NetConnectLeadIf>::LeadAssociate(i_aide, i_sharedMutex);
            }

        private:    // connection state management
            Ptr<MutexRefCount *>    m_stateMutexConnectLead;
            Conditional *           m_newConnectionEvent;

            enum ConnectState {waiting, failed, established};
            ConnectState m_state;

            at::NetConnectionResponder &m_responder;
            at::Ptr<at::Net*> m_net;
            at::Ptr<at::NetConnection*> m_endpoint;
        };

        TCPClientConnector m_clientConnector;

    private:    // IO state management
        Ptr<MutexRefCount *>    m_stateMutexResponderLead;
        Conditional *           m_responseCompleteEvent;

        std::multiset<std::string> m_sent_messages;
        bool m_waiting_for_responses;
        std::string m_messages;
        at::Ptr<at::Net*> m_net;
        at::Ptr<at::NetConnection*> m_endpoint;

        // A character to terminate our messages.
        const static char s_terminator = '-';

    };

    class UDPServerResponder : public at::LeadTwinMT_Basic<at::NetDatagramChannelResponderIf>, public at::PtrTarget_MT
{
public:
    UDPServerResponder(at::PtrDelegate<at::NetDatagramChannel*> io_datagramChannel):
    m_datagramChannel(io_datagramChannel)
    {
        std::cout << "UDPServerResponder constructor called ..." << std::endl;

        m_datagramChannel->DatagramChannelNotify(this);
    }

    ~UDPServerResponder()
    {
        std::cout << "UDPServerResponder destructor called ..." << std::endl;

        LeadCancel();
    }

    // NetDatagramChannelResponderIf interface
    virtual void ReceiveFailure(const at::NetDatagramError &i_error)
    {
        std::cout << "UDPServerResponder::ReceiveFailure called ..." << std::endl;
    }

    virtual void SendCompleted(at::PtrDelegate<const at::Buffer*> i_buffer)
    {
        std::cout << "UDPServerResponder::SendCompleted called ..." << std::endl;
    }


    virtual void SendFailure(at::PtrDelegate<const at::Buffer*> i_buffer, const at::NetDatagramError &i_error)
    {
        std::cout << "UDPServerResponder::SendFailure called ..." << std::endl;
    }

    virtual void DataReady()
    {
        std::cout << "UDPServerResponder::DataReady called ..." << std::endl;

        std::list<at::NetDatagramBuffer> l_messages;
        while (m_datagramChannel->Receive(l_messages));

        std::list<at::NetDatagramBuffer>::iterator l_messageIter(l_messages.begin());
        std::list<at::NetDatagramBuffer>::iterator l_messageEnd(l_messages.end());

        for (; l_messageIter != l_messageEnd; ++l_messageIter)
        {
            // Get the message ...
            at::Ptr<const at::Buffer*> l_messageBuffer(l_messageIter->m_buffer);
            std::string l_message(l_messageBuffer->Region().String());
            at::Ptr<at::NetAddress*> l_address(l_messageIter->m_address);

            // ... and send it back to where it came from
            at::Ptr<at::Buffer*> l_buffer(NewBuffer());
            l_buffer->Set(l_message);

            m_datagramChannel->Send(l_buffer, l_address);
        }
    }

private:
    at::Ptr<at::NetDatagramChannel*> m_datagramChannel;
};

class UDPServer : public at::LeadTwinMT_Basic<at::NetDatagramLeadIf>
{
public:
    UDPServer():
    m_net(FactoryRegister<at::Net, DKy>::Get().Create("Net")()),
    m_responders(),
    m_listening(false)
    {
        std::cout << "UDPServer constructor called ..." << std::endl;
    }

    virtual ~UDPServer()
    {
        std::cout << "UDPServer destructor called ..." << std::endl;

        LeadCancel();
    }

    bool ListenDatagram(at::PtrDelegate<at::NetAddress*> i_address)
    {
        std::cout << "UDPServer::Listen called ..." << std::endl;

        // If the call to ListenDatagram returns without ListenerReady
        // having been called then it was a failed call.
        m_listening = false;

        at::Ptr<at::NetParameters*> l_parameters;
        m_net->ListenDatagram(this, i_address, l_parameters);

        return m_listening;
    }

    // NetDatagramLeadIf interface
    virtual void Failed(const at::NetError &i_err)
    {
        std::cout << "UDPServer::Failed called ..." << std::endl;
    }

    virtual void ListenerReady(at::PtrDelegate<at::NetDatagramChannel*> io_datagramChannel)
    {
        std::cout << "UDPServer::ListenerReady called ..." << std::endl;

        at::Ptr<UDPServerResponder*> l_responder(new UDPServerResponder(io_datagramChannel));
        m_responders.push_back(l_responder);

        m_listening = true;
    }

private:
    at::Ptr<at::Net*> m_net;
    std::list<at::Ptr<UDPServerResponder*> > m_responders;
    bool m_listening;
};

class UDPClient : public at::LeadTwinMT_Basic<at::NetDatagramLeadIf>, public at::LeadTwinMT_Basic<at::NetDatagramChannelResponderIf>
{
public:
    UDPClient():
    m_sent_messages_mutex(),
    m_response_waiter(),
    m_net(FactoryRegister<at::Net, DKy>::Get().Create("Net")()),
    m_listening(false),
    m_datagramChannel(0),
    m_waiting_for_responses(false),
    m_sent_messages()
    {
        std::cout << "UDPClient constructor called ..." << std::endl;
    }

    virtual ~UDPClient()
    {
        std::cout << "UDPClient destructor called ..." << std::endl;

        at::LeadTwinMT_Basic<at::NetDatagramChannelResponderIf>::LeadCancel();
        at::LeadTwinMT_Basic<at::NetDatagramLeadIf>::LeadCancel();
    }

    bool ListenDatagram(at::PtrDelegate<at::NetAddress*> i_address)
    {
        std::cout << "UDPClient::Connect called ..." << std::endl;

        // If the call to ListenDatagram returns without ListenerReady
        // having been called then it was a failed call.
        m_listening = false;

        at::Ptr<at::NetParameters*> l_parameters;
        m_net->ListenDatagram(this, i_address, l_parameters);

        return m_listening;
    }

    void Send(std::string i_message, at::Ptr<at::NetAddress*> i_address)
    {
        std::cout << "UDPClient::Send called with the message " << i_message << std::endl;

        // Remember this message so that we know to wait for the server's
        // response
        {
            at::Lock<at::ConditionalMutex> l_lock(m_response_waiter);

            m_waiting_for_responses = true;
        }

        {
            at::Lock<at::Mutex> l_lock(m_sent_messages_mutex);

            m_sent_messages.insert(i_message);
        }

        at::Ptr<at::Buffer*> l_buffer(NewBuffer());
        l_buffer->Set(i_message);

        m_datagramChannel->Send(l_buffer, i_address);
    }

    void WaitUntilFinished()
    {
        std::cout << "UDPClient::WaitUntilFinished called ..." << std::endl;

        at::Lock<at::ConditionalMutex> l_lock(m_response_waiter);

        while (m_waiting_for_responses)
        {
            m_response_waiter.Wait();
        }
    }

    // NetDatagramLeadIf interface
    virtual void Failed(const at::NetError &i_err)
    {
        std::cout << "UDPServer::Failed called ..." << std::endl;
    }

    virtual void ListenerReady(at::PtrDelegate<at::NetDatagramChannel*> io_datagramChannel)
    {
        std::cout << "UDPServer::ListenerReady called ..." << std::endl;

        m_datagramChannel = io_datagramChannel;
        m_datagramChannel->DatagramChannelNotify(this);

        m_listening = true;
    }

    // NetDatagramChannelResponderIf interface
    virtual void ReceiveFailure(const at::NetDatagramError &i_error)
    {
        std::cout << "UDPClient::ReceiveFailure called ..." << std::endl;
    }

    virtual void SendCompleted(at::PtrDelegate<const at::Buffer*> i_buffer)
    {
        std::cout << "UDPClient::SendCompleted called ..." << std::endl;
    }


    virtual void SendFailure(at::PtrDelegate<const at::Buffer*> i_buffer, const at::NetDatagramError &i_error)
    {
        std::cout << "UDPClient::SendFailure called ..." << std::endl;
    }

    virtual void DataReady()
    {
        std::cout << "UDPClient::DataReady called ..." << std::endl;

        std::list<at::NetDatagramBuffer> l_messages;

        while (m_datagramChannel->Receive(l_messages));

        std::list<at::NetDatagramBuffer>::iterator l_messageIter(l_messages.begin());
        std::list<at::NetDatagramBuffer>::iterator l_messageEnd(l_messages.end());

        for (; l_messageIter != l_messageEnd; ++l_messageIter)
        {
            // Get the message ...
            at::Ptr<const at::Buffer*> l_messageBuffer(l_messageIter->m_buffer);
            std::string l_message(l_messageBuffer->Region().String());

            // ... and check that it matches one of the messages that we sent.
            {
                at::Lock<at::Mutex> l_lock(m_sent_messages_mutex);

                std::multiset<std::string>::iterator l_position_in_set(m_sent_messages.find(l_message));
                AT_TCAssert(l_position_in_set != m_sent_messages.end(), "A message has been corrupted");
                m_sent_messages.erase(l_position_in_set);
            }
        }

        {
            at::Lock<at::Mutex> l_lock(m_sent_messages_mutex);

            if (m_sent_messages.empty())
            {
                // We aren't waiting for any responses so we wake any threads
                // that were waiting for us to finish.
                at::Lock<at::Mutex> l_lock(m_response_waiter);

                m_waiting_for_responses = false;
                m_response_waiter.Post();
            }
            else
            {
                // Even though the communication is via UDP, all transmission
                // in this test is through the loopback and so should be
                // reliable.  We assume therefore that if we don't receive a
                // response then it has been lost by our code.
                at::Lock<at::Mutex> l_lock(m_response_waiter);

                m_waiting_for_responses = true;
            }
        }
    }

private:
    at::Mutex m_sent_messages_mutex;
    at::ConditionalMutex m_response_waiter;
    at::Ptr<at::Net*> m_net;
    bool m_listening;
    at::Ptr<at::NetDatagramChannel*> m_datagramChannel;
    bool m_waiting_for_responses;
    std::multiset<std::string> m_sent_messages;
};

AT_TestArea(Net, "Network Interface Tests");

    AT_DefineTestLevel(UDPBasic, Net, "UDP (Datagram) Network Test",UnitTestTraits::TestSuccess,1)
    {
        void Run()
        {
            std::list<NetDatagramBuffer> packets;
            DatagramRecorder recordera;
            DatagramRecorder recorderb;
            DatagramEvent ev;
            Ptr<Pool*> pool = FactoryRegister<at::Pool,DKy>::Get().Create("Basic")();
            if (!pool) {
                std::cout << " No Pool Object returned from factory" << std::endl;
                AT_Assert(pool);
                return;
            }
            Ptr<Net*> ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
            if (!ip) {
                std::cout << " No Net Object returned from factory" << std::endl;
                AT_Assert(ip);
                return;
            }
            Ptr<IPv4Address*> addra = new IPv4Address(127,0,0,1,56565);
            Ptr<IPv4Address*> addrb = new IPv4Address(127,0,0,1,54545);
            const NetError *error = 0;
            Ptr<NetDatagramChannel*> channela;
            Ptr<NetDatagramChannel*> channelb;
            channela = SyncListenDatagram(ip, addra, 0, pool, &error);
            if (!error && channela) {
                channelb = SyncListenDatagram(ip, addrb, 0, pool, &error);
            }
            if (!error && channela && channelb) {
                Ptr<Buffer*> buffer = pool->Create();
                buffer->Append("This is a packet.");
                channela->DatagramChannelNotify(&recordera);
                channelb->DatagramChannelNotify(&recorderb);
                for (int i = 0; i < MAX_NET_ITER; ++i)
                {
                    packets = std::list<NetDatagramBuffer>();
                    channela->Send(buffer, addrb);
                    {
                        Lock<Ptr<MutexRefCount *> > l(recorderb.m_mutex);
                        while (recorderb.events.size() == 0) {
                            if (!recorderb.m_newEvent->Wait(maxTestWait)) {
                                AT_Assert(!"recorderb.m_newEvent->Wait(maxTestWait)");
                                break;
                            }
                        }
                        // REV/BUG: epoll appears to gives a superfluous
                        // EPOLLIN event, which causes a superfluous DataReady
                        // call.  This may be avoidable (or might actually be a
                        // bug in my code) but for the time being, I am
                        // treating superfluous DataReady calls as harmless.
                        while (recorderb.events.size())
                        {
                            ev = recorderb.events.front();
                            AT_Assert(ev.dataReady);
                            recorderb.events.pop_front();
                        }
                    }
                    while (channelb->Receive(packets));
                    AT_Assert(packets.size() == 1);
                    Buffer::t_Region original = buffer->Region();
                    Buffer::t_ConstRegion received = packets.front().m_buffer->Region();
                    AT_Assert(original.m_allocated == received.m_allocated);
                    AT_Assert(std::equal(
                                original.m_mem,
                                original.m_mem + original.m_allocated,
                                received.m_mem));
                    AT_Assert(*addra == *packets.front().m_address);
                    // REV: Sometimes the superfluous DataReady call will
                    // happen after we have read the packet, so we just reset
                    // these.
                    {
                        Lock<Ptr<MutexRefCount *> > l(recorderb.m_mutex);
                        recordera.events.clear();
                        recorderb.events.clear();
                    }
                }
                channela = 0;
                {
                    Lock<Ptr<MutexRefCount *> > l(recordera.m_mutex);
                    while (recordera.events.size() == 0) {
                        if (!recordera.m_newEvent->Wait(maxTestWait)) {
                            AT_Assert(!"recordera.m_newEvent->Wait(maxTestWait)");
                            break;
                        }
                    }
                }
                ev = recordera.events.front();
                recordera.events.pop_front();
                AT_Assert(ev.closed);
            } else {
                std::cout << " connection failed\n" << std::endl;
            }
        }
    };

    AT_RegisterTest(UDPBasic, Net);

    AT_DefineTestLevel(TCPBasic, Net, "TCP Network Test",UnitTestTraits::TestSuccess,0)
    {
        void Run()
        {
            for (int x = 0; x < 10; ++x)
            {
                Ptr<NetConnectionRecorder *>svrrec;
                Ptr<NetConnectionRecorder *>clirec;
                NetConnectionEvent ev;
                Ptr<Net*> ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
                if (!ip) {
                    std::cout << " No Net Object returned from factory" << std::endl;
                    AT_Assert(ip);
                    continue;
                }
                Ptr<IPv4Address*> addr;
                addr = new IPv4Address(127,0,0,1,9002+x);
                Ptr<NetConnection*> svrconn;
                Ptr<NetConnection*> cliconn;
                XConnectLead<NetConnectionRecorder> svrConnectLead;
                XConnectLead<NetConnectionRecorder> cliConnectLead;
                Ptr<Buffer*> outBuffer = NewBuffer();
                outBuffer->Append("This is not a packet.");
                ip->Listen(&svrConnectLead, addr, 0);
                ip->Connect(&cliConnectLead, addr, 0);
                cliConnectLead.Wait(&clirec, 0, maxTestWait);
                if (clirec) {
                    // only wait for server if client succeeded (otherwise, server will listen forever)
                    svrConnectLead.Wait(&svrrec, 0, maxTestWait);
                } else {
                    // among other reasons, the client can fail in this test suite due to the server being
                    // overwhelmed, where previous client sessions being closed asynchronously have not yet
                    // been closed, and the servers resources have been depleted temporarily until the
                    // pending closes are flushed.
                    std::cerr << " tcpbasic -- client connect failed: port [" << 9002 + x << "]" << std::endl;
                }

                if (svrrec && clirec) {
                    svrconn = svrrec->Connection();
                    cliconn = clirec->Connection();
                }

                if (cliconn && svrconn) {
                    for (int i = 0; i < MAX_NET_ITER; ++i)
                    {
                        Ptr<Buffer*> inBuffer = NewBuffer();
                        inBuffer->reserve(inBuffer->size() + 100);
                        cliconn->Send(outBuffer);
                        cliconn->Send(outBuffer);

                        time_t timeStart = ::time(NULL);
                        time_t timeElapsed = 0;
                        bool done = false;
                        while (timeElapsed < maxTestSeconds) {
                            Lock<Ptr<MutexRefCount *> > l(svrrec->StateMutex());
                            while (svrconn->Receive(inBuffer));
                            if (inBuffer->Region().m_allocated >= 2 * outBuffer->Region().m_allocated) {
                                break;  // got enough
                            }
                            while (svrrec->EventList().size() == 0) {
                                if (!svrrec->GetConditional().Wait(maxTestWait)) {
                                    AT_Assert(!"timeout in TCPBasic waiting for message at server");
                                    break;
                                }
                            }
                            while (svrrec->EventList().size())
                            {
                                ev = svrrec->EventList().front();
                                AT_Assert(ev.type == NetConnectionEvent::DataReady);
                                svrrec->EventList().pop_front();
                            }
                            timeElapsed = ::time(NULL) - timeStart;
                        }

                        Buffer::t_Region original = outBuffer->Region();
                        Buffer::t_ConstRegion received = inBuffer->Region();

                        AT_Assert(2*original.m_allocated == received.m_allocated);
                        AT_Assert(std::equal(
                                    original.m_mem,
                                    original.m_mem + original.m_allocated,
                                    received.m_mem));
                        AT_Assert(std::equal(
                                    original.m_mem,
                                    original.m_mem + original.m_allocated,
                                    received.m_mem + original.m_allocated));
                        inBuffer = NULL;
                    }
                } else {
                    std::cout << " connection failed\n" << std::endl;
                }
                outBuffer = NULL;

                svrconn = NULL;
                cliconn = NULL;

                ip = NULL;

                svrrec = NULL;
                clirec = NULL;

                // TODO: We'd like to destroy one of the connection objects
                // and then check that the other sees the close, but we'd
                // have to try a read and let it fail, which is fine but I
                // don't feel like it right now.
            }
        }
    };

    AT_RegisterTest(TCPBasic, Net);

    AT_DefineTestLevel(TCPFail, Net, "TCP test for things that should fail",UnitTestTraits::TestSuccess,0)
    {
        void Run()
        {
            // keep the iterations small or the test harness will think that the test hung
            // on win32 with certain firewall software, since they have a random hang-time
            // to prevent active machine identification
            for (int x = 0; x < 25; ++x)
            {
                // Make sure that a connection attempt to an address with
                // no one listening for connections fails properly.
                {
                    Ptr<NetConnectionRecorder *>rec;
                    const NetError *err;
                    Ptr<IPv4Address*> addr;
                    XConnectLead<NetConnectionRecorder> lead;

                    // 49151 is reserved by the IANA, so hopefully it's a
                    // good number to use for a port to which nothing
                    // should be listening...nice try...someone WAS listening
                    // we will try 1023 instead.
                    //addr = new IPv4Address(127,0,0,1,49151);
                    addr = new IPv4Address(127,0,0,1,1023);
                    Ptr<Net*> ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
                    if (!ip) {
                        std::cout << " No Net Object returned from factory" << std::endl;
                        AT_Assert(!" No Net Object returned from factory");
                        continue;
                    }
                    ip->Connect(&lead, addr, 0);
                    lead.Wait(&rec, &err, maxTestWait);
                    if (rec || (err != &NetError::s_connection_refused)) {
                        if (rec) {
                            std::cout << " unexpected connection!" << rec;
                        }
                        std::cout << err;
                    }
                    AT_Assert(rec == 0);
//                    AT_Assert(err == &NetError::s_connection_refused);
                    rec = NULL;
                    ip = NULL;
                }
            }
        }
    };

    AT_RegisterTest(TCPFail, Net);

    AT_DefineTestLevel(TCP, Net, "TCP Client and Server Test", UnitTestTraits::TestSuccess, 0)
    {
        void Run()
        {
            TCPServer l_server1;
            TCPServer l_server2;

            TCPClient l_client1;
            TCPClient l_client2;
            TCPClient l_client3;
            TCPClient l_client4;

            // Try to find ports for the servers to listen on
            for (at::IPv4Address::t_Port l_port(8000) ; l_port < 8050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for server 1" << std::endl;

                at::Ptr<at::NetAddress*> l_address(new at::IPv4Address(127, 0, 0, 1, l_port));
                if (!l_server1.Listen(l_address))
                {
                    continue;
                }
                if (!l_client1.Connect(l_address))
                {
                    continue;
                }
                if (!l_client2.Connect(l_address))
                {
                    continue;
                }

                // Both clients have managed to connect to the server, so this port
                // will do.
                std::cout << "Using port " << l_port << " for server 1" << std::endl;
                
                break;
            }

            for (at::IPv4Address::t_Port l_port(9000) ; l_port < 9050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for server 2" << std::endl;
                
                at::Ptr<at::NetAddress*> l_address(new at::IPv4Address(127, 0, 0, 1, l_port));
                if (!l_server2.Listen(l_address))
                {
                    continue;
                }
                if (!l_client3.Connect(l_address))
                {
                    continue;
                }
                if (!l_client4.Connect(l_address))
                {
                    continue;
                }

                // Both clients have managed to connect to the server, so this port
                // will do.
                std::cout << "Using port " << l_port << " for server 2" << std::endl;
                
                break;
            }

            // Send some messages
            for (int count(0); count < 100; ++count)
            {
                l_client1.Send(RandomString(10));
                l_client2.Send(RandomString(10));
                l_client3.Send(RandomString(10));
                l_client4.Send(RandomString(10));
            }

            // Wait until the servers have responded to all of the messages that
            // were sent.
            l_client1.WaitUntilFinished();
            l_client2.WaitUntilFinished();
            l_client3.WaitUntilFinished();
            l_client4.WaitUntilFinished();
        }
    };

    AT_RegisterTest(TCP, Net);

    AT_DefineTestLevel(UDP, Net, "UDP Client and Server Test", UnitTestTraits::TestSuccess, 0)
    {
        void Run()
        {
            // Make a couple of servers and some clients
            UDPServer l_server1;
            UDPServer l_server2;

            UDPClient l_client1;
            UDPClient l_client2;
            UDPClient l_client3;
            UDPClient l_client4;

            // Try to find ports for the servers and clients to listen on
            at::Ptr<at::NetAddress*> l_ServerAddress1;

            for (at::IPv4Address::t_Port l_port(8000); l_port < 8050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for server 1" << std::endl;

                l_ServerAddress1 = new at::IPv4Address(127, 0, 0, 1, l_port);
                if (l_server1.ListenDatagram(l_ServerAddress1))
                {
                    std::cout << "Using port " << l_port << " for server 1" << std::endl;
                    break;
                }
            }

            at::Ptr<at::NetAddress*> l_ServerAddress2;

            for (at::IPv4Address::t_Port l_port(8000); l_port < 8050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for server 2" << std::endl;

                l_ServerAddress2 = new at::IPv4Address(127, 0, 0, 1, l_port);
                if (l_server2.ListenDatagram(l_ServerAddress2))
                {
                    std::cout << "Using port " << l_port << " for server 2" << std::endl;
                    break;
                }
            }

            for (at::IPv4Address::t_Port l_port(9000); l_port < 9050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for client 1" << std::endl;

                at::Ptr<at::NetAddress*> l_address(new at::IPv4Address(127, 0, 0, 1, l_port));
                if (l_client1.ListenDatagram(l_address))
                {
                    std::cout << "Using port " << l_port << " for client 1" << std::endl;
                    break;
                }
            }

            for (at::IPv4Address::t_Port l_port(9000); l_port < 9050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for client 2" << std::endl;

                at::Ptr<at::NetAddress*> l_address(new at::IPv4Address(127, 0, 0, 1, l_port));
                if (l_client2.ListenDatagram(l_address))
                {
                    std::cout << "Using port " << l_port << " for client 2" << std::endl;
                    break;
                }
            }

            for (at::IPv4Address::t_Port l_port(9000); l_port < 9050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for client 3" << std::endl;

                at::Ptr<at::NetAddress*> l_address(new at::IPv4Address(127, 0, 0, 1, l_port));
                if (l_client3.ListenDatagram(l_address))
                {
                    std::cout << "Using port " << l_port << " for client 3" << std::endl;
                    break;
                }
            }

            for (at::IPv4Address::t_Port l_port(9000); l_port < 9050; ++l_port)
            {
                std::cout << "Trying port " << l_port << " for client 4" << std::endl;

                at::Ptr<at::NetAddress*> l_address(new at::IPv4Address(127, 0, 0, 1, l_port));
                if (l_client4.ListenDatagram(l_address))
                {
                    std::cout << "Using port " << l_port << " for client 4" << std::endl;
                    break;
                }
            }

            // Send some messages
            for (int count(0); count < 100; ++count)
            {
                l_client1.Send(RandomString(10), l_ServerAddress1);
                l_client2.Send(RandomString(10), l_ServerAddress1);
                l_client3.Send(RandomString(10), l_ServerAddress2);
                l_client4.Send(RandomString(10), l_ServerAddress2);
            }

            // Wait until the servers have responded to all of the messages that
            // were sent.
            l_client1.WaitUntilFinished();
            l_client2.WaitUntilFinished();
            l_client3.WaitUntilFinished();
            l_client4.WaitUntilFinished();
        }
    };

    AT_RegisterTest(UDP, Net);

} // namespace
