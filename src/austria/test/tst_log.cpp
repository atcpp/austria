#include "at_preference.h"
#include "at_log.h"
#include "at_log_taskid.h"
#include "at_unit_test.h"

#include <sstream>
#include <iostream>

typedef
    at::PreferenceManager<>
        ATLOGTEST_PreferenceManager;


// this is now provided in at_preference.h but any specialization will need it
#ifdef FALSE_AT_InstrumentPreferenceMgr
namespace at
{
    // This is used only for instrumented version of the
    // AbstractPreferenceManager - it automatically selects the
    // right Ptr type (the type that registers with the ref list)
    //
    
    //
    // Select an instrumented pointer for NCTV_PreferenceManager
    template <
        typename w_First,
        typename w_Second
    >
    class PtrSelect< ATLOGTEST_PreferenceManager *, w_First, w_Second >
    {
        public:
    
        typedef PtrTraits<
            ATLOGTEST_PreferenceManager *,
            PtrClassRefWrapperInstrumented< ATLOGTEST_PreferenceManager * >
        >  type;
    };
    
    template <
        typename w_First,
        typename w_Second
    >
    class PtrSelect< const ATLOGTEST_PreferenceManager *, w_First, w_Second >
    {
        public:
    
        typedef PtrTraits<
            const ATLOGTEST_PreferenceManager *,
            PtrClassRefWrapperInstrumented< const ATLOGTEST_PreferenceManager * >
        >  type;
    };
}
#endif // AT_InstrumentPreferenceMgr

using namespace at;

typedef
    LogManager<
        LogManagerInputPolicy_Default,
        LogManagerTimePolicy_NA,
        LogManagerTaskIDPolicy_ATTask,
        LogManagerLockPolicy_SingleThreaded,
        LogManagerWriterPolicy_SingleWriter_SameThread< AbstractLogWriter >::t_template
    >
        ATLOGTEST_LogManager;

#undef  AT_LOG_MANAGER_TYPE
#define  AT_LOG_MANAGER_TYPE  ATLOGTEST_LogManager


class LogWriterOutputPolicy_SStream
{

public:

    typedef  std::ostringstream  t_ostream_type;

private:

    t_ostream_type l_ss;

public:

    t_ostream_type & OStream()
    {
        return l_ss;
    }

    void Flush()
    {
    }

    std::string GetOutputString()
    {
        return l_ss.str();
    }

};

typedef
    LogWriter<
        ATLOGTEST_LogManager,
        LogWriterOutputPolicy_SStream
    >
        ATLOGTEST_LogWriter;


AT_TestArea( Log, "Logging tests" );


AT_DefineTest( Log, Log, "Basic Logging test" )
{

    void Run()
    {

        Ptr< ATLOGTEST_PreferenceManager * > l_pm = new ATLOGTEST_PreferenceManager;
        ATLOGTEST_LogManager l_lm;

        {

            ATLOGTEST_LogWriter l_lw( l_lm );

            Preference< int > l_loglevel( "loglevel", ~0, "/FooProgram", l_pm );

            AT_STARTLOG( l_lm, l_loglevel, AT_LOGLEVEL_SEVERE, "Some Description" )

                at_log("Some Variables") << 55 << 123.456 << "Some String";

            AT_ENDLOG

            AT_STARTLOG( l_lm, l_loglevel, AT_LOGLEVEL_SEVERE, "Some Description" )

                at_log("Some Variables") << 55 << 123.456 << "Some String";

            AT_ENDLOG

        }

    }

};

AT_RegisterTest( Log, Log );

