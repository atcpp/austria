/*/
 * This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 * Any unauthorized use of this file is prohibited and will be prosecuted
 * to the full extent of the law.
 */

#include "at_unit_test.h"
#include "at_net.h"
#include "at_net_http.h"
#include "at_net_ipv4.h"
#include "at_net_helpers.h"
#include "at_http_misc.h"

#include <time.h>
#include <list>
#include <locale>

using namespace at;
using namespace std;

#define MAX_NET_ITER 1000

const unsigned int maxTestSeconds = 10;
const TimeInterval maxTestWait (maxTestSeconds*TimeInterval::PerSec);


const unsigned int infiniteTestSeconds = (unsigned int)(~0);
const TimeInterval infiniteTestWait (infiniteTestSeconds);

namespace at
{
    template<
        typename w_element_type
    >
    bool accumulate(
        w_element_type & i_val,
        const w_element_type & i_file_size,
        char i_digit
    );
}

namespace
{
	struct rangeTestItem {
		rangeTestItem() : range_values(), expected_result(false) {;};
		rangeTestItem(const std::string & i_r_value, const bool i_exp_result) { range_values = i_r_value; expected_result = i_exp_result;};

		std::string range_values;
		bool expected_result;

		bool merge(rangeTestItem &to_add) 
		{
			if(range_values.empty())
			{
				range_values = to_add.range_values;
				expected_result = to_add.expected_result;
			}
			else
			{
				range_values += ',' + to_add.range_values;
				expected_result &= to_add.expected_result;
			}
			return true;
		};
	};


    string to_string(Ptr<Buffer*> p)
    {
        return string(">>>") + string(p->begin(), p->end()) + string("<<<");
    }

    struct ConnectionEvent
    {
        enum Type {
            Invalid, ReceiveFailure, SendFailure, SendCompleted,
            DataReady, Closed, ReceivedProperties
        };
        int seqno;
        Type type;
        bool hasProperties;
        bool hasAllProperties;
        const NetError *error;
        Ptr<const Buffer*> const_buffer;
        const char *begin;
        const char *end;
        NetConnection::t_PropList props;
        ConnectionEvent() : type(Invalid),
                            hasProperties(false),
                            hasAllProperties(false),
                            error(0),
                            begin(0),
                            end(0)
        { }

        static const char* TypeToString(Type t)
        {
            switch (t)
            {
            case ReceiveFailure:
                return "ReceiveFailure";
            case SendFailure:
                return "SendFailure";
            case SendCompleted:
                return "SendCompleted";
            case DataReady:
                return "DataReady";
            case Closed:
                return "Closed";
            case ReceivedProperties:
                return "ReceivedProperties";
            case Invalid:
            default:
								std::cout << "XAXA" << std::endl;
                return "Invalid";
            }
        }
    };

    std::ostream& operator<<(std::ostream &os, const ConnectionEvent &ev)
    {
        os << "ConnEv(seq=" << ev.seqno
           << ", type=" << ConnectionEvent::TypeToString(ev.type);
        if (ev.type == ConnectionEvent::ReceivedProperties)
            os << " prop count=" << static_cast<unsigned int>(ev.props.size());
        os << ")";
        return os;
    }

    class ConnectionRecorder :
        public LeadTwinMT_Basic<NetConnectionResponderIf>,
        public PtrTarget_MT
    {
    private:
        int m_seq;
        Ptr<NetConnection*> m_conn;
        std::list<ConnectionEvent> events;
        Ptr<MutexRefCount *>        m_mutex;
        Conditional *               m_newEvent;

    public:
    // accessors
        Ptr<MutexRefCount *> StateMutex(void) { return m_mutex;}
        Conditional & GetConditional(void) { return *m_newEvent;}       // REV: need a releasor, or refcounted conditional
        std::list<ConnectionEvent>  &EventList(void)  { return events;}
        PtrView<NetConnection *> Connection(void) { return m_conn;}


        ConnectionRecorder(PtrDelegate<NetConnection*> conn) :
            m_seq(0),
            m_conn(conn),
            events(),
            m_mutex(new MutexRefCount(Mutex::Recursive)),
            m_newEvent(new Conditional(*m_mutex))
        {
            m_conn->ConnectionNotify(*this);
        }

        ~ConnectionRecorder()
        {
            LeadCancel();
            {
                Lock<Ptr<MutexRefCount *> > l(m_mutex);
                delete m_newEvent;
                m_newEvent = NULL;
            }
            m_mutex = NULL;
        }

        void ReceiveFailure(const NetConnectionError &i_error)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::ReceiveFailure;
            call.error = &i_error;

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        void SendCompleted(PtrDelegate<const Buffer*> i_buffer,
                           const char *i_end)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::SendCompleted;
            call.const_buffer = i_buffer;
            call.end = i_end;

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        void SendFailure(PtrDelegate<const Buffer*> i_buffer,
                         const char *i_begin,
                         const NetConnectionError &i_error)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::SendCompleted;
            call.const_buffer = i_buffer;
            call.begin = i_begin;
            call.error = &i_error;

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }


        void DataReady()
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::DataReady;
#if 0
            bool b;
            call.buffer = NewBuffer();
            do
            {
                call.buffer->reserve(call.buffer->capacity() * 2 + 1);
                b = m_conn->Receive(call.buffer);
            }
            while (b);
#endif
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        void OutOfBandDataReady()
        {
            AT_Assert(false);
        }

        void ReceivedPropertiesNotification(
            bool i_complete, bool i_props_in_queue)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::ReceivedProperties;
            call.hasProperties = i_props_in_queue;
            call.hasAllProperties = i_complete;
            call.props = m_conn->GetReceivedProperties();

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_mutex)
        {
            // catch a copy of the mutex, which is the entire purpose of this override
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
			m_newEvent->PostAll();
			delete m_newEvent;
            m_newEvent = new Conditional(*i_mutex);
            m_mutex = i_mutex;
            return LeadTwinMT_Basic<NetConnectionResponderIf>::LeadAssociate(i_aide, i_mutex);
        }
    };

    AT_TestArea(CSVR,"Content Server");

	AT_DefineTestLevel(HTTPRangeHeaderParser, CSVR, "HTTP Range Header Parser test", UnitTestTraits::TestSuccess, 0)
	{
		void Run(void)
		{
            //prepare the unit tests
			//These test cases are for a 10 byte file
            at::Uint64 fileSize = 10;
			std::vector <rangeTestItem> test_cases;
			std::vector <rangeTestItem> simple_test_cases;
			std::vector <rangeTestItem> combination_test_cases;
			std::vector <rangeTestItem> binary_count_test_cases;
			test_cases.push_back(rangeTestItem("0-0", true));	//First Byte Only
			test_cases.push_back(rangeTestItem("0-9", true));	//Entire File
			test_cases.push_back(rangeTestItem("1-1", true));	//Second Byte Only
			test_cases.push_back(rangeTestItem("0-", true));	//Entire File
			test_cases.push_back(rangeTestItem("5-", true));	//Byte 5 and onward
			test_cases.push_back(rangeTestItem("-10", true));	//Last 10 bytes (In this case) Entire file (Should translate to 0-9)
			test_cases.push_back(rangeTestItem("-5", true));	//Last Five Bytes
			test_cases.push_back(rangeTestItem("0-10", true));	//This should become return range 0-9
			test_cases.push_back(rangeTestItem("10-", false));	//Returns false, the start is too large
			test_cases.push_back(rangeTestItem("-", false));	//This is the entire file.
			test_cases.push_back(rangeTestItem("", false));		//invalid range

			simple_test_cases = test_cases;
			
			std::vector<rangeTestItem>::iterator it1start = test_cases.begin();
			std::vector<rangeTestItem>::iterator it1end = test_cases.end();
			std::vector<rangeTestItem>::iterator it2start = test_cases.begin();
			std::vector<rangeTestItem>::iterator it2end = test_cases.end();

			while(it1start != it1end)
			{
				while(it2start != it2end)
				{
					combination_test_cases.push_back(*it1start);
					combination_test_cases.back().merge(*it2start);
					++it2start;
				}
				it2start = test_cases.begin();
				++it1start;
			}
			int maxBinary = 1 << (simple_test_cases.size());
			for (int i = 1; i  < maxBinary; ++i)
			{
				rangeTestItem rt;
				for (size_t j = 0; j < simple_test_cases.size(); ++j)
				{
					if ((1 << j)  & i)
					{
						rt.merge(simple_test_cases[j]);
					}
				}
				binary_count_test_cases.push_back(rt);
			}


			std::cout << "Current size of Test Vector: " << static_cast<unsigned int>(test_cases.size()) << " elements." << std::endl;
			std::cout << "Size of Combination Test Vector: " << static_cast<unsigned int>(combination_test_cases.size()) << " elements." << std::endl;
			test_cases.insert(test_cases.end(), combination_test_cases.begin(), combination_test_cases.end());
			std::cout << "Current size of Test Vector: " << static_cast<unsigned int>(test_cases.size()) << " elements." << std::endl;
			std::cout << "Size of Binary Test Cases: " << static_cast<unsigned int>(binary_count_test_cases.size()) << " elements." <<std::endl;
			test_cases.insert(test_cases.end(), binary_count_test_cases.begin(), binary_count_test_cases.end());

			it1start = binary_count_test_cases.begin();

			while(it1start != binary_count_test_cases.end())
			{
				std::cout << "Test Case: " << ((*it1start).expected_result ? "(Success) " : "(Failure) ") << (*it1start).range_values << std::endl;
				++it1start;
			}
			std::cout << std::endl;

            // execute the unit tests
            it1start = test_cases.begin();
            it1end = test_cases.end();
            while (it1start != it1end) {
                vector < pair< at::Uint64, at::Uint64 > >  return_ranges;
                AT_TCAssert(it1start->expected_result == ParseHttpRange(it1start->range_values,fileSize,return_ranges),"range validation");
                // AT_TCAssert(validate_return_ranges(return_ranges),"return ranges validation");
                ++it1start;
            }

		}
	};
	AT_RegisterTest(HTTPRangeHeaderParser, CSVR);

	AT_DefineTestLevel(HTTPRangeAccumulator, CSVR, "HTTP Range Accumulator test", UnitTestTraits::TestSuccess, 0)
	{
		void Run(void)
		{
            at::Uint64 file_size( 18446744073709551615uLL );
            at::Uint64 i;

            i = 0;
            AT_TCAssert( at::accumulate( i, file_size, '0' ), "unexpected" );
            AT_TCAssert(0==i,"unexpected");

            i = 0;
            AT_TCAssert( at::accumulate( i, file_size, '9' ), "unexpected" );
            AT_TCAssert(9==i,"unexpected");

            i = 9;
            AT_TCAssert( at::accumulate( i, file_size, '9' ), "unexpected" );
            AT_TCAssert(99==i,"unexpected");

            i = 99;
            AT_TCAssert( at::accumulate( i, file_size, '9' ), "unexpected" );
            AT_TCAssert(999==i,"unexpected");

            i = 8;
            AT_TCAssert( ! at::accumulate( i, file_size, 'x' ), "non numeric" );
            AT_TCAssert(8==i,"no change to input");

            //  18446744073709551615 = maxVal_64bit
            i =  1844674407370955161uLL;
            AT_TCAssert( at::accumulate( i, file_size, '4' ), "1 below" );
            AT_TCAssert(18446744073709551614uLL==i,"1 below");

            i =  1844674407370955161uLL;
            AT_TCAssert( at::accumulate( i, file_size, '5' ), "range limit" );
            AT_TCAssert(18446744073709551615uLL==i,"range limit");

            i =  1844674407370955161uLL;   // overflow on add digit
            AT_TCAssert( at::accumulate( i, file_size, '6' ), "1 above" );
            AT_TCAssert(18446744073709551615uLL==i,"i set equal to file_size");

            i =  1844674407370955162uLL;   // overflow on multiply
            AT_TCAssert( at::accumulate( i, file_size, '0' ), "1 above" );
            AT_TCAssert(18446744073709551615uLL==i,"i set equal to file_size");
        }
    };
	AT_RegisterTest(HTTPRangeAccumulator, CSVR);

    AT_DefineTestLevel(ContentServerBasic, CSVR, "Basic content server test",UnitTestTraits::TestSuccess,5)
		{
			void Run()
			{
				Ptr<ConnectionRecorder *> svrrec = 0;
				ConnectionEvent ev;
				Ptr<Net*> ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
				if(!ip)
				{
					std::cout << "No at::Net object returned from factory" << std::endl;
					AT_Assert(!"No at::Net object returned from factory");
					return;
				}
				Ptr<Net*> http = FactoryRegister<at::Net, DKy>::Get().Create("net_http")();
				if(!http)
				{
					std::cout << "No HTTP object returned from factory" << std::endl;
					AT_Assert(!"No HTTP Object returned from factory");
					return;
				}

				Ptr<IPv4Address*> addr;
				addr = new IPv4Address(127,0,0,1,8080);
				Ptr<NetConnection*> svrconn;
				XConnectLead<ConnectionRecorder> svrConnectLead;

				http->Listen(&svrConnectLead, addr, 0);

				svrConnectLead.Wait(&svrrec, 0, infiniteTestWait); // @@ TODO refactor here to pass svrrec to activity list

				if(svrrec)
				{
					svrconn = svrrec->Connection();
					if(!svrconn)
					{
						AT_Assert(!"svrrec->Connection() failed");
						return;
					}
					else
					{
						
						time_t timeStart = ::time(NULL);
						time_t timeElapsed = 0;
						bool done = false;
		
						do {
							Lock<Ptr<MutexRefCount *> > l(svrrec->StateMutex());
							while (svrrec->EventList().size() == 0)
							{
								if(!svrrec->GetConditional().Wait(infiniteTestWait))
								{
									AT_Assert(!"svrrec->GetConditional().Wait(infiniteTestWait)");
									return;
								}
							}
							while (svrrec->EventList().size())
							{
								ev = svrrec->EventList().front();
								svrrec->EventList().pop_front();
								std::cout << ev << std::endl;
								if (ev.type == ConnectionEvent::ReceivedProperties)
								{
									done = true;
									break;
								}
							}
							timeElapsed = ::time(NULL) - timeStart;
						} while (!done && ((unsigned) timeElapsed < maxTestSeconds));
		
						NetConnection::t_PropList props = svrconn->GetReceivedProperties();
						NetConnection::t_PropList::iterator it = props.begin();
						NetConnection::t_PropList::iterator end = props.end();
		
						int propCount = 0;
						while (it != end)
						{
							NetProperty *p = it->Get();
							HTTPVersionProperty *v  = dynamic_cast<HTTPVersionProperty*>(p);
    						HTTPURIProperty *u      = dynamic_cast<HTTPURIProperty*>(p);
							HTTPHeaderProperty *h   = dynamic_cast<HTTPHeaderProperty*>(p);
							HTTPMethodProperty *m   = dynamic_cast<HTTPMethodProperty*>(p);
		
							if (v)
							{
								std::cout << propCount << ". HTTP Version: " << v->major << "." << v->minor << std::endl;
							}
                            else if (u)
                            {
								std::cout << propCount << ". HTTP URI: " << u->uri << std::endl;
							}
							else if (h)
							{
								if(h->name == "Range")
								{
									std::cout << propCount << ". Special Header: " << h->name << ": " << h->value << std::endl;
									vector< pair< at::Uint64, at::Uint64 > > t_vec;
                                    ParseHttpRange( h->value, at::Uint64( 1000 ), t_vec );
								}
								else
								{
									std::cout << propCount << ". HTTP Header: " << h->name << ": " << h->value << std::endl;
								}
							}
                            else if (m)
							{
								std::cout << propCount << ". HTTP Method: " << m->method << std::endl;
							}
							else
							{
								std::cout << "Unknown." << std::endl;
							}
							++propCount;
							++it;
						}
		
						Ptr<Buffer*> testbuf = NewBuffer();
						testbuf->reserve(52);
			
						timeStart = ::time(NULL);
						timeElapsed = 0;
						props = NetConnection::t_PropList();
						props.push_back(new HTTPHeaderProperty("Content-Length", "26"));
						svrconn->AddOutgoingProperties(props);
						testbuf = NewBuffer();
						testbuf->Append(std::string("abcdefghijklmnopqrstuvwxyz"));
	                    svrconn->Send(testbuf);

						
					}
				}


				svrconn = NULL;
				svrrec = NULL;
				http = NULL;
				ip = NULL;
			}
		};
		AT_RegisterTest(ContentServerBasic, CSVR);

} //namespace
