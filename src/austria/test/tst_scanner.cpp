
#include "at_scanner.h"

#include "at_unit_test.h"

using namespace at;

namespace ScannerTest {

AT_TestArea( Scanner, "Sanner tests" );

AT_DefineTest( Scanner, Scanner, "Basic Scanner test" )
{
    
	void Run()
	{
        typedef std::string         KeyType;
        
        at::SimpleScanner< char, KeyType >   l_scanner;
    
        KeyType         l_collision;

        std::string     names[] = { "ZZ", "YY", "DX" };
        
        l_scanner.AddTerminal( "abcde", names[0], l_collision );
        l_scanner.AddTerminal( "dx_", names[1], l_collision );
        l_scanner.AddTerminal( "xyz", names[2], l_collision );
    
        static const char      l_test[] = "abcde_test_abcdx_xyz";
        static const char      l_unmatched[] = "_test_abc";
    
        at::IteratorTranverser< const char *, char >    l_trav( l_test, l_test + sizeof( l_test ) -1 );
    
        at::SimpleScanner< char, KeyType >::Machine     machine;
    
        KeyType             l_result;
        int                 count = 0;
        int                 ucount = 0;
    
        while (true )
        {
            if ( l_scanner.Traverse( machine, l_trav, l_result ) )
            {
                
                AT_TCAssert( count < CountElements( names ), "Too many matches\n" );
                AT_TCAssert( l_result == names[ count ++ ], "Scanner failed match\n" );
            }
            else
            {
                char            l_ch;
                
                if ( ! machine.GetChar( l_ch ) )
                {
                    if ( ! l_trav.GetChar( l_ch ) )
                    {
                        break;
                    }
                }

                AT_TCAssert( ucount < CountElements( l_unmatched ), "Too many unmatched chars" );
                AT_TCAssert( l_unmatched[ ucount ++ ] == l_ch, "Matched incorrect character" );
                
            }
        }

        AT_TCAssert( count == CountElements( names ), "Incorrect number of elements matched" );
        AT_TCAssert( ucount == CountElements( l_unmatched ) -1, "Incorrect number of unmatched chars" );
    }

};

AT_RegisterTest( Scanner, Scanner );

} // ScannerTest namespace

