
#include "at_dbghelp.h"

#include "at_unit_test.h"

#include <iostream>

namespace {
    
AT_TestArea( DebugHelp, "Debugging helper test" );

#include <iostream>



AT_DefineTest( Debug, DebugHelp, "Debug basic test" )
{
	void Run()
	{
        at::DebugDelay      l_hold( "xx_dbg_hold", true, "xx_trigger" );
        
	}
};

AT_RegisterTest( Debug, DebugHelp );


};
