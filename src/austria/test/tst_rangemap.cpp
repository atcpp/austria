
#include "at_rangemap.h"

#include "at_unit_test.h"

#include <iostream>
#include <vector>
#include <cstdlib>

using namespace at;

namespace RangemapTest {

//


// ======== TestBitMap ================================================
/**
 * TestBitMap is like a range map but the logic is far easier.
 *
 */

template <typename w_RangeType, typename w_RangeTraits=TypeRange<w_RangeType> >
class TestBitMap
{
    public:

    // range type
    typedef w_RangeType                                 t_RangeType;
    typedef w_RangeTraits                               t_RangeTraits;


    // ======== AddRange ==============================================
    /**
     * Add a segment to the range.
     *
     * @param i_begin   The beginning of the range (inclusive)
     * @param i_end     The end of the range (inclusive)
     * @return nothing
     */

    void AddRange( const t_RangeType & i_begin, const t_RangeType & i_end )
    {
        const bool l_less_than( i_end < i_begin );
        
        const t_RangeType & l_begin = ! l_less_than ? i_begin : i_end;
        const t_RangeType & l_end =     l_less_than ? i_begin : i_end;

        CheckSize( l_end );

        for ( unsigned i = l_begin; i <= l_end; ++i )
        {
            m_bitmap[ i ] = true;
        }
    }

    // ======== SubtractRange =========================================
    /**
     * SubtractRange removes the range. (opposite of Add)
     *
     *
     * @param i_begin Beginning of range to subtract
     * @param i_end End of range to subtract
     * @return nothing
     */

    void SubtractRange( const t_RangeType & i_begin, const t_RangeType & i_end )
    {
        const bool l_less_than( i_end < i_begin );
        
        const t_RangeType & l_begin = ! l_less_than ? i_begin : i_end;
        const t_RangeType & l_end =     l_less_than ? i_begin : i_end;

        CheckSize( l_end );
        
        for ( unsigned i = l_begin; i <= l_end; ++i )
        {
            m_bitmap[ i ] = false;
        }
        
    }


    // ======== IsSet =================================================
    /**
     * Checks to see if the position is set
     *
     * @param i_pos
     * @return True if the position is set
     */

    bool IsSet( const t_RangeType & i_pos )
    {
        if ( m_bitmap.size() < std::size_t( i_pos ) )
        {
            return false;
        }
        return m_bitmap[ i_pos ];
    }
    
    
    void CheckSize( const t_RangeType & i_end )
    {
        if ( m_bitmap.size() < std::size_t(i_end + 1) )
        {
            m_bitmap.resize( i_end + 1 );
        }
    }
    
    std::vector<bool>   m_bitmap;
};



// ======== TestBoth ==================================================
/**
 * 
 *
 */

template <typename w_RangeType, typename w_RangeTraits=TypeRange<w_RangeType> >
class TestBoth
{
    public:
    // range type
    typedef w_RangeType                                 t_RangeType;
    typedef w_RangeTraits                               t_RangeTraits;


    // ======== AddRange ==============================================
    /**
     * Add a segment to the range.
     *
     * @param i_begin   The beginning of the range (inclusive)
     * @param i_end     The end of the range (inclusive)
     * @return nothing
     */

    void AddRange( const t_RangeType & i_begin, const t_RangeType & i_end )
    {
        m_rangemap_pre = m_rangemap;
        m_rangemap.AddRange( i_begin, i_end );
        m_bitmap.AddRange( i_begin, i_end );

        Verify( "Add", i_begin, i_end );
    }

    // ======== SubtractRange =========================================
    /**
     * SubtractRange removes the range. (opposite of Add)
     *
     *
     * @param i_begin Beginning of range to subtract
     * @param i_end End of range to subtract
     * @return nothing
     */

    void SubtractRange( const t_RangeType & i_begin, const t_RangeType & i_end )
    {
        m_rangemap_pre = m_rangemap;
        m_rangemap.SubtractRange( i_begin, i_end );
        m_bitmap.SubtractRange( i_begin, i_end );

        Verify( "Sub", i_begin, i_end );
    }

    void Verify( const char * i_op, const t_RangeType & i_begin, const t_RangeType & i_end )
    {
        unsigned l_elems = m_bitmap.m_bitmap.size();

        typename at::RangeMap< w_RangeType, w_RangeTraits >::t_Iterator      l_bound;
        typename at::RangeMap< w_RangeType, w_RangeTraits >::t_Iterator      l_previous;
        bool l_previous_set = false;


        for ( unsigned i = 0; i < (l_elems); ++ i )
        {
            l_bound = m_rangemap.m_map.lower_bound( i );
            
            bool l_rangemap_val = l_bound == m_rangemap.m_map.end() ? false : !( i < l_bound->second );
            bool l_bitmap_val = m_bitmap.IsSet(i);
            bool l_segment_fail = false;

            if ( l_previous_set )
            {
                if ( l_rangemap_val )
                {
                    l_segment_fail = l_previous != l_bound;
                }
            }
            else
            {
            }

            l_previous_set = l_rangemap_val;

            if ( l_rangemap_val && ! l_segment_fail )
            {
                l_previous = l_bound;
            }

            if ( ( l_rangemap_val != l_bitmap_val ) || l_segment_fail )
            {
                if ( l_segment_fail )
                {
                    std::cout << "Segments ( " << l_bound->second << ", " << l_bound->first << " ) and \n";
                    std::cout << "         ( " << l_previous->second << ", " << l_previous->first << " ) and \n";
                }
                std::cout << "Operation = " << i_op << " i_begin = " << i_begin << " i_end = " << i_end << "\n";
                std::cout << "Pre operation ";
                DumpRanges( m_rangemap_pre.m_map );
                std::cout << "Post operation ";
                DumpRanges( m_rangemap.m_map );
                std::cout << "rangemap " << w_RangeType(i) << " - l_rangemap_val = " << l_rangemap_val << ", l_bitmap_val = " << l_bitmap_val << "\n";
            }
            
            AT_TCAssert( m_rangemap.IsSet(i) == m_bitmap.IsSet(i), "Bitmap differs" );
        }
    }

    void DumpRanges()
    {
        DumpRanges( m_rangemap.m_map );
    }


    void DumpRanges( typename at::RangeMap< w_RangeType, w_RangeTraits >::t_Map & l_map )
    {
        typename at::RangeMap< w_RangeType, w_RangeTraits >::t_Iterator      l_iterator;

        for ( l_iterator = l_map.begin(); l_iterator != l_map.end(); ++ l_iterator )
        {
            std::cout << "( " << l_iterator->second << ", " << l_iterator->first << " )";
        }
        std::cout << "\n";
    }



    at::RangeMap< w_RangeType, w_RangeTraits >  m_rangemap;
    at::RangeMap< w_RangeType, w_RangeTraits >  m_rangemap_pre;
    TestBitMap< w_RangeType, w_RangeTraits >    m_bitmap;
};


    

AT_TestArea( RangeMap, "Rangemap object tests" );

AT_DefineTest( RangeMap, RangeMap, "Basic RangeMap test" )
{
    
	void Run()
	{

        {
            TestBoth<unsigned char> l_rm;
    
            l_rm.AddRange( 'a','x' );
            
            l_rm.AddRange( 'A','X' );
            
            l_rm.AddRange( 'z','z' );
            
            l_rm.AddRange( 'Y','Z' );
    
            l_rm.DumpRanges();
        }
        {
            TestBoth<unsigned char> l_rm;
    
            l_rm.AddRange( '0','0' );
            
            l_rm.AddRange( 'a','a' );
            
            l_rm.AddRange( 'c','c' );
            
            l_rm.AddRange( 'h','h' );

            l_rm.AddRange( 'b','g' );
    
            l_rm.DumpRanges();
        }
        {
            TestBoth<unsigned char> l_rm;
    
            l_rm.AddRange( '0','0' );
            
            l_rm.AddRange( 'a','a' );
            
            l_rm.AddRange( 'c','c' );
            
            l_rm.AddRange( 'h','h' );

            l_rm.AddRange( 'b','i' );
    
            l_rm.DumpRanges();
            
            l_rm.AddRange( 'c','c' );
            
            l_rm.DumpRanges();

            l_rm.SubtractRange( 'b','b' );
            l_rm.SubtractRange( 'c','c' );
            l_rm.SubtractRange( 'd','d' );
            l_rm.SubtractRange( 'c','c' );
            
            l_rm.DumpRanges();
        }
        {
            TestBoth<unsigned char> l_rm;
    
            
            l_rm.AddRange( 'a','a' );
            
            l_rm.AddRange( 'c','c' );
            
            l_rm.AddRange( 'h','h' );

            l_rm.AddRange( 'b','i' );

            l_rm.AddRange( '0','0' );
            
            l_rm.SubtractRange( '0','0' );

            l_rm.AddRange( '0', 'i' );
    
            l_rm.SubtractRange( '0','i' );
            
            l_rm.AddRange( 'A','Z' );
            
            l_rm.SubtractRange( 'M','M' );
            
            l_rm.DumpRanges();
        }
    }

};

AT_RegisterTest( RangeMap, RangeMap );


AT_DefineTest( MonteRangeMap, RangeMap, "Basic RangeMap test" )
{
    
	void Run()
	{

        TestBoth<unsigned> l_rm;

        std::srand( 39000 );

        int range = 60;

        for ( int i = 0; i < 10000; ++i )
        {

            unsigned l_begin = std::rand() % range;
            unsigned l_end = std::rand() % range;

            bool operation = ( 2 & std::rand() ) == 0;

            if ( operation )
            {
                l_rm.SubtractRange( l_begin, l_end );
            }
            else
            {
                l_rm.AddRange( l_begin, l_end );
            }
            
        }
    }

};

AT_RegisterTest( MonteRangeMap, RangeMap );


} // RangeMap Test namespace


