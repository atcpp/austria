

#include "at_factory.h"

#include "at_unit_test.h"

using namespace at;

AT_TestArea( ATFactory, "Factory tests" );


class Interface
{

	public:

	virtual const char * Thingy() = 0;
	virtual const char * Thingy1() = 0;
	virtual const char * Thingy2() = 0;
	virtual const char * Thingy3() = 0;

	bool * mark_del;

	Interface()
	  : mark_del( 0 )
	{
	}

	virtual ~Interface()
	{
		if ( mark_del )
		{
			* mark_del = true;
		}
	}

	void MarkPtr( bool * i_mark_del )
	{
		mark_del = i_mark_del;
	}

	virtual int ContructParamCount() = 0;
	
};


AT_DefineTest( Basic, ATFactory, "Basic 1 parameter constructor factory test" )
{
	void Run()
	{

		Interface * ptr =
			FactoryRegister< Interface, DKy, Creator1P< Interface, DKy, const char * > >
				::Get().Create( "ImplementorKEY" )( "contructor param" );

		AT_TCAssert( ptr != 0, "Failed to created ImplementorKEY object" );

		AT_TCAssert( ptr->ContructParamCount() == 1, "Expected parameter count of 1" );
		
		AT_TCAssert( std::string( ptr->Thingy() ) == "contructor param", "Created object does not give right Thingy" );

		bool is_del = false;

		ptr->MarkPtr( & is_del );

		delete ptr;

		AT_TCAssert( is_del, "Expected to be set to true when deleted" );
	}

};
AT_RegisterTest( Basic, ATFactory );


AT_DefineTest( Basic2, ATFactory, "Basic 2 parameter constructor factory test" )
{
	void Run()
	{

		Interface * ptr =
			FactoryRegister< Interface, DKy, Creator2P< Interface, DKy, const char *, const char * > >
				::Get().Create( "ImplementorKEY" )( "contructor param", "param 2" );

		AT_TCAssert( ptr != 0, "Failed to created ImplementorKEY object" );

		AT_TCAssert( ptr->ContructParamCount() == 2, "Expected parameter count of 2" );
		
		AT_TCAssert( std::string( ptr->Thingy() ) == "contructor param", "Created object does not give right Thingy" );
		AT_TCAssert( std::string( ptr->Thingy1() ) == "param 2", "Created object does not give right Thingy1" );

		bool is_del = false;

		ptr->MarkPtr( & is_del );

		delete ptr;

		AT_TCAssert( is_del, "Expected to be set to true when deleted" );
	}

};
AT_RegisterTest( Basic2, ATFactory );


AT_DefineTest( Basic3, ATFactory, "Basic 3 parameter constructor factory test" )
{
	void Run()
	{

		Interface * ptr =
			FactoryRegister< Interface, DKy, Creator3P< Interface, DKy, const char *, const char *, const char * > >
				::Get().Create( "ImplementorKEY" )( "contructor param", "param 2", "param 3" );

		AT_TCAssert( ptr != 0, "Failed to created ImplementorKEY object" );

		AT_TCAssert( ptr->ContructParamCount() == 3, "Expected parameter count of 3" );
		
		AT_TCAssert( std::string( ptr->Thingy() ) == "contructor param", "Created object does not give right Thingy" );
		AT_TCAssert( std::string( ptr->Thingy1() ) == "param 2", "Created object does not give right Thingy1" );
		AT_TCAssert( std::string( ptr->Thingy2() ) == "param 3", "Created object does not give right Thingy2" );

		bool is_del = false;

		ptr->MarkPtr( & is_del );

		delete ptr;

		AT_TCAssert( is_del, "Expected to be set to true when deleted" );
	}

};
AT_RegisterTest( Basic3, ATFactory );


AT_DefineTest( Basic4, ATFactory, "Basic 3 parameter constructor factory test" )
{
	void Run()
	{

		Interface * ptr =
			FactoryRegister< Interface, DKy, Creator4P< Interface, DKy, const char *, const char *, const char *, const char *  > >
				::Get().Create( "ImplementorKEY" )( "contructor param", "param 2", "param 3", "param 4" );

		AT_TCAssert( ptr != 0, "Failed to created ImplementorKEY object" );

		AT_TCAssert( ptr->ContructParamCount() == 3, "Expected parameter count of 3" );
		
		AT_TCAssert( std::string( ptr->Thingy() ) == "contructor param", "Created object does not give right Thingy" );
		AT_TCAssert( std::string( ptr->Thingy1() ) == "param 2", "Created object does not give right Thingy1" );
		AT_TCAssert( std::string( ptr->Thingy2() ) == "param 3", "Created object does not give right Thingy2" );
		AT_TCAssert( std::string( ptr->Thingy3() ) == "param 4", "Created object does not give right Thingy3" );

		bool is_del = false;

		ptr->MarkPtr( & is_del );

		delete ptr;

		AT_TCAssert( is_del, "Expected to be set to true when deleted" );
	}

};
AT_RegisterTest( Basic4, ATFactory );


AT_DefineTest( PIMPL, ATFactory, "Pimpl test" )
{
	void Run()
	{
		Creator0P<Interface, DKy> * factory =
			FactoryRegister< Interface, DKy, Creator0P< Interface, DKy > >
				::Get().Find( "ImplementorKEY" );

		AT_TCAssert( factory != 0, "Failed to find implementor factory" );
		
		char	buffer[ 300 ];
			
		AT_TCAssert( factory->Size() <= static_cast<int>( sizeof( buffer ) ), "Test buffer needs to be bigger\n" );

		Interface * ptr = factory->Create( static_cast<void *>( buffer ) );
		
		AT_TCAssert( std::string( ptr->Thingy() ) == "Empty", "Created object does not give right Thingy" );

		bool is_del = false;

		ptr->MarkPtr( & is_del );

		factory->Destroy( ptr );

		AT_TCAssert( is_del, "Expected to be set to true when deleted" );
		
	}

};


AT_RegisterTest( PIMPL, ATFactory );




class Implementor
  : public Interface
{

	public:

	virtual const char * Thingy()
	{
		return m_str;
	}

	virtual const char * Thingy1()
	{
		return m_str2;
	}

	virtual const char * Thingy2()
	{
		return m_str3;
	}

	virtual const char * Thingy3()
	{
		return m_str4;
	}

	Implementor( const char * str, const char * str2, const char * str3, const char * str4 )
	  : m_str( str ),
		m_str2( str2 ),
		m_str3( str3 ),
		m_str4( str4 ),
		contruction_parameter_count( 3 )
	{
	}

	Implementor( const char * str, const char * str2, const char * str3 )
	  : m_str( str ),
		m_str2( str2 ),
		m_str3( str3 ),
		m_str4( ),
		contruction_parameter_count( 3 )
	{
	}

	Implementor( const char * str, const char * str2 )
	  : m_str( str ),
		m_str2( str2 ),
		m_str3( ),
		m_str4( ),
		contruction_parameter_count( 2 )
	{
	}

	Implementor( const char * str )
	  : m_str( str ),
		m_str2( ),
		m_str3( ),
		m_str4( ),
		contruction_parameter_count( 1 )
	{
	}

	Implementor()
	  : m_str( "Empty" ),
		m_str2( ),
		m_str3( ),
		m_str4( ),
		contruction_parameter_count( 0 )
	{
	}

	virtual int ContructParamCount()
	{
		return contruction_parameter_count;
	}

	int contruction_parameter_count;

	const char * m_str;
	const char * m_str2;
	const char * m_str3;
	const char * m_str4;
};


AT_MakeFactory1P( "ImplementorKEY", Implementor, Interface, DKy, const char * );
AT_MakeFactory2P( "ImplementorKEY", Implementor, Interface, DKy, const char *, const char * );
AT_MakeFactory3P( "ImplementorKEY", Implementor, Interface, DKy, const char *, const char *, const char * );
AT_MakeFactory4P( "ImplementorKEY", Implementor, Interface, DKy, const char *, const char *, const char *, const char * );

AT_MakeFactory0P( "ImplementorKEY", Implementor, Interface, DKy );


#define HardLink(A) \
    A(Implementor,1)        \
    A(Implementor,2)

#include "at_factory_link.h"


