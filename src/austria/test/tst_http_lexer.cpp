
#include "at_http_lexer.h"

#include "at_unit_test.h"

//using namespace at;

AT_TestArea( HTTPLexerArea, "Buffer area" );

#include <iostream>


namespace {


// ======== LexerTest ============================================
/**
 * LexerTest performs all the tests for a buffer
 *
 *
 */

struct TestStructX {
    char    buf[ 20 ];
};

void LexerTest(
    const char      * i_key
) {

    at::Ptr< at::Pool * > l_pool = at::FactoryRegister< at::Pool, at::DKy >::Get().Create( i_key )();

    AT_TCAssert( l_pool, "Failed to create pool" );

    at::Ptr< at::Buffer * > l_buffer = l_pool->Create();
    
    AT_TCAssert( l_buffer, "Failed to create buffer" );

    // test request
    {
        std::string l_str = "GET / HTTP/1.0\r\nHeader-Name: HeaderValue\r\n\r\nREQUEST DATA";
    
        l_buffer->Set( l_str );
    
        at::HttpRequestLexer::t_MapType     l_map;
        AT_TCAssert( at::HttpRequestLexer::Parse( l_map, l_buffer ) == at::HttpRequestLexer::Success, "Failed to parse HTTP header" );
    
        std::cout << "l_map[ \"Header-Name\" ] == \"" << l_map[ "Header-Name" ] << "\"\n";
    
        AT_TCAssert( l_map[ at::HttpRequestLexer::s_HTTPVersionStrKey ] == "1.0", "Version string error expected 1.0" );
        AT_TCAssert( l_map[ at::HttpRequestLexer::s_MethodKey ] == "GET", "Method string error expected GET" );
        AT_TCAssert( l_map[ at::HttpRequestLexer::s_RequestURIKey ] == "/", "URI string error expected /" );
        AT_TCAssert( l_map[ "Header-Name" ] == "HeaderValue", "Header-Name string error expected HeaderValue" );
        AT_TCAssert( l_buffer->Region().String() == "REQUEST DATA", "The string should have been truncated" );
    }

    // test response
    {
        std::string l_str = "HTTP/1.2 200 OK\r\nHeader-Name: RESPHeaderValue\r\n\r\nRESPONSE DATA";
    
        l_buffer->Set( l_str );
    
        at::HttpResponseLexer::t_MapType     l_map;
        AT_TCAssert( at::HttpResponseLexer::Parse( l_map, l_buffer ) == at::HttpResponseLexer::Success, "Failed to parse HTTP header" );
    
        std::cout << "l_map[ \"Header-Name\" ] == \"" << l_map[ "Header-Name" ] << "\"\n";
    
        AT_TCAssert( l_map[ at::HttpResponseLexer::s_HTTPVersionStrKey ] == "1.2", "Version string error expected 1.0" );
        AT_TCAssert( l_map[ at::HttpResponseLexer::s_StatusCodeKey ] == "200", "Method string error expected GET" );
        AT_TCAssert( l_map[ at::HttpResponseLexer::s_StatusTextKey ] == "OK", "URI string error expected /" );
        AT_TCAssert( l_map[ "Header-Name" ] == "RESPHeaderValue", "Header-Name string error expected HeaderValue" );
        AT_TCAssert( l_buffer->Region().String() == "RESPONSE DATA", "The string should have been truncated" );
    }

    
}



AT_DefineTest( HTTPLexer, HTTPLexerArea, "A basic buffer test" )
{
	void Run()
	{
        LexerTest( "Basic" );
	}
};

AT_RegisterTest( HTTPLexer, HTTPLexerArea );


} // anon namespace

