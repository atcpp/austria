/**
 * Test lifetime
 *
 */

#include "at_lifetime_basic.h"
#include "at_lifetime_mt.h"
#include "at_lifetime_instr.h"

#include "at_unit_test.h"
#include "at_atomic.h"


namespace at
{

AT_TestArea( Life, "Lifetime tests" );

//PtrTarget_InstrMT type object


// To use the instrumented type, all the classes that use this
// type must use the instrumented PtrSelect stuff.

class InstrumentedTestObject;

//
// Select a Medusa Traits class for ActivityListOwner
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< InstrumentedTestObject *, w_First, w_Second >
{
    public:

    typedef PtrTraits<
        InstrumentedTestObject *,
        PtrClassRefWrapperInstrumented< InstrumentedTestObject * >
    >  type;
};

//
// Select a Medusa Traits class
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< const InstrumentedTestObject *, w_First, w_Second >
{
    public:

    typedef PtrTraits<
        const InstrumentedTestObject *,
        PtrClassRefWrapperInstrumented< const InstrumentedTestObject * >
    >  type;
};


class InstrumentedTestObject
  : public PtrTarget_InstrMT
{
    public:

    int                 m_val;

    InstrumentedTestObject( int i_val )
      : m_val( i_val )
    {
    }

    ~InstrumentedTestObject()
    {
        std::cerr << "InstrumentedTestObject destructed " << m_val << "\n";
    }

};



AT_DefineTest( Instrumented, Life, "Instrumented pointer test" )
{

	void Run()
	{
        {
            
            InstrumentedTestObject * l_ap = new InstrumentedTestObject( 4 );
            InstrumentedTestObject * l_bp = new InstrumentedTestObject( 5 );
            InstrumentedTestObject * l_cp = new InstrumentedTestObject( 6 );
            
            at::Ptr< InstrumentedTestObject * > l_a = l_ap;
            at::Ptr< InstrumentedTestObject * > l_b = l_bp;
            at::Ptr< InstrumentedTestObject * > l_c = l_cp;
    
        }
        
        {
            
            InstrumentedTestObject * l_ap = new InstrumentedTestObject( 1 );
            InstrumentedTestObject * l_bp = new InstrumentedTestObject( 2 );
            InstrumentedTestObject * l_cp = new InstrumentedTestObject( 3 );
            
            at::Ptr< InstrumentedTestObject * > l_a = l_ap;
            at::PtrDelegate< InstrumentedTestObject * > l_b = l_bp;
            at::PtrView< InstrumentedTestObject * > l_c = l_cp;
    
            PrintPointerOwners( std::cout, * l_a );
            PrintPointerOwners( std::cout, * l_b );
            PrintPointerOwners( std::cout, * l_c );
    
            l_a = l_c;
            l_b = l_a;
    
            PrintPointerOwners( std::cout, * l_a );
    
            l_c = 0;
            l_b = 0;
            l_a = 0;
        }
        
	}

};


AT_RegisterTest( Instrumented, Life );



// ======== PtrStyle ==================================================
/**
 * PtrStyle defines a style of reference counted object that is
 * more natural than COM.
 */

class PtrStyle_TEST
  : public PtrStyle
{
    public:


    // ======== ZeroReferenceAction ===================================
    // override this

    template <typename w_type>
    inline static void ZeroReferenceAction( w_type i_obj )
    {
        // no deleting - this is a test
    }
    
    enum {
        DeleteCheck = 1
    };
};

/**
 * This is a test reference counting class for use
 * as part of the unit test.  The reference count
 * is public and can be tested.
 */

class AT_TEST_LifeControlExp
  : public PtrTarget_Generic<int, PtrStyle_TEST>
{

	public:

// The MS compiler does not like this which is what it should be ...
//    using PtrTarget_Generic<int, PtrStyle_TEST>::m_ref_count;

	int				& m_ref_count;

	AT_TEST_LifeControlExp()
      : m_ref_count( PtrTarget_Generic<int, PtrStyle_TEST>::m_ref_count )
	{
	}

	AT_TEST_LifeControlExp * Reset()
	{
		m_ref_count = t_ptr_style::InitCount;

		return this;
	}

    static const bool DoingDeleteCheck = DoDeleteCheck;

};

}; // namespace at

using namespace at;


typedef ::at::AT_TEST_LifeControlExp  x_LifeControlExp;

const int DeletedRefCount =
    ( ::at::AT_TEST_LifeControlExp::DoingDeleteCheck ) ? -1 : 0;





class x_LifeControlExp_Derived : public x_LifeControlExp
{
	public:
	
	int		v;

	x_LifeControlExp_Derived()
	  : v( 0 )
	{
	}

	~x_LifeControlExp_Derived()
	{
		v = 1;
	}
};



class LifeTimerTester
  : public LifeControl_Basic
{
	public:
	
	virtual ~LifeTimerTester()
	{
		* m_ptester = true;
	}

	LifeTimerTester( bool & o_tester_bool )
	  : m_ptester( & o_tester_bool )
	{
		o_tester_bool = false;
	}

	bool						* m_ptester;
};

AT_DefineTest( LifeLine, Life, "LifeLine test" )
{

	void Run()
	{

		bool											l_deleted = false;
		bool											l_deleted2 = false;

		Ptr< LifeControl_Basic * >			l_test;

		Ptr< LifeControl_Basic * >			l_test_null;

		l_test = new LifeTimerTester( l_deleted );

		AT_TCAssert( &( * l_test ) == l_test.Get(), "Address operator test failed" );

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		PtrDelegate< LifeControl_Basic * > l_line = l_test;

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_line = l_test_null;

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_test = l_test_null;

		AT_TCAssert( l_deleted == true, "Expected l_deleted to be true" );

		l_line = new LifeTimerTester( l_deleted );

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_test = l_line;

        AT_TCAssert( l_deleted == false, "Expected l_deleted to be true" );

		l_line = l_test_null;

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_test = l_test_null;

		AT_TCAssert( l_deleted == true, "Expected l_deleted to be true" );

		l_line = new LifeTimerTester( l_deleted );
		l_test = new LifeTimerTester( l_deleted2 );

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );
		AT_TCAssert( l_deleted2 == false, "Expected l_deleted2 to be false" );

		l_line = l_test;

		AT_TCAssert( l_deleted == true, "Expected l_deleted to be true" );
		AT_TCAssert( l_deleted2 == false, "Expected l_deleted to be false" );

		l_line = l_test_null;

		AT_TCAssert( l_deleted2 == false, "Expected l_deleted2 to be false" );

		l_test = l_test_null;

		AT_TCAssert( l_deleted2 == true, "Expected l_deleted2 to be true" );

		
	}
	
};


AT_RegisterTest( LifeLine, Life );

AT_DefineTest( LifeView, Life, "LifeView test" )
{

	void Run()
	{

		x_LifeControlExp				l_test_obj1;
		x_LifeControlExp                l_test_obj2;


		Ptr< x_LifeControlExp * >			l_test = & l_test_obj1;

		AT_TCAssert( l_test->m_ref_count == 1, "ref count to be 1" );

		
		Ptr< x_LifeControlExp * >			l_test_null;

		
		PtrDelegate< x_LifeControlExp * > l_line = l_test;
		
		AT_TCAssert( l_test->m_ref_count == 2, "ref count to be 2" );
		

		PtrView< x_LifeControlExp * > l_view = l_line;

		AT_TCAssert( l_test->m_ref_count == 2, "ref count to be 2" );

		
		l_view = l_line;
		
		AT_TCAssert( l_view->m_ref_count == 2, "ref count to be 2" );
		
		
		PtrView< x_LifeControlExp * > l_view2 = l_test;

		AT_TCAssert( l_test->m_ref_count == 2, "ref count to be 2" );

		
		l_test = & l_test_obj2;

		AT_TCAssert( l_test->m_ref_count == 1, "ref count to be 1" );
		AT_TCAssert( l_line->m_ref_count == 1, "ref count to be 1" );


		PtrView< x_LifeControlExp * > l_view3 = l_test;

		AT_TCAssert( l_view3->m_ref_count == 1, "ref count to be 1" );


		l_line = l_view3;

		AT_TCAssert( l_view3->m_ref_count == 2, "ref count to be 2" );
        
		AT_TCAssert( l_view->m_ref_count == DeletedRefCount, "ref count to be DeletedRefCount" );
		
	}
	
};


AT_RegisterTest( LifeView, Life );


AT_DefineTest( BasicTest, Life, "Lifetime basic test" )
{

	void Run()
	{

		bool											l_deleted;

		Ptr< LifeControl_Basic * >			l_test;
		Ptr< LifeControl_Basic * >			l_test2;

		l_test = new LifeTimerTester( l_deleted );

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		PtrView< LifeControl_Basic * > l_view = l_test;

		PtrDelegate< LifeControl_Basic * > l_line = l_test;

		l_test = l_view;

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		AT_TCAssert( Ptr< LifeControl_Basic * >( l_test2 ) == 0, "l_test failed the self assignment test" );

		l_test2 = l_test;

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_test = l_test2;

		AT_TCAssert( l_test == l_test2, "Expect to be equal" );
		AT_TCAssert( l_test >= l_test2, "Expect to be equal" );
		AT_TCAssert( l_test <= l_test2, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_test2 ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_test2 ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_test2 ), "Expect to be equal" );

		AT_TCAssert( l_test == l_view, "Expect to be equal" );
		AT_TCAssert( l_test >= l_view, "Expect to be equal" );
		AT_TCAssert( l_test <= l_view, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_view ), "Expect to be equal" );

		AT_TCAssert( l_test == l_line, "Expect to be equal" );
		AT_TCAssert( l_test >= l_line, "Expect to be equal" );
		AT_TCAssert( l_test <= l_line, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_line ), "Expect to be equal" );

		AT_TCAssert( l_view == l_test, "Expect to be equal" );
		AT_TCAssert( l_view >= l_test, "Expect to be equal" );
		AT_TCAssert( l_view <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_view != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_view > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_view < l_test ), "Expect to be equal" );

		AT_TCAssert( l_line == l_test, "Expect to be equal" );
		AT_TCAssert( l_line >= l_test, "Expect to be equal" );
		AT_TCAssert( l_line <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_line != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_line > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_line < l_test ), "Expect to be equal" );

		AT_TCAssert( l_line == l_view, "Expect to be equal" );
		AT_TCAssert( l_line >= l_view, "Expect to be equal" );
		AT_TCAssert( l_line <= l_view, "Expect to be equal" );
		AT_TCAssert( !( l_line != l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_line > l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_line < l_view ), "Expect to be equal" );

		AT_TCAssert( l_view == l_line, "Expect to be equal" );
		AT_TCAssert( l_view >= l_line, "Expect to be equal" );
		AT_TCAssert( l_view <= l_line, "Expect to be equal" );
		AT_TCAssert( !( l_view != l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_view > l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_view < l_line ), "Expect to be equal" );

		l_test = l_line;
		
		l_test = 0;

		AT_TCAssert( !l_test, "Expect conversion to bool to be false." );
		
		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_test2 = 0;

		AT_TCAssert( l_deleted == true, "Expected object to be deleted" );

		//
		// now 
		
		Ptr< LifeTimerTester * >				l_tester;
		Ptr< LifeTimerTester * >				l_tester2;

		LifeTimerTester			* lta = new LifeTimerTester( l_deleted );
		LifeTimerTester			* ltb = new LifeTimerTester( l_deleted );

		if ( lta > ltb )
		{
			LifeTimerTester * ltc = lta;
			lta = ltb;
			ltb = ltc;
		}

		l_tester = lta;
		l_tester2 = ltb;

		l_test = l_tester;
		l_test2 = l_tester2;

		l_tester->AddRef();
		int countin = l_tester->Release();		

		int countafter;
		
		{
			PtrDelegate< LifeControl_Basic * > l_line_leak = l_tester;

			l_tester->AddRef();
			countafter = l_tester->Release();
		}

		l_tester->AddRef();
		int countout = l_tester->Release();

		AT_TCAssert( countin + 1 == countafter, "Expected reference count to have incremented" );
		AT_TCAssert( countin == countout, "Expected reference count to be same." );

		AT_TCAssert( ! ( l_test == l_tester2 ), "Expect not equal" );
		AT_TCAssert( ! ( l_test >= l_tester2 ), "Expect less than" );
		AT_TCAssert( l_test <= l_tester2, "Expect less than" );
		AT_TCAssert( l_test != l_tester2, "Expect not equal" );
		AT_TCAssert( !( l_test > l_tester2 ), "Expect less than" );
		AT_TCAssert( l_test < l_tester2, "Expect less than" );

		l_view = l_test;
		l_line = l_test;
		
		PtrView< LifeTimerTester * > l_viewer = l_tester;
		PtrDelegate< LifeTimerTester * > l_lineer = l_tester;

		AT_TCAssert( l_test == l_viewer, "Expect to be equal" );
		AT_TCAssert( l_test >= l_viewer, "Expect to be equal" );
		AT_TCAssert( l_test <= l_viewer, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_viewer ), "Expect to be equal" );

		AT_TCAssert( l_test == l_lineer, "Expect to be equal" );
		AT_TCAssert( l_test >= l_lineer, "Expect to be equal" );
		AT_TCAssert( l_test <= l_lineer, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_lineer ), "Expect to be equal" );

		AT_TCAssert( l_viewer == l_test, "Expect to be equal" );
		AT_TCAssert( l_viewer >= l_test, "Expect to be equal" );
		AT_TCAssert( l_viewer <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_viewer != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer < l_test ), "Expect to be equal" );

		AT_TCAssert( l_lineer == l_test, "Expect to be equal" );
		AT_TCAssert( l_lineer >= l_test, "Expect to be equal" );
		AT_TCAssert( l_lineer <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_lineer != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer < l_test ), "Expect to be equal" );

		AT_TCAssert( l_lineer == l_viewer, "Expect to be equal" );
		AT_TCAssert( l_lineer >= l_viewer, "Expect to be equal" );
		AT_TCAssert( l_lineer <= l_viewer, "Expect to be equal" );
		AT_TCAssert( !( l_lineer != l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer > l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer < l_viewer ), "Expect to be equal" );

		AT_TCAssert( l_viewer == l_lineer, "Expect to be equal" );
		AT_TCAssert( l_viewer >= l_lineer, "Expect to be equal" );
		AT_TCAssert( l_viewer <= l_lineer, "Expect to be equal" );
		AT_TCAssert( !( l_viewer != l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer > l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer < l_lineer ), "Expect to be equal" );


		l_tester = l_lineer;
		l_test = l_line;
	}
	
};

AT_RegisterTest( BasicTest, Life );

/**
 * Constructor test
 */
template <
	typename w_dev_t,
	typename w_lifeX,
	typename w_lifeY,
	int count1,
	int count2,
	int count3,
	int count4,
	int county
>
struct LifeConstruct
{
    public:

    static const int s_count1 = count1;
    static const int s_count2 = count2;
    static const int s_count3 = count3;
    static const int s_count4 = count4;
    static const int s_county = county;

    LifeConstruct()
    {
		AssignTest();
		AssignTest2();
		ComparisonTest();
		BoolTest();
    }

	void ConstructTest()
	{
		w_dev_t	obj1[1];

		{
			w_lifeX x = obj1;
			
			AT_TCAssert( obj1->m_ref_count == s_count1, "Refcount problem - count 1" );

			{
				w_lifeY y = x;

				AT_TCAssert( obj1->m_ref_count == s_count2, "Refcount problem - count 2" );
			}

			AT_TCAssert( obj1->m_ref_count == s_count3, "Refcount problem - count 3" );

		}

		AT_TCAssert( obj1->m_ref_count == s_count4, "Refcount problem - count 4" );

	}

	void AssignTest()
	{
		w_dev_t	obj1[1];

		{
			w_lifeX x;

			x = obj1;
                
			AT_TCAssert( obj1->m_ref_count == s_count1, "Refcount problem - count 1" );

			{
				w_dev_t	obj2[1];
				
				w_lifeY y = obj2;

				y = x;

				AT_TCAssert( obj2->m_ref_count == s_county, "Refcount problem - count y" );
                
				AT_TCAssert( obj1->m_ref_count == s_count2, "Refcount problem - count 2" );
			}

			AT_TCAssert( obj1->m_ref_count == s_count3, "Refcount problem - count 3" );

		}

		AT_TCAssert( obj1->m_ref_count == s_count4, "Refcount problem - count 4" );
	}
	
	void AssignTest2()
	{
		w_dev_t	obj1[1];

		{
			w_lifeX x;

			x = obj1;
			
			AT_TCAssert( obj1->m_ref_count == s_count1, "Refcount problem - count 1" );

			{
				
				w_lifeY y;

				y = x;

				AT_TCAssert( obj1->m_ref_count == s_count2, "Refcount problem - count 2" );
			}

			AT_TCAssert( obj1->m_ref_count == s_count3, "Refcount problem - count 3" );

		}

		AT_TCAssert( obj1->m_ref_count == s_count4, "Refcount problem - count 4" );
	}


	void ComparisonTest()
	{
		w_dev_t	obj1[1];
		w_dev_t	obj2[1];

		bool	compare = obj1 < obj2;


		w_lifeX x = obj1;

		w_lifeY y = obj2;

		AT_TCAssert( ( x < y ) == compare, "Comparison failed" );
		AT_TCAssert( ( x <= y ) == compare, "Comparison failed" );

		AT_TCAssert( ( x > y ) != compare, "Comparison failed" );
		AT_TCAssert( ( x >= y ) != compare, "Comparison failed" );

		y = obj1;

		AT_TCAssert( ( x < y ) == false, "Comparison failed" );
		AT_TCAssert( ( x <= y ) == true, "Comparison failed" );
		AT_TCAssert( ( x == y ) == true, "Comparison failed" );
		
		AT_TCAssert( ( x > y ) == false, "Comparison failed" );
		AT_TCAssert( ( x >= y ) == true, "Comparison failed" );

        obj2->Reset();
		x = obj2;
		
		AT_TCAssert( ( x < y ) != compare, "Comparison failed" );
		AT_TCAssert( ( x <= y ) != compare, "Comparison failed" );

		AT_TCAssert( ( x > y ) == compare, "Comparison failed" );
		AT_TCAssert( ( x >= y ) == compare, "Comparison failed" );

	}
	
	void BoolTest()
	{
		w_dev_t	obj1[1];
		w_dev_t	obj2[1];

		w_lifeX x = obj1;

		w_lifeY y = obj2;

		int	val = 0;

		if ( x )
		{
			val = 1;
		}

		AT_TCAssert( val == 1, "Expecting val to be 1" );

		if ( x && y )
		{
			val = 2;
		}
		
		AT_TCAssert( val == 2, "Expecting val to be 2" );

		AT_TCAssert( ! x == false, "Expecting ! x to be false" );

		x = 0;
		val = 0;
		
		if ( x )
		{
			val = 3;
		}

		AT_TCAssert( val == 0, "Expecting val to be 0" );

		if ( x && y )
		{
			val = 4;
		}
		
		AT_TCAssert( val == 0, "Expecting val to be 0 again" );

		AT_TCAssert( ! x == true, "Expecting ! x to be true" );
		
		
	}
	
};


AT_DefineTest( FullTest, Life, "Test all common functions" )
{

	void Run()
	{
        enum
        {
            V = PtrStyle_TEST::InitCount,
            D = DeletedRefCount
        };

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp_Derived * >, Ptr< x_LifeControlExp_Derived * >,
            1, 2, 1, D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp_Derived * >, Ptr< x_LifeControlExp_Derived * >,
            V, 1+V, V?V:D, V?V:D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp_Derived * >, Ptr< x_LifeControlExp_Derived * >,
            1, 1, D, D, D > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp_Derived * >, PtrView< x_LifeControlExp_Derived * >,
            1, 1, 1, D, V > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp_Derived * >, PtrView< x_LifeControlExp_Derived * >,
            V, V, V, V, V > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp_Derived * >, PtrView< x_LifeControlExp_Derived * >,
            1, 1, 1, D, V > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp_Derived * >, PtrDelegate< x_LifeControlExp_Derived * >,
            1, 2, 1, D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp_Derived * >, PtrDelegate< x_LifeControlExp_Derived * >,
            V, 1+V, V?V:D, V?V:D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp_Derived * >, PtrDelegate< x_LifeControlExp_Derived * >,
            1, 1, D, D, D > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp_Derived * >, Ptr< x_LifeControlExp * >,
            1, 2, 1, D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp_Derived * >, Ptr< x_LifeControlExp * >,
            V, 1+V, V?V:D, V?V:D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp_Derived * >, Ptr< x_LifeControlExp * >,
            1, 1, D, D, D > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp_Derived * >, PtrView< x_LifeControlExp * >,
            1, 1, 1, D, V > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp_Derived * >, PtrView< x_LifeControlExp * >,
            V, V, V, V, V > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp_Derived * >, PtrView< x_LifeControlExp * >,
            1, 1, 1, D, V > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp_Derived * >, PtrDelegate< x_LifeControlExp * >,
            1, 2, 1, D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp_Derived * >, PtrDelegate< x_LifeControlExp * >,
            V, 1+V, V?V:D, V?V:D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp_Derived * >, PtrDelegate< x_LifeControlExp * >,
            1, 1, D, D, D > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp * >, Ptr< x_LifeControlExp * >,
            1, 2, 1, D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp * >, Ptr< x_LifeControlExp * >,
            V, 1+V, V?V:D, V?V:D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp * >, Ptr< x_LifeControlExp * >,
            1, 1, D, D, D > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp * >, PtrView< x_LifeControlExp * >,
            1, 1, 1, D, V > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp * >, PtrView< x_LifeControlExp * >,
            V, V, V, V, V > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp * >, PtrView< x_LifeControlExp * >,
            1, 1, 1, D, V > a; }

		{ LifeConstruct< x_LifeControlExp_Derived, Ptr< x_LifeControlExp * >, PtrDelegate< x_LifeControlExp * >,
            1, 2, 1, D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrView< x_LifeControlExp * >, PtrDelegate< x_LifeControlExp * >,
            V, 1+V, V?V:D, V?V:D, D > a; }
		{ LifeConstruct< x_LifeControlExp_Derived, PtrDelegate< x_LifeControlExp * >, PtrDelegate< x_LifeControlExp * >,
            1, 1, D, D, D > a; }

	}

};


AT_RegisterTest( FullTest, Life );



class MTLifeTimerTester
  : public PtrTarget_MT
{
	public:
	
	virtual ~MTLifeTimerTester()
	{
		* m_ptester = true;
	}

	MTLifeTimerTester( bool & o_tester_bool )
	  : m_ptester( & o_tester_bool )
	{
		o_tester_bool = false;
	}

	bool						* m_ptester;
};


AT_DefineTest( MTTest, Life, "Lifetime basic MT test" )
{

	void Run()
	{

		bool											l_deleted;

		Ptr< PtrTarget_MT * >			l_test;
		Ptr< PtrTarget_MT * >			l_test2;

		l_test = new MTLifeTimerTester( l_deleted );

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		PtrView< PtrTarget_MT * > l_view = l_test;

		PtrDelegate< PtrTarget_MT * > l_line = l_test;

		l_test = l_view;

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		AT_TCAssert( Ptr< PtrTarget_MT * >( l_test2 ) == 0, "l_test failed the self assignment test" );

		l_test2 = l_test;

		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_test = l_test2;

		AT_TCAssert( l_test == l_test2, "Expect to be equal" );
		AT_TCAssert( l_test >= l_test2, "Expect to be equal" );
		AT_TCAssert( l_test <= l_test2, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_test2 ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_test2 ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_test2 ), "Expect to be equal" );

		AT_TCAssert( l_test == l_view, "Expect to be equal" );
		AT_TCAssert( l_test >= l_view, "Expect to be equal" );
		AT_TCAssert( l_test <= l_view, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_view ), "Expect to be equal" );

		AT_TCAssert( l_test == l_line, "Expect to be equal" );
		AT_TCAssert( l_test >= l_line, "Expect to be equal" );
		AT_TCAssert( l_test <= l_line, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_line ), "Expect to be equal" );

		AT_TCAssert( l_view == l_test, "Expect to be equal" );
		AT_TCAssert( l_view >= l_test, "Expect to be equal" );
		AT_TCAssert( l_view <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_view != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_view > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_view < l_test ), "Expect to be equal" );

		AT_TCAssert( l_line == l_test, "Expect to be equal" );
		AT_TCAssert( l_line >= l_test, "Expect to be equal" );
		AT_TCAssert( l_line <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_line != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_line > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_line < l_test ), "Expect to be equal" );

		AT_TCAssert( l_line == l_view, "Expect to be equal" );
		AT_TCAssert( l_line >= l_view, "Expect to be equal" );
		AT_TCAssert( l_line <= l_view, "Expect to be equal" );
		AT_TCAssert( !( l_line != l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_line > l_view ), "Expect to be equal" );
		AT_TCAssert( !( l_line < l_view ), "Expect to be equal" );

		AT_TCAssert( l_view == l_line, "Expect to be equal" );
		AT_TCAssert( l_view >= l_line, "Expect to be equal" );
		AT_TCAssert( l_view <= l_line, "Expect to be equal" );
		AT_TCAssert( !( l_view != l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_view > l_line ), "Expect to be equal" );
		AT_TCAssert( !( l_view < l_line ), "Expect to be equal" );

		l_test = l_line;
		
		l_test = 0;

		AT_TCAssert( !l_test, "Expect conversion to bool to be false." );
		
		AT_TCAssert( l_deleted == false, "Expected l_deleted to be false" );

		l_test2 = 0;

		AT_TCAssert( l_deleted == true, "Expected object to be deleted" );

		//
		// now 
		
		Ptr< MTLifeTimerTester * >				l_tester;
		Ptr< MTLifeTimerTester * >				l_tester2;

		MTLifeTimerTester			* lta = new MTLifeTimerTester( l_deleted );
		MTLifeTimerTester			* ltb = new MTLifeTimerTester( l_deleted );

		if ( lta > ltb )
		{
			MTLifeTimerTester * ltc = lta;
			lta = ltb;
			ltb = ltc;
		}

		l_tester = lta;
		l_tester2 = ltb;

		l_test = l_tester;
		l_test2 = l_tester2;

		l_tester->AddRef();
		int countin = l_tester->Release();		

		int countafter;
		
		{
			PtrDelegate< PtrTarget_MT * > l_line_leak = l_tester;

			l_tester->AddRef();
			countafter = l_tester->Release();
		}

		l_tester->AddRef();
		int countout = l_tester->Release();

		AT_TCAssert( countin + 1 == countafter, "Expected reference count to have incremented" );
		AT_TCAssert( countin == countout, "Expected reference count to be same." );

		AT_TCAssert( ! ( l_test == l_tester2 ), "Expect not equal" );
		AT_TCAssert( ! ( l_test >= l_tester2 ), "Expect less than" );
		AT_TCAssert( l_test <= l_tester2, "Expect less than" );
		AT_TCAssert( l_test != l_tester2, "Expect not equal" );
		AT_TCAssert( !( l_test > l_tester2 ), "Expect less than" );
		AT_TCAssert( l_test < l_tester2, "Expect less than" );

		l_view = l_test;
		l_line = l_test;
		
		PtrView< MTLifeTimerTester * > l_viewer = l_tester;
		PtrDelegate< MTLifeTimerTester * > l_lineer = l_tester;

		AT_TCAssert( l_test == l_viewer, "Expect to be equal" );
		AT_TCAssert( l_test >= l_viewer, "Expect to be equal" );
		AT_TCAssert( l_test <= l_viewer, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_viewer ), "Expect to be equal" );

		AT_TCAssert( l_test == l_lineer, "Expect to be equal" );
		AT_TCAssert( l_test >= l_lineer, "Expect to be equal" );
		AT_TCAssert( l_test <= l_lineer, "Expect to be equal" );
		AT_TCAssert( !( l_test != l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_test > l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_test < l_lineer ), "Expect to be equal" );

		AT_TCAssert( l_viewer == l_test, "Expect to be equal" );
		AT_TCAssert( l_viewer >= l_test, "Expect to be equal" );
		AT_TCAssert( l_viewer <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_viewer != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer < l_test ), "Expect to be equal" );

		AT_TCAssert( l_lineer == l_test, "Expect to be equal" );
		AT_TCAssert( l_lineer >= l_test, "Expect to be equal" );
		AT_TCAssert( l_lineer <= l_test, "Expect to be equal" );
		AT_TCAssert( !( l_lineer != l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer > l_test ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer < l_test ), "Expect to be equal" );

		AT_TCAssert( l_lineer == l_viewer, "Expect to be equal" );
		AT_TCAssert( l_lineer >= l_viewer, "Expect to be equal" );
		AT_TCAssert( l_lineer <= l_viewer, "Expect to be equal" );
		AT_TCAssert( !( l_lineer != l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer > l_viewer ), "Expect to be equal" );
		AT_TCAssert( !( l_lineer < l_viewer ), "Expect to be equal" );

		AT_TCAssert( l_viewer == l_lineer, "Expect to be equal" );
		AT_TCAssert( l_viewer >= l_lineer, "Expect to be equal" );
		AT_TCAssert( l_viewer <= l_lineer, "Expect to be equal" );
		AT_TCAssert( !( l_viewer != l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer > l_lineer ), "Expect to be equal" );
		AT_TCAssert( !( l_viewer < l_lineer ), "Expect to be equal" );


		l_tester = l_lineer;
		l_test = l_line;
	}
	
};

AT_RegisterTest( MTTest, Life );

template <typename w_count>
class PtrTest_Generic
{

    mutable w_count                             m_ref_count;

    template <typename w_count_basis, typename w_compare_t>
    static inline w_count_basis AssertGreater( w_count_basis i_v, w_compare_t i_cmp )
    {
        AT_Assert( i_v > i_cmp );
        return i_v;
    }
    
    public:

    virtual ~PtrTest_Generic() {};  // Virtual destructor
    
    PtrTest_Generic()
      : m_ref_count( 1 )
    {
    }

    PtrTest_Generic( const PtrTest_Generic & )
      : m_ref_count( 1 )
    {
    }

    PtrTest_Generic & operator = ( const PtrTest_Generic & )
    {
        return *this;
    }

    int AddRef() const
    {
        
        return AssertGreater( ++ m_ref_count, 1 );
    }

    int Release() const
    {

        int l_ret_val = AssertGreater( m_ref_count --, 0 ) -1;
        
        if ( l_ret_val == 0 )
        {
            return 0;
        }

        return l_ret_val;
    }
};

template<typename T>
class PerfTester_LT
{
public:

    static const int        m_num_elems = 1000;
    static const int        m_num_iter = 1000;

    T                       m_t;
//    T                       m_array[ m_num_elems ];
    Ptr<T *>                m_ptr_array[ m_num_elems ];

    PerfTester_LT()
    {
        Ptr<T *>    v = & m_t;
        for ( Ptr<T *> * j = m_ptr_array; j < m_ptr_array + m_num_elems; j ++ )
        {
            * j = v;
        }
        
//        for ( T * i = m_array; i < m_array + m_num_elems; i ++, j ++ )
//        {
//            * j = i;
//        }
    }

};

template<typename PT>
class PerfTest_Iters
{
    public:
    void DoTest()
    {
        for ( int i = 0; i < PT::m_num_iter; i ++ )
        {
            PT  * v = new PT();

            delete v;
        }
    }
};
            


AT_DefineTest( MTPerfTest, Life, "Lifetime MT Performance test" )
    , PerfTest_Iters< PerfTester_LT< PtrTest_Generic<AtomicCount> > >
{

	void Run()
	{
        DoTest();
	}
	
};

AT_RegisterTest( MTPerfTest, Life );


AT_DefineTest( PerfTest, Life, "Lifetime NON-MT Performance test" )
    , PerfTest_Iters< PerfTester_LT< PtrTest_Generic<int> > >
{

	void Run()
	{
        DoTest();
	}
	
};

AT_RegisterTest( PerfTest, Life );

class MedusaTest
  : public PtrTarget_MT
{
	public:

    MedusaTest()
      : m_medusa_count( 0 ),
        m_reached_zero( false )
    {
    }

    mutable AtomicCount                     m_medusa_count;
    mutable bool                            m_reached_zero;

    void MedusaInc( const MedusaTest * ) const
    {
        ++ m_medusa_count;
    }

    void MedusaDec( const MedusaTest * ) const
    {
        if ( -- m_medusa_count == 0 )
        {
            std::cout << "Medusa count reached 0\n";
            m_reached_zero = true;
        }
    }
};

namespace at {
    
// Select a Medusa Traits class
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< MedusaTest *, w_First, w_Second >
{
    public:

    typedef PtrTraitsMedusa<MedusaTest *>  type;
};

// Select a Medusa Traits class
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< const MedusaTest *, w_First, w_Second >
{
    public:

    typedef PtrTraitsMedusa<const MedusaTest *>  type;
};

} // end namespace at

class MedusaTest_Derivative
  : public MedusaTest
{
    // not much else to do here
};


AT_DefineTest( MedusaTest, Life, "Medusa test" )
{

	void Run()
	{
        Ptr<MedusaTest_Derivative *> l_dm = new MedusaTest_Derivative();

        AT_TCAssert( ! l_dm->m_reached_zero, "m_medusa_count should not have reached 0" );

        PtrDelegate<MedusaTest *> l_dmed = l_dm;
        Ptr<const MedusaTest *> l_med = l_dm;
        
        AT_TCAssert( ! l_dm->m_reached_zero, "m_medusa_count should not have reached 0" );

        l_med = l_dmed;
        l_med = 0;
        
        AT_TCAssert( l_dm->m_reached_zero, "m_medusa_count should have reached 0" );
	}
	
};

AT_RegisterTest( MedusaTest, Life );


class RefCountTester
{
	public:
	
	bool						* m_ptester;
    mutable int                             m_ref_count;

    template <typename w_count_basis, typename w_compare_t>
    static inline w_count_basis AssertGreater( w_count_basis i_v, w_compare_t i_cmp )
    {
        AT_Assert( i_v > i_cmp );
        return i_v;
    }
    
    virtual ~RefCountTester() {};  // Virtual destructor
    
	RefCountTester( bool & o_tester_bool )
	  : m_ptester( & o_tester_bool ),
        m_ref_count( 0 )
    {
		o_tester_bool = false;
    }

    RefCountTester( const RefCountTester & )
      : m_ref_count( 1 )
    {
    }

    RefCountTester & operator = ( const RefCountTester & )
    {
        return *this;
    }

    int AddRef() const
    {
        
        return AssertGreater( ++ m_ref_count, 0 );
    }

    int Release() const
    {

        int l_ret_val = AssertGreater( m_ref_count --, 0 ) -1;
        
        if ( l_ret_val == 0 )
        {
            -- m_ref_count;
            * m_ptester = true;
            return 0;
        }

        return l_ret_val;
    }

};



AT_DefineTestLevel(
    MultiThreadPtr,
    Life,
    "Multithreaded smart Ptr test",
    UnitTestTraits::TestSuccess,
    0 // Currently has bug
)
{

	void Run()
	{
        bool    l_deleted( false );

        {
            RefCountTester l_test( l_deleted );

            Ptr<RefCountTester *, PtrTraits< RefCountTester *, PtrClassRefWrapperMT< RefCountTester * > > > l_dm = & l_test;

            AT_TCAssert( 1 == l_test.m_ref_count, "Ref count should be 1" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
    
            Ptr<RefCountTester *> l_d = l_dm;
    
            AT_TCAssert( 2 == l_test.m_ref_count, "Ref count should be 2" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            l_d = l_dm;
            
            AT_TCAssert( 2 == l_test.m_ref_count, "Ref count should be 2" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            l_dm = l_d;
    
            AT_TCAssert( 2 == l_test.m_ref_count, "Ref count should be 2" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            l_dm = l_dm;
            
            AT_TCAssert( 2 == l_test.m_ref_count, "Ref count should be 2" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            PtrDelegate<RefCountTester *> l_dd = l_dm;
    
            AT_TCAssert( 3 == l_test.m_ref_count, "Ref count should be 3" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            l_dm = l_dd;
            
            AT_TCAssert( 2 == l_test.m_ref_count, "Ref count should be 2" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );

            PtrDelegate<RefCountTester *, PtrTraits< RefCountTester *, PtrClassRefWrapperMT< RefCountTester * > > > l_ddm = l_dm;
            
            AT_TCAssert( 3 == l_test.m_ref_count, "Ref count should be 3" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            PtrDelegate<RefCountTester *, PtrTraits< RefCountTester *, PtrClassRefWrapperMT< RefCountTester * > > > l_ddm2 = l_d;
    
            AT_TCAssert( 4 == l_test.m_ref_count, "Ref count should be 4" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            l_dm = l_ddm;
            
            AT_TCAssert( 3 == l_test.m_ref_count, "Ref count should be 3" );
            AT_TCAssert( !l_deleted, "Should not be deleted" );
            
            l_d = l_ddm2;
            
            AT_TCAssert( 2 == l_test.m_ref_count, "Ref count should be 2" );

            l_d = 0;

            AT_TCAssert( 1 == l_test.m_ref_count, "Ref count should be 1" );
            
            l_dm = 0;
            AT_TCAssert( l_deleted, "Should be deleted" );

            
        }

        AT_TCAssert( l_deleted, "Should be deleted" );
        
	}
	
};

AT_RegisterTest( MultiThreadPtr, Life );


