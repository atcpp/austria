//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * tst_os.cpp
 *
 * This contains the implementation of tst_os.h.
 *
 * platform specific test integration
 */

#if defined(WIN32) || defined(_WIN32)

#elif defined(__APPLE__)

#else
    #include "posix/tst_aio.cpp"
#endif
