/**
 * Test twins.
 *
 */

#include "at_twin_basic.h"

#include "at_unit_test.h"

#include <cstring>

namespace {

using namespace at;

class LeadApplicationA
  : public LeadTwin_Basic< AideTwin< NullTwinInterface >, NotifyLeadInterface >
{
	public:

	LeadApplicationA()
	  : m_code( 0 ),
		m_competion_code( TwinTraits::Uninitialized )
	{
	}
	
	virtual void Notify( int i_code )
	{
		m_code = i_code;

		std::cout << "Notify called - i_code=" << i_code << "\n";
	}


	void AppLeadCompleted( TwinTraits::TwinCode i_competion_code )
	{
		m_competion_code = i_competion_code;
	}
		
	int												m_code;

	TwinTraits::TwinCode							m_competion_code;
};


class AideApplication
  : public AideTwin_Basic<NullTwinInterface, NotifyLeadInterface>
{

	public:
	
	void DoNotifyLead( int i_code )
	{
		t_LeadPointer		l_lead;

		GetLead( l_lead );

		if ( l_lead )
		{
			l_lead->Notify( i_code );
		}
	}
};

class AideApplication2
  : public AideTwin_Basic<NullTwinInterface, NotifyLeadInterface>
{

	public:
	
	void DoNotifyLead( int i_code )
	{
		t_LeadPointer		l_lead;

		GetLead( l_lead );

		if ( l_lead )
		{
			l_lead->Notify( i_code );
		}
	}

    void AppAideCloseNotify(
		TwinTraits::TwinCode            i_competion_code
    ) {
        m_competion_code = i_competion_code;
    }

    AideApplication2()
      : m_competion_code( TwinTraits::Uninitialized )
    {
    }
    
	TwinTraits::TwinCode                m_competion_code;
};

AT_TestArea( Twins, "Test Lead and Aide Twin Module" );

class Twins_Fixture
{
	public:

	LeadApplicationA										m_lead[ 1 ];
	
	LeadApplicationA										m_lead2[ 1 ];

	AideApplication										m_aide[ 1 ];
	
	Twins_Fixture()
	{
	}



};

#define CharAssign( x_dst, x_val ) std::memcpy( x_dst, x_val, sizeof( x_val ) )

AT_DefineTest( BasicTest, Twins, "Basic test" ),
	public Twins_Fixture
{

	void Run()
	{
		m_aide->AideAssociate( m_lead );

		m_aide->DoNotifyLead( 33 );

		AT_TCAssert( m_lead->m_code == 33, "Notification failure" );

		m_lead->LeadCancel();

		AT_TCAssert( m_lead->m_competion_code == TwinTraits::CancelRequested, "Cancel failed" );
		
		m_aide->AideAssociate( m_lead );
		
		m_aide->AideAssociate( m_lead2 );
		
		AT_TCAssert( m_lead->m_competion_code == TwinTraits::AideGrabbed, "Grab failed" );

		{

			AideApplication								l_aide[ 1 ];

			l_aide->AideAssociate( m_lead );

			l_aide->DoNotifyLead( 44 );

			AT_TCAssert( m_lead->m_code == 44, "Notification failure" );
		}

		AT_TCAssert( m_lead->m_competion_code == TwinTraits::AideDelete, "Aide delete failed" );


        AideApplication2            l_aide2[ 1 ];
        {
            LeadApplicationA             l_lead[ 1 ];

            l_aide2->AideAssociate( l_lead );

            AT_TCAssert( l_aide2->m_competion_code == TwinTraits::Uninitialized, "Aide2 constructor failed" );
        }
		
        AT_TCAssert( l_aide2->m_competion_code != TwinTraits::Uninitialized, "l_lead failed to notify destruction" );
        
		return;
	}

};

AT_RegisterTest( BasicTest, Twins );

} // anon namespace

