#include "at_preference_xml.h"
#include "at_xml_parser.h"
#include "at_xml_printer.h"
#include "at_unit_test.h"

#include <iostream>
#include <sstream>

using namespace std;
using namespace at;


    class XmlPreferenceDocumentReaderErrorPolicy_PrintToCout
    {

    public:

        inline void ReportMissingRootElementError() {cout << "MissingRootElementError\n";}

        inline void ReportRootElementWrongTagNameError() {cout << "RootElementWrongTagNameError\n";}

        inline void ReportRootElementUnknownAttributeError() {cout << "RootElementUnknownAttributeError\n";}

        inline void ReportPreferenceElementWrongNodeTypeError( unsigned short i_type ) {cout << "PreferenceElementWrongNodeTypeError\n" << "Got type " << i_type << "\n";}

        inline void ReportPreferenceElementWrongTagNameError() {cout << "PreferenceElementWrongTagNameError\n";}

        inline void ReportPreferenceElementMissingNameAttributeError() {cout << "PreferenceElementMissingNameAttributeError\n";}

        inline void ReportPreferenceElementMissingContextAttributeError() {cout << "PreferenceElementMissingContextAttributeError\n";}

        inline void ReportPreferenceElementMissingTypeAttributeError() {cout << "PreferenceElementMissingTypeAttributeError\n";}

        inline void ReportPreferenceElementUnknownAttributeError() {cout << "PreferenceElementUnknownAttributeError\n";}

        inline void ReportDuplicateNameAndContextError() {cout << "DuplicateNameAndContextError\n";}

        inline void ReportPreferenceMissingValueError() {cout << "PreferenceMissingValueError\n";}

        inline void ReportValueElementWrongNodeTypeError() {cout << "ValueElementWrongNodeTypeError\n";}

        inline void ReportPreferenceMultipleValuesError() {cout << "PreferenceMultipleValuesError\n";}

        inline void ReportValueElementUnknownTagNameError() {cout << "ValueElementUnknownTagNameError\n";}

        inline void ReportAtomMissingValueAttributeError() {cout << "AtomMissingValueAttributeError\n";}

        inline void ReportAtomUnknownAttributeError() {cout << "AtomUnknownAttributeError\n";}

        inline void ReportVectorUnknownAttributeError() {cout << "VectorUnknownAttributeError\n";}

    };


AT_TestArea( XMLPrefs, "XML preference tests" );


AT_DefineTest( XMLPrefs, XMLPrefs, "Basic XML preference test" )
{

    void Run()
    {
        const std::string l_text(
            "\n"
            "<preferences>\n"
            "\t<preference\n"
            "\t\tname=\"Austria\"\n"
            "\t\tcontext=\"/Switzerland\"\n"
            "\t\ttype=\"string\">\n"
            "\t\t<atom\n"
            "\t\t\tvalue=\"Lichtenstein\"/>\n"
            "\t</preference>\n"
            "\t<preference\n"
            "\t\tname=\"Belgium\"\n"
            "\t\tcontext=\"/Netherlands\"\n"
            "\t\ttype=\"string\">\n"
            "\t\t<atom\n"
            "\t\t\tvalue=\"Portugal\"/>\n"
            "\t</preference>\n"
            "\t<preference\n"
            "\t\tname=\"France\"\n"
            "\t\tcontext=\"/Denmark\"\n"
            "\t\ttype=\"string\">\n"
            "\t\t<atom\n"
            "\t\t\tvalue=\"Norway\"/>\n"
            "\t</preference>\n"
            "\t<preference\n"
            "\t\tname=\"France\"\n"
            "\t\tcontext=\"/Italy\"\n"
            "\t\ttype=\"string\">\n"
            "\t\t<atom\n"
            "\t\t\tvalue=\"Germany\"/>\n"
            "\t</preference>\n"
            "\t<preference\n"
            "\t\tname=\"France\"\n"
            "\t\tcontext=\"/Sweden\"\n"
            "\t\ttype=\"string\">\n"
            "\t\t<atom\n"
            "\t\t\tvalue=\"Iceland\"/>\n"
            "\t</preference>\n"
            "\t<preference\n"
            "\t\tname=\"Hungary\"\n"
            "\t\tcontext=\"/Slovenia\"\n"
            "\t\ttype=\"pair(string,string)\">\n"
            "\t\t<vector>\n"
            "\t\t\t<atom\n"
            "\t\t\t\tvalue=\"Bosnia\"/>\n"
            "\t\t\t<atom\n"
            "\t\t\t\tvalue=\"Serbia\"/>\n"
            "\t\t</vector>\n"
            "\t</preference>\n"
            "\t<preference\n"
            "\t\tname=\"Latvia\"\n"
            "\t\tcontext=\"/Lithuania\"\n"
            "\t\ttype=\"vector(string)\">\n"
            "\t\t<vector>\n"
            "\t\t\t<atom\n"
            "\t\t\t\tvalue=\"Estonia\"/>\n"
            "\t\t\t<atom\n"
            "\t\t\t\tvalue=\"CzechRepublic\"/>\n"
            "\t\t\t<atom\n"
            "\t\t\t\tvalue=\"Slovakia\"/>\n"
            "\t\t</vector>\n"
            "\t</preference>\n"
            "</preferences>\n"
        );

        PreferenceValueTable l_table1;
        l_table1.SetValue( "Austria", "/Switzerland", "string", MultiString( "Lichtenstein" ) );
        l_table1.SetValue( "Belgium", "/Netherlands", "string", MultiString( "Portugal" ) );
        l_table1.SetValue( "France", "/Italy", "string", MultiString( "Germany" ) );
        l_table1.SetValue( "France", "/Denmark", "string", MultiString( "Norway" ) );
        l_table1.SetValue( "France", "/Sweden", "string", MultiString( "Iceland" ) );
        {
            std::vector< MultiString > l_empty_vector;
            MultiString l_ms( l_empty_vector );
            std::vector< MultiString > & l_vec = l_ms.Vec();
            l_vec.push_back( MultiString( "Estonia" ) );
            l_vec.push_back( MultiString( "CzechRepublic" ) );
            l_vec.push_back( MultiString( "Slovakia" ) );
            l_table1.SetValue( "Latvia", "/Lithuania", "vector(string)", l_ms );
        }
        {
            std::vector< MultiString > l_empty_vector;
            MultiString l_ms( l_empty_vector );
            l_ms.Vec().push_back( MultiString( "Bosnia" ) );
            l_ms.Vec().push_back( MultiString( "Serbia" ) );
            l_table1.SetValue( "Hungary", "/Slovenia", "pair(string,string)", l_ms );
        }
        Ptr< XmlDocument * > l_doc1 = WriteXmlPreferenceDocument( l_table1 );
        ostringstream oss;
        XmlPrettyPrint( oss, l_doc1 );
        AT_TCAssert( oss.str() == l_text, "oss.str() and l_text should be identical.\n" );
        Ptr< XmlDocument * > l_doc2 =
            XmlParseMemory(
                oss.str().c_str(),
                strlen( oss.str().c_str() )
            );
        AT_TCAssert( l_doc2, "XmlParseMemory() should return true\n" );
        PreferenceValueTable l_table2;
        XmlPreferenceDocumentReader< XmlPreferenceDocumentReaderErrorPolicy_PrintToCout > l_reader;
        bool l_success1 = l_reader.Read( l_doc2, l_table2 );
        AT_TCAssert( l_success1, "XmlPreferenceDocumentReader<>::Read() should return true\n" );
        AT_TCAssert( l_table1 == l_table2, "l_table1 and l_table2 should be identical.\n" );
        Ptr< PreferenceManager<> * > l_pm = new PreferenceManager<>();
        l_pm->SwapValueTable( l_table2 );
        {
            Preference< string > l_pref1( "Austria", "Luxembourg", "/Switzerland", l_pm );
            AT_TCAssert( l_pref1.Value() == "Lichtenstein", "l_pref1.Value() == \"Lichtenstein\" should evaluate true\n" );
        }
        {
            Preference< string > l_pref2( "Belgium", "Spain", "/Netherlands", l_pm );
            AT_TCAssert( l_pref2.Value() == "Portugal", "l_pref2.Value() == \"Portugal\" should evaluate true\n" );
        }
        {
            Preference< string > l_pref3( "France", "Poland", "/Denmark", l_pm );
            AT_TCAssert( l_pref3.Value() == "Norway", "l_pref3.Value() == \"Norway\" should evaluate true\n" );
        }
        {
            Preference< vector< string > > l_pref4( "Latvia", vector< string >(), "/Lithuania", l_pm );
            AT_TCAssert( l_pref4.Value().size() == 3, "l_pref4.Value() == 3 should evaluate true\n" );
            AT_TCAssert( l_pref4.Value()[0] == "Estonia", "l_pref4.Value()[0] == \"Estonia\" should evaluate true\n" );
            AT_TCAssert( l_pref4.Value()[1] == "CzechRepublic", "l_pref4.Value()[1] == \"CzechRepublic\" should evaluate true\n" );
            AT_TCAssert( l_pref4.Value()[2] == "Slovakia", "l_pref4.Value()[2] == \"Slovakia\" should evaluate true\n" );
        }
        {
            Preference< pair< string, string > > l_pref5( "Hungary", pair< string, string >(), "/Slovenia", l_pm );
            AT_TCAssert( l_pref5.Value().first == "Bosnia", "l_pref5.Value().first == \"Bosnia\" should evaluate true\n" );
            AT_TCAssert( l_pref5.Value().second == "Serbia", "l_pref5.Value().second == \"Serbia\" should evaluate true\n" );
        }
    }

};

AT_RegisterTest( XMLPrefs, XMLPrefs );
