


#include "at_base64.h"

#include "at_unit_test.h"

namespace {
    
AT_TestArea( Base64, "Basic BASE64 tests" );

#include <iostream>

AT_DefineTest( Basic, Base64, "Print base64 key test" )
{
	void Run()
	{
        at::Base64Key                   l_key2;

        GenerateBase64Key( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", 24, l_key2 );

        at::BaseNKey<6, at::BaseNEndianLittle>  l_key1;
        
        GenerateBase64Key( "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@-", l_key1 );

        at::BaseNKey<6, at::BaseNEndianLittle>  & l_key = l_key1;

        std::cout << "{\n";
		for ( unsigned i = 0; i < sizeof l_key.m_key; ++ i )
		{
            std::cout << "'" << l_key.m_key.m_data[ i ] << "', ";
            if ( (i & 0xf) == 0xf )
            {
                std::cout << "\n";
            }
		}
        std::cout << "},{\n";
        
		for ( unsigned i = 0; i < sizeof l_key.m_key_inverse; ++ i )
		{
            std::cout << "" << (int)(l_key.m_key_inverse.m_data[ i ]) << ", ";
            if ( (i & 0xf) == 0xf )
            {
                std::cout << "\n";
            }
		}
        std::cout << "},\n";

        std::cout.flush();
	}
};

AT_RegisterTest( Basic, Base64 );


AT_DefineTest( Encode, Base64, "Basic base64 encode/decode test" )
{
	void Run()
	{
        {
            const at::Base64Key   & l_key = at::g_default_base64_key;
    
            at::BaseNEncoder<6, at::BaseNEndianBig>   l_encoder( l_key );
    
            std::string l_instr = "A";
            
            std::string l_outstr;
            std::back_insert_iterator< std::string >    l_inserter( l_outstr );
    
            l_encoder.Encode(
                l_instr.begin(),
                l_instr.end(),
                l_inserter
            );

            std::cout << " - '" << l_outstr << "' - " << std::endl;
        
        }
        const at::Base64Key   & l_key = at::g_default_base64_key;

        at::BaseNEncoder<6, at::BaseNEndianBig>   l_encoder( l_key );

        std::string l_instr = "This is a string to encode !";
        
        std::string l_outstr;
        std::back_insert_iterator< std::string >    l_inserter( l_outstr );

        l_encoder.Encode(
            l_instr.begin(),
            l_instr.end(),
            l_inserter
        );
        
        l_encoder.Flush( l_inserter );
        
        std::cout << "\n" << l_outstr << std::endl;

        // decorate the string now
        l_outstr = std::string( " | ''''\n" ) + l_outstr + "%%%%% ***\n\r";
        
        at::BaseNDecoder<6, at::BaseNEndianBig>   l_decoder( l_key );
        std::string l_outstr_plain;
        std::back_insert_iterator< std::string >    l_inserter_plain( l_outstr_plain );

        l_decoder.Decode(
            l_outstr.begin(),
            l_outstr.end(),
            l_inserter_plain
        );

        l_decoder.Flush();

        std::cout << " - '" << l_outstr_plain << "' - " << std::endl;
        
        AT_TCAssert( l_outstr_plain == l_instr, "Unconverted string does not equal original" );

        l_outstr = "QWxhZGRpbjpvcGVuIHNlc2FtZQ==";

        l_outstr_plain.clear();

        l_decoder.Decode(
            l_outstr.begin(),
            l_outstr.end(),
            l_inserter_plain
        );
        
        std::cout << " - '" << l_outstr_plain << "' - " << std::endl;
        
	}
};

AT_RegisterTest( Encode, Base64 );


AT_DefineTest( Helpers, Base64, "Basic helpers test" )
{
	void Run()
	{

        std::string l_instr = "A";
        std::string l_ostr;
        std::string l_decstr;
        
        std::cout << "E - '" << ( l_ostr = at::EncodeBase64( l_instr, at::g_default_base64_key ) ) << "' - " << std::endl;
        std::cout << "D - '" << ( l_decstr = at::DecodeBase64( l_ostr, at::g_default_base64_key ) ) << "' - " << std::endl;

        AT_TCAssert( l_instr == l_decstr, "Decode failed" );
	}
};

AT_RegisterTest( Helpers, Base64 );

AT_DefineTest( Stress, Base64, "One at a time decode test" )
{
	void Run()
	{
        const at::Base64Key   & l_key = at::g_default_base64_key;

        at::BaseNEncoder<6, at::BaseNEndianBig>   l_encoder( l_key );

        std::string l_instr = "xtras writing output files";
        
        std::string l_outstr;
        std::back_insert_iterator< std::string >    l_inserter( l_outstr );

        for (
            std::string::iterator i = l_instr.begin();
            i != l_instr.end();
            ++ i
        )
        {
            l_encoder.Encode(
                i,
                i + 1,
                l_inserter
            );
        }
        
        l_encoder.Flush( l_inserter );
        
        std::cout << "\n" << l_outstr << std::endl;

        AT_TCAssert( l_outstr == "eHRyYXMgd3JpdGluZyBvdXRwdXQgZmlsZXM=", "Output unexpected" );
        
        // decorate the string now
        l_outstr = std::string( " | ''''\n" ) + l_outstr + "%%%%% ***\n\r";
        
        at::BaseNDecoder<6, at::BaseNEndianBig>   l_decoder( l_key );
        std::string l_outstr_plain;
        std::back_insert_iterator< std::string >    l_inserter_plain( l_outstr_plain );

        for (
            std::string::iterator i = l_outstr.begin();
            i != l_outstr.end();
            ++ i
        )
        {
            l_decoder.Decode(
                i,
                i + 1,
                l_inserter_plain
            );
        }
        
        l_decoder.Flush();

        std::cout << " - '" << l_outstr_plain << "' - " << std::endl;
        
        AT_TCAssert( l_outstr_plain == l_instr, "Unconverted string does not equal original" );

	}
};

AT_RegisterTest( Stress, Base64 );


};
