
#include "at_activity.h"
#include "at_atomic.h"

#include "at_unit_test.h"

#include <iostream>


namespace {

// MakeAL makes an activity list - either a thread pool
// or an ActivityList_FedEx.

at::Ptr< at::ActivityListOwner *> MakeActivityList( int i_type )
{
    at::ActivityList_Pool         * l_pool_list;
    at::ActivityList_FedEx        * l_fedex_list;
    
    if ( i_type == 0 )
    {
        l_pool_list = new at::ActivityList_Pool( 6 );
        
        at::Ptr< at::ActivityList_Pool *>   l_al( l_pool_list );

        AT_TCAssert( l_al, "Failed to create Pool activity list" );

        return l_al->GetOwner();
    }
    else
    {
        l_fedex_list = new at::ActivityList_FedEx();
        
        at::Ptr< at::ActivityList_FedEx *>   l_al( l_fedex_list );

        AT_TCAssert( l_al, "Failed to create FedEx activity list" );

        return l_al->GetOwner();
    }
}

    
AT_TestArea( ActivityArea, "Activity tests" );

// AT_ActivityPerformBinder(P,Q) creates an activity class
// named P with activity class named Q that overrides
// the Activity::Perform() and called Perform ## P

AT_ActivityPerformBinder( EventA, at::Activity_ControlledSeated )
AT_ActivityPerformBinder( EventB, at::Activity_ControlledSeated )

class Application
  : public EventA,
    public EventB
{
    public:

    void PerformEventA()
    {
        ++ m_count;
    }

    void PerformEventB()
    {
        -- m_count;
    }

    void SendEventA()
    {
        EventA::Enqueue();
    }

    void SendEventB()
    {
        EventB::Enqueue();
    }

    at::AtomicCount             m_count;

    Application( int i_type )
      : at::ActivityList_Cradle( MakeActivityList( i_type ) )
    {
    }

    virtual ~Application()
    {
        // this will shut down the activity list
        GetActivityList()->Terminate();

        // Could have used this instead ...
        DisableAll();
    }
};

void Test( int i_type )
{
    Application     l_application( i_type );

    // Send an event 
    l_application.SendEventA();

    // wait for all events to be consumed
    l_application.GetActivityList()->WaitCompletion();

    // The count had better be 1
    AT_TCAssert( l_application.m_count.Get() == 1, "Expected to have performed DoEventA()" );

    // Send the B event
    l_application.SendEventB();

    // Wait for events to be consumed
    l_application.GetActivityList()->WaitCompletion();

    // The count had better be 0
    AT_TCAssert( l_application.m_count.Get() == 0, "Expected to have performed DoEventB()" );

}


AT_DefineTest( AppTest, ActivityArea, "Basic FedEx activity list test" )
{
	void Run()
	{
        Test( 0 );
        Test( 1 );
    }
};

AT_RegisterTest( AppTest, ActivityArea );


template<int N>
class TstActivityBase
  : public at::Activity_ControlledSeated
{
    public:

    TstActivityBase(
        at::AtomicCount             * i_count
    )
      : m_count( i_count )
    {
        std::cout << "TstActivityBase<" << N << "> Created \n";
    }

    virtual void Perform()
    {
        ++ ( * m_count );
        std::cout << "TstActivityBase<" << N << "> Perfomed \n";
    }

    virtual void Cancelled()
    {
        std::cout << "TstActivityBase<" << N << "> Cancelled \n";
    }

    ~TstActivityBase()
    {
        std::cout << "TstActivityBase<" << N << "> Destroyed \n";
    }

    at::AtomicCount                 * m_count;
};

// macro defines class TestBinder and methpd PerformTestBinder.
AT_ActivityPerformBinder( TestBinder,  at::Activity_Seated )


// ======== TstActivity ===============================================
/**
 * This is the basic test harness
 *
 */

class TstActivity
  : public TstActivityBase<0>,
    public TstActivityBase<1>,
    public TstActivityBase<2>,
    public TstActivityBase<3>,
    public TstActivityBase<4>,
    public TestBinder,
    public virtual at::ActivityList_Cradle
{
    public:

    // MakeAL makes an activity list - either a thread pool
    // or an ActivityList_FedEx.
    
    at::Ptr< at::ActivityList *> MakeAL( int i_type )
    {
        if ( i_type == 0 )
        {
            m_pool_list = new at::ActivityList_Pool( 6 );
            
            at::Ptr< at::ActivityList_Pool *>   l_al( m_pool_list );
    
            AT_TCAssert( l_al, "Failed to create Pool activity list" );
    
            return l_al;
        }
        else
        {
            m_fedex_list = new at::ActivityList_FedEx();
            
            at::Ptr< at::ActivityList_FedEx *>   l_al( m_fedex_list );
    
            AT_TCAssert( l_al, "Failed to create FedEx activity list" );
    
            return l_al;
        }
    }

    /**
     * TstActivity
     *
     */
    TstActivity( int i_type = 0 )
      : at::ActivityList_Cradle(
            MakeAL( i_type )->GetOwner()
        ),
        TstActivityBase<0>( & m_count ),
        TstActivityBase<1>( & m_count ),
        TstActivityBase<2>( & m_count ),
        TstActivityBase<3>( & m_count ),
        TstActivityBase<4>( & m_count )
    {
        std::cout << "TstActivity Created\n";

        AT_TCAssert( ActivityList_Cradle::GetActivityList(), "Should have an activity list !" );
    }

    ~TstActivity()
    {
        std::cout << "Disabling\n";
        
        DisableAll( true );
        
        std::cout << "Done Disabling\n";
    }

    template<int N>
    TstActivityBase<N> & Get()
    {
        return * static_cast< TstActivityBase<N> *>( this );
    }

    void PerformTestBinder()
    {
    }
    
    
    /**
     * m_count is a test count
     */
    at::AtomicCount                 m_count;

    at::ActivityList_Pool         * m_pool_list;
    at::ActivityList_FedEx        * m_fedex_list;

};




AT_DefineTest( Activity, ActivityArea, "Basic thread pool test" )
{
    TstActivity             m_ta;
    
	void Run()
	{

        m_ta.Get<0>().Enqueue();

        //at::Task::Sleep( at::TimeInterval( 100 * at::TimeInterval::PerMilliSec ) );
        m_ta.Get<0>().Wait();

        AT_TCAssert( m_ta.m_count.Get() == 1, "Expected to have performed m_ta.Get<0>" );

        at::Ptr< at::ActivityList * > l_al = m_ta.GetActivityList();

        l_al->Suspend();

        m_ta.Get<0>().Enqueue();

        m_ta.Get<0>().Disable();
        m_ta.Get<0>().Enable();
        
        l_al->Resume();

        m_ta.Get<0>().Wait();
        
        AT_TCAssert( m_ta.m_count.Get() == 1, "Suspend()/Disable() seems to have failed" );
        
        m_ta.Get<0>().Enqueue();

        m_ta.Get<0>().Wait();

        AT_TCAssert( m_ta.m_count.Get() == 2, "Expected to have performed m_ta.Get<0> a second time" );

        m_ta.Get<0>().Enqueue();
        m_ta.Get<1>().Enqueue();
        m_ta.Get<2>().Enqueue();
        m_ta.Get<3>().Enqueue();
        m_ta.Get<4>().Enqueue();

        l_al->WaitCompletion();

        int count_is = m_ta.m_count.Get();
        
        AT_TCAssert( count_is == 7, "Expected to have all activities performed" );
	}
};

AT_RegisterTest( Activity, ActivityArea );


struct TstActivity_FedEx
  : TstActivity
{
    TstActivity_FedEx( int i_type = 1 )
      : at::ActivityList_Cradle(
            MakeAL( i_type )->GetOwner()
        )
    {
    }
};
    

AT_DefineTest( FedEx, ActivityArea, "Basic FedEx activity list test" )
{
    TstActivity_FedEx           m_ta;
    
	void Run()
	{
        m_ta.Get<0>().Enqueue();

        //at::Task::Sleep( at::TimeInterval( 100 * at::TimeInterval::PerMilliSec ) );
        m_ta.Get<0>().Wait();

		int i = m_ta.m_count.Get();

		AT_TCAssert( m_ta.m_count.Get() == 1, "Expected to have performed m_ta.Get<0>" );

        at::Ptr< at::ActivityList * > l_al = m_ta.GetActivityList();

        l_al->Suspend();

        m_ta.Get<0>().Enqueue();

        m_ta.Get<0>().Disable();
        m_ta.Get<0>().Enable();
        
        l_al->Resume();

        m_ta.Get<0>().Wait();

		i = m_ta.m_count.Get();

        AT_TCAssert( m_ta.m_count.Get() == 1, "Suspend()/Disable() seems to have failed" );
        
        m_ta.Get<0>().Enqueue();

        m_ta.Get<0>().Wait();

		i = m_ta.m_count.Get();

        AT_TCAssert( m_ta.m_count.Get() == 2, "Expected to have performed m_ta.Get<0> a second time" );

        m_ta.Get<0>().Enqueue();
        m_ta.Get<1>().Enqueue();
        m_ta.Get<2>().Enqueue();
        m_ta.Get<3>().Enqueue();
        m_ta.Get<4>().Enqueue();

        l_al->WaitCompletion();

        int count_is = m_ta.m_count.Get();
        
        AT_TCAssert( count_is == 7, "Expected to have all activities performed" );
	}
};

AT_RegisterTest( FedEx, ActivityArea );


};


