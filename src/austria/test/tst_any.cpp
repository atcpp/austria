
#include "at_any.h"

#include "at_unit_test.h"
#include <iostream>
#include <vector>
#include <map>

#include "at_lifetime.h"

using namespace at;

void xxx(){}
void yyy(){}

namespace AnyTest {

AT_TestArea( Any, "Any object tests" );

struct NoCopy
{
    NoCopy() {}

    private:
    NoCopy( const NoCopy & );
    NoCopy & operator= ( const NoCopy & );
};
    

enum TestsId
{
    Nothing = 0,
    DefaultConstruct = 1,
    CopyConstruct = 2,
    Assignment = 3,
    LessThan = 4,
    Equality = 5,
    Destruction = 6
};

struct Null {};
struct Null2 {};

template<int N>
class Yeller;

}; // namespace

namespace std {

using namespace at;

template<
    typename        w_char_type,
    class           w_traits,
    int             w_N
>                   
basic_ostream<w_char_type, w_traits>& operator << (
    basic_ostream<w_char_type, w_traits>            & i_ostream,
    const AnyTest::Yeller<w_N>                      & i_value
) {
    return i_ostream << "Yeller<" << w_N << ">( " << & i_value << " )\n";
    
} // end basic_ostream<w_char_type, w_traits>& operator <<

} // namespace std

namespace AnyTest {

template<int N>
class Yeller
{
    public:

    static TestsId          s_value;
    int                     m_sign;

    Yeller()
      : m_sign( N ) 
    {
        s_value = DefaultConstruct;
        std::cout << "Yeller<" << N << "> Default Construction " << this << std::endl;
        AT_TCAssert( m_sign == N, "Data was currupted\n" );
    }

    Yeller( const Yeller & i_val )
      : m_sign( N ) 
    {
        s_value = CopyConstruct;
        std::cout << "Yeller<" << N << "> Copy constructor " << this << std::endl;
        AT_TCAssert( m_sign == N, "Data was currupted\n" );
    }

    Yeller & operator= ( const Yeller & i_val )
    {
        AT_TCAssert( i_val.m_sign == N, "Data was currupted\n" );
        s_value = Assignment;
        std::cout << "Yeller<" << N << "> Assignment " << this << std::endl;
        AT_TCAssert( m_sign == N, "Data was currupted\n" );
        return * this;
    }

    bool operator< ( const Yeller & i_val ) const
    {
        s_value = LessThan;
        std::cout << "Yeller<" << N << "> Less Than " << this << std::endl;
        AT_TCAssert( m_sign == N, "Data was currupted\n" );
        return this < & i_val;
    }

    bool operator== ( const Yeller & i_val ) const
    {
        s_value = Equality;
        std::cout << "Yeller<" << N << "> CompareEqual " << this << std::endl;
        AT_TCAssert( m_sign == N, "Data was currupted\n" );
        return this == & i_val;
    }

    ~Yeller()
    {
        s_value = Destruction;
        std::cout << "Yeller<" << N << "> Destroyed " << this << std::endl;
        AT_TCAssert( m_sign == N, "Data was currupted\n" );
    }

};

template<int N>
TestsId Yeller<N>::s_value = Nothing;

template<int N>
struct YellerPtr
  : Yeller<N>,
    PtrTarget_Basic
{
};


AT_DefineTest( Any, Any, "Basic Any test" )
{
    
	void Run()
	{
        at::Any<>           l_empty;
        
        Yeller<0>           y0;
        AT_TCAssert( y0.s_value == DefaultConstruct, "Yeller should do default construct\n" );

        at::Any<NoCopy>       a0( ToAny( y0 ) );
        AT_TCAssert( y0.s_value == CopyConstruct, "Yeller should copy construct\n" );

        Yeller<0>          & y0x = a0.MakeRef< Yeller<0> >();
        
        const Yeller<0>   & y0r = a0.Refer();
        bool x = a0 > a0;

        x = a0 == Any<>( y0, AnyInit );

        a0.Refer() = y0;
        AT_TCAssert( y0.s_value == Assignment, "Yeller should do assignment\n" );

        at::Any<Null>       b0( ToAny( y0 ) );
        AT_TCAssert( y0.s_value == CopyConstruct, "Yeller should copy construct\n" );

        b0 = ToAny( y0 );
        AT_TCAssert( y0.s_value == Assignment, "Yeller should do assignment\n" );
        
        y0.s_value = Nothing;
        at::Any<Null>       c0( y0, AnyInit );
        AT_TCAssert( y0.s_value == CopyConstruct, "Yeller should copy construct\n" );
        
        a0 = b0;
        AT_TCAssert( y0.s_value == Assignment, "Yeller should copy construct\n" );

        Yeller<0>           x0 = a0.Refer();
        AT_TCAssert( y0.s_value == CopyConstruct, "Yeller should copy construct\n" );
        
        y0 = a0.Refer();
        AT_TCAssert( y0.s_value == Assignment, "Yeller should do assignment\n" );

        bool l_is_good = false;

        try {
            Yeller<1>           x1 = b0.Refer();
        } catch ( const at::ExceptionDerivation<Any_ConversionFailure> & )
        {
            l_is_good = true;
        }

        AT_TCAssert( l_is_good, "Conversion error detection failed" );

        l_is_good = false;
        
        Yeller<1>               x1;
        try {
            x1 = b0.Refer();
        } catch ( const at::ExceptionDerivation<Any_ConversionFailure> & )
        {
            l_is_good = true;
        }

        AT_TCAssert( l_is_good, "Conversion error detection failed" );

        x1.s_value = Nothing;
        b0.Assign( x1 );
        AT_TCAssert( x1.s_value == CopyConstruct, "Yeller should copy construct\n" );

        x1 = b0.Refer();
        AT_TCAssert( x1.s_value == Assignment, "Yeller should do assignment\n" );

        x1.s_value = y0.s_value = Nothing;
        b0 = a0;
        AT_TCAssert( x1.s_value == Destruction, "Yeller should do assignment\n" );
        AT_TCAssert( y0.s_value == CopyConstruct, "Yeller should do assignment\n" );

        Yeller<0>           z0 = b0.Refer();

        Any<>               other = b0;
        other = b0;

        AT_TCAssert( !( a0 == b0 ), "Comparison of Yeller's should not be equal" );
        AT_TCAssert( y0.s_value == Equality, "Yeller should do CompareEqual\n" );
        AT_TCAssert( !( a0 > a0 ), "Comparison a0 can't be greater than itself" );
        AT_TCAssert( y0.s_value == LessThan, "Yeller should do CompareLess\n" );


        const Any<>         ca0 = a0;

//        ca0 = a0;
//        ca0.Refer() = y0;
//        ca0.Assign( 1 );
//        ca0.Assign( ca0 );
        z0 = ca0.Refer();

        std::cerr << ca0 << std::endl;

        std::vector< Any<> >    any_vec( 1 );

        any_vec[ 0 ] = ca0;

        any_vec.push_back( ToAny( 2 ) );

        std::cerr << any_vec[ 1 ] << std::endl;

        std::map< Any<>, Any<> >        any_map;

        any_map[ ca0 ] = ToAny( 3 );

        any_map[ ToAny( std::string( "HELLO" ) ) ] = ToAny( std::string( "THIS IS THE STRING AT Any<>(HELLO)" ) );

        std::cerr << any_map[ ToAny( std::string( "HELLO" ) ) ] << std::endl;

        bool eq1 = any_vec[ 1 ] == 2;
        bool eq2 = 2 == any_vec[ 1 ];

        AT_TCAssert( eq1 && eq2, "Equal failed .." );

        Any<>   i1 = ToAny( 1 );
        Any<>   i2 = ToAny( 2 );

        AT_TCAssert( i1 == i1, "Equal failed Any == Any" );
        AT_TCAssert( i1 == 1, "Equal failed Any == int" );
        AT_TCAssert( 1  == i1, "Equal failed int == Any" );
        
        AT_TCAssert( !( i1 != i1 ), "Equal failed Any != Any" );
        AT_TCAssert( !( i1 != 1  ), "Equal failed Any != int" );
        AT_TCAssert( !( 1  != i1 ), "Equal failed int != Any" );
        
        AT_TCAssert( !( i1 < i1 ), "Less failed Any < Any" );
        AT_TCAssert( !( i1 < 1  ), "Less failed Any < int" );
        AT_TCAssert( !( 1  < i1 ), "Less failed int < Any" );
        
        AT_TCAssert( ( i1 < i2 ), "Less failed Any < Any" );
        AT_TCAssert( ( i1 < 2  ), "Less failed Any < int" );
        AT_TCAssert( ( 1  < i2 ), "Less failed int < Any" );
        
        AT_TCAssert( !( i1 > i1 ), "Less failed Any < Any" );
        AT_TCAssert( !( i1 > 1  ), "Less failed Any < int" );
        AT_TCAssert( !( 1  > i1 ), "Less failed int < Any" );
        
        AT_TCAssert( ( i2 > i1 ), "Less failed Any < Any" );
        AT_TCAssert( ( i2 > 1  ), "Less failed Any < int" );
        AT_TCAssert( ( 2  > i1 ), "Less failed int < Any" );

        Any<>   anystr( std::string( "Hello world" ), AnyInit );

        std::string * l_str = 0;

        AT_TCAssert( anystr.ReadPointer( l_str ), "ReadPointer failed to return true" );

        AT_TCAssert( * l_str == "Hello world", "String failed to be copied correctly" );

        int         * l_int;
        // anystr contains a string not an int - ReadPointer should fail.
        AT_TCAssert( ! anystr.ReadPointer( l_int ), "ReadPointer failed to return false" );

        const Any<>   & canystr = anystr;

        const std::string * l_cstr = 0;

        AT_TCAssert( canystr.ReadPointer( l_cstr ), "ReadPointer failed to return true" );

        AT_TCAssert( * l_cstr == "Hello world", "String failed to be copied correctly" );

        const int         * l_cint;
        // anystr contains a string not an int - ReadPointer should fail.
        AT_TCAssert( ! canystr.ReadPointer( l_cint ), "ReadPointer failed to return false" );

        at::Ptr< YellerPtr<44> * >  l_py = new YellerPtr<44>();

        a0 = ToAny( l_py );

        l_py = a0.ReferPtr();

        at::PtrView< YellerPtr<44> * >  l_pview;

        l_pview = a0.ReferPtr();

        a0.ReferPtr() = l_pview;

        l_pview = a0.ReferPtr();

        a0.Refer() = l_pview;

        l_pview = a0.ReferPtr();

        l_py = 0;

        a0.ReferPtr() = l_pview;

        l_pview = a0.ReferPtr();
        
        at::PtrDelegate< YellerPtr<44> * >  l_pde;

        l_pde = a0.ReferPtr();

        a0.ReferPtr() = l_pde;

        l_pview = a0.ReferPtr();

        l_pde = a0.ReferPtr();

        l_py = a0.ReferPtr();

        a0.Read( l_pview );
        a0.Read( l_pde );
        a0.Read( l_py );

    }

};

AT_RegisterTest( Any, Any );


AT_DefineTest( Any2, Any, "Basic Any test" )
{
    
	void Run()
	{

        at::Any<>           l_empty;

        l_empty.Default< Yeller<20> >();

        Yeller<20>  & y20 = l_empty.Refer();
        
    }

};

AT_RegisterTest( Any2, Any );

AT_DefineTest( AnyReference, Any, "Basic Any test" )
{
    
	void Run()
	{

        std::string             l_val( "I AM A STRING" );
        
        at::AnyReference        l_valref( l_val );

        at::Any<>               l_anyval( ToAny( std::string( "I AM A NEW STRING" ) ) );

        l_valref = l_anyval;

        AT_TCAssert( l_val == "I AM A NEW STRING", "Assignment through AnyReference failed" );


    }

};

AT_RegisterTest( AnyReference, Any );

} // AnyTest namespace

