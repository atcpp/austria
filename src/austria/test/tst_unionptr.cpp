

#include "at_unionptr.h"

#include "at_unit_test.h"

using namespace at;

AT_TestArea( UnionPtr, "Union pointer basic tests" );


class UnionPointerFixture
{
	public:
	
	enum KindEnum
	{
		A_enum = 0,
		B_enum = 1,
		C_enum = 2,
		D_enum = 3,
	};

//	UnionPtr<KindEnum, char, short>							  m_broken;

	class A_Kind
	{
		public:
		int								a;

	};
	
	class B_Kind
	{
		public:
		int								b;

	};
	
	class C_Kind
	{
		public:
		int								c;

	};

	class D_Kind
	{
		public:
		int								d;

	};

	UnionPtr<KindEnum, A_Kind *, B_Kind*>						m_AB;
	UnionPtr<KindEnum, A_Kind *, B_Kind*>						m_AB_x;
	UnionPtr<KindEnum, A_Kind *, B_Kind*, C_Kind*>				m_ABC;
	UnionPtr<KindEnum, A_Kind *, B_Kind*, C_Kind*>				m_ABC_x;
	UnionPtr<KindEnum, A_Kind *, B_Kind*, C_Kind*, D_Kind*>		m_ABCD;
	UnionPtr<KindEnum, A_Kind *, B_Kind*, C_Kind*, D_Kind*>		m_ABCD_x;

	A_Kind								m_a_A[1];
	B_Kind								m_a_B[1];
	C_Kind								m_a_C[1];
	D_Kind								m_a_D[1];
	
	A_Kind								* m_p_A;
	B_Kind								* m_p_B;
	C_Kind								* m_p_C;
	D_Kind								* m_p_D;

	UnionPointerFixture()
	  : m_p_A( 0 ),
		m_p_B( 0 ),
		m_p_C( 0 ),
		m_p_D( 0 )
	{
	}
	
};

AT_DefineTest( Basic, UnionPtr, "This is a basic test" ) , public UnionPointerFixture
{
	void Run()
	{

		m_AB = m_a_A;

		AT_TCAssert( m_AB.Get( m_p_A, m_p_B ) == A_enum, "Failed to get correct type" );

		AT_TCAssert( m_p_A == m_a_A, "Retrieved pointer is incorrect" );
		
		
		m_AB = m_a_B;

		AT_TCAssert( m_AB.Get( m_p_A, m_p_B ) == B_enum, "Failed to get correct type" );

		AT_TCAssert( m_p_B == m_a_B, "Retrieved pointer is incorrect" );

		m_p_B = 0;

		m_AB_x = m_AB;
		
		AT_TCAssert( m_AB_x.Get( m_p_A, m_p_B ) == B_enum, "Failed to get correct type" );

		m_ABC = m_a_C;

		AT_TCAssert( m_ABC.Get( m_p_A, m_p_B, m_p_C ) == C_enum, "Failed to get correct type" );

		AT_TCAssert( m_p_C == m_a_C, "Retrieved pointer is incorrect" );		

		m_ABCD = m_a_D;

		AT_TCAssert( m_ABCD.Get( m_p_A, m_p_B, m_p_C, m_p_D ) == D_enum, "Failed to get correct type" );

		AT_TCAssert( m_p_D == m_a_D, "Retrieved pointer is incorrect" );		

		m_ABCD_x = m_ABCD;

		m_p_D = 0;

		m_ABCD_x = m_ABCD;
		
		AT_TCAssert( m_ABCD_x.Get( m_p_A, m_p_B, m_p_C, m_p_D ) == D_enum, "Failed to get correct type" );
		
		// any test that returns is successful
		return;
	}

};

AT_RegisterTest( Basic, UnionPtr );


class UnionTypePointerFixture
{
	public:

	enum KindEnum
	{
		A_enum = 0,
		B1_enum = 1,
		B2_enum = 2,
		B3_enum = 3,
	};

	class A_Kind
	{
		public:
		int								a;

	};
	
	class B_Kind
	{
		public:
		int								b;

	};
	

	UnionPtrType<KindEnum, A_Kind *, B_Kind*>			m_AB;

	A_Kind								m_a_A[1];
	B_Kind								m_a_B1[1];
	B_Kind								m_a_B2[1];
	B_Kind								m_a_B3[1];
	
	A_Kind								* m_p_A;
	B_Kind								* m_p_B;

	UnionTypePointerFixture()
	  : m_p_A( 0 ),
		m_p_B( 0 )
	{
		m_a_B1->b = 1;
		m_a_B2->b = 2;
		m_a_B3->b = 3;
	}
	
};

AT_DefineTest( PtrType, UnionPtr, "This is the test for UnionPtrType" ) , public UnionTypePointerFixture
{
	void Run()
	{

		m_AB = m_a_A;
		
		AT_TCAssert( m_AB.Get( m_p_A, m_p_B ) == A_enum, "Failed to get correct type" );

		AT_TCAssert( m_p_A == m_a_A, "Retrieved pointer is incorrect" );
		
		
		m_AB = m_a_B1;

		AT_TCAssert( m_AB.Get( m_p_A, m_p_B ) == B1_enum, "Failed to get correct type" );

		AT_TCAssert( m_p_B == m_a_B1, "Retrieved pointer is incorrect" );
		
		m_AB.Set( m_a_B1, B2_enum );

		AT_TCAssert( m_AB.Get( m_p_A, m_p_B ) == B2_enum, "Failed to get correct type" );

		AT_TCAssert( m_p_B == m_a_B1, "Retrieved pointer is incorrect" );
		
		// any test that returns is successful
		return;
	}

};

AT_RegisterTest( PtrType, UnionPtr );


