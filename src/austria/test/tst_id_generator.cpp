
#include "at_id_generator.h"

#include "at_unit_test.h"

#include <iostream>

namespace {
    
AT_TestArea( IDGen, "Basic ID generator test" );

#include <iostream>

AT_DefineTest( Basic, IDGen, "Basic stack trace test" )
{
	void Run()
	{

        std::string l_id1 = at::NewIdent();
        std::string l_id2 = at::NewIdent();


        std::cout << l_id1 << " - " << l_id2 << std::endl;

        AT_TCAssert( l_id1 != l_id2, "NewIdent did not produce different ID" );
        
	}
};

AT_RegisterTest( Basic, IDGen );


};
