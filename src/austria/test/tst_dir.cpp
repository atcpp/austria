/**
 * tst_dir.cpp
 *
 * tst_dir.cpp tests directory handling
 *
 */

// #ifdef BILLTODO

#include "at_unit_test.h"
#include "at_assert.h"
#include "at_file.h"
#include "at_dir.h"
#include "at_thread.h"
#include <cstdlib>
#include "at_twin_basic.h"
#include <string>
#include <sstream>

using namespace at;

namespace DirUnitTest 
{

    AT_TestArea( Dir, "Directory test suite" );


// ======== =============================================

AT_DefineTest( AA_StaticFunctions, Dir, "" )
{
    void Run()
    {
    	int test = 0;
    	try
    	{
	        // --------======= Test Static Directory Functions ========--------
	        test++;
	        Directory::Remove ( FilePath ( "level11" ), true ); // should also remove "level1/level2" if it exists
	        test++;
	        AT_TCAssert ( Directory::Create ( FilePath ( "level11" ) ), "Create Directory Failed!" );
	        test++;
	        AT_TCAssert ( Directory::Create ( FilePath ( "level11/level2" ) ), "Create Directory Failed!" );
	
	        // Create some files in the directories
	        test++;
	        for ( int i = 0; i < 6; i++ )
	        {
	            std::string file1 ( "level11/File_" );
	            std::string file2 ( "level11/level2/File_" );
	            
	 			std::ostringstream stream;
	
				stream.seekp ( 0 );
				stream << i << std::ends;            
				
	            file1 += stream.str ();
	            file2 += stream.str ();
	
	            WFile wf1 ( FilePath ( file1 ), FileAttr::Create );
	            if ( wf1.Open () )
	                wf1.Close ();
	
	            WFile wf2 ( FilePath ( file2 ), FileAttr::Create );
	            if ( wf2.Open () )
	            {
	                //wf2.SetPermissions ( FilePermission::UserRead );
	                wf2.Close ();
	            }
	        }
	        
	        // Remove all the above
	        test++;
	        AT_TCAssert ( Directory::Remove ( FilePath ( "level11" ), true ), "Driectory Remove Failed" ); 
	
	        // Try Renaming an Empty folder
	        test++;
	        Directory::Remove ( FilePath ( "foo" ), true ); // remove so we can create
	        AT_TCAssert ( Directory::Create ( FilePath ( "foo" ) ), "Create Directory Failed!" );
	        // Remove bar if there
	        test++;
	        Directory::Remove ( FilePath ( "bar" ), true ); // remove so we can rename
	        test++;
	        AT_TCAssert ( Directory::Rename ( FilePath ( "foo" ), FilePath ( "bar" ) ), "Rename Directory Failed!" );
	        test++;
	        AT_TCAssert ( Directory::Remove ( FilePath ( "bar" ), true ), "Directory Remove Failed" ); 
	
	        // Try Renaming a full Folder
	        test++;
	        AT_TCAssert ( Directory::Create ( FilePath ( "foo" ) ), "Create Directory Failed!" );
	        test++;
	        for ( int i = 0; i < 6; i++ )
	        {
	            std::string file1 ( "foo/File_" );
	 			std::ostringstream stream;
	
				stream.seekp ( 0 );
				stream << i << std::ends;            
				
	            file1 += stream.str ();
	
	            WFile wf1 ( FilePath ( file1 ), FileAttr::Create );
	            if ( wf1.Open () )
	                wf1.Close ();
	        }
	
	        test++;
	        AT_TCAssert ( Directory::Rename ( FilePath ( "foo" ), FilePath ( "bar" ) ), "Rename Directory Failed!" );
	        test++;
	        AT_TCAssert ( Directory::Exists ( FilePath ( "bar" ) ), "Exists Directory Failed!" );
	        test++;
	        AT_TCAssert ( Directory::Remove ( FilePath ( "bar" ), true ), "Directory Remove Failed" ); 
    	}
    	catch ( const FileError & fe )
    	{
    		std::cout << "\nThere was a File Error Exeception during test " << test << " it was " << fe.What () << std::endl;
    		
    		throw;
    	} 
    	catch ( ... )
    	{
    		std::cout << "\nThere was an Unknown Exception during test " << test << std::endl;
    		
    		throw;
    	}
    }
};

AT_RegisterTest( AA_StaticFunctions, Dir );

AT_DefineTest( AB_, Dir, "" )
{
    void Run()
    {
        // --------======= ========--------
    }
};

AT_RegisterTest( AB_, Dir );

AT_DefineTest( AC_, Dir, "" )
{
    void Run()
    {
        // --------======= ========--------
    }
};

AT_RegisterTest( AC_, Dir );

AT_DefineTest( AD_, Dir, "" )
{
    void Run()
    {
        // --------======= ========--------
    }
};

AT_RegisterTest( AD_, Dir );

}; // NameSpace DirUnitTest

// #endif // Bill Todo

