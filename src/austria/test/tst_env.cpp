#include "at_preference.h"
#include "at_preference_mt.h"
#include "at_log.h"
#include "at_log_time.h"
#include "at_log_taskid.h"
#include "at_log_mt.h"
#include "at_env.h"
#include "at_env_mt.h"
#include "at_unit_test.h"

#include <sstream>
#include <iostream>

using namespace at;
using namespace std;


class LogWriterOutputPolicy_SStream
{

public:

    typedef  std::ostringstream  t_ostream_type;

private:

    t_ostream_type l_ss;

public:

    t_ostream_type & OStream()
    {
        return l_ss;
    }

    void Flush()
    {
    }

    std::string GetOutputString()
    {
        return l_ss.str();
    }

};


typedef
    LogManager<
        LogManagerInputPolicy_Default,
        LogManagerTimePolicy_Default,
        LogManagerTaskIDPolicy_ATTask,
        LogManagerLockPolicy_ATMutexLock,
        LogManagerWriterPolicy_SingleWriter_SameThread<
            AbstractLogWriter
        >::t_template
    >
        MyLogManager;

typedef
    LogWriter<
        MyLogManager,
        LogWriterOutputPolicy_SStream,
        LogWriterFormatPolicy_DefaultFormat<
            LogWriterTimeFormatPolicy_Default
        >::t_template
    >
        MyLogWriter;

typedef
    Environment<
        PreferenceManager< PreferenceManagerLockPolicy_ATMutexLock >,
        MyLogManager,
        EnvironmentPtrPolicy_MT
    >
        MyEnv;

#undef  AT_LOG_MANAGER_TYPE
#define  AT_LOG_MANAGER_TYPE  t_log_mgr_type


class MyRoot
  : virtual public MyEnv
{

public:

    MyLogWriter m_lw;

    MyRoot()
      : MyEnv( MyEnv::Make() )
    {

        m_lw.Register( LogMgr() );

        Preference< string > m_logfilename( "logfilename", "./logfile", "/ModuleName", PrefMgr() );

    }

    ~MyRoot()
    {

        m_lw.Unregister();

        // cout << "\n" << m_lw.GetOutputString() << "\n";

    }

    void Work();

};


class MyLeaf
  : virtual public MyEnv
{

public:

    MyLeaf( MyRoot & i_parent )
        : MyEnv( i_parent )
    {
    }

    void f()
    {

       Preference< int > l_loglevel( "loglevel", AT_GLOBAL_LOGLEVEL, "/ModuleName", PrefMgr() );

       AT_STARTLOG( LogMgr(), l_loglevel, AT_LOGLEVEL_SEVERE, "Description of logged event." )

           at_log("Description of variable(s).") << 55 << 123.456 << "Some string";

       AT_ENDLOG

    }

};


void MyRoot::Work()
{
    MyLeaf l_leaf( *this );
    l_leaf.f();
}


AT_TestArea( Env, "Environment tests" );


AT_DefineTest( Env, Env, "Basic Environment test" )
{

    void Run()
    {
        MyRoot l_root;
        l_root.Work();
    }

};

AT_RegisterTest( Env, Env );
