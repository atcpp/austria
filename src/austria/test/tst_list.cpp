
#include "at_list.h"

#include "at_unit_test.h"

using namespace at;

AT_TestArea( ATList, "List test" );

class ListFixture
{
};


//class ListObject_ListB;
class Payload_Class;
typedef ListHead_Basic< Payload_Class, Payload_Class >      ListObject_ListA;
typedef ListHead_Basic< Payload_Class, ListObject_ListA >   ListObject_ListB;

class Payload_ParentA
  : public ListAccessorDeclare<ListObject_ListA, ListElementBase<Payload_Class> >
{
};

class Payload_ParentB
  : public ListAccessorDeclare<ListObject_ListB,Payload_ParentA>
{
};

class Payload_Class
  : public Payload_ParentB
{
	
public:

	int				m_val;

	Payload_Class()
	  : m_val( -1 )
	{
	}
};

/*
class ListObject_ListA
  : public List_Types< ListObject_ListA, Payload_Class, List_Traits< Payload_Class, ListNothing_Basic > >::xC_List_Entry
{
	public:
	
	virtual bool GetIterator( Iterator<ListObject_ListA> & o_iterator )
	{
		o_iterator.m_pointer = this;
		
		return true;
	}

};

class ListObject_ListB
  : public List_Types< ListObject_ListB, ListObject_ListA, List_Traits< Payload_Class, ListNothing_Basic > >::xC_List_Entry
{
	public:
	
	virtual bool GetIterator( Iterator<ListObject_ListB> & o_iterator )
	{
		o_iterator.m_pointer = this;
		
		return true;
	}

};
*/


AT_DefineTest( Basic, ATList, "This is a basic test" ) , public ListFixture
{
	void Run()
	{

		ListObject_ListB		Object1[1];
		Object1->m_val = 1;

		ListObject_ListB		Object2[1];
		Object2->m_val = 2;

		ListObject_ListB		Object3[1];
		Object3->m_val = 3;

		ListObject_ListB::list		b;
		ListObject_ListA::list		a;

		a.push_back( * Object1 );
		
		b.push_back( * Object1 );

		a.push_back( * Object2 );
		a.push_back( * Object3 );
				
		b.push_back( * Object2 );
		b.push_back( * Object3 );
		
		ListObject_ListB::list::iterator	l_iter = b.begin();

		Payload_Class		* l_pl = &(*l_iter);

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		l_iter = b.begin();

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		AT_TCAssert( l_pl->m_val == 1, "Have the wrong value" );

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		AT_TCAssert( l_iter->m_val == 2, "Have the wrong value" );

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		AT_TCAssert( l_iter->m_val == 3, "Have the wrong value" );

		//
		// This is the main objective - the payload object must
		// be able to get it's iterator !
		//
		Iterator<ListObject_ListB>		l_upper_iter;
		
		l_pl->GetIterator( l_upper_iter );

		AT_TCAssert( b.begin() == l_upper_iter, "Iterator fails to compare" );

		l_iter = l_upper_iter;

		AT_TCAssert( b.begin() == l_iter, "Iterator fails to compare" );

		//
		// get the iterator for the other list
		
		Iterator<ListObject_ListA>		l_upper_iter_A;

		Payload_ParentA						* l_ppa = l_pl;
		
		l_ppa->GetIterator( l_upper_iter_A );

		AT_TCAssert( a.begin() == l_upper_iter_A, "A iterator also checks out" );

		Payload_Class * l_pl1 = static_cast<Payload_Class *>( l_ppa );
		
	}
};

AT_RegisterTest( Basic, ATList );


void tester_tester_inserter(
	ListObject_ListA::list		* a,
	ListObject_ListB			* b
)
{
	a->push_back( * b );
}


class Test_List1
  : public Default_List_Traits
{
public:
	template <
		typename		w_inheritor
	>
	class Payload
		: public w_inheritor
	{
		
		public:

		std::string			m_val;
	
		Payload()
			: m_val( "empty" )
		{
		}
	};

};

	
AT_DefineTest( List1, ATList, "Test the List1 template" ) , public ListFixture
{
	void Run()
	{

		typedef List1<Test_List1>			t_wrap_list_1;

		t_wrap_list_1::list_1					l_list;

		t_wrap_list_1::Apogee					l_item1;

		l_list.push_back( l_item1 );

		typedef List2<Test_List1>			t_wrap_list_2;

		t_wrap_list_2::list_1					l_list2_1;
		t_wrap_list_2::list_2					l_list2_2;

		t_wrap_list_2::Apogee					l_item2_a;
		t_wrap_list_2::Apogee					l_item2_b;
		t_wrap_list_2::Apogee					l_item2_c;
		t_wrap_list_2::Apogee					l_item2_d;

		l_list2_1.push_back( l_item2_a );
		l_list2_2.push_back( l_item2_a );

	}
};

AT_RegisterTest( List1, ATList );

class Test_List2
  : public Default_List_Traits
{
	public:
	template <
		typename		w_inheritor
	>
	class Payload
		: public w_inheritor
	{
		
		public:

		std::string			m_val;
	
		Payload()
			: m_val( "empty" )
		{
		}


		
	};

};


class ListFixture2
{
	public:

	typedef List2<Test_List2>			t_wrap_list_2;

	t_wrap_list_2::list_1					l_list2_1;
	t_wrap_list_2::list_2					l_list2_2;

	t_wrap_list_2::Apogee					l_item_a;
	t_wrap_list_2::Apogee					l_item_b;
	t_wrap_list_2::Apogee					l_item_c;
	t_wrap_list_2::Apogee					l_item_d;

	ListFixture2()
	{
		l_item_a.m_val = "l_item_a";
		l_item_b.m_val = "l_item_b";
		l_item_c.m_val = "l_item_c";
		l_item_d.m_val = "l_item_d";
	}

};


AT_DefineTest( List2, ATList, "Test the List2 template" ) , public ListFixture2
{
	void Run()
	{

		l_list2_1.push_back( l_item_a );
		l_list2_2.push_back( l_item_a );

		l_list2_1.push_back( l_item_b );
		l_list2_2.push_back( l_item_b );
		
		l_list2_1.push_back( l_item_c );
		l_list2_2.push_back( l_item_c );
		
		l_list2_1.push_back( l_item_d );
		l_list2_2.push_back( l_item_d );

		AT_TCAssert( l_list2_1.size() == 4, "Wrong number of elements" );
		
		AT_TCAssert( l_list2_2.size() == 4, "Wrong number of elements" );

		t_wrap_list_2::list_1::iterator l_iterator = t_wrap_list_2::GetIterator( l_list2_1, l_item_c );

		l_iterator.unlink();

		AT_TCAssert( l_list2_1.size() == 3, "Wrong number of elements" );

		l_iterator = t_wrap_list_2::GetIterator( l_list2_1, l_item_a );
		
		t_wrap_list_2::list_1					l_list_z;

		l_list_z.push_back( l_iterator );
		
		AT_TCAssert( l_list2_1.size() == 2, "Wrong number of elements" );
		AT_TCAssert( l_list_z.size() == 1, "Wrong number of elements" );
		
	}
};

AT_RegisterTest( List2, ATList );

class ListFixture3
{
	public:

	typedef List3<Test_List2>			t_wrap_list;

	t_wrap_list::list_1					l_list_1;
	t_wrap_list::list_2					l_list_2;
	t_wrap_list::list_3					l_list_3;

	t_wrap_list::Apogee					l_item_a;
	t_wrap_list::Apogee					l_item_b;
	t_wrap_list::Apogee					l_item_c;
	t_wrap_list::Apogee					l_item_d;

	ListFixture3()
	{
		l_item_a.m_val = "l_item_a";
		l_item_b.m_val = "l_item_b";
		l_item_c.m_val = "l_item_c";
		l_item_d.m_val = "l_item_d";
	}

};


AT_DefineTest( List3, ATList, "Test the List3 template" ) , public ListFixture3
{
	void Run()
	{

		l_list_1.push_back( l_item_a );
		l_list_2.push_back( l_item_a );
		l_list_3.push_back( l_item_a );

		l_list_1.push_back( l_item_b );
		l_list_2.push_back( l_item_b );
		l_list_3.push_back( l_item_b );
		
		l_list_1.push_back( l_item_c );
		l_list_2.push_back( l_item_c );
		l_list_3.push_back( l_item_c );
		
		l_list_1.push_back( l_item_d );
		l_list_2.push_back( l_item_d );
		l_list_3.push_back( l_item_d );

		AT_TCAssert( l_list_1.size() == 4, "Wrong number of elements" );
		
		AT_TCAssert( l_list_2.size() == 4, "Wrong number of elements" );

		AT_TCAssert( l_list_3.size() == 4, "Wrong number of elements" );

		t_wrap_list::list_3::iterator l_iterator = t_wrap_list::GetIterator( l_list_3, l_item_c );

		l_iterator.unlink();

		AT_TCAssert( l_list_3.size() == 3, "Wrong number of elements" );

		l_iterator = t_wrap_list::GetIterator( l_list_3, l_item_a );
		
		t_wrap_list::list_3					l_list_z;

		l_list_z.push_back( l_iterator );
		
		AT_TCAssert( l_list_3.size() == 2, "Wrong number of elements" );
		AT_TCAssert( l_list_z.size() == 1, "Wrong number of elements" );
		
	}
};

AT_RegisterTest( List3, ATList );


class ListFixture4
{
	public:

	typedef List4<Test_List2>			t_wrap_list;

	t_wrap_list::list_1					l_list_1;
	t_wrap_list::list_2					l_list_2;
	t_wrap_list::list_3					l_list_3;
	t_wrap_list::list_4					l_list_4;

	t_wrap_list::Apogee					l_item_a;
	t_wrap_list::Apogee					l_item_b;
	t_wrap_list::Apogee					l_item_c;
	t_wrap_list::Apogee					l_item_d;
	
	t_wrap_list::Apogee					l_item_X;

	ListFixture4()
	{
		l_item_a.m_val = "l_item_a";
		l_item_b.m_val = "l_item_b";
		l_item_c.m_val = "l_item_c";
		l_item_d.m_val = "l_item_d";
	}

};


AT_DefineTest( List4, ATList, "Test the List4 template" ) , public ListFixture4
{
	void Run()
	{

		l_list_1.push_back( l_item_a );
		l_list_2.push_back( l_item_a );
		l_list_3.push_back( l_item_a );
		l_list_4.push_back( l_item_a );

		l_list_1.push_back( l_item_b );
		l_list_2.push_back( l_item_b );
		l_list_3.push_back( l_item_b );
		l_list_4.push_back( l_item_b );
		
		l_list_1.push_back( l_item_c );
		l_list_2.push_back( l_item_c );
		l_list_3.push_back( l_item_c );
		l_list_4.push_back( l_item_c );
		
		l_list_1.push_back( l_item_d );
		l_list_2.push_back( l_item_d );
		l_list_3.push_back( l_item_d );
		l_list_4.push_back( l_item_d );

		AT_TCAssert( l_list_1.size() == 4, "Wrong number of elements" );
		
		AT_TCAssert( l_list_2.size() == 4, "Wrong number of elements" );

		AT_TCAssert( l_list_3.size() == 4, "Wrong number of elements" );

		AT_TCAssert( l_list_4.size() == 4, "Wrong number of elements" );

		t_wrap_list::list_4::iterator l_iterator = t_wrap_list::GetIterator( l_list_4, l_item_c );

		l_iterator.unlink();

		AT_TCAssert( l_list_4.size() == 3, "Wrong number of elements" );

		l_iterator = t_wrap_list::GetIterator( l_list_4, l_item_a );
		
		t_wrap_list::list_4					l_list_z;

		l_list_z.push_back( l_iterator );
		
		AT_TCAssert( l_list_4.size() == 2, "Wrong number of elements" );
		AT_TCAssert( l_list_z.size() == 1, "Wrong number of elements" );

		l_item_X = l_item_a;

		l_iterator = t_wrap_list::GetIterator( l_list_4, l_item_X );

		AT_TCAssert( ! l_iterator.is_linked(), "copied empty iterator is not at end" );
	}
};

AT_RegisterTest( List4, ATList );


#include "at_lifetime.h"

#include <iostream>

class NoisyRC
{
    public:

    void AddRef()
    {
        std::cout << "AddRef called\n";
    }

    void Release()
    {
        std::cout << "Release called\n";
    }

};


//class RCListObject_ListB;
class RCPayload_Class;
typedef ListHead_Basic<
    RCPayload_Class,
    RCPayload_Class,
    List_RefCountTraits< RCPayload_Class, ListNothing_Basic > >     RCListObject_ListA;
    
typedef ListHead_Basic< RCPayload_Class, RCListObject_ListA >       RCListObject_ListB;

class RCPayload_ParentA
  : public ListAccessorDeclare<RCListObject_ListA, ListElementBaseEx<RCPayload_Class, NoisyRC> >
{
};

class RCPayload_ParentB
  : public ListAccessorDeclare<RCListObject_ListB,RCPayload_ParentA>
{
};

class RCPayload_Class
  : public RCPayload_ParentB
{
	
public:

	int				m_val;

	RCPayload_Class()
	  : m_val( -1 )
	{
	}
};


AT_DefineTest( RCBasic, ATList, "This is a basic test" ) , public ListFixture
{
	void Run()
	{

		RCListObject_ListB      Object1[1];
		Object1->m_val = 1;

		RCListObject_ListB		Object2[1];
		Object2->m_val = 2;

		RCListObject_ListB		Object3[1];
		Object3->m_val = 3;

		RCListObject_ListB::list		b;
		RCListObject_ListA::list		a;

		a.push_back( * Object1 );
		
		b.push_back( * Object1 );

		a.push_back( * Object2 );
		a.push_back( * Object3 );
				
		b.push_back( * Object2 );
		b.push_back( * Object3 );
		
		RCListObject_ListB::list::iterator	l_iter = b.begin();

		RCPayload_Class		* l_pl = &(*l_iter);

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		l_iter = b.begin();

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		AT_TCAssert( l_pl->m_val == 1, "Have the wrong value" );

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		AT_TCAssert( l_iter->m_val == 2, "Have the wrong value" );

		l_iter ++;

std::cout << "VALUE IS " << l_iter->m_val << "\n";

		AT_TCAssert( l_iter->m_val == 3, "Have the wrong value" );

		//
		// This is the main objective - the payload object must
		// be able to get it's iterator !
		//
		Iterator<RCListObject_ListB>		l_upper_iter;
		
		l_pl->GetIterator( l_upper_iter );

		AT_TCAssert( b.begin() == l_upper_iter, "Iterator fails to compare" );

		l_iter = l_upper_iter;

		AT_TCAssert( b.begin() == l_iter, "Iterator fails to compare" );

		//
		// get the iterator for the other list
		
		Iterator<RCListObject_ListA>		l_upper_iter_A;

		RCPayload_ParentA						* l_ppa = l_pl;
		
		l_ppa->GetIterator( l_upper_iter_A );

		AT_TCAssert( a.begin() == l_upper_iter_A, "A iterator also checks out" );

		RCPayload_Class * l_pl1 = static_cast<RCPayload_Class *>( l_ppa );
		
	}
};

AT_RegisterTest( RCBasic, ATList );



class Test_RCList2
  : public Default_List_Traits
{
    public:
    
    typedef NoisyRC         Root_Base;
      
    template <
        typename    w_container_wrapper
    >
    class Entry_1_Traits
      : public List_RefCountTraits< w_container_wrapper, ListNothing_Basic >
    {
    };
    
	public:
	template <
		typename		w_inheritor
	>
	class Payload
		: public w_inheritor
	{
		
		public:

		std::string			m_val;
	
		Payload()
			: m_val( "empty" )
		{
		}
		
	};

};


class RCListFixture2
{
	public:

    typedef List2<Test_RCList2>             t_wrap_list_2;

	t_wrap_list_2::list_1					l_list2_1;
	t_wrap_list_2::list_2					l_list2_2;

	t_wrap_list_2::Apogee					l_item_a;
	t_wrap_list_2::Apogee					l_item_b;
	t_wrap_list_2::Apogee					l_item_c;
	t_wrap_list_2::Apogee					l_item_d;

	RCListFixture2()
	{
		l_item_a.m_val = "l_item_a";
		l_item_b.m_val = "l_item_b";
		l_item_c.m_val = "l_item_c";
		l_item_d.m_val = "l_item_d";
	}

};


AT_DefineTest( RCList2, ATList, "Test the RCList2 template" ) , public RCListFixture2
{
	void Run()
	{

		l_list2_1.push_back( l_item_a );
		l_list2_2.push_back( l_item_a );

		l_list2_1.push_back( l_item_b );
		l_list2_2.push_back( l_item_b );
		
		l_list2_1.push_back( l_item_c );
		l_list2_2.push_back( l_item_c );
		
		l_list2_1.push_back( l_item_d );
		l_list2_2.push_back( l_item_d );

		AT_TCAssert( l_list2_1.size() == 4, "Wrong number of elements" );
		
		AT_TCAssert( l_list2_2.size() == 4, "Wrong number of elements" );

		t_wrap_list_2::list_1::iterator l_iterator = t_wrap_list_2::GetIterator( l_list2_1, l_item_c );

		l_iterator.unlink();

		AT_TCAssert( l_list2_1.size() == 3, "Wrong number of elements" );

		l_iterator = t_wrap_list_2::GetIterator( l_list2_1, l_item_a );
		
		t_wrap_list_2::list_1					l_list_z;

		l_list_z.push_back( l_iterator );
		
		AT_TCAssert( l_list2_1.size() == 2, "Wrong number of elements" );
		AT_TCAssert( l_list_z.size() == 1, "Wrong number of elements" );
		
	}
};

AT_RegisterTest( RCList2, ATList );


