/*
 *
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 */

/**
 * tst_xml_parser.cpp
 *
 * tst_xml_parser.cpp tests the XML parser.
 *
 */

#include "at_unit_test.h"
#include "at_assert.h"
#include "at_xml_parser.h"
#include "at_xml_printer.h"

using namespace at;

namespace XmlParserUnitTest
{
    AT_TestArea( XmlParser, "XML Parser test suite" );

    const char vcprojDocument[] = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
"<VisualStudioProject\n"
"	ProjectType=\"Visual C++\"\n"
"	Version=\"7.10\"\n"
"	Name=\"nctv_wmf_sdk_support\"\n"
"	ProjectGUID=\"{98D0C962-36A6-48D9-AD53-947DE5184964}\"\n"
"	Keyword=\"Win32Proj\">\n"
"	<Platforms>\n"
"		<Platform\n"
"			Name=\"Win32\"/>\n"
"	</Platforms>\n"
"	<Configurations>\n"
"		<Configuration\n"
"			Name=\"Debug|Win32\"\n"
"			OutputDirectory=\"Debug\"\n"
"			IntermediateDirectory=\"Debug\"\n"
"			ConfigurationType=\"4\"\n"
"			CharacterSet=\"2\">\n"
"			<Tool\n"
"				Name=\"VCCLCompilerTool\"\n"
"				Optimization=\"0\"\n"
"				PreprocessorDefinitions=\"WIN32;_DEBUG;_LIB\"\n"
"				MinimalRebuild=\"TRUE\"\n"
"				BasicRuntimeChecks=\"3\"\n"
"				RuntimeLibrary=\"5\"\n"
"				UsePrecompiledHeader=\"0\"\n"
"				WarningLevel=\"3\"\n"
"				Detect64BitPortabilityProblems=\"TRUE\"\n"
"				DebugInformationFormat=\"4\"/>\n"
"			<Tool\n"
"				Name=\"VCCustomBuildTool\"/>\n"
"			<Tool\n"
"				Name=\"VCLibrarianTool\"\n"
"				OutputFile=\"$(OutDir)/nctv_wmf_sdk_support.lib\"/>\n"
"			<Tool\n"
"				Name=\"VCMIDLTool\"/>\n"
"			<Tool\n"
"				Name=\"VCPostBuildEventTool\"/>\n"
"			<Tool\n"
"				Name=\"VCPreBuildEventTool\"/>\n"
"			<Tool\n"
"				Name=\"VCPreLinkEventTool\"/>\n"
"			<Tool\n"
"				Name=\"VCResourceCompilerTool\"/>\n"
"			<Tool\n"
"				Name=\"VCWebServiceProxyGeneratorTool\"/>\n"
"			<Tool\n"
"				Name=\"VCXMLDataGeneratorTool\"/>\n"
"			<Tool\n"
"				Name=\"VCManagedWrapperGeneratorTool\"/>\n"
"			<Tool\n"
"				Name=\"VCAuxiliaryManagedWrapperGeneratorTool\"/>\n"
"		</Configuration>\n"
"		<Configuration\n"
"			Name=\"Release|Win32\"\n"
"			OutputDirectory=\"Release\"\n"
"			IntermediateDirectory=\"Release\"\n"
"			ConfigurationType=\"4\"\n"
"			CharacterSet=\"2\">\n"
"			<Tool\n"
"				Name=\"VCCLCompilerTool\"\n"
"				PreprocessorDefinitions=\"WIN32;NDEBUG;_LIB\"\n"
"				RuntimeLibrary=\"4\"\n"
"				UsePrecompiledHeader=\"0\"\n"
"				WarningLevel=\"3\"\n"
"				Detect64BitPortabilityProblems=\"TRUE\"\n"
"				DebugInformationFormat=\"3\"/>\n"
"			<Tool\n"
"				Name=\"VCCustomBuildTool\"/>\n"
"			<Tool\n"
"				Name=\"VCLibrarianTool\"\n"
"				OutputFile=\"$(OutDir)/nctv_wmf_sdk_support.lib\"/>\n"
"			<Tool\n"
"				Name=\"VCMIDLTool\"/>\n"
"			<Tool\n"
"				Name=\"VCPostBuildEventTool\"/>\n"
"			<Tool\n"
"				Name=\"VCPreBuildEventTool\"/>\n"
"			<Tool\n"
"				Name=\"VCPreLinkEventTool\"/>\n"
"			<Tool\n"
"				Name=\"VCResourceCompilerTool\"/>\n"
"			<Tool\n"
"				Name=\"VCWebServiceProxyGeneratorTool\"/>\n"
"			<Tool\n"
"				Name=\"VCXMLDataGeneratorTool\"/>\n"
"			<Tool\n"
"				Name=\"VCManagedWrapperGeneratorTool\"/>\n"
"			<Tool\n"
"				Name=\"VCAuxiliaryManagedWrapperGeneratorTool\"/>\n"
"		</Configuration>\n"
"	</Configurations>\n"
"	<References>\n"
"	</References>\n"
"	<Files>\n"
"		<Filter\n"
"			Name=\"Source Files\"\n"
"			Filter=\"cpp;c;cxx;def;odl;idl;hpj;bat;asm;asmx\"\n"
"			UniqueIdentifier=\"{4FC737F1-C7A5-4376-A066-2A32D752A2FF}\">\n"
"		</Filter>\n"
"		<Filter\n"
"			Name=\"Header Files\"\n"
"			Filter=\"h;hpp;hxx;hm;inl;inc;xsd\"\n"
"			UniqueIdentifier=\"{93995380-89BD-4b04-88EB-625FBE52EBFB}\">\n"
"		</Filter>\n"
"		<Filter\n"
"			Name=\"Resource Files\"\n"
"			Filter=\"rc;ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe;resx\"\n"
"			UniqueIdentifier=\"{67DA6AB6-F800-4c08-8B7A-83BB121AAD01}\">\n"
"		</Filter>\n"
"		<File\n"
"			RelativePath=\"./ReadMe.txt\">\n"
"		</File>\n"
"	</Files>\n"
"	<Globals>\n"
"	</Globals>\n"
"</VisualStudioProject>";

    // ======== XML Parser Testing =============================================

    AT_DefineTest( VCProjTest, XmlParser, "VCProj Test" )
    {
        void Run()
        {
            // Parse the vcproj XML document
            Ptr< XmlDocument* > l_document = XmlParseMemory( vcprojDocument, strlen( vcprojDocument ) );

            // Collect all the configuration elements
            XmlNodeList l_confList = l_document->GetElementsByTagName( "Configuration" );
            const XmlNodeList::size_type l_confNum = l_confList.Length();
            AT_TCAssert( l_confNum == 2, "Test of XmlDocument::GetElementsByTagName failed" );

            // Look for a Configuration element with Name="Release|Win32"
            PtrDelegate< XmlElement* > l_relWin32;
            for ( XmlNodeList::size_type i = 0; i < l_confNum; i++ )
            {
                PtrView<  XmlElement* > l_confElt = l_confList.Item( i )->AsElement();
                if ( l_confElt->GetAttribute( "Name" ) == "Release|Win32" )
                {
                    l_relWin32 = l_confElt;
                }
            }
            AT_TCAssert( l_relWin32, "There should be a Configuration element with Name=\"Release|Win32\"" );
            AT_TCAssert( l_relWin32->HasChildNodes(), "Test of method HasChildNodes failed" );
            AT_TCAssert( l_relWin32->HasAttributes(), "Test of method HasAttributes failed" );

            // Add an attribute
            const std::string l_newAttrName( "NewAttribute" );
            const std::string l_newAttrValue( "an attribute value" );
            l_relWin32->SetAttribute( l_newAttrName , l_newAttrValue );
            AT_TCAssert( l_relWin32->GetAttribute( l_newAttrName ) == l_newAttrValue,
                         "Test of XmlElement::SetAttribute failed" );

            // Remove the attribute we've just added
            l_relWin32->RemoveAttribute( l_newAttrName );
            AT_TCAssert( l_relWin32->GetAttribute( l_newAttrName ) == std::string(),
                         "Test of XmlElement::RemoveAttribute failed" );
        }
    };

    AT_RegisterTest( VCProjTest, XmlParser );


    // ======== XML Parser Testing =============================================

    AT_DefineTest( PrintTest, XmlParser, "Create a simple xml doc and print it" )
    {
        void Run()
        {
            // create a basic document
            at::Ptr< at::XmlDocument* > l_document( new at::XmlDocument() );

            // create the main document node
            at::Ptr< at::XmlElement* > l_element = l_document->CreateElement( "packages" );
            l_document->AppendChild( l_element );
            
            at::Ptr< at::XmlElement* > l_sub_element = l_document->CreateElement( "package" );
            l_element->AppendChild( l_sub_element );

            l_sub_element->SetAttribute( "big", "gun" );
            
            at::Ptr< at::XmlElement* > l_sub_sub_element = l_document->CreateElement( "sub_package" );

            l_sub_element->AppendChild( l_sub_sub_element );

            at::XmlPrettyPrint( std::cout, l_document );
        }
    };

    AT_RegisterTest( PrintTest, XmlParser );

    // ======== XML Parser Testing =============================================

    AT_DefineTest( BadFile, XmlParser, "Try parsing a bad xml doc from a file" )
    {
        void ParseFile( const char* file )
        {
            std::list< FileStatusReport_Basic > l_errlist;
            if ( ! XmlParseFile( file, &l_errlist ) )
            {
                std::cerr << std::endl;
                for (
                    std::list< FileStatusReport_Basic >::const_iterator
                        l_iter = l_errlist.begin(),
                        l_end = l_errlist.end();
                    l_iter != l_end;
                    ++l_iter
                    )
                {
                    std::cerr << *l_iter << std::endl;
                }
            }
        }

        void Run()
        {
            ParseFile( "./unexisting.xml" );
            ParseFile( "./bad.xml" );
        }
    };

    AT_RegisterTest( BadFile, XmlParser );

} // namespace
