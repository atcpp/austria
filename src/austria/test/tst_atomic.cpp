

#include "at_unit_test.h"

#include "at_atomic.h"

using namespace at;

AT_TestArea( Atomic, "Atomic operations" );

#include <iostream>

class TestThing
{
    public:

    int a;
};

class AtomicFixture
{
    public:

    AtomicFixture()
      : m_a( 3 ),
        m_b( new TestThing ),
        m_c( 4 )
    {
    }

    AtomicCount                 m_a;

    AtomicExchange_Ptr<TestThing *> m_b;

    AtomicExchange_Val<int>         m_c;

};

AT_DefineTest( Basic, Atomic, "Test basic atomic operations" )
    , public AtomicFixture
{
    
	void Run()
	{
        AT_TCAssert( ( ++ m_a ) == 4, "Pre-increment should result in 4" );
        AT_TCAssert( ( -- m_a ) == 3, "Pre-decrement should result in 3" );
        AT_TCAssert( ( m_a ++ ) == 3, "Post-increment should result in 3" );
        AT_TCAssert( ( m_a -- ) == 4, "Post-decrement should result in 4" );

        AT_TCAssert( m_a.Bump( 10 ) == 3, "Bump by 10 should return original value of 3" );
        AT_TCAssert( m_a.Get() == 13, "Bump by 10 should make it 13" );

        volatile AtomicCountType    val = 30;

        AT_TCAssert( 30 == AtomicBump( & val, -30 ), "Atomic bump failed" );

        AT_TCAssert( val == 0, "Atomic bump failed" );

        TestThing   * l_v1 = m_b.Get();
        delete l_v1;

        TestThing   * l_v2 = m_b.Exchange( 0 );

        AT_TCAssert( l_v2 == l_v1, "Exchanged pointer is not the same as the real pointer" );

        AT_TCAssert( m_b.Get() == 0, "Pointer was not set to 0" );
        
        int         l_v3 = m_c.Exchange( 0 );

        AT_TCAssert( l_v3 == 4, "Exchanged value is not the same as the initial value" );

        AT_TCAssert( m_c.Get() == 0, "Exchanged value not set to 0" );

        int         l_v4 = m_c.CompareExchange( 2, 0 );

        AT_TCAssert( l_v4 == 0, "Exchanged value is not the same as the initial value" );

        AT_TCAssert( m_c.Get() == 2, "Exchanged value not set to 2" );

        int         l_v5 = m_c.CompareExchange( 3, 0 );

        AT_TCAssert( l_v5 == 2, "Should be 2\n" );

        AT_TCAssert( m_c.Get() == 2, "Exchanged value should remain set to 2" );
        
	}
};

AT_RegisterTest( Basic, Atomic );

