/*
 *
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 */

/**
 * tst_file.cpp
 *
 * tst_file.cpp tests file handling
 *
 */


#include "at_unit_test.h"
#include "at_assert.h"
#include "at_file.h"
#include "at_dir.h"
#include "at_thread.h"
#include "at_twin_basic.h"
#include "at_twinmt_basic.h"

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace at;

#pragma warning (disable : 4101)

namespace FileUnitTest 
{

    const char testtext1[] = {"WASHINGTON (CNN) -- The U.S. military faces between 13,000 and 17,000 insurgents in Iraq, the \
large majority of them backers of ousted Iraqi leader Saddam Hussein and his Baath Party, a senior military official said Tuesday.\
Those figures came to light the same day an apparent suicide bombing killed more than 20 people in central Baghdad as they waited\
in line to apply be police officers, U.S. and Iraqi officials said.\
The bulk of the insurgency is made up of 12,000 to 15,000 Arab Sunni \
followers of Saddams party, the official told CNN. \
The Baath Party was overthrown by a U.S. led invasion in March 2003. \
Of those, the source said 5,000 to 7,000 are considered committed fighters, with the rest considered fencesitters\
criminals or facilitators who contribute material support or sanctuary to the guerrillas. \
The official, who is familiar with the region, said about 500 other fighters have come from other countries to battle \
the U.S.-led forces in Iraq, while another group of fewer than 1,000 are believed to be followers of Jordanian-born \
Islamic terrorist Abu Musab al-Zarqawi.  \
Members of Congress have been pressing senior officers for an assessment of the strength of the insurgency since Iraq's January 30 elections. \
At a Senate Armed Services Committee hearing last week, Sen. John McCain criticized Gen. Richard Myers, chairman of\
the Joint Chiefs of Staff, for lacking a readily available estimate of the armed opposition."};
    const char testtext2[] = {"'I don't know how you defeat an insurgency unless you have some handle on the number\
of people that you are facing,' the Arizona Republican said. \
Told by Myers that some numbers exist but are classified, McCain said, 'I think the American people should know the extent of the enemy we are fa' \
The numbers are considerably higher than the 5,000 fighters that Gen. John Abizaid, head of the U.S. Central Command, estimated in November 2003. \
The Pentagon cautioned, however, that trends are difficult to track.\
The official who provided Tuesday's estimate said the U.S. military believes it killed between 10,000\
and 15,000 guerillas in combat last year perhaps as many as 3,000 during the November push to retake the\
western Iraqi city of Falluja from insurgents. \
But because others join the insurgency to replace those killed, Pentagon analysts have difficulty \
matching the current number against previous \
In the wake of the elections, in which Iraqis turned out to vote for a transitional parliament, \
U.S. commanders expressed hope that Iraqis will \rethink their commitment to the insurgency. \
Bombings target police \
In Tuesday morning's bombing, at least 22 people were killed and nearly 30 wounded, said \
Thair al-Nakib, a spokesman for interim Iraqi Prime Min \
Ayad Allawi. \
'To attack and brutally murder patriotic and innocent Iraqis on their way to volunteer to protect their homeland \
is a crime against all people of Iraq,' al-Nakib said in a statement. \
A representative with the U.S. 1st Cavalry Division said 27 people were wounded in the blast, \
which witnesses said took place at an Iraqi army post. \
Meanwhile, an Iraqi politician survived an attempt on his life Tuesday, but his two sons and a bodyguard were killed, police said. \
Unknown gunmen opened fire on Mithal al-Alousi's convoy in Baghdad, police said. Al-Alousi is the general secretary of the Iraqi Nation \
Democratic Party."};
    const char testtext3[] = { "Tuesday's violence followed a pair of suicide bombings Monday against police that \
killed 27 Iraqis, officials said. \
In Mosul, a suicide bomber outside Jumhuriya Hospital summoned policemen to him and detonated a bomb, killing 12 and wounding four, \
officials and witnesses said. \
About a half-hour later near a Baquba police station, an explosives-laden taxi blew up, killing 15 people, police said. \
Col. Dana Pittard, commander of the 3rd Brigade, U.S. 1st Infantry Division, said the dead were civilians looking for work \
as police officers or in other position at the station. \
The car bomb was within 50 feet (15 meters) of the police complex and was inside a security cordon at the time of the blast. \
Other developments \
# The electoral list backed by the Iraq's top Shiite clerics is likely to claim the office of prime minister but \
'will work with' the country's Sunni and Kurdish minorities to fill other top posts, one of Iraq's deputy presidents, \
Ibrahim Jafari al-Eshaiker, said Tuesday. His Dawa movement is part of the United Iraqi \
Alliance, which early returns suggest is headed for a majority in the transitional National Assembly. \
# Iraq will resume exporting oil from northern oil fields, a spokesman with the Northern Oil Co. said Tuesday. \
The office of the general manager said exportation would be 250,000 barrels per day. Oil exportation through \
northern pipelines to Turkey was suspended after sabotage attacks. \
# U.S. Secretary of State Condoleezza Rice, in Paris on Tuesday, attempted to mend the rift between France and the \
United States over the Iraq war, saying it is 'time to turn away from the disagreements of the past.' \
"};

AT_TestArea( Files, "File test suite" );

at::Mutex g_lead_count_lock; 
at::Conditional g_lead_conditional ( g_lead_count_lock );

/**
 *  Buffer Class for Files
 */
class AUSTRIA_EXPORT FileBuffer
{
    public: 

        static at::Ptr< at::Pool * > m_pool;

        FileBuffer ( Int64 i_length ) : m_buf ( 0 ) 
        {
            Create ();
            {
                if ( m_buf )
                {
                    m_buf.Get ()->SetAllocated ( i_length );
                }
            }
        }

        FileBuffer () : m_buf ( 0 ) 
        {
            Create ();
        }

        void SetLength ( Int64 i_length )
        {
            if ( m_buf ) 
            {
                m_buf.Get ()->SetAllocated ( i_length );
            }
        }

        Int64 GetLength () const
        {
            if ( m_buf ) 
            {
                return m_buf.Get ()->Region ().m_allocated;
            }

            return 0;
        }

        void SetData ( unsigned char* bytearray, Int64 i_length = -1 )
        {
            Int64 length;
            if ( i_length == -1 )
                length = GetLength ();
            else
                length = i_length;

            if ( m_buf )
            {
                char* _str = new char[ length ];
                memcpy ( _str, bytearray, length );

                SetLength ( length );
                m_buf.Get ()->Set ( _str );

                delete [] _str;
            }
        }

        void SetData ( const std::string& bytearray )
        {
            if ( m_buf )
            {
                SetLength ( bytearray.length () );
                m_buf.Get ()->Set ( bytearray );
            }
        }

        operator const at::PtrDelegate < at::Buffer* > ()
        {
            return m_buf;
        }

        operator const at::PtrView < at::Buffer* > ()
        {
            return m_buf;
        }

        operator const at::Ptr < at::Buffer* > ()
        {
            return m_buf;
        }

        at::PtrDelegate < at::Buffer* > GetData () const
        {
            return m_buf;
        }

        std::string String () const
        {
            AT_Assert ( m_buf );

            return m_buf.Get ()->Region ().String ();
        }

        void* GetRaw () const
        {
            if ( m_buf )
                return static_cast<void *> ( m_buf.Get ()->Region ().m_mem ); 

            return 0;
        }

    protected:

    void Create ()
    {
        if ( GetPool () )
        {
	        m_buf = m_pool->Create ();
        }
    }

    at::Ptr< at::Pool * > GetPool () const
    {
        if ( m_pool == 0 )
        {
            m_pool = at::FactoryRegister< at::Pool, at::DKy >::Get().Create( "Basic" )();
        }

        return m_pool;
    }
    private:
        at::Ptr< at::Buffer* > m_buf;
};

/**
*  File Buffer Pool [[ TO DO Deletion ]]
*/
at::Ptr< at::Pool * > FileBuffer::m_pool;

class LeadApplication : public LeadTwinMT_Basic< FileNotifyLeadInterface >
{
	public:
	
    // counter is preset from main thread, then decremented by the lead::cancel routine
    // as async i/o completes successfully OR WITH AN ERROR
    // it is safe for the main thread to preset the count as long as there are no background
    // async i/o still pending
    //
    static int s_count;     // protect all Read/decrement access with Mutex g_lead_count_lock

    at::Conditional m_cond;
	at::Mutex m_mutex;
	bool m_done;
	
    LeadApplication () : m_cond ( m_mutex )
	{
	    m_done = false;
	    m_index = 0;
	}
	
	LeadApplication ( int i_index ) : m_cond ( m_mutex )
	{
	    m_done = false;
	    m_index = i_index;
	}
	
	~LeadApplication ()
	{
	}
    
    virtual void IOCompleted ( FileContext* o_filecontext, const FileErrorBasic &i_fe )
    {
        if ( i_fe == FileError::s_success )
        {
            at::PtrDelegate<Buffer*> buf = o_filecontext->GetBuffer ();
        }
        else
        {
        	std::cout << "\n Async IO ERROR " << i_fe.What () << std::endl;
        }
    }

    virtual void Cancel(void)
    {
		// cancel twin before posting waiting threads who want to destruct us
		this->LeadCancel();
        {
			// activity completion
            Lock<Mutex> l_m ( m_mutex );
	        m_done = true;
            m_cond.Post ();
        }

        {

            // internal i/o completion
            Lock<Mutex> g_l( g_lead_count_lock );
	        if ( --s_count == 0 )
	        {
	            std::cout << "\nLead test counter reached Zero!\n";
                g_lead_conditional.Post ();
	        }
	        else
		        std::cout << " S" << s_count << " ";
        }
    }

    protected:
	    int m_index;
};

// A global File for MultiThreaded testing
RWFile g_rwFile;

// A global Mutex for MultiThreaded testing
at::Mutex g_fileLock;

#define NUM_SINGLEWRITES 2000
#define NUM_THREADS 100
#define WRITE_BLOCK 1024*1024*4
#define ASYNC_WRITES 20

class FileThread : public Task, public LeadApplication
{
    public:
        FileThread ( int i_index ) : LeadApplication ( i_index ) {}
        ~FileThread () 
        { 
			Wait ();
        }

        void Work ()
        {
            Task::TaskID tid = Task::GetSelfId ();
            std::cout << "[" << int(m_index) << " Thread id=" << int(tid) << " is Working\n";

            {
                Lock < Mutex > m_l ( g_fileLock );

                if ( !g_rwFile.IsOpen () )
                {
                    g_rwFile.Open ( FilePath ( "rwfile.txt" ), FileAttr::Create | FileAttr::Async );
                    g_rwFile.SetLength ( NUM_THREADS * 21 );
                }

            }

			std::ostringstream message;
			message << "[" << int(m_index) << "Thread id=" << std::setw(4) << int(tid) << " ";

            FileBuffer fbuf ( 21 );
            fbuf.SetData ( message.str() );

            {
                Lock < Mutex > m_l ( g_fileLock );

                g_rwFile.Seek ( m_index * 21, BaseFile::SeekBegin );
                if ( false == g_rwFile.WriteAsync ( fbuf, this ) )
                {
                	std::cout << "====>WARNING: FileThread Write Failed - " << g_rwFile.GetError ().What () << std::endl;
                	
                	return;
                }

            }

			// Wait here for IOComplete or destructor to Unlock Us
            Lock<Mutex> l_m( m_mutex );
            if (!m_done)
            {
                m_cond.Wait ();
            }
        }
};

int LeadApplication::s_count = 0;   // protect all access with g_lead_count_lock

// ======== File Testing =============================================

AT_DefineTest( AG_SimpleRead, Files, "Simple Read" )
{
 void Run()
 {
        // --------======= Simplest Test for reading a whole File ========--------
        RFile RF ( FilePath ( "Test.Text" ) );
        if ( RF.Open () )
        {
            FileBuffer fbuf ( RF.GetLength () );
            RF.Read ( fbuf );
            std::cout << fbuf.String () << std::endl;
            RF.Close ();
        }

    }
};

AT_RegisterTest( AG_SimpleRead, Files );

AT_DefineTest( AF_MultiWriteAsync, Files, "Multi Write Async" )
{
     void Run ()
    {   // ------================== Multi Write ==================-----

        // TEST: Multiple Writes of a small number of bytes from the same Thread
        LeadApplication::s_count = NUM_SINGLEWRITES;
        LeadApplication *leadwrite[NUM_SINGLEWRITES];
        FileBuffer file_buffer[NUM_SINGLEWRITES];

        WFile wfmultiple;
        if ( wfmultiple.Open ( FilePath ( "multiwrite.test" ), FileAttr::Async | FileAttr::Create ) )
        {

            // This is necessary because the offset sepecified in the write
            // may not yet be valid
			wfmultiple.SetLength ( NUM_SINGLEWRITES * 5 );
			
            for ( int w = 0; w < NUM_SINGLEWRITES; w++ )
            {
				std::ostringstream message;
				message << std::setw(4) << w << "\n";
                std::cout << w << "W+ "; //buffer << std::endl;
                file_buffer[w].SetLength ( 5 );
				file_buffer[w].SetData ( message.str() );
                wfmultiple.Seek ( w * 5, BaseFile::SeekBegin );
                leadwrite[w] = new LeadApplication ( w );
                if ( false == wfmultiple.WriteAsync ( file_buffer[w], leadwrite[w] ) )
                {
                	std::cout << "###===> WARNING: Write Failed - " << wfmultiple.GetError ().What () << std::endl;
                }
                // Every 10 writes yield some time
                if ( w % 10 )
                {
                    at::Task::Sleep ( at::TimeInterval( 1 * at::TimeInterval::PerMilliSec ));
                }
            }

            std::cout << "\nWaiting for all Multi Write Async Tests to complete\n";
            
            {
                Lock<Mutex> g_l( g_lead_count_lock );
                if ( LeadApplication::s_count != 0 )
                {
                    g_lead_conditional.Wait ();
                }
            }
			
            wfmultiple.Close ();

            for ( int w = 0; w < NUM_SINGLEWRITES; w++ )
            {
            	delete leadwrite[w];
            }
        }
    }
};

AT_RegisterTest( AF_MultiWriteAsync, Files );

AT_DefineTest( AE_MultiWriteThreadAsync, Files, "Multi Write Thread Async" )
{
    void Run ()
    {
        // TEST: Multiple Threads Writing to Same file 
        LeadApplication::s_count = NUM_THREADS;

        FileThread *ft[NUM_THREADS];

        // Start the Threads Running
        for ( int t = 0 ; t  < NUM_THREADS ; t ++ )
        {
            ft[t] = new FileThread ( t );
            ft[t]->Start ();
        }

        // Wait for them All to Finish
        int s;
        for ( s = 0 ; s < NUM_THREADS ; s ++ )
        {
            ft[s]->Wait ();
        }

        std::cout << "\nWaiting for all Multi Write Thread Async tests to complete\n";
        
        {
            Lock<Mutex> g_l( g_lead_count_lock );
            std::cout << LeadApplication::s_count << "\n";
            if ( LeadApplication::s_count != 0 )
            {
                g_lead_conditional.Wait ();
            }
        }
        
        // Close the test File
        if ( g_rwFile.IsOpen () )
        {
            g_rwFile.Close ();
        }

        // Delete the threads
        for ( s = 0 ; s < NUM_THREADS ; s ++ )
        {
            delete ft[s];
        }

    }
};

AT_RegisterTest( AE_MultiWriteThreadAsync, Files );

AT_DefineTest( AC_BasicTests, Files, "Basic file Tests" )
{
    void Run ()
    {
        bool bTest;   // test passed or failed
	    int test = 0; // number of tests that should complete

	    // Quick and dirty compile time test! Perhaps there needs to be a harness to test compile time 
	    // unit teats
	    FileAttr i_attr = FileAttr::Write | FileAttr::Create | FileAttr::Async;
	    // ============ Uncomment the line below to check compile time error checking ====
	    // FileAttr i_attr_bad = FileAttr::Write | FileAttr::Read;

	    // Test Files 
	    FilePath fp1 ( "test.fp1" ); // Must not Exist
	    FilePath fp2 ( "test.fp2" ); // Must Exist and be a Read Only File
	    FilePath fp3 ( "test.fp3" ); // Must not Exist

	    // *** Do the test Setup 
	    // This setup actually uses some of the Functions so
	    // There might be the situation where the test setup is incorrect
	    // Perhaps there needs to be a way to setup the unit test environment

	    try
	    {
			    // Make sure fp1 doesn't Exist
			    // if it does set permission and delete it 

			    std::cout << std::endl;
			    std::cout << "Initializing " << fp1.String () << std::endl;

			    RFile RF1 ( fp1 );
			    if ( BaseFile::Exists ( fp1 ) )
                {
                    RF1.SetPermissions ( FilePermission::UserWrite );
                }
				if ( RF1.Open () )
			    {
				    RF1.Close ();
				    if ( false == BaseFile::Remove ( fp1 ) )
				    {
					    std::cout << "Cannot remove " << fp1.String () << "!!" << std::endl;
                        throw;
				    }
			    }
			    else
			    {
                    // cant open the file for reading Access Denied!
    			    if ( RF1.GetError () == FileError::s_access_denied )
				    {
				        std::cout << "Access Denied changing Permissions " << std::endl;
				        RF1.SetPermissions ( FilePermission::UserWrite | FilePermission::UserRead );
				        if ( false == BaseFile::Remove ( fp1 ) )
				        {
					        std::cout << "Cannot remove " << fp1.String () << "!!" << std::endl;
                            throw;
				        }
				    }
			    }

			    std::cout << "Initializing " << fp2.String () << std::endl;

			    // Make Sure fp2 Exists 
			    WFile WF ( fp2, FileAttr::Write | FileAttr::Create );
			    if ( !BaseFile::Exists ( fp2 ) )
			    {
				    // If not create it 
				    if ( WF.Open () )
				    {
					    // It has to be readonly by everybody
                        WF.SetPermissions ( FilePermission::UserRead | FilePermission::GroupRead  );
					    WF.Close ();
				    }
			    }
			    WF.SetPermissions ( FilePermission::UserRead );

			    std::cout << "Initializing " << fp3.String () << std::endl;

			    // Make Sure Fp3 Does not Exist
			    RFile RF3 ( fp3 );
		    	if ( BaseFile::Exists ( fp3 ) )
                {
                    RF3.SetPermissions ( FilePermission::UserWrite );
                }
                if ( RF3.Open () )
			    {
				    RF3.Close ();
				    if ( false == BaseFile::Remove ( fp3 ) )
				    {
 					    std::cout << "Cannot remove " << fp3.String () << "!!" << std::endl;
					    throw;
				    }
			    }
	    }
	    catch ( const FileError & fe )
	    {
		    std::cerr << "Error during unit test intialization. The error is " << fe.What () << std::endl;

		    throw;
	    }
	    catch ( ... )
	    {
		    std::cerr << " Exception thrown - exiting test." << std::endl;

		    throw;
	    }
    	
	    // Test Setting attributes of RFile, WFile and RWFile Class Types
	    // Test Mode Flags

	    // Correct
	    std::cout << "RFile Open test using " << fp2.String () << std::endl;
	    {RFile RF1;
	    AT_TCAssert ( true == RF1.Open ( fp2 ), "RFile with Open should not fail" );}
    	
	    // Illegal
	    {RFile RF1;
	    AT_TCAssert ( false == RF1.Open ( fp2, FileAttr::Write ), "RFile Open Mode with Write should fail" );}

	    // Illegal 
	    {RFile RF1;
	    AT_TCAssert ( false == RF1.Open ( fp2, FileAttr::Create ), "RFile Open Mode with Create should fail" );}

	    // Illegal 
	    {RFile RF1;
	    AT_TCAssert ( false == RF1.Open ( fp2, FileAttr::ReadWrite ), "RFile Open Mode with ReadWrite sholud fail" );}

	    // Illegal 
	    // ============ NOTE: Uncomment these 2 lines to check compile time error checking ====
	    //{RFile RF1;
	    //AT_TCAssert ( false != RF1.Open ( fp1, FileAttr::Read | FileAttr::Write ), "Invalid Open Mode Not Caught" );}

    	
	    // Correct - Default Reader
	    {RFile RF2 ( fp2 );
	    AT_TCAssert ( true == RF2.Open (), "RFile Construction with Open should not fail when file does not exist");}

	    // Illegal 
	    {RFile RF2 ( fp2, FileAttr::Write );
	    AT_TCAssert ( false == RF2.Open (), "RFile Construction with Write should fail" );}

	    // Illegal 
	    {RFile RF2 ( fp2, FileAttr::Create );
	    AT_TCAssert ( false == RF2.Open (), "RFile Construction with Create should fail" );}

	    // Illegal 
	    {RFile RF2 ( fp2, FileAttr::ReadWrite );
	    AT_TCAssert ( false == RF2.Open (), "RFile Construction with ReadWrite should fail" );}

	    // Illegal 
	    // ============ NOTE: Uncomment these 2 lines to check compile time error checking ====
	    //{RFile RF2 ( FileAttr::Read | FileAttr::Write );
	    //AT_TCAssert ( false == RF2 () ), "RFile Construction with Write should fail" );}

	    std::cout << "WFile Open test using " << fp1.String () << std::endl;
	    // Correct 
	    {WFile WF1;
	    AT_TCAssert ( false == WF1.Open ( fp1 ), "WFile with Open should fail if the file does not exist" );}
    	
	    // Correct
	    {WFile WF1;
	    AT_TCAssert ( true == WF1.Open ( fp1, FileAttr::Create ), "WFile Open Mode with Create should not fail" );}

	    // Correct - File Should Now Exist
	    {WFile WF1;
	    AT_TCAssert ( true == WF1.Open ( fp1 ), "WFile with Open should not fail" );}

	    // Illegal
	    {WFile WF1;
	    AT_TCAssert ( false == WF1.Open ( fp1, FileAttr::Read ), "WFile Open Mode with Read should fail" );}

	    // Illegal 
	    {WFile WF1;
	    AT_TCAssert ( false == WF1.Open ( fp1, FileAttr::ReadWrite ), "WFile Open Mode with ReadWrite should fail" );}

	    // Illegal 
	    // ============ NOTE: Uncomment these 2 lines to check compile time error checking ====
	    //{WFile WF1;
	    //AT_TCAssert ( false == WF1.Open ( fp1, FileAttr::Read | FileAttr::Write ), "Invalid Open Mode Not Caught" );}

    	
	    // Correct
	    {WFile WF1 ( fp1, FileAttr::Create );
	    AT_TCAssert ( true == WF1.Open (), "WFile Open Mode with Create should not fail" );}

	    // Correct - File Should Now Exist
	    {WFile WF1 ( fp1 );
	    AT_TCAssert ( true == WF1.Open (), "WFile with Open should not fail" );}

	    // Illegal
	    {WFile WF1 ( fp1, FileAttr::Read );
	    AT_TCAssert ( false == WF1.Open (), "WFile Construction with Read should fail" );}

	    // Illegal 
	    {WFile WF1 ( fp1, FileAttr::ReadWrite );
	    AT_TCAssert ( false == WF1.Open (), "WFile Construction with ReadWrite should fail" );}

	    // Illegal 
	    // ============ NOTE: Uncomment these 2 lines to check compile time error checking ====
	    //{WFile WF1 ( fp1, FileAttr::Read | FileAttr::Write );
	    //AT_TCAssert ( false != WF1.Open (), "Invalid Open Mode Not Caught" );}
    	
	    std::cout << "Reset Test Conditions for RWFile tests " << std::endl;
	    RFile RF1 ( fp1 );
		if ( BaseFile::Exists ( fp1 ) )
        {
            RF1.SetPermissions ( FilePermission::UserWrite );
        }
        if ( RF1.Open () )
		{
			RF1.Close ();
			if ( false == BaseFile::Remove ( fp1 ) )
			{
			    std::cout << "Cannot remove " << fp1.String () << "!!" << std::endl;
                throw;
			}
		}
		else
		{
            // cant open the file for reading Access Denied!
    		if ( RF1.GetError () == FileError::s_access_denied )
		    {
			    std::cout << "Access Denied - changing Permissions " << std::endl;
				RF1.SetPermissions ( FilePermission::UserWrite | FilePermission::UserRead );
			    if ( false == BaseFile::Remove ( fp1 ) )
			    {
			        std::cout << "Cannot remove " << fp1.String () << "!!" << std::endl;
                    throw;
			    }
		    }
		}

	    std::cout << "RWFile Open test using " << fp1.String () << std::endl;
	    // Correct 
	    {RWFile RW1;
	    AT_TCAssert ( false == RW1.Open ( fp1 ), "RWFile with Open should fail if the file does not exist" );}
    	
	    // Correct
	    {RWFile RW1;
	    AT_TCAssert ( true == RW1.Open ( fp1, FileAttr::Create ), "RWFile Open Mode with Create should not fail" );}

	    // Correct - File Should Now Exist
	    
	    {RWFile RW1 ( fp1 ); 
	    AT_TCAssert ( true == RW1.Open (), "RWFile with Open should not fail" );}

	    // Illegal
	    {RWFile RW1;
	    AT_TCAssert ( false == RW1.Open ( fp1, FileAttr::Read ), "RWFile Open Mode with Read should fail" );}

	    // Illegal
	    {RWFile RW1;
	    AT_TCAssert ( false == RW1.Open ( fp1, FileAttr::Write ), "RWFile Open Mode with Write should fail" );}

	    // Illegal 
	    {RWFile RW1;
	    AT_TCAssert ( true == RW1.Open ( fp1, FileAttr::ReadWrite ), "RWFile Open Mode with ReadWrite should not fail" );}

	    // Illegal 
	    // ============ NOTE: Uncomment these 2 lines to check compile time error checking ====
	    //{RWFile RW1;
	    //AT_TCAssert ( false != RW1.Open ( fp1, FileAttr::Read | FileAttr::Write ), "Invalid Open Mode Not Caught" );}

    	
	    // Correct
	    {RWFile RW1 ( fp1, FileAttr::Create );
	    AT_TCAssert ( true == RW1.Open (), "RWFile Construction with Create should not fail" );}

	    // Correct - File Should Now Exist
	    {RWFile RW1 ( fp1 );
	    AT_TCAssert ( true == RW1.Open (), "RWFile with Open should not fail" );}

	    // Illegal
	    {RWFile RW1( fp1, FileAttr::Read );
	    AT_TCAssert ( false == RW1.Open (), "RWFile Construction with Read should fail" );}

	    // Illegal 
	    {RWFile RW1 ( fp1, FileAttr::Write );
	    AT_TCAssert ( false == RW1.Open (), "RWFile Construction with Write should fail" );}

	    // Illegal 
	    {RWFile RW1 ( fp1, FileAttr::ReadWrite );
	    AT_TCAssert ( true == RW1.Open (), "RWFile Construction with ReadWrite should not fail" );}

	    // Illegal 
	    // ============ NOTE: Uncomment these 2 lines to check compile time error checking ====
	    //{RWFile RW1 ( fp1, FileAttr::Read | FileAttr::Write );
	    //AT_TCAssert ( false != RW1.Open (), "Invalid Open Mode Not Caught" );}
    	
	    // =================== Check Exists, Remove and Rename ================================
	    try
	    {
		    test = 1;

		    // Check fp1 still Exists
		    bTest = BaseFile::Exists ( fp1 ); 
		    AT_TCAssert ( bTest == true, "fp1 Does not exist it should." );

		    test++;
		    // Create fp2 (It exists!)
		    WFile WF2 ( fp2, FileAttr::Write | FileAttr::Create );
		    AT_TCAssert ( false == WF2.Open (), "Write|Create on existing File should fail" );
    			
		    test++;
		    // fp2 must exist! Check fp2 Exists function
		    bTest = BaseFile::Exists ( fp2 ); 
		    AT_TCAssert ( bTest == true, "fp2 must exist" );

		    test++;
		    // Try to Delete a readonly file ( Ok on Linux Not OK on Windows! )
		    WF2.SetPermissions ( FilePermission::UserRead );
		    //bTest = BaseFile::Remove ( fp2);
		    //AT_TCAssert ( bTest == false, "Deleted a readonly File" );

		    test++;
		    // Change Permissions to Write and try to delete
		    WF2.SetPermissions ( FilePermission::UserWrite );
		    bTest = BaseFile::Remove ( fp2);
		    AT_TCAssert ( bTest == true, "Can't delete a readonly a write File" );

		    test++;
		    // Fp2 is now gone try to close it.
		    WF2.Close ();

		    test++;
		    // Create fp2 it should Pass
		    bTest = WF2.Open ( fp2, FileAttr::Write | FileAttr::Create );
		    AT_TCAssert ( bTest == true, "Write|Create on deleted file Failed!" );

		    test++;
		    // Test fp2 exists
		    bTest = BaseFile::Exists ( fp2 ); 
            AT_TCAssert ( bTest == true, "fp2 does not exist after successful Create" );

            test++;
            // Rename fp1 to fp3 check fp3 exists
            BaseFile::Rename ( fp1, fp3);
            bTest = BaseFile::Exists ( fp3 ); 
            AT_TCAssert ( bTest == true, "fp3 does not exisit after rename" );

            test++;
            // Try to Remove fp1 ( it doesnt exist )
            bTest = BaseFile::Remove ( fp1 );
            AT_TCAssert ( bTest == false, "fp1 could not be removed after renaming" );

            test++;
            // Test for existance
            bTest = BaseFile::Exists ( fp1 ); 
            AT_TCAssert ( bTest == false, "fp1 exisits after a rename" );

            test++;
            // Create a new fp1
            RWFile RW1;
            bTest = RW1.Open ( fp1, FileAttr::ReadWrite | FileAttr::Create );
            AT_TCAssert ( bTest == true, "Unable to create an RWFile" );
            RW1.Close ();

            test++;
            // Try to Set Attributes on a Closed File 
            if ( BaseFile::Exists ( fp1 ) )
            {
                RW1.SetPermissions ( FilePermission::UserRead );
            }

            test++;
            // Rename fp3 back to fp1 (fp1 is read only)
            BaseFile::Rename ( fp3, fp1);
            bTest = BaseFile::Exists ( fp1 ); 
            AT_TCAssert ( bTest == true, "Should not be able to rename a file to an exisitng readonly file"  );

            // Set RW1 back to Write
            if ( BaseFile::Exists ( fp1 ) )
            {
                RW1.SetPermissions ( FilePermission::UserWrite );
            }
     }
     catch ( const FileError & fe )
     {
        std::cerr << "Caught an FileError Exception during File Open Tests " << fe.What () << " Caught at Step #" << test << std::endl;

        throw;
     }
     catch ( ... )
     {
        std::cerr << "Unknown Exception!!" <<std::endl;

        throw;
     }


     // ============ SetLength GetLength LockRange UnlockRange Seek ----------- 

     // Test Files 
     FilePath fp4 ( "test.fp4" ); // Write
     FilePath fp5 ( "test.fp5" ); // Read 
     FilePath fp6 ( "test.fp6" ); // ReadWrite

     WFile WF4 ( fp4, FileAttr::Write | FileAttr::Create );
     WFile WF5 ( fp5, FileAttr::Write | FileAttr::Create );
     WFile WF6 ( fp6, FileAttr::Write | FileAttr::Create );

     try
     {
        test = 0;

        // Check the test files exisit if not create them
        // Make Sure fp2 Exists 
        if ( !BaseFile::Exists ( fp4 ) )
        {
            // If not create it 
            if ( WF4.Open () )
            {
                WF4.Close ();
            }
        }

        if ( !BaseFile::Exists ( fp5 ) )
        {
            // If not create it 
            if ( WF5.Open () )
            {
                WF5.Close ();
            }
        }

        if ( !BaseFile::Exists ( fp6 ) )
        {
            // If not create it 
            if ( WF6.Open () )
            {
                WF6.SetPermissions ( FilePermission::UserWrite | FilePermission::UserRead );
                WF6.Close ();
            }
        }

        // Set/Get the length of Fp4 
        if ( WF4.Open () )
        {
            // Small Size
            bTest = WF4.SetLength ( 8 );
            AT_TCAssert ( bTest == true, "Setlength ( 8 ) failed" );
            long length = WF4.GetLength ();
            AT_TCAssert ( length == 8, "Getlength ( 8 ) failed" );

            // Zero Size
            bTest = WF4.SetLength ( 0 );
            AT_TCAssert ( bTest == true, "Setlength ( 0 ) failed" );
            length = WF4.GetLength ();
            AT_TCAssert ( length == 0, "Getlength ( 0 ) failed" );

            // Large Size
            bTest = WF4.SetLength ( 1024*1024*1024 );
            AT_TCAssert ( bTest == true, "Setlength ( 1024*1024*1024 ) failed" );
            length = WF4.GetLength ();
            AT_TCAssert ( length == 1024*1024*1024, "Getlength ( 1024*1024*1024 ) failed" );

            // Invalid Size (Negative)
            bTest = WF4.SetLength ( -1 );
            AT_TCAssert ( bTest == false, "Setlength ( -1 ) should have failed" );
            if ( bTest == false )
            {
                std::cout << "SetLength -1 returned error " << WF4.GetError ().What () << std::endl;
            }
            length = WF4.GetLength ();
            AT_TCAssert ( length == 1024*1024*1024, "Getlength ( -1 ) failed" );

            WF4.Close ();
        }
    }
    catch ( const FileError & fe )
    {
        std::cerr << "Caught an FileError Exception during File Seek " << fe.What () << " Caught at Step #" << test << std::endl;

        throw;
    }
    catch ( ... )
    {
        std::cerr << "Unknown Exception!!" <<std::endl;

        throw;
    }

    WF4.SetPermissions ( FilePermission::UserWrite );
    if ( WF4.Open () )
    {
        // WF4 Should still be a gig in size
        // =========================================== Perform Seek Tests ========================
        long pos = WF4.Seek ( 0, BaseFile::SeekBegin );
        AT_TCAssert ( pos == 0, "Seek ( 0, FILE_BEGIN ) failed" );

        pos = WF4.Seek ( 1024*1024, BaseFile::SeekCurrent );
        AT_TCAssert ( pos == 1024*1024, "Seek ( 1024*1024, FILE_CURRENT ) failed" );

        pos = WF4.Seek ( 1024*1024*512, BaseFile::SeekCurrent );
        AT_TCAssert ( pos == 1024*1024 + 1024*1024*512, "Seek ( 1024*1024*512, FILE_CURRENT ) failed" );

        //pos = WF4.Seek ( 1024*1024*512, BaseFile::SeekEnd );
        //AT_TCAssert ( pos == 1024*1024*512, "Seek ( 1024*1024*512, FILE_END ) failed" );

        bool catchme = false;
        try 
        {
            pos = WF4.Seek ( -1, BaseFile::SeekBegin );
        }
        catch ( const FileError & fe )
        {
            std::cout << "Seek -1 returned error " << WF4.GetError ().What () << std::endl;
            catchme = true;
        }
        AT_TCAssert ( catchme == true, "Seek ( -1, FILE_CURRENT ) failed" );

        catchme = false;
        try 
        {
            pos = WF4.Seek ( -1, BaseFile::SeekCurrent );
        }
        catch ( const FileError & fe )
        {
            std::cout << "Seek -1 SeekCurrent returned error " << WF4.GetError ().What () << std::endl;
            AT_TCAssert ( false, "Seek -1 SeekCurrent failed" );
        }
        // AT_TCAssert ( pos == 1024*1024*512 - 1, "Seek ( -1, FILE_CURRENT ) failed" );
     }


     // =========================================== Perform Read and Write Tests ========================
     try 
     {
        WFile WF4 (fp5),WF5 ( fp4 );
        WF4.SetPermissions ( FilePermission::UserWrite );
        WF5.SetPermissions ( FilePermission::UserWrite );

        if ( WF5.Open () && WF4.Open () )
        {
            // Allocate a 15 Byte
            FileBuffer l_buffer ( 15 );
            l_buffer.SetData ( "123456789012345" );

            WF5.Write ( l_buffer );
            WF4.Write ( l_buffer );

            // Go to the end of WF5 and write it again for next test
            WF5.Seek ( 15, BaseFile::SeekBegin );
            WF5.Write ( l_buffer );

            WF4.Close ();
            WF5.Close ();
        }

        RFile RF5 ( fp5 );
        RF5.SetPermissions ( FilePermission::UserRead );
        if ( RF5.Open () )
        {
            FileBuffer l_buffer ( 15 );
            // Read the contents of the file
            RF5.Read ( l_buffer );
            std::string charread  = l_buffer.String ();
            AT_TCAssert ( l_buffer.GetLength () == 15, "Read incorrect number of bytes RF5 " );
            AT_TCAssert ( charread == "123456789012345", "Read incorrect string from RF5 " );
            // Output the String for Fun
            std::cout << charread << std::endl;

            RF5.Close ();
        }

        RWFile RW6 ( fp6 );
        // RW6.SetPermissions ( FilePermission::UserRead | FilePermission::UserWrite );
        if ( RW6.Open () )
        {
            FileBuffer l_buffer;

            // Allocate a 15 Byte buffer
            l_buffer.SetLength ( 15 );
            l_buffer.SetData ( "123456789012345" );

            RW6.Write ( l_buffer ); 

            RW6.Seek ( 0 , BaseFile::SeekBegin );
            RW6.Read ( l_buffer );

            std::string charread  = l_buffer.String ();
            AT_TCAssert ( l_buffer.GetLength () == 15, "Read incorrect number of bytes from Test.FP5" );
            AT_TCAssert ( charread == "123456789012345", "Read incorrect string from Test.FP5" );
            // Output the String 
            std::cout << "R/W Read Write test: " << charread << std::endl;

            RW6.Close ();
        }
     }
     catch ( const FileError & fbe )
     {
        std::cout << "Caught Exception during Read Write Tests - " << fbe.What () << std::endl;

        throw;
     }

     // =========================================== Perform Lock Region Tests ===================
     // =========================================== Perform DirectIO Tests ======================
     // =========================================== Perform HardLink Tests ======================

    try
    {
        FilePath fp7 ( "test.fp7" );
        FilePath fp8 ( "test.fp8" );

        bTest = BaseFile::Exists ( fp7 );
        AT_TCAssert ( bTest == false, "fp7 Exists; it shouldn't." );

        bTest = BaseFile::Exists ( fp8 );
        AT_TCAssert ( bTest == false, "fp8 Exists; it shouldn't." );

        WFile WF7 ( fp7, FileAttr::Write | FileAttr::Create );
        AT_TCAssert ( true == WF7.Open (), "Couldn't create fp7." );
        WF7.SetPermissions ( FilePermission::UserWrite | FilePermission::UserRead );
        WF7.Close ();

        RWFile RW7 ( fp7 );
        AT_TCAssert ( true == RW7.Open (), "Couldn't open fp7." );

        {
            FileBuffer l_buffer;

            l_buffer.SetLength ( 3 );
            l_buffer.SetData ( "FOO" );

            RW7.Write ( l_buffer ); 
        }
        RW7.Close ();

        bTest = BaseFile::HardLink( fp7, fp8 );
        AT_TCAssert ( bTest == true, "HardLink() returned error." );
        
        bTest = BaseFile::Exists ( fp8 );
        AT_TCAssert ( bTest == true, "fp8 doesn't exist; it should." );

        RWFile RW8 ( fp8 );
        AT_TCAssert ( true == RW8.Open (), "Couldn't open fp8." );

        {
            FileBuffer l_buffer;

            l_buffer.SetLength ( 3 );
            l_buffer.SetData ( "   " );

            RW8.Read ( l_buffer );

            std::string charread  = l_buffer.String ();
            AT_TCAssert ( l_buffer.GetLength () == 3, "Read incorrect number of bytes from fp8" );
            AT_TCAssert ( charread == "FOO", "Read incorrect string from fp8" );

            l_buffer.SetData ( "BAR" );

            RW8.Seek ( 0 , BaseFile::SeekBegin );

            RW8.Write ( l_buffer ); 
        }
        RW8.Close ();

        bTest = BaseFile::Exists ( fp7 );
        AT_TCAssert ( bTest == true, "fp7 doesn't exist; it should." );

        AT_TCAssert ( true == RW7.Open (), "Couldn't open fp7." );

        {
            FileBuffer l_buffer;

            l_buffer.SetLength ( 3 );
            l_buffer.SetData ( "   " );

            RW7.Read ( l_buffer );

            std::string charread  = l_buffer.String ();
            AT_TCAssert ( l_buffer.GetLength () == 3, "Read incorrect number of bytes from fp7" );
            AT_TCAssert ( charread == "BAR", "Read incorrect string from fp7" );

        }
        RW7.Close ();

        bTest = BaseFile::Remove ( fp7 );

        bTest = BaseFile::Exists ( fp7 );
        AT_TCAssert ( bTest == false, "fp7 exists; it shouldn't." );

        bTest = BaseFile::Exists ( fp8 );
        AT_TCAssert ( bTest == true, "fp8 doesn't exist; it should." );

        bTest = BaseFile::Remove ( fp8 );

        bTest = BaseFile::Exists ( fp8 );
        AT_TCAssert ( bTest == false, "fp8 exists; it shouldn't." );

    }
    catch ( const FileError & fe )
    {
        std::cerr << "Caught a FileError Exception during HardLink tests " << fe.What () << " Caught at Step #" << test << std::endl;

        throw;
    }
    catch ( ... )
    {
        std::cerr << "Unknown Exception!!" <<std::endl;

        throw;
    }

    return;
 }

};

AT_RegisterTest( AC_BasicTests, Files );

AT_DefineTest ( AB_FileBuffer, Files, "File Buffer Tests" )
{
    void Run ()
    {
        // Test FileBuffer 
        unsigned char ucdata[16] = "012345678901234";
        FileBuffer ucarray ( 16 );
        ucarray.SetData ( ucdata );
        unsigned char* ucresult = (unsigned char*)ucarray.GetRaw ();
        AT_TCAssert ( 0 == memcmp ( ucdata, ucresult, 15 ), "FileBuffer Error" );
    }
};
AT_RegisterTest( AB_FileBuffer, Files );

AT_DefineTest ( AD_Async, Files, "Async Tests" )
{
    void Run ()
    {
       	std::cout << "\n 400 Simultaneous Reads Test\n";
        // 400 Simultaneous reads
        LeadApplication* la[400];

        // Do test however number of times
        for ( int y = 0; y < 1; y++ )
        {
            std::cout << "\nTEST RUN " << y << std::endl;
            LeadApplication::s_count = 400;

            RFile RFA ( FilePath ( "Test.Text" ) );
            RFA.SetPermissions ( FilePermission::UserRead | FilePermission::UserWrite );
            if ( RFA.Open ( FilePath ( "Test.Text" ), FileAttr::Read | FileAttr::Async ) )
            {
                for ( int i = 0 ; i < 400; i++ )
                {
                    FileBuffer rfabuffer ( 8 );
                    RFA.Seek ( i*8, BaseFile::SeekBegin );
                    la[i] = new LeadApplication ( i );
                    std::cout << " R+ ";
                    if ( false == RFA.ReadAsync ( rfabuffer, la[i] ) )
                        {
                        	std::cout << "Async Read failed " << RFA.GetError ().What () << std::endl;
                        }
                    // Something else goes on
                    if ( i % 3 )
                        at::Task::Sleep ( at::TimeInterval( 1 * at::TimeInterval::PerMilliSec ) );
                }
                RFA.Close ();
            }

            std::cout << "\nWaiting For All Read Async Tests to complete\n";
            
            {
                Lock<Mutex> g_l( g_lead_count_lock );
                if ( LeadApplication::s_count != 0 )
                {
                    g_lead_conditional.Wait ();
                }
            }
        }

        std::cout << "Starting to Delete Lead Applications " << std::endl;
        for ( int l = 0; l < 400; l++ )
        {
            delete la[l];
        }
        std::cout << "Done Deleteing Lead Applications " << std::endl;
         
     	FilePath fpasyncw  ( "testasync.write" );
     	FilePath fpasyncr  ( "testasync.read"  );
     	FilePath fpasyncrw ( "testasync.rw"    );

        WFile WFAW ( fpasyncw );
        if ( BaseFile::Exists ( fpasyncw ) )
        {
            WFAW.SetPermissions ( FilePermission::UserWrite );
        }
        if ( WFAW.Open ( fpasyncw, FileAttr::Create | FileAttr::Write ) )
        {
            WFAW.Close ();
        }
        else
        {
            AT_TCAssert ( false, "Async File Test Create Error" );
        }

        if ( WFAW.Open ( fpasyncr, FileAttr::Create | FileAttr::Write ) )
        {
   			// Allocate a 15 Byte buffer
            FileBuffer l_buffer ( 15 );
   			l_buffer.SetData ( "1234512345123\r\n" );

            WFAW.Write ( l_buffer );
            WFAW.Write ( l_buffer );
            WFAW.Write ( l_buffer );
            WFAW.Write ( l_buffer );
            WFAW.Write ( l_buffer );

            WFAW.Close ();
        }
        else
        {
            AT_TCAssert ( false, "Async File Test Create Error" );
        }

        {
        	std::cout << "\n" << ASYNC_WRITES << " Simultaneous writes test\n";
            WFile WFA1; 
            if ( WFA1.Open ( fpasyncw, FileAttr::Write | FileAttr::Create | FileAttr::Async ) )
         	{
         		WFA1.SetLength ( ASYNC_WRITES * WRITE_BLOCK );
       			LeadApplication *lead[ASYNC_WRITES];
                LeadApplication::s_count = ASYNC_WRITES;

    			int i;
                for ( i = 0; i < ASYNC_WRITES; i++ )
                {
                    FileBuffer l_buffer ( WRITE_BLOCK );
           			l_buffer.SetData ( "===>" );
           			try
           			{
            			WFA1.Seek ( i*WRITE_BLOCK, BaseFile::SeekBegin );
                        lead[i] = new LeadApplication ( i );
     					std::cout << " W+ ";
            			if ( false == WFA1.WriteAsync ( l_buffer, lead[i] ) ) // Buffer is transferered at this time
            			{
            				std::cout << "===>>WARNING: Write failure - " << WFA1.GetError ().What () << std::endl;
            			}
                        if ( i % 3 )
                        {
                            at::Task::Sleep ( at::TimeInterval( 1 * at::TimeInterval::PerMilliSec ));
                        }
                    }
           			catch ( const FileError & fe )
           			{
            			std::cout << "Error While Writing Async " << fe.What () << std::endl;

                        throw;
           			}
                }

                std::cout << "\nWaiting for all Async Write Tests to complete\n";
                
                {
                    Lock<Mutex> g_l( g_lead_count_lock );
                    if ( LeadApplication::s_count != 0 )
                    {
                        g_lead_conditional.Wait ();
                    }
                }

                // Clean Up the Leads
                for ( i = 0; i < ASYNC_WRITES; i++ )
                {
                    delete lead[i];
                }

                std::cout << "\nWrite Seek Tests\n";

                FileBuffer l_buffer1;
       			FileBuffer l_buffer2;
       			FileBuffer l_buffer3;

                l_buffer1.SetLength ( 1024 );
       			l_buffer1.SetData ( "1111111111111111" );

                l_buffer2.SetLength ( 1024 );
       			l_buffer2.SetData ( "2222222222222222" );

                l_buffer3.SetLength ( 1024 );
       			l_buffer3.SetData ( "3333333333333333" );

                LeadApplication::s_count = 3;
                LeadApplication *leadtest[3];
                

                for ( i = 0; i < 3; i++ )
                {
                    leadtest[i] = new LeadApplication ( i );
                }

       			try
       			{
				    WFA1.Seek ( 0, BaseFile::SeekBegin );
            		if ( false == WFA1.WriteAsync ( l_buffer1, leadtest[0] ) ) // Buffer is transferered at this time
            		{
            			std::cout << "===>>WARNING: Write Seek failure - " << WFA1.GetError ().What () << std::endl;
            		}

				    WFA1.Seek ( 16, BaseFile::SeekBegin );
				    if (false == WFA1.WriteAsync ( l_buffer2, leadtest[1] ))
            		{
            			std::cout << "===>>WARNING: Write Seek failure - " << WFA1.GetError ().What () << std::endl;
            		}

				    WFA1.Seek ( 16 + 16, BaseFile::SeekBegin );
                    if (false ==  WFA1.WriteAsync ( l_buffer3, leadtest[2] ))
            		{
            			std::cout << "===>>WARNING: Write Seek failure - " << WFA1.GetError ().What () << std::endl;
            		}
			    }
			    catch ( const FileError & fe )
			    {
				    std::cout << "Error While Writing Async " << fe.What () << std::endl;

                    throw;
			    }

                std::cout << "\nWaiting for Write Seek tests to complete\n";
                
                {
                    Lock<Mutex> g_l( g_lead_count_lock );
                    if ( LeadApplication::s_count != 0 )
                    {
                        std::cout << "wait count=" << LeadApplication::s_count << std::endl;
                        g_lead_conditional.Wait ();
                    }
                }

                WFA1.Close ();
                WFA1.SetPermissions ( FilePermission::UserRead );

                for ( i = 0; i < 3; i++ )
                {
                    delete leadtest[i];
                }
	        }
        }

        {
        	std::cout << "\nRead Seek Tests\n";
            LeadApplication *leadtest[3];
            LeadApplication::s_count = 3;
            
            for ( int i = 0; i < 3; i++ )
            {
                leadtest[i] = new LeadApplication ( i );
            }

            RFile RFA1;
            if ( RFA1.Open ( fpasyncw, FileAttr::Read | FileAttr::Async ) )
	        {
                FileBuffer l_buffer[3];
			    // Allocate a 16 Byte buffer
			    l_buffer[0].SetLength ( 16 );
			    l_buffer[1].SetLength ( 16 );
			    l_buffer[2].SetLength ( 16 );

			    // Test EOF by reading 16 bytes at a time, Read only reads the size of the buffer
			    RFA1.Seek ( 1, BaseFile::SeekBegin );
			    if ( false == RFA1.ReadAsync ( l_buffer[0], leadtest[0] ) )
			    {
			    	std::cout << "==>WARNING: Read Error - " << RFA1.GetError ().What () << std::endl;
			    }
			    RFA1.Seek ( 16, BaseFile::SeekBegin );
			    if ( false == RFA1.ReadAsync ( l_buffer[1], leadtest[1] ) )
			    {
			    	std::cout << "==>WARNING: Read Error - " << RFA1.GetError ().What () << std::endl;
			    }
			    RFA1.Seek ( 16 + 16, BaseFile::SeekBegin );
			    if ( false == RFA1.ReadAsync ( l_buffer[2], leadtest[2] ) )
			    {
			    	std::cout << "==>WARNING: Read Error - " << RFA1.GetError ().What () << std::endl;
			    }

                std::cout << "\nWaiting for Read Seeks to Complete\n";
                {
                    Lock<Mutex> g_l( g_lead_count_lock );
                    if ( LeadApplication::s_count != 0 )
                    {
                        g_lead_conditional.Wait ();
                    }
                }
                int index;
			    for ( index = 0; index < 3; index++ )
                {
                    std::string charread  = l_buffer[index].String ();
			        std::cout << "Read (" << index << "): " << charread << std::endl;
                }


			    // REPEAT TEST USING NEW SEEK PARAM
                LeadApplication::s_count = 3;

                // Test EOF by reading 16 bytes at a time, Read only reads the size of the buffer
			    if ( false == RFA1.ReadAsync ( l_buffer[0], leadtest[0], 1, BaseFile::SeekBegin ) )
			    {
			    	std::cout << "==>WARNING: Read Error - " << RFA1.GetError ().What () << std::endl;
			    }
			    if ( false == RFA1.ReadAsync ( l_buffer[1], leadtest[1],16,BaseFile::SeekBegin ) )
			    {
			    	std::cout << "==>WARNING: Read Error - " << RFA1.GetError ().What () << std::endl;
			    }
			    if ( false == RFA1.ReadAsync ( l_buffer[2], leadtest[2],16 + 16, BaseFile::SeekBegin ) )
			    {
			    	std::cout << "==>WARNING: Read Error - " << RFA1.GetError ().What () << std::endl;
			    }

                std::cout << "\nWaiting for Read with Seek to Complete\n";
                {
                    Lock<Mutex> g_l( g_lead_count_lock );
                    if ( LeadApplication::s_count != 0 )
                    {
                        g_lead_conditional.Wait ();
                    }
                }

			    for ( index = 0; index < 3; index++ )
                {
                    std::string charread  = l_buffer[index].String ();
			        std::cout << "Read With Seek (" << index << "): " << charread << std::endl;
                }

				// Reset Count
                LeadApplication::s_count = 3;

				// NOTE: Using the same Buffer for async reads causes problems
			    // Try to Read 1024 past EOF
			    RFA1.Seek ( 0, BaseFile::SeekBegin );
			    l_buffer[0].SetLength ( 1024 );
			    bool bTest = RFA1.ReadAsync ( l_buffer[0], leadtest[0] );
			    if ( false == bTest )
			    {
			    	std::cout << "Seek 0 Read 1024 Failed!!\n";
			    }

			    // Read 10 Bytes from the begining in less than 10ms
			    // Reuse l_buffer[0] to show badness 
			    RFA1.Seek ( 0, BaseFile::SeekBegin );
			    l_buffer[1].SetLength ( 10 );
			    bTest = RFA1.ReadAsync ( l_buffer[1], leadtest[1] );
			    if ( false == bTest )
			    {
			    	std::cout << "Seek 0 Read 10 Failed!!\n";
			    }

			    // Seek 15 bytes in and read 99 bytes in the fastest time possible
			    RFA1.Seek ( 15, BaseFile::SeekBegin );
			    l_buffer[2].SetLength ( 99 );
			    bTest = RFA1.ReadAsync ( l_buffer[2], leadtest[2] );
			    if ( false == bTest )
			    {
			    	std::cout << "Seek 15 Read 99 Failed!!\n";
			    }

                {
                    Lock<Mutex> g_l( g_lead_count_lock );
                    if ( LeadApplication::s_count != 0 )
                    {
                        g_lead_conditional.Wait ();
                    }
                }
		        
			    for ( int index = 0; index < 3; index++ )
                {
                    std::string charread  = l_buffer[index].String ();
			        std::cout << "Read (" << index << "): " << charread << std::endl;
                }

       			RFA1.Close ();
         	}
         	else
         		{
                    Lock<Mutex> g_l( g_lead_count_lock );
					LeadApplication::s_count = 0;		        	
         			AT_TCAssert ( false, "RFileAsync Open Error!\n ");
         		}
         	
            for (int i=0;i<3;i++)
            {
				delete leadtest[i];
            }
         	
        }

        {
        	std::cout << "\nRead Write Tests\n";
            LeadApplication *leadtest[2];

            for (int i=0;i<2;i++)
            {
                leadtest[i] = new LeadApplication ( i );
            }
            
            RWFile RWFA1 ( fpasyncrw ); 
            if (BaseFile::Exists ( fpasyncrw ) )
            	RWFA1.SetPermissions ( FilePermission::UserWrite | FilePermission::UserRead );
            if ( RWFA1.Open ( fpasyncrw, FileAttr::ReadWrite | FileAttr::Create | FileAttr::Async ) )
	        {
			    // Allocate a 16 Byte buffer
			    FileBuffer l_buffer ( 16 );
			    // Write something then Read it back
			    l_buffer.SetData ( "123456789012345\n" );

			    try
			    {
            		LeadApplication::s_count = 1;
				    RWFA1.Seek ( 0, BaseFile::SeekBegin );
				    bool bTest = RWFA1.WriteAsync ( l_buffer, leadtest[0] ); // Buffer is transferered at this time
				    if ( bTest )
                    {
		                while ( LeadApplication::s_count ) // [[ TO DO ]] Put a timeout here
                        {
                            at::Task::Sleep ( at::TimeInterval ( 10 * at::TimeInterval::PerMilliSec ) );
                        }
                    } else {
				    	std::cout << "RW Test Write failed!\n";
                    }

            		LeadApplication::s_count = 1;

				    RWFA1.Seek ( 0, BaseFile::SeekBegin );
				    bTest = RWFA1.ReadAsync ( l_buffer, leadtest[1] );
				    if ( false == bTest )
				    	{
				    		std::cout << "RW Test Read failed!\n";
				    	}
			    }
			    catch ( const FileError & fe )
			    {
				    std::cout << "Error While Writing RW/Async " << fe.What () << std::endl;

                    throw;
			    }
        		
			    RWFA1.Close ();

                std::cout << "\nWaiting for all tests RW Async tests to complete\n";
                
                {
                    Lock<Mutex> g_l ( g_lead_count_lock );
                    if ( LeadApplication::s_count != 0 )
                    {
                        g_lead_conditional.Wait ();
                    }
                }

			    std::string charread  = l_buffer.String ();
			    std::cout << "R/W Read :" << charread << std::endl;

                for ( int i = 0; i < 2; i++ )
                {
                   delete leadtest[i];
                }

	        }
        }
    }
};
AT_RegisterTest ( AD_Async , Files );

AT_DefineTest ( AA_CreateTestFiles, Files, "Create Test Files" )
{
    void Run ()
    {
        // Create Test.Text
        WFile testtext ( FilePath ( "Test.Text" ), FileAttr::Create );
        if ( testtext.Open () )
        {
           FileBuffer fb;
           fb.SetData ( testtext1 );
           testtext.Write ( fb );
           fb.SetData ( testtext2 );
           testtext.Write ( fb );
           fb.SetData ( testtext3 );
           testtext.Write ( fb );

           testtext.Close ();
        }

        // Quick test to see if Getlength Works
        RWFile rfgl ( FilePath ("TestGL.txt" ),  FileAttr::Create );
        if ( rfgl.Open () )
        {
            FileBuffer fb;
            fb.SetData ( "12345678" );
            rfgl.Write ( fb );
            rfgl.Close ();

            RWFile rfgg ( FilePath ("TestGL.txt" ) );
            rfgg.Open ();
            at::Uint64 len = rfgg.GetLength ();
            AT_TCAssert ( len == 8,  "GetLength Failed" );
            rfgg.Close ();
        }

        // Test ReadAsync
    }
};
AT_RegisterTest( AA_CreateTestFiles, Files );

AT_DefineTest ( AG_CancelAsync, Files, "Test Cancel Async" )
{
    void Run ()
    {
		FileBuffer bufhuge ( 1024 * 1024 * 32  );
		WFile wf;
		if ( wf.Open ( FilePath ( "huge.file" ), FileAttr::Create | FileAttr::Async ) )
		{
			bufhuge.SetData ( "Its Big!" );
			LeadApplication *lead = new LeadApplication ( 1 );
			LeadApplication::s_count = 1;
			if ( wf.WriteAsync ( bufhuge, lead ) )
			{
				lead->Cancel ();
			}
			wf.Close ();
            delete lead;
		}
    }
};
AT_RegisterTest( AG_CancelAsync, Files );

AT_DefineTest ( AB_FilePath, Files, "File Path Tests" )
{
    void Run ()
    {
        // Test FilePath
        FilePath::s_directory_separator = '/';
        TestClean( "", "." );
        TestClean( "./", "." );
        TestClean( "./.", "." );
        TestClean( "././/.///././//////", "." );
        TestClean( "///", "/" );
        TestClean( "/..//..///.././//./../././../.", "/" );
        TestClean( "./..//.///./././/foo/bar/.././bar/gee/../gee/////", "../foo/bar/gee" );
        TestClean( "/./../../foo/./bar/./gee", "/foo/bar/gee" );
    }

    void TestClean (const char * i_initial_path, const char * i_cleaned_path )
    {
        FilePath l_fp( i_initial_path );
        l_fp.Clean();
        AT_TCAssert ( 0 == strcmp( l_fp.String(), i_cleaned_path ), "FilePath::Clean() Error" );
    }
};
AT_RegisterTest( AB_FilePath, Files );

AT_DefineTest( AZ_CleanUpMyBigMess, Files, "Call 1 800 MerryMaids" )
{
    void Run()
    {
        MopUp ( FilePath ( "rwfile.txt"      ) );
        MopUp ( FilePath ( "Test.Text"       ) );
        MopUp ( FilePath ( "multiwrite.test" ) );
        MopUp ( FilePath ( "test.fp1"        ) );
        MopUp ( FilePath ( "test.fp2"        ) );
        MopUp ( FilePath ( "test.fp3"        ) );    
        MopUp ( FilePath ( "test.fp4"        ) );
        MopUp ( FilePath ( "test.fp5"        ) );
        MopUp ( FilePath ( "test.fp6"        ) );    
        MopUp ( FilePath ( "test.fp7"        ) );    
        MopUp ( FilePath ( "test.fp8"        ) );    
        MopUp ( FilePath ( "Test.Text"       ) );
     	MopUp ( FilePath ( "testasync.write" ) );
     	MopUp ( FilePath ( "testasync.read"  ) );
     	MopUp ( FilePath ( "testasync.rw"    ) );
        MopUp ( FilePath ("TestGL.txt"       ) );
        MopUp ( FilePath ( "huge.file"       ) );
    }

    bool MopUp ( const FilePath &i_file )
    {
        if ( BaseFile::Exists ( i_file ) )
        {
            RFile cleanme ( i_file );
            cleanme.SetPermissions ( FilePermission::UserWrite | FilePermission::UserRead );

            return BaseFile::Remove ( i_file );
        }

        return false;
    }

};
AT_RegisterTest( AZ_CleanUpMyBigMess, Files );

AT_DefineTest( CreatePath, Files, "Create file path test" )
{
    void Run ()
    {
        FilePath l_stem = at::CurrentDirectory();

        FilePath l_path = l_stem / "CreatePathTest" / "A" / "B" / "C" / "x.txt";

        AT_TCAssert( CreatePath( l_path, l_stem ), std::string( "Failed to create directory " ) + l_path.Head().StlString() );

        {
            at::RWFile l_file( l_path, FileAttr::Create );

            AT_TCAssert( l_file.Open(), std::string( "Failed to create file " ) + l_path.StlString() );
        }

        try {
            if ( CreatePath( l_path, l_stem, true ) )
            {
                AT_TCAssert( 0, std::string( "Impossibly created a directory over a file : " ) + l_path.StlString() );
            }
            AT_TCAssert( 0, std::string( "Should have thrown at::ExceptionDerivation<DirectoryCreateFailed> : " ) + l_path.StlString() );
        }
        catch ( at::ExceptionDerivation<DirectoryCreateFailed> & e )
        {
            // ok
        }

        AT_TCAssert( Directory::Remove( l_stem / "CreatePathTest", true ), "Error removing the test directory" );
        
    }

};
AT_RegisterTest( CreatePath, Files );


}; // NameSpace FileUnitTest
