#include <iostream>

#include "at_first_unique.h"

#include "at_unit_test.h"


namespace {
    
AT_TestArea( FirstUnique, "First unique tests" );

#include <iostream>

const int array[] = { 3,1,3,7,1,2,4,4,3 };

AT_DefineTest( BasicFU, FirstUnique, "Basic test" )
{
	void Run()
	{
        const int * l_val = at::FirstUnique( &array[0], at::ArrayEnd( array ) );

        AT_TCAssert( l_val != at::ArrayEnd( array ), "Failed to find item" );
        AT_TCAssert( ( l_val >= array ) && ( l_val < at::ArrayEnd( array ) ), "Result out of range" );

        std::cout << "Found " << * l_val << "\n";

        AT_TCAssert( 7 == * l_val, "Failed to find the right value" );
	}
    
};

AT_RegisterTest( BasicFU, FirstUnique );


};
