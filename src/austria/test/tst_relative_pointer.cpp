/**
 * tst_unit_test.cpp
 *
 * tst_unit_test.cpp tests unit tests !
 *
 */

#include "at_relative_pointer.h"

#include "at_unit_test.h"

#include <cstring>

using namespace at;

AT_TestArea( RelativePointer, "Relative Pointers" );

class RelativePointer_Fixture
{
	public:

	RelativePointer_Fixture()
	{
	}


	//
	// A foo structure 
	
	struct Foo
	{

		RelativePointer<Foo *>			m_next_foo;
		RelativePointer<Foo *>			m_prev_foo;

		char								m_message[ 100 ];
	};

	//
	// MapppedFile is supposed to be this mapped file of
	// random stuff in memory.
	//

	class MapppedFile
	  : public MapHeader
	{
		public:
		
		MapppedFile()
		{
		}

		void CreateSomeStuff()
		{
			m_length = sizeof( * this );

			Foo * l_foo = reinterpret_cast<Foo *>( m_stuff );
			
			l_foo->m_next_foo( this ) = l_foo + 1;

			l_foo->m_prev_foo( this ) = 0;

			( l_foo + 1 )->m_next_foo( this ) = 0;
			
			( l_foo + 1 )->m_prev_foo( this ) = l_foo;
		}

		Foo * GetFirstFoo()
		{
			return reinterpret_cast<Foo *>( m_stuff );
		}

		char								m_stuff[ sizeof( Foo ) * 2 ];

	};
	

};

#define CharAssign( x_dst, x_val ) std::memcpy( x_dst, x_val, sizeof( x_val ) )

AT_DefineTest( BasicTest, RelativePointer, "Basic test" ),
	public RelativePointer_Fixture
{

	void Run()
	{

		std::cout << "\nSizeof Foo is " << sizeof( Foo ) << "\n";

		MapppedFile			  l_file_stuff[ 1 ];
		
//		MapHeader		* ptr = l_file_stuff;

		l_file_stuff->CreateSomeStuff();

		Foo * y = l_file_stuff->GetFirstFoo();

		CharAssign( y->m_message, "First One" );

		Foo * x = y->m_next_foo( l_file_stuff );

		CharAssign( x->m_message, "Second One" );

		MapppedFile			  l_file_stuff_2[ 1 ];

		std::memcpy( l_file_stuff_2, l_file_stuff, sizeof( l_file_stuff_2 ) );

		CharAssign( x->m_message, "Second One Changed" );

		Foo * y_2 = l_file_stuff_2->GetFirstFoo();

		Foo * x_2 = y_2->m_next_foo( l_file_stuff_2 );

		AT_TCAssert( std::strcmp( x_2->m_message, "Second One" ) == 0, "x_2 is pointing to the wrong place" );

		Foo	z;

		char * cp = ( * ( y_2->m_next_foo( l_file_stuff_2 ) ) ).m_message;
		
		return;
	}

};

AT_RegisterTest( BasicTest, RelativePointer );


