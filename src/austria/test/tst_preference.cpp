#include "at_preference.h"

#include "at_unit_test.h"

using namespace std;
using namespace at;


class MovieRating
{
    string m_value;

    public:

    MovieRating() : m_value("G") {}

    MovieRating( string i_s )
    {
        AT_Assert( i_s == "G" || i_s == "PG" || i_s == "R" );
        m_value = i_s;
    }

    bool operator==( const MovieRating &i_rhs ) const
    {
        return m_value == i_rhs.m_value;
    }

    typedef MovieRating t_value_type;

    typedef string t_external_type;

    static string Name()
    {
        return "rating";
    }

    static bool Import(const t_external_type &i_s, t_value_type &o_value)
    {
        if (i_s == "G" || i_s == "PG" || i_s == "R")
        {
            o_value = MovieRating(i_s);
            return true;
        }
        return false;
    }

    static void Export(const t_value_type &i_value, t_value_type &o_s)
    {
        o_s = i_value;
    }

};


template< class w_type >
class PreferenceNotificationPolicy_SetFlag
{

    public:

    bool *m_flag_ptr;

    protected:

    void Notify()
    {
        *m_flag_ptr = true;
    }

};


class MyType;


namespace at
{

    template< >
    struct PreferenceType< MyType >
    {

        typedef int t_value_type;

        typedef string t_external_type;

        static string Name()
        {
            return "mytype";
        }

        static bool Import( const t_external_type &i_s, t_value_type &o_value )
        {
            o_value = 0;
            return true;
        }

        static void Export( const t_value_type &i_value, t_external_type &o_s)
        {
            o_s = "0";
        }

    };

}


AT_TestArea( Prefs, "Preference tests" );


AT_DefineTest( Prefs, Prefs, "Basic Preference test" )
{

    void Run()
    {

        Ptr< PreferenceManager<> * > l_pm = new PreferenceManager<>();

        {
            Preference< bool > l_pref1( "pref1", false, "/FooProgram", l_pm );
            AT_TCAssert( l_pref1.Value() == false, "l_pref1.Value() == false should evaluate true\n" );
        }

        {
            Preference< bool > l_pref2( "pref2", true, "/FooProgram", l_pm );
            AT_TCAssert( l_pref2.Value() == true, "l_pref2.Value() == true should evaluate true\n" );
        }

        {
            Preference< int > l_pref3( "pref3", 101, "/FooProgram", l_pm );
            AT_TCAssert( l_pref3.Value() == 101, "l_pref3.Value() == 101 should evaluate true\n" );
        }

        {
            Preference< string > l_pref4( "pref4", "Austria", "/FooProgram", l_pm );
            AT_TCAssert( l_pref4.Value() == "Austria", "l_pref4.Value() == \"Austria\" should evaluate true\n" );
        }

        {
            Preference< MovieRating > l_pref5( "pref5", MovieRating("PG"), "/FooProgram", l_pm );
            AT_TCAssert( l_pref5.Value() == MovieRating("PG"), "l_pref5.Value() == MovieRating(\"PG\") should evaluate true\n" );
        }

        {
            PreferenceValueTable l_pvt1;
            l_pvt1.SetValue( "pref6", "/FooProgram", "bool", MultiString( std::string( "1" ) ) );
            l_pm->SwapValueTable( l_pvt1 );
            Preference< bool > l_pref6( "pref6", 0, "/FooProgram", l_pm );
            AT_TCAssert( l_pref6.Value() == true, "l_pref6.Value() == true should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt1 );
        }

        {
            PreferenceValueTable l_pvt2;
            l_pvt2.SetValue( "pref7", "/FooProgram", "bool", MultiString( std::string( "0" ) ) );
            l_pm->SwapValueTable( l_pvt2 );
            Preference< bool > l_pref7( "pref7", 1, "/FooProgram", l_pm );
            AT_TCAssert( l_pref7.Value() == false, "l_pref7.Value() == false should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt2 );
        }

        {
            PreferenceValueTable l_pvt3;
            l_pvt3.SetValue( "pref8", "/FooProgram", "int", MultiString( std::string( "202" ) ) );
            l_pm->SwapValueTable( l_pvt3 );
            Preference< int > l_pref8( "pref8", 303, "/FooProgram", l_pm );
            AT_TCAssert( l_pref8.Value() == 202, "l_pref8.Value() == 202 should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt3 );
        }

        {
            PreferenceValueTable l_pvt4;
            l_pvt4.SetValue( "pref9", "/FooProgram", "string", MultiString( std::string( "Switzerland" ) ) );
            l_pm->SwapValueTable( l_pvt4 );
            Preference< string > l_pref9( "pref9", "Lichtenstein", "/FooProgram", l_pm );
            AT_TCAssert( l_pref9.Value() == "Switzerland", "l_pref9.Value() == \"Switzerland\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt4 );
        }

        {
            PreferenceValueTable l_pvt5;
            l_pvt5.SetValue( "pref10", "/FooProgram", "rating", MultiString( std::string( "R" ) ) );
            l_pm->SwapValueTable( l_pvt5 );
            Preference< MovieRating > l_pref10( "pref10", MovieRating("G"), "/FooProgram", l_pm );
            AT_TCAssert( l_pref10.Value() == MovieRating("R"), "l_pref10.Value() == MovieRating(\"R\") should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt5 );
        }

        {
            Preference< string > l_pref11( "pref11", "Luxembourg", "/FooProgram", l_pm );
            PreferenceValueTable l_pvt6;
            l_pvt6.SetValue( "pref11", "/FooProgram", "string", MultiString( std::string( "Belgium" ) ) );
            l_pm->SwapValueTable( l_pvt6 );
            AT_TCAssert( l_pref11.Value() == "Belgium", "l_pref11.Value() == \"Belgium\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt6 );
        }

        {
            PreferenceValueTable l_pvt7;
            l_pvt7.SetValue( "pref12", "/FooProgram", "string", MultiString( std::string( "Netherlands" ) ) );
            l_pvt7.SetValue( "pref13", "/FooProgram", "string", MultiString( std::string( "Andorra" ) ) );
            l_pvt7.SetValue( "pref14", "/FooProgram", "string", MultiString( std::string( "Monaco" ) ) );
            l_pvt7.SetValue( "pref15", "/FooProgram", "string", MultiString( std::string( "San Marino" ) ) );
            l_pm->SwapValueTable( l_pvt7 );
            Preference< string > l_pref14( "pref14", "Denmark", "/FooProgram", l_pm );
            AT_TCAssert( l_pref14.Value() == "Monaco", "l_pref14.Value() == \"Monaco\" should evaluate true\n" );
            Preference< string > l_pref12( "pref12", "Norway", "/FooProgram", l_pm );
            AT_TCAssert( l_pref12.Value() == "Netherlands", "l_pref12.Value() == \"Netherlands\" should evaluate true\n" );
            Preference< string > l_pref13( "pref13", "Sweden", "/FooProgram", l_pm );
            AT_TCAssert( l_pref13.Value() == "Andorra", "l_pref13.Value() == \"Andorra\" should evaluate true\n" );
            Preference< string > l_pref15( "pref15", "Finland", "/FooProgram", l_pm );
            AT_TCAssert( l_pref15.Value() == "San Marino", "l_pref15.Value() == \"San Marino\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt7 );
            Preference< string > l_pref12b( "pref12", "Unset", "/FooProgram", l_pm );
            AT_TCAssert( l_pref12b.Value() == "Unset", "l_pref12b.Value() == \"Unset\" should evaluate true\n" );
            Preference< string > l_pref13b( "pref13", "Unset", "/FooProgram", l_pm );
            AT_TCAssert( l_pref13b.Value() == "Unset", "l_pref13b.Value() == \"Unset\" should evaluate true\n" );
            Preference< string > l_pref14b( "pref14", "Unset", "/FooProgram", l_pm );
            AT_TCAssert( l_pref14b.Value() == "Unset", "l_pref14b.Value() == \"Unset\" should evaluate true\n" );
            Preference< string > l_pref15b( "pref15", "Unset", "/FooProgram", l_pm );
            AT_TCAssert( l_pref15b.Value() == "Unset", "l_pref15b.Value() == \"Unset\" should evaluate true\n" );
        }

        {
            PreferenceValueTable l_pvt8;
            l_pvt8.SetValue( "pref16", "/FooProgram", "string", MultiString( std::string( "Estonia" ) ) );
            l_pvt8.SetValue( "pref17", "/FooProgram", "string", MultiString( std::string( "Latvia" ) ) );
            l_pvt8.SetValue( "pref18", "/FooProgram", "string", MultiString( std::string( "Lithuania" ) ) );
            l_pm->SwapValueTable( l_pvt8 );
            Preference< string > l_pref19( "pref19", "Poland", "/FooProgram", l_pm );
            AT_TCAssert( l_pref19.Value() == "Poland", "l_pref19.Value() == \"Poland\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt8 );
        }

        {
            PreferenceValueTable l_pvt9;
            l_pvt9.SetValue( "pref20", "/England", "string", MultiString( std::string( "Portugal" ) ) );
            l_pvt9.SetValue( "pref20", "/England/Scottland", "string", MultiString( std::string( "Spain" ) ) );
            l_pvt9.SetValue( "pref20", "/England/Scottland/Ireland", "string", MultiString( std::string( "France" ) ) );
            l_pm->SwapValueTable( l_pvt9 );
            Preference< string > l_pref20( "pref20", "Germany", "/England/Scottland/Wales", l_pm );
            AT_TCAssert( l_pref20.Value() == "Spain", "l_pref20.Value() == \"Spain\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt9 );
        }

        {
            PreferenceValueTable l_pvt10;
            l_pvt10.SetValue( "pref21", "/CzechRepublic", "string", MultiString( std::string( "Kossovo" ) ) );
            l_pvt10.SetValue( "pref21", "/Slovakia", "string", MultiString( std::string( "Macedonia" ) ) );
            l_pvt10.SetValue( "pref21", "/Croatia", "string", MultiString( std::string( "Greece" ) ) );
            l_pvt10.SetValue( "pref21", "/Croatia/Slovenia", "string", MultiString( std::string( "Bulgaria" ) ) );
            l_pvt10.SetValue( "pref21", "/Croatia/Bosnia", "string", MultiString( std::string( "Romania" ) ) );
            l_pvt10.SetValue( "pref21", "/Serbia", "string", MultiString( std::string( "Moldova" ) ) );
            l_pm->SwapValueTable( l_pvt10 );
            Preference< string > l_pref21( "pref21", "Ukraine", "/Hungary", l_pm );
            AT_TCAssert( l_pref21.Value() == "Ukraine", "l_pref21.Value() == \"Ukraine\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt10 );
        }

        {
            PreferenceValueTable l_pvt11;
            l_pvt11.SetValue( "pref22", "/Israel", "string", MultiString( std::string( "Chile" ) ) );
            l_pvt11.SetValue( "pref22", "/Lebanon", "string", MultiString( std::string( "Argentina" ) ) );
            l_pvt11.SetValue( "pref22", "/Lebanon/Egypt", "string", MultiString( std::string( "Paraguay" ) ) );
            l_pvt11.SetValue( "pref22", "/Lebanon/Libya", "string", MultiString( std::string( "Bolivia" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia", "string", MultiString( std::string( "Peru" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Afghanistan", "string", MultiString( std::string( "Ecuador" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Australia", "string", MultiString( std::string( "Brazil" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus", "string", MultiString( std::string( "Guiana" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Armenia", "string", MultiString( std::string( "Venezuela" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Georgia", "string", MultiString( std::string( "Columbia" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Kazakhistan", "string", MultiString( std::string( "Panama" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Kazakhistan/Mongolia", "string", MultiString( std::string( "ElSalvador" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Kazakhistan/China", "string", MultiString( std::string( "CostaRica" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Korea/India", "string", MultiString( std::string( "Nicaragua" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Korea/Japan", "string", MultiString( std::string( "Honduras" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Korea/Nepal/Bangladesh", "string", MultiString( std::string( "Belize" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Korea/Nepal/Bhutan", "string", MultiString( std::string( "Guatamala" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Korea/Nepal/Bhutan/Myanmar", "string", MultiString( std::string( "Mexico" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Korea/Nepal/SriLanka", "string", MultiString( std::string( "Barbados" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Korea/Taiwan", "string", MultiString( std::string( "Grenada" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Uzbekistan", "string", MultiString( std::string( "Martinique" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Uzbekistan", "string", MultiString( std::string( "Trinidad" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Belarus/Uzbekistan", "string", MultiString( std::string( "Tobago" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Cyprus", "string", MultiString( std::string( "Guadelupe" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Syria", "string", MultiString( std::string( "Montserrat" ) ) );
            l_pvt11.SetValue( "pref22", "/Russia/Turkey", "string", MultiString( std::string( "StKitts" ) ) );
            l_pvt11.SetValue( "pref22", "/Swaziland", "string", MultiString( std::string( "Jamaica" ) ) );
            l_pvt11.SetValue( "pref22", "/Tanzania", "string", MultiString( std::string( "Cuba" ) ) );
            l_pvt11.SetValue( "pref22", "/Tanzania/Mozambique", "string", MultiString( std::string( "Haiti" ) ) );
            l_pvt11.SetValue( "pref22", "/Uruguay", "string", MultiString( std::string( "DominicanRepublic" ) ) );
            l_pm->SwapValueTable( l_pvt11 );
            Preference< string > l_pref22( "pref22", "Bahamas", "/Russia/Belarus/Korea/Nepal/Pakistan", l_pm );
            AT_TCAssert( l_pref22.Value() == "Guiana", "l_pref22.Value() == \"Guiana\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt11 );
        }

        {
            PreferenceValueTable l_pvt12;
            l_pvt12.SetValue( "pref23", "/Algeria", "string", MultiString( std::string( "Namibia" ) ) );
            l_pvt12.SetValue( "pref23", "/Algeria/Morocco", "string", MultiString( std::string( "SouthAfrica" ) ) );
            l_pvt12.SetValue( "pref23", "/Algeria/Tunisia", "string", MultiString( std::string( "Botswana" ) ) );
            l_pvt12.SetValue( "pref23", "/Chad", "string", MultiString( std::string( "Zimbabwe" ) ) );
            l_pvt12.SetValue( "pref23", "/Mali", "string", MultiString( std::string( "Malawi" ) ) );
            l_pvt12.SetValue( "pref23", "/Nigeria", "string", MultiString( std::string( "Lesotho" ) ) );
            l_pm->SwapValueTable( l_pvt12 );
            Preference< string > l_pref23( "pref23", "Zambia", "/Algeria", l_pm );
            AT_TCAssert( l_pref23.Value() == "Namibia", "l_pref23.Value() == \"Namibia\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt12 );
        }

        {
            PreferenceValueTable l_pvt13;
            l_pvt13.SetValue( "pref24", "/Fiji", "string", MultiString( std::string( "Iceland" ) ) );
            l_pvt13.SetValue( "pref24", "/Kirbati", "string", MultiString( std::string( "Malta" ) ) );
            l_pvt13.SetValue( "pref24", "/NewZealand", "string", MultiString( std::string( "Yemen" ) ) );
            l_pvt13.SetValue( "pref24", "/NewZealand/Micronesia", "string", MultiString( std::string( "SaudiArabia" ) ) );
            l_pvt13.SetValue( "pref24", "/NewZealand/Micronesia/Indonesia", "string", MultiString( std::string( "Iraq" ) ) );
            l_pvt13.SetValue( "pref24", "/NewZealand/Micronesia/Indonesia/Malaysia", "string", MultiString( std::string( "Iran" ) ) );
            l_pvt13.SetValue( "pref24", "/NewZealand/Micronesia/Indonesia/Malaysia/Timor", "string", MultiString( std::string( "UnitedArabEmirates" ) ) );
            l_pm->SwapValueTable( l_pvt13 );
            Preference< string > l_pref24( "pref24", "Phillipines", "/NewZealand/Micronesia/Indonesia/Malaysia/Timor/Thailand/Myanmar", l_pm );
            AT_TCAssert( l_pref24.Value() == "UnitedArabEmirates", "l_pref24.Value() == \"UnitedArabEmirates\" should evaluate true\n" );
            l_pm->SwapValueTable( l_pvt13 );
        }

        {
            Preference< string, PreferenceNotificationPolicy_SetFlag > l_pref25( "pref25", "VietNam", "/FooProgram", l_pm );
            bool l_notified = false;
            l_pref25.m_flag_ptr = &l_notified;
            PreferenceValueTable l_pvt14;
            l_pvt14.SetValue( "pref25", "/FooProgram", "string", MultiString( std::string( "Uganda" ) ) );
            l_pm->SwapValueTable( l_pvt14 );
            AT_TCAssert( l_notified, "pref25 should be Notify'ed\n" );
            l_pm->SwapValueTable( l_pvt14 );
        }

    }

};

AT_RegisterTest( Prefs, Prefs );

