
#include "at_file.h"

#include "at_unit_test.h"

#include <iostream>

namespace {
    
AT_TestArea( FileRange, "File lock tests" );

#include <iostream>

AT_DefineTest( LockRange, FileRange, "File lockrange" )
{
	void Run()
	{
        at::FileRemover l_remover( "LockRangeTest" );
        
        at::WFile   l_file( l_remover.m_filename, at::FileAttr::Write | at::FileAttr::Create );

        l_file.Open();

        AT_TCAssert( l_file.IsOpen(), "File failed to open" );

        at::FileLockRange( l_file, 0, 1 );
        
	}

    
};

AT_RegisterTest( LockRange, FileRange );


};
