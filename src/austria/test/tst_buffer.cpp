
#include "at_buffer.h"

#include "at_unit_test.h"

//using namespace at;

AT_TestArea( BufferArea, "Buffer area" );

#include <iostream>


namespace {


// ======== PrintRegionInfo ===========================================
/**
 * Print the info in the buffer region
 *
 *
 * @param i_br A buffer region
 * @return nothing
 */

void PrintRegionInfo(
    const at::BufferRegion< at::Buffer::t_BaseType > & l_br
)
{
    std::cout << "**** Length = " << l_br.m_allocated << " Reserved = " << l_br.m_max_available << "\n";
}


// ======== BufferAreaTest ============================================
/**
 * BufferAreaTest performs all the tests for a buffer
 *
 *
 */

struct TestStructX {
    char    buf[ 20 ];
};

void BufferAreaTest(
    const char      * i_key
) {

    at::Ptr< at::Pool * > l_pool = at::FactoryRegister< at::Pool, at::DKy >::Get().Create( i_key )();

    AT_TCAssert( l_pool, "Failed to create pool" );

    at::Ptr< at::Buffer * > l_buffer = l_pool->Create();
    
    AT_TCAssert( l_buffer, "Failed to create buffer" );

    AT_TCAssert( l_buffer->Region().String() == "", "The default string should be empty" );

    std::string l_str = "String";

    l_buffer->Set( l_str );

    AT_TCAssert( l_buffer->Region().String() == l_str, "The string should be set to l_str" );

    at::Ptr< const at::Buffer * > l_cbuffer = l_buffer;

    // test the const version
    AT_TCAssert( l_cbuffer->Region().String() == l_str, "The string should be set to l_str" );

    // Try the self-append logic
    * l_buffer += * l_buffer;

    AT_TCAssert( l_cbuffer->Region().String() == ( l_str + l_str ), "Expecting l_str + l_str" );

    std::cout << "\n";
    PrintRegionInfo( l_buffer->Region() );
    * l_buffer += * l_buffer;
    PrintRegionInfo( l_buffer->Region() );
    * l_buffer += * l_buffer;
    PrintRegionInfo( l_buffer->Region() );
    * l_buffer += * l_buffer;
    PrintRegionInfo( l_buffer->Region() );
    * l_buffer += * l_buffer;
    PrintRegionInfo( l_buffer->Region() );
    * l_buffer += * l_buffer;
    PrintRegionInfo( l_buffer->Region() );
    * l_buffer += * l_buffer;
    PrintRegionInfo( l_buffer->Region() );

    AT_TCAssert( l_buffer->SetAvailability( l_str.length() ).String() == l_str, "Truncation failed" );

    PrintRegionInfo( l_buffer->Region() );

    l_buffer->Append( l_str );
    
    PrintRegionInfo( l_buffer->Region() );
    AT_TCAssert( l_buffer->Region().String() == ( l_str + l_str ), "Expecting l_str + l_str" );

    PrintRegionInfo( l_buffer->SetAllocated( l_str.length() ) );
    
    AT_TCAssert( l_buffer->Region().String() == l_str, "Truncation failed" );

    l_buffer->Append( l_str );
    
    AT_TCAssert( l_buffer->Region().String() == ( l_str + l_str ), "Expecting l_str + l_str" );

    // These will assert
    //    l_buffer->Append( l_buffer->Region() );
    //    l_buffer->Append( l_cbuffer->Region() );

    // The second buffer
    at::Ptr< at::Buffer * > l_buffer2 = l_pool->Create();

    l_buffer2->Append( l_cbuffer->Region() );

    AT_TCAssert( l_buffer2->Region().String() == l_cbuffer->Region().String(), "Region append fails" );

    l_buffer2->Set( l_cbuffer->Region() );

    AT_TCAssert( l_buffer2->Region().String() == l_cbuffer->Region().String(), "Region Set( BR ) fails" );

    TestStructX   x = { "0123456789012345678" };

    TestStructX & nx = l_buffer2->InsecureAssign( x );

    TestStructX * px = l_buffer2->InsecureCast();

    std::string l_str2( x.buf, sizeof( x.buf ) );
    std::string l_str3( px->buf, sizeof( px->buf ) );

    AT_TCAssert( l_str2 == l_str3, "Insecure assignment fails" );

    // make the buffer too small ...
    l_buffer->SetAllocated( 1 );
    try {
        // This should throw a ExceptionDerivation<Buffer_ConversionFailure> 
        TestStructX * px = l_buffer->InsecureCast();

        throw 1;
    }
    catch ( at::ExceptionDerivation<at::Buffer_ConversionFailure> & i_cofa )
    {
        // this is good ...

        std::cout << * ( i_cofa.Trace() );
    }
    catch ( ... )
    {
        // oops ...
        AT_TCAssert( false, "Expected a Buffer_ConversionFailure" );
    }
    
    std::string l_strx = "StringStringStringStringStringStringStringStringStringStringStringStringStringString";

    l_buffer2->Set( l_strx );

    l_buffer2->Append( l_buffer2->Region() );

    AT_TCAssert( l_buffer2->Region().String() == l_strx + l_strx, "Region Set( BR ) fails" );

    std::string l_stry = "ABCDEF";

    l_buffer2->Set( l_stry );

    l_buffer2->Erase( 3 );

    AT_TCAssert( l_buffer2->Region().String() == "ABC", "Erase fails" );

    l_buffer2->Erase( 1, 2 );
    
    AT_TCAssert( l_buffer2->Region().String() == "AC", "Erase fails" );

    l_buffer2->Erase( );
    
    AT_TCAssert( l_buffer2->Region().String() == "", "Erase fails" );

}



AT_DefineTest( BasicBuffer, BufferArea, "A basic buffer test" )
{
	void Run()
	{
        BufferAreaTest( "Basic" );
	}
};

AT_RegisterTest( BasicBuffer, BufferArea );


} // anon namespace
