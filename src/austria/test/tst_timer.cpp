#include "at_assert.h"
#include "at_thread.h"
#include "at_timer.h"
#include "at_types.h"
#include "at_unit_test.h"

#include <vector>
#include <iostream>

using namespace at;
using namespace std;


class TimerLog
{

public:

    struct m_event
    {
        int m_client;
        int m_time;

        m_event(
            int i_client,
            int i_time
        )
          : m_client( i_client ),
            m_time( i_time )
        {
        }

        bool operator==( const m_event & i_rhs ) const
        {
            return m_client == i_rhs.m_client && m_time == i_rhs.m_time;
        }

    };

    typedef  vector< m_event >  t_log_type;

    typedef  t_log_type::const_iterator  t_log_const_iterator;

private:

    TimeStamp m_base_time;
    TimeInterval m_tick_interval;

    t_log_type m_log;

    bool m_echo;

public:

    TimerLog()
      : m_echo( false )
    {
    }

    TimerLog(
        TimeStamp i_base_time,
        TimeInterval i_tick_interval,
        bool i_echo = false
    )
      : m_base_time( i_base_time ),
        m_tick_interval( i_tick_interval ),
        m_echo( i_echo )
    {
    }

    void push_back(
        int i_client,
        int i_time
    )
    {
        m_log.push_back( m_event( i_client, i_time ) );

        if ( m_echo )
        {
            cout << "Time: " << i_time <<
                "   Client: " << i_client << "\n";
        }

    }

    void push_back(
        int i_client,
        TimeStamp i_time
    )
    {
        push_back(
            i_client,
            static_cast< int >(
                ( ( i_time - m_base_time ) + ( m_tick_interval / 2 ) ) /
                    m_tick_interval
            )
        );
    }

    bool operator==( const TimerLog & i_rhs ) const
    {
        return ( m_log == i_rhs.m_log );
    }

    friend bool SameOrder(
        const TimerLog & i_first,
        const TimerLog & i_second
    )
    {
        for ( TimerLog::t_log_const_iterator
                  l_first_iter = i_first.m_log.begin(),
                  l_second_iter = i_second.m_log.begin();
              l_first_iter != i_first.m_log.end() ||
                  l_second_iter != i_second.m_log.end();
              ++l_first_iter, ++l_second_iter
            )
        {
            if ( l_first_iter == i_first.m_log.end() ||
                 l_second_iter == i_second.m_log.end() ||
                 l_first_iter->m_client != l_second_iter->m_client
               )
            {
                return false;
            }
        }
        return true;
    }

    template<
        typename w_char_type,
        class w_traits
    >
    friend std::basic_ostream< w_char_type, w_traits> & operator<<(
        std::basic_ostream< w_char_type, w_traits > & o_lhs,
        const TimerLog & i_rhs
    )
    {
        for ( TimerLog::t_log_const_iterator l_iter = i_rhs.m_log.begin();
              l_iter != i_rhs.m_log.end();
              ++l_iter
            )
        {
            o_lhs << "Time: " << l_iter->m_time <<
                "   Client: " << l_iter->m_client << "\n";
        }
        return o_lhs;
    }

    size_t size()
    {
        return m_log.size();
    }

};


class MyTimerClient
  : public TimerClient_Basic
{

    TimerLog & m_log;
    int m_client_number;

public:

    MyTimerClient( TimerLog & io_log, int i_client_number )
      : m_log( io_log ),
        m_client_number( i_client_number )
    {
    }

protected:

    virtual bool Reschedule(
        const TimeStamp & i_current_time,
        TimeStamp & o_reschedule_time    
    ) = 0;

private:

    virtual bool Event(
        const TimeStamp & i_current_time,
        TimeStamp & o_reschedule_time
    )
    {
        m_log.push_back( m_client_number, ThreadSystemTime() );
        return Reschedule( i_current_time, o_reschedule_time );
    }

};


class MyTimerClient_NTimesFixedInterval
  : public MyTimerClient
{

    TimeStamp m_next_time;
    TimeInterval m_interval;
    int m_n;

public:

    MyTimerClient_NTimesFixedInterval(
        Ptr< TimerService * > io_ts,
        TimerLog & io_log,
        int i_client_number,
        TimeStamp i_first_time,
        TimeInterval i_interval,
        int i_n
    )
      : MyTimerClient( io_log, i_client_number ),
        m_next_time( i_first_time ),
        m_interval( i_interval ),
        m_n( i_n )
    {
        io_ts->StartTimer( *this, m_next_time );

        if ( ThreadSystemTime() < m_next_time )
        {
            TimeStamp l_next_time;
            if ( GetNextEventTime( l_next_time ) )
            {
                AT_TCAssert( l_next_time == m_next_time, "GetNextEventTime() does not return the expected value" );
            }
        }
    }

    virtual ~MyTimerClient_NTimesFixedInterval()
    {
        LeadCancel();
    }

private:

    virtual bool Reschedule(
        const TimeStamp & i_current_time,
        TimeStamp & o_reschedule_time
    )
    {
        if ( m_n > 1 )
        {
            --m_n;
            m_next_time += m_interval;
            o_reschedule_time = m_next_time;
            return true;
        }
        else
        {
            return false;
        }
    }

};


class MyTimerClient_NTimesFixedInterval_RefCounted
  : public MyTimerClient_NTimesFixedInterval,
    public PtrTarget_MT
{

public:

    MyTimerClient_NTimesFixedInterval_RefCounted(
        Ptr< TimerService * > io_ts,
        TimerLog & io_log,
        int i_client_number,
        TimeStamp i_first_time,
        TimeInterval i_interval,
        int i_n
    )
      : MyTimerClient_NTimesFixedInterval(
            io_ts,
            io_log,
            i_client_number,
            i_first_time,
            i_interval,
            i_n
        )
    {
    }

};


class MyTimerClient2
  : public TimerClient_Basic,
    public PtrTarget_MT
{

    Ptr< TimerService * > m_ts;
    TimerLog & m_log;
    int m_client_number;
    vector< Ptr< MyTimerClient2 * > > & m_clients;
    TimeStamp m_next_time;
    TimeInterval m_interval;
    int m_n;
    int m_generation;
    int m_repeats;
    int m_spawns;

public:

    MyTimerClient2(
        Ptr< TimerService * > io_ts,
        TimerLog & o_log,
        int i_client_number,
        vector< Ptr< MyTimerClient2 * > > & io_clients,
        TimeStamp i_first_time,
        TimeInterval i_interval,
        int i_generation,
        int i_repeats,
        int i_spawns
    )
      : m_ts( io_ts ),
        m_log( o_log ),
        m_client_number( i_client_number ),
        m_clients( io_clients ),
        m_next_time( i_first_time ),
        m_interval( i_interval ),
        m_n( 0 ),
        m_generation( i_generation ),
        m_repeats( i_repeats ),
        m_spawns( i_spawns )
    {
        m_ts->StartTimer( *this, m_next_time );
    }

    virtual bool Event(
        const TimeStamp & i_current_time,
        TimeStamp & o_reschedule_time
    )
    {
        TimeStamp l_next_time;
        const bool l_is_scheduled = GetNextEventTime( l_next_time );
        AT_TCAssert( ! l_is_scheduled, "Timer event scheduled when Event() executes." );

        m_log.push_back( m_client_number, ThreadSystemTime() );
        if ( m_generation > 1 )
        {
            for ( int l_i = 0; l_i != m_spawns; ++l_i )
            {
                int l_client_number = m_clients.size() + 4;
                m_clients.push_back(
                    new MyTimerClient2(
                            m_ts,
                            m_log,
                            l_client_number,
                            m_clients,
                            m_next_time + m_interval,
                            m_interval,
                            m_generation - 1,
                            m_repeats,
                            m_spawns
                        )
                );
            }
        }
        if ( ++m_n == m_repeats )
        {
            return false;
        }
        else
        {
            m_next_time += m_interval;
            o_reschedule_time = m_next_time;
            return true;
        }
    }

};


class MyTimerClient3
  : public TimerClient_Basic,
    public PtrTarget_MT
{

    TimerLog & m_log;
    int m_client_number;
    vector< Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > > & m_clients;
    Ptr< MyTimerClient3 * > & m_self_ptr;
    TimeStamp m_next_time;
    TimeInterval m_interval;
    int m_n;

public:

    MyTimerClient3(
        Ptr< TimerService * > io_ts,
        TimerLog & o_log,
        int i_client_number,
        vector< Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > > & io_clients,
        Ptr< MyTimerClient3 * > & io_self_ptr,
        TimeStamp i_first_time,
        TimeInterval i_interval
    )
      : m_log( o_log ),
        m_client_number( i_client_number ),
        m_clients( io_clients ),
        m_self_ptr( io_self_ptr ),
        m_next_time( i_first_time ),
        m_interval( i_interval ),
        m_n( 0 )
    {
        io_ts->StartTimer( *this, m_next_time );
    }

    virtual bool Event(
        const TimeStamp & i_current_time,
        TimeStamp & o_reschedule_time
    )
    {
        m_log.push_back( m_client_number, ThreadSystemTime() );
        if ( m_n == int( m_clients.size() ) )
        {
            m_self_ptr = 0;
            return false;
        }
        else
        {
            m_clients[ m_n++ ] = 0;
            m_next_time += m_interval;
            o_reschedule_time = m_next_time;
            return true;
        }
    }

};


AT_TestArea( Timer, "Timer service module tests" );

AT_DefineTestLevel(
    Timer,
    Timer,
    "Basic timer service module test",
    UnitTestTraits::TestSuccess,
    0
)
{

    void Run()
    {

        cout << "\n\n";

        {

            cout <<
                "Basic unit test:\n\n"
                "Register one client, make it the only thing referencing the timer\n"
                "service, let it receive some events, then delete it with more events\n"
                "still pending.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 10 );

            TimerLog l_expected;
            l_expected.push_back( 1, 10 );
            l_expected.push_back( 1, 20 );
            l_expected.push_back( 1, 30 );
            l_expected.push_back( 1, 40 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                MyTimerClient_NTimesFixedInterval l_client1(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval, 
                    10
                );

                l_timer_service = 0;

                Task::Sleep( 45 * l_tick_interval );

            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( l_actual == l_expected, "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif

        }


        {

            cout <<
                "Create a TimerService and immediately destroy it without registering\n"
                "any clients.\n" << endl;

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );
            }

            cout << "\n\n\n";

        }


        {

            cout <<
                "Register one client, make it the only thing referencing the timer\n"
                "service, then immediately delete the client.\n" << endl;   

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 10 );

            TimerLog l_expected;

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                MyTimerClient_NTimesFixedInterval l_client1(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval,
                    10
                );

                l_timer_service = 0;
            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( SameOrder( l_actual, l_expected ), "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif
        }


        {

            cout <<
                "Register one client, make it the only thing referencing the timer\n"
                "service, let it receive some events, then delete it with no remaining\n"
                "pending events.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 10 );

            TimerLog l_expected;
            l_expected.push_back( 1, 10 );
            l_expected.push_back( 1, 20 );
            l_expected.push_back( 1, 30 );
            l_expected.push_back( 1, 40 );
            l_expected.push_back( 1, 50 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                MyTimerClient_NTimesFixedInterval l_client1(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval, 
                    5
                );

                l_timer_service = 0;

                Task::Sleep( 75 * l_tick_interval );

            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( SameOrder( l_actual, l_expected ), "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif
        }


        {

            cout <<
                "Register one client, make it the only thing referencing the timer\n"
                "service, set it up to receive events at the shortest possible\n"
                "interval, and immediately delete the client.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::NanoSecs( 100 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                MyTimerClient_NTimesFixedInterval l_client1(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time,
                    l_tick_interval,
                    1000
                );

                l_timer_service = 0;
            }

#if 0
            cout << "\nGot:\n" << l_actual << "\n";
#else
            cout << "\n\n";
#endif

        }


        {

            cout <<
                "Repeatedly register one client for one event at the shortest\n"
                "possible interval, then sleep long enough for the server to\n"
                "handle the event." << endl;

            TimeInterval l_tick_interval = TimeInterval::NanoSecs( 100 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                for ( int l_i = 0; l_i != 100; ++l_i )
                {

                    MyTimerClient_NTimesFixedInterval l_client1(
                        l_timer_service,
                        l_actual,
                        1,
                        l_start_time + l_tick_interval,
                        l_tick_interval,
                        1
                    );

                    Task::Sleep( TimeInterval::MilliSecs( 10 ) );

                }
            }

#if 0
            cout << "\nGot:\n" << l_actual << "\n";
#else
            cout << "\n\n";
#endif

        }


        {

            cout <<
                "Register one client for one event at the shortest possible\n"
                "interval and immediately destroy it, N times, for different\n"
                "values of N." << endl;

            TimeInterval l_tick_interval = TimeInterval::NanoSecs( 100 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            for ( int l_j = 0; l_j != 25; ++l_j )
            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                for ( int l_i = 0; l_i != l_j; ++l_i )
                {

                    MyTimerClient_NTimesFixedInterval l_client1(
                        l_timer_service,
                        l_actual,
                        1,
                        l_start_time + l_tick_interval,
                        l_tick_interval,
                        1
                    );

                }
            }

            cout << "\n\n";

        }


        {

            cout <<
                "Register several clients, in descending order of requested time.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 10 );

            TimerLog l_expected;
            l_expected.push_back( 5, 10 );
            l_expected.push_back( 4, 20 );
            l_expected.push_back( 3, 30 );
            l_expected.push_back( 2, 40 );
            l_expected.push_back( 1, 50 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                MyTimerClient_NTimesFixedInterval l_client1(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 50 * l_tick_interval ),
                    10 * l_tick_interval,
                    1
                );
                MyTimerClient_NTimesFixedInterval l_client2(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 40 * l_tick_interval ),
                    10 * l_tick_interval,
                    1
                );
                MyTimerClient_NTimesFixedInterval l_client3(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 30 * l_tick_interval ),
                    10 * l_tick_interval,
                    1
                );
                MyTimerClient_NTimesFixedInterval l_client4(
                    l_timer_service,
                    l_actual,
                    4,
                    l_start_time + ( 20 * l_tick_interval ),
                    10 * l_tick_interval,
                    1
                );
                MyTimerClient_NTimesFixedInterval l_client5(
                    l_timer_service,
                    l_actual,
                    5,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval,
                    1
                );

                Task::Sleep( l_tick_interval * 75 );

            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( l_actual == l_expected, "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif

        }


        {

            cout <<
                "Register several clients, each requesting events at exactly the same\n"
                "times.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 10 );

            TimerLog l_expected;
            l_expected.push_back( 1, 10 );
            l_expected.push_back( 2, 10 );
            l_expected.push_back( 3, 10 );
            l_expected.push_back( 4, 10 );
            l_expected.push_back( 5, 10 );
            l_expected.push_back( 1, 20 );
            l_expected.push_back( 2, 20 );
            l_expected.push_back( 3, 20 );
            l_expected.push_back( 4, 20 );
            l_expected.push_back( 5, 20 );
            l_expected.push_back( 1, 30 );
            l_expected.push_back( 2, 30 );
            l_expected.push_back( 3, 30 );
            l_expected.push_back( 4, 30 );
            l_expected.push_back( 5, 30 );
            l_expected.push_back( 1, 40 );
            l_expected.push_back( 2, 40 );
            l_expected.push_back( 3, 40 );
            l_expected.push_back( 4, 40 );
            l_expected.push_back( 5, 40 );
            l_expected.push_back( 1, 50 );
            l_expected.push_back( 2, 50 );
            l_expected.push_back( 3, 50 );
            l_expected.push_back( 4, 50 );
            l_expected.push_back( 5, 50 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                MyTimerClient_NTimesFixedInterval l_client1(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval, 
                    5
                );
                MyTimerClient_NTimesFixedInterval l_client2(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval, 
                    5
                );
                MyTimerClient_NTimesFixedInterval l_client3(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval, 
                    5
                );
                MyTimerClient_NTimesFixedInterval l_client4(
                    l_timer_service,
                    l_actual,
                    4,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval, 
                    5
                );
                MyTimerClient_NTimesFixedInterval l_client5(
                    l_timer_service,
                    l_actual,
                    5,
                    l_start_time + ( 10 * l_tick_interval ),
                    10 * l_tick_interval, 
                    5
                );

                l_timer_service = 0;

                Task::Sleep( 75 * l_tick_interval );

            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( SameOrder( l_actual, l_expected), "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif

        }


        {

            cout <<
                "Register several clients, requesting events at different intervals.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 2 );

            TimerLog l_expected;
            l_expected.push_back( 1, 30 );
            l_expected.push_back( 2, 50 );
            l_expected.push_back( 1, 60 );
            l_expected.push_back( 3, 70 );
            l_expected.push_back( 1, 90 );
            l_expected.push_back( 2, 100 );
            l_expected.push_back( 4, 110 );
            l_expected.push_back( 1, 120 );
            l_expected.push_back( 5, 130 );
            l_expected.push_back( 3, 140 );
            l_expected.push_back( 2, 150 );
            l_expected.push_back( 1, 150 );
            l_expected.push_back( 6, 170 );
            l_expected.push_back( 1, 180 );
            l_expected.push_back( 7, 190 );
            l_expected.push_back( 2, 200 );
            l_expected.push_back( 3, 210 );
            l_expected.push_back( 1, 210 );
            l_expected.push_back( 4, 220 );
            l_expected.push_back( 8, 230 );
            l_expected.push_back( 1, 240 );
            l_expected.push_back( 2, 250 );
            l_expected.push_back( 5, 260 );
            l_expected.push_back( 1, 270 );
            l_expected.push_back( 3, 280 );
            l_expected.push_back( 2, 300 );
            l_expected.push_back( 1, 300 );
            l_expected.push_back( 4, 330 );
            l_expected.push_back( 1, 330 );
            l_expected.push_back( 6, 340 );
            l_expected.push_back( 3, 350 );
            l_expected.push_back( 2, 350 );
            l_expected.push_back( 1, 360 );
            l_expected.push_back( 7, 380 );
            l_expected.push_back( 5, 390 );
            l_expected.push_back( 1, 390 );
            l_expected.push_back( 2, 400 );
            l_expected.push_back( 3, 420 );
            l_expected.push_back( 1, 420 );
            l_expected.push_back( 4, 440 );
            l_expected.push_back( 2, 450 );
            l_expected.push_back( 1, 450 );
            l_expected.push_back( 8, 460 );
            l_expected.push_back( 1, 480 );
            l_expected.push_back( 3, 490 );
            l_expected.push_back( 2, 500 );
            l_expected.push_back( 6, 510 );
            l_expected.push_back( 1, 510 );
            l_expected.push_back( 5, 520 );
            l_expected.push_back( 1, 540 );
            l_expected.push_back( 4, 550 );
            l_expected.push_back( 2, 550 );
            l_expected.push_back( 3, 560 );
            l_expected.push_back( 7, 570 );
            l_expected.push_back( 1, 570 );
            l_expected.push_back( 2, 600 );
            l_expected.push_back( 1, 600 );
            l_expected.push_back( 3, 630 );
            l_expected.push_back( 1, 630 );
            l_expected.push_back( 5, 650 );
            l_expected.push_back( 2, 650 );
            l_expected.push_back( 4, 660 );
            l_expected.push_back( 1, 660 );
            l_expected.push_back( 6, 680 );
            l_expected.push_back( 8, 690 );
            l_expected.push_back( 1, 690 );
            l_expected.push_back( 3, 700 );
            l_expected.push_back( 2, 700 );
            l_expected.push_back( 1, 720 );
            l_expected.push_back( 2, 750 );
            l_expected.push_back( 1, 750 );
            l_expected.push_back( 7, 760 );
            l_expected.push_back( 4, 770 );
            l_expected.push_back( 3, 770 );
            l_expected.push_back( 5, 780 );
            l_expected.push_back( 1, 780 );
            l_expected.push_back( 2, 800 );
            l_expected.push_back( 1, 810 );
            l_expected.push_back( 3, 840 );
            l_expected.push_back( 1, 840 );
            l_expected.push_back( 6, 850 );
            l_expected.push_back( 2, 850 );
            l_expected.push_back( 1, 870 );
            l_expected.push_back( 4, 880 );
            l_expected.push_back( 2, 900 );
            l_expected.push_back( 1, 900 );
            l_expected.push_back( 5, 910 );
            l_expected.push_back( 3, 910 );
            l_expected.push_back( 8, 920 );
            l_expected.push_back( 1, 930 );
            l_expected.push_back( 7, 950 );
            l_expected.push_back( 2, 950 );
            l_expected.push_back( 1, 960 );
            l_expected.push_back( 3, 980 );
            l_expected.push_back( 4, 990 );
            l_expected.push_back( 1, 990 );
            l_expected.push_back( 2, 1000 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                MyTimerClient_NTimesFixedInterval l_client1(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 30 * l_tick_interval ),
                    30 * l_tick_interval, 
                    33
                );
                MyTimerClient_NTimesFixedInterval l_client2(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 50 * l_tick_interval ),
                    50 * l_tick_interval, 
                    20
                );
                MyTimerClient_NTimesFixedInterval l_client3(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 70 * l_tick_interval ),
                    70 * l_tick_interval, 
                    14
                );
                MyTimerClient_NTimesFixedInterval l_client4(
                    l_timer_service,
                    l_actual,
                    4,
                    l_start_time + ( 110 * l_tick_interval ),
                    110 * l_tick_interval, 
                    9
                );
                MyTimerClient_NTimesFixedInterval l_client5(
                    l_timer_service,
                    l_actual,
                    5,
                    l_start_time + ( 130 * l_tick_interval ),
                    130 * l_tick_interval, 
                    7
                );
                MyTimerClient_NTimesFixedInterval l_client6(
                    l_timer_service,
                    l_actual,
                    6,
                    l_start_time + ( 170 * l_tick_interval ),
                    170 * l_tick_interval, 
                    5
                );
                MyTimerClient_NTimesFixedInterval l_client7(
                    l_timer_service,
                    l_actual,
                    7,
                    l_start_time + ( 190 * l_tick_interval ),
                    190 * l_tick_interval, 
                    5
                );
                MyTimerClient_NTimesFixedInterval l_client8(
                    l_timer_service,
                    l_actual,
                    8,
                    l_start_time + ( 230 * l_tick_interval ),
                    230 * l_tick_interval, 
                    4
                );

                l_timer_service = 0;

                Task::Sleep( 1050 * l_tick_interval );

            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( SameOrder( l_actual, l_expected), "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif

        }


        {

            cout <<
                "Register and unregister clients at various times while the timer\n"
                "service is running.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 1 );

            TimerLog l_expected;
            l_expected.push_back( 1, 45 );
            l_expected.push_back( 1, 90 );
            l_expected.push_back( 1, 135 );
            l_expected.push_back( 2, 145 );
            l_expected.push_back( 1, 180 );
            l_expected.push_back( 2, 190 );
            l_expected.push_back( 2, 235 );
            l_expected.push_back( 2, 280 );
            l_expected.push_back( 2, 325 );
            l_expected.push_back( 1, 345 );
            l_expected.push_back( 2, 370 );
            l_expected.push_back( 1, 390 );
            l_expected.push_back( 2, 415 );
            l_expected.push_back( 1, 425 );
            l_expected.push_back( 3, 445 );
            l_expected.push_back( 2, 460 );
            l_expected.push_back( 1, 470 );
            l_expected.push_back( 3, 490 );
            l_expected.push_back( 2, 505 );
            l_expected.push_back( 3, 535 );
            l_expected.push_back( 2, 550 );
            l_expected.push_back( 3, 580 );
            l_expected.push_back( 2, 595 );
            l_expected.push_back( 2, 640 );
            l_expected.push_back( 2, 685 );
            l_expected.push_back( 2, 730 );
            l_expected.push_back( 1, 745 );
            l_expected.push_back( 2, 775 );
            l_expected.push_back( 1, 790 );
            l_expected.push_back( 1, 835 );
            l_expected.push_back( 1, 880 );
            l_expected.push_back( 1, 925 );
            l_expected.push_back( 2, 945 );
            l_expected.push_back( 1, 970 );
            l_expected.push_back( 2, 990 );
            l_expected.push_back( 1, 1015 );
            l_expected.push_back( 1, 1060 );
            l_expected.push_back( 1, 1105 );
            l_expected.push_back( 2, 1145 );
            l_expected.push_back( 1, 1150 );
            l_expected.push_back( 2, 1190 );
            l_expected.push_back( 1, 1195 );
            l_expected.push_back( 2, 1235 );
            l_expected.push_back( 1, 1240 );
            l_expected.push_back( 3, 1245 );
            l_expected.push_back( 2, 1280 );
            l_expected.push_back( 1, 1285 );
            l_expected.push_back( 3, 1290 );
            l_expected.push_back( 2, 1325 );
            l_expected.push_back( 1, 1330 );
            l_expected.push_back( 2, 1370 );
            l_expected.push_back( 1, 1375 );
            l_expected.push_back( 2, 1415 );
            l_expected.push_back( 2, 1460 );
            l_expected.push_back( 2, 1505 );
            l_expected.push_back( 1, 1545 );
            l_expected.push_back( 2, 1550 );
            l_expected.push_back( 1, 1590 );
            l_expected.push_back( 2, 1595 );
            l_expected.push_back( 1, 1635 );
            l_expected.push_back( 1, 1680 );
            l_expected.push_back( 1, 1725 );
            l_expected.push_back( 2, 1745 );
            l_expected.push_back( 1, 1770 );
            l_expected.push_back( 2, 1790 );
            l_expected.push_back( 2, 1835 );
            l_expected.push_back( 2, 1880 );
            l_expected.push_back( 2, 1925 );
            l_expected.push_back( 1, 1945 );
            l_expected.push_back( 2, 1970 );
            l_expected.push_back( 1, 1990 );
            l_expected.push_back( 1, 2035 );
            l_expected.push_back( 1, 2080 );
            l_expected.push_back( 1, 2125 );
            l_expected.push_back( 2, 2145 );
            l_expected.push_back( 1, 2170 );
            l_expected.push_back( 2, 2190 );
            l_expected.push_back( 2, 2235 );
            l_expected.push_back( 2, 2280 );
            l_expected.push_back( 2, 2325 );
            l_expected.push_back( 1, 2345 );
            l_expected.push_back( 2, 2370 );
            l_expected.push_back( 1, 2390 );
            l_expected.push_back( 2, 2415 );
            l_expected.push_back( 1, 2435 );
            l_expected.push_back( 3, 2445 );
            l_expected.push_back( 2, 2460 );
            l_expected.push_back( 1, 2480 );
            l_expected.push_back( 3, 2490 );
            l_expected.push_back( 2, 2505 );
            l_expected.push_back( 1, 2525 );
            l_expected.push_back( 3, 2535 );
            l_expected.push_back( 4, 2545 );
            l_expected.push_back( 2, 2550 );
            l_expected.push_back( 1, 2570 );
            l_expected.push_back( 3, 2580 );
            l_expected.push_back( 4, 2590 );
            l_expected.push_back( 2, 2595 );
            l_expected.push_back( 1, 2615 );
            l_expected.push_back( 3, 2625 );
            l_expected.push_back( 2, 2640 );
            l_expected.push_back( 1, 2660 );
            l_expected.push_back( 3, 2670 );
            l_expected.push_back( 2, 2685 );
            l_expected.push_back( 1, 2705 );
            l_expected.push_back( 2, 2730 );
            l_expected.push_back( 1, 2750 );
            l_expected.push_back( 2, 2775 );
            l_expected.push_back( 1, 2795 );
            l_expected.push_back( 1, 2840 );
            l_expected.push_back( 1, 2885 );
            l_expected.push_back( 1, 2930 );
            l_expected.push_back( 2, 2945 );
            l_expected.push_back( 1, 2975 );
            l_expected.push_back( 2, 2990 );
            l_expected.push_back( 1, 3020 );
            l_expected.push_back( 2, 3035 );
            l_expected.push_back( 3, 3045 );
            l_expected.push_back( 1, 3065 );
            l_expected.push_back( 2, 3080 );
            l_expected.push_back( 3, 3090 );
            l_expected.push_back( 2, 3125 );
            l_expected.push_back( 3, 3135 );
            l_expected.push_back( 2, 3170 );
            l_expected.push_back( 3, 3180 );
            l_expected.push_back( 2, 3215 );
            l_expected.push_back( 3, 3225 );
            l_expected.push_back( 1, 3245 );
            l_expected.push_back( 2, 3260 );
            l_expected.push_back( 3, 3270 );
            l_expected.push_back( 1, 3290 );
            l_expected.push_back( 2, 3305 );
            l_expected.push_back( 3, 3315 );
            l_expected.push_back( 1, 3335 );
            l_expected.push_back( 4, 3345 );
            l_expected.push_back( 2, 3350 );
            l_expected.push_back( 3, 3360 );
            l_expected.push_back( 1, 3380 );
            l_expected.push_back( 4, 3390 );
            l_expected.push_back( 2, 3395 );
            l_expected.push_back( 3, 3405 );
            l_expected.push_back( 1, 3425 );
            l_expected.push_back( 4, 3435 );
            l_expected.push_back( 2, 3440 );
            l_expected.push_back( 5, 3445 );
            l_expected.push_back( 3, 3450 );
            l_expected.push_back( 1, 3470 );
            l_expected.push_back( 4, 3480 );
            l_expected.push_back( 2, 3485 );
            l_expected.push_back( 5, 3490 );
            l_expected.push_back( 3, 3495 );
            l_expected.push_back( 1, 3515 );
            l_expected.push_back( 4, 3525 );
            l_expected.push_back( 5, 3535 );
            l_expected.push_back( 3, 3540 );
            l_expected.push_back( 1, 3560 );
            l_expected.push_back( 4, 3570 );
            l_expected.push_back( 5, 3580 );
            l_expected.push_back( 3, 3585 );
            l_expected.push_back( 1, 3605 );
            l_expected.push_back( 4, 3615 );
            l_expected.push_back( 5, 3625 );
            l_expected.push_back( 1, 3650 );
            l_expected.push_back( 4, 3660 );
            l_expected.push_back( 5, 3670 );
            l_expected.push_back( 1, 3695 );
            l_expected.push_back( 4, 3705 );
            l_expected.push_back( 5, 3715 );
            l_expected.push_back( 4, 3750 );
            l_expected.push_back( 5, 3760 );
            l_expected.push_back( 4, 3795 );
            l_expected.push_back( 5, 3805 );
            l_expected.push_back( 5, 3850 );
            l_expected.push_back( 5, 3895 );
            l_expected.push_back( 5, 3940 );
            l_expected.push_back( 1, 3945 );
            l_expected.push_back( 5, 3985 );
            l_expected.push_back( 1, 3990 );
            l_expected.push_back( 1, 4035 );
            l_expected.push_back( 1, 4080 );
            l_expected.push_back( 1, 4125 );
            l_expected.push_back( 2, 4145 );
            l_expected.push_back( 1, 4170 );
            l_expected.push_back( 2, 4190 );
            l_expected.push_back( 1, 4215 );
            l_expected.push_back( 2, 4235 );
            l_expected.push_back( 3, 4245 );
            l_expected.push_back( 1, 4260 );
            l_expected.push_back( 2, 4280 );
            l_expected.push_back( 3, 4290 );
            l_expected.push_back( 1, 4305 );
            l_expected.push_back( 3, 4335 );
            l_expected.push_back( 1, 4350 );
            l_expected.push_back( 3, 4380 );
            l_expected.push_back( 1, 4395 );
            l_expected.push_back( 3, 4425 );
            l_expected.push_back( 1, 4440 );
            l_expected.push_back( 2, 4445 );
            l_expected.push_back( 3, 4470 );
            l_expected.push_back( 1, 4485 );
            l_expected.push_back( 2, 4490 );
            l_expected.push_back( 3, 4515 );
            l_expected.push_back( 1, 4530 );
            l_expected.push_back( 2, 4535 );
            l_expected.push_back( 4, 4545 );
            l_expected.push_back( 3, 4560 );
            l_expected.push_back( 1, 4575 );
            l_expected.push_back( 2, 4580 );
            l_expected.push_back( 4, 4590 );
            l_expected.push_back( 3, 4605 );
            l_expected.push_back( 2, 4625 );
            l_expected.push_back( 4, 4635 );
            l_expected.push_back( 3, 4650 );
            l_expected.push_back( 2, 4670 );
            l_expected.push_back( 4, 4680 );
            l_expected.push_back( 3, 4695 );
            l_expected.push_back( 2, 4715 );
            l_expected.push_back( 4, 4725 );
            l_expected.push_back( 3, 4740 );
            l_expected.push_back( 1, 4745 );
            l_expected.push_back( 2, 4760 );
            l_expected.push_back( 4, 4770 );
            l_expected.push_back( 3, 4785 );
            l_expected.push_back( 1, 4790 );
            l_expected.push_back( 4, 4815 );
            l_expected.push_back( 3, 4830 );
            l_expected.push_back( 1, 4835 );
            l_expected.push_back( 4, 4860 );
            l_expected.push_back( 3, 4875 );
            l_expected.push_back( 1, 4880 );
            l_expected.push_back( 4, 4905 );
            l_expected.push_back( 1, 4925 );
            l_expected.push_back( 4, 4950 );
            l_expected.push_back( 1, 4970 );
            l_expected.push_back( 4, 4995 );
            l_expected.push_back( 1, 5015 );
            l_expected.push_back( 4, 5040 );
            l_expected.push_back( 2, 5045 );
            l_expected.push_back( 1, 5060 );
            l_expected.push_back( 4, 5085 );
            l_expected.push_back( 2, 5090 );
            l_expected.push_back( 4, 5130 );
            l_expected.push_back( 2, 5135 );
            l_expected.push_back( 4, 5175 );
            l_expected.push_back( 2, 5180 );
            l_expected.push_back( 2, 5225 );
            l_expected.push_back( 2, 5270 );

            TimeStamp l_start_time = ThreadSystemTime();

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n";

            TimerLog l_actual( l_start_time, l_tick_interval, true );
#else
            TimerLog l_actual( l_start_time, l_tick_interval, false );
#endif

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > l_client1;
                Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > l_client2;
                Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > l_client3;
                Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > l_client4;
                Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > l_client5;

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 45 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 100 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 145 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 200 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 300 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 345 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 400 * l_tick_interval ) - ThreadSystemTime() );

                l_client3 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 445 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 500 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 600 * l_tick_interval ) );

                l_client3 = 0;

                Task::Sleep( l_start_time + ( 700 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 745 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 800 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 900 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 945 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 1000 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 1100 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 1145 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 1200 * l_tick_interval ) );

                l_client3 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 1245 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 1300 * l_tick_interval ) );

                l_client3 = 0;

                Task::Sleep( l_start_time + ( 1400 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 1500 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 1545 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 1600 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 1700 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 1745 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 1800 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 1900 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 1945 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 2000 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 2100 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 2145 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 2200 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 2300 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 2345 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 2400 * l_tick_interval ) );

                l_client3 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 2445 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 2500 * l_tick_interval ) );

                l_client4 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    4,
                    l_start_time + ( 2545 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 2600 * l_tick_interval ) );

                l_client4 = 0;

                Task::Sleep( l_start_time + ( 2700 * l_tick_interval ) );

                l_client3 = 0;

                Task::Sleep( l_start_time + ( 2800 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 2900 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 2945 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 3000 * l_tick_interval ) );

                l_client3 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 3045 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 3100 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 3200 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 3245 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 3300 * l_tick_interval ) );

                l_client4 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    4,
                    l_start_time + ( 3345 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 3400 * l_tick_interval ) );

                l_client5 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    5,
                    l_start_time + ( 3445 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 3500 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 3600 * l_tick_interval ) );

                l_client3 = 0;

                Task::Sleep( l_start_time + ( 3700 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 3800 * l_tick_interval ) );

                l_client4 = 0;

                Task::Sleep( l_start_time + ( 3900 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 3945 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 4000 * l_tick_interval ) );

                l_client5 = 0;

                Task::Sleep( l_start_time + ( 4100 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 4145 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 4200 * l_tick_interval ) );

                l_client3 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    3,
                    l_start_time + ( 4245 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 4300 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 4400 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 4445 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 4500 * l_tick_interval ) );

                l_client4 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    4,
                    l_start_time + ( 4545 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 4600 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 4700 * l_tick_interval ) );

                l_client1 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    1,
                    l_start_time + ( 4745 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                Task::Sleep( l_start_time + ( 4800 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 4900 * l_tick_interval ) );

                l_client3 = 0;

                Task::Sleep( l_start_time + ( 5000 * l_tick_interval ) );

                l_client2 = new MyTimerClient_NTimesFixedInterval_RefCounted(
                    l_timer_service,
                    l_actual,
                    2,
                    l_start_time + ( 5045 * l_tick_interval ),
                    45 * l_tick_interval, 
                    10000
                );

                l_timer_service = 0;

                Task::Sleep( l_start_time + ( 5100 * l_tick_interval ) );

                l_client1 = 0;

                Task::Sleep( l_start_time + ( 5200 * l_tick_interval ) );

                l_client4 = 0;

                Task::Sleep( l_start_time + ( 5300 * l_tick_interval ) );

                l_client2 = 0;

                Task::Sleep( l_start_time + ( 5400 * l_tick_interval ) );

            }

            cout << "\n\n";

#if 0
            AT_TCAssert( SameOrder( l_actual, l_expected ), "Actual event log doesn't match expected." );
#endif

        }


        {

            cout <<
                "Repeatedly register a client for an event in the far future, then\n"
                "immediately destroy it, many times.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::Secs( 1000000000 );

            TimerLog l_expected;

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                for ( int l_i = 0; l_i != 10000; ++l_i )
                {
                    MyTimerClient_NTimesFixedInterval l_client1(
                        l_timer_service,
                        l_actual,
                        1,
                        l_start_time + l_tick_interval,
                        l_tick_interval,
                        1
                    );
                }
            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( SameOrder( l_actual, l_expected), "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif

        }


        {

            cout <<
                "Register new events from within an event.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::NanoSecs( 100 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                {
                    vector< Ptr< MyTimerClient2 * > > l_clients;
                    Ptr< MyTimerClient2 * > l_root1 =
                        new MyTimerClient2(
                            l_timer_service,
                            l_actual,
                            1,
                            l_clients,
                            l_start_time + l_tick_interval,
                            l_tick_interval,
                            6,
                            3,
                            1
                        );
                    Ptr< MyTimerClient2 * > l_root2 =
                        new MyTimerClient2(
                            l_timer_service,
                            l_actual,
                            2,
                            l_clients,
                            l_start_time + l_tick_interval,
                            l_tick_interval,
                            7,
                            1,
                            3
                        );
                    Ptr< MyTimerClient2 * > l_root3 =
                        new MyTimerClient2(
                            l_timer_service,
                            l_actual,
                            3,
                            l_clients,
                            l_start_time + l_tick_interval,
                            l_tick_interval,
                            5,
                            2,
                            2
                        );

                    l_timer_service = 0;

                    Task::Sleep( TimeInterval::Secs( 1 ) );

                }

#if 0
                cout << "\nGot:\n" << l_actual << "\n";
#else
                cout << "\n\n";
#endif

            }

        }

        {

            cout <<
                "Cancel clients from within an event.\n" << endl;

            TimeInterval l_tick_interval = TimeInterval::MilliSecs( 1 );

            TimerLog l_expected;
            l_expected.push_back( 2, 45 );
            l_expected.push_back( 3, 45 );
            l_expected.push_back( 4, 45 );
            l_expected.push_back( 5, 45 );
            l_expected.push_back( 6, 45 );
            l_expected.push_back( 2, 90 );
            l_expected.push_back( 3, 90 );
            l_expected.push_back( 4, 90 );
            l_expected.push_back( 5, 90 );
            l_expected.push_back( 6, 90 );
            l_expected.push_back( 1, 100 );
            l_expected.push_back( 3, 135 );
            l_expected.push_back( 4, 135 );
            l_expected.push_back( 5, 135 );
            l_expected.push_back( 6, 135 );
            l_expected.push_back( 3, 180 );
            l_expected.push_back( 4, 180 );
            l_expected.push_back( 5, 180 );
            l_expected.push_back( 6, 180 );
            l_expected.push_back( 1, 200 );
            l_expected.push_back( 4, 225 );
            l_expected.push_back( 5, 225 );
            l_expected.push_back( 6, 225 );
            l_expected.push_back( 4, 270 );
            l_expected.push_back( 5, 270 );
            l_expected.push_back( 6, 270 );
            l_expected.push_back( 1, 300 );
            l_expected.push_back( 5, 315 );
            l_expected.push_back( 6, 315 );
            l_expected.push_back( 5, 360 );
            l_expected.push_back( 6, 360 );
            l_expected.push_back( 1, 400 );
            l_expected.push_back( 6, 405 );
            l_expected.push_back( 6, 450 );
            l_expected.push_back( 6, 495 );
            l_expected.push_back( 1, 500 );
            l_expected.push_back( 1, 600 );

            TimeStamp l_start_time = ThreadSystemTime();

            TimerLog l_actual( l_start_time, l_tick_interval );

            {
                Ptr< TimerService * > l_timer_service =
                    FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
                AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

                {
                    vector< Ptr< MyTimerClient_NTimesFixedInterval_RefCounted * > > l_clients( 5 );
                    Ptr< MyTimerClient3 * > l_client;
                    l_client =
                        new MyTimerClient3(
                            l_timer_service,
                            l_actual,
                            1,
                            l_clients,
                            l_client,
                            l_start_time + ( 100 * l_tick_interval ),
                            100 * l_tick_interval
                        );

                    for ( int l_i = 0; l_i != 5; ++l_i )
                    {
                        l_clients[ l_i ] =
                            new MyTimerClient_NTimesFixedInterval_RefCounted(
                                l_timer_service,
                                l_actual,
                                2 + l_i,
                                l_start_time + ( 45 * l_tick_interval ),
                                45 * l_tick_interval,
                                100
                            );
                    }

                    Task::Sleep( 1000 * l_tick_interval );

#if 0
                    for ( int l_j = 0; l_j != 5; ++l_j )
                    {
                        AT_TCAssert( ! l_clients[ l_j ] , "Client pointer didn't get nulled." );
                    }
                    AT_TCAssert( ! l_client , "Client pointer didn't get nulled." );
#endif

                }

            }

#if 0
            cout << "\nExpected:\n" << l_expected <<
                    "\nGot:\n" << l_actual << "\n";

            AT_TCAssert( l_actual == l_expected, "Actual event log doesn't match expected." );
#else
            cout << "\n\n";
#endif

        }

    }



};

AT_RegisterTest( Timer, Timer );



// ======== Timer_TaskBase =============================================

template <int N, int ThreadCount = 3 >
class Timer_TaskBase
  : public Task
{
    public:

    virtual ~Timer_TaskBase() {}

    static const int            m_thr_count = ThreadCount;
    static const int            m_iters = 1 << 15;

    static volatile unsigned    m_count;
    
    static volatile unsigned    m_count_left;

    static AtomicCount          m_value;

    static ConditionalMutex     m_mutex;

    virtual void TestWork( int l_thrnum ) = 0;
    
    static Ptr< MutexRefCount * >  s_mutex;
    static Ptr< MutexRefCount * >  s_assmutex;

    void Work();
};

template <int N, int ThreadCount >
AtomicCount     Timer_TaskBase<N,ThreadCount>::m_value;

template <int N, int ThreadCount >
ConditionalMutex     Timer_TaskBase<N,ThreadCount>::m_mutex;

template <int N, int ThreadCount >
Ptr< MutexRefCount * >  Timer_TaskBase<N,ThreadCount>::s_mutex;

template <int N, int ThreadCount >
Ptr< MutexRefCount * >  Timer_TaskBase<N,ThreadCount>::s_assmutex;

template <int N, int ThreadCount >
volatile unsigned    Timer_TaskBase<N,ThreadCount>::m_count;
    
template <int N, int ThreadCount >
volatile unsigned    Timer_TaskBase<N,ThreadCount>::m_count_left;

template <int N, int ThreadCount >
void Timer_TaskBase<N,ThreadCount>::Work()
{

    unsigned l_num;
    
    {
        // stuff is done here

        Lock<ConditionalMutex>      l_lock( m_mutex );

        l_num = m_count ++;
        ++ m_count_left;

        if ( ! s_mutex )
        {
            s_mutex = new MutexRefCount( Mutex::Recursive );
            s_assmutex = new MutexRefCount( );
        }
    
        if ( ( m_thr_count - 1 ) == l_num )
        {
            std::cerr << l_num << " calling PostAll\n";
            l_lock.PostAll();
        }
        else
        {
            l_lock.Wait();
        }
    }

    TestWork( l_num );
    
}


class MonteTimerClient
  : public TimerClient_Basic
{

public:

    TimeInterval    m_tick_interval;
    bool            m_reschedule;

    MonteTimerClient()
      : m_reschedule()
    {
    }

private:

    virtual bool Event(
        const TimeStamp & i_current_time,
        TimeStamp & o_reschedule_time    
    ) {
        if ( ! m_reschedule )
        {
            return false;
        }
        
        o_reschedule_time = i_current_time + m_tick_interval;
        return true;
    }

};

class MonteTimerService
{
public:

    Ptr< TimerService * >   m_ts;

    MonteTimerService()
      : m_ts( FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )() )
    {

        AT_TCAssert( m_ts != 0, "Factory failed to create TimerService." );
    }
};



const unsigned g_primes[] = { 1, 2, 3, 7, 11 };

class TimerHardTask
  : public Timer_TaskBase< 2 >
{
    public:

    const static int                s_lead_count = 100;
    static MonteTimerClient         s_leads[ s_lead_count ];
    static MonteTimerService      * s_ts;
    static AtomicCount              s_task_counter;

    virtual void TestWork( int l_thrnum )
    {
        unsigned pnum = g_primes[ l_thrnum % CountElements( g_primes ) ];
        unsigned count = l_thrnum;
        unsigned done = 0;

        MonteTimerClient            l_local_client;

        TimeStamp l_start_time = ThreadSystemTime();
            
        l_local_client.m_tick_interval = TimeInterval::MilliSecs( 10 * l_thrnum );
        s_ts->m_ts->StartTimer( l_local_client, l_start_time + l_local_client.m_tick_interval );

        
        for ( int i = 0; i < m_iters; ++i )
        {
            MonteTimerClient   & l_lead = s_leads[ count % s_lead_count ];
            count += pnum;

            unsigned operation = count % 13;
            count += pnum;

            switch ( operation )
            {
                case 0 :
                case 4 :
                {
                    l_lead.LeadCancel();
                    break;
                }
                case 1 :
                case 5 :
                case 6 :
                case 7 :
                case 12 :
                {
                    s_ts->m_ts->StartTimer( l_lead, l_start_time + TimeInterval::MilliSecs( 10 * l_thrnum ) );
                    break;
                }
                case 2 :
                case 9 :
                case 10 :
                {
                    l_lead.m_tick_interval = l_local_client.m_tick_interval;
                    break;
                }
                case 3 :
                case 11 :
                {
                    l_lead.m_reschedule = (count % 17) < 10;
                    break;
                }
            }

        }

        for ( int i = 0; i < s_lead_count; ++i )
        {
            MonteTimerClient    & l_lead = s_leads[ i ];
            l_lead.LeadCancel();
        }

        s_task_counter.Bump( 1 );
    }

    TimerHardTask()
    {
        Start();
    }

    ~TimerHardTask()
    {
        Wait();
    }
};

AtomicCount             TimerHardTask::s_task_counter;
MonteTimerClient        TimerHardTask::s_leads[ TimerHardTask::s_lead_count ];
MonteTimerService     * TimerHardTask::s_ts = 0;


AT_DefineTestLevel(
    TimerMonte,
    Timer,
    "Timer Monte Carlo stress test",
    UnitTestTraits::TestSuccess,
    0
)
{

    void Run()
    {

        // create a timer
        MonteTimerService       l_ts;
        TimerHardTask::s_ts = & l_ts;

        // create and destruct all the tasks
        {
            TimerHardTask       l_tasks[ TimerHardTask::m_thr_count ];
        }
        
        
    }
};


AT_RegisterTest( TimerMonte, Timer );


class ShutdownClient
  : public TimerClient_Basic
{


public:

    ShutdownClient( int i_client_number )
      : m_client_number( i_client_number ),
        m_cancelled()
    {
    }


    virtual bool Event(
        const TimeStamp & i_current_time,
        TimeStamp & o_reschedule_time
    )
    {
        return false;
    }

    // ======== AppLeadCompleted ==================================
    virtual void AppLeadCompleted(
        TwinTraits::TwinCode i_completion_code
    )
    {
        std::cerr << "ShutdownClient was cancelled " << m_client_number << "\n";;
        m_cancelled = true;
    }

    int     m_client_number;
    bool    m_cancelled;
};


AT_DefineTestLevel(
    TimerShutdown,
    Timer,
    "Basic Timer Service Shutdown Test",
    UnitTestTraits::TestSuccess,
    0
)
{

    void Run()
    {

        ShutdownClient      c1(1);
        ShutdownClient      c2(2);

        Ptr< TimerService * > l_timer_service =
            FactoryRegister< TimerService, DKy >::Get().Create( "Basic" )();
        AT_TCAssert( l_timer_service != 0, "Factory failed to create TimerService." );

        TimeStamp l_start_time = ThreadSystemTime();

        l_timer_service->StartTimer( c1, l_start_time + TimeInterval::Secs( 3000 ) );
        at::Ptr<MutexRefCount *>    l_mutex1 = c1.GetMutex();
        l_timer_service->StartTimer( c2, l_start_time + TimeInterval::Secs( 2000 ) );
        at::Ptr<MutexRefCount *>    l_mutex2 = c2.GetMutex();

        const MutexRefCount::t_count & cn1 = l_mutex1->GetRefCount();
        const MutexRefCount::t_count & cn2 = l_mutex2->GetRefCount();
        
        MutexRefCount * mc1 = &*l_mutex1;
        MutexRefCount * mc2 = &*l_mutex2;

        int count1 = cn1.Get();
        int count2 = cn2.Get();

        l_mutex1 = 0;
        l_mutex2 = 0;
    
        int counta1 = cn1.Get();
        int counta2 = cn2.Get();

        l_timer_service->Shutdown();
    }
};


AT_RegisterTest( TimerShutdown, Timer );



