/**
 * tst_thread.cpp
 *
 * tst_thread.cpp tests threads
 *
 */

#include "at_unit_test.h"
#include "at_assert.h"
#include "at_thread.h"
#include "at_atomic.h"
#include <cstdlib>

using namespace at;

AT_TestArea( Threads, "Test for threading" );



// ======== Task_TestBase =============================================

template <int N, int ThreadCount = 3>
class Task_TestBase
  : public Task
{
    public:

    virtual ~Task_TestBase() {}

    static const int            m_thr_count = ThreadCount;
    static const int            m_iters = 1 << 22;

    static volatile unsigned    m_count;
    
    static volatile unsigned    m_count_left;

    static AtomicCount          m_value;

    static ConditionalMutex     m_mutex;

    virtual void TestWork( int l_thrnum ) = 0;
    
    void Work()
    {

        unsigned l_num;
        
        {
            // stuff is done here
    
            Lock<ConditionalMutex>      l_lock( m_mutex );

            l_num = m_count ++;
            ++ m_count_left;
    
            if ( ( m_thr_count - 1 ) == l_num )
            {
                std::cerr << l_num << " calling PostAll\n";
                l_lock.PostAll();
            }
            else
            {
                l_lock.Wait();
            }
        }

        TestWork( l_num );
        
    }
};


template <int N, int ThreadCount>
volatile unsigned   Task_TestBase<N,ThreadCount>::m_count = 0;

template <int N, int ThreadCount>
volatile unsigned   Task_TestBase<N,ThreadCount>::m_count_left = 0;

template <int N, int ThreadCount>
AtomicCount         Task_TestBase<N,ThreadCount>::m_value;

template <int N, int ThreadCount>
ConditionalMutex    Task_TestBase<N,ThreadCount>::m_mutex;


// ======== Task_Test ===============================================

class Task_Test
  : public Task_TestBase<1>
{
    public:

    Task_Test()
      : Task_TestBase<1>()
    {
        Start();
    }

    virtual ~Task_Test()
    {
        Wait();
    }

    void TestWork( int l_thrnum )
    {

        for ( int i = m_iters; i --; )
        {
            ++ m_value;
        }
        
    }

};

#ifdef unix
#include <unistd.h>
#endif

AT_DefineTest( BasicThreadTest, Threads, "Initial threading test" )
{
	void Run()
	{

        srand( 44 );
        
        {
			Task_Test       l_tests[ Task_Test::m_thr_count ];

        }

        // all threads should now be complete

        AT_TCAssert( Task_Test::m_value.Get() == Task_Test::m_iters * Task_Test::m_thr_count, "m_value should equal m_iters * m_thr_count" );
		return;
	}

};

AT_RegisterTest( BasicThreadTest, Threads );



// ======== Test_ConditionLazyHolder ==================================
/**
 * 
 *
 */

struct Test_ConditionLazyHolder
{
    public:

    Test_ConditionLazyHolder()
      : m_cl( 0 ),
        m_wait_count(),
        m_allocated( false )
    {
        
    }

    void Wait()
    {
        ++ m_wait_count;
        m_cl->Wait();
        -- m_wait_count;
    }

    void Wipe()
    {
        delete m_cl;
        m_cl = 0;
    }

    void Post()
    {
        m_cl->Post();
    }

    void PostAll()
    {
        m_cl->PostAll();
    }

    void New( Mutex & i_mutex )
    {
        AT_TCAssert( m_cl == 0, "Creating a mutex over somthing already there !" );
        m_cl = new ConditionLazy( i_mutex );
    }

    ConditionLazy               * volatile m_cl;
    int                                    m_wait_count;
    bool                                   m_allocated;
};


// ======== Task_TestLazee ===============================================

typedef Task_TestBase<2,3>   LazeeBase;

class Task_TestLazee
  : public LazeeBase
{
    public:
    
    static const unsigned           m_iters = 1 << 9;

    static const bool               m_verbose = false;

    // millisecs to wait
    static const unsigned           m_time = 0;
    
    static volatile unsigned        m_count_wait;
    
    static const int                m_cl_count = 2;
    
    static Test_ConditionLazyHolder m_lazy_cond[ m_cl_count ];

    
    Task_TestLazee()
      : LazeeBase()
    {
        Start();
    }

    ~Task_TestLazee()
    {
        Wait();
    }

    enum Ops
    {
        
    };

    void TestWork( int l_thrnum )
    {
        for ( int i = m_iters; i --; )
        {

            int     l_cond = std::rand() % m_cl_count;

            Test_ConditionLazyHolder * l_lazee = & m_lazy_cond[ l_cond ];

            Lock<Mutex>         l_lock( m_mutex );

            if ( l_lazee->m_cl )
            {
                // there is a ConditionLazy here ...
                // - should I wait for it ?

                if ( m_count_left > 2 )
                {
                    // let's wait !

                    ++ m_count_wait;

                    if ( m_verbose ) {
                        std::cerr << "Thread " << l_thrnum << " calling Wait (" << l_cond << ") " << m_count_wait << "\n";
                    }
                    l_lazee->Wait();
                    -- m_count_wait;
                    if ( m_verbose ) {
                        std::cerr << "Thread " << l_thrnum << " woken (" << l_cond << ") " << m_count_wait << "\n";
                    }
                }
            }
            else
            {
                // go do somthing else if this is still allocated
                if ( ! l_lazee->m_allocated )
                {
                    l_lazee->m_allocated = true;
                    // no condition lazy here ...
                    l_lazee->New( m_mutex );
    
                    {
                        Unlock<Mutex>       l_unlock( m_mutex );
    
                        int l_numb_iter = 1 + ( std::rand() % 5 );
                        
                        while ( l_numb_iter -- )
                        {
                            // sleep for 10 milliseconds
                            at::Task::Sleep( TimeInterval( m_time * TimeInterval::PerMilliSec ) );
    
                            int l_one_or_all = std::rand() % 2;
    
                            Lock<Mutex>         l_lock( m_mutex );
                                
                            if ( l_one_or_all )
                            {
                                if ( m_verbose ) {
                                    std::cerr << "Thread "<< l_thrnum << " calling Post (" << l_cond << ") " << m_count_wait << "\n";
                                }
                                l_lazee->Post();
                            }
                            else
                            {
                                if ( m_verbose ) {
                                    std::cerr << "Thread "<< l_thrnum << " calling PostAll (" << l_cond << ") " << m_count_wait << "\n";
                                }
                                l_lazee->PostAll();
                            }
    
                        }
                    }
    
                    // we're locked again here
                    // get rid of the lazy thing
    
                    l_lazee->PostAll();
                    l_lazee->Wipe(); // destruction of ConditionLazy - the underlying
                                     // condition variable is potentially still alive
    
                    while ( l_lazee->m_wait_count )
                    {
                        {
                            Unlock<Mutex>       l_unlock( m_mutex );
                            at::Task::Sleep( TimeInterval( m_time * TimeInterval::PerMilliSec ) );

                        }

// l_lazee->PostAll();

                        if ( l_lazee->m_wait_count )
                        {
                            if ( m_verbose ) {
                                std::cerr << l_thrnum << " wait poll " << l_lazee->m_wait_count << "\n";
                            }
                        }
                        
                    }
// l_lazee->Wipe();

                    l_lazee->m_allocated = false;                    
                }

            }
        }
        
    }

};

Test_ConditionLazyHolder    Task_TestLazee::m_lazy_cond[ Task_TestLazee::m_cl_count ];

volatile unsigned   Task_TestLazee::m_count_wait = 0;

AT_DefineTest( ConditionLazee, Threads, "Lazy condition variable test" )
{
	void Run()
	{
        {
			Task_TestLazee       l_tests[ Task_TestLazee::m_thr_count ];
        }

        AT_TCAssert( Task_TestLazee::m_count_wait == 0, "There should be no waiters" );

        // all threads should now be complete - verify that all ConditionLazy
        // objects are deleted

        for ( int i = 0; i < Task_TestLazee::m_cl_count; ++ i )
        {
            AT_TCAssert( Task_TestLazee::m_lazy_cond[i].m_cl == 0, "ConditionLazy should have been deleted" );
        }

		return;
	}

};

AT_RegisterTest( ConditionLazee, Threads );


// ======== Ptr_Test ===============================================

struct PtrTest_Object
  : public PtrTarget_MT
{

	bool						* m_ptester;
    
    PtrTest_Object( bool & o_tester_bool )
      : m_ptester( & o_tester_bool )
    {
        o_tester_bool = false;
    }

    ~PtrTest_Object()
    {
        * m_ptester = true;
    }
};

struct PtrTest_Base
{
    Ptr<PtrTest_Object *, PtrTraits< PtrTest_Object *, PtrClassRefWrapperMT< PtrTest_Object * > > > m_ptr_a;
    
    Ptr<PtrTest_Object *, PtrTraits< PtrTest_Object *, PtrClassRefWrapperMT< PtrTest_Object * > > > m_ptr_b;
    
};

PtrTest_Base        * g_ptr_test = 0;

class PtrTest_Task
  : public Task_TestBase<3>
{
    public:

    PtrTest_Base            & m_ptrtest_base;

    PtrTest_Task()
      : Task_TestBase<3>(),
        m_ptrtest_base( * g_ptr_test )
    {
        Start();
    }

    virtual ~PtrTest_Task()
    {
        Wait();
    }

    void TestWork( int l_thrnum )
    {

        Ptr<PtrTest_Object *>   l_ptr;
    
        for ( int i = m_iters; i --; )
        {

            switch ( i % 8 )
            {
                case 1 :
                case 3 :
                {
                    l_ptr = m_ptrtest_base.m_ptr_a;
                    break;
                }
                
                case 0 :
                case 4 :
                {
                    m_ptrtest_base.m_ptr_a = l_ptr;
                    break;
                }

                case 2 :
                {
                    l_ptr = m_ptrtest_base.m_ptr_b;
                    break;
                }

                default :
                {
                    m_ptrtest_base.m_ptr_a = m_ptrtest_base.m_ptr_b;
                    break;
                }
                    
            }
        }
        
    }

};


AT_DefineTest( MultithreadedPtr, Threads, "Ptr with multithreaded assignment etc" )
{
	void Run()
	{

        g_ptr_test = new PtrTest_Base;

        bool    l_deleted;

        PtrTest_Object   * l_obj = new PtrTest_Object( l_deleted );

        g_ptr_test->m_ptr_b = l_obj;
        g_ptr_test->m_ptr_a = g_ptr_test->m_ptr_b;

        {
			PtrTest_Task       l_tests[ PtrTest_Task::m_thr_count ];

        }

        AT_TCAssert( ! l_deleted, "The object was deleted - oh no" );
        
        // all threads should now be complete
        delete g_ptr_test;
        g_ptr_test = 0;
        
        AT_TCAssert( l_deleted, "Expected the object to be deleted" );

		return;
	}

};

AT_RegisterTest( MultithreadedPtr, Threads );


struct Task_TestTryLockSharedData
{

    Mutex m_mutex;

    bool m_flag;

    Task_TestTryLockSharedData()
      : m_mutex( Mutex::NonRecursive )
    {
    }

};


class Task_TestTryLock1
  : public Task
{

    Task_TestTryLockSharedData & m_data;

public:

    Task_TestTryLock1( Task_TestTryLockSharedData & io_data )
      : m_data( io_data )
    {
        Start();
    }

    ~Task_TestTryLock1()
    {
        Wait();
    }

    void Work()
    {
        Lock< Mutex > l_lock( m_data.m_mutex );
        Task::Sleep( TimeInterval::MilliSecs( 20 ) );
        m_data.m_flag = true;
        Task::Sleep( TimeInterval::MilliSecs( 10 ) );
    }

};


class Task_TestTryLock2
  : public Task
{

    Task_TestTryLockSharedData & m_data;

public:

    Task_TestTryLock2( Task_TestTryLockSharedData & io_data )
      : m_data( io_data )
    {
        Start();
    }

    ~Task_TestTryLock2()
    {
        Wait();
    }

    void Work()
    {
        Task::Sleep( TimeInterval::MilliSecs( 10 ) );
        TryLock< Mutex > m_lock( m_data.m_mutex, m_data.m_flag );
        AT_TCAssert( ! m_lock.IsAcquired(), "Should not have aquired the lock." );
    }

};


class Task_TestTryLock3
  : public Task
{

    Task_TestTryLockSharedData & m_data;

public:

    Task_TestTryLock3( Task_TestTryLockSharedData & io_data )
      : m_data( io_data )
    {
        Start();
    }

    ~Task_TestTryLock3()
    {
        Wait();
    }

    void Work()
    {
        {
            Lock< Mutex > l_lock( m_data.m_mutex );
            Task::Sleep( TimeInterval::MilliSecs( 20 ) );
        }
        Task::Sleep( TimeInterval::MilliSecs( 10 ) );
    }

};


class Task_TestTryLock4
  : public Task
{

    Task_TestTryLockSharedData & m_data;

public:

    Task_TestTryLock4( Task_TestTryLockSharedData & io_data )
      : m_data( io_data )
    {
        Start();
    }

    ~Task_TestTryLock4()
    {
        Wait();
    }

    void Work()
    {
        Task::Sleep( TimeInterval::MilliSecs( 10 ) );
        TryLock< Mutex > m_lock( m_data.m_mutex, m_data.m_flag );
        AT_TCAssert( m_lock.IsAcquired(), "Should have aquired the lock." );
    }

};


AT_DefineTest( TryLock, Threads, "TryLock test" )
{

    void Run()
    {
        Task_TestTryLockSharedData l_shared;
        l_shared.m_flag = false;
        {
            Task_TestTryLock1 l_task1( l_shared );
            Task_TestTryLock2 l_task2( l_shared );
        }
        l_shared.m_flag = false;
        {
            Task_TestTryLock3 l_task3( l_shared );
            Task_TestTryLock4 l_task4( l_shared );
        }
    }

};


AT_RegisterTest( TryLock, Threads );
