
#include "at_bw_transform.h"

#include "at_unit_test.h"

using namespace at;

AT_TestArea( ATBWTransform, "BW Transform" );

#include <iostream>

AT_DefineTest( Basic, ATBWTransform, "Basic BW Transform code test" )
{
	void Run()
	{
		std::string			test_str = "fun faster fast";

		std::vector<char>	test_result( test_str.size() );

		unsigned i = BWTEncode( test_str.begin(), test_str.end(), test_result.begin() );

		std::cout << "BW( \"" << test_str << "\" ) = (" << i << ") \""
			<< std::string( test_result.begin(), test_result.end() ) << "\"\n";

		std::vector<char>	test_decode( test_str.size() );

		BWTDecode( i, test_result.begin(), test_result.end(), test_decode.begin() );

		std::cout << "Decode( \"" << std::string( test_result.begin(), test_result.end() ) << "\" ) (" << i << ") = \""
			<< std::string( test_decode.begin(), test_decode.end() ) << "\"\n";

		AT_TCAssert( std::string( test_decode.begin(), test_decode.end() ) == test_str, "BWT decode failed" );
	}
};

AT_RegisterTest( Basic, ATBWTransform );


