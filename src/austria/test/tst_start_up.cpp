
#include "at_start_up.h"

#include "at_unit_test.h"

using namespace at;

AT_TestArea( StartUp, "Start Up Initializers" );

//
// Start-up initializers are a little difficult to test because
// it happens around the framework, nonetheless, we can test
// wether the initializer is called.

class TestInitializer
  : public Initializer
{
public:

    static int m_tester;
    
    TestInitializer( int & argc, const char ** & argv )
    {
        m_tester = 3;
    }

    ~TestInitializer()
    {
        m_tester = 2;
    }

};

// make the factory ...
AT_MakeFactory2P( "TestInitializer", TestInitializer, Initializer, DKy, int & , const char ** & );

int TestInitializer::m_tester = 0;

class InitializerTest
{
public:
    ~InitializerTest()
    {
		AT_TCAssert( TestInitializer::m_tester == 2, "TestInitializer failed to be destroyed." );
    }

};

// the destructor of this object should be called when the
// the test program is about to exit and it will check that
// the initializer destructor is called.
InitializerTest     basic_destruct_test;


AT_DefineTest( Basic, StartUp, "Start Up basic test" )
{
	void Run()
	{
		AT_TCAssert( TestInitializer::m_tester == 3, "TestInitializer failed to be created" );
	}
};

AT_RegisterTest( Basic, StartUp );


