
#include "at_gray_code.h"

#include "at_unit_test.h"

using namespace at;

AT_TestArea( ATGrayCode, "gray_code test" );


#include <iostream>

template <typename T1, typename T2, bool bigger = sizeof(T1) >= sizeof(T2) >
struct bigger_t
{
	typedef T1	Type;
};

template <typename T1, typename T2>
struct bigger_t<T1,T2,false>
{
	typedef T2	Type;
};

template <typename T, bool verbose>
void gray_checker( const T & value )
{
	typedef typename bigger_t<T,int>::Type	tint;

	if ( verbose )
	{
		std::cout << "bin_to_gray( gray_to_bin( " << tint(value) << " ) ) = " << tint(gray_tools::bin_to_gray( gray_tools::gray_to_bin( value ))) << "\n";
		std::cout << "bin_to_gray( " << tint(value) << " ) = " << tint(gray_tools::bin_to_gray( value )) << "\n";
		std::cout << "gray_to_bin( " << tint(value) << " ) = " << tint(gray_tools::gray_to_bin( value )) << "\n";
	}
    
    AT_TCAssert( gray_tools::bin_to_gray( gray_tools::gray_to_bin( (value) ) ) == (value), "Conversion error" );
}



template <typename T, bool verbose>
void graytest( T from = std::numeric_limits<T>::min(), T to = std::numeric_limits<T>::max() )
{

    T tester;

    for (
        tester = from;
        tester < to;
        tester ++
    )
    {
        gray_checker<T, verbose>( tester );
    }

    gray_checker<T, verbose>( to );
}



AT_DefineTestLevel(
    Basic,
    ATGrayCode,
    "Basic gray code test",
    at::UnitTestTraits::TestSuccess,
    5
)
{
	void Run()
	{


		graytest<unsigned char, false>();
		graytest<char, false>();
	
		graytest<unsigned short, false>();
		graytest<short, false>();
	
		graytest<unsigned int, false>( 0, 300 );
		graytest<unsigned int, false>( ~0U-300, ~0U );
		graytest<int, false>( -200, 300 );
	
		graytest<unsigned long long, false>( 0, 300 );
		
		graytest<unsigned long long, false>( ~0ULL-300, ~0ULL );
	
	
		gray_code<unsigned char>		x(200);
		gray_code<unsigned>				y(x);
	
		AT_TCAssert( y.gtez(), "gtez() failed - must evaluate to true for unsigned" );
	
		x = y;
	}
};

AT_RegisterTest( Basic, ATGrayCode );


