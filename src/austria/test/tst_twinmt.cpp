/**
 *  Test twins.
 *
 */

#include "at_twinmt_basic.h"
#include "at_twin_basic.h"

#include "at_unit_test.h"

#include <cstring>

using namespace at;

namespace {

class LeadApplicationMT
  : public LeadTwinMT_Basic< NotifyLeadInterface >
{

  public:

    LeadApplicationMT()
      : m_code( 0 ),
        m_completion_code( TwinTraits::Uninitialized )
    {
    }

    virtual void Notify( int i_code )
    {
        m_code = i_code;

        std::cout << "Notify called - i_code=" << i_code << "\n";
    }

    void AppLeadCompleted( TwinTraits::TwinCode i_completion_code )
    {
        m_completion_code = i_completion_code;
    }

    int  m_code;

    TwinTraits::TwinCode  m_completion_code;

};


class AideApplication
  : public AideTwinMT_Basic< NotifyLeadInterface >
{

  public:

    void DoNotifyLead( int i_code )
    {
        CallLead().VoidCall( &NotifyLeadInterface::Notify, i_code );
    }

};

class AideApplication2
  : public AideTwinMT_Basic< NotifyLeadInterface >
{

  public:

    void DoNotifyLead( int i_code )
    {
        CallLead().VoidCall( &NotifyLeadInterface::Notify, i_code );
    }

    void AppAideCloseNotify( TwinTraits::TwinCode  i_completion_code )
    {
        m_completion_code = i_completion_code;
    }

    AideApplication2()
      : m_completion_code( TwinTraits::Uninitialized )
    {
    }

    TwinTraits::TwinCode  m_completion_code;

};

AT_TestArea( TwinMT, "Multi-threaded Lead and Aide Twin Module" );

class Twins_Fixture
{

  public:

    LeadApplicationMT  m_lead[ 1 ];

    LeadApplicationMT  m_lead2[ 1 ];

    AideApplication  m_aide[ 1 ];

    Twins_Fixture()
    {
    }

};

#define  CharAssign( x_dst, x_val )  std::memcpy( x_dst, x_val, sizeof( x_val ) )

AT_DefineTest( BasicTest, TwinMT, "Basic test" ),
    public Twins_Fixture
{

    void Run()
    {

        Ptr< MutexRefCount * >  m_mutex = new MutexRefCount( Mutex::Recursive );

        m_aide->AideAssociate( m_lead, m_mutex );

        m_aide->DoNotifyLead( 33 );

        AT_TCAssert( m_lead->m_code == 33, "Notification failure" );

        m_lead->LeadCancel();

        AT_TCAssert( m_lead->m_completion_code == TwinTraits::CancelRequested, "Cancel failed" );

    }

};

AT_RegisterTest( BasicTest, TwinMT );


class LeadHardTest
  : public LeadTwinMT_Basic< NotifyLeadInterface >
{

  public:
    static AtomicCount                  s_counter;
    static const int                    s_lead_count = 5;

    LeadHardTest()
    {
    }

    virtual void Notify( int i_code )
    {
        ++ s_counter;
    }

};

AtomicCount LeadHardTest::s_counter;


class AideLeadHardTest
  : public AideTwinMT_Basic< NotifyLeadInterface >
{

  public:

    bool DoNotifyLead()
    {
        int l_val = 0;
        return CallLead().VoidCall( &NotifyLeadInterface::Notify, l_val );
    }

};

// ======== Task_TestBase =============================================

template <int N, int ThreadCount = 3 >
class Twin_TaskBase
  : public Task
{
    public:

    virtual ~Twin_TaskBase() {}

    static const int            m_thr_count = ThreadCount;
    static const int            m_iters = 1 << 19;

    static volatile unsigned    m_count;
    
    static volatile unsigned    m_count_left;

    static AtomicCount          m_value;

    static ConditionalMutex     m_mutex;

    virtual void TestWork( int l_thrnum ) = 0;
    
    static Ptr< MutexRefCount * >  s_mutex;
    static Ptr< MutexRefCount * >  s_assmutex;

    void Work();
};

template <int N, int ThreadCount >
AtomicCount     Twin_TaskBase<N,ThreadCount>::m_value;

template <int N, int ThreadCount >
ConditionalMutex     Twin_TaskBase<N,ThreadCount>::m_mutex;

template <int N, int ThreadCount >
Ptr< MutexRefCount * >  Twin_TaskBase<N,ThreadCount>::s_mutex;

template <int N, int ThreadCount >
Ptr< MutexRefCount * >  Twin_TaskBase<N,ThreadCount>::s_assmutex;

template <int N, int ThreadCount >
volatile unsigned    Twin_TaskBase<N,ThreadCount>::m_count;
    
template <int N, int ThreadCount >
volatile unsigned    Twin_TaskBase<N,ThreadCount>::m_count_left;

template <int N, int ThreadCount >
void Twin_TaskBase<N,ThreadCount>::Work()
{

    unsigned l_num;
    
    {
        // stuff is done here

        Lock<ConditionalMutex>      l_lock( m_mutex );

        l_num = m_count ++;
        ++ m_count_left;

        if ( ! s_mutex )
        {
            s_mutex = new MutexRefCount( Mutex::Recursive );
            s_assmutex = new MutexRefCount( );
        }
    
        if ( ( m_thr_count - 1 ) == l_num )
        {
            std::cerr << l_num << " calling PostAll\n";
            l_lock.PostAll();
        }
        else
        {
            l_lock.Wait();
        }
    }

    TestWork( l_num );
    
}

const unsigned g_primes[] = { 1, 2, 3, 7, 11 };

class HardTask
  : public Twin_TaskBase< 2 >
{
    public:
    static LeadHardTest            s_leads[ LeadHardTest::s_lead_count ];
    static AideLeadHardTest        s_aides[ m_thr_count ];
    static AtomicCount             s_task_counter;

    virtual void TestWork( int l_thrnum )
    {
        unsigned pnum = g_primes[ l_thrnum % CountElements( g_primes ) ];
        unsigned count = l_thrnum;
        unsigned done = 0;

        for ( int i = 0; i < m_iters; ++i )
        {
            LeadHardTest    & l_lead = s_leads[ count % LeadHardTest::s_lead_count ];
            count += pnum;
            AideLeadHardTest    & l_aide = s_aides[ l_thrnum ];

            {
                Lock<Mutex>    l_lock( * s_assmutex );
                l_aide.AideAssociate( & l_lead, s_mutex );
            }

            if ( l_aide.DoNotifyLead() )
            {
                ++ done;
            }
        }

        s_task_counter.Bump( done );
    }

    HardTask()
    {
        Start();
    }

    ~HardTask()
    {
        Wait();
    }
};

AtomicCount HardTask::s_task_counter;
LeadHardTest            HardTask::s_leads[ LeadHardTest::s_lead_count ];
AideLeadHardTest        HardTask::s_aides[ m_thr_count ];
    
AT_DefineTest( HardTest, TwinMT, "Basic test" )
{

    void Run()
    {
        {
            // This will both start
            HardTask        l_tasks[ HardTask::m_thr_count ];
            // and end the test threads
        }

        // then this should be good.
        AT_Assert( HardTask::s_task_counter.Get() == LeadHardTest::s_counter.Get() );

    }

};

AT_RegisterTest( HardTest, TwinMT );



} // namespace 
