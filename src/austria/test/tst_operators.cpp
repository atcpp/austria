#include "at_operators.h"
#include "at_unit_test.h"

using namespace at;


class MyClass1
  : public EqualOps< MyClass1 >
{

    int m_value;

public:

    MyClass1()
      : m_value( 0 )
    {
    }

    MyClass1( int i_value )
      : m_value( i_value )
    {
    }

    bool operator==( const MyClass1 & i_rhs ) const
    {
        return m_value == i_rhs.m_value;
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass2
  : public CompareOps< MyClass2 >
{

    int m_value;

public:

    MyClass2()
      : m_value( 0 )
    {
    }

    MyClass2( int i_value )
      : m_value( i_value )
    {
    }

    bool operator==( const MyClass2 & i_rhs ) const
    {
        return m_value == i_rhs.m_value;
    }

    bool operator<( const MyClass2 & i_rhs ) const
    {
        return m_value < i_rhs.m_value;
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass3;


class MyClass4
{

    friend class MyClass3;
    friend class MyClass6;
    friend class MyClass10;

    int m_value;

public:

    MyClass4()
      : m_value( 0 )
    {
    }

    MyClass4( int i_value )
      : m_value( i_value )
    {
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass3
  : public AddSubtractOps< MyClass3, MyClass4 >
{

    int m_value;

public:

    MyClass3()
      : m_value( 0 )
    {
    }

    MyClass3( int i_value )
      : m_value( i_value )
    {
    }

    MyClass3 & operator+=( const MyClass4 & i_rhs )
    {
        m_value += i_rhs.m_value;
        return *this;
    }

    MyClass3 & operator-=( const MyClass4 & i_rhs )
    {
        m_value -= i_rhs.m_value;
        return *this;
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass5
  : public AddSubtractOps< MyClass5, MyClass5 >
{

    int m_value;

public:

    MyClass5()
      : m_value( 0 )
    {
    }

    MyClass5( int i_value )
      : m_value( i_value )
    {
    }

    MyClass5 & operator+=( const MyClass5 & i_rhs )
    {
        m_value += i_rhs.m_value;
        return *this;
    }

    MyClass5 & operator-=( const MyClass5 & i_rhs )
    {
        m_value -= i_rhs.m_value;
        return *this;
    }

    MyClass5 operator-() const
    {
        return MyClass5( - m_value );
    }

    MyClass5 operator-( const MyClass5 & i_rhs ) const
    {
        return AddSubtractOps< MyClass5, MyClass5 >::operator-( i_rhs );
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass6;


class MyClass7
{

    friend class MyClass6;

    int m_value;

public:

    MyClass7()
      : m_value( 0 )
    {
    }

    MyClass7( int i_value )
      : m_value( i_value )
    {
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass6
  : public AddSubtractOps< MyClass6, MyClass4 >,
    public AddSubtractOps< MyClass6, MyClass7 >
{

    int m_value;

public:

    MyClass6()
      : m_value( 0 )
    {
    }

    MyClass6( int i_value )
      : m_value( i_value )
    {
    }

    MyClass6 & operator+=( const MyClass4 & i_rhs )
    {
        m_value += i_rhs.m_value;
        return *this;
    }

    MyClass6 & operator+=( const MyClass7 & i_rhs )
    {
        m_value += i_rhs.m_value;
        return *this;
    }

    MyClass6 & operator-=( const MyClass4 & i_rhs )
    {
        m_value -= i_rhs.m_value;
        return *this;
    }

    MyClass6 & operator-=( const MyClass7 & i_rhs )
    {
        m_value -= i_rhs.m_value;
        return *this;
    }

    MyClass6 operator+( const MyClass4 & i_rhs ) const
    {
        return AddSubtractOps< MyClass6, MyClass4 >::operator+( i_rhs );
    }

    MyClass6 operator+( const MyClass7 & i_rhs ) const
    {
        return AddSubtractOps< MyClass6, MyClass7 >::operator+( i_rhs );
    }

    MyClass6 operator-( const MyClass4 & i_rhs ) const
    {
        return AddSubtractOps< MyClass6, MyClass4 >::operator-( i_rhs );
    }

    MyClass6 operator-( const MyClass7 & i_rhs ) const
    {
        return AddSubtractOps< MyClass6, MyClass7 >::operator-( i_rhs );
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass8
  : public IncrementOps< MyClass8 >
{

    int m_value;

public:

    MyClass8()
      : m_value( 0 )
    {
    }

    MyClass8( int i_value )
      : m_value( i_value )
    {
    }

    MyClass8 & operator++()
    {
        ++m_value;
        return *this;
    }

    MyClass8 operator++( int i )
    {
        return IncrementOps< MyClass8 >::operator++( i );
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass9
  : public IncrementDecrementOps< MyClass9 >
{

    int m_value;

public:

    MyClass9()
      : m_value( 0 )
    {
    }

    MyClass9( int i_value )
      : m_value( i_value )
    {
    }

    MyClass9 & operator++()
    {
        ++m_value;
        return *this;
    }

    MyClass9 operator++( int i )
    {
        return IncrementDecrementOps< MyClass9 >::operator++( i );
    }

    MyClass9 & operator--()
    {
        --m_value;
        return *this;
    }

    MyClass9 operator--( int i )
    {
        return IncrementDecrementOps< MyClass9 >::operator--( i );
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass10
  : public IntegerAddSubtractOps< MyClass10, MyClass4 >
{

    int m_value;

public:

    MyClass10()
      : m_value( 0 )
    {
    }

    MyClass10( int i_value )
      : m_value( i_value )
    {
    }

    MyClass10 & operator+=( const MyClass4 & i_rhs )
    {
        m_value += i_rhs.m_value;
        return *this;
    }

    MyClass10 & operator-=( const MyClass4 & i_rhs )
    {
        m_value -= i_rhs.m_value;
        return *this;
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass11;


class MyClass12
{

    friend class MyClass11;
    friend class MyClass13;

    int m_value;

public:

    MyClass12()
      : m_value( 0 )
    {
    }

    MyClass12( int i_value )
      : m_value( i_value )
    {
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass11
  : public IntegerMultiplyDivideOps< MyClass11, MyClass12 >
{

    int m_value;

public:

    MyClass11()
      : m_value( 0 )
    {
    }

    MyClass11( int i_value )
      : m_value( i_value )
    {
    }

    MyClass11 & operator*=( const MyClass12 & i_rhs )
    {
        m_value *= i_rhs.m_value;
        return *this;
    }

    MyClass11 & operator/=( const MyClass12 & i_rhs )
    {
        m_value /= i_rhs.m_value;
        return *this;
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass13;


class MyClass14
{

    friend class MyClass13;

    int m_value;

public:

    MyClass14()
      : m_value( 0 )
    {
    }

    MyClass14( int i_value )
      : m_value( i_value )
    {
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass13
  : public IntegerMultiplyDivideOps< MyClass13, MyClass12 >,
    public IntegerMultiplyDivideOps< MyClass13, MyClass14 >
{

    int m_value;

public:

    MyClass13()
      : m_value( 0 )
    {
    }

    MyClass13( int i_value )
      : m_value( i_value )
    {
    }

    MyClass13 & operator*=( const MyClass12 & i_rhs )
    {
        m_value *= i_rhs.m_value;
        return *this;
    }

    MyClass13 & operator*=( const MyClass14 & i_rhs )
    {
        m_value *= i_rhs.m_value;
        return *this;
    }

    MyClass13 & operator/=( const MyClass12 & i_rhs )
    {
        m_value /= i_rhs.m_value;
        return *this;
    }

    MyClass13 & operator/=( const MyClass14 & i_rhs )
    {
        m_value /= i_rhs.m_value;
        return *this;
    }

    MyClass13 operator*( const MyClass12 & i_rhs ) const
    {
        return IntegerMultiplyDivideOps< MyClass13, MyClass12 >::operator*( i_rhs );
    }

    MyClass13 operator*( const MyClass14 & i_rhs ) const
    {
        return IntegerMultiplyDivideOps< MyClass13, MyClass14 >::operator*( i_rhs );
    }

    MyClass13 operator/( const MyClass12 & i_rhs ) const
    {
        return IntegerMultiplyDivideOps< MyClass13, MyClass12 >::operator/( i_rhs );
    }

    MyClass13 operator/( const MyClass14 & i_rhs ) const
    {
        return IntegerMultiplyDivideOps< MyClass13, MyClass14 >::operator/( i_rhs );
    }

    int Value() const
    {
        return m_value;
    }

};


class MyClass15
  : public ModulusOps< MyClass15 >
{

    int m_value;

public:

    MyClass15()
      : m_value( 0 )
    {
    }

    MyClass15( int i_value )
      : m_value( i_value )
    {
    }

    MyClass15 & operator%=( const MyClass15 & i_rhs )
    {
        m_value %= i_rhs.m_value;
        return *this;
    }

    int Value() const
    {
        return m_value;
    }

};


AT_TestArea( Ops, "Overloaded Operator module tests" );


AT_DefineTest( Ops, Ops, "Basic Overloaded Operator module test" )
{

    void Run()
    {

        const MyClass1 l_myvar_1a( 55 );
        const MyClass1 l_myvar_1b( 55 );
        const MyClass1 l_myvar_1c( 101 );

        AT_TCAssert( ! ( l_myvar_1a != l_myvar_1b ), "l_myvar_1a != l_myvar_1b should evaluate false\n" );
        AT_TCAssert( l_myvar_1a != l_myvar_1c, "l_myvar_1a != l_myvar_1c should evaluate true\n" );

        const MyClass2 l_myvar_2a( 55 );
        const MyClass2 l_myvar_2b( 55 );
        const MyClass2 l_myvar_2c( 101 );

        AT_TCAssert( ! ( l_myvar_2a > l_myvar_2b ), "l_myvar_2a > l_myvar_2b should evaluate false\n" );
        AT_TCAssert( ! ( l_myvar_2a > l_myvar_2c ), "l_myvar_2a > l_myvar_2c should evaluate false\n" );
        AT_TCAssert( l_myvar_2c > l_myvar_2a, "l_myvar_2c > l_myvar_2a should evaluate true\n" );
        AT_TCAssert( l_myvar_2a <= l_myvar_2b, "l_myvar_2a <= l_myvar_2b should evaluate true\n" );
        AT_TCAssert( l_myvar_2a <= l_myvar_2c, "l_myvar_2a <= l_myvar_2c should evaluate true\n" );
        AT_TCAssert( !( l_myvar_2c <= l_myvar_2a ), "l_myvar_2c <= l_myvar_2a should evaluate false\n" );
        AT_TCAssert( l_myvar_2a >= l_myvar_2b, "l_myvar_2a >= l_myvar_2b should evaluate true\n" );
        AT_TCAssert( !( l_myvar_2a >= l_myvar_2c ), "l_myvar_2a >= l_myvar_2c should evaluate false\n" );
        AT_TCAssert( l_myvar_2c >= l_myvar_2a, "l_myvar_2c >= l_myvar_2a should evaluate true\n" );

        MyClass3 l_myvar_3a;
        const MyClass4 l_myvar_4a( 55 );
        const MyClass3 l_myvar_3b( 101 );

        AT_TCAssert( ( l_myvar_3b + l_myvar_4a ).Value() == 156, "l_myvar_3b + l_myvar_4a should equal 156\n" );
        AT_TCAssert( ( l_myvar_3b - l_myvar_4a ).Value() == 46, "l_myvar_3b - l_myvar_4a should equal 46\n" );

        MyClass5 l_myvar_5a;
        const MyClass5 l_myvar_5b( 55 );
        const MyClass5 l_myvar_5c( 101 );

        AT_TCAssert( ( l_myvar_5c + l_myvar_5b ).Value() == 156, "l_myvar_5c + l_myvar_5b should equal 156\n" );
        AT_TCAssert( ( l_myvar_5c - l_myvar_5b ).Value() == 46, "l_myvar_5c - l_myvar_5b should equal 46\n" );

        MyClass6 l_myvar_6a;
        const MyClass7 l_myvar_7a( 42 );
        const MyClass6 l_myvar_6b( 101 );

        AT_TCAssert( ( l_myvar_6b + l_myvar_4a ).Value() == 156, "l_myvar_6b + l_myvar_4a should equal 156\n" );
        AT_TCAssert( ( l_myvar_6b - l_myvar_4a ).Value() == 46, "l_myvar_6b - l_myvar_4a should equal 46\n" );
        AT_TCAssert( ( l_myvar_6b + l_myvar_7a ).Value() == 143, "l_myvar_6b + l_myvar_7a should equal 143\n" );
        AT_TCAssert( ( l_myvar_6b - l_myvar_7a ).Value() == 59, "l_myvar_6b - l_myvar_7a should equal 59\n" );

        MyClass8 l_myvar_8a;
        l_myvar_8a = MyClass8( 55 );
        AT_TCAssert( l_myvar_8a++.Value() == 55, "l_myvar_8a++ should evaluate as 55\n" );
        AT_TCAssert( l_myvar_8a.Value() == 56, "l_myvar_8a should equal 56\n" );

        MyClass9 l_myvar_9a;
        l_myvar_9a = MyClass9( 55 );
        AT_TCAssert( l_myvar_9a++.Value() == 55, "l_myvar_9a++ should evaluate as 55\n" );
        AT_TCAssert( l_myvar_9a.Value() == 56, "l_myvar_9a should equal 56\n" );
        l_myvar_9a = MyClass9( 55 );
        AT_TCAssert( l_myvar_9a--.Value() == 55, "l_myvar_9a-- should evaluate as 55\n" );
        AT_TCAssert( l_myvar_9a.Value() == 54, "l_myvar_9a should equal 54\n" );

        MyClass10 l_myvar_10a;
        MyClass10 l_myvar_10b( 101 );

        AT_TCAssert( ( l_myvar_10b + l_myvar_4a ).Value() == 156, "l_myvar_10b + l_myvar_4a should equal 156\n" );
        AT_TCAssert( ( l_myvar_10b - l_myvar_4a ).Value() == 46, "l_myvar_10b - l_myvar_4a should equal 46\n" );
        l_myvar_10a = MyClass10( 55 );
        AT_TCAssert( ( ++l_myvar_10a ).Value() == 56, "++l_myvar_10a should evaluate as 56\n" );
        AT_TCAssert( l_myvar_10a++.Value() == 56, "l_myvar_10a++ should evaluate as 56\n" );
        AT_TCAssert( l_myvar_10a.Value() == 57, "l_myvar_10a should equal 57\n" );
        l_myvar_10a = MyClass10( 55 );
        AT_TCAssert( ( --l_myvar_10a ).Value() == 54, "--l_myvar_10a should evaluate as 54\n" );
        AT_TCAssert( l_myvar_10a--.Value() == 54, "l_myvar_10a-- should evaluate as 54\n" );
        AT_TCAssert( l_myvar_10a.Value() == 53, "l_myvar_10a should equal 53\n" );

        const MyClass11 l_myvar_11a( 55 );
        const MyClass12 l_myvar_12a( 11 );
        AT_TCAssert( ( l_myvar_11a * l_myvar_12a ).Value() == 605, "l_myvar_11a * l_myvar_12a should equal 605\n" );
        AT_TCAssert( ( l_myvar_11a / l_myvar_12a ).Value() == 5, "l_myvar_11a / l_myvar_12a should equal 5\n" );

        const MyClass13 l_myvar_13a( 55 );
        const MyClass14 l_myvar_14a( 5 );
        AT_TCAssert( ( l_myvar_13a * l_myvar_12a ).Value() == 605, "l_myvar_13a * l_myvar_12a should equal 605\n" );
        AT_TCAssert( ( l_myvar_13a / l_myvar_12a ).Value() == 5, "l_myvar_13a / l_myvar_12a should equal 5\n" );
        AT_TCAssert( ( l_myvar_13a * l_myvar_14a ).Value() == 275, "l_myvar_13a * l_myvar_14a should equal 275\n" );
        AT_TCAssert( ( l_myvar_13a / l_myvar_14a ).Value() == 11, "l_myvar_13a / l_myvar_14a should equal 11\n" );

        const MyClass15 l_myvar_15a( 101 );
        const MyClass15 l_myvar_15b( 42 );
        AT_TCAssert( ( l_myvar_15a % l_myvar_15b ).Value() == 17, "l_myvar_15a % l_myvar_15a should equal 17\n" );

    }

};

AT_RegisterTest( Ops, Ops );
