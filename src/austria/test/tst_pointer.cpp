
#include "at_pointers.h"

#include "at_unit_test.h"

using namespace at;

AT_TestArea( PointerWrapper, "at_pointers.h pointer wrapper" );


#include <list>

typedef std::list<int>				t_list;
typedef t_list::iterator		t_iterator;


class PointerWrapperTest
{
	public:

	typedef PointerTo<t_iterator>::t_pointer_traits_debug t_pwrapd;
	typedef PointerTo<t_iterator>::t_pointer_traits_nodebug t_pwrap;

	t_list			m_list;

	t_pwrapd		m_wrapperd1;
	t_pwrapd		m_wrapperd2;
	
	t_pwrap 		m_wrapper1;
	t_pwrap			m_wrapper2;

	t_iterator MakeIter()
	{
		m_list.push_front( 6 );

		return m_list.begin();
	}

	PointerWrapperTest()
	  : m_wrapperd1( MakeIter() ),
		m_wrapperd2(),
	    m_wrapper1( MakeIter() ),
		m_wrapper2()
	{
	}


};

AT_DefineTest( Basic, PointerWrapper, "Basic iterator function test" ) , public PointerWrapperTest
{
	void Run()
	{
		m_wrapper2 = m_wrapper1;

		m_wrapper2.AssignInvalid();

		m_wrapper2.Get();

		m_wrapper2.AssignNull();

		
		m_wrapperd2 = m_wrapperd1;

		m_wrapperd2.AssignInvalid();

		m_wrapperd2.Get();

	}
	
};

AT_RegisterTest( Basic, PointerWrapper );


