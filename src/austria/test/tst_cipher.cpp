
#include "at_crypto.h"

#include "at_unit_test.h"

AT_TestArea( Ciphers, "Ciphers" );

#include <iostream>


namespace {

const char s_pub_key[] =
"-----BEGIN RSA PUBLIC KEY-----\n"
"MEgCQQDWDXQb9588j1dWHuiBzjvFl7bFyYzebo4HujTjdQpSUceoxHCFh3sxgVTP\n"
"p7FmJdSGvcdci4sBFf+oUk3zj4WzAgMBAAE=\n"
"-----END RSA PUBLIC KEY-----\n";

const char s_pub_keyx[] =
"-----BEGIN RSA PUBLIC KEY-----\n"
"MEgCQQDWDXQb9588j1dWHuiBzjvFl7bFyYzebo4HujTjdQpSUc\n"
"eoxHCFh3sxgVTPp7FmJdSGvcdci4sBFf+oUk3zj4WzAgMBAAE=\n"
"-----END RSA PUBLIC KEY-----\n";

const char s_prv_key[] =
"-----BEGIN RSA PRIVATE KEY-----\n"
"MIIBPAIBAAJBANYNdBv3nzyPV1Ye6IHOO8WXtsXJjN5ujge6NON1ClJRx6jEcIWH\n"
"ezGBVM+nsWYl1Ia9x1yLiwEV/6hSTfOPhbMCAwEAAQJBAK2LxNqz/l/sW8QQVTSB\n"
"EAcZBXABBHyHKBJtz1mnWBH6jga5/cYOB2OQPlvSdTJbR7iN8gBfahk58T9CZQp5\n"
"X3ECIQD21KEB7EqIpkMBZowJFZRbW3+EjMNdDUXEBQtwvfL0uwIhAN4BGpumlHBM\n"
"XxVNcItUvjs8Qv6LiL6aOVuM+zFEq59pAiEA2ZbVnKldhxryJJDuz69LSkHdps6k\n"
"BiqGHufHjwqT27MCIQCqs36aIEQS9SZHYvweplJ/iZ6ZywCiTkO8OXMMNxkfwQIg\n"
"YIKD/8ryUizzqfZ8ljpZmgbvRQsfOzbL5VsVMGtfa4g=\n"
"-----END RSA PRIVATE KEY-----\n";

void printit( const std::string & x )
{
    std::cout << "length = " << x.length() << " '" << x << "'" << std::endl;
}

AT_DefineTestLevel(
    RSA,
    Ciphers,
    "The simple pass-thru cipher test",
    at::UnitTestTraits::TestSuccess,
    0
)
{
	void Run()
	{
        at::Ptr< at::CipherPublicKey * > l_key_genr = at::NewKeyGenPublic( "RSA" );

        AT_TCAssert( l_key_genr, "Failed to generate public key generator" );

        std::string     l_public_key;
        std::string     l_private_key;

        l_key_genr->GenerateKey( l_public_key, l_private_key );

        at::Ptr<at::Buffer *>   l_public_key_buf = at::NewBuffer();
        at::Ptr<at::Buffer *>   l_private_key_buf = at::NewBuffer();

        l_key_genr->GenerateKey( l_public_key_buf, l_private_key_buf );

        std::cout << "\n";
        std::cout << l_public_key_buf->Region().String() << "\n";
        std::cout << l_private_key_buf->Region().String() << std::endl;
        
        at::Ptr< at::Cipher * >     l_pub_cipher = l_key_genr->NewPublicCipher();
        at::Ptr< at::Cipher * >     l_prv_cipher = l_key_genr->NewPrivateCipher();

        AT_TCAssert( l_pub_cipher, "Could not create Cipher" );
        AT_TCAssert( l_prv_cipher, "Could not create Cipher" );

        std::string l_orig_str = "HELLO";
        
        std::string l_enc_str = l_pub_cipher->Encrypt( l_public_key, l_orig_str );

        std::string l_dec_str = l_prv_cipher->Decrypt( l_private_key, l_enc_str );

        printit( l_orig_str );
        printit( l_dec_str );
        
        AT_TCAssert( l_orig_str == l_dec_str, "Encrypted/decrypted strings do not match" );

        
        std::string l_enc_str1 = l_prv_cipher->Encrypt( l_private_key, l_orig_str );

        std::string l_dec_str1 = l_pub_cipher->Decrypt( l_public_key, l_enc_str1 );

        printit( l_dec_str1 );
        
        AT_TCAssert( l_orig_str == l_dec_str1, "Encrypted/decrypted strings do not match" );

        
        l_pub_cipher = at::NewCipher( "RSA_PUB" );

        AT_TCAssert( l_pub_cipher, "RSA_PUB cipher not found" );

        std::string l_enc_str2 = l_pub_cipher->Encrypt( l_public_key, l_orig_str );

        std::string l_dec_str2 = l_prv_cipher->Decrypt( l_private_key, l_enc_str );
        
        printit( l_dec_str2 );
        
        AT_TCAssert( l_dec_str2 == l_orig_str, "Encrypted/decrypted strings do not match" );

        
        l_prv_cipher = at::NewCipher( "RSA_PRV" );

        AT_TCAssert( l_prv_cipher, "RSA_PRV cipher not found" );

        std::string l_enc_str3 = l_prv_cipher->Encrypt( l_private_key, l_orig_str );

        std::string l_dec_str3 = l_pub_cipher->Decrypt( l_public_key, l_enc_str3 );
    
        AT_TCAssert( l_dec_str3 == l_orig_str, "Encrypted/decrypted strings do not match" );
        
	}
};

AT_RegisterTest( RSA, Ciphers );

AT_DefineTestLevel(
    RSA2,
    Ciphers,
    "The simple pass-thru cipher test",
    at::UnitTestTraits::TestSuccess,
    0
)
{
	void Run()
	{
        
        std::string l_orig_str = "HELLO BABY";
        
        at::Ptr< at::Cipher * > l_pub_cipher = at::NewCipher( "RSA_PUB" );

        at::Ptr< at::Cipher * > l_prv_cipher = at::NewCipher( "RSA_PRV" );

        printit( "A" );
        
        std::string l_enc_str4 = l_prv_cipher->Encrypt( s_prv_key, l_orig_str );

        std::string l_dec_str4 = l_pub_cipher->Decrypt( s_pub_key, l_enc_str4 );
    
        AT_TCAssert( l_dec_str4 == l_orig_str, "Encrypted/decrypted strings do not match" );
        
        printit( "B" );
        
        std::string l_enc_str5 = l_pub_cipher->Encrypt( s_pub_keyx, l_orig_str );

        std::string l_dec_str5 = l_prv_cipher->Decrypt( s_prv_key, l_enc_str5 );
    
        AT_TCAssert( l_dec_str5 == l_orig_str, "Encrypted/decrypted strings do not match" );
        
        printit( "C" );
        
        l_enc_str5 = l_pub_cipher->Encrypt( s_pub_key, l_orig_str );

        l_dec_str5 = l_prv_cipher->Decrypt( s_prv_key, l_enc_str5 );
    
        AT_TCAssert( l_dec_str5 == l_orig_str, "Encrypted/decrypted strings do not match" );

	}
};

AT_RegisterTest( RSA2, Ciphers );

AT_DefineTestLevel(
    RSA3,
    Ciphers,
    "Extract public key from private",
    at::UnitTestTraits::TestSuccess,
    0
)
{
	void Run()
	{
        
        std::string l_orig_str = "HELLO BABY3";
        
        at::Ptr< at::Cipher * > l_pub_cipher = at::NewCipher( "RSA_PUB" );

        at::Ptr< at::Cipher * > l_prv_cipher = at::NewCipher( "RSA_PRV" );

        // read the private key
        l_prv_cipher->SetKey( s_prv_key );

        // extract the public key
        at::CipherProperty l_prop = l_prv_cipher->GetProperty( at::Cipher::PublicKey );

        AT_TCAssert( at::s_no_property != l_prop, "Propery get failed\n" );

        std::string l_pubkey = l_prop.Refer();

        printit( l_pubkey );
        
        std::string l_enc_str4 = l_prv_cipher->Encrypt( s_prv_key, l_orig_str );

        std::string l_dec_str4 = l_pub_cipher->Decrypt( l_pubkey, l_enc_str4 );
    
        AT_TCAssert( l_dec_str4 == l_orig_str, "Encrypted/decrypted strings do not match" );
	}
};

AT_RegisterTest( RSA3, Ciphers );


AT_DefineTestLevel(
    AES,
    Ciphers,
    "A symmetrc cipher test",
    at::UnitTestTraits::TestSuccess,
    1
)
{
	void Run()
	{
        at::Ptr< at::CipherSymmetricKey * > l_key_genr = at::NewKeyGenSymmetric( "aes_128_cbc" );

        AT_TCAssert( l_key_genr, "Failed to create key generator" );

        std::string l_key = l_key_genr->GenerateKey();

        std::string l_iv = l_key_genr->GenerateInitVector();

        at::Ptr<at::Cipher *> l_cipher = l_key_genr->NewCipher();
        
        std::string l_orig = "HI THERE";

        std::string l_enc = l_cipher->Encrypt( l_key, l_iv, l_orig );
        
        std::string l_dec = l_cipher->Decrypt( l_key, l_iv, l_enc );

        printit( l_orig );
        printit( l_dec );
        
        AT_TCAssert( l_orig == l_dec, "Encrypted/decrypted strings do not match" );
	}
};

AT_RegisterTest( AES, Ciphers );


} // anon namespace
