/**
 * tst_unit_test.cpp
 *
 * tst_unit_test.cpp tests unit tests !
 *
 */


#include "at_unit_test.h"
#include "at_assert.h"

using namespace at;

AT_TestArea( UnitTest, "Test area for unit test system" );

class TEST_Fixture
{
	public:

	TEST_Fixture()
	{
	}

};


AT_DefineTest( BasicSucceedTest, UnitTest, "This unit test should succeed" ) , public TEST_Fixture
{
	void Run()
	{
		// any test that returns is successful
		return;
	}

};

AT_RegisterTest( BasicSucceedTest, UnitTest );

AT_DefineTestLevel(
    BasicDefaultUnselected,
    UnitTest,
    "This unit test should not be selected by default",
    UnitTestTraits::TestSuccess,
    5
), public TEST_Fixture
{
	void Run()
	{
		// any test that returns is successful
		return;
	}

};

AT_RegisterTest( BasicDefaultUnselected, UnitTest );

AT_DefineTestLevel(
    BasicDefaultUnselectedFailure,
    UnitTest,
    "This unit test should not be selected by default but should fail when selected",
    UnitTestTraits::TestSuccess,
    10
), public TEST_Fixture
{
	void Run()
	{
		// any test that returns is successful
        AT_TCAssert( false, "Should fail here" )
		return;
	}

};

AT_RegisterTest( BasicDefaultUnselectedFailure, UnitTest );



AT_DefineTestExpect( BasicFailTest, UnitTest, "This unit test should fail while running", UnitTestTraits::TestRunFailure ) , public TEST_Fixture
{
	void Run()
	{
		// any test that returns is successful

		AT_TCAssert( false, "Should fail here" )
		return;
	}

};

AT_RegisterTest( BasicFailTest, UnitTest );



class TEST_ConstructorFailFixture
{
	public:

	TEST_ConstructorFailFixture()
	{
		AT_TCAssert( false, "Constructor should fail here" )
	}

};

AT_DefineTestExpect( ConstructorFailTest, UnitTest, "This unit test should fail in the constructor", UnitTestTraits::TestConstructorFailure ),
	public TEST_ConstructorFailFixture
{
	void Run()
	{
		// This test should fail in the constructor so
		// we won't get here
		return;
	}

};

AT_RegisterTest( ConstructorFailTest, UnitTest );


class TEST_DestructorFailFixture
{
	public:

	~TEST_DestructorFailFixture()
	{
		AT_TCAssert( false, "Destructor should fail here !and cause a memory leak!" )
	}

};

AT_DefineTestExpect( DestructorFailTest, UnitTest, "This unit test should fail in the destructor", UnitTestTraits::TestDestructorFailure ),
	public TEST_DestructorFailFixture
{
	void Run()
	{
		return;
	}

};

AT_RegisterTest( DestructorFailTest, UnitTest );



AT_TestArea( UnitTest_B, "B Test area for unit test system" );


AT_DefineTest( ABTest, UnitTest_B, "A second success test" )
{
	void Run()
	{
		return;
	}

};

AT_DefineTestLevel(
    AT_AbortFailTest,
    UnitTest,
    "This unit test should fail while running - testing AT_Abort",
    UnitTestTraits::TestRunFailure,
    5
) , public TEST_Fixture
{
	void Run()
	{
		// any test that returns is successful

		AT_Abort();
		return;
	}

};

AT_RegisterTest( AT_AbortFailTest, UnitTest );


AT_RegisterTest( ABTest, UnitTest_B );
