
#include "at_unit_test.h"
#include "at_ffs.h"

using namespace at;

#include <iostream>

namespace {

AT_TestArea( FindFirstSetArea, "Find first set tests" );

template <typename w_T>
void FFSTest()
{
    w_T l_v = 0;

    AT_TCAssert( FindFirstSet( l_v ) == 0, "Failed the zero test" );

    l_v = 1;
    AT_TCAssert( FindFirstSet( l_v ) == 1, "Failed the one test" );
    
    for ( int i = 2; l_v += l_v; ++ i )
    {
        AT_TCAssert( FindFirstSet( l_v ) == i, "Failed the N test" );
    }
}

AT_DefineTest( Basic, FindFirstSetArea, "Basic test" )
{
	void Run()
	{

        std::cout << "\n#-----" << IsInteger<float>::m_value << "-----#\n";
        std::cout << "#-----" << IsInteger<char>::m_value << "-----#\n";

        FFSTest<char>();
        FFSTest<signed char>();
        FFSTest<unsigned char>();
        FFSTest<short>();
        FFSTest<unsigned short>();
        FFSTest<int>();
        FFSTest<unsigned int>();
        FFSTest<long>();
        FFSTest<unsigned long>();
        FFSTest<long long>();
        FFSTest<unsigned long long>();

	}
};

AT_RegisterTest( Basic, FindFirstSetArea );

}
