/*
 * This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 * Any unauthorized use of this file is prohibited and will be prosecuted
 * to the full extent of the law.
 */

#include "at_unit_test.h"
#include "at_net.h"
#include "at_net_http.h"
#include "at_net_ipv4.h"
#include "at_net_helpers.h"

#include <time.h>
#include <list>

using namespace at;
using namespace std;

#define MAX_NET_ITER 1000

const unsigned int maxTestSeconds = 10;
const TimeInterval maxTestWait (maxTestSeconds*TimeInterval::PerSec);

namespace
{
    string to_string(Ptr<Buffer*> p)
    {
        return string(">>>") + string(p->begin(), p->end()) + string("<<<");
    }

    struct ConnectionEvent
    {
        enum Type {
            Invalid, ReceiveFailure, SendFailure, SendCompleted,
            DataReady, Closed, ReceivedProperties
        };
        int seqno;
        Type type;
        bool hasProperties;
        bool hasAllProperties;
        const NetError *error;
        Ptr<const Buffer*> const_buffer;
        const char *begin;
        const char *end;
        NetConnection::t_PropList props;
        ConnectionEvent() : type(Invalid),
                            hasProperties(false),
                            hasAllProperties(false),
                            error(0),
                            begin(0),
                            end(0)
        { }

        static const char* TypeToString(Type t)
        {
            switch (t)
            {
            case ReceiveFailure:
                return "ReceiveFailure";
            case SendFailure:
                return "SendFailure";
            case SendCompleted:
                return "SendCompleted";
            case DataReady:
                return "DataReady";
            case Closed:
                return "Closed";
            case ReceivedProperties:
                return "ReceivedProperties";
            case Invalid:
            default:
                return "Invalid";
            }
        }
    };

    std::ostream& operator<<(std::ostream &os, const ConnectionEvent &ev)
    {
        os << "ConnEv(seq=" << ev.seqno
           << ", type=" << ConnectionEvent::TypeToString(ev.type);
        if (ev.type == ConnectionEvent::ReceivedProperties)
            os << " prop count=" << ev.props.size();
        os << ")";
        return os;
    }


    class ConnectionRecorder :
        public LeadTwinMT_Basic<NetConnectionResponderIf>,
        public PtrTarget_MT
    {
    private:
        int m_seq;
        Ptr<NetConnection*> m_conn;
        std::list<ConnectionEvent> events;
        Ptr<MutexRefCount *>        m_mutex;
        Conditional *               m_newEvent;

    public:
    // accessors
        Ptr<MutexRefCount *> StateMutex(void) { return m_mutex;}
        Conditional & GetConditional(void) { return *m_newEvent;}       // REV: need a releasor, or refcounted conditional
        std::list<ConnectionEvent>  &EventList(void)  { return events;}
        PtrView<NetConnection *> Connection(void) { return m_conn;}


        ConnectionRecorder(PtrDelegate<NetConnection*> conn) :
            m_seq(0),
            m_conn(conn),
            events(),
            m_mutex(new MutexRefCount(Mutex::Recursive)),
            m_newEvent(new Conditional(*m_mutex))
        {
            m_conn->ConnectionNotify(*this);
        }

        ~ConnectionRecorder()
        {
            LeadCancel();
            {
                Lock<Ptr<MutexRefCount *> > l(m_mutex);
                delete m_newEvent;
                m_newEvent = NULL;
            }
            m_mutex = NULL;
        }

        void ReceiveFailure(const NetConnectionError &i_error)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::ReceiveFailure;
            call.error = &i_error;

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        void SendCompleted(PtrDelegate<const Buffer*> i_buffer,
                           const char *i_end)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::SendCompleted;
            call.const_buffer = i_buffer;
            call.end = i_end;

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        void SendFailure(PtrDelegate<const Buffer*> i_buffer,
                         const char *i_begin,
                         const NetConnectionError &i_error)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::SendCompleted;
            call.const_buffer = i_buffer;
            call.begin = i_begin;
            call.error = &i_error;

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        void DataReady()
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::DataReady;
#if 0
            bool b;
            call.buffer = NewBuffer();
            do
            {
                call.buffer->reserve(call.buffer->capacity() * 2 + 1);
                b = m_conn->Receive(call.buffer);
            }
            while (b);
#endif
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        void OutOfBandDataReady()
        {
            AT_Assert(false);
        }

        void ReceivedPropertiesNotification(
            bool i_complete, bool i_props_in_queue)
        {
            ConnectionEvent call = ConnectionEvent();
            call.type = ConnectionEvent::ReceivedProperties;
            call.hasProperties = i_props_in_queue;
            call.hasAllProperties = i_complete;
            call.props = m_conn->GetReceivedProperties();

            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            call.seqno = m_seq++;
            events.push_back(call);
            m_newEvent->Post();
        }

        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_mutex)
        {
            // catch a copy of the mutex, which is the entire purpose of this override
            Lock<Ptr<MutexRefCount *> > l(m_mutex);
            delete m_newEvent;
            m_newEvent = new Conditional(*i_mutex);
            m_mutex = i_mutex;
            return LeadTwinMT_Basic<NetConnectionResponderIf>::LeadAssociate(i_aide, i_mutex);
        }
    };

    AT_TestArea(HTTP, "HTTP connectivity");

    AT_DefineTestLevel(HTTPServerBasic, HTTP, "Basic HTTP server test",UnitTestTraits::TestSuccess,0)
    {
        void Run()
        {
            for (int x = 0; x < 10; ++x) {
                Ptr<ConnectionRecorder *> svrrec = 0;
                Ptr<ConnectionRecorder *> clirec = 0;
                ConnectionEvent ev;
                Ptr<Net*> ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
                if (!ip) {
                    std::cout << "No Net Object returned from factory" << std::endl;
                    AT_Assert(!"No Net Object returned from factory");
                    return;
                }
                Ptr<Net*> http = FactoryRegister<at::Net, DKy>::Get().Create("net_http")();
                if (!http) {
                    std::cout << "No HTTP Object returned from factory" << std::endl;
                    AT_Assert(!"No HTTP Object returned from factory");
                    return;
                }
                Ptr<IPv4Address*> addr;
                addr = new IPv4Address(127,0,0,1,4000+x);
                Ptr<NetConnection*> svrconn;
                Ptr<NetConnection*> cliconn;
                XConnectLead<ConnectionRecorder> svrConnectLead;
                XConnectLead<ConnectionRecorder> cliConnectLead;

                http->Listen(&svrConnectLead, addr, 0);
                ip->Connect(&cliConnectLead, addr, 0);

                cliConnectLead.Wait(&clirec, 0, maxTestWait);
                if (clirec) {
                    // only wait for server if client succeeded (otherwise, server will listen forever)
                    svrConnectLead.Wait(&svrrec, 0, maxTestWait);
                } else {
                    // among other reasons, the client can fail in this test suite due to the server being
                    // overwhelmed, where previous client sessions being closed asynchronously have not yet
                    // been closed, and the servers resources have been depleted temporarily until the
                    // pending closes are flushed.
                    std::cerr << " HTTPServerBasic -- client connect failed: port [" << 4000 + x << "]" << std::endl;
                }

                if (svrrec && clirec) {
                    svrconn = svrrec->Connection();
                    cliconn = clirec->Connection();
                }
                if (cliconn && svrconn) {
                    std::string request = 
                            "POST /word_to_your_mother HTTP/1.1235\r\n"
                            "This: is a header\r\n"
                            "Content-length: 52\r\n"
                            "\r\n"
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                    size_t len = request.size();
                    size_t sent = 0;
                    for (int i = 0; i < x+2; ++i) {
                        Ptr<Buffer*> outBuffer = NewBuffer();
                        const char *d = request.data();
                        int off = std::min(size_t(len/(x+1)), request.size());
                        if (off == 0) {
                            break;
                        }
                        outBuffer->append(d, d+off);
                        sent += off;
                        request.erase(0, off);
                        cliconn->Send(outBuffer);
                    }
                    AT_Assert(sent == len);
                    // wait for properties to be received (terminated by body elements)
                    time_t timeStart = ::time(NULL);
                    time_t timeElapsed = 0;
                    bool done = false;
                    do {
                        Lock<Ptr<MutexRefCount *> > l(svrrec->StateMutex());
                        while (svrrec->EventList().size() == 0) {
                            if (!svrrec->GetConditional().Wait(maxTestWait)) {
                                AT_Assert(!"svrrec->GetConditional().Wait(maxTestWait)");
                                break;
                            }
                        }
                        while (svrrec->EventList().size())
                        {
                            ev = svrrec->EventList().front();
                            svrrec->EventList().pop_front();
                            if (ev.type != ConnectionEvent::ReceivedProperties) {
                                done = true;
                                break;
                            }
                        }
                        timeElapsed = ::time(NULL) - timeStart;
                    } while (!done && (timeElapsed < maxTestSeconds));
                    NetConnection::t_PropList props = svrconn->GetReceivedProperties();

                    NetConnection::t_PropList::iterator it = props.begin();
                    NetConnection::t_PropList::iterator end = props.end();
                    int propCount = 0;
                    while (it != end) {
                        NetProperty *p = it->Get();
                        HTTPVersionProperty *v;
                        HTTPURIProperty *u;
                        HTTPHeaderProperty *h;
                        HTTPMethodProperty *m;
                        if ((v = dynamic_cast<HTTPVersionProperty*>(p))) {
                            AT_Assert(v->major == 1);
                            AT_Assert(v->minor == 1235);
                        } else if ((u = dynamic_cast<HTTPURIProperty*>(p))) {
                            AT_Assert(u->uri == "/word_to_your_mother");
                        } else if ((h = dynamic_cast<HTTPHeaderProperty*>(p))) {
                            if (h->name == "Content-length") {
                                AT_Assert(h->value == "52");
                            } else if (h->name == "This") {
                                AT_Assert(h->value == "is a header");
                            } else {
                                AT_Assert(!"Unexpected header property");
                            }
                        } else if (m = dynamic_cast<HTTPMethodProperty*>(p)) {
                            AT_Assert(m->method == "POST");
                        } else {
                            AT_Assert(!"Unexpected method property");
                        }
                        ++propCount;
                        ++it;
                    }
                    AT_Assert(propCount == 5);

                    Ptr<Buffer*> abc = NewBuffer();
                    abc->reserve(52);

                    timeStart = ::time(NULL);
                    timeElapsed = 0;

                    while (timeElapsed < maxTestSeconds) {
                        Lock<Ptr<MutexRefCount *> > l(svrrec->StateMutex());
                        while (svrconn->Receive(abc));
                        if (abc->size() >= 52) {
                            break;
                        }
                        while (svrrec->EventList().size() == 0) {
                            if (!svrrec->GetConditional().Wait(maxTestWait)) {
                                AT_Assert(!"svrrec->GetConditional().Wait(maxTestWait)");
                                break;
                            }
                        }
                        while (svrrec->EventList().size()) {
                            ev = svrrec->EventList().front();
                            svrrec->EventList().pop_front();
                            AT_Assert(ev.type == ConnectionEvent::DataReady);
                        }
                        timeElapsed = ::time(NULL) - timeStart;
                    }
                    AT_Assert(abc->size() == 52);
                    AT_Assert(std::equal(abc->begin(), abc->end(),
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "abcdefghijklmnopqrstuvwxyz"));
                } // if (cliconn && svrconn)

                svrconn = NULL;
                cliconn = NULL;

                svrrec = NULL;
                clirec = NULL;

                http = NULL;
                ip = NULL;
            }
        }
    };
    AT_RegisterTest(HTTPServerBasic, HTTP);

    AT_DefineTestLevel(HTTPClientBasic, HTTP, "Basic HTTP client test",UnitTestTraits::TestSuccess,1)
    {
        void Run()
        {
            for (int x = 0; x < 10; ++x)
            {
                Ptr<ConnectionRecorder *> svrrec = 0;
                Ptr<ConnectionRecorder *> clirec = 0;
                ConnectionEvent ev;
                Ptr<Net*> http = FactoryRegister<at::Net, DKy>::Get().Create("net_http")();
                if (!http) {
                    std::cout << "No HTTP Object returned from factory" << std::endl;
                    AT_Assert(http);
                    return;
                }
                Ptr<IPv4Address*> addr;
                addr = new IPv4Address(127,0,0,1,4000+x);
                Ptr<NetConnection*> svrconn;
                Ptr<NetConnection*> cliconn;
                XConnectLead<ConnectionRecorder> svrConnectLead;
                XConnectLead<ConnectionRecorder> cliConnectLead;
                NetConnection::t_PropList props;
                Ptr<Buffer*> buf;
                // Set up the connection.
                http->Listen(&svrConnectLead, addr, 0);
                http->Connect(&cliConnectLead, addr, 0);
                cliConnectLead.Wait(&clirec, 0, maxTestWait);
                if (clirec) {
                    // only wait for server if client succeeded (otherwise, server will listen forever)
                    svrConnectLead.Wait(&svrrec, 0, maxTestWait);
                } else {
                    // among other reasons, the client can fail in this test suite due to the server being
                    // overwhelmed, where previous client sessions being closed asynchronously have not yet
                    // been closed, and the servers resources have been depleted temporarily until the
                    // pending closes are flushed.
                    std::cerr << " HTTPClientBasic -- client connect failed: port [" << 4000 + x << "]" << std::endl;
                }

                if (svrrec && clirec) {
                    svrconn = svrrec->Connection();
                    cliconn = clirec->Connection();
                }
                if (cliconn && svrconn) {
                    // Send the request.
                    props.push_back(new HTTPURIProperty("/path"));
                    props.push_back(new HTTPMethodProperty("GET"));
                    props.push_back(new HTTPVersionProperty(1, 0));
                    props.push_back(new HTTPHeaderProperty("X-Name1", "value1"));
                    props.push_back(new HTTPHeaderProperty("X-Name1", "value2"));
                    props.push_back(new HTTPHeaderProperty("Content-Length", "10"));
                    cliconn->AddOutgoingProperties(props);

                    buf = NewBuffer();
                    buf->Append(std::string("01234"));
                    cliconn->Send(buf);
                    buf = NewBuffer();
                    buf->Append(std::string("56789"));
                    cliconn->Send(buf);

                    // Observe the request on the server side.
                    buf = NewBuffer();
                    buf->reserve(10);
                    props = NetConnection::t_PropList();
                    time_t timeStart = ::time(NULL);
                    time_t timeElapsed = 0;
                    while ((timeElapsed < maxTestSeconds) && ((buf->size() < 10))) {
                        Lock<Ptr<MutexRefCount *> > l(svrrec->StateMutex());
                        while (!svrrec->EventList().size()) {
                            if (!svrrec->GetConditional().Wait(maxTestWait)) {
                                AT_Assert(!"timeout in HTTPClientBasic waiting for message at server");
                                break;
                            }
                        }
                        while (svrrec->EventList().size())
                        {
                            ev = svrrec->EventList().front();
                            svrrec->EventList().pop_front();
                            if (ev.type == ConnectionEvent::DataReady) {
                                while (svrconn->Receive(buf));
                            } else if (ev.type == ConnectionEvent::ReceivedProperties) {
                                props.insert(props.end(), ev.props.begin(), ev.props.end());
                            }
                        }
                        timeElapsed = ::time(NULL) - timeStart;
                    }
                    AT_Assert(props.size() >= 3);
                    AT_Assert(buf->size() == 10);
                    AT_Assert(!memcmp((void*)"0123456789",(void*)buf->begin(), 10));
                    // TODO: Check properties.

                    // Write a response.
                    props = NetConnection::t_PropList();
                    props.push_back(new HTTPHeaderProperty("Content-Length", "26"));
                    svrconn->AddOutgoingProperties(props);
                    buf = NewBuffer();
                    buf->Append(std::string("abcdefghijklmnopqrstuvwxyz"));
                    svrconn->Send(buf);

                    // Observe the response on the client side.
                    buf = NewBuffer();
                    buf->reserve(30);
                    props = NetConnection::t_PropList();
                    timeStart = ::time(NULL);
                    timeElapsed = 0;
                    while ((timeElapsed < maxTestSeconds) && ((buf->size() < 26))) {
                        Lock<Ptr<MutexRefCount *> > l(clirec->StateMutex());
                        while (!clirec->EventList().size()) {
                            if (!clirec->GetConditional().Wait(maxTestWait)) {
                                AT_Assert(!"timeout in HTTPClientBasic waiting for response at client");
                                break;
                            }
                        }
                        while (clirec->EventList().size())
                        {
                            ev = clirec->EventList().front();
                            clirec->EventList().pop_front();
                            if (ev.type == ConnectionEvent::DataReady) {
                                while (cliconn->Receive(buf));
                            } else if (ev.type == ConnectionEvent::ReceivedProperties) {
                                props.insert(props.end(), ev.props.begin(), ev.props.end());
                            }
                        }
                        timeElapsed = ::time(NULL) - timeStart;
                    }
                    AT_Assert(props.size() >= 2);
                    AT_Assert(buf->size() == 26);
                    AT_Assert(!memcmp((void*)"abcdefghijklmnopqrstuvwxyz",(void*)buf->begin(), 26));
                } else {
                    std::cout << " connection failed\n" << std::endl;
                }
                // TODO: Check properties.

                svrconn = NULL;
                cliconn = NULL;

                http = NULL;

                svrrec = NULL;
                clirec = NULL;
            }
        }
    };
    AT_RegisterTest(HTTPClientBasic, HTTP);

} // namespace
