
#include "at_stack_trace.h"

#include "at_unit_test.h"

using namespace at;

namespace {
    
AT_TestArea( StackTrace, "Test the stack trace" );

#include <iostream>

AT_DefineTest( Basic, StackTrace, "Basic stack trace test" )
{
	void Run()
	{

		for ( int i = 0; i < 3; ++ i )
		{
			at::Ptr< StackTrace * >     l_st = NewStackTrace();

			l_st->m_line_prefix = "    =  ";

			std::cout << "\n" << * l_st;
		}
        
	}
};

AT_RegisterTest( Basic, StackTrace );


};
