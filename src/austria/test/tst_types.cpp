#include "at_types.h"
#include "at_unit_test.h"

using namespace at;


AT_TestArea( Types, "Misc types tests" );

AT_DefineTest( Types, Types, "Misc types tests" )
{

    void Run()
    {
        std::pair< int, int > l_result;

        l_result = FloorDivide< int >( 21, 7 );
        AT_TCAssert( l_result.first == 3, "l_result.first should equal 3\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

        l_result = FloorDivide< int >( 27, 7 );
        AT_TCAssert( l_result.first == 3, "l_result.first should equal 3\n" );
        AT_TCAssert( l_result.second == 6, "l_result.second should equal 6\n" );

        l_result = FloorDivide< int >( 28, 7 );
        AT_TCAssert( l_result.first == 4, "l_result.first should equal 4\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

        l_result = FloorDivide< int >( 42, 1 );
        AT_TCAssert( l_result.first == 42, "l_result.first should equal 42\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

        l_result = FloorDivide< int >( 43, 1 );
        AT_TCAssert( l_result.first == 43, "l_result.first should equal 43\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

        l_result = FloorDivide< int >( 42, 2 );
        AT_TCAssert( l_result.first == 21, "l_result.first should equal 21\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

        l_result = FloorDivide< int >( 43, 2 );
        AT_TCAssert( l_result.first == 21, "l_result.first should equal 21\n" );
        AT_TCAssert( l_result.second == 1, "l_result.second should equal 1\n" );

        l_result = FloorDivide< int >( 0, 55 );
        AT_TCAssert( l_result.first == 0, "l_result.first should equal 0\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

        l_result = FloorDivide< int >( -21, 7 );
        AT_TCAssert( l_result.first == -3, "l_result.first should equal -3\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

        l_result = FloorDivide< int >( -22, 7 );
        AT_TCAssert( l_result.first == -4, "l_result.first should equal -4\n" );
        AT_TCAssert( l_result.second == 6, "l_result.second should equal 6\n" );

        l_result = FloorDivide< int >( -28, 7 );
        AT_TCAssert( l_result.first == -4, "l_result.first should equal -4\n" );
        AT_TCAssert( l_result.second == 0, "l_result.second should equal 0\n" );

    }

};

AT_RegisterTest( Types, Types );


AT_TestArea( Time, "TimeStamp/Interval tests" );


AT_DefineTest( Time, Time, "Basic TimeStamp/Interval test" )
{

    void Run()
    {

        const TimeInterval l_ti1( 55 );
        const TimeInterval l_ti2( 55 );
        const TimeInterval l_ti3( 101 );

        AT_TCAssert( l_ti1 == l_ti2, "l_ti1 == l_ti2 should evaluate true\n" );
        AT_TCAssert( ! ( l_ti2 == l_ti3 ), "l_ti2 == l_ti3 should evaluate false\n" );

        AT_TCAssert( ! ( l_ti1 != l_ti2 ), "l_ti1 != l_ti2 should evaluate false\n" );
        AT_TCAssert( l_ti2 != l_ti3, "l_ti2 != l_ti3 should evaluate true\n" );

        AT_TCAssert( ! ( l_ti1 < l_ti2 ), "l_ti1 < l_ti2 should evaluate false\n" );
        AT_TCAssert( l_ti2 < l_ti3, "l_ti2 < l_ti3 should evaluate true\n" );
        AT_TCAssert( ! ( l_ti3 < l_ti2 ), "l_ti3 < l_ti2 should evaluate false\n" );

        AT_TCAssert( ! ( l_ti1 > l_ti2 ), "l_ti1 > l_ti2 should evaluate false\n" );
        AT_TCAssert( ! ( l_ti2 > l_ti3 ), "l_ti2 > l_ti3 should evaluate false\n" );
        AT_TCAssert( l_ti3 > l_ti2, "l_ti3 > l_ti2 should evaluate true\n" );

        AT_TCAssert( l_ti1 <= l_ti2, "l_ti1 <= l_ti2 should evaluate true\n" );
        AT_TCAssert( l_ti2 <= l_ti3, "l_ti2 <= l_ti3 should evaluate true\n" );
        AT_TCAssert( ! ( l_ti3 <= l_ti2 ), "l_ti3 <= l_ti2 should evaluate false\n" );

        AT_TCAssert( l_ti1 >= l_ti2, "l_ti1 >= l_ti2 should evaluate true\n" );
        AT_TCAssert( ! ( l_ti2 >= l_ti3 ), "l_ti2 >= l_ti3 should evaluate false\n" );
        AT_TCAssert( l_ti3 >= l_ti2, "l_ti3 >= l_ti2 should evaluate true\n" );

        TimeInterval l_ti4;

        AT_TCAssert( l_ti4 == TimeInterval( 0 ), "l_ti4 should equal TimeInterval( 0 )\n" );

        l_ti4 = TimeInterval( 55 );
        l_ti4 += l_ti3;
        AT_TCAssert( l_ti4 == TimeInterval( 156 ), "l_ti4 should equal TimeInterval( 156 )\n" );

        l_ti4 = TimeInterval( 101 );
        l_ti4 -= l_ti1;
        AT_TCAssert( l_ti4 == TimeInterval( 46 ), "l_ti4 should equal TimeInterval( 46 )\n" );

        AT_TCAssert( l_ti1 + l_ti3 == TimeInterval( 156 ), "l_ti1 + l_ti3 should equal TimeInterval( 156 )\n" );

        AT_TCAssert( l_ti3 - l_ti1 == TimeInterval( 46 ), "l_ti3 - l_ti1 should equal TimeInterval( 46 )\n" );

        AT_TCAssert( - l_ti1 == TimeInterval( -55 ), "- l_ti1 should equal TimeInterval( -55 )\n" );

        l_ti4 = TimeInterval( 55 );
        l_ti4 *= 11;
        AT_TCAssert( l_ti4 == TimeInterval( 605 ), "l_ti4 should equal TimeInterval( 605 )\n" );

        l_ti4 = TimeInterval( 55 );
        l_ti4 /= 5;
        AT_TCAssert( l_ti4 == TimeInterval( 11 ), "l_ti4 should equal TimeInterval( 11 )\n" );

        AT_TCAssert( l_ti1 * 7 == TimeInterval( 385 ), "l_ti1 * 7 should equal TimeInterval( 385 )\n" );

        AT_TCAssert( l_ti1 / 11 == TimeInterval( 5 ), "l_ti1 / 11 should equal TimeInterval( 5 )\n" );

        const TimeInterval l_ti5( 11 );

        AT_TCAssert( l_ti1 / l_ti5 == 5, "l_ti1 / l_ti5 should equal 5\n" );

        const TimeInterval l_ti6( 42 );

        l_ti4 = TimeInterval( 101 );
        l_ti4 %= l_ti6;
        AT_TCAssert( l_ti4 == TimeInterval( 17 ), "l_ti4 should equal TimeInterval( 17 )\n" );

        AT_TCAssert( l_ti3 % l_ti6 == TimeInterval( 17 ), "l_ti3 % l_ti6 should equal TimeInterval( 17 )\n" );

        const TimeInterval l_ti7( 550000000 );

        AT_TCAssert( l_ti7.Secs() == 55, "l_ti7.Secs() should equal 55\n" );
        AT_TCAssert( l_ti7.MilliSecs() == 55000, "l_ti7.MilliSecs() should equal 55000\n" );
        AT_TCAssert( l_ti7.MicroSecs() == 55000000, "l_ti7.MicroSecs() should equal 55000000\n" );
        AT_TCAssert( l_ti7.NanoSecs() == ( 55 * Int64( 1000000000 ) ), "l_ti7.NanoSecs() should equal 55000000000\n" );

        const TimeInterval l_ti8( -550000001 );

        AT_TCAssert( l_ti8.Secs() == -56, "l_ti8.Secs() should equal -56\n" );
        AT_TCAssert( l_ti8.MilliSecs() == -55001, "l_ti8.MilliSecs() should equal -55001\n" );
        AT_TCAssert( l_ti8.MicroSecs() == -55000001, "l_ti8.MicroSecs() should equal -55000001\n" );

        AT_TCAssert( TimeInterval::Secs( 55 ) == TimeInterval( 550000000 ), "TimeInterval::Secs( 55 ) should equal TimeInterval( 550000000 )\n" );
        AT_TCAssert( TimeInterval::MilliSecs( 55 ) == TimeInterval( 550000 ), "TimeInterval::MilliSecs( 55 ) should equal TimeInterval( 550000 )\n" );
        AT_TCAssert( TimeInterval::MicroSecs( 55 ) == TimeInterval( 550 ), "TimeInterval::MicroSecs( 55 ) should equal TimeInterval( 550 )\n" );
        AT_TCAssert( TimeInterval::NanoSecs( 5500 ) == TimeInterval( 55 ), "TimeInterval::NanoSecs( 5500 ) should equal TimeInterval( 55 )\n" );

        AT_TCAssert( TimeInterval::NanoSecs( -5501 ) == TimeInterval( -56 ), "TimeInterval::NanoSecs( -5501 ) should equal TimeInterval( -56 )\n" );

        AT_TCAssert( 5 * l_ti1 == TimeInterval( 275 ), "5 * l_ti1 should equal TimeInterval( 275 )\n" );

        const TimeStamp l_ts1( 55 );
        const TimeStamp l_ts2( 55 );
        const TimeStamp l_ts3( 101 );

        AT_TCAssert( l_ts1 == l_ts2, "l_ts1 == l_ts2 should evaluate true\n" );
        AT_TCAssert( ! ( l_ts2 == l_ts3 ), "l_ts2 == l_ts3 should evaluate false\n" );

        AT_TCAssert( ! ( l_ts1 != l_ts2 ), "l_ts1 != l_ts2 should evaluate false\n" );
        AT_TCAssert( l_ts2 != l_ts3, "l_ts2 != l_ts3 should evaluate true\n" );

        AT_TCAssert( ! ( l_ts1 < l_ts2 ), "l_ts1 < l_ts2 should evaluate false\n" );
        AT_TCAssert( l_ts2 < l_ts3, "l_ts2 < l_ts3 should evaluate true\n" );
        AT_TCAssert( ! ( l_ts3 < l_ts2 ), "l_ts3 < l_ts2 should evaluate false\n" );

        AT_TCAssert( ! ( l_ts1 > l_ts2 ), "l_ts1 > l_ts2 should evaluate false\n" );
        AT_TCAssert( ! ( l_ts2 > l_ts3 ), "l_ts2 > l_ts3 should evaluate false\n" );
        AT_TCAssert( l_ts3 > l_ts2, "l_ts3 > l_ts2 should evaluate true\n" );

        AT_TCAssert( l_ts1 <= l_ts2, "l_ts1 <= l_ts2 should evaluate true\n" );
        AT_TCAssert( l_ts2 <= l_ts3, "l_ts2 <= l_ts3 should evaluate true\n" );
        AT_TCAssert( ! ( l_ts3 <= l_ts2 ), "l_ts3 <= l_ts2 should evaluate false\n" );

        AT_TCAssert( l_ts1 >= l_ts2, "l_ts1 >= l_ts2 should evaluate true\n" );
        AT_TCAssert( ! ( l_ts2 >= l_ts3 ), "l_ts2 >= l_ts3 should evaluate false\n" );
        AT_TCAssert( l_ts3 >= l_ts2, "l_ts3 >= l_ts2 should evaluate true\n" );

        TimeStamp l_ts4;

        AT_TCAssert( l_ts4 == TimeStamp( 0 ), "l_ts4 should equal TimeStamp( 0 )\n" );

        l_ts4 = TimeStamp( 55 );
        l_ts4 += l_ti3;
        AT_TCAssert( l_ts4 == TimeStamp( 156 ), "l_ts4 should equal TimeStamp( 156 )\n" );

        l_ts4 = TimeStamp( 101 );
        l_ts4 -= l_ti1;
        AT_TCAssert( l_ts4 == TimeStamp( 46 ), "l_ts4 should equal TimeStamp( 46 )\n" );

        AT_TCAssert( l_ts1 + l_ti3 == TimeStamp( 156 ), "l_ts1 + l_ti3 should equal TimeStamp( 156 )\n" );

        AT_TCAssert( l_ts3 - l_ti1 == TimeStamp( 46 ), "l_ts3 - l_ti1 should equal TimeStamp( 46 )\n" );

        AT_TCAssert( l_ts3 - l_ts1 == TimeInterval( 46 ), "l_ts3 - l_ts1 should equal TimeInterval( 46 )\n" );

        AT_TCAssert( l_ti1 + l_ts3 == TimeStamp( 156 ), "l_ti1 + l_ts3 should equal TimeStamp( 156 )\n" );

    }

};

AT_RegisterTest( Time, Time );
