#include "at_unit_test.h"
#include "at_range_set.h"
#include "at_assert.h"

#include <string>
#include <vector>


using namespace at;


void BitMask(
    const std::string & i_s,
    std::vector< bool > & o_mask
)
{
    o_mask.resize( i_s.size(), 0 );
    for ( int i_i = 0; i_i != int( i_s.size() ); ++i_i )
    {
        AT_Assert( i_s[ i_i ] == '0' || i_s[ i_i ] == '1' );
        if ( i_s[ i_i ] == '1' )
        {
            o_mask[ i_i ] = true;
        }
    }
}


AT_TestArea( RangeSet, "RangeSet tests" );

AT_DefineTestLevel(
    RangeSet,
    RangeSet,
    "Basic RangeSet test",
    UnitTestTraits::TestSuccess,
    0
)
{

    void Run()
    {
        std::vector< bool > l_expected;
        std::vector< bool > l_actual;

        // Five non-contiguous, non-overlapping regions.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00011100110010000111110001111000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 17, 22 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 3, 6 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 25, 29 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 8, 10 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 12, 13 ) );

            l_rs.GranularMask( 32, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Two overlapping regions, second one starting inside first.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00000011111111100000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 6, 11 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 8, 15 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Two overlapping regions, first one starting inside second.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00001111111111110000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 9, 16 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 4, 13 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Two regions, second one completely inside first.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00000111111111000000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 5, 14 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 9, 12 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Two regions, first one completely inside second.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00011111111111100000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 8, 11 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 3, 15 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Two abutting regions.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00000000011111110000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 9, 12 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 12, 16 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Two regions with same beginning, longer one first.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00000001111111000000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 7, 14 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 7, 10 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Two regions with same beginning, shorter one first.
        {
            at::RangeSet< int > l_rs;

            BitMask( "00000011111111110000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 6, 16 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 6, 11 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Same region twice
        {
            at::RangeSet< int > l_rs;

            BitMask( "00001111111110000000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 4, 13 ) );
            l_rs.Add( at::HalfOpenInterval< int >( 4, 13 ) );

            l_rs.GranularMask( 20, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Region starting at position zero.
        {
            at::RangeSet< int > l_rs;

            BitMask( "1111110000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 0, 6 ) );

            l_rs.GranularMask( 10, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Region extending beyond end of bitmask.
        {
            at::RangeSet< int > l_rs;

            BitMask( "0000001111", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 6, 15 ) );

            l_rs.GranularMask( 10, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Region completely outside bitmask.
        {
            at::RangeSet< int > l_rs;

            BitMask( "0000000000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 13, 19 ) );

            l_rs.GranularMask( 10, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Zero-length region.
        {
            at::RangeSet< int > l_rs;

            BitMask( "0000000000", l_expected );

            l_rs.Add( at::HalfOpenInterval< int >( 3, 3 ) );

            l_rs.GranularMask( 10, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        // Using the closed interval interface version.
        {
            at::RangeSet< int > l_rs;

            BitMask( "0001111100", l_expected );

            l_rs.Add( at::ClosedInterval< int >( 3, 7 ) );

            l_rs.GranularMask( 10, 1, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

        {
            at::RangeSet< int > l_rs0;

            l_rs0.Add( at::HalfOpenInterval< int >( 9, 14 ) );
            l_rs0.Add( at::HalfOpenInterval< int >( 18, 24 ) );
            l_rs0.Add( at::HalfOpenInterval< int >( 37, 41 ) );
            l_rs0.Add( at::HalfOpenInterval< int >( 45, 47 ) );
            l_rs0.Add( at::HalfOpenInterval< int >( 53, 60 ) );
            l_rs0.Add( at::HalfOpenInterval< int >( 65, 69 ) );

            // Adding non-overlapping element before all existing elements.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00111000011111000011111100000000000001111000011000000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 2, 5 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding non-overlapping element after all existing elements.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100000000000001111000011000000111111100000111100011110000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 72, 76 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding non-overlapping element in the middle.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100011111110001111000011000000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 27, 34 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting in the middle of a block.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111111111100000001111000011000000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 22, 30 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting in the middle of a block, overlapping one more.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111111111111111111111000011000000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 22, 40 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting in the middle of a block, enclosing one more.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111111111111111111111110011000000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 22, 43 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting in the middle of a block, enclosing two and overlapping one.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111111111111111111111111111111111111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 22, 56 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting at the end of a block.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111111111111000001111000011000000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 24, 32 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element fitting exactly in-between two blocks.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100000000000001111000011111111111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 47, 53 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting at the end of a block, and enclosing the next two.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111111111111111111111111111111000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 24, 50 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting at the beginning of a block, and ending within it.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100000000000001111000011000000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 53, 57 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting at the beginning of a block, and ending outside it.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100000000000001111000011000000111111111110111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 53, 64 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting at the beginning of a block, enclosing one and overlapping one.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100000000000001111111111111111111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 37, 58 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element starting between blocks, enclosing two.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100000000001111111111111110000111111100000111100000000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 34, 49 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }
 
            // Adding element starting between blocks, enclosing last four.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00000000011111000011111100000000001111111111111111111111111111111111111110000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 27, 73 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

            // Adding element enclosing all blocks.
            {
                at::RangeSet< int > l_rs( l_rs0 );

                BitMask( "00001111111111111111111111111111111111111111111111111111111111111111111100000000", l_expected );

                l_rs.Add( at::HalfOpenInterval< int >( 4, 72 ) );

                l_rs.GranularMask( 80, 1, l_actual );

                AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
            }

        }

        // Testing granular bitmask functionality.
        {
            at::RangeSet< int > l_rs;

            BitMask( "0010010011001111101001110011100010", l_expected );

            l_rs.Add( at::ClosedInterval< int >( 10, 15 ) );
            l_rs.Add( at::ClosedInterval< int >( 28, 30 ) );
            l_rs.Add( at::ClosedInterval< int >( 44, 46 ) );
            l_rs.Add( at::ClosedInterval< int >( 62, 77 ) );
            l_rs.Add( at::ClosedInterval< int >( 79, 83 ) );
            l_rs.Add( at::ClosedInterval< int >( 92, 93 ) );
            l_rs.Add( at::ClosedInterval< int >( 109, 116 ) );
            l_rs.Add( at::ClosedInterval< int >( 130, 131 ) );
            l_rs.Add( at::ClosedInterval< int >( 132, 133 ) );
            l_rs.Add( at::ClosedInterval< int >( 134, 135 ) );
            l_rs.Add( at::ClosedInterval< int >( 138, 139 ) );
            l_rs.Add( at::ClosedInterval< int >( 140, 141 ) );
            l_rs.Add( at::ClosedInterval< int >( 160, 161 ) );

            l_rs.GranularMask( 167, 5, l_actual );

            AT_TCAssert( l_actual == l_expected, "Actual bit pattern doesn't match expected." );
        }

    }

};

AT_RegisterTest( RangeSet, RangeSet );
