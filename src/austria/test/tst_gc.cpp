#include "at_unit_test.h"
#include "at_gc.h"
#include "at_assert.h"

#include <map>
#include <set>
#include <string>
#include <iterator>

using namespace at;

class AdjacencyList
{

public:

    typedef  std::map< char, std::set< char > >  t_map_type;

    t_map_type m_map;

    void AddVertices( const std::string & i_vertices )
    {
        for ( std::string::const_iterator l_iter = i_vertices.begin();
              l_iter != i_vertices.end();
              ++l_iter
            )
        {
            m_map[ *l_iter ];
        }
    }

    void AddDependants(
        char i_vertex,
        const std::string & i_dependants
    )
    {
        for ( std::string::const_iterator l_iter = i_dependants.begin();
              l_iter != i_dependants.end();
              ++l_iter
            )
        {
            AT_Assert( m_map.find( i_vertex ) != m_map.end() );
            m_map[ i_vertex ].insert( *l_iter );
        }
    }

};


class SimpleGarbageCollector_VertexPolicy_UnitTest
{
public:

    template<
        class w_error_policy,
        class w_caster
    >
    class t_type
    {

        AdjacencyList m_adjacency_list;

    public:

        typedef  char  t_vertex_ref;
        typedef
            std::set< char >::const_iterator
                t_vertex_dependencies_const_iterator;

        void GetVertexDependencies(
            const t_vertex_ref & i_vertex,
            t_vertex_dependencies_const_iterator & o_dependencies_begin,
            t_vertex_dependencies_const_iterator & o_dependencies_end
        )
        {
            AT_Assert(
                m_adjacency_list.m_map.find( i_vertex ) != m_adjacency_list.m_map.end()
            );
            o_dependencies_begin = m_adjacency_list.m_map[ i_vertex ].begin();
            o_dependencies_end = m_adjacency_list.m_map[ i_vertex ].end();
        }

        void SetAdjacencyList( const AdjacencyList & i_adjacency_list )
        {
            m_adjacency_list = i_adjacency_list;
        }

    };

};


typedef
    SimpleGarbageCollector<
        SimpleGarbageCollector_VertexPolicy_UnitTest,
        SimpleGarbageCollector_ErrorPolicy_Null
    >
        SimpleGarbageCollector_UnitTest;


AT_TestArea( SimpleGarbageCollector, "Simple garbage collector tests" );

AT_DefineTestLevel(
    SimpleGarbageCollector,
    SimpleGarbageCollector,
    "Basic simple garbage collector test",
    UnitTestTraits::TestSuccess,
    0
)
{

    void Run()
    {

        {
            std::string l_vertices( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            std::string l_anchors( "K" );
            AdjacencyList l_adjacency_list;
            l_adjacency_list.AddVertices( l_vertices );
            l_adjacency_list.AddDependants( 'B', "JL" );
            l_adjacency_list.AddDependants( 'E', "SUZ" );
            l_adjacency_list.AddDependants( 'G', "NRT" );
            l_adjacency_list.AddDependants( 'K', "ABEFG" );
            l_adjacency_list.AddDependants( 'T', "W" );
            l_adjacency_list.AddDependants( 'U', "X" );

            std::string l_expected_keep_list( "ABEFGJKLNRSTUWXZ" );
            std::string l_expected_garbage_list( "CDHIMOPQVY" );

            std::string l_keep_list;
            std::string l_garbage_list;

            SimpleGarbageCollector_UnitTest l_gc;
            l_gc.SetAdjacencyList( l_adjacency_list );
            l_gc.FindLinkedAndUnlinkedVertices(
                l_vertices.begin(),
                l_vertices.end(),
                l_anchors.begin(),
                l_anchors.end(),
                std::back_inserter( l_keep_list ),
                std::back_inserter( l_garbage_list )
            );

            AT_TCAssert( l_keep_list == l_expected_keep_list, "Actual keep list doesn't match expected." );
            AT_TCAssert( l_garbage_list == l_expected_garbage_list, "Actual garbage list doesn't match expected." );
        }

        {
            std::string l_vertices( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            std::string l_anchors( "DGLQS" );
            AdjacencyList l_adjacency_list;
            l_adjacency_list.AddVertices( l_vertices );
            l_adjacency_list.AddDependants( 'A', "IR" );
            l_adjacency_list.AddDependants( 'B', "V" );
            l_adjacency_list.AddDependants( 'D', "W" );
            l_adjacency_list.AddDependants( 'E', "A" );
            l_adjacency_list.AddDependants( 'F', "X" );
            l_adjacency_list.AddDependants( 'H', "D" );
            l_adjacency_list.AddDependants( 'K', "ET" );
            l_adjacency_list.AddDependants( 'L', "B" );
            l_adjacency_list.AddDependants( 'N', "K" );
            l_adjacency_list.AddDependants( 'O', "F" );
            l_adjacency_list.AddDependants( 'P', "C" );
            l_adjacency_list.AddDependants( 'Q', "Q" );
            l_adjacency_list.AddDependants( 'R', "Y" );
            l_adjacency_list.AddDependants( 'S', "Z" );
            l_adjacency_list.AddDependants( 'T', "R" );
            l_adjacency_list.AddDependants( 'U', "H" );
            l_adjacency_list.AddDependants( 'V', "L" );
            l_adjacency_list.AddDependants( 'W', "J" );
            l_adjacency_list.AddDependants( 'X', "O" );
            l_adjacency_list.AddDependants( 'Z', "E" );

            std::string l_expected_keep_list( "ABDEGIJLQRSVWYZ" );
            std::string l_expected_garbage_list( "CFHKMNOPTUX" );

            std::string l_keep_list;
            std::string l_garbage_list;

            SimpleGarbageCollector_UnitTest l_gc;
            l_gc.SetAdjacencyList( l_adjacency_list );
            l_gc.FindLinkedAndUnlinkedVertices(
                l_vertices.begin(),
                l_vertices.end(),
                l_anchors.begin(),
                l_anchors.end(),
                std::back_inserter( l_keep_list ),
                std::back_inserter( l_garbage_list )
            );

            AT_TCAssert( l_keep_list == l_expected_keep_list, "Actual keep list doesn't match expected." );
            AT_TCAssert( l_garbage_list == l_expected_garbage_list, "Actual garbage list doesn't match expected." );
        }

        {
            std::string l_vertices( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            std::string l_anchors( "PY" );
            AdjacencyList l_adjacency_list;
            l_adjacency_list.AddVertices( l_vertices );
            l_adjacency_list.AddDependants( 'A', "M" );
            l_adjacency_list.AddDependants( 'B', "KO" );
            l_adjacency_list.AddDependants( 'C', "Y" );
            l_adjacency_list.AddDependants( 'D', "J" );
            l_adjacency_list.AddDependants( 'E', "N" );
            l_adjacency_list.AddDependants( 'F', "Z" );
            l_adjacency_list.AddDependants( 'G', "K" );
            l_adjacency_list.AddDependants( 'H', "A" );
            l_adjacency_list.AddDependants( 'I', "Q" );
            l_adjacency_list.AddDependants( 'J', "S" );
            l_adjacency_list.AddDependants( 'K', "P" );
            l_adjacency_list.AddDependants( 'L', "C" );
            l_adjacency_list.AddDependants( 'M', "X" );
            l_adjacency_list.AddDependants( 'N', "W" );
            l_adjacency_list.AddDependants( 'O', "S" );
            l_adjacency_list.AddDependants( 'P', "V" );
            l_adjacency_list.AddDependants( 'Q', "C" );
            l_adjacency_list.AddDependants( 'R', "Y" );
            l_adjacency_list.AddDependants( 'S', "M" );
            l_adjacency_list.AddDependants( 'U', "G" );
            l_adjacency_list.AddDependants( 'V', "FW" );
            l_adjacency_list.AddDependants( 'W', "B" );
            l_adjacency_list.AddDependants( 'X', "DZ" );
            l_adjacency_list.AddDependants( 'Z', "T" );

            std::string l_expected_keep_list( "BDFJKMOPSTVWXYZ" );
            std::string l_expected_garbage_list( "ACEGHILNQRU" );

            std::string l_keep_list;
            std::string l_garbage_list;

            SimpleGarbageCollector_UnitTest l_gc;
            l_gc.SetAdjacencyList( l_adjacency_list );
            l_gc.FindLinkedAndUnlinkedVertices(
                l_vertices.begin(),
                l_vertices.end(),
                l_anchors.begin(),
                l_anchors.end(),
                std::back_inserter( l_keep_list ),
                std::back_inserter( l_garbage_list )
            );

            AT_TCAssert( l_keep_list == l_expected_keep_list, "Actual keep list doesn't match expected." );
            AT_TCAssert( l_garbage_list == l_expected_garbage_list, "Actual garbage list doesn't match expected." );
        }

        {
            std::string l_vertices( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            std::string l_anchors( "CFKLRWYZ" );
            AdjacencyList l_adjacency_list;
            l_adjacency_list.AddVertices( l_vertices );
            l_adjacency_list.AddDependants( 'A', "H" );
            l_adjacency_list.AddDependants( 'B', "S" );
            l_adjacency_list.AddDependants( 'C', "M" );
            l_adjacency_list.AddDependants( 'D', "IV" );
            l_adjacency_list.AddDependants( 'E', "T" );
            l_adjacency_list.AddDependants( 'F', "W" );
            l_adjacency_list.AddDependants( 'G', "G" );
            l_adjacency_list.AddDependants( 'H', "NR" );
            l_adjacency_list.AddDependants( 'I', "Q" );
            l_adjacency_list.AddDependants( 'J', "R" );
            l_adjacency_list.AddDependants( 'K', "M" );
            l_adjacency_list.AddDependants( 'L', "D" );
            l_adjacency_list.AddDependants( 'P', "B" );
            l_adjacency_list.AddDependants( 'Q', "D" );
            l_adjacency_list.AddDependants( 'R', "AE" );
            l_adjacency_list.AddDependants( 'S', "H" );
            l_adjacency_list.AddDependants( 'U', "A" );
            l_adjacency_list.AddDependants( 'V', "L" );
            l_adjacency_list.AddDependants( 'W', "Z" );
            l_adjacency_list.AddDependants( 'X', "S" );
            l_adjacency_list.AddDependants( 'Y', "M" );
            l_adjacency_list.AddDependants( 'Z', "F" );

            std::string l_expected_keep_list( "ACDEFHIKLMNQRTVWYZ" );
            std::string l_expected_garbage_list( "BGJOPSUX" );

            std::string l_keep_list;
            std::string l_garbage_list;

            SimpleGarbageCollector_UnitTest l_gc;
            l_gc.SetAdjacencyList( l_adjacency_list );
            l_gc.FindLinkedAndUnlinkedVertices(
                l_vertices.begin(),
                l_vertices.end(),
                l_anchors.begin(),
                l_anchors.end(),
                std::back_inserter( l_keep_list ),
                std::back_inserter( l_garbage_list )
            );

            AT_TCAssert( l_keep_list == l_expected_keep_list, "Actual keep list doesn't match expected." );
            AT_TCAssert( l_garbage_list == l_expected_garbage_list, "Actual garbage list doesn't match expected." );
        }

        {
            std::string l_vertices( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            std::string l_anchors( "A" );
            AdjacencyList l_adjacency_list;
            l_adjacency_list.AddVertices( l_vertices );
            l_adjacency_list.AddDependants( 'A', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'B', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'C', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'D', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'E', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'F', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'G', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'H', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'I', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'J', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'K', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'L', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'M', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'N', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'O', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'P', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'Q', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'R', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'S', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'T', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'U', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'V', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'W', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'X', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'Y', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            l_adjacency_list.AddDependants( 'Z', "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );

            std::string l_expected_keep_list( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
            std::string l_expected_garbage_list( "" );

            std::string l_keep_list;
            std::string l_garbage_list;

            SimpleGarbageCollector_UnitTest l_gc;
            l_gc.SetAdjacencyList( l_adjacency_list );
            l_gc.FindLinkedAndUnlinkedVertices(
                l_vertices.begin(),
                l_vertices.end(),
                l_anchors.begin(),
                l_anchors.end(),
                std::back_inserter( l_keep_list ),
                std::back_inserter( l_garbage_list )
            );

            AT_TCAssert( l_keep_list == l_expected_keep_list, "Actual keep list doesn't match expected." );
            AT_TCAssert( l_garbage_list == l_expected_garbage_list, "Actual garbage list doesn't match expected." );
        }

    }

};

AT_RegisterTest( SimpleGarbageCollector, SimpleGarbageCollector );
