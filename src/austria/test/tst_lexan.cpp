
#include "at_lexan.h"
#include "at_unit_test.h"

#include <sstream>
#include <iostream>


namespace
{
    
AT_TestArea( LexAnal, "Lexical Analysis" );

enum Tokens
{
    ABC,
    ABCDOT
};

class Context
{
};

AT_DefineTest( LexAnal, LexAnal, "Basic Lexical Test" )
{

    void Run()
    {
		CompileTest<int>();
    }

    template <typename T>
    void CompileTest()
    {

//... noreturn
//... different input/process type to expression type

        at::LexExpr<char>   l_expra0 = at::LexExpr<char>( "ABC" );
        at::LexExpr<char>   l_expra1 = at::LexExpr<char>( "ABC" );
        at::LexExpr<char>   l_expra = at::LexExpr<char>( (const char*)"ABC" ) | "DEF";
        at::LexExpr<char>   l_exprb = l_expra + '.';
        at::LexExprRoot<char>   l_exprx = l_expra && l_exprb + 'x'; // context
        at::LexExprRoot<char>   l_exprz = l_expra || l_exprb + 'x'; // noreturn
        at::LexExpr<char>   l_exprc = l_expra + at::LexRange<char>( 'a', 'z' )( 'A', 'Z' )( '_' ).Not();
        at::LexExpr<char>   l_exprxx = at::LexRange<char>( 'a', 'z' )( 'A', 'Z' )( '_' ).Not() ++;
        at::LexExpr<char>   l_expr1 = ( ( at::LexExpr<char>( "ABC" ) | "DEF" ) + '.' + at::LexRange<char>( 'a', 'z' )( 'A', 'Z' ) ) ++ ;
        
        at::LexExpr<char>   l_expr2 = ( l_expra + 'v' ).Match( 3, 5 );

        at::LexAnalyser< at::LexBasicTraits<char, Tokens> > l_anal;

        at::LexAnalyser< at::LexBasicTraits<char, Tokens> >::Terminal l_term = l_anal.Add( l_expra, ABC );
        l_anal.Add( l_exprb, ABCDOT );

        Context             l_context;
#if 0        
        at::LexProcessor< at::LexAnalyser< at::LexBasicTraits<char, Tokens> >, Context >  l_processor( l_anal, l_context );

        l_processor.Process( 'A' );
        l_processor.Process( 'B' );
        l_processor.Process( 'C' );
        l_processor.Process( '.' );
#endif
    }

};

AT_RegisterTest( LexAnal, LexAnal );

} // anon namespace
