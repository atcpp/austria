//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *
 * at_buffer.cpp
 *
 *
 * Basic buffer implementation.
 *
 */


#include "at_buffer.h"
#include "at_lifetime_mt.h"
#include "at_ffs.h"
#include "at_factory.h"

#include <cstdlib>
#include <cmath>

#pragma warning (disable : 4250)

// Austria namespace
namespace at_buffer
{

using namespace at;


// ======== NextChunkSize =============================================
/**
 * This will compute the next chunk size to allocate.  To
 * reduce the fragmentation behaviour, allocations are done in
 * 2^n chunks.
 *
 * @param i_new_size The new size
 * @return Something greater or equal to i_new_size
 */

SizeMem NextChunkSize(
    SizeMem                 i_new_size
) {
    static const int l_off_siz = 6 * sizeof( void * );

    SizeMem l_size = 1 << FindFirstSet( i_new_size + l_off_siz );

    AT_Assert( ( l_size - l_off_siz ) >= i_new_size );

    return l_size - l_off_siz;
}


class Buffer_Basic
  : public PtrTarget_MTVirtual,
    public Buffer
{
    public:

    BufferRegion< t_BaseType >                  m_region;

    // ======== CleanUp ===============================================
    /**
     * CleanUp will deallocate the buffer.
     *
     * @return nothing
     */

    void CleanUp()
    {
        if ( m_region.m_mem )
        {
            std::free( m_region.m_mem );
        }
        
        m_region.m_mem = 0;
        m_region.m_allocated = 0;
        m_region.m_max_available = 0;
    }
    

    // ======== DoRealloc =============================================
    /**
     * Allocate more (or less) buffer space.
     *
     * @param i_new_size The size of the region to reserve
     */

    void DoRealloc(
        SizeMem                 i_new_size
    ) {

        t_BaseType * l_new;

        if ( i_new_size == 0 )
        {
            CleanUp();
            return;
        }
        
        if ( m_region.m_mem )
        {
            l_new = static_cast< t_BaseType * >( std::realloc( m_region.m_mem, i_new_size ) );
        }
        else
        {
            l_new = static_cast< t_BaseType * >( std::malloc( i_new_size ) );
        }
            
        // we might have an allocation failure
        if ( ! l_new )
        {
            AT_ThrowDerivation( Buffer_BadAllocation, "Buffer_Basic::DoRealloc malloc/realloc failure" );
        }

        m_region.m_mem = l_new;

        m_region.m_max_available = i_new_size;

        if ( m_region.m_allocated > m_region.m_max_available )
        {
            m_region.m_allocated = m_region.m_max_available;
        }

    }


    // ======== Region ================================================
    
    virtual const BufferRegion< t_BaseType > & Region()
    {
        return m_region;
    }

    // ======== Region ================================================
    
    virtual const BufferRegion< const t_BaseType > Region() const
    {
        // This return is deceptively similar to the previous
        // Region() method but it is very different.  The previous
        // Region() method returns a reference, this return makes
        // a copy using a conversion constructor hence it is a more
        // expensive call.  If we wanted to shave a few cycles
        // we could violate the standard and reinterpret cast
        // the m_region reference since it would probably work,
        // but I doubt that this will be a major performance issue.

        return m_region;
    }


    // ======== SetAvailability =======================================

    virtual const BufferRegion< t_BaseType > & SetAvailability(
        SizeMem                 i_new_size
    ) {
        if ( i_new_size == 0 )
        {
            CleanUp();
            return m_region;
        }

        DoRealloc( i_new_size );

        return m_region;        
    }


    // ======== SetAllocated ==========================================
    
    virtual const BufferRegion< t_BaseType > & SetAllocated(
        SizeMem                 i_new_size
    ) {
        if ( i_new_size > m_region.m_max_available )
        {
            DoRealloc( NextChunkSize( i_new_size ) );
        }

        m_region.m_allocated = i_new_size;
        
        return m_region;
    }


    // ======== IncrementalAllocate ===================================

    virtual t_BaseType * IncrementalAllocate(
        SizeMem                 i_new_size
    ) {

        // need to know first where we're allocated
        SizeMem                 i_orig_allocated = m_region.m_allocated;

        Buffer_Basic::SetAllocated( i_orig_allocated + i_new_size );

        return i_orig_allocated + m_region.m_mem;
    }

    Buffer_Basic()
    {
    }

    ~Buffer_Basic()
    {
        if ( m_region.m_mem )
        {
            std::free( m_region.m_mem );
        }
    }

};

// ======== Pool ======================================================
/**
 * Pool is an interface that manages a "Pool".  There may be alternate
 * implementatios of at::Pool, however, this essentially creates new
 * buffers.
 */

class AUSTRIA_EXPORT Pool_Basic
  : public PtrTarget_MTVirtual,
    public Pool
{
    public:


    // ======== Create ================================================

    virtual at::PtrDelegate< Buffer * > Create()
    {
        return new Buffer_Basic();
    }

};

// Register the factory
AT_MakeFactory0P( "Basic", Pool_Basic, at::Pool, at::DKy );

} // namespace at_buffer

namespace at
{
    Ptr<Buffer*>
    NewBuffer()
    {
        return new at_buffer::Buffer_Basic;
    }
}
