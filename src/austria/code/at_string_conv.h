//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_string_conv.h
 *
 */

#ifndef x_at_string_conv_h_x
#define x_at_string_conv_h_x 1

#include "at_exception.h"
#include <string>
#include <sstream>

// Austria namespace
namespace at
{

// ======== ToString ==================================================
/**
 * Performs a conversion from a type to a string using std::stringstream
 * Usage:
 *      std::string l_str = ToString( 22 );
 *
 *
 * @param w_type    The type of the value to be converted
 * @param i_value   The value to be converted
 * @return A std::string representation of the value.
 */

template <typename w_type>
inline std::string ToString(
    const w_type        & i_value
) {

    std::ostringstream  l_stream;

    l_stream << i_value;

    return l_stream.str();
}


// ======== StringConversionError =====================================
/**
 * at:ExceptionDerivation<at::StringConversionError> can be used to
 * catch conversion errors.
 */

struct StringConversionError {};

// ======== FromString ================================================
/**
 * FromString is a class that can be used to convert from a string
 * value to a destination value.
 * Usage:
 *      int i = FromString( "22" );
 *
 */


struct FromString
{
    FromString( const std::string & i_str )
      : m_str( i_str )
    {
    }

    /**
     * m_str is the string being converted
     */
    const std::string           & m_str;

    template <typename w_type>
    operator w_type ()
    {
        std::istringstream  i_stream( m_str );

        w_type   l_value = w_type();

        if ( ! ( i_stream >> l_value ) )
        {
            AT_ThrowDerivation( at::StringConversionError, "Failure in conversion to string" );
        }

        return l_value;
    }

};



// ======== ToValue ===================================================
/**
 * ToValue performs the same function as FromString however the
 * converted to parameter needs to be specifically defined.
 * Usage:
 *      int i = ToValue<short>( "22" );
 *
 * @param i_str The string being converted
 * @return The converted value of the string
 */

template <typename w_type>
inline w_type ToValue(
    const std::string           & i_str
)
{
    std::istringstream  i_stream( i_str );

    w_type   l_value = w_type();

    if ( ! ( i_stream >> l_value ) )
    {
        AT_ThrowDerivation( StringConversionError, "Failure in conversion to string" );
    }

    return l_value;
}


    
} // namespace at

#endif // #if x_at_string_conv_h_x


