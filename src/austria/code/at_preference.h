/**
 *  \file at_preference.h
 *
 *  \author Bradley Austin
 *
 *  at_preference.h is a preferences library.
 *
 */

#ifndef x_at_preference_h_x
#define x_at_preference_h_x 1

#include "at_assert.h"
#include "at_lifetime.h"

// Uncomment below to get the instrumented version
// of the PreferenceManager
//#define AT_InstrumentPreferenceMgr

#ifdef AT_InstrumentPreferenceMgr
#include "at_lifetime_instr.h"
#endif
namespace at
{
    class AbstractPreferenceManager;
}
#ifdef AT_InstrumentPreferenceMgr
namespace at
{
    // It is critical that this be the first thing the code sees
    
    // This is used only for instrumented version of the
    // AbstractPreferenceManager - it automatically selects the
    // right Ptr type (the type that registers with the ref list)
    //
    
    //
    // Select an instrumented pointer for AbstractPreferenceManager
    template <
        typename w_First,
        typename w_Second
    >
    class PtrSelect< AbstractPreferenceManager *, w_First, w_Second >
    {
        public:
    
        typedef PtrTraits<
            AbstractPreferenceManager *,
            PtrClassRefWrapperInstrumented< AbstractPreferenceManager * >
        >  type;
    };
    
    template <
        typename w_First,
        typename w_Second
    >
    class PtrSelect< const AbstractPreferenceManager *, w_First, w_Second >
    {
        public:
    
        typedef PtrTraits<
            const AbstractPreferenceManager *,
            PtrClassRefWrapperInstrumented< const AbstractPreferenceManager * >
        >  type;
    };
}
#endif // AT_InstrumentPreferenceMgr


#include "at_policy.h"
#include "at_thread.h"
#include "at_types.h"

#include <algorithm>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

// Austria namespace
namespace at
{

    /**
     *  @defgroup AustriaPreference Preferences

    <h2>Introduction</h2>

    <p>Program preferences, a.k.a. options, settings, flags, etc.,
    are a ubiquitous feature in applications programming.  Whether
    controlled by command line arguments, configuration files, or
    whatever other means, preferences have the commonality that they
    tend to introduce an amount of complexity into a program that's
    disproportionate to the mundane role they serve.
    </p>
    <p>
    One reason for this is that preferences are typically used in many
    different places in a program, but need to be set in one place, or
    in a small number of places.  This means that the code to parse and
    error-check the command line arguments, or configuration file,
    etc., must be tightly coupled to many other diverse and unrelated
    parts of the program.  Furthermore, as programs evolve, new
    preferences are frequently added, and old preferences are often
    given new functionalities or behaviors, and occasionally deleted.
    Keeping the external preferences interface consistent and up-to-
    date with all the internal uses of preferences manually is error-
    prone, and quickly becomes tedious.  Additionally, the logistical
    issues that the code managing the external preferences interface
    should be centrally concerned with (like whether the preferences
    are coming from the command line or a preferences file, etc.) are
    largely orthogonal to issues such as what individual named
    preferences exist, what values they're allowed to have, what those
    values mean, etc.  The Austria Preferences module attempts to solve
    these problems and more, by providing a framework in which issues
    that are tightly coupled with usage can be managed at the usage
    site, while seperate interface code independently manages the more
    general logistical issues.
    </p>
    *
    * @{
    */


    class AbstractMutex
    {

    public:

        virtual void Lock() = 0;

        virtual void Unlock() = 0;

        inline AbstractMutex() {}

    private:

        /* Unimplemented */
        AbstractMutex( const AbstractMutex & );
        AbstractMutex & operator=( const AbstractMutex & );

    };


    class DummyMutex
      : public AbstractMutex
    {

    public:

        virtual void Lock();

        virtual void Unlock();

        inline DummyMutex() {}

    private:

        /* Unimplemented */
        DummyMutex( const DummyMutex & );
        DummyMutex & operator=( const DummyMutex & );

    };


    class MultiString
    {

        class Impl
          : public PtrTarget_Basic
        {

        public:

            virtual bool IsVector() const = 0;

            virtual ~Impl() {}

        };

        class Impl_String
          : public Impl
        {

            std::string m_str;

        public:

            inline Impl_String() {}

            inline Impl_String( const std::string & i_str )
              : m_str( i_str )
            {
            }

            virtual ~Impl_String() {}

            virtual bool IsVector() const
            {
                return false;
            }

            inline std::string & Str()
            {
                return m_str;
            }

            inline const std::string & Str() const
            {
                return m_str;
            }

        };

        class Impl_Vector
          : public Impl
        {

            std::vector< MultiString > m_vec;

        public:

            inline Impl_Vector() {}

            inline Impl_Vector( const std::vector< MultiString > & i_vec )
              : m_vec( i_vec )
            {
            }

            virtual ~Impl_Vector() {}

            virtual bool IsVector() const
            {
                return true;
            }

            inline std::vector< MultiString > & Vec()
            {
                return m_vec;
            }

            inline const std::vector< MultiString > & Vec() const
            {
                return m_vec;
            }

        };

        Ptr< Impl * > m_impl;

    public:

        inline MultiString() {}

        inline MultiString( const std::string & i_str )
          : m_impl( new Impl_String( i_str ) )
        {
        }

        inline MultiString( const std::vector< MultiString > & i_vec )
          : m_impl( new Impl_Vector( i_vec ) )
        {
        }

        inline bool IsString() const
        {
            return m_impl && ! m_impl->IsVector();
        }

        inline bool IsVector() const
        {
            return m_impl && m_impl->IsVector();
        }

        inline std::string & Str()
        {
            AT_Assert( IsString() );
            return ( static_cast< Impl_String * >( &*m_impl ) )->Str();
        }

        inline const std::string & Str() const
        {
            AT_Assert( IsString() );
            return ( static_cast< const Impl_String * >( &*m_impl ) )->Str();
        }

        inline std::vector< MultiString > & Vec()
        {
            AT_Assert( IsVector() );
            return ( static_cast< Impl_Vector * >( &*m_impl ) )->Vec();
        }

        inline const std::vector< MultiString > & Vec() const
        {
            AT_Assert( IsVector() );
            return ( static_cast< const Impl_Vector * >( &*m_impl ) )->Vec();
        }

        inline MultiString & operator=( const std::string & i_rhs )
        {
            m_impl = new Impl_String( i_rhs );
            return *this;
        }

        inline MultiString & operator=(
            const std::vector< MultiString > & i_rhs
        )
        {
            m_impl = new Impl_Vector( i_rhs );
            return *this;
        }

        bool operator==( const MultiString & i_rhs ) const
        {
            if ( ! m_impl )
            {
                return i_rhs.m_impl == 0;
            }
            else if ( m_impl->IsVector() )
            {
                return Vec() == i_rhs.Vec();
            }
            else
            {
                return Str() == i_rhs.Str();
            }
        }

        inline bool operator!=( const MultiString & i_rhs ) const
        {
            return ! ( *this == i_rhs );
        }

        inline void swap( MultiString & io_other )
        {
            std::swap( m_impl, io_other.m_impl );
        }

    };


}


namespace std
{


    inline void swap( at::MultiString & first, at::MultiString & second )
    {
        first.swap( second );
    }


}


namespace at
{


    class AbstractPreferenceValue
      : public PtrTarget_Basic
    {

    protected:

        virtual ~AbstractPreferenceValue() {}

    };

    template< class w_type >
    class PreferenceValue
      : public AbstractPreferenceValue
    {

    public:

        w_type m_value;

        PreferenceValue( const w_type & i_value )
          : m_value( i_value )
        {
        }

        virtual ~PreferenceValue() {}

    };


    // ======== PreferenceType ========================================
    /**
     *  The PreferenceType template controls the "type"-dependent
     *  behavior of Preference objects.
     *
     *  Each Preference object has a "type", which is represented by a
     *  C++ type, on which the Preference class is parameterized.  This
     *  "type" parameter determines such things as the C++ type of the
     *  value stored in the Preference (this may be the same as the
     *  "type" parameter type itself, but it doesn't have to be), the
     *  C++ type used to represent the Preference value in the external
     *  interface code (either std::string, or some container of
     *  strings composed of possibly-nested vectors and/or pairs), the
     *  procedures for converting between the two, and the associated
     *  rules for error checking.  All of these are implemented in the
     *  form of member typedefs and static methods.  In order that POD
     *  types may be used as Preference "type" parameters, these member
     *  typedefs and static methods are not (necessarily) defined in
     *  the Preference "type" parameter type itself, but rather in an
     *  instantiation of a template named PreferenceType, which is
     *  paramaterized on the Preference "type" parameter type, in a
     *  parallel manner.
     *
     *  Put more simply, given a type 'foo' (which may be a POD type),
     *  the class PreferenceType<foo> controls the "type"-dependent
     *  behavior for objects of the class Preference<foo>.
     *
     *  By default (when not specialized for a given type),
     *  PreferenceType simply derives from its parameter type.  This
     *  allows a class that might be used as a Preference "type"
     *  parameter to implement the PreferenceType interface itself, as
     *  an alternative to requiring PreferenceType to be specialized
     *  for it.
     *
     *  The only thing the preferences module does with the
     *  PreferenceType template is instantiate it for various types in
     *  order to access the member typedefs and static methods.  The
     *  only thing a user of the preferences module should do with
     *  PreferenceType, in normal circumstances, is define
     *  specializations for it.  In particular, it would never make
     *  sense for anyone to create PreferenceType objects.
     *
     *  Code that defines a specialization for PreferenceType must do
     *  so in the 'at' namespace.
     *
     *  The PreferenceType interface consists of two typedefs and three
     *  static methods:
     *
     *      t_value_type is a member typedef defining the type of the
     *      value stored in the Preference.  This may be the same type
     *      as w_type, but often will not be.
     *
     *      t_external_type is a member typedef controlling how the
     *      Preference value is represented in external situations,
     *      such as in the user interface or in a preferences file.
     *      This must be either std::string, or some container of
     *      strings composed of possibly-nested vectors and/or pairs.
     *
     *      Name() is a static method, taking no arguments, and
     *      returning the name of the Preference "type" as a string.
     *      It will probably make sense for this name to be similar, if
     *      not identical, to the name of the C++ type w_type
     *      represents.  However, the only requirement is that Name()
     *      always returns the same string for the same w_type, and
     *      different strings for different w_type's.  This name may be
     *      visible to end-users, for example through the user
     *      interface or in a preferences file, so it should be
     *      something appropriate for an end-user to see.
     *
     *      Import() is a static method, taking two arguments, and
     *      returing a bool.  The first argument must be a type
     *      equivalent to 'const t_external_type &'.  The second
     *      argument must be a type equivalent to 't_value_type &'.
     *      Import() is used to read a value into a Preference from an
     *      external source, such as a user interface or a preferences
     *      file, and also to perform error checking.  If the input is
     *      semantically valid (i.e. is a valid, unique representation
     *      for a value of type t_value_type), Import() should return
     *      true and store the resulting value in the second argument.
     *      Otherwise it should return false.
     *
     *      Export() is a static method, taking two arguments, and
     *      returning void.  The first argument must be a type
     *      equivalent to 'const t_value_type &'.  The second argument
     *      must be a type equivalent to 't_external_type &'.  Export()
     *      performs the inverse operation to that of Import(), taking
     *      the value stored in a Preference and converting it to a
     *      type suitable for external representation.  The preferences
     *      module assumes that any value stored in a Preference can be
     *      validly and uniquely represented by a value of type
     *      t_external_type, so no provision is made for Export()
     *      failing.  Export() can, however, throw an exception if
     *      necessary.  Otherwise it should store the resulting value
     *      in the second argument.
     *
     *  @param w_type is the C++ type representing the Preference "type".
     *
     */
    template< class w_type >
    struct PreferenceType : w_type
    {

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };


    // ======== PreferenceType< bool > ================================
    /**
     *  Specialization of the PreferenceType template for bool
     *  Preferences.
     *
     */
    template< >
    struct PreferenceType< bool >
    {

        typedef bool t_value_type;

        typedef std::string t_external_type;

        static std::string Name()
        {
            return "bool";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            std::istringstream l_iss( i_s );
            bool l_b;
            l_iss >> l_b;
            if ( l_iss.fail() )
            {
                return false;
            }
            if ( l_iss.good() )
            {
                std::string l_garbage;
                l_iss >> l_garbage;
                if ( ! l_iss.fail() )
                {
                    return false;
                }
            }
            else if ( ! l_iss.eof() )
            {
                return false;
            }
            o_value = l_b;
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            std::ostringstream l_oss;
            l_oss << i_value;
            o_s = l_oss.str();
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };


    // ======== PreferenceType< int > =================================
    /**
     *  Specialization of the PreferenceType template for int
     *  Preferences.
     *
     */
    template< >
    struct PreferenceType< int >
    {

        typedef int t_value_type;

        typedef std::string t_external_type;

        static std::string Name()
        {
            return "int";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            std::istringstream l_iss( i_s );
            int l_i;
            l_iss >> l_i;
            if ( l_iss.fail() )
            {
                return false;
            }
            if ( l_iss.good() )
            {
                std::string l_garbage;
                l_iss >> l_garbage;
                if ( ! l_iss.fail() )
                {
                    return false;
                }
            }
            else if ( ! l_iss.eof() )
            {
                return false;
            }
            o_value = l_i;
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            std::ostringstream l_oss;
            l_oss << i_value;
            o_s = l_oss.str();
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };

    // ======== PreferenceType< unsigned > =================================
    /**
     *  Specialization of the PreferenceType template for unsigned
     *  Preferences.
     *
     */
    template< >
    struct PreferenceType< unsigned >
    {

        typedef unsigned t_value_type;

        typedef std::string t_external_type;

        static std::string Name()
        {
            return "unsigned";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            std::istringstream l_iss( i_s );
            unsigned l_i;
            l_iss >> l_i;
            if ( l_iss.fail() )
            {
                return false;
            }
            if ( l_iss.good() )
            {
                std::string l_garbage;
                l_iss >> l_garbage;
                if ( ! l_iss.fail() )
                {
                    return false;
                }
            }
            else if ( ! l_iss.eof() )
            {
                return false;
            }
            o_value = l_i;
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            std::ostringstream l_oss;
            l_oss << i_value;
            o_s = l_oss.str();
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };
    
    // ======== PreferenceType< unsigned > =================================
    /**
     *  Specialization of the PreferenceType template for unsigned
     *  Preferences.
     *
     */
    template< >
    struct PreferenceType< at::TimeInterval >
    {

        typedef at::TimeInterval t_value_type;

        typedef std::string t_external_type;

        static std::string Name()
        {
            return "TimeInterval";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            std::istringstream l_iss( i_s );
            at::Int64 l_i;
            l_iss >> l_i;
            if ( l_iss.fail() )
            {
                return false;
            }
            if ( l_iss.good() )
            {
                std::string l_garbage;
                l_iss >> l_garbage;
                if ( ! l_iss.fail() )
                {
                    return false;
                }
            }
            else if ( ! l_iss.eof() )
            {
                return false;
            }
            o_value.m_timeinterval = l_i;
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            std::ostringstream l_oss;
            l_oss << i_value.m_timeinterval;
            o_s = l_oss.str();
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };
    
    // ======== PreferenceType< string > ==============================
    /**
     *  Specialization of the PreferenceType template for std::string
     *  Preferences.
     *
     */
    template< >
    struct PreferenceType< std::string >
    {

        typedef std::string t_value_type;

        typedef std::string t_external_type;

        static std::string Name()
        {
            return "string";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            o_value = i_s;
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            o_s = i_value;
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };


    // ======== PreferenceType< Int64 > =================================
    /**
     *  Specialization of the PreferenceType template for 64-bit int
     *  Preferences.
     *
     */
    template< >
    struct PreferenceType< Int64 >
    {

        typedef Int64 t_value_type;

        typedef std::string t_external_type;

        static std::string Name()
        {
            return "int64";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            std::istringstream l_iss( i_s );
            Int64 l_i;
            l_iss >> l_i;
            if ( l_iss.fail() )
            {
                return false;
            }
            if ( l_iss.good() )
            {
                std::string l_garbage;
                l_iss >> l_garbage;
                if ( ! l_iss.fail() )
                {
                    return false;
                }
            }
            else if ( ! l_iss.eof() )
            {
                return false;
            }
            o_value = l_i;
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            std::ostringstream l_oss;
            l_oss << i_value;
            o_s = l_oss.str();
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };


    // ======== PreferenceType< vector< > > ===========================
    /**
     *  Partial specialization of the PreferenceType template for
     *  vectors.
     *
     *  @param w_element_type is the value_type of the vector.
     */
    template< typename w_element_type >
    struct PreferenceType< std::vector< w_element_type > >
    {

        typedef
            std::vector<
                typename PreferenceType< w_element_type >::t_value_type
            >
                t_value_type;

        typedef
            std::vector<
                typename PreferenceType< w_element_type >::t_external_type
            >
                t_external_type;

        static std::string Name()
        {
            return
                "vector(" +
                PreferenceType< w_element_type >::Name() +
                ")";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            const size_t l_size = i_s.size();
            t_value_type l_value;
            l_value.resize( l_size );
            for ( size_t l_i = 0; l_i != l_size; ++l_i )
            {
                if ( ! PreferenceType< w_element_type >::
                           Import( i_s[ l_i ], l_value[ l_i ] )
                   )
                {
                    return false;
                }
            }
            o_value.swap( l_value );
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            const size_t l_size = i_value.size();
            o_s.clear();
            o_s.resize( l_size );
            for ( size_t l_i = 0; l_i != l_size; ++l_i )
            {
                PreferenceType< w_element_type >::
                    Export( i_value[ l_i ], o_s[ l_i ] );
            }
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };


    // ======== PreferenceType< pair< > > =============================
    /**
     *  Partial specialization of the PreferenceType template for
     *  pairs.
     *
     *  @param w_first_type is the first_type of the pair.
     *  @param w_second_type is the second_type of the pair.
     */
    template< typename w_first_type, typename w_second_type >
    struct PreferenceType< std::pair< w_first_type, w_second_type > >
    {

        typedef
            std::pair<
                typename PreferenceType< w_first_type >::t_value_type,
                typename PreferenceType< w_second_type >::t_value_type
            >
                t_value_type;

        typedef
            std::pair<
                typename PreferenceType< w_first_type >::t_external_type,
                typename PreferenceType< w_second_type >::t_external_type
            >
                t_external_type;

        static std::string Name()
        {
            return
                "pair(" +
                PreferenceType< w_first_type >::Name() +
                "," +
                PreferenceType< w_second_type >::Name() +
                ")";
        }

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            t_value_type l_value;
            if ( ( ! PreferenceType< w_first_type >::
                         Import( i_s.first, l_value.first )
                 ) ||
                 ( ! PreferenceType< w_second_type >::
                         Import( i_s.second, l_value.second )
                 )
               )
            {
                return false;
            }
            std::swap( o_value.first, l_value.first );
            std::swap( o_value.second, l_value.second );
            return true;
        }

        static void Export(
            const t_value_type &i_value,
            t_external_type &o_s
        )
        {
            PreferenceType< w_first_type >::
                Export( i_value.first, o_s.first );
            PreferenceType< w_first_type >::
                Export( i_value.second, o_s.second );
        }

    private:

        /* Unimplemented */
        PreferenceType();
        PreferenceType( const PreferenceType & );
        ~PreferenceType();
        PreferenceType & operator=( const PreferenceType & );

    };


    template< class w_type >
    struct PreferenceMultiStringConversion
    {

    private:

        /* Unimplemented */
        PreferenceMultiStringConversion();
        PreferenceMultiStringConversion
            ( const PreferenceMultiStringConversion & );
        ~PreferenceMultiStringConversion();
        PreferenceMultiStringConversion & operator=
            ( const PreferenceMultiStringConversion & );

    };


    template< >
    struct PreferenceMultiStringConversion< std::string >
    {

        static bool Import(
            const MultiString & i_ms,
            std::string & o_s
        )
        {
            if ( ! i_ms.IsString() )
            {
                return false;
            }
            o_s = i_ms.Str();
            return true;
        }

        static void Export( const std::string & i_s, MultiString & o_ms )
        {
            o_ms = i_s;
        }

    private:

        /* Unimplemented */
        PreferenceMultiStringConversion();
        PreferenceMultiStringConversion
            ( const PreferenceMultiStringConversion & );
        ~PreferenceMultiStringConversion();
        PreferenceMultiStringConversion & operator=
            ( const PreferenceMultiStringConversion & );

    };


    template< class w_value_type >
    struct PreferenceMultiStringConversion< std::vector< w_value_type > >
    {

        typedef  std::vector< w_value_type >  t_type;

        static bool Import(
            const MultiString & i_ms,
            t_type & o_s
        )
        {
            if ( ! i_ms.IsVector() )
            {
                return false;
            }
            const std::vector< MultiString > & l_vec = i_ms.Vec();
            const size_t l_vec_size = l_vec.size();
            t_type l_s;
            l_s.resize( l_vec_size );
            for ( size_t l_i = 0; l_i != l_vec_size; ++l_i )
            {
                if ( ! PreferenceMultiStringConversion< w_value_type >::
                           Import( l_vec[ l_i ], l_s[ l_i ] )
                   )
                {
                    return false;
                }
            }
            o_s.swap( l_s );
            return true;
        }

        static void Export( const t_type & i_s, MultiString & o_ms )
        {
            o_ms = std::vector< MultiString >();
            const size_t l_s_size = i_s.size();
            std::vector< MultiString > & l_vec = o_ms.Vec();
            l_vec.resize( l_s_size );
            for ( size_t l_i = 0; l_i != l_s_size; ++l_i )
            {
                PreferenceMultiStringConversion< w_value_type >::
                    Export( i_s[ l_i ], l_vec[ l_i ] );
            }
        }

    private:

        /* Unimplemented */
        PreferenceMultiStringConversion();
        PreferenceMultiStringConversion
            ( const PreferenceMultiStringConversion & );
        ~PreferenceMultiStringConversion();
        PreferenceMultiStringConversion & operator=
            ( const PreferenceMultiStringConversion & );

    };


    template< class w_first_type, class w_second_type >
    struct PreferenceMultiStringConversion<
        std::pair< w_first_type, w_second_type >
    >
    {

        typedef  std::pair< w_first_type, w_second_type >  t_type;

        static bool Import(
            const MultiString & i_ms,
            t_type & o_s
        )
        {
            if ( ! i_ms.IsVector() )
            {
                return false;
            }
            const std::vector< MultiString > & l_vec = i_ms.Vec();
            if ( l_vec.size() != 2 )
            {
                return false;
            }
            t_type l_s;
            if ( ( ! PreferenceMultiStringConversion< w_first_type >::
                         Import( l_vec[0], l_s.first )
                 ) ||
                 ( ! PreferenceMultiStringConversion< w_second_type >::
                         Import( l_vec[1], l_s.second )
                 )
               )
            {
                return false;
            }
            std::swap( o_s.first, l_s.first );
            std::swap( o_s.second, l_s.second );
            return true;
        }

        static void Export( const std::string & i_s, MultiString & o_ms )
        {
            o_ms = i_s;
        }

    private:

        /* Unimplemented */
        PreferenceMultiStringConversion();
        PreferenceMultiStringConversion
            ( const PreferenceMultiStringConversion & );
        ~PreferenceMultiStringConversion();
        PreferenceMultiStringConversion & operator=
            ( const PreferenceMultiStringConversion & );

    };

    // ======== AbstractPreference ====================================
    /**
     *  AbstractPreference is the non-parameterized base class from
     *  which all Preference instantiations derive.  It contains the
     *  interface PreferenceManager uses to iteract with Preference
     *  objects, and is agnostic to Preference "type" issues.
     *
     */
    class AbstractPreference
    {

    protected:

        Ptr< AbstractPreferenceManager * > m_mgr;

        inline AbstractPreference( PtrDelegate< AbstractPreferenceManager * > i_mgr )
          : m_mgr( i_mgr )
        {
        }

        /*  This shouldn't really need to be virtual, since the only
         *  things that should be pointing to AbstractPreference (as
         *  opposed to the concrete Preference template) are the
         *  PreferenceManager and its database policy class(es), and
         *  they never delete the objects they point to.  However this
         *  avoids a compiler warning on some platforms, and may even
         *  save someone from slicing bugs if they're doing something
         *  really strange.
         */
        virtual ~AbstractPreference() {}

    public:

        virtual std::string TypeName() = 0;

        virtual bool Import(
            const std::string & i_type_name,
            const MultiString & i_s,
            Ptr< AbstractPreferenceValue * > & o_value
        ) = 0;

        virtual bool SetValue(
            PtrView< const AbstractPreferenceValue * > i_value
        ) = 0;

    private:

        /* Unimplemented */
        AbstractPreference( const AbstractPreference & );
        AbstractPreference & operator=( const AbstractPreference & );

    };


    class AbstractPreferenceManager
#ifndef AT_InstrumentPreferenceMgr
      : public virtual PtrTarget
#else
      : public PtrTarget_InstrMT
#endif
    {

    public:

        virtual AbstractMutex & ReadLock() = 0;

        virtual void RegisterPref(
            const std::string & i_name,
            const std::string & i_context,
            AbstractPreference & io_pref
        ) = 0;

        virtual void UnregisterPref( AbstractPreference & io_pref ) = 0;

        virtual void ReportWrongTypeNameError(
            AbstractPreference * i_pref,
            const std::string & i_expected_type_name,
            const std::string & i_provided_type_name
        ) = 0;

        virtual void ReportWrongExternalTypeError(
            AbstractPreference * i_pref,
            const std::string & i_type_name,
            const MultiString & i_s
        ) = 0;

        virtual void ReportSyntaxError(
            AbstractPreference * i_pref,
            const std::string & i_type_name,
            const MultiString & i_s
        ) = 0;

        inline AbstractPreferenceManager() {}

        virtual ~AbstractPreferenceManager() {}

    private:

        /* Unimplemented */
        AbstractPreferenceManager( const AbstractPreferenceManager & );
        AbstractPreferenceManager & operator=
            ( const AbstractPreferenceManager & );

    };

    // ======== PreferenceNotificationPolicy_NoNotification ===========
    /**
     *  A class template implementing a Preference notification policy.
     *  Designed to be used as the second parameter in an instantiation
     *  of the Preference template.
     *
     *  This policy performs no notification, and is the default policy
     *  used when a Preference is declared with no second template
     *  parameter.
     *
     *  @param w_type The Preference's w_type parameter.
     */
    template< class w_type >
    class PreferenceNotificationPolicy_NoNotification
    {

    protected:

        static inline void Notify() {}

        inline PreferenceNotificationPolicy_NoNotification() {}
        inline ~PreferenceNotificationPolicy_NoNotification() {}

    private:

        /* Unimplemented */
        PreferenceNotificationPolicy_NoNotification
            ( const PreferenceNotificationPolicy_NoNotification & );
        PreferenceNotificationPolicy_NoNotification & operator=
            ( const PreferenceNotificationPolicy_NoNotification & );

    };


    // ======== Preference ============================================
    /**
     *  The Preference template is used both to instantiate individual
     *  named preferences, and to access them for reading at each
     *  location in the program where the value must be known in order
     *  to control optional program behavior.  Conceptually, a
     *  Preference object can be thought of as a wrapper for a single
     *  read-only variable, that can be declared and used on an ad-hoc
     *  basis, but whose value is controlled (or at least changable) by
     *  the user of the program (or at least by forces beyond the scope
     *  of the code using the Preference) by some means that the code
     *  using the Preference need not be aware of.
     *
     *  The C++ type of the variable contained in the Preference is
     *  determined by (but not necessarily the same as) the type used
     *  as the first parameter to the template.  See the PreferenceType
     *  template for information on the significance of Preference
     *  "types".
     *
     *  Each Preference object must be registered with some
     *  PreferenceManager object.  This registration takes place in the
     *  Preference constructor, which takes a reference to the
     *  PreferenceManager as one of its arguments.  (Similarly,
     *  de-registration takes place automatically in the Preference
     *  destructor.)  The PreferenceManager with which the Preference
     *  is regerstered is responsible for setting the Preference value
     *  according to external input, and (optionally) notifying the
     *  code using the Preference when its value changes.
     *
     *  The mapping of Preference objects to actual preference values
     *  supplied to the program from outside is determined in part by
     *  two string arguments to the Preference constructor, i_name and
     *  i_context.  Conceptually, these represent the name of the
     *  preference, and the context (functional portion of the program)
     *  in which the specific Preference object is being used,
     *  respectively.  Both may be visible to end-users, for example
     *  through the user interface or in a preferences file, and so
     *  should be something appropriate for an end-user to see.  It
     *  will also usually make sense for i_name to be similar, if not
     *  identical, to the name of the Preference variable.  Also, in
     *  order for i_context to be useful, it is important that
     *  different Preference objects use a consistent scheme for
     *  nomenclating the different parts of the program.
     *
     *  Loosely speaking, two Preference objects registered with the
     *  same PreferenceManager represent two views into the same
     *  "preference" if their i_name's are the same, and into different
     *  preferences if their i_name's are different.  However, the
     *  actual mapping is somewhat more complicated than this, as a
     *  single named preference can have different values in different
     *  contexts (or can even be defined in some contexts and undefined
     *  in others).  Also the value of a given named preference in a
     *  given context, or its status as defined or undefined, can
     *  change over time.
     *
     *  Also, each Preference object has a default value, which is
     *  passed as another argument to the Preference constructor, and
     *  determines the value the Preference object contains when no
     *  externally-supplied value is available.  Since default values
     *  are set per Preference object rather than per named Preference,
     *  two Preference objects registered with the same
     *  PreferenceManager, using the same name and context, can still
     *  have different values at the same time, as a result of their
     *  default values being different.
     *
     *  Although it's technically possible to declare two Preference
     *  objects with the same name, and registered with the same
     *  PreferenceManager, but with different w_type's, this should not
     *  normally be done, as at least one of the Preference objects
     *  will necessarily have the wrong w_type for the externally-
     *  supplied value.
     *
     *  Each PreferenceManager has its own namespace for Preference
     *  i_name's.  The rules for what i_name and i_context values are
     *  legal, what i_name's are considered equivalent to each other,
     *  how i_context's are parsed, etc., are up to the
     *  PreferenceManager.
     *
     *  Registration of Preferences with a PreferenceManager may take
     *  place either before or after the PreferenceManager receives
     *  preference values from its external source.  If after, upon
     *  registration the Preference will receive the previously loaded
     *  value, if any, for the given name and context.  If before, upon
     *  registration the Preference will have its default value, and
     *  the value will be updated to the externally-supplied value when
     *  the PreferenceManager receives it.  Since the PreferenceManager
     *  has no way of knowing that a named preference exists, or what
     *  type it is, until registration occurs, it may have to perform
     *  different levels of error-checking in stages.  How to handle
     *  such issues, and what to do when errors are detected, is up to
     *  the PreferenceManager.
     *
     *  The second parameter to the Preference template is a policy
     *  class template controlling what the Preference does to notify
     *  the code using it when its value changes.  By default,
     *  Preference performs no notification.  If notification is
     *  desired, w_notification_policy should be a class template
     *  exposing a method named Notify(), taking no arguments and
     *  returning void, and performing whatever action is desired to
     *  occur when the Preference value changes.  Notify() can be
     *  either static or non-static, and should allow at least
     *  protected access.  If it needs to know the new value, it can
     *  access this (and anything else in Preference that's public) by
     *  statically downcasting the 'this' pointer.  For example:
     *
<code>
<pre>

    template< class w_type >
    class MyNotify
    {
        protected:

        void Notify()
        {
            typename PreferenceType< w_type >::t_value_type &l_value =
                static_cast< Preference< w_type, ::MyNotify > * >( this )->Value();

            ...

        }

    };

    ...

    Preference< bool, MyNotify > my_pref;

</pre>
</code>
     *
     *  The value contained in a Preference can be accessed using the
     *  Value() accessor method.  For efficiency reasons (since the
     *  value may be a container or other large structure), Value()
     *  returns a reference to a member variable inside the Preference
     *  object, rather than returning a copy of the value.  Note that
     *  this kind of access is inherently not thread-safe, since the
     *  Preference object has no way of knowing when the code calling
     *  Value() is finished accessing the value.  For this reason it is
     *  important that the PreferenceManager not update Preference
     *  objects at the same time their contained values are being
     *  accessed.  It is envisioned that, since Preference values
     *  change relatively infrequently, updates can be done with
     *  threads synchronised.  In any event, it is up to the user of
     *  the preferences module to ensure this.
     *
     *  @param w_type The Preference "type" parameter.
     *
     */
    template<
        class w_type,
        template < class > class w_notification_policy =
            PreferenceNotificationPolicy_NoNotification
    >
    class Preference
      : public AbstractPreference,
        public w_notification_policy< w_type >
    {

    public:

        typedef  PreferenceType< w_type >  t_pref_type;

        /**
         * t_value_type is the C++ type of the Preference value.  (May
         * be the same type as w_type, but doesn't have to be.)
         */
        typedef  typename t_pref_type::t_value_type  t_value_type;

        typedef  typename t_pref_type::t_external_type  t_external_type;

        typedef
            PreferenceMultiStringConversion< t_external_type >
                t_conversion_type;

    private:

        t_value_type m_value;

    public:

        using w_notification_policy< w_type >::Notify;

        /**
         *  The one and only constructor.
         *
         *  @param i_name The name of the named "preference".
         *  @param i_default_value The value the Preference contains when no externally-supplied value is available.
         *  @param i_context A string representing the functional portion of the program in which the Preference is being used.
         *  @param i_manager The PreferenceManager managing this Preference.
         */
        Preference(
            const std::string &i_name,
            const t_value_type &i_default_value,
            const std::string &i_context,
            PtrDelegate< AbstractPreferenceManager * > i_manager
        )
          : m_value( i_default_value ),
            AbstractPreference( i_manager )
        {
            m_mgr->RegisterPref( i_name, i_context, *this );
        }

        inline ~Preference()
        {
            m_mgr->UnregisterPref( *this );
        }

        // ======== operator const t_value_type & =====================
        /**
         *  Preference provides a standard conversion to the contained
         *  value type, which can be convenient in contexts that allow
         *  it.  In contexts where this doesn't work, call the Value()
         *  method instead.
         *
         *  @return A const reference to the Preference value.
         */
        inline operator const t_value_type &() const
        {
            Lock< AbstractMutex > l_lock( m_mgr->ReadLock() );
            return m_value;
        }


        // ======== Value =============================================
        /**
         *  Value() returns a const reference to the Preference value.
         *
         *  @return A const reference to the Preference value.
         */
        inline const t_value_type &Value() const
        {
            Lock< AbstractMutex > l_lock( m_mgr->ReadLock() );
            return m_value;
        }

        virtual std::string TypeName()
        {
            return t_pref_type::Name();
        }

        virtual bool Import(
            const std::string & i_type_name,
            const MultiString & i_s,
            Ptr< AbstractPreferenceValue * > & o_value
        )
        {
            std::string l_expected_type_name = t_pref_type::Name();
            if ( i_type_name != l_expected_type_name )
            {
                m_mgr->ReportWrongTypeNameError
                    ( this, l_expected_type_name, i_type_name );
                return false;
            }
            t_external_type l_s;
            if ( ! t_conversion_type::Import( i_s, l_s ) )
            {
                m_mgr->ReportWrongExternalTypeError
                    ( this, l_expected_type_name, i_s );
                return false;
            }
            t_value_type l_value;
            if ( ! t_pref_type::Import( l_s, l_value ) )
            {
                m_mgr->ReportSyntaxError( this, l_expected_type_name, i_s );
                return false;
            }
            o_value = new PreferenceValue< t_value_type >( l_value );
            return true;
        }

        virtual bool SetValue(
            PtrView< const AbstractPreferenceValue * > i_value
        )
        {
            const PreferenceValue< t_value_type > * l_value =
                dynamic_cast< const PreferenceValue< t_value_type > * >
                    ( &*i_value );
            if ( ! l_value )
            {
                return false;
            }
            m_value = l_value->m_value;
            Notify();
            return true;
        }


    private:

        /* Unimplemented */
        Preference( const Preference & );
        Preference & operator=( const Preference & );

    };


    class PreferenceManagerLockPolicy_SingleThreaded;

    class PreferenceManagerErrorPolicy_Null;

    template<
        class w_lock_policy,
        class w_caster
    >
    class PreferenceManagerDBPolicy_Default;


    // ======== PreferenceManager =====================================
    /**
     * This is the basic PreferenceManager implementation.
     *
     */

    template<
        class w_lock_policy = PreferenceManagerLockPolicy_SingleThreaded,
        class w_error_policy = PreferenceManagerErrorPolicy_Null,
        template<
            class w_lock_policy,
            class w_caster
        > class w_db_policy = PreferenceManagerDBPolicy_Default
    >
    class PreferenceManager;
    
#ifdef AT_InstrumentPreferenceMgr
    // This is used only for instrumented version of the
    // PreferenceManager<> - it automatically selects the
    // right Ptr type (the type that registers with the ref list)
    //

    // All specializations of PreferenceManager<> need to have
    // one of these if the instrumented pointer is not to break
    // things.
    
    //
    // Select an instrumented pointer for AbstractPreferenceManager
    template <
        typename w_First,
        typename w_Second
    >
    class PtrSelect< PreferenceManager<> *, w_First, w_Second >
    {
        public:
    
        typedef PtrTraits<
            PreferenceManager<> *,
            PtrClassRefWrapperInstrumented< PreferenceManager<> * >
        >  type;
    };
    
    template <
        typename w_First,
        typename w_Second
    >
    class PtrSelect< const PreferenceManager<> *, w_First, w_Second >
    {
        public:
    
        typedef PtrTraits<
            const PreferenceManager<> *,
            PtrClassRefWrapperInstrumented< const PreferenceManager<> * >
        >  type;
    };

#endif // AT_InstrumentPreferenceMgr


    template<
        class w_lock_policy,
        class w_error_policy,
        template<
            class w_lock_policy,
            class w_caster
        > class w_db_policy
    >
    class PreferenceManager
      : public AbstractPreferenceManager,
        public w_lock_policy::t_ptr_target_type,
        public w_lock_policy,
        public w_error_policy,
        public w_db_policy<
            w_lock_policy,
            PolicyCaster<
                PreferenceManager<
                    w_lock_policy,
                    w_error_policy,
                    w_db_policy
                >
            >
        >
    {

    public:

        typedef
            typename w_lock_policy::t_ptr_target_type
                t_ptr_target_type;

        typedef
            w_db_policy<
                w_lock_policy,
                PolicyCaster< PreferenceManager >
            >
                t_db_policy;

        using w_lock_policy::WriteLock;

        AbstractMutex & ReadLock()
        {
            return w_lock_policy::ReadLock();
        }

        virtual void RegisterPref(
            const std::string & i_name,
            const std::string & i_context,
            AbstractPreference & io_pref
        )
        {
            t_db_policy::RegisterPref( i_name, i_context, io_pref );
        }

        virtual void UnregisterPref( AbstractPreference & io_pref )
        {
            t_db_policy::UnregisterPref( io_pref );
        }

        virtual void ReportWrongTypeNameError(
            AbstractPreference * i_pref,
            const std::string & i_expected_type_name,
            const std::string & i_provided_type_name
        )
        {
            w_error_policy::ReportWrongTypeNameError(
                i_pref,
                i_expected_type_name,
                i_provided_type_name
            );
        }

        virtual void ReportWrongExternalTypeError(
            AbstractPreference * i_pref,
            const std::string & i_type_name,
            const MultiString & i_s
        )
        {
            w_error_policy::ReportWrongExternalTypeError(
                i_pref,
                i_type_name,
                i_s
            );
        }

        virtual void ReportSyntaxError(
            AbstractPreference * i_pref,
            const std::string & i_type_name,
            const MultiString & i_s
        )
        {
            w_error_policy::ReportSyntaxError(
                i_pref,
                i_type_name,
                i_s
            );
        }


        inline PreferenceManager() {}

        virtual ~PreferenceManager() {}


        /**
         *  AddRef() and Release() wrappers needed to avoid a compiler
         *  warning in msvc.
         */

#ifndef AT_InstrumentPreferenceMgr
        virtual int AddRef() const
        {
            return t_ptr_target_type::AddRef();
        }

        virtual int Release() const
        {
            return t_ptr_target_type::Release();
        }
#endif

    private:

        /* Unimplemented */
        PreferenceManager( const PreferenceManager & );
        PreferenceManager & operator=( const PreferenceManager & );

    };


    class PreferenceManagerLockPolicy_SingleThreaded
    {

    public:

#ifndef AT_InstrumentPreferenceMgr
        typedef  PtrTarget_Virtual  t_ptr_target_type;
#else
        struct NullRefCnt {};
        typedef NullRefCnt t_ptr_target_type;
#endif

    private:

        DummyMutex m_mutex;

    public:

        inline AbstractMutex & ReadLock()
        {
            return m_mutex;
        }

        inline AbstractMutex & WriteLock()
        {
            return m_mutex;
        }

    public:

        inline PreferenceManagerLockPolicy_SingleThreaded() {}
        inline ~PreferenceManagerLockPolicy_SingleThreaded() {}

    private:

        /* Unimplemented */
        PreferenceManagerLockPolicy_SingleThreaded
            ( const PreferenceManagerLockPolicy_SingleThreaded & );
        PreferenceManagerLockPolicy_SingleThreaded & operator=
            ( const PreferenceManagerLockPolicy_SingleThreaded & );

    };


    class PreferenceValueTable
    {

        typedef  std::pair< std::string, MultiString >  t_table_value_type;

        typedef
            std::map< std::string, t_table_value_type >
                t_context_subtable_type;

        typedef
            std::map< std::string, t_context_subtable_type >
                t_table_type;

        typedef  t_table_type::iterator  t_table_iterator;
        typedef  t_table_type::const_iterator  t_table_const_iterator;
        typedef  t_table_type::reverse_iterator  t_table_reverse_iterator;
        typedef
            t_table_type::const_reverse_iterator
                t_table_const_reverse_iterator;
        typedef
            t_context_subtable_type::iterator
                t_context_subtable_iterator;
        typedef
            t_context_subtable_type::const_iterator
                t_context_subtable_const_iterator;
        typedef
            t_context_subtable_type::reverse_iterator
                t_context_subtable_reverse_iterator;
        typedef
            t_context_subtable_type::const_reverse_iterator
                t_context_subtable_const_reverse_iterator;

        t_table_type m_table;

    public:

        void SetValue(
            const std::string & i_name,
            const std::string & i_context,
            const std::string & i_type_name,
            const MultiString & i_external_value
        );

        class LookupResult
        {

            const std::string * m_name;

            const std::pair<
                const std::string,
                t_table_value_type
            > * m_value;

        public:

            inline bool Found() const
            {
                return m_value != 0;
            }

            inline const std::string & Name() const
            {
                AT_Assert( m_value != 0 );
                return *m_name;
            }

            inline const std::string & Context() const
            {
                AT_Assert( m_value != 0 );
                return m_value->first;
            }

            inline const std::string & Type() const
            {
                AT_Assert( m_value != 0 );
                return m_value->second.first;
            }

            inline const MultiString & Value() const
            {
                AT_Assert( m_value != 0 );
                return m_value->second.second;
            }

            inline LookupResult(
                const std::string * i_name,
                const std::pair<
                    const std::string,
                    t_table_value_type
                > * i_value
            )
              : m_name( i_name ),
                m_value( i_value )
            {
                AT_Assert(
                    ( i_name && i_value ) ||
                    ( ( ! i_name ) && ( ! i_value ) )
                );
            }

        };

        LookupResult Lookup(
            const std::string & i_name,
            const std::string & i_context
        ) const;


        class const_iterator
        {
            friend class PreferenceValueTable;

            t_table_const_iterator m_table_iter;
            t_table_const_iterator m_table_end;
            t_context_subtable_const_iterator m_subtable_iter;
            mutable LookupResult m_cache;

            inline const_iterator(
                t_table_const_iterator i_table_iter,
                t_table_const_iterator i_table_end,
                t_context_subtable_const_iterator i_subtable_iter
            )
              : m_table_iter( i_table_iter ),
                m_table_end( i_table_end ),
                m_subtable_iter( i_subtable_iter ),
                m_cache( 0, 0 )
            {
            }

        public:

            inline const_iterator()
              : m_cache( 0, 0 )
            {
            }

            bool operator==( const const_iterator & i_rhs ) const
            {
                return
                    m_table_end == i_rhs.m_table_end &&
                    m_table_iter == i_rhs.m_table_iter &&
                    ( m_table_iter == m_table_end ||
                      m_subtable_iter == i_rhs.m_subtable_iter
                    );
            }

            inline bool operator!=( const const_iterator & i_rhs ) const
            {
                return ! ( *this == i_rhs );
            }

            const_iterator & operator++()
            {
                AT_Assert( m_table_iter != m_table_end );
                AT_Assert(
                    m_table_iter->second.begin() != m_table_iter->second.end()
                );
                if ( ++m_subtable_iter == m_table_iter->second.end() )
                {
                    if ( ++m_table_iter == m_table_end )
                    {
                        m_subtable_iter = t_context_subtable_const_iterator();
                    }
                    else
                    {
                        AT_Assert(
                            m_table_iter->second.begin() !=
                               m_table_iter->second.end()
                        );
                        m_subtable_iter = m_table_iter->second.begin();
                    }
                }
                return *this;
            }

            inline const_iterator operator++( int )
            {
                const_iterator l_iter = *this;
                ++*this;
                return l_iter;
            }

            const_iterator & operator--()
            {
                AT_Assert(
                    m_table_iter == m_table_end ||
                    m_table_iter->second.begin() != m_table_iter->second.end()
                );
                if ( m_table_iter == m_table_end ||
                     m_subtable_iter == m_table_iter->second.begin()
                   )
                {
                    --m_table_iter;
                    AT_Assert(
                        m_table_iter->second.begin() !=
                            m_table_iter->second.end()
                    );
                    m_subtable_iter = m_table_iter->second.end();
                }
                --m_subtable_iter;
                return *this;
            }

            inline const_iterator operator--( int )
            {
                const_iterator l_iter = *this;
                --*this;
                return l_iter;
            }

            LookupResult operator*() const
            {
                AT_Assert( m_table_iter != m_table_end );
                return
                    LookupResult(
                        &( m_table_iter->first ),
                        &*m_subtable_iter
                    );
            }

            const LookupResult * operator->() const
            {
                m_cache = **this;
                return &m_cache;
            }

        };


        class const_reverse_iterator
        {
            friend class PreferenceValueTable;

            t_table_const_reverse_iterator m_table_iter;
            t_table_const_reverse_iterator m_table_rend;
            t_context_subtable_const_reverse_iterator m_subtable_iter;
            mutable LookupResult m_cache;

            inline const_reverse_iterator(
                t_table_const_reverse_iterator i_table_iter,
                t_table_const_reverse_iterator i_table_rend,
                t_context_subtable_const_reverse_iterator i_subtable_iter
            )
              : m_table_iter( i_table_iter ),
                m_table_rend( i_table_rend ),
                m_subtable_iter( i_subtable_iter ),
                m_cache( 0, 0 )
            {
            }

        public:

            inline const_reverse_iterator()
              : m_cache( 0, 0 )
            {
            }

            bool operator==( const const_reverse_iterator & i_rhs ) const
            {
                return
                    m_table_rend == i_rhs.m_table_rend &&
                    m_table_iter == i_rhs.m_table_iter &&
                    ( m_table_iter == m_table_rend ||
                      m_subtable_iter == i_rhs.m_subtable_iter
                    );
            }

            inline bool operator!=(
                const const_reverse_iterator & i_rhs
            ) const
            {
                return ! ( *this == i_rhs );
            }

            const_reverse_iterator & operator++()
            {
                AT_Assert( m_table_iter != m_table_rend );
                AT_Assert(
                    m_table_iter->second.rbegin() !=
                        m_table_iter->second.rend()
                );
                if ( ++m_subtable_iter == m_table_iter->second.rend() )
                {
                    if ( ++m_table_iter == m_table_rend )
                    {
                        m_subtable_iter =
                            t_context_subtable_const_reverse_iterator();
                    }
                    else
                    {
                        AT_Assert(
                            m_table_iter->second.rbegin() !=
                               m_table_iter->second.rend()
                        );
                        m_subtable_iter = m_table_iter->second.rbegin();
                    }
                }
                return *this;
            }

            inline const_reverse_iterator operator++( int )
            {
                const_reverse_iterator l_iter = *this;
                ++*this;
                return l_iter;
            }

            const_reverse_iterator & operator--()
            {
                AT_Assert(
                    m_table_iter == m_table_rend ||
                    m_table_iter->second.rbegin() !=
                        m_table_iter->second.rend()
                );
                if ( m_table_iter == m_table_rend ||
                     m_subtable_iter == m_table_iter->second.rbegin()
                   )
                {
                    --m_table_iter;
                    AT_Assert(
                        m_table_iter->second.rbegin() !=
                            m_table_iter->second.rend()
                    );
                    m_subtable_iter = m_table_iter->second.rend();
                }
                --m_subtable_iter;
                return *this;
            }

            inline const_reverse_iterator operator--( int )
            {
                const_reverse_iterator l_iter = *this;
                --*this;
                return l_iter;
            }

            LookupResult operator*() const
            {
                AT_Assert( m_table_iter != m_table_rend );
                return
                    LookupResult(
                        &( m_table_iter->first ),
                        &*m_subtable_iter
                    );
            }

            const LookupResult * operator->() const
            {
                m_cache = **this;
                return &m_cache;
            }

        };


        const_iterator begin() const
        {
            return const_iterator(
                m_table.begin(),
                m_table.end(),
                ( m_table.begin() == m_table.end() ?
                      t_context_subtable_const_iterator() :
                      m_table.begin()->second.begin()
                )
            );
        }

        const_iterator end() const
        {
            return const_iterator(
                m_table.end(),
                m_table.end(),
                t_context_subtable_const_iterator()
            );
        }

        const_reverse_iterator rbegin() const
        {
            return const_reverse_iterator(
                m_table.rbegin(),
                m_table.rend(),
                ( m_table.rbegin() == m_table.rend() ?
                      t_context_subtable_const_reverse_iterator() :
                      m_table.rbegin()->second.rbegin()
                )
            );
        }

        const_reverse_iterator rend() const
        {
            return const_reverse_iterator(
                m_table.rend(),
                m_table.rend(),
                t_context_subtable_const_reverse_iterator()
            );
        }

        bool empty() const
        {
            return m_table.empty();
        }

        inline void clear()
        {
            m_table.clear();
        }

        inline bool operator==( const PreferenceValueTable & i_rhs ) const
        {
            return m_table == i_rhs.m_table;
        }

        inline bool operator!=( const PreferenceValueTable & i_rhs ) const
        {
            return ! ( *this == i_rhs );
        }

        inline void swap( PreferenceValueTable & io_other )
        {
            m_table.swap( io_other.m_table );
        }

    };


}


namespace std
{


    /*  Performance optimization - use map::swap to swap tables.  (Just
     *  swaps the root pointers of the two maps; no data copying.)
     */
    inline void swap(
        at::PreferenceValueTable & first,
        at::PreferenceValueTable & second
    )
    {
        first.swap( second );
    }


}


namespace at
{


    template<
        class w_lock_policy,
        class w_caster
    >
    class PreferenceManagerDBPolicy_Default
    {

        typedef  std::pair< std::string, std::string >  t_name_and_context;

        typedef
            std::map< AbstractPreference *, t_name_and_context >
                t_pref_obj_table_type;

        typedef
            std::map< t_name_and_context, Ptr< AbstractPreferenceValue * > >
                t_cache_type;


        t_pref_obj_table_type m_pref_obj_table;

        PreferenceValueTable m_value_table;

        t_cache_type m_cache;


        inline AbstractMutex & WriteLock()
        {
            return
                PolicyCast<
                    w_lock_policy,
                    w_caster
                >::From( *this ).WriteLock();
        }

        void UpdatePref(
            const std::string & i_name,
            const std::string & i_context,
            AbstractPreference & io_pref
        )
        {
            PreferenceValueTable::LookupResult l_lookup_result =
                m_value_table.Lookup( i_name, i_context );
            if ( l_lookup_result.Found() )
            {
                const t_name_and_context l_name_and_context(
                    l_lookup_result.Name(),
                    l_lookup_result.Context()
                );
                t_cache_type::iterator l_iter =
                    m_cache.find( l_name_and_context );
                if ( l_iter != m_cache.end() )
                {
                    if ( io_pref.SetValue( l_iter->second ) )
                    {
                        return;
                    }
                    m_cache.erase( l_iter );
                }
                Ptr< AbstractPreferenceValue * > l_cache_value;
                if ( ! io_pref.Import(
                           l_lookup_result.Type(),
                           l_lookup_result.Value(),
                           l_cache_value
                       )
                   )
                {
                    return;
                }
                l_iter = m_cache.insert(
                    std::pair<
                        const t_name_and_context,
                        Ptr< AbstractPreferenceValue * >
                    >( l_name_and_context, l_cache_value )
                ).first;
                bool l_success = io_pref.SetValue( l_iter->second );
                AT_Assert( l_success );
            }
        }

    public:

        void RegisterPref(
            const std::string & i_name,
            const std::string & i_context,
            AbstractPreference & io_pref
        )
        {
            Lock< AbstractMutex > l_lock( WriteLock() );

            AT_Assert(
                m_pref_obj_table.find( &io_pref ) == m_pref_obj_table.end()
            );
            m_pref_obj_table.insert(
                std::pair< AbstractPreference * const, t_name_and_context >
                    ( &io_pref, t_name_and_context( i_name, i_context ) )
            );
            UpdatePref( i_name, i_context, io_pref );
        }


        void UnregisterPref( AbstractPreference & io_pref )
        {
            Lock< AbstractMutex > l_lock( WriteLock() );

            t_pref_obj_table_type::iterator l_pref_obj_iter =
                m_pref_obj_table.find( &io_pref );
            AT_Assert( l_pref_obj_iter != m_pref_obj_table.end() );
            m_pref_obj_table.erase( l_pref_obj_iter );
        }


        const PreferenceValueTable & ValueTable() const
        {
            return m_value_table;
        }


        void SwapValueTable( PreferenceValueTable & i_table )
        {
            Lock< AbstractMutex > l_lock( WriteLock() );

            m_value_table.swap( i_table );
            m_cache.clear();
            for ( t_pref_obj_table_type::iterator
                      l_iter = m_pref_obj_table.begin(),
                      l_end = m_pref_obj_table.end();
                  l_iter != l_end;
                  ++l_iter
                )
            {
                UpdatePref(
                    l_iter->second.first,
                    l_iter->second.second,
                    *( l_iter->first )
                );
            }
        }

    protected:

        inline PreferenceManagerDBPolicy_Default() {}

        inline ~PreferenceManagerDBPolicy_Default()
        {
            AT_Assert( m_pref_obj_table.empty() );
        }

    private:

        /* Unimplemented */
        PreferenceManagerDBPolicy_Default
            ( const PreferenceManagerDBPolicy_Default & );
        PreferenceManagerDBPolicy_Default & operator=
            ( const PreferenceManagerDBPolicy_Default & );

    };


    class PreferenceManagerErrorPolicy_Null
    {

    public:

        inline void ReportWrongTypeNameError(
            AbstractPreference * i_pref,
            const std::string & i_expected_type_name,
            const std::string & i_provided_type_name
        )
        {
        }

        inline void ReportWrongExternalTypeError(
            AbstractPreference * i_pref,
            const std::string & i_type_name,
            const MultiString & i_s
        )
        {
        }

        inline void ReportSyntaxError(
            AbstractPreference * i_pref,
            const std::string & i_type_name,
            const MultiString & i_s
        )
        {
        }

    protected:

        inline PreferenceManagerErrorPolicy_Null() {}
        inline ~PreferenceManagerErrorPolicy_Null() {}

    private:

        /* Unimplemented */
        PreferenceManagerErrorPolicy_Null
            ( const PreferenceManagerErrorPolicy_Null & );
        PreferenceManagerErrorPolicy_Null & operator=
            ( const PreferenceManagerErrorPolicy_Null & );

    };


};


/** @} */


#endif
