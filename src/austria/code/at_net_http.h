/*
 * This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 * Any unauthorized use of this file is prohibited and will be prosecuted
 * to the full extent of the law.
 */
#ifndef x_at_net_http_h_x
#define x_at_net_http_h_x

#include "at_net.h"
#include <string>
#include <sstream>

namespace at
{
    /** An HTTPAddress consists only of a URI. */
    class HTTPAddress : public NetAddress
    {
    public:
        std::string uri;

        HTTPAddress(const std::string &u) : uri(u) { }
        virtual ~HTTPAddress() {}
    };

    class HTTPVersionProperty : public NetProperty
    {
    public:
        int major;
        int minor;

        HTTPVersionProperty(int ma, int mi) { major = ma; minor = mi; }
        HTTPVersionProperty(const std::string&);
        virtual ~HTTPVersionProperty() {}
    };

    class HTTPURIProperty : public NetProperty
    {
    public:
        const std::string uri;

        HTTPURIProperty(const std::string &u) : uri(u) { }
        virtual ~HTTPURIProperty() {}
    };

    class HTTPStatusProperty : public NetProperty
    {
    public:
        int statusCode;
        std::string reasonPhrase;

        /** Sets the reason phrase according to the status code. */
        HTTPStatusProperty(int c);

        HTTPStatusProperty(int c, const std::string &ph) :
            statusCode(c), reasonPhrase(ph) { }

        virtual ~HTTPStatusProperty() {}
    };

    class HTTPHeaderProperty : public NetProperty
    {
    public:
        std::string name;
        std::string value;

        HTTPHeaderProperty(const std::string &n, const std::string &v) :
            name(n), value(v) { }
        HTTPHeaderProperty(const std::string &n, long long v) :
            name(n)

        {
            std::stringstream ss;
            ss << v;
            value = ss.str();
        }

        virtual ~HTTPHeaderProperty() {}
    };

    class HTTPMethodProperty : public NetProperty
    {
    public:
        std::string method;

        HTTPMethodProperty(const std::string &m) : method(m) { }
        virtual ~HTTPMethodProperty() {}
    };

    /**
     * HTTPError contains errors related to the HTTP protocol.
     */
    class HTTPError : public NetConnectionError
    {
    protected:
        HTTPError(const char *s) : NetConnectionError(s) { }

    public:
        static const HTTPError s_malformed_request;

        virtual ~HTTPError() {}
    };
} // namespace at

#endif // #ifndef x_at_net_http_h_x
