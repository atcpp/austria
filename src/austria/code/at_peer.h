/*
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 */
/* NOTICE: Do not use this header.  Use at_twin.h and friends instead. */

#ifndef x_at_peer_h_x
#define x_at_peer_h_x 1

namespace at
{
    /**
     * Peer attempts to ease the burden of programming two classes that
     * wish to interact with each other but neither of whom "owns" the
     * other.  It does this by making sure that both objects in a peering
     * are in sync (that is, each one thinks that the other is its peer)
     * and also making sure that the peering relationship is broken when
     * either of the peers is destroyed.
     *
     * To use it, subclass from it, specifying the type of class that you
     * wish to peer with and your own type as template arguments.  It will
     * provide others with the ability to tell you who your peer is and
     * provide you with the ability to find out who your peer is.
     *
     * This class is not thread-safe.
     */
    template<typename w_PeerType, typename w_YourClassType>
    class Peer
    {
    public:
        typedef w_PeerType PeerType;
        typedef Peer<w_YourClassType, w_PeerType> PeerBaseType;

        friend class PeerBaseType;

    public:
        /**
         * m_peer points to our peer or is null if no peer relationship
         * is in effect.
         */
        PeerType *m_peer;

    public:
        /**
         * The default Peer constructor sets the object's peer to null.
         */
        Peer() : m_peer(0) {}

        /**
         * The Peer destructor breaks the peering relationship, if one
         * exists.
         */
        virtual ~Peer()
        {
            this->setPeer(0);
        }

        /**
         * Set the peer to i_newPeer, replacing any existing peer.
         *
         * @param i_newPeer The new peer
         */
        void setPeer(PeerType *i_newPeer)
        {
            if (m_peer)
            {
                m_peer->peerChanged();
                m_peer->m_peer = 0;
            }
            if (i_newPeer && i_newPeer->m_peer)
            {
                i_newPeer->m_peer->peerChanged();
            }
            m_peer = i_newPeer;
            if (m_peer)
            {
                m_peer->m_peer = dynamic_cast<w_YourClassType*>(this);
                m_peer->peerChanged();
            }
        }

        /**
         * Return the peer.
         *
         * @return The peer
         */
        PeerType* peer()
        {
            return m_peer;
        }

    protected:
        /**
         * This method enables the subclasser to take note that the peering
         * has changed.  The default implementation does nothing.
         *
         * @param i_newPeer The new peer--equal to what peer() would return
         */
        virtual void peerChanged()
        {
        }
    };
} // namespace austria


#endif // #ifndef x_at_peer_h_x
