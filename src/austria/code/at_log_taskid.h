/**
 * \file at_log_taskid.h
 *
 * \author Bradley Austin
 *
 * at_log_taskid.h contains an extension to the Austria logging module to
 * support querying the TaskID from at::Task.
 *
 */

#ifndef x_at_log_taskid_h_x
#define x_at_log_taskid_h_x 1


#include "at_thread.h"


// Austria namespace
namespace at
{


    class LogManagerTaskIDPolicy_ATTask
    {

    public:

        typedef  Task::TaskID  t_taskid_type;

        inline static t_taskid_type GetTaskID()
        {
            return Task::GetSelfId();
        }

    protected:

        inline LogManagerTaskIDPolicy_ATTask() {}
        inline ~LogManagerTaskIDPolicy_ATTask() {}

    private:

        /* Unimplemented */
        LogManagerTaskIDPolicy_ATTask
            ( const LogManagerTaskIDPolicy_ATTask & );
        LogManagerTaskIDPolicy_ATTask & operator=
            ( const LogManagerTaskIDPolicy_ATTask & );

    };


}


#endif
