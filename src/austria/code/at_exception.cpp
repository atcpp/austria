//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
//
// at_url_parse.cpp
//
//

#include "at_exception.h"

// Austria namespace
namespace at
{

//
// Full_Exception::s_default_file contains the file name when the
// source locator is not set.

const char Full_Exception::s_default_file[] = "NotSet";

bool Exception::s_abort_on_assert = false;

}; // namespace

