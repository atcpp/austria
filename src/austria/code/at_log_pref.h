/**
 *  \file at_log_pref.h
 *
 *  \author Bradley Austin
 *
 */

#ifndef x_at_log_pref_h_x
#define x_at_log_pref_h_x 1


#include "at_preference.h"
#include "at_log.h"
#include "at_static_init.h"

#include <string>
#include <vector>
#include <map>


namespace at
{


    class LogLevel {};


    // ======== PreferenceType< LogLevel > ============================
    /**
     *  Specialization of the PreferenceType template for LogLevel
     *  Preferences.
     *
     */
    template< >
    struct PreferenceType< LogLevel >
    {

        typedef  int  t_value_type;

        typedef  std::vector< std::string >  t_external_type;

        static std::string Name()
        {
            return "loglevel";
        }

        typedef  std::map< std::string, int >  t_keyword_to_bits_map_type;

        struct InitKeywordToBitsMap
        {
            static void Initialize( t_keyword_to_bits_map_type & o_map )
            {
                o_map[ "TRACE" ] = AT_LOGLEVEL_TRACE;
                o_map[ "DEBUG" ] = AT_LOGLEVEL_DEBUG;
                o_map[ "INFO" ] = AT_LOGLEVEL_INFO;
                o_map[ "WARNING" ] = AT_LOGLEVEL_WARNING;
                o_map[ "ERROR" ] = AT_LOGLEVEL_ERROR;
                o_map[ "SEVERE" ] = AT_LOGLEVEL_SEVERE;

                o_map[ "TRACE_AND_ABOVE" ] =
                    AT_LOGLEVEL_TRACE |
                    AT_LOGLEVEL_DEBUG |
                    AT_LOGLEVEL_INFO |
                    AT_LOGLEVEL_WARNING |
                    AT_LOGLEVEL_ERROR |
                    AT_LOGLEVEL_SEVERE;

                o_map[ "DEBUG_AND_ABOVE" ] =
                    AT_LOGLEVEL_DEBUG |
                    AT_LOGLEVEL_INFO |
                    AT_LOGLEVEL_WARNING |
                    AT_LOGLEVEL_ERROR |
                    AT_LOGLEVEL_SEVERE;

                o_map[ "INFO_AND_ABOVE" ] =
                    AT_LOGLEVEL_INFO |
                    AT_LOGLEVEL_WARNING |
                    AT_LOGLEVEL_ERROR |
                    AT_LOGLEVEL_SEVERE;

                o_map[ "WARNING_AND_ABOVE" ] =
                    AT_LOGLEVEL_WARNING |
                    AT_LOGLEVEL_ERROR |
                    AT_LOGLEVEL_SEVERE;

                o_map[ "ERROR_AND_ABOVE" ] =
                    AT_LOGLEVEL_ERROR |
                    AT_LOGLEVEL_SEVERE;

                o_map[ "SEVERE_AND_ABOVE" ] =
                    AT_LOGLEVEL_SEVERE;

                o_map[ "ALL" ] =
                    AT_LOGLEVEL_TRACE |
                    AT_LOGLEVEL_DEBUG |
                    AT_LOGLEVEL_INFO |
                    AT_LOGLEVEL_WARNING |
                    AT_LOGLEVEL_ERROR |
                    AT_LOGLEVEL_SEVERE;

                o_map[ "OFF" ] = 0;

            }
        };

        static bool Import(
            const t_external_type &i_s,
            t_value_type &o_value
        )
        {
            static const Initialized<
                t_keyword_to_bits_map_type,
                InitKeywordToBitsMap
            > l_keyword_to_bits_map;

            int l_value = 0;
            for ( t_external_type::const_iterator
                      l_iter = i_s.begin(),
                      l_limit = i_s.end();
                  l_iter != l_limit;
                  ++l_iter
                )
            {
                t_keyword_to_bits_map_type::const_iterator l_map_iter =
                    l_keyword_to_bits_map.find( *l_iter );
                if ( l_map_iter != l_keyword_to_bits_map.end() )
                {
                    l_value |= l_map_iter->second;
                }
                else
                {
                    int l_i;
                    if ( ! PreferenceType< int >::Import( *l_iter, l_i ) )
                    {
                        return false;
                    }
                    l_value |= l_i;
                }
            }
            o_value = l_value;
            return true;
        }

        typedef
            std::map< int, std::string >
                t_bit_to_keyword_map_type;

        struct InitBitToKeywordMap
        {
            static void Initialize( t_bit_to_keyword_map_type & o_map )
            {
                o_map[ AT_LOGLEVEL_TRACE ] = "TRACE";
                o_map[ AT_LOGLEVEL_DEBUG ] = "DEBUG";
                o_map[ AT_LOGLEVEL_INFO ] = "INFO";
                o_map[ AT_LOGLEVEL_WARNING ] = "WARNING";
                o_map[ AT_LOGLEVEL_ERROR ] = "ERROR";
                o_map[ AT_LOGLEVEL_SEVERE ] = "SEVERE";
            }
        };

        static void Export(
            const t_value_type & i_value,
            t_external_type &o_s
        )
        {
            static const Initialized<
                t_bit_to_keyword_map_type,
                InitBitToKeywordMap
            > l_bit_to_keyword_map;

            o_s.clear();
            for ( t_bit_to_keyword_map_type::const_iterator
                      l_iter = l_bit_to_keyword_map.begin(),
                      l_limit = l_bit_to_keyword_map.end();
                  l_iter != l_limit;
                  ++l_iter
                )
            {
                if ( i_value & l_iter->first )
                {
                    o_s.push_back( l_iter->second );
                }
            }
        }

    };


    template<
        class w_pref_mgr_type
    >
    class LogWriterPrefMgrPolicy_ManageLifetimeManually
    {

    public:

        typedef  PtrView< w_pref_mgr_type * >  t_pref_mgr_ptr;

    private:

        t_pref_mgr_ptr m_pref_mgr;

    protected:

        inline LogWriterPrefMgrPolicy_ManageLifetimeManually()
          : m_pref_mgr()
        {
        }

        inline ~LogWriterPrefMgrPolicy_ManageLifetimeManually() {}

    public:

        inline t_pref_mgr_ptr PrefMgr()
        {
            return m_pref_mgr;
        }

        inline void SetPrefMgr( t_pref_mgr_ptr i_pref_mgr )
        {
            m_pref_mgr = i_pref_mgr;
        }

    };


    template<
        class w_inner_output_policy,
        class w_pref_mgr_policy
    >
    class LogWriterOutputPolicy_PreferenceControlledSwitch
      : public w_inner_output_policy,
        public w_pref_mgr_policy,
        public ThisInBaseInitializerHack<
            LogWriterOutputPolicy_PreferenceControlledSwitch<
                w_inner_output_policy,
                w_pref_mgr_policy
            >
        >
    {

    public:

        typedef
            typename w_inner_output_policy::t_ostream_type
                t_inner_ostream_type;

        typedef
            LogWriterOutputPolicy_Stream<
                LogWriterOutputPolicy_PreferenceControlledSwitch
            >
                t_ostream_type;

        typedef
            typename w_pref_mgr_policy::t_pref_mgr_ptr
                t_pref_mgr_ptr;

    private:

        t_ostream_type m_ostream;
        std::ostringstream m_buffer;
        bool m_initialized;
        std::string m_pref_name;
        std::string m_pref_context;

    protected:

        using w_pref_mgr_policy::PrefMgr;

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_PreferenceControlledSwitch
        >::This;

        inline LogWriterOutputPolicy_PreferenceControlledSwitch()
          : m_ostream( This() ),
            m_initialized( false )
        {
        }

        inline ~LogWriterOutputPolicy_PreferenceControlledSwitch() {}

    public:

        template< typename w_type >
        inline void Write( const w_type & i_value )
        {
            m_buffer << i_value;
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        void Flush()
        {
            std::string l_string = m_buffer.str();
            m_buffer.str( std::string() );

            if ( m_initialized )
            {
                t_pref_mgr_ptr l_pref_mgr( PrefMgr() );
                if ( l_pref_mgr )
                {
                    Preference< bool > l_switch( m_pref_name, false, m_pref_context, l_pref_mgr );
                    if ( l_switch )
                    {
                        w_inner_output_policy::OStream() << l_string;
                        w_inner_output_policy::Flush();
                    }
                }
            }
        }

        void SetPrefNameAndContext(
            const std::string & i_name,
            const std::string & i_context
        )
        {
            m_pref_name = i_name;
            m_pref_context = i_context;
            m_initialized = true;
        }

        inline t_inner_ostream_type & InnerOStream()
        {
            return w_inner_output_policy::OStream();
        }

    private:

        /* Unimplemented. */
        LogWriterOutputPolicy_PreferenceControlledSwitch(
            const LogWriterOutputPolicy_PreferenceControlledSwitch &
        );
        LogWriterOutputPolicy_PreferenceControlledSwitch & operator=(
            const LogWriterOutputPolicy_PreferenceControlledSwitch &
        );

    };


}

#endif
