/**
 * /file at_crypto_ossl.cpp
 *
 * 
 */

#include "at_crypto.h"
#include "at_id_generator.h"
#include "at_factory.h"

#include "openssl/rsa.h"
#include "openssl/engine.h"
#include "openssl/rand.h"
#include "openssl/bio.h"
#include "openssl/pem.h"
#include "openssl/err.h"

#include <cstring>

namespace at_ossl
{
// at_ossl namespace


// ======== OsslError =================================================
/**
 * OsslError fetches openssl's error
 *
 *
 * @param i_alt An alternative error if Openssl's error returns 0
 * @return nothing
 */

const char * OsslError(
    const char  * i_alt
) {
    const char * l_err = ERR_reason_error_string( ERR_get_error() );

    if ( l_err )
    {
        return l_err;
    }

    return i_alt;
}



/**
 * This file is the only file that should contain the openssl crypto
 * implementations. This is all private.  In theory, any desired
 * features should be added to the main at_crypto.h header and
 * translated in this file.
 */

class BioMethod;

// ======== BioMethodWrapperContext ===============================
/**
 * BioMethodWrapperContext is used to manage the read position etc of
 * a either the BioMethodWrapperImpl<*> classes. The current
 * implementation wraps ony at::Buffer or a std::string, in theory
 * this can be extended to std::istream and std::ostream or other
 * containers.
 */

class BioMethodWrapperContext
{
    public:

    BioMethodWrapperContext( BioMethod * i_meth )
      : m_meth( i_meth ),
        m_readpos() // initializes to 0
    {
    }

    at::Pointer<BioMethod *>    m_meth;
    int                         m_readpos;
};


// ======== BioMethod =================================================
/**
 * BioMethod is a base class for wrapping BIO functions.  This is the
 * C++ class that gets wrapped in the BIO functions.  Hence, the BIO
 * methods get translated through these.
 */

class BioMethod
{
    public:

    virtual ~BioMethod() {};

    /**
     * method_write method_read method_gets method_ctrl are obvious
     * extensions of the BIO calls.
     */
    virtual int  method_write( BIO *h, const char *buf,int num, BioMethodWrapperContext * o_context ) = 0;
    virtual int  method_read( BIO *h, char *buf, int size, BioMethodWrapperContext * o_context ) = 0;
    virtual int  method_gets( BIO *h, char *str, int size, BioMethodWrapperContext * o_context ) = 0;
    virtual long method_ctrl( BIO *h, int cmd, long arg1, void *arg2, BioMethodWrapperContext * o_context ) = 0;

    /**
     * method_get_value_pointer returns a pointer to the object contained
     * in objects derived from this class.  Type safety is assured through
     * at::Any<>.  This pointer can be used to both read and write the
     * pointer.
     */
    virtual void method_get_value_pointer( at::Any<> & l_any ) = 0;

    /**
     * method_num_pending and method_is_eof are very similar to the 
     * BIO methods of similar names.
     */
    virtual int method_num_pending( BioMethodWrapperContext * o_context ) = 0;

    virtual int method_is_eof( BioMethodWrapperContext * o_context ) = 0;
};


// ======== BioMethodWrapper ==========================================
/**
 * This class contains the actual methods added to the BIO_METHOD
 * function tables. These will extract the BioMethodWrapperContext
 * and allow access directly to the BioMethod class within.
 * These methods are non specific to the type being wrapped, however
 * BioMethodWrapperImpl inherits this class and is a template that
 * is specific to the wrapped type.
 *
 */

class BioMethodWrapper
{

    public:
    static int austria_free(BIO *a)
	{
        if (a == NULL) return(0);
        
        delete ( (BioMethodWrapperContext *)a->ptr );

        return(1);
	}
	
    static int austria_read(BIO *b, char *out, int outl)
    {
        if (out == NULL) return(0);
        
        BioMethodWrapperContext * ctx=(BioMethodWrapperContext *)b->ptr;
    
        if ( ctx == NULL ) return(0);
        
        // Attempt to read from the buffer
        return ctx->m_meth->method_read( b, out, outl, ctx );
    }

    static int austria_write(BIO *b, const char *in, int inl)
    {    
        if ((in == NULL) || (inl <= 0)) return(0);
        BioMethodWrapperContext * ctx=(BioMethodWrapperContext *)b->ptr;

        if ( ctx == NULL ) return(0);
        
        return ctx->m_meth->method_write( b, in, inl, ctx );
    }

    enum {
        BIO_C_WRAP_GET = 300,
    };
    
    static long austria_ctrl( BIO *b, int cmd, long num, void * ptr )
    {
        BioMethodWrapperContext * ctx = (BioMethodWrapperContext *)b->ptr;
    
        if ( ctx == NULL ) return(0);
        
        switch (cmd)
        {
            case BIO_CTRL_EOF:
            {
                return ctx->m_meth->method_is_eof( ctx );
            }
            case BIO_C_WRAP_GET:
            {
                if (ptr != NULL)
                {
                    ctx->m_meth->method_get_value_pointer( * ( at::Any<> * ) ptr );
                }
                return 0;
            }
            
            case BIO_CTRL_GET_CLOSE:
            {
                return (long)b->shutdown;
            }

            case BIO_CTRL_SET_CLOSE:
            {
                b->shutdown=(int)num;
                return 0;
            }
        
            case BIO_CTRL_WPENDING:
            {
                return ctx->m_meth->method_num_pending( ctx );
            }
            case BIO_CTRL_PENDING:
            {
                return ctx->m_meth->method_num_pending( ctx );
            }
            
            case BIO_CTRL_DUP:
            case BIO_CTRL_FLUSH:
            {
                return 1;
            }
            
            case BIO_CTRL_PUSH:
            case BIO_CTRL_POP:
            {
                return 0;
            }
            
            case BIO_CTRL_RESET:
            {
                ctx->m_readpos = 0;
                break;
            }

            default :
            {
                return ctx->m_meth->method_ctrl( b, cmd, num, ptr, ctx );
            }
        }

        return 0;
    }

    static long austria_callback_ctrl(BIO *b, int cmd, bio_info_cb *fp)
	{
        return(0);
	}

    static int austria_gets(BIO *b, char *buf, int size)
	{
    
        BioMethodWrapperContext * ctx=(BioMethodWrapperContext *)b->ptr;
        
        if ( ctx == NULL ) return(0);

        return ctx->m_meth->method_gets( b, buf, size, ctx );
    }

    static int austria_puts(BIO *b, const char *str)
    {
        return austria_write(b,str,strlen(str));
    }

}; // class BioMethodWrapper



// ======== BioMethodWrapperImpl ======================================
/**
 * BioMethodWrapperImpl implements the specialized components of the
 * BioMethodWrapper wrapper for each type of wrapper that is used.
 */

template <typename w_traits>
class BioMethodWrapperImpl
  : public BioMethodWrapper
{
    public:

    typedef w_traits                        t_traits;

    typedef typename w_traits::value_type   value_type;

    static const BIO_METHOD s_methods_buffer;

    static BIO_METHOD * BIO_methods()
    {
        return const_cast<BIO_METHOD *>(&s_methods_buffer);
    }

    static int austria_new( BIO *bi )
    {
        BioMethodWrapperContext * ctx = t_traits::NewBioMethodWrapperContext();
    
        if (ctx == NULL) return(0);
    
        bi->init=1;
        bi->ptr=(char*)(ctx);
        bi->flags=0;
        return(1);
    }


    // ======== GetVar ================================================
    /**
     * GetVar is a static method that takes any BIO structure and 
     * accesses the wrapped object in a type safe way.
     *
     * GetVar throws if the returned object is not of the type
     * expected.
     *
     * Calls to GetVar are relatively expensive.  Call GetVar once
     * and cache the result.
     *
     * @param i_bio The bio object.
     * @return A reference to the internal object.
     */

    static value_type & GetVar( BIO * i_bio )
    {
        at::Any<>       l_any;
        
        BIO_ctrl( i_bio, BIO_C_WRAP_GET, 0, (char *) & l_any );

        // this will throw if the types are wrong - be careful
        value_type * l_val = l_any.Refer();

        return * l_val;
    }

};

// ======== s_methods_buffer ==========================================

template <typename w_traits>
const BIO_METHOD BioMethodWrapperImpl<w_traits>::s_methods_buffer =
{
    (t_traits::s_bio_type|0x0200),
    t_traits::s_bio_name,
    BioMethodWrapperImpl<w_traits>::austria_write,
    BioMethodWrapperImpl<w_traits>::austria_read,
    BioMethodWrapperImpl<w_traits>::austria_puts,
    BioMethodWrapperImpl<w_traits>::austria_gets,
    BioMethodWrapperImpl<w_traits>::austria_ctrl,
    BioMethodWrapperImpl<w_traits>::austria_new,
    BioMethodWrapperImpl<w_traits>::austria_free,
    BioMethodWrapperImpl<w_traits>::austria_callback_ctrl,
};



// ======== BioMethodStdString ========================================
/**
 * BioMethodStdString contains the read/write methods to/from a std::string
 * needed by BioMethod.
 */

class BioMethodStdString
  : public BioMethod
{
    public:
    
    typedef std::string                 value_type;

    std::string                         m_string;

    BioMethodStdString()
    {
    }

    int  method_write( BIO *h, const char *buf,int num, BioMethodWrapperContext * o_context )
    {
        m_string.append( buf, buf + num );
        return num;
    }
    
    int  method_read( BIO *h, char *buf, int size, BioMethodWrapperContext * o_context )
    {
        BIO_clear_retry_flags(h);
        
        int l_length = m_string.length();
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            if ( h->num )
            {
                BIO_set_retry_read(h);
                return h->num;
            }
        }

        if ( l_left < size )
        {
            size = l_left;
        }
        
        std::memcpy( buf, m_string.data() + o_context->m_readpos, size );
        o_context->m_readpos += size;
        return size;
    }
    
    int  method_gets( BIO *h, char *str, int size, BioMethodWrapperContext * o_context )
    {
        // gets is used quite alot. The exact semantics of method_gets
        // is not well documented.  This tries to perform in exactly
        // the same way as the similar gets types in openssl's bio
        // methods.
        
        BIO_clear_retry_flags(h);
        if ( size <= 0 )
        {
            return 0;
        }

        int l_length = m_string.length();
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            * str = 0;
            return 0;
        }

        std::string::const_iterator l_itr = m_string.begin() + o_context->m_readpos;

        char * const l_ostr = str;
        if ( (size -1) < l_left )
        {
            l_left = size -1;
        }

        char l_ch;
        while ( l_left )
        {
            l_ch = ( * ( str ++ ) = * ( l_itr ++ ) );
            -- l_left;
            
            if ( '\n' == l_ch )
            {
                goto found_eol;
            }
        }

        BIO_set_retry_read(h);
found_eol:
        o_context->m_readpos = l_itr - m_string.begin();
        int l_read_size = str - l_ostr;
        * ( str ++ ) = 0;

        return l_read_size;
                
    }

    long method_ctrl( BIO *h, int cmd, long arg1, void *arg2, BioMethodWrapperContext * o_context )
    {
        return 0;
    }

    void method_get_value_pointer( at::Any<> & l_any )
    {
        // put the address of the string into the any pointer
        l_any = at::ToAny( & m_string );
    }
    
    virtual int method_num_pending( BioMethodWrapperContext * o_context )
    {
        int l_length = m_string.length();
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            return 0;
        }

        return l_left;
        
    }

    virtual int method_is_eof( BioMethodWrapperContext * o_context )
    {
        int l_length = m_string.length();
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            return 1;
        }

        return 0;
    }
    
    private:

    BioMethodStdString & operator=( const BioMethodStdString & );
    BioMethodStdString ( const BioMethodStdString & );
    
};


// ======== BioMethodStdStringTraits ==================================
/**
 * This defines BioMethodWrapperImpl traits for reading/writing
 * to a std::string.
 *
 */

class BioMethodStdStringTraits
{
    public:

    typedef BioMethodStdString          t_bio_type;
    typedef t_bio_type::value_type      value_type;

    // s_bio_type needs to be unique among all types.
    static const int s_bio_type = 39;
    static char s_bio_name[];
    

    // ======== NewBioMethodWrapperContext ============================
    /**
     * Creates a new context for this particular traits type.
     *
     *
     * @return A pointer to a BioMethodWrapperContext
     */

    static BioMethodWrapperContext * NewBioMethodWrapperContext()
    {
        return new BioMethodWrapperContext( new BioMethodStdString() );
    }
    
};

char BioMethodStdStringTraits::s_bio_name[] = "stdstring";


// ======== BioMethodAtBuffer =========================================
/**
 * BioMethodAtBuffer wraps an at::Buffer in the same way as
 * the BioMethodStdString class above.
 *
 */

class BioMethodAtBuffer
  : public BioMethod
{

    public:
    
    typedef at::Ptr<at::Buffer *>       value_type;

    value_type                          m_string;

    BioMethodAtBuffer()
    {
    }

    int  method_write( BIO *h, const char *buf,int num, BioMethodWrapperContext * o_context )
    {
        m_string->Append( buf, num );
        return num;
    }
    
    int  method_read( BIO *h, char *buf, int size, BioMethodWrapperContext * o_context )
    {
        BIO_clear_retry_flags(h);
        
        const at::Buffer::t_Region & l_breg = m_string->Region();
        
        int l_length = l_breg.m_allocated;
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            if ( h->num )
            {
                BIO_set_retry_read(h);
                return h->num;
            }
        }

        if ( l_left < size )
        {
            size = l_left;
        }
        
        std::memcpy( buf, l_breg.m_mem + o_context->m_readpos, size );
        o_context->m_readpos += size;
        return size;
    }
    
    int  method_gets( BIO *h, char *str, int size, BioMethodWrapperContext * o_context )
    {
        BIO_clear_retry_flags(h);
        if ( size <= 0 )
        {
            return 0;
        }

        const at::Buffer::t_Region & l_breg = m_string->Region();
        
        int l_length = l_breg.m_allocated;
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            * str = 0;
            return 0;
        }

        const char * l_itr = l_breg.m_mem + o_context->m_readpos;

        char * const l_ostr = str;
        if ( (size -1) < l_left )
        {
            l_left = size -1;
        }

        char l_ch;
        while ( l_left )
        {
            l_ch = ( * ( str ++ ) = * ( l_itr ++ ) );
            -- l_left;
            
            if ( '\n' == l_ch )
            {
                goto found_eol;
            }
        }

        BIO_set_retry_read(h);
found_eol:
        o_context->m_readpos = l_itr - l_breg.m_mem;
        int l_read_size = str - l_ostr;
        * ( str ++ ) = 0;

        return l_read_size;
    }

    long method_ctrl( BIO *h, int cmd, long arg1, void *arg2, BioMethodWrapperContext * o_context )
    {
        return 0;
    }

    void method_get_value_pointer( at::Any<> & l_any )
    {
        // put the address of the string into the any pointer
        l_any = at::ToAny( & m_string );
    }
    
    virtual int method_num_pending( BioMethodWrapperContext * o_context )
    {
        const at::Buffer::t_Region & l_breg = m_string->Region();
        
        int l_length = l_breg.m_allocated;
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            return 0;
        }

        return l_left;
        
    }

    virtual int method_is_eof( BioMethodWrapperContext * o_context )
    {
        const at::Buffer::t_Region & l_breg = m_string->Region();
        
        int l_length = l_breg.m_allocated;
        int l_left = l_length - o_context->m_readpos;

        if ( l_left <= 0 )
        {
            return 1;
        }

        return 0;
    }
    
    private:

    BioMethodAtBuffer & operator=( const BioMethodAtBuffer & );
    BioMethodAtBuffer ( const BioMethodAtBuffer & );
    
};


// ======== BioMethodAtBufferTraits ===================================
/**
 * BioMethodAtBufferTraits is the traits class for the at::Buffer
 * version of the BioMethodWrapperImpl wrapper.
 */

class BioMethodAtBufferTraits
{
    public:


    typedef BioMethodAtBuffer           t_bio_type;
    typedef t_bio_type::value_type      value_type;
    
    static const int s_bio_type = 40;
    static char s_bio_name[];
    

    // ======== NewBioMethodWrapperContext ============================
    /**
     * Creates a new context for this particular traits type.
     *
     *
     * @return A pointer to a BioMethodWrapperContext
     */

    static BioMethodWrapperContext * NewBioMethodWrapperContext()
    {
        return new BioMethodWrapperContext( new BioMethodAtBuffer() );
    }
    
};

char BioMethodAtBufferTraits::s_bio_name[] = "atbuffer";


// ======== OsslWrap ===================================================
/**
 * This structue is used to manage the openssl RSA, BIO or other
 * openssl structures.  Many of the openssl types have a "new"
 * method - these only return pointers to openssl types and
 * have an associated "free" function.  OsslWrap wraps these in a
 * reference counted container.
 *
 */

template <typename w_traits>
class OsslWrap
  : public at::PtrTarget_MT
{
    public:

    typedef typename w_traits::t_type   t_type;

    /**
     * OsslWrap constructor
     *
     */
    OsslWrap( t_type * i_val )
      : m_val( i_val )
    {
    }

    /**
     * OsslWrap default constructor
     */
    OsslWrap()
      : m_val()
    {
    }

    ~OsslWrap()
    {
        if ( m_val )
        {
            w_traits::FreeMethod( m_val );
        }
    }

    /**
     * m_val is the RSA pointer
     */
    t_type                  * m_val;

    private:

    // don't allow copies
    OsslWrap & operator=( const OsslWrap & );
    OsslWrap( const OsslWrap & );
};


// ======== OsslCtxWrap ===============================================
/**
 * OsslCtxWrap is for wrapping any object that has initializes
 * and unintializers. In particular the EVP_CIPHER_CTX.
 * However, this could be used to do any object that must
 * be initialized and "finalized" once allocated.
 */

template <typename w_traits>
class OsslCtxWrap
  : public w_traits::t_Base
{
    public:

    typedef w_traits                            t_Traits;
    typedef typename w_traits::t_Context        t_Context;
    typedef typename w_traits::t_Base           t_Base;

    /**
     * m_context is the POD context that has defined initializer
     * finalizers
     */
    t_Context                                   m_context;

    /**
     * OsslCtxWrap constructor
     *
     */
    OsslCtxWrap()
      : m_context() // clears out the bits - may be overkill
    {
        t_Traits::Initializer( m_context );
    }

    ~OsslCtxWrap()
    {
        t_Traits::Finalizer( m_context );
    }


    // ======== ReInitialize ==========================================
    /**
     * ReInitialize performs a Finalizer/Initializer sequence to
     * cleanup and re-use the object
     *
     * @return nothing
     */

    void ReInitialize()
    {
        t_Traits::Finalizer( m_context );
        m_context = t_Context(); // clears out bits - may be overkill
        t_Traits::Initializer( m_context );
    }    

    private:
    OsslCtxWrap( const OsslCtxWrap & );
    OsslCtxWrap & operator=( const OsslCtxWrap & );

};


// ======== OsslBIO ===================================================
/**
 * OsslBIO is the traits to manage the BIO openssl type
 *
 */

class OsslBIO
{
    public:

    typedef BIO             t_type;

    static void FreeMethod( t_type * i_rsa )
    {
        BIO_free( i_rsa );
    }
};


// ======== OsslRSA ===================================================
/**
 * OsslRSA is the traits to manage the RSA openssl type
 *
 */

class OsslRSA
{
    public:

    typedef RSA             t_type;

    static void FreeMethod( t_type * i_rsa )
    {
        RSA_free( i_rsa );
    }
};


// ======== BufferStdStringWrapper ====================================
/**
 * BufferStdStringWrapper is to provide a mechanism to hide a std::string
 * around an at::Buffer.  This is simply to make it so we have one
 * implementation of Cipher_OsslTraits_RSA's output type.  This is not
 * a generic wrapper.
 */

class BufferWrapper
  : public at::PtrTarget_MT
{
    public:

    // ======== GetBuffer =============================================
    /**
     * GetBuffer gets an interface to an at::Buffer
     *
     *
     * @return An at::Buffer pointer
     */

    virtual at::PtrView< at::Buffer * > GetBuffer() = 0;


    // ======== Flush =================================================
    /**
     * Flush performs the flush from the internal buffers to external
     * buffers.  This is a nop for at::Buffer.
     *
     * @return nothing
     */

    virtual void	Flush() = 0;
    
};

// ======== BufferWrapper_StdString ===================================
/**
 * BufferWrapper_StdString is a wrapper for std::string to make it
 * conform at::Buffer and std::string conform to a single interface.
 * Unfortunately, since std::string does not guarentee contiguous
 * memory, bytes need to be written to a buffer and then followed
 * by copying to the string.  To avoid double copies use the
 * at::Buffer methods instead.
 */

class BufferWrapper_StdString
  : public BufferWrapper
{
    public:
    
    /**
     * The std::string will recieve all the data.
     */
    std::string                     & m_str;

    /**
     * m_buffer is the buffer to use to interface with the
     * cipher code.
     */
    at::Ptr<at::Buffer *>             m_buffer;

    // ======== BufferWrapper_StdString ===============================
    /**
     *	BufferWrapper_StdString constructor takes an std::string
     *
     * @param o_str  The string being mirrored
     * @return nothing
     */

    BufferWrapper_StdString(
        std::string         & o_str
    )
      : m_str( o_str ),
        m_buffer( at::NewBuffer() )
    {
    }

    // ======== GetBuffer =============================================
    /**
     * GetBuffer gets an interface to an at::Buffer
     *
     *
     * @return An at::Buffer pointer
     */

    at::PtrView< at::Buffer * > GetBuffer()
    {
        return m_buffer;
    }

    // ======== Flush =================================================
    /**
     * Flush performs the flush from the at::buffer to the std::string.
     * This should be called once we're done with the buffer.
     *
     * @return nothing
     */

    virtual void Flush()
    {
        const at::Buffer::t_Region    & l_region = m_buffer->Region();
        m_str.append(
            l_region.m_mem,
            l_region.m_allocated
        );
        m_buffer->SetAllocated(0);
    }

};


// ======== BufferWrapper_AtBuffer ====================================
/**
 * BufferWrapper for at::Buffer
 *
 */

class BufferWrapper_AtBuffer
  : public BufferWrapper
{
    public:

    /**
     * m_buffer is the buffer to use to interface with the
     * cipher code.
     */
    at::Ptr<at::Buffer *>             m_buffer;

    // ======== BufferWrapper_AtBuffer ===============================
    /**
     *	BufferWrapper_AtBuffer constructor takes a buffer parameter
     *
     * @param o_buffer The buffer
     * @return nothing
     */

    BufferWrapper_AtBuffer(
        at::PtrDelegate<at::Buffer *> o_buffer
    )
      : m_buffer( o_buffer )
    {
    }

    // ======== GetBuffer =============================================
    /**
     * GetBuffer gets an interface to an at::Buffer
     *
     *
     * @return An at::Buffer pointer
     */

    at::PtrView< at::Buffer * > GetBuffer()
    {
        return m_buffer;
    }

    // ======== Flush =================================================

    virtual void Flush()
    {
    }
};



// ======== OsslRandomSeeder ==========================================
/**
 * OsslRandomSeeder performs a seeding of the random number generator
 * for openssl
 */

class OsslRandomSeeder
{
    public:

    static OsslRandomSeeder & Seeder()
    {
        static OsslRandomSeeder    l_seeder;

        return l_seeder;
    }

    private:
    /**
     * Constructor 
     *
     */
    OsslRandomSeeder()
    {
        do
        {
            std::string l_randbits = at::BinaryRandomSequence( 16 );
            
            RAND_seed( l_randbits.data(), l_randbits.length() );
            
        } while ( RAND_status() != 1 );
    }

    ~OsslRandomSeeder()
    {
        // This should clean up remaining cached data in openssl
        // so we can do leak checks without noise.
        // CRYPTO_cleanup_all_ex_data(); remove for now
    }
};


// ======== Cipher_OsslTraits_RSA_Pub =================================
/**
 * This is the RSA public cipher traits - it is an encapsulation of
 * the openssl cipher functions that operate on public keys.
 */

class Cipher_OsslTraits_RSA_Pub
{
    public:

    static int OSSL_Encrypt(
        int                 flen,
        unsigned char     * from,
        unsigned char     * to,
        RSA               * rsa,
        int                 padding
    ) {
        return RSA_public_encrypt( flen, from, to, rsa, padding );
    }
        
    static int OSSL_Decrypt(
        int                 flen,
        unsigned char     * from,
        unsigned char     * to,
        RSA               * rsa,
        int                 padding
    ) {
        return RSA_public_decrypt( flen, from, to, rsa, padding );
    }
        
    static RSA * OSSL_Readkey(
        BIO              * bp,
        RSA             ** x,
        pem_password_cb  * cb,
        void             * u
    ) {
        return PEM_read_bio_RSAPublicKey( bp, x, cb, u );
    }
    
    static const char                       s_cipher_id[];
    static const char                       s_cipher_id2[];
};

const char Cipher_OsslTraits_RSA_Pub::s_cipher_id[] = "RSA_PUB";
const char Cipher_OsslTraits_RSA_Pub::s_cipher_id2[] = "RSAOSSL_PUB";

// ======== Cipher_OsslTraits_RSA_Prv =================================
/**
 * This is the RSA private cipher traits - it is an encapsulation of
 * the openssl cipher functions that operate on private keys.
 *
 */

class Cipher_OsslTraits_RSA_Prv
{
    public:

    static int OSSL_Encrypt(
        int                 flen,
        unsigned char     * from,
        unsigned char     * to,
        RSA               * rsa,
        int                 padding
    ) {
        return RSA_private_encrypt( flen, from, to, rsa, padding );
    }
        
    static int OSSL_Decrypt(
        int                 flen,
        unsigned char     * from,
        unsigned char     * to,
        RSA               * rsa,
        int                 padding
    ) {
        return RSA_private_decrypt( flen, from, to, rsa, padding );
    }
        
    static RSA * OSSL_Readkey(
        BIO              * bp,
        RSA             ** x,
        pem_password_cb  * cb,
        void             * u
    ) {
        return PEM_read_bio_RSAPrivateKey( bp, x, cb, u );
    }
        
    static const char                       s_cipher_id[];
    static const char                       s_cipher_id2[];
};

const char Cipher_OsslTraits_RSA_Prv::s_cipher_id[] = "RSA_PRV";
const char Cipher_OsslTraits_RSA_Prv::s_cipher_id2[] = "RSAOSSL_PRV";

class CipherPublicKey_Ossl_RSA;

// ======== Cipher_OsslTraits =========================================
/**
 * Cipher_OsslTraits_RSA_Pub is the public key encrypter traits.
 * - in theory this could be used for other ossl PKCS ciphers
 */

template <typename w_pub_or_prv>
class Cipher_OsslTraits_RSA
{
    public:

    typedef OsslWrap<OsslRSA>               t_KeyTypeWrapper;
    typedef at::Ptr<t_KeyTypeWrapper *>     t_ContextValue;
    typedef at::PtrView<t_KeyTypeWrapper *> t_ContextParameter;
    typedef at::Buffer::t_ConstRegion       t_InputType;
    typedef at::Ptr<BufferWrapper *>        t_OutputType;
    typedef at::PtrView<BufferWrapper *>    t_OutputParamType;
    typedef int                             t_Parameters;
    typedef CipherPublicKey_Ossl_RSA        t_KeyGenerator;
    typedef w_pub_or_prv                    t_pub_or_prv_traits;

    const static t_Parameters s_params_default = RSA_PKCS1_PADDING;

    static void SetOutput(
        t_OutputType        & o_output,
        std::string         & o_str
    ) {
        o_output = new BufferWrapper_StdString( o_str );
    }

    static void SetOutput(
        t_OutputType                    & o_output,
        at::PtrDelegate<at::Buffer *>     o_buffer
    ) {
        o_output = new BufferWrapper_AtBuffer( o_buffer );
    }

    static void ReadKey(
        at::PtrView<OsslWrap<OsslBIO> *>    i_bio,
        t_ContextValue                    & o_key
    )
    {
        at::PtrDelegate<t_KeyTypeWrapper *> l_key = new t_KeyTypeWrapper(
            w_pub_or_prv::OSSL_Readkey( i_bio->m_val, 0, 0, 0 )
        );

        // make sure we have a key
        if ( ! l_key->m_val )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadKey,
                OsslError( "OSSL_Readkey failed" )
            );
        }

        o_key = l_key;
    }

    static void DoScramble(
        t_ContextParameter                    i_key,
        const t_InputType                   & i_input,
        t_OutputParamType                     o_output,
        const t_Parameters                  & i_params
    )
    {
                
        const at::SizeMem l_len = RSA_size( i_key->m_val );

        at::PtrView< at::Buffer * > l_woutput = o_output->GetBuffer();
        
        char * l_ptr = l_woutput->IncrementalAllocate( l_len );
        
        int l_result = w_pub_or_prv::OSSL_Encrypt(
            i_input.m_allocated,
            (unsigned char *) (i_input.m_mem),
            (unsigned char *) l_ptr,
            i_key->m_val,
            i_params
        );

        if ( l_result == -1 )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadCipherParameters,
                OsslError( "openssl Encrypt failed" )
            );
        }

        AT_Assert( l_len == l_result );

        o_output->Flush();
    }

    static void DoUnscramble(
        t_ContextParameter                    i_key,
        const t_InputType                   & i_input,
        t_OutputParamType                     o_output,
        const t_Parameters                  & i_params
    )
    {
        
        const at::SizeMem l_len = RSA_size( i_key->m_val );

        // RSA requires an exact number of input bits.
        if ( i_input.m_allocated != l_len )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadInputData,
                "Input data length is incorrect"
            );
        }

        at::PtrView< at::Buffer * > l_woutput = o_output->GetBuffer();
        
        // make sure there is enough space in the output buffer.
        const at::Buffer::t_Region & l_output = l_woutput->RegionExtend( l_len, 0 );

        int l_result = w_pub_or_prv::OSSL_Decrypt(
            i_input.m_allocated,
            (unsigned char *) (i_input.m_mem),
            (unsigned char *) (l_output.m_mem + l_output.m_allocated),
            i_key->m_val,
            i_params
        );

        if ( l_result == -1 )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadCipherParameters,
                OsslError( "Decrypt failed" )
            );
        }

        // Put the allocated chunk into the string (if it is a std::string
        l_woutput->SetAllocated( l_output.m_allocated + l_result );
        
        o_output->Flush();
    }

    
    static const char * s_cipher_id;
    static const char * s_cipher_id2;
};

template <typename w_pub_or_prv>
const char * Cipher_OsslTraits_RSA<w_pub_or_prv>::s_cipher_id = w_pub_or_prv::s_cipher_id;
template <typename w_pub_or_prv>
const char * Cipher_OsslTraits_RSA<w_pub_or_prv>::s_cipher_id2 = w_pub_or_prv::s_cipher_id2;

// ======== Cipher_Ossl ===============================================
/**
 * Cipher_Ossl_RSA is a template class that can be used to create
 * various kinds of ciphers.
 */

template <typename w_rsa_traits>
class Cipher_Ossl
  : public at::Cipher
{
    public:

    typedef typename w_rsa_traits::t_KeyTypeWrapper     t_KeyTypeWrapper;
    typedef typename w_rsa_traits::t_ContextValue       t_ContextValue;
    typedef typename w_rsa_traits::t_ContextParameter   t_ContextParameter;
    typedef typename w_rsa_traits::t_Parameters         t_Parameters;
    typedef typename w_rsa_traits::t_OutputType         t_OutputType;
    typedef typename w_rsa_traits::t_OutputParamType    t_OutputParamType;
    typedef typename w_rsa_traits::t_InputType          t_InputType;
    typedef typename w_rsa_traits::t_KeyGenerator       t_KeyGenerator;
    typedef typename w_rsa_traits::t_pub_or_prv_traits  t_pub_or_prv_traits;

    typedef void ( * t_cipher_func)(
        t_ContextParameter                    i_key,
        const t_InputType                   & i_input,
        t_OutputParamType                     o_output,
        const t_Parameters                  & i_params
    );

    /**
     * m_key is the RSA key to use.
     */
    t_ContextValue                          m_key;

    /**
     * m_output contains the location of the result of the
     * encryption.
     */
    t_OutputType                            m_output;

    /**
     * m_parameters are parameters specific to the cipher
     */
    t_Parameters                            m_parameters;

    /**
     * cipher mode
     */
    t_cipher_func                           m_func;


    Cipher_Ossl()
      : m_parameters( w_rsa_traits::s_params_default ),
        m_func( & w_rsa_traits::DoScramble )
    {
    }

    Cipher_Ossl( at::PtrView<t_KeyTypeWrapper *> i_key )
      : m_key( i_key ),
        m_parameters( w_rsa_traits::s_params_default ),
        m_func( & w_rsa_traits::DoScramble )
    {
    }

    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this Cipher.
     *
     *
     * @param i_property is the requested property
     * @return A CipherProperty "any" type
     */

    at::CipherProperty GetProperty(
        Property            i_property
    ) {

        switch ( i_property )
        {
            case IsSymmetric :
            {
                return at::ToAny( bool( false ) );
            }
            case IsPublic :
            {
                return at::ToAny( bool( true ) );
            }
            case KeyGenerator :
            {
                // PtrDelegate gets converted to a Ptr in Any types.
                return at::ToAny(
                    at::PtrDelegate<at::CipherPublicKey*>( new t_KeyGenerator )
                );
            }
            case PublicKey :
            {
                return at::ToAny(
                    t_KeyGenerator::GetPublicKeyFromPrivatekey( m_key )
                );
            }
        }
        
        return at::s_no_property;
    }
    
    // ======== SetProperty ===========================================
    /**
     * SetProperty can set a property on the key generator.
     *
     * @param i_property The requested property identifer
     * @param o_value The at::Any object to write the value of the requested property
     * @return True if the property was set
     */

    virtual bool SetProperty(
        Property                      i_property,
        at::CipherProperty          & i_value
    ) {
        return false;
    }

    // ======== SetCipherParameters =======================================
    /**
     * SetCipherParameters is used to add extra information about this key.
     * For example : SetCipherParameters( "RSA:512:65537" ) will set the key
     * to a 512 bit key with an exponent of 65537.
     *
     * @param i_key_params The key parameters
     * @return nothing
     */

    virtual void	SetCipherParameters(
        const std::string           & i_key_params
    ) {
    }


    // ======== SetFunc ===============================================
    /**
     * SetFunc will set the cipher function to either encrypt
     * or decrypt based on mode.
     *
     * @param i_mode The cipher mode being requested
     * @return nothing
     */

    void SetFunc(
        Mode                  i_mode
    ) {
        switch ( i_mode )
        {
            case Scramble :
            {
                m_func = & w_rsa_traits::DoScramble;
                break;
            }
            case Unscramble :
            {
                m_func = & w_rsa_traits::DoUnscramble;
                break;
            }

            default :
            {
                AT_Abort(); //unsupported mode
            }
        }
    }
    
    
    // ======== SetKey ================================================
    /**
     * SetKey is typically the first call to the Cipher.  This sets the
     * key to use for this cipher.
     *
     * SetKey also sets the "directionality" of the cipher i.e. Scramble
     * or Unscramble.  A Cipher may only support one or the other in the
     * case of public keys.  It is up to the application to use the
     * appropriate sense.  If a key is incapable of performing the said
     * mode a 
     *
     * @param i_key     Contains the key to use
     * @param i_mode    The type of Cipher wanted
     * @return nothing
     */

    virtual void SetKey(
        const std::string   & i_key,
        Mode                  i_mode
    ) {
        SetFunc( i_mode );

        at::Ptr<OsslWrap<OsslBIO> *> l_mem = new OsslWrap<OsslBIO>(
            BIO_new( BioMethodWrapperImpl<BioMethodStdStringTraits>::BIO_methods() )
        );

        std::string & l_val = BioMethodWrapperImpl<BioMethodStdString>::GetVar( l_mem->m_val );

        l_val = i_key;

        w_rsa_traits::ReadKey( l_mem, m_key ); // throws if we have probs
    }


    // ======== SetKey ================================================
    /**
     * This is an overload of SetKey that takes an at::Buffer as a 
     * key.
     *
     * @param i_key     Contains the key to use
     * @return nothing
     */

    virtual void SetKey(
        at::PtrView<const at::Buffer *>     i_key,
        Mode                                i_mode
    ) {
        
        SetFunc( i_mode );

        at::Ptr<OsslWrap<OsslBIO> *> l_mem = new OsslWrap<OsslBIO>(
            BIO_new( BioMethodWrapperImpl<BioMethodAtBufferTraits>::BIO_methods() )
        );

        // get the reference to the buffer Ptr used in the BIO struction
        at::Ptr<at::Buffer *> & l_obuffer =
            BioMethodWrapperImpl<BioMethodAtBufferTraits>::GetVar( l_mem->m_val );
            
        l_obuffer = at::PtrView<at::Buffer *>(
            const_cast<at::Buffer *>( & * i_key )
        );
        
        w_rsa_traits::ReadKey( l_mem, m_key ); // throws if we have probs
    }

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * 
     * @return nothing
     */

    virtual void	SetInitVector(
        at::PtrDelegate<const at::Buffer *>     i_iv
    ) {
    }

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * @return nothing
     */

    virtual void	SetInitVector(
        const at::Buffer::t_ConstRegion     & i_iv
    ) {
    }

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * @return nothing
     */

    virtual void	SetInitVector(
        const std::string               & i_iv
    ) {
    }


    // ======== WriteTo ===============================================
    /**
     * WriteTo sets the the buffer to write (append) the output
     * of the encryption.  The contents of the buffer are incomplete
     * unless "Flush" is called.  However, if Flushed is called, the contents
     * the buffer can no longer be appended to hence WriteTo must be called
     * again with a new buffer to write the encryption contents.
     *
     * @param i_output  The buffer to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        at::PtrDelegate<at::Buffer *>    o_output
    ) {
        w_rsa_traits::SetOutput( m_output, o_output );
    }


    // ======== WriteTo ===============================================
    /**
     * Overloaded version of WriteTo for a std::string writer.  The
     * string passed to WriteTo will be accessed by Cipher when
     * data is added.
     *
     * @param i_output  The std::string to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        std::string                 & o_output
    ) {
        w_rsa_traits::SetOutput( m_output, o_output );
    }

    // ======== Flush =================================================
    /**
     * Flush will finalize this cipher and pass the remaining data
     * to the output "WriteTo" buffer.
     *
     * @return nothing
     */

    virtual void Flush()
    {
        // nothing to do here
    }
    
    // ======== AddData ===============================================
    /**
     * AddData will pass the added data through the Cipher
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        const at::Buffer::t_ConstRegion       & i_region
    ) {
        m_func(
            m_key,
            i_region,
            m_output,
            m_parameters
        );
    }

    // ======== AddData ===============================================
    /**
     * AddData will pass the added data through the Cipher
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        at::PtrView<const at::Buffer *>     i_data
    ) {
        m_func(
            m_key,
            i_data->Region(),
            m_output,
            m_parameters
        );
    }

    // ======== AddData ===============================================
    /**
     * Overload of AddData for std::string
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        const std::string   & i_data
    ) {
        m_func(
            m_key,
            t_InputType( i_data ),
            m_output,
            m_parameters
        );
    }

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier()
    {
        return w_rsa_traits::s_cipher_id;
    }
};



typedef Cipher_Ossl< Cipher_OsslTraits_RSA<Cipher_OsslTraits_RSA_Pub> > Cipher_Ossl_RSA_Pub;
typedef Cipher_Ossl< Cipher_OsslTraits_RSA<Cipher_OsslTraits_RSA_Prv> > Cipher_Ossl_RSA_Prv;

// Register the factory - make one factory for generic RSA and another for RSA for OpenSSL or RSAOSSL
AT_MakeFactory0P( Cipher_Ossl_RSA_Prv::t_pub_or_prv_traits::s_cipher_id, Cipher_Ossl_RSA_Prv, at::Cipher, at::DKy );
AT_MakeNamedFactory0P( Cipher_Ossl_RSA_Prv_OSSL, Cipher_Ossl_RSA_Prv::t_pub_or_prv_traits::s_cipher_id2, Cipher_Ossl_RSA_Prv, at::Cipher, at::DKy );

// Register the factory - make one factory for generic RSA and another for RSA for OpenSSL or RSAOSSL
AT_MakeFactory0P( Cipher_Ossl_RSA_Pub::t_pub_or_prv_traits::s_cipher_id, Cipher_Ossl_RSA_Pub, at::Cipher, at::DKy );
AT_MakeNamedFactory0P( Cipher_Ossl_RSA_Pub_OSSL, Cipher_Ossl_RSA_Pub::t_pub_or_prv_traits::s_cipher_id2, Cipher_Ossl_RSA_Pub, at::Cipher, at::DKy );


// ======== CipherPublicKey_Ossl_RSA ==================================
/**
 * CipherPublicKey_Ossl_RSA is the implementation of the RSA public
 * key.
 *
 */

class CipherPublicKey_Ossl_RSA
  : public at::CipherPublicKey
{
    public:

    /**
     * m_modulus_bits is the modulus size
     */
    int                             m_modulus_bits;

    /**
     * m_exponent is the exponent of the key
     */
    unsigned long                   m_exponent;

    /**
     * A place for the key
     */
    at::Ptr<OsslWrap<OsslRSA> *>    m_rsa;
    
    CipherPublicKey_Ossl_RSA( int i_modulus_bits, unsigned long i_exponent )
      : m_modulus_bits( i_modulus_bits ),
        m_exponent( i_exponent )
    {
    }
    
    CipherPublicKey_Ossl_RSA()
      : m_modulus_bits( 512 ),
        m_exponent( 65537 )
    {
    }

    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses Buffer parameters
     *
     * @param o_new_public_key  A new public key is written to this param
     * @param o_new_private_key A new public key is written here
     * @return nothing
     */

    static std::string GetPublicKeyFromPrivatekey(
        at::PtrView<OsslWrap<OsslRSA> *>    i_rsa
    ) {

        at::Ptr<OsslWrap<OsslBIO> *> l_mem = new OsslWrap<OsslBIO>(
            BIO_new( BioMethodWrapperImpl<BioMethodStdStringTraits>::BIO_methods() )
        );

        std::string & l_val = BioMethodWrapperImpl<BioMethodStdString>::GetVar( l_mem->m_val );

        if ( ! PEM_write_bio_RSAPublicKey( l_mem->m_val, i_rsa->m_val ) )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadKey,
                OsslError( "Public key is invalid" )
            );            
        }

        return l_val;
    }
    
    // ======== SetCipherParameters =======================================
    /**
     * SetCipherParameters is used to add extra information about this key.
     * For example : SetCipherParameters( "RSA:512:65537" ) will set the key
     * to a 512 bit key with an exponent of 65537.
     *
     * @param i_key_params The key parameters
     * @return nothing
     */

    virtual void SetCipherParameters(
        const std::string           & i_key_params
    ) {
    }

    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this key
     * generator.
     *
     * @param i_property The requested property identifer
     * @return A CipherProperty "any" type
     */

    virtual at::CipherProperty GetProperty(
        const at::CipherGenPropertyId   & i_property
    ) {
        return at::s_no_property;
    }
    
    // ======== SetProperty ===========================================
    /**
     * SetProperty can set a property on the key generator.
     *
     * @param i_property The requested property identifer
     * @param o_value The at::Any object to write the value of the requested property
     * @return true if the value was set
     */

    virtual bool SetProperty(
        const at::CipherGenPropertyId   & i_property,
        const at::CipherProperty        & i_value
    ) {
        return false;
    }


    // ======== GenerateKey ===========================================
    /**
     * GenerateKey performs key generation and returns an RSA *
     *
     *
     * @param o_rsa location to deposit key
     * @return nothing
     */

    void GenerateKey(
        at::Ptr<OsslWrap<OsslRSA> *> & o_rsa
    ) {
        // Make sure the seeder is seeded
        OsslRandomSeeder::Seeder();

        do {
            o_rsa = new OsslWrap<OsslRSA>(
                RSA_generate_key( m_modulus_bits, m_exponent, 0, 0 )
            );
        } while ( ! o_rsa );
    }

    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses a std::string parameter.
     *
     * @param o_new_public_key  A new public key is written to this param
     * @param o_new_private_key A new public key is written here
     * @return nothing
     */

    virtual void	GenerateKey(
        std::string             & o_new_public_key,
        std::string             & o_new_private_key
    ) {
        GenerateKey( m_rsa );
        
        at::Ptr<OsslWrap<OsslBIO> *> l_mem = new OsslWrap<OsslBIO>(
            BIO_new( BioMethodWrapperImpl<BioMethodStdStringTraits>::BIO_methods() )
        );

        PEM_write_bio_RSAPrivateKey( l_mem->m_val, m_rsa->m_val, NULL, NULL, 0, 0, NULL);

        std::string & l_val = BioMethodWrapperImpl<BioMethodStdString>::GetVar( l_mem->m_val );

        o_new_private_key = l_val;

        // clear the value
        l_val.clear();

        PEM_write_bio_RSAPublicKey( l_mem->m_val, m_rsa->m_val );

        o_new_public_key = l_val;
        
    }

    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses Buffer parameters
     *
     * @param o_new_public_key  A new public key is written to this param
     * @param o_new_private_key A new public key is written here
     * @return nothing
     */

    virtual void	GenerateKey(
        at::PtrView<at::Buffer *>         o_new_public_key,
        at::PtrView<at::Buffer *>         o_new_private_key
    ) {

        GenerateKey( m_rsa );
        
        at::Ptr<OsslWrap<OsslBIO> *> l_mem = new OsslWrap<OsslBIO>(
            BIO_new( BioMethodWrapperImpl<BioMethodAtBufferTraits>::BIO_methods() )
        );

        // get the reference to the buffer Ptr used in the BIO struction
        at::Ptr<at::Buffer *> & l_obuffer =
            BioMethodWrapperImpl<BioMethodAtBufferTraits>::GetVar( l_mem->m_val );

        l_obuffer = o_new_private_key;

        PEM_write_bio_RSAPrivateKey( l_mem->m_val, m_rsa->m_val, NULL, NULL, 0, 0, NULL);

        l_obuffer = o_new_public_key;

        PEM_write_bio_RSAPublicKey( l_mem->m_val, m_rsa->m_val );

    }


    // ======== NewPublicCipher =============================================
    /**
     * NewPublicCipher returns a new cipher associated with this key generator
     * that takes a public key.
     *
     * @return nothing
     */

    virtual at::PtrDelegate<at::Cipher *> NewPublicCipher()
    {
        return new Cipher_Ossl< Cipher_OsslTraits_RSA<Cipher_OsslTraits_RSA_Pub> >( m_rsa );
    }

    // ======== NewPrivateCipher =============================================
    /**
     * NewPrivateCipher returns a new cipher associated with this key generator
     *
     * @return nothing
     */

    virtual at::PtrDelegate<at::Cipher *> NewPrivateCipher()
    {
        return new Cipher_Ossl< Cipher_OsslTraits_RSA<Cipher_OsslTraits_RSA_Prv> >( m_rsa );
    }

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier()
    {
        return s_cipher_id;
    }
    
    static const char                       s_cipher_id[];
    static const char                       s_cipher_id2[];
};

const char CipherPublicKey_Ossl_RSA::s_cipher_id[] = "RSA";
const char CipherPublicKey_Ossl_RSA::s_cipher_id2[] = "RSAOSSL";


// Register the factory - make one factory for generic RSA and another for RSA for OpenSSL or RSAOSSL
AT_MakeFactory0P( CipherPublicKey_Ossl_RSA::s_cipher_id, CipherPublicKey_Ossl_RSA, at::CipherPublicKey, at::DKy );
AT_MakeNamedFactory0P(
    CipherPublicKey_Ossl_RSA_A, CipherPublicKey_Ossl_RSA::s_cipher_id2, CipherPublicKey_Ossl_RSA, at::CipherPublicKey, at::DKy
);



// ======== OsslEVP_CIPHER_CTX ========================================
/**
 * OsslEVP_CIPHER_CTX is the traits to manage the OsslEVP_CIPHER_CTX
 * openssl type.
 *
 */

class OsslEVP_CIPHER_CTX
{
    public:

    typedef EVP_CIPHER_CTX              t_Context;

    // no base class for you !
    struct t_Base {};


    // ======== Initializer ===========================================
    /**
     * Initializer method
     *
     * @param o_ctx The context to initialize
     * @return nothing
     */

    static void Initializer( EVP_CIPHER_CTX & o_ctx )
    {
        EVP_CIPHER_CTX_init( & o_ctx );
    }

    // ======== Finalizer =============================================
    /**
     * Finalizer method
     *
     * @param o_ctx The context to finalize
     * @return nothing
     */

    static void Finalizer( EVP_CIPHER_CTX & o_ctx )
    {
        EVP_CIPHER_CTX_cleanup( & o_ctx );
    }
    
};

// ======== EVPEncDecFunctions ========================================
/**
 * EVPEncDecFunctions wraps the encoder/decoder functions
 *
 */

struct EVPEncDecFunctions
{
    typedef int ( * t_Init_ex )(
        EVP_CIPHER_CTX      *ctx,
        const EVP_CIPHER    *type,
        ENGINE              *impl,
        const unsigned char *key,
        const unsigned char *iv
    );

    typedef int ( * t_Update)(
        EVP_CIPHER_CTX      *ctx,
        unsigned char       *out,
        int                 *outl,
        const unsigned char *in,
        int                  inl
    );

    typedef int ( * t_Final_ex)(
        EVP_CIPHER_CTX      *ctx,
        unsigned char       *out,
        int                 *outl
    );

    /**
     * m_init_ex, m_update and m_final_ex are pointers to
     * the openssl EVP encryption/decryption functions.
     */
    const t_Init_ex               m_init_ex;
    const t_Update                m_update;
    const t_Final_ex              m_final_ex;
    
    enum EVP_enc
    {
        do_decryption = 0,
        do_encryption = 1,
    };

    /**
     * m_mode is the encryption mode.
     */
    const EVP_enc                 m_mode;

};

/**
 * s_evp_encrypt_funcs is the list of functions for performing
 * encryption
 */

const EVPEncDecFunctions  s_evp_encrypt_funcs = {
    & EVP_EncryptInit_ex,
    & EVP_EncryptUpdate,
    & EVP_EncryptFinal_ex,
    EVPEncDecFunctions::do_decryption
};

/**
 * s_evp_encrypt_funcs is the list of functions for performing
 * decryption
 */
const EVPEncDecFunctions  s_evp_decrypt_funcs = {
    & EVP_DecryptInit_ex,
    & EVP_DecryptUpdate,
    & EVP_DecryptFinal_ex,
    EVPEncDecFunctions::do_decryption
};


// ======== EVP_Context ===============================================
/**
 * Wraps an EVP_CIPHER_CTX and adds a member functions that access
 * it.
 */

struct EVP_Context
  : public OsslCtxWrap<OsslEVP_CIPHER_CTX>
{

    EVP_Context(
        const EVP_CIPHER * i_evp_cipher
    )
      : m_evp_cipher( i_evp_cipher )
    {
        EVP_CipherInit_ex( & m_context, m_evp_cipher, 0, 0, 0, 1 );
    }

    // ======== ReInitialize ==========================================
    /**
     * ReInitialize performs a Finalizer/Initializer sequence to
     * cleanup and re-use the EVP_CIPHER_CTX and also sets up the
     * context so the key length can be set.
     * 
     * @return nothing
     */

    void ReInitialize()
    {
        OsslCtxWrap<OsslEVP_CIPHER_CTX>::ReInitialize();
        EVP_CipherInit_ex( & m_context, m_evp_cipher, 0, 0, 0, 1 );
    }    


    // ======== NeedsIv ===========================================
    /**
     * NeedsIv returns true if the cipher uses an iv
     *
     * @return 
     */

    bool NeedsIv()
    {
        return EVP_CIPHER_iv_length( m_evp_cipher ) != 0;
    }

    // ======== IvLength ==============================================
    /**
     * Returns the IV length
     *
     * @return The IV length
     */

    int IvLength()
    {
        return EVP_CIPHER_iv_length( m_evp_cipher );
    }
    

    // ======== BlockSize =============================================
    /**
     * BlockSize is the block size of the cipher
     *
     * @return The block size of this cipher.
     */

    int BlockSize()
    {
        return m_evp_cipher->block_size;
    }    

    /**
     * This is the evp cipher
     */
    const EVP_CIPHER                      * m_evp_cipher;

};


// ======== Cipher_OsslEVP_Base =======================================
/**
 * The base class for EVP based ciphers
 *
 */

class Cipher_OsslEVP_Base
  : public at::Cipher
{
    public:
    typedef at::Ptr<BufferWrapper *>        t_OutputType;

    /**
     * m_ctx wraps the EVP_CIPHER_CTX structure and provides
     * some convenient functions.
     */
    EVP_Context                             m_ctx;
    
    /**
     * m_output contains the location of the result of the
     * encryption.
     */
    t_OutputType                            m_output;

    /**
     * m_funcs contains pointers to the appropriate encrypt or decrypt functions.
     */

    const EVPEncDecFunctions              * m_funcs;

    // ======== CipherState ===========================================
    /**
     * CipherState describes the possible states of this cipher
     *
     */

    enum CipherState
    {

        /**
         * The cipher has just been created.
         */
        Initial,

        /**
         * KeySet Indicates that the key has been set
         */

        KeySet,

        /**
         * IVSet Indicates that the initialization vector
         * was set but the key has not yet been set.
         */

        IVSet,

        /**
         * KeySetAndReady indicates that the key has been set and the
         * initialization vector has also been set if one was needed.
         */

        KeySetAndReady,

        /**
         * DataFlushed indicates that the context has been flushed.
         * The cipher may need initializing again.
         */

        DataFlushed,

        /**
         * DataAdded indicates that data has been passed
         * to the cipher - setting the key in this state
         * would automatically flush.
         */

        DataAdded
    };

    CipherState                             m_state;

    /**
     * m_iv is the initialization vector
     */
    std::string                             m_iv;

    /**
     * m_key is the key of this cipher
     */
    std::string                             m_key;

    Cipher_OsslEVP_Base(
        const EVP_CIPHER * i_evp_cipher
    )
      : m_ctx( i_evp_cipher ),
        m_funcs( & s_evp_encrypt_funcs ),
        m_state( Initial )
    {
    }

    Cipher_OsslEVP_Base(
        const EVP_CIPHER * i_evp_cipher,
        const std::string   & i_key,
        const std::string   & i_iv
    )
      : m_ctx( i_evp_cipher ),
        m_funcs( & s_evp_encrypt_funcs ),
        m_state( Initial ),
        m_key( i_key ),
        m_iv( i_iv )
    {
        SetFunc( Scramble ); 
    }

    // ======== GetKeyGenerator =======================================
    /**
     * GetKeyGenerator returns a key generator for this cipher.
     *
     * @return The key generator for this cipher
     */

    virtual at::PtrDelegate<at::CipherSymmetricKey*> GetKeyGenerator() = 0;


    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this Cipher.
     *
     *
     * @param i_property is the requested property
     * @return A CipherProperty "any" type
     */

    at::CipherProperty GetProperty(
        Property            i_property
    ) {
        switch ( i_property )
        {
            case IsSymmetric :
            {
                return at::ToAny( bool( true ) );
            }
            case IsPublic :
            {
                return at::ToAny( bool( false ) );
            }
            case KeyGenerator :
            {
                // PtrDelegate gets converted to a Ptr in Any types.
                return at::ToAny( GetKeyGenerator() );
            }
        }
        
        return at::s_no_property;
    }
    
    // ======== SetProperty ===========================================
    /**
     * SetProperty can set a property on the key generator.
     *
     * @param i_property The requested property identifer
     * @param o_value The at::Any object to write the value of the requested property
     * @return True if the property was set
     */

    virtual bool SetProperty(
        Property                      i_property,
        at::CipherProperty          & i_value
    ) {
        return false;
    }

    // ======== SetCipherParameters =======================================
    /**
     * SetCipherParameters is used to add extra information about this key.
     * For example : SetCipherParameters( "RSA:512:65537" ) will set the key
     * to a 512 bit key with an exponent of 65537.
     *
     * @param i_key_params The key parameters
     * @return nothing
     */

    virtual void	SetCipherParameters(
        const std::string           & i_key_params
    ) {
    }


    // ======== ReInitializeContext ===================================
    /**
     * ReInitializeContext is used to restart the context
     *
     * @return nothing
     */

    void ReInitializeContext()
    {
        m_ctx.ReInitialize();
        std::size_t l_length = m_key.length();

        if ( ! EVP_CIPHER_CTX_set_key_length( & m_ctx.m_context, l_length ) )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadKey,
                OsslError( "EVP_CIPHER_CTX_set_key_length failed" )
            );            
        }
    }

    
    // ======== SetFunc ===============================================
    /**
     * SetFunc will set the cipher function to either encrypt
     * or decrypt based on mode.
     *
     * @param i_mode The cipher mode being requested
     * @return nothing
     */

    void SetFunc(
        Mode                  i_mode
    ) {
        const EVPEncDecFunctions * l_funcs;
    
        switch ( i_mode )
        {
            case Scramble :
            {
                l_funcs = & s_evp_encrypt_funcs;
                break;
            }
            case Unscramble :
            {
                l_funcs = & s_evp_decrypt_funcs;
                break;
            }

            default :
            {
                AT_Abort(); //unsupported mode
            }
        }
        
        m_funcs = l_funcs;
        
        switch ( m_state )
        {
            case Initial :
            {
                // check to see if we need an initialization vector set
    
                if ( m_ctx.NeedsIv() )
                {
                    m_state = KeySet;
                }
                else
                {
                    m_state = KeySetAndReady;
                }
                break;
            }
            
            case KeySetAndReady :
            case IVSet :
            {
                m_state = KeySetAndReady;
                break;
            }

            case KeySet :
            {
                break;
            }

            case DataFlushed :
            case DataAdded :
            {
                // oops - data has been added - let's make sure that
                // the cipher is re initialized
                m_ctx.ReInitialize();

                m_state = KeySetAndReady;
                break;
            }

            default :
            {
                AT_Abort();
            }

        } // switch

    }
    
    
    // ======== SetKey ================================================
    /**
     * SetKey is typically the first call to the Cipher.  This sets the
     * key to use for this cipher.
     *
     * SetKey also sets the "directionality" of the cipher i.e. Scramble
     * or Unscramble.  A Cipher may only support one or the other in the
     * case of public keys.  It is up to the application to use the
     * appropriate sense.  If a key is incapable of performing the said
     * mode a 
     *
     * @param i_key     Contains the key to use
     * @param i_mode    The type of Cipher wanted
     * @return nothing
     */

    virtual void SetKey(
        const std::string   & i_key,
        Mode                  i_mode
    ) {

        m_key = i_key;

        SetFunc( i_mode );
        
        std::size_t l_length = m_key.length();

        if ( ! EVP_CIPHER_CTX_set_key_length( & m_ctx.m_context, l_length ) )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadKey,
                OsslError( "EVP_CIPHER_CTX_set_key_length failed" )
            );            
        }
    }


    // ======== SetKey ================================================
    /**
     * This is an overload of SetKey that takes an at::Buffer as a 
     * key.
     *
     * @param i_key     Contains the key to use
     * @return nothing
     */

    virtual void SetKey(
        at::PtrView<const at::Buffer *>     i_key,
        Mode                                i_mode
    ) {

        SetKey( i_key->Region().String(), i_mode );
    }

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * 
     * @return nothing
     */

    virtual void	SetInitVector(
        at::PtrDelegate<const at::Buffer *>     i_iv
    ) {

        SetInitVector( i_iv->Region().String() );
    }

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * @return nothing
     */

    virtual void	SetInitVector(
        const at::Buffer::t_ConstRegion     & i_iv
    ) {
        SetInitVector( i_iv.String() );
    }

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * @return nothing
     */

    virtual void	SetInitVector(
        const std::string               & i_iv
    ) {
        if ( ! m_ctx.NeedsIv() )
        {
            if ( ! i_iv.length() )
            {
                return;
            }
        
            AT_ThrowDerivBase(
                at::Cipher_IVectorNotNeeded,
                "Initialization vector is not used by this cipher"
            );
        }
        
        if ( i_iv.length() != size_t( m_ctx.IvLength() ) )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadInitVector,
                "Initialization vector has incorrect length"
            );
        }

        m_iv = i_iv;
        
        switch ( m_state )
        {
            case Initial :
            {
                m_state = IVSet;
                break;
            }
            
            case DataFlushed :
            case IVSet :
            {
                break;
            }

            case KeySetAndReady :
            case KeySet :
            {
                m_state = KeySetAndReady;
                break;
            }

            case DataAdded :
            {
                // Don't allow calls
                AT_Abort();
                break;
            }

            default :
            {
                AT_Abort();
            }

        } // switch

    }


    // ======== CipherInit ============================================
    /**
     * CipherInit initializes this cipher in the context 
     * defined of the EVP cipher.
     *
     * @return nothing
     */

    inline void CipherInit()
    {
        
        switch ( m_state )
        {
            case IVSet :
            case Initial :
            {
                AT_ThrowDerivBase(
                    at::Cipher_BadKey,
                    "Key has not been set"
                );
                break;
            }
            
            case DataFlushed :
            {
                ReInitializeContext();
                break;
            }

            case KeySetAndReady :
            {
                break;
            }
            
            case KeySet :
            {
                // in theory, the initialization
                // vector has not been set -
                // set one to a string of nulls
                m_iv = std::string( m_ctx.IvLength(), 0 );
                m_state = KeySetAndReady;
                break;
            }

            case DataAdded :
            {
                // no initialization required when
                // data has already been added.
                return;
            }

            default :
            {
                AT_Abort();
            }

        } // switch
        
        m_state = DataAdded;
        m_funcs->m_init_ex(
            & m_ctx.m_context,
            m_ctx.m_evp_cipher,
            0,
            (const unsigned char*) m_key.c_str(),
            (const unsigned char*) m_iv.c_str()
        );

        EVP_CIPHER_CTX_set_padding( & m_ctx.m_context, 0 );

    }

    // ======== WriteTo ===============================================
    /**
     * WriteTo sets the the buffer to write (append) the output
     * of the encryption.  The contents of the buffer are incomplete
     * unless "Flush" is called.  However, if Flushed is called, the contents
     * the buffer can no longer be appended to hence WriteTo must be called
     * again with a new buffer to write the encryption contents.
     *
     * @param i_output  The buffer to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        at::PtrDelegate<at::Buffer *>    o_output
    ) {
        m_output = new BufferWrapper_AtBuffer( o_output );
    }

    // ======== WriteTo ===============================================
    /**
     * Overloaded version of WriteTo for a std::string writer.  The
     * string passed to WriteTo will be accessed by Cipher when
     * data is added.
     *
     * @param i_output  The std::string to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        std::string                 & o_output
    ) {
        m_output = new BufferWrapper_StdString( o_output );
    }

    // ======== Flush =================================================
    /**
     * Flush will finalize this cipher and pass the remaining data
     * to the output "WriteTo" buffer.
     *
     * @return nothing
     */

    virtual void Flush()
    {
        switch ( m_state )
        {
            case Initial :
            case IVSet :
            case KeySetAndReady :
            case KeySet :
            {
                break;
            }

            case DataAdded :
            {
                // indicate that the data is now flushed
                m_state = DataFlushed;
                
                // get the output buffer
                at::PtrView< at::Buffer * > l_woutput = m_output->GetBuffer();

                // make some room
                at::SizeMem l_extra = EVP_MAX_BLOCK_LENGTH * 2;
        
                // make sure there is enough space in the output buffer.
                const at::Buffer::t_Region & l_output = l_woutput->RegionExtend( l_extra, 0 );
        
                int l_out_sz; // bytes written to output buffer goes here
        
                int l_ret = m_funcs->m_final_ex(
                    & m_ctx.m_context,
                    ( unsigned char * ) ( l_output.m_mem + l_output.m_allocated ),
                    & l_out_sz
                );
        
                if ( 0 == l_ret )
                {
                    AT_ThrowDerivBase(
                        at::Cipher_BadCipherParameters,
                        OsslError( "Flush failed" )
                    );
                }
        
                // no bytes were written
                if ( ! l_out_sz )
                {
                    return;
                }
        
                // Assert (may get compiled away by so we put it here for the
                // sake of debug builds). In theory, if we had ignored the
                // assert below, it will get caught in the if/abort below
                // and that will fail only if there was truly a buffer overrun.
                // This assert below fails only if the estimate is wrong.
                AT_Assert( l_out_sz <= l_extra );
        
                // hard check here - if we overran the buffer stop dead
                if ( l_out_sz > l_output.m_max_available - l_output.m_allocated )
                {
                    AT_Abort();
                }
        
                // make sure that the return value is correct
                l_woutput->SetAllocated( l_output.m_allocated + l_out_sz );
        
                m_output->Flush();
                
                break;
            }

            default :
            {
                AT_Abort();
            }

        } // switch
    }


    // ======== AddData ===============================================
    /**
     * AddData takes a region and performs the encrypt or decrypt
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        const at::Buffer::t_ConstRegion       & i_region
    ) {
        // The output is not set - this is a programming error so
        // abort here (not exception)
        if ( ! m_output )
        {
            AT_Abort();
        }
        
        // get the output buffer
        at::PtrView< at::Buffer * > l_woutput = m_output->GetBuffer();

        // make sure the cipher is properly initialized
        CipherInit();

        // use this to pass to the cipher
        int l_in_sz( i_region.m_allocated );
        
        // estimate the maximum number of bytes in the output buffer
        // round the input size up to the next block size and
        // add another block for good measure.
        at::SizeMem l_extra =
            ( ( l_in_sz - 1 ) | ( EVP_MAX_BLOCK_LENGTH - 1) ) + EVP_MAX_BLOCK_LENGTH;

        // make sure there is enough space in the output buffer.
        const at::Buffer::t_Region & l_output = l_woutput->RegionExtend( l_extra, 4096 );

        int l_out_sz; // bytes written to output buffer goes here

        int l_ret = m_funcs->m_update(
            & m_ctx.m_context,
            ( unsigned char * ) ( l_output.m_mem + l_output.m_allocated ),
            & l_out_sz,
            ( unsigned char * ) ( i_region.m_mem ),
            l_in_sz
        );

        if ( 0 == l_ret )
        {
            AT_ThrowDerivBase(
                at::Cipher_BadCipherParameters,
                OsslError( "Add failed" )
            );
        }

        // no bytes were written
        if ( ! l_out_sz )
        {
            return;
        }

        // Assert (may get compiled away by so we put it here for the
        // sake of debug builds). In theory, if we had ignored the
        // assert below, it will get caught in the if/abort below
        // and that will fail only if there was truly a buffer overrun.
        // This assert below fails only if the estimate is wrong.
        AT_Assert( l_out_sz <= l_extra );

        // hard check here - if we overran the buffer stop dead
        if ( l_out_sz > l_output.m_max_available - l_output.m_allocated )
        {
            AT_Abort();
        }

        // make sure that the return value is correct
        l_woutput->SetAllocated( l_output.m_allocated + l_out_sz );

        m_output->Flush();
    }
    
    // ======== AddData ===============================================
    /**
     * AddData will pass the added data through the Cipher
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        at::PtrView<const at::Buffer *>     i_data
    ) {
        AddData( i_data->Region() );
    }

    // ======== AddData ===============================================
    /**
     * Overload of AddData for std::string
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        const std::string   & i_data
    ) {
        AddData( at::Buffer::t_ConstRegion( i_data ) );
    }

}; // class Cipher_OsslEVP_Base

template <typename w_evp_traits>
class CipherSymmetricKey_EVP_Tmpl;

// ======== Cipher_OsslEVP ===============================================
/**
 * Cipher_OsslEVP_RSA is a template class that can be used to create
 * various kinds of ciphers.
 */
template <typename w_evp_traits>
class Cipher_OsslEVP
  : public Cipher_OsslEVP_Base
{
    public:
    typedef w_evp_traits                        t_evp_traits;

    public:
    Cipher_OsslEVP()
      : Cipher_OsslEVP_Base( w_evp_traits::GetCipher() )
    {
    }
    
    Cipher_OsslEVP(
        const std::string   & i_key,
        const std::string   & i_iv
    )
      : Cipher_OsslEVP_Base( w_evp_traits::GetCipher(), i_key, i_iv )
    {
    }

    // ======== GetKeyGenerator =======================================
    /**
     * GetKeyGenerator returns a key generator for this cipher.
     *
     * @return The key generator for this cipher
     */

    virtual at::PtrDelegate<at::CipherSymmetricKey*> GetKeyGenerator()
    {
        return new CipherSymmetricKey_EVP_Tmpl<w_evp_traits>;
    }
    
    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier()
    {
        return w_evp_traits::s_cipher_id;
    }

};

// ======== CipherSymmetricKey ========================================
/**
 * CipherSymmetricKey is a key generator for a symmetric key Cipher.
 *
 */

class CipherSymmetricKey_EVP
  : public at::CipherSymmetricKey
{
    public:

    const EVP_CIPHER            * m_evp_cipher;

    /**
     * m_iv is the initialization vector
     */
    std::string                             m_iv;

    /**
     * m_key is the key of this cipher
     */
    std::string                             m_key;

    /**
     * m_keylen is the key length for a cipher.
     */
    int                                     m_keylen;

    // ======== InitVectorLength ======================================
    /**
     * InitVectorLength returns 0 if no init vector is needed or
     * the length of the required init vector.
     *
     * @return 0 if none or length
     */

    at::SizeMem InitVectorLength()
    {
        return EVP_CIPHER_iv_length( m_evp_cipher );
    }

    // ======== KeyLength =============================================
    /**
     * KeyLength returns 0 if the key can be a custom length or a
     * value indicating the required length of the key.
     *
     * @return nothing
     */

    at::SizeMem KeyLength()
    {
        if( EVP_CIPHER_flags( m_evp_cipher ) & EVP_CIPH_VARIABLE_LENGTH )
        {
            return 0;
        }
        return EVP_CIPHER_key_length( m_evp_cipher );
    }

    // ======== CipherSymmetricKey_EVP ================================
    /**
     * CipherSymmetricKey_EVP constructor takes the EVP_CIPHER and
     * discovers the parameters needed for the keys.
     *
     *
     * @param i_evp_cipher The EVP_CIPHER associated with this cipher
     */

    CipherSymmetricKey_EVP(
        const EVP_CIPHER * i_evp_cipher
    )
      : m_evp_cipher( i_evp_cipher ),
        m_keylen( EVP_CIPHER_key_length( i_evp_cipher ) )
    {
    }
    
    // ======== SetCipherParameters =======================================
    /**
     * SetCipherParameters is used to add extra information about this key.
     * For example : SetCipherParameters( "RSA:512:65537" ) will set the key
     * to a 512 bit key with an exponent of 65537.
     *
     * @param i_key_params The key parameters
     * @return nothing
     */

    void SetCipherParameters(
        const std::string           & i_key_params
    ) {
    }

    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this key
     * generator.
     *
     * @param i_property The requested property identifer
     * @return A CipherProperty "any" type
     */

    at::CipherProperty GetProperty(
        const at::CipherGenPropertyId   & i_property
    ) {
        return at::s_no_property;        
    }
    
    // ======== SetProperty ===========================================
    /**
     * SetProperty can set a property on the key generator.
     *
     * @param i_property The requested property identifer
     * @param o_value The at::Any object to write the value of the requested property
     * @return True if the property was set
     */

    bool SetProperty(
        const at::CipherGenPropertyId   & i_property,
        at::CipherProperty              & i_value
    ) {
        return false;
    }
    
    // ======== GenerateKey ===========================================
    /**
     * GenerateKey will create a key for the Cipher associated with
     * this key.
     *
     * @return nothing
     */

    std::string GenerateKey()
    {
        int l_length = m_keylen;

        if ( l_length == 0 )
        {
            m_keylen = l_length = 16;
        }
        
        return ( m_key = at::BinaryRandomSequence( l_length ) );
    }


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses a std::string parameter.
     *
     * @param o_new_key  A new key is written to this param
     * @return nothing
     */

    void	GenerateKey(
        std::string             & o_new_key
    ) {
        o_new_key = GenerateKey();
    }


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses Buffer parameters
     *
     * @param o_new_key     A new key is written to this param
     * @return nothing
     */

    void	GenerateKey(
        at::PtrView<at::Buffer *>   o_new_public_key
    ) {
        o_new_public_key->Set( GenerateKey() );
    }

    // ======== GenerateInitVector ====================================
    /**
     * GenerateInitVector is called when an initialization vector is needed
     *
     * @return The initialization vector or an empty string if none is required
     */

    std::string GenerateInitVector()
    {
        int l_len = EVP_CIPHER_iv_length( m_evp_cipher );
        if ( 0 == l_len )
        {
            return "";
        }

        return ( m_iv = at::BinaryRandomSequence( l_len ) );
    }

    // ======== GenerateInitVector ====================================
    /**
     * GenerateInitVector is called when an ivector is needed
     *
     *
     * @param o_init_vector The initialization vector is placed
     *                      in this location.
     * @return nothing
     */

    void GenerateInitVector(
        at::PtrView<at::Buffer *>         o_init_vector
    ) {
        o_init_vector->Set( GenerateInitVector() );
    }

    // ======== GenerateInitVector ====================================
    /**
     * GenerateInitVector is called when an ivector is needed
     *
     *
     * @param o_init_vector The initialization vector is placed
     *                      in this location.
     * @return nothing
     */

    void GenerateInitVector(
        std::string             & o_init_vector
    ) {
        o_init_vector = GenerateInitVector();
    }

};


// ======== CipherSymmetricKey_EVP_Tmpl ===============================
/**
 * 
 *
 */

template <typename w_evp_traits>
class CipherSymmetricKey_EVP_Tmpl
  : public CipherSymmetricKey_EVP
{
    public:

    /**
     * CipherSymmetricKey_EVP_Tmpl
     *
     */
    CipherSymmetricKey_EVP_Tmpl()
      : CipherSymmetricKey_EVP( w_evp_traits::GetCipher() )
    {
    }

    // ======== NewCipher =============================================
    /**
     * NewCipher returns a new cipher associated with this key generator
     *
     * @return nothing
     */

    virtual at::PtrDelegate<at::Cipher *> NewCipher()
    {
        return new Cipher_OsslEVP<w_evp_traits>( m_key, m_iv );
    }

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return identifier
     */

    virtual std::string CipherIdentifier()
    {
        return w_evp_traits::s_cipher_id;
    }

};



// ======== RegisterOsslEVPCipher =========================================
/**
 * RegisterOsslEVPCipher is a macro that does all the work of defining
 * the EVP cipher traits class.
 */

#define RegisterOsslEVPCipher( i_full_name, i_short_name )               \
class Cipher_EVP_ ## i_full_name                                         \
{                                                                        \
    public:                                                              \
                                                                         \
    inline static const EVP_CIPHER * GetCipher()                         \
    {                                                                    \
        return EVP_ ## i_full_name();                                    \
    }                                                                    \
                                                                         \
    static const char s_cipher_id[];                                     \
    static const char s_cipher_id2[];                                    \
};                                                                       \
                                                                         \
const char Cipher_EVP_ ## i_full_name::s_cipher_id[] = # i_full_name;    \
const char Cipher_EVP_ ## i_full_name::s_cipher_id2[] = # i_short_name;  \
                                                                         \
typedef                                                                  \
    Cipher_OsslEVP<Cipher_EVP_ ## i_full_name>                           \
    Cipher_OsslEVP_ ## i_full_name;                                      \
                                                                         \
typedef                                                                  \
    CipherSymmetricKey_EVP_Tmpl<Cipher_EVP_ ## i_full_name>              \
    CipherSymmetricKey_EVP_ ## i_full_name;                              \
                                                                         \
AT_MakeFactory0P(                                                        \
    Cipher_OsslEVP_ ## i_full_name::t_evp_traits::s_cipher_id,           \
    Cipher_OsslEVP_ ## i_full_name,                                      \
    at::Cipher,                                                          \
    at::DKy                                                              \
);                                                                       \
                                                                         \
AT_MakeNamedFactory0P(                                                   \
    Cipher_OsslEVP_ ## i_full_name ## _A,                                \
    Cipher_OsslEVP_ ## i_full_name::t_evp_traits::s_cipher_id2,          \
    Cipher_OsslEVP_ ## i_full_name,                                      \
    at::Cipher,                                                          \
    at::DKy                                                              \
);                                                                       \
                                                                         \
AT_MakeFactory0P(                                                        \
    Cipher_OsslEVP_ ## i_full_name::t_evp_traits::s_cipher_id,           \
    CipherSymmetricKey_EVP_ ## i_full_name,                              \
    at::CipherSymmetricKey,                                              \
    at::DKy                                                              \
);                                                                       \
                                                                         \
AT_MakeNamedFactory0P(                                                   \
    CipherSymmetricKey_EVP_ ## i_full_name ## _A,                        \
    Cipher_OsslEVP_ ## i_full_name::t_evp_traits::s_cipher_id2,          \
    CipherSymmetricKey_EVP_ ## i_full_name,                              \
    at::CipherSymmetricKey,                                              \
    at::DKy                                                              \
);                                                                       \
// end macro

// Create all the openssl ciphers
RegisterOsslEVPCipher( aes_128_cbc,         a7c )
RegisterOsslEVPCipher( aes_128_cfb1,        a7cf1 )
RegisterOsslEVPCipher( aes_128_cfb128,      a7cf7 )
RegisterOsslEVPCipher( aes_128_cfb8,        a7cf3 )
RegisterOsslEVPCipher( aes_128_ecb,         a7e )
RegisterOsslEVPCipher( aes_128_ofb,         a7o )
RegisterOsslEVPCipher( aes_192_cbc,         a75c )
RegisterOsslEVPCipher( aes_192_cfb1,        a75cf1 )
RegisterOsslEVPCipher( aes_192_cfb128,      a75cf7 )
RegisterOsslEVPCipher( aes_192_cfb8,        a75cf3 )
RegisterOsslEVPCipher( aes_192_ecb,         a75e )
RegisterOsslEVPCipher( aes_192_ofb,         a75o )
RegisterOsslEVPCipher( aes_256_cbc,         a8c )
RegisterOsslEVPCipher( aes_256_cfb1,        a8cf1 )
RegisterOsslEVPCipher( aes_256_cfb128,      a8cf7 )
RegisterOsslEVPCipher( aes_256_cfb8,        a8cf3 )
RegisterOsslEVPCipher( aes_256_ecb,         a8e )
RegisterOsslEVPCipher( aes_256_ofb,         a8o )
RegisterOsslEVPCipher( bf_cbc,              bc )
RegisterOsslEVPCipher( bf_cfb64,            bcf6 )
RegisterOsslEVPCipher( bf_ecb,              be )
RegisterOsslEVPCipher( bf_ofb,              bo )
RegisterOsslEVPCipher( cast5_cbc,           cc )
RegisterOsslEVPCipher( cast5_cfb64,         ccf6 )
RegisterOsslEVPCipher( cast5_ecb,           ce )
RegisterOsslEVPCipher( cast5_ofb,           co )
RegisterOsslEVPCipher( des_cbc,             dc )
RegisterOsslEVPCipher( des_cfb1,            dcf1 )
RegisterOsslEVPCipher( des_cfb64,           dcf6 )
RegisterOsslEVPCipher( des_cfb8,            dcf3 )
RegisterOsslEVPCipher( des_ecb,             de )
RegisterOsslEVPCipher( des_ede,             dd )
RegisterOsslEVPCipher( des_ede3,            dd3 )
RegisterOsslEVPCipher( des_ede3_cbc,        dd3c )
RegisterOsslEVPCipher( des_ede3_cfb1,       dd3cf1 )
RegisterOsslEVPCipher( des_ede3_cfb64,      dd3cf6 )
RegisterOsslEVPCipher( des_ede3_cfb8,       dd3cb3 )
RegisterOsslEVPCipher( des_ede3_ecb,        dd3e )
RegisterOsslEVPCipher( des_ede3_ofb,        dd3o )
RegisterOsslEVPCipher( des_ede_cbc,         ddc )
RegisterOsslEVPCipher( des_ede_cfb64,       ddcf6 )
RegisterOsslEVPCipher( des_ede_ecb,         dde )
RegisterOsslEVPCipher( des_ede_ofb,         ddo )
RegisterOsslEVPCipher( des_ofb,             do )
RegisterOsslEVPCipher( desx_cbc,            dxc )
RegisterOsslEVPCipher( enc_null,            null )
RegisterOsslEVPCipher( rc2_40_cbc,          r240c )
RegisterOsslEVPCipher( rc2_64_cbc,          r264c )
RegisterOsslEVPCipher( rc2_cbc,             r2c )
RegisterOsslEVPCipher( rc2_cfb64,           r2cf6 )
RegisterOsslEVPCipher( rc2_ecb,             r2e )
RegisterOsslEVPCipher( rc2_ofb,             r2o )
RegisterOsslEVPCipher( rc4,                 r4 )
RegisterOsslEVPCipher( rc4_40,              r440 )


} // end at_ossl namespace

