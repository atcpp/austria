//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_types.h
 *
 */

#ifndef x_at_types_h_x
#define x_at_types_h_x 1

#include "at_exports.h"
#include "at_operators.h"
#include "at_assert.h"

#ifndef AT_NO_STRING
#include <string>
typedef std::string       AT_String;
#endif

#include <cstring>
#include <utility>

// Austria namespace
namespace at
{

// FIX: These types are broken.  They make assumptions about the
// platform.
// REV: @@ TODO -- provide compile time asserts if the platform assumptions
// are wrong.

typedef unsigned char Uint8;
typedef signed char Int8;
typedef short Int16;
typedef unsigned short Uint16;
typedef int Int32;
typedef unsigned int Uint32;
typedef long long Int64;
typedef unsigned long long Uint64;

typedef float Real32;
typedef double Real64;

typedef long PtrDiff;

typedef long SizeMem;
extern const SizeMem MaxSizeMem;


// ======== IType ==================================================
/**
 * Clever little template to initialize some basic bariables.
 *
 */

template < typename w_type, typename w_init_type, w_init_type w_val >
class IType
{
    public:

    w_type                                          m_value;

    inline IType()
      : m_value( w_val )
    {
    }

    inline operator w_type & ()
    {
        return m_value;
    }

    inline operator const w_type & () const
    {
        return m_value;
    }

    inline w_type & operator = ( const w_type & i_value )
    {
        m_value = i_value;

        return m_value;
    }

};



// ======== CountElements =============================================
/**
 * CountElements will return the number of elements in an array - the
 * array must be statically typed - this will not work with dynamically
 * allocated arrays.
 *
 * @param i_array The array whose number of elements is returned
 * @return The number of elements in the i_array array
 */

template <typename w_Type, int w_N>
inline int CountElements( w_Type (&i_array)[ w_N ] )
{
    return w_N;
}

template <typename w_Type, int w_N>
char (&CountElementsStaticHelper( w_Type (&i_array)[ w_N ] ))[w_N];

// A statically computed version
#define AT_CountElementsStatic(A) sizeof( ::at::CountElementsStaticHelper(A) )


// ======== ArrayEnd ==================================================
/**
 * Find the "one past the end" of an array type
 *
 *
 * @param i_array The array whose end er find
 * @return nothing
 */

template <typename w_Type, int w_N>
w_Type * ArrayEnd( w_Type (&i_array)[ w_N ] )
{
    return i_array + w_N;
}

// a statically computed version
#define AT_ArrayEnd(A) ( sizeof( ::at::CountElementsStaticHelper(A) ) + (A) )

// ======== ClearStruct =============================================
/**
 * ClearStruct will set all the bits in a struct to zero.
 *
 * @param o_var The variable to be cleared
 * @return The number of elements in the i_array array
 */

template <typename w_Type>
void ClearStruct( w_Type & o_var )
{
    memset( & o_var, 0, sizeof( w_Type ) );
}


// ======== FloorDivide ===============================================
/**
 *  Returns the quotient and modulus that result from dividing the
 *  given numerator by the given denominator, where the quotient is
 *  "floored" (rounded towards negative infinity), and the modulus, if
 *  non-zero, has the same sign as the denominator.  (This differs from
 *  normal C++ integer division, in which the rounding direction and
 *  modulus sign when either or both of the inputs are negative is
 *  implementation-defined, and from the x86 IDIV instruction, in which
 *  quotient is rounded toward zero, and the modulus, if non-zero, has
 *  the same sign as the numerator.)  The property that the quotient
 *  times the denominator plus the modulus equals the numerator is
 *  maintained.
 *
 *  Implemented without jumps for efficiency.
 *
 *  Will work for any built-in signed integral type, as well as custom
 *  types that behave sufficiently similarly to ints.
 *
 *  @return A pair, with the quotient as the first element and the modulus as the second
 */
template< class t_type >
inline std::pair< t_type, t_type > FloorDivide(
    const t_type & i_numerator,
    const t_type & i_denominator
)
{
    t_type l_quotient = i_numerator / i_denominator;
    t_type l_modulus = i_numerator % i_denominator;

    /*
     *  l_mask is -1 if the sign of the modulus needs to be changed,
     *  otherwise 0.
     */
    const t_type l_mask =
        - ( l_modulus && ( ( l_modulus < 0 ) != ( i_denominator < 0 ) ) );

    l_quotient += l_mask;
    l_modulus += i_denominator & l_mask;

    return std::pair< t_type, t_type >( l_quotient, l_modulus );
}


// ======== TimeInterval ==============================================
/**
 * TimeInterval describes a difference between two TimeStamp objects.
 *
 *
 */

class AUSTRIA_EXPORT TimeInterval
  : public CompareOps< TimeInterval >,
    public AddSubtractOps< TimeInterval, TimeInterval >,
    public IntegerMultiplyDivideOps< TimeInterval, Int64 >,
    public ModulusOps< TimeInterval >
{

    public:

    /**
     * The units are 100's of nanoseconds.
     */

    Int64                m_timeinterval;

    inline TimeInterval()
      : m_timeinterval( 0 )
    {
    }

    // ======== TimeInterval ==========================================
    /**
     * TimeInterval constructor - this takes
     *
     */
    inline explicit TimeInterval( const Int64 & i_time )
      : m_timeinterval( i_time )
    {
    }


    // ======== operator== ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return True iff *this == i_rhs.
     */
    inline bool operator==( const TimeInterval & i_rhs ) const
    {
        return m_timeinterval == i_rhs.m_timeinterval;
    }


    // ======== operator< =============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return True iff *this < i_rhs.
     */
    inline bool operator<( const TimeInterval & i_rhs ) const
    {
        return m_timeinterval < i_rhs.m_timeinterval;
    }


    // ======== operator+= ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return A reference to *this.
     */
    inline TimeInterval & operator+=( const TimeInterval & i_rhs )
    {
        m_timeinterval += i_rhs.m_timeinterval;
        return *this;
    }


    // ======== operator-= ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return A reference to *this.
     */
    inline TimeInterval & operator-=( const TimeInterval & i_rhs )
    {
        m_timeinterval -= i_rhs.m_timeinterval;
        return *this;
    }


    // ======== operator- =============================================
    /**
     * Unary - (change sign) of this TimeInterval
     *
     * @return The computed TimeInterval
     */
    inline TimeInterval operator-() const
    {
        return TimeInterval( - m_timeinterval );
    }

    /*  Disambiguation.  Necessary so that operator-( TimeInterval )
     *  isn't hidden by the definition of operator-().
     */
    inline TimeInterval operator-( const TimeInterval & i_rhs ) const
    {
        return AddSubtractOps< TimeInterval, TimeInterval >::operator-( i_rhs );
    }


    // ======== operator= ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return A reference to *this.
     */
    inline TimeInterval & operator=( Int64 i_rhs )
    {
        m_timeinterval = i_rhs;
        return *this;
    }


    // ======== operator*= ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return A reference to *this.
     */
    inline TimeInterval & operator*=( Int64 i_rhs )
    {
        m_timeinterval *= i_rhs;
        return *this;
    }


    // ======== operator/= ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return A reference to *this.
     */
    inline TimeInterval & operator/=( Int64 i_rhs )
    {
        m_timeinterval =
            FloorDivide( m_timeinterval, i_rhs ).first;
        return *this;
    }


    // ======== operator/ =============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return The result of the division.
     */
    inline Int64 operator/( const TimeInterval & i_rhs ) const
    {
        return FloorDivide( m_timeinterval, i_rhs.m_timeinterval ).first;
    }

    /*  Disambiguation.  Necessary so that operator/( Int64 ) isn't
     *  hidden by the definition of operator/( TimeInterval ).
     */
    inline TimeInterval operator/( const Int64 & i_rhs ) const
    {
        return IntegerMultiplyDivideOps< TimeInterval, Int64 >::operator/( i_rhs );
    }


    // ======== operator%= ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return A reference to *this.
     */
    inline TimeInterval & operator%=( const TimeInterval & i_rhs )
    {
        m_timeinterval =
            FloorDivide( m_timeinterval, i_rhs.m_timeinterval ).second;
        return *this;
    }


    enum {
        PerSec = 10000000,
        PerMilliSec = 10000,
        PerMicroSec = 10,
        NanoSecPer = 100
    };

    // ======== Secs ==================================================
    /**
     * Return this time in units of seconds, rounding down (away from
     * zero if the time interval is negative).  Remainder (always
     * non-negative) stored in optional output argument.
     *
     * @return This time in seconds
     */
    inline Int64 Secs( TimeInterval * o_remainder = 0 ) const
    {
        std::pair< Int64, Int64 > l_p =
            FloorDivide< Int64 >( m_timeinterval, PerSec );
        if ( o_remainder )
        {
            *o_remainder = l_p.second;
        }
        return l_p.first;
    }

    // ======== MilliSecs =============================================
    /**
     * Return this time in units of milliseconds, rounding down (away
     * from zero if the time interval is negative).  Remainder (always
     * non-negative) stored in optional output argument.
     *
     * @return This time in milliseconds
     */
    inline Int64 MilliSecs( TimeInterval * o_remainder = 0 ) const
    {
        std::pair< Int64, Int64 > l_p =
            FloorDivide< Int64 >( m_timeinterval, PerMilliSec );
        if ( o_remainder )
        {
            *o_remainder = l_p.second;
        }
        return l_p.first;
    }

    // ======== MicroSecs =============================================
    /**
     * Return this time in units of microseconds, rounding down (away
     * from zero if the time interval is negative).  Remainder (always
     * non-negative) stored in optional output argument.
     *
     * @return This time in microseconds
     */
    inline Int64 MicroSecs( TimeInterval * o_remainder = 0 ) const
    {
        std::pair< Int64, Int64 > l_p =
            FloorDivide< Int64 >( m_timeinterval, PerMicroSec );
        if ( o_remainder )
        {
            *o_remainder = l_p.second;
        }
        return l_p.first;
    }

    // ======== NanoSecs ==============================================
    /**
     * Return this time in units of nano seconds.
     *
     * @return This time in nanoseconds
     */
    inline Int64 NanoSecs() const
    {
        return m_timeinterval * NanoSecPer;
    }


    // ======== Secs ==================================================
    /**
     * Return a TimeInverval equal to the given number of seconds.
     *
     * @param i_secs The number of seconds.
     * @return The computed TimeInterval.
     */
    inline static TimeInterval Secs( Int64 i_secs )
    {
        return TimeInterval( i_secs * PerSec );
    }

    // ======== MilliSecs =============================================
    /**
     * Return a TimeInverval equal to the given number of milliseconds.
     *
     * @param i_millisecs The number of milliseconds.
     * @return The computed TimeInterval.
     */
    inline static TimeInterval MilliSecs( Int64 i_millisecs )
    {
        return TimeInterval( i_millisecs * PerMilliSec );
    }

    // ======== MicroSecs =============================================
    /**
     * Return a TimeInverval equal to the given number of microseconds.
     *
     * @param i_microsecs The number of microseconds.
     * @return The computed TimeInterval.
     */
    inline static TimeInterval MicroSecs( Int64 i_microsecs )
    {
        return TimeInterval( i_microsecs * PerMicroSec );
    }

    // ======== NanoSecs ==============================================
    /**
     * Return the TimeInverval closest to the given number of
     * nanoseconds, rounding down (away from zero if the given number
     * of nanoseconds is negative).
     *
     * @param i_nanosecs The number of nanoseconds.
     * @return The computed TimeInterval.
     */
    inline static TimeInterval NanoSecs( Int64 i_nanosecs )
    {
        return TimeInterval(
            FloorDivide< Int64 >( i_nanosecs, NanoSecPer ).first
        );
    }

};


// ======== operator* =================================================
/**
 *  @param i_lhs The left-hand-side operand.
 *  @param i_rhs The right-hand-side operand.
 *  @return The result of the multiplication.
 */
inline TimeInterval operator*(
    Int64  i_lhs,
    const TimeInterval  & i_rhs
)
{
    return i_rhs * i_lhs;
}


// ======== TimeStamp ================================================
/**
 * TimeStamp is an absolute time.  TimeStamp co-operates with
 * TimeInterval and creates a strict meaning of time.
 *
 */

class AUSTRIA_EXPORT TimeStamp
  : public CompareOps< TimeStamp >,
    public AddSubtractOps< TimeStamp, TimeInterval >
{

    public:


    /**
     * m_time is basically the same as a Windows filesystem time.  The
     * units are 100's of nanoseconds since 1-Jan-1601.   This
     * format is much more interesting since the precision is fine while
     * it will be quite some time before we run into a problem with overflow.
     */

    Int64                m_time;


    inline TimeStamp()
      : m_time( 0 )
    {
    }


    // ======== TimeStamp =============================================
    /**
     * Constructing a TimeStamp needs to be explicit so that conversions
     * from integers do not happen by accident.
     *
     * @param i_time The value being set
     */
    inline explicit TimeStamp( Int64  i_time )
      : m_time( i_time )
    {
    }


    // ======== operator== ============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return True iff *this == i_rhs.
     */
    inline bool operator==( const TimeStamp  & i_rhs ) const
    {
        return m_time == i_rhs.m_time;
    }

    // ======== operator< =============================================
    /**
     *  @param i_rhs The right-hand-side operand.
     *  @return True iff *this < i_rhs.
     */
    inline bool operator<( const TimeStamp  & i_rhs ) const
    {
        return m_time < i_rhs.m_time;
    }


    // ======== operator+= ============================================
    /**
     * Compute a later time (or earlier for negative values of i_rhs)
     * from a TimeStamp value.
     *
     * @param i_rhs The relative time (offset to i_rhs)
     * @return A reference to *this.
     */
    inline TimeStamp & operator+=( const TimeInterval  & i_rhs )
    {
        m_time += i_rhs.m_timeinterval;
        return *this;
    }


    // ======== operator-= ============================================
    /**
     * Compute an earlier time (or later for negative values of i_rhs)
     * from a TimeStamp value.
     *
     * @param i_rhs The relative time (offset to i_rhs)
     * @return A reference to *this.
     */
    inline TimeStamp & operator-=( const TimeInterval  & i_rhs )
    {
        m_time -= i_rhs.m_timeinterval;
        return *this;
    }


    // ======== operator- =============================================
    /**
     * The difference of two TimeStamp objects creates a TimeInterval
     * object.
     *
     * @param i_rhs The absolute time on the right hand side of -
     * @return A difference of the times as a TimeInterval
     */
    inline TimeInterval operator-( const TimeStamp & i_rhs ) const
    {
        return TimeInterval( m_time - i_rhs.m_time );
    }

    /*  Disambiguation.  Necessary so that operator-( TimeInterval )
     *  isn't hidden by the definition of operator-( TimeStamp ).
     */
    inline TimeStamp operator-( const TimeInterval & i_rhs ) const
    {
        return AddSubtractOps< TimeStamp, TimeInterval >::operator-( i_rhs );
    }


};


// ======== operator+ =================================================
/**
 * Compute an later time (or earlier for negative values of i_rhs)
 * from a TimeStamp value.
 *
 * @param i_lhs The absolute time on the left hand side of +
 * @param i_rhs The relative time (offset to i_rhs)
 * @return The computed time
 */
inline TimeStamp operator+(
    const TimeInterval  & i_lhs,
    const TimeStamp     & i_rhs
)
{
    return i_rhs + i_lhs;
}


// ======== ConversionOverflow ========================================
/**
 * This is used to determine convert from one type to another but dealing
 * with a conversion factor.  This is used when converting from
 * a TimeInterval to a timeval (or timespec). The type of the conversion
 * is determined by the template resolution.
 *
 * @param o_val Is the result value
 * @param i_input Is the input value
 * @param i_ret_conversion The return conversion factor
 * @return Overflow divided by i_ret_conversion
 */

template <typename w_res_t, typename w_in_t>
inline w_in_t ConversionOverflow(
    w_res_t                 & o_val,
    const w_in_t              i_input,
    w_in_t                    i_ret_conversion
) {
    w_res_t                   l_res = static_cast<w_res_t>( i_input );

    // check for overflow
    if ( l_res == i_input )
    {
        // check if l_res is too large
        if ( l_res < i_ret_conversion )
        {
            o_val = l_res;
            return 0;
        }
    }

    o_val = static_cast<w_res_t>( i_input % i_ret_conversion );

    return i_input / i_ret_conversion;
}



// ======== NonConst ==================================================
/**
 * NonConst<T>::t_Type will be the "non const" of type T.  i.e.
 * NonConst< const int >::t_Type is int.  Similarly,
 * NonConst< int >::t_Type is also int.
 */

template <typename w_Type>
class NonConst
{
    public:

    typedef w_Type              t_Type;
};

template <typename w_Type>
class NonConst< const w_Type >
{
    public:

    typedef w_Type              t_Type;
};

// ======== NonVolatile ==================================================
/**
 * NonVolatile<T>::t_Type will be the "non const" of type T.  i.e.
 * NonVolatile< const int >::t_Type is int.  Similarly,
 * NonVolatile< int >::t_Type is also int.
 */

template <typename w_Type>
class NonVolatile
{
    public:

    typedef w_Type              t_Type;
};

template <typename w_Type>
class NonVolatile< volatile w_Type >
{
    public:

    typedef w_Type              t_Type;
};


// ======== NonConstVolatile ==========================================
/**
 * NonConstVolatile<T>::t_Type be the "non const' and 'non volatile'
 * of the parameter type w_Type
 *
 */

template <typename w_Type>
class NonConstVolatile
{
    public:

    typedef typename NonVolatile< typename NonConst< w_Type >::t_Type >::t_Type t_Type;
};


// ======== DereferenceTraits =========================================
/**
 * DereferenceTraits<T>::m_size is the size of the object that
 * T dereferences to.  This will allow the case where T is a smart
 * pointer.
 *
 */

template <typename w_Type>
class DereferenceTraits
{
    protected:
    // only used to understand the function
    static w_Type NonImplementedFunction();
    
    public:

    enum
    {
        m_sizeof = sizeof( * NonImplementedFunction() )
    };
};


// ======== FalseStaticValue ==========================================
/**
 * FalseStaticValue is used in templates to denote a false value
 */

struct FalseStaticValue
{
    enum { m_value = 0 };
};


// ======== TrueStaticValue ==========================================
/**
 * FalseStaticValue is used in templates to denote a false value
 */

struct TrueStaticValue
{
    enum { m_value = 1 };
};


// ======== StaticTypeInfo ============================================
/**
 * Static type information template.  This may be extended in the
 * future.
 */

template <typename w_Type>
struct StaticTypeInfo
{
    typedef FalseStaticValue    t_is_integer;
    typedef FalseStaticValue    t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef double				t_floating_eqiv;
};

template <typename w_Type> struct StaticTypeInfo< const w_Type >
{
    typedef typename StaticTypeInfo< w_Type >::t_is_integer t_is_integer;
    typedef typename StaticTypeInfo< w_Type >::t_is_numeric t_is_numeric;
    typedef typename StaticTypeInfo< w_Type >::t_is_floating t_is_floating;
    typedef const typename StaticTypeInfo< w_Type >::t_unsigned_of t_unsigned_of;
};

template <> struct StaticTypeInfo< char >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned char       t_unsigned_of;
    typedef float				t_floating_eqiv;
};

template <> struct StaticTypeInfo< unsigned char >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned char       t_unsigned_of;
    typedef float				t_floating_eqiv;
};

template <> struct StaticTypeInfo< signed char >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned char       t_unsigned_of;
    typedef float				t_floating_eqiv;
};

template <> struct StaticTypeInfo< short >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned short      t_unsigned_of;
    typedef float				t_floating_eqiv;
};

template <> struct StaticTypeInfo< unsigned short >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned short      t_unsigned_of;
    typedef float				t_floating_eqiv;
};

template <> struct StaticTypeInfo< int >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned int        t_unsigned_of;
    typedef double				t_floating_eqiv;
};

template <> struct StaticTypeInfo< unsigned int >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned int        t_unsigned_of;
    typedef double				t_floating_eqiv;
};

template <> struct StaticTypeInfo< long >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned long       t_unsigned_of;
    typedef double				t_floating_eqiv;
};

template <> struct StaticTypeInfo< unsigned long >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned long       t_unsigned_of;
    typedef double				t_floating_eqiv;
};

template <> struct StaticTypeInfo< long long >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned long long  t_unsigned_of;
    typedef double				t_floating_eqiv;
};

template <> struct StaticTypeInfo< unsigned long long >
{
    typedef TrueStaticValue     t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef FalseStaticValue    t_is_floating;
    typedef unsigned long long  t_unsigned_of;
    typedef double				t_floating_eqiv;
};

template <> struct StaticTypeInfo< float >
{
    typedef FalseStaticValue    t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef TrueStaticValue     t_is_floating;
    typedef float				t_floating_eqiv;
};

template <> struct StaticTypeInfo< double >
{
    typedef FalseStaticValue    t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef TrueStaticValue     t_is_floating;
    typedef double				t_floating_eqiv;
};

template <> struct StaticTypeInfo< long double >
{
    typedef FalseStaticValue    t_is_integer;
    typedef TrueStaticValue     t_is_numeric;
    typedef TrueStaticValue     t_is_floating;
    typedef long double			t_floating_eqiv;
};

// ======== IsInteger =================================================
/**
 * IsInteger is a template class that can be used to determine if
 * a type is one of the base integer types.
 */

template <typename w_Type>
struct IsInteger
{
    enum { m_value = StaticTypeInfo< w_Type >::t_is_integer::m_value };
};

// ======== IsNumeric =================================================
/**
 * IsNumeric is a template class that can be used to determine if
 * a type is one of the base numeric types
 */

template <typename w_Type>
struct IsNumeric
{
    enum { m_value = StaticTypeInfo< w_Type >::t_is_numeric::m_value };
};

// ======== IsFloating =================================================
/**
 * IsFloating is a template class that can be used to determine if
 * a type is one of the base floating point types
 */

template <typename w_Type>
struct IsFloating
{
    enum { m_value = StaticTypeInfo< w_Type >::t_is_floating::m_value };
};


// ======== IsSame ====================================================
/**
 * IsSame checks for equality of type
 *
 */

template <typename w_Lhs, typename Rhs>
struct IsSame
{
    enum { m_value = false };
};

template <typename w_Type>
struct IsSame<w_Type,w_Type>
{
    enum { m_value = true };
};

    
// ======== TypeSelect ================================================
/**
 * This is like an if/else selection of a type based on a bool.
 *
 */
// by default do the true case and specialize the false case.
template <bool w_select, typename w_T1, typename w_T2>
class TypeSelect   
{
    public:
    
    typedef w_T1    t_Type;
};
     
// specialization for the "False" case
template <typename w_T1, typename w_T2>
class TypeSelect< false, w_T1, w_T2 >
{
    public:
        
    typedef w_T2    t_Type;
};
        

// ======== Pair ======================================================
/**
 * Pair defines an class that is a pair of members and is avalable
 * as a 
 *
 */

template <typename w_T1, typename w_T2>
class Pair
{
    public:

    /**
     * Pair constructor
     *
     */
    Pair(
        const w_T1          & i_value_1,
        const w_T2          & i_value_2
    )
      : m_value_1( i_value_1 ),
        m_value_2( i_value_2 )
    {
    }

    w_T1                        m_value_1;
    w_T2                        m_value_2;

};

template <typename w_LhsT1, typename w_LhsT2, typename w_RhsT1, typename w_RhsT2>
bool operator == ( const Pair< w_LhsT1, w_LhsT2 > & i_lhs, const Pair< w_RhsT1, w_RhsT2 > & i_rhs )
{
    return ( i_lhs.m_value_1 == i_rhs.m_value_1 ) && ( i_lhs.m_value_2 == i_rhs.m_value_2 );
}

template <typename w_LhsT1, typename w_LhsT2, typename w_RhsT1, typename w_RhsT2>
bool operator < ( const Pair< w_LhsT1, w_LhsT2 > & i_lhs, const Pair< w_RhsT1, w_RhsT2 > & i_rhs )
{
    if ( i_lhs.m_value_1 < i_rhs.m_value_1 )
    {
        return true;
    }
    
    if ( i_rhs.m_value_1 < i_lhs.m_value_1 )
    {
        return false;
    }
    
    return ( i_lhs.m_value_2 < i_rhs.m_value_2 );
}

}; // namespace at


// ======== operator<< ================================================
/**
 * This is the std::ostream::operator<< for Pair objects.
 *
 *
 */

namespace std {

template<
    typename        w_char_type,
    class           w_traits,
    typename        w_T1,
    typename        w_T2
>                   
basic_ostream<w_char_type, w_traits>& operator << (
    basic_ostream<w_char_type, w_traits>        & o_ostream,
    const at::Pair<w_T1,w_T2>                   & i_value
) {
    return o_ostream << i_value.m_value_1 << ':' << i_value.m_value_2;
    
} // end basic_ostream<w_char_type, w_traits>& operator <<

} // namespace std

namespace at {
//

// ======== Promote ===============================================
/**
 * This template allows you to determine the type that the compiler
 * will promote an arithmetic expression to.
 */


template <typename T1, typename T2>
struct Promote
{
};

// the same types are the same ! cool

template <typename T1>
struct Promote<T1,T1>
{
     typedef T1 type;
};

// specify all the type promotions ...

template <> struct Promote<unsigned char,char> { typedef int type; };
template <> struct Promote<signed char,char> { typedef int type; };
template <> struct Promote<short,char> { typedef int type; };
template <> struct Promote<unsigned short,char> { typedef int type; };
template <> struct Promote<int,char> { typedef int type; };
template <> struct Promote<unsigned int,char> { typedef unsigned int type; };
template <> struct Promote<long,char> { typedef long type; };
template <> struct Promote<unsigned long,char> { typedef unsigned long type; };
template <> struct Promote<long long,char> { typedef long long type; };
template <> struct Promote<unsigned long long,char> { typedef unsigned long long type; };
template <> struct Promote<float,char> { typedef float type; };
template <> struct Promote<double,char> { typedef double type; };
template <> struct Promote<long double,char> { typedef long double type; };
template <> struct Promote<char,unsigned char> { typedef int type; };
template <> struct Promote<signed char,unsigned char> { typedef int type; };
template <> struct Promote<short,unsigned char> { typedef int type; };
template <> struct Promote<unsigned short,unsigned char> { typedef int type; };
template <> struct Promote<int,unsigned char> { typedef int type; };
template <> struct Promote<unsigned int,unsigned char> { typedef unsigned int type; };
template <> struct Promote<long,unsigned char> { typedef long type; };
template <> struct Promote<unsigned long,unsigned char> { typedef unsigned long type; };
template <> struct Promote<long long,unsigned char> { typedef long long type; };
template <> struct Promote<unsigned long long,unsigned char> { typedef unsigned long long type; };
template <> struct Promote<float,unsigned char> { typedef float type; };
template <> struct Promote<double,unsigned char> { typedef double type; };
template <> struct Promote<long double,unsigned char> { typedef long double type; };
template <> struct Promote<char,signed char> { typedef int type; };
template <> struct Promote<unsigned char,signed char> { typedef int type; };
template <> struct Promote<short,signed char> { typedef int type; };
template <> struct Promote<unsigned short,signed char> { typedef int type; };
template <> struct Promote<int,signed char> { typedef int type; };
template <> struct Promote<unsigned int,signed char> { typedef unsigned int type; };
template <> struct Promote<long,signed char> { typedef long type; };
template <> struct Promote<unsigned long,signed char> { typedef unsigned long type; };
template <> struct Promote<long long,signed char> { typedef long long type; };
template <> struct Promote<unsigned long long,signed char> { typedef unsigned long long type; };
template <> struct Promote<float,signed char> { typedef float type; };
template <> struct Promote<double,signed char> { typedef double type; };
template <> struct Promote<long double,signed char> { typedef long double type; };
template <> struct Promote<char,short> { typedef int type; };
template <> struct Promote<unsigned char,short> { typedef int type; };
template <> struct Promote<signed char,short> { typedef int type; };
template <> struct Promote<unsigned short,short> { typedef int type; };
template <> struct Promote<int,short> { typedef int type; };
template <> struct Promote<unsigned int,short> { typedef unsigned int type; };
template <> struct Promote<long,short> { typedef long type; };
template <> struct Promote<unsigned long,short> { typedef unsigned long type; };
template <> struct Promote<long long,short> { typedef long long type; };
template <> struct Promote<unsigned long long,short> { typedef unsigned long long type; };
template <> struct Promote<float,short> { typedef float type; };
template <> struct Promote<double,short> { typedef double type; };
template <> struct Promote<long double,short> { typedef long double type; };
template <> struct Promote<char,unsigned short> { typedef int type; };
template <> struct Promote<unsigned char,unsigned short> { typedef int type; };
template <> struct Promote<signed char,unsigned short> { typedef int type; };
template <> struct Promote<short,unsigned short> { typedef int type; };
template <> struct Promote<int,unsigned short> { typedef int type; };
template <> struct Promote<unsigned int,unsigned short> { typedef unsigned int type; };
template <> struct Promote<long,unsigned short> { typedef long type; };
template <> struct Promote<unsigned long,unsigned short> { typedef unsigned long type; };
template <> struct Promote<long long,unsigned short> { typedef long long type; };
template <> struct Promote<unsigned long long,unsigned short> { typedef unsigned long long type; };
template <> struct Promote<float,unsigned short> { typedef float type; };
template <> struct Promote<double,unsigned short> { typedef double type; };
template <> struct Promote<long double,unsigned short> { typedef long double type; };
template <> struct Promote<char,int> { typedef int type; };
template <> struct Promote<unsigned char,int> { typedef int type; };
template <> struct Promote<signed char,int> { typedef int type; };
template <> struct Promote<short,int> { typedef int type; };
template <> struct Promote<unsigned short,int> { typedef int type; };
template <> struct Promote<unsigned int,int> { typedef unsigned int type; };
template <> struct Promote<long,int> { typedef long type; };
template <> struct Promote<unsigned long,int> { typedef unsigned long type; };
template <> struct Promote<long long,int> { typedef long long type; };
template <> struct Promote<unsigned long long,int> { typedef unsigned long long type; };
template <> struct Promote<float,int> { typedef float type; };
template <> struct Promote<double,int> { typedef double type; };
template <> struct Promote<long double,int> { typedef long double type; };
template <> struct Promote<char,unsigned int> { typedef unsigned int type; };
template <> struct Promote<unsigned char,unsigned int> { typedef unsigned int type; };
template <> struct Promote<signed char,unsigned int> { typedef unsigned int type; };
template <> struct Promote<short,unsigned int> { typedef unsigned int type; };
template <> struct Promote<unsigned short,unsigned int> { typedef unsigned int type; };
template <> struct Promote<int,unsigned int> { typedef unsigned int type; };
template <> struct Promote<long,unsigned int> { typedef unsigned long type; };
template <> struct Promote<unsigned long,unsigned int> { typedef unsigned long type; };
template <> struct Promote<long long,unsigned int> { typedef long long type; };
template <> struct Promote<unsigned long long,unsigned int> { typedef unsigned long long type; };
template <> struct Promote<float,unsigned int> { typedef float type; };
template <> struct Promote<double,unsigned int> { typedef double type; };
template <> struct Promote<long double,unsigned int> { typedef long double type; };
template <> struct Promote<char,long> { typedef long type; };
template <> struct Promote<unsigned char,long> { typedef long type; };
template <> struct Promote<signed char,long> { typedef long type; };
template <> struct Promote<short,long> { typedef long type; };
template <> struct Promote<unsigned short,long> { typedef long type; };
template <> struct Promote<int,long> { typedef long type; };
template <> struct Promote<unsigned int,long> { typedef unsigned long type; };
template <> struct Promote<unsigned long,long> { typedef unsigned long type; };
template <> struct Promote<long long,long> { typedef long long type; };
template <> struct Promote<unsigned long long,long> { typedef unsigned long long type; };
template <> struct Promote<float,long> { typedef float type; };
template <> struct Promote<double,long> { typedef double type; };
template <> struct Promote<long double,long> { typedef long double type; };
template <> struct Promote<char,unsigned long> { typedef unsigned long type; };
template <> struct Promote<unsigned char,unsigned long> { typedef unsigned long type; };
template <> struct Promote<signed char,unsigned long> { typedef unsigned long type; };
template <> struct Promote<short,unsigned long> { typedef unsigned long type; };
template <> struct Promote<unsigned short,unsigned long> { typedef unsigned long type; };
template <> struct Promote<int,unsigned long> { typedef unsigned long type; };
template <> struct Promote<unsigned int,unsigned long> { typedef unsigned long type; };
template <> struct Promote<long,unsigned long> { typedef unsigned long type; };
template <> struct Promote<long long,unsigned long> { typedef long long type; };
template <> struct Promote<unsigned long long,unsigned long> { typedef unsigned long long type; };
template <> struct Promote<float,unsigned long> { typedef float type; };
template <> struct Promote<double,unsigned long> { typedef double type; };
template <> struct Promote<long double,unsigned long> { typedef long double type; };
template <> struct Promote<char,long long> { typedef long long type; };
template <> struct Promote<unsigned char,long long> { typedef long long type; };
template <> struct Promote<signed char,long long> { typedef long long type; };
template <> struct Promote<short,long long> { typedef long long type; };
template <> struct Promote<unsigned short,long long> { typedef long long type; };
template <> struct Promote<int,long long> { typedef long long type; };
template <> struct Promote<unsigned int,long long> { typedef long long type; };
template <> struct Promote<long,long long> { typedef long long type; };
template <> struct Promote<unsigned long,long long> { typedef long long type; };
template <> struct Promote<unsigned long long,long long> { typedef unsigned long long type; };
template <> struct Promote<float,long long> { typedef float type; };
template <> struct Promote<double,long long> { typedef double type; };
template <> struct Promote<long double,long long> { typedef long double type; };
template <> struct Promote<char,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<unsigned char,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<signed char,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<short,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<unsigned short,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<int,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<unsigned int,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<long,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<unsigned long,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<long long,unsigned long long> { typedef unsigned long long type; };
template <> struct Promote<float,unsigned long long> { typedef float type; };
template <> struct Promote<double,unsigned long long> { typedef double type; };
template <> struct Promote<long double,unsigned long long> { typedef long double type; };
template <> struct Promote<char,float> { typedef float type; };
template <> struct Promote<unsigned char,float> { typedef float type; };
template <> struct Promote<signed char,float> { typedef float type; };
template <> struct Promote<short,float> { typedef float type; };
template <> struct Promote<unsigned short,float> { typedef float type; };
template <> struct Promote<int,float> { typedef float type; };
template <> struct Promote<unsigned int,float> { typedef float type; };
template <> struct Promote<long,float> { typedef float type; };
template <> struct Promote<unsigned long,float> { typedef float type; };
template <> struct Promote<long long,float> { typedef float type; };
template <> struct Promote<unsigned long long,float> { typedef float type; };
template <> struct Promote<double,float> { typedef double type; };
template <> struct Promote<long double,float> { typedef long double type; };
template <> struct Promote<char,double> { typedef double type; };
template <> struct Promote<unsigned char,double> { typedef double type; };
template <> struct Promote<signed char,double> { typedef double type; };
template <> struct Promote<short,double> { typedef double type; };
template <> struct Promote<unsigned short,double> { typedef double type; };
template <> struct Promote<int,double> { typedef double type; };
template <> struct Promote<unsigned int,double> { typedef double type; };
template <> struct Promote<long,double> { typedef double type; };
template <> struct Promote<unsigned long,double> { typedef double type; };
template <> struct Promote<long long,double> { typedef double type; };
template <> struct Promote<unsigned long long,double> { typedef double type; };
template <> struct Promote<float,double> { typedef double type; };
template <> struct Promote<long double,double> { typedef long double type; };
template <> struct Promote<char,long double> { typedef long double type; };
template <> struct Promote<unsigned char,long double> { typedef long double type; };
template <> struct Promote<signed char,long double> { typedef long double type; };
template <> struct Promote<short,long double> { typedef long double type; };
template <> struct Promote<unsigned short,long double> { typedef long double type; };
template <> struct Promote<int,long double> { typedef long double type; };
template <> struct Promote<unsigned int,long double> { typedef long double type; };
template <> struct Promote<long,long double> { typedef long double type; };
template <> struct Promote<unsigned long,long double> { typedef long double type; };
template <> struct Promote<long long,long double> { typedef long double type; };
template <> struct Promote<unsigned long long,long double> { typedef long double type; };
template <> struct Promote<float,long double> { typedef long double type; };
template <> struct Promote<double,long double> { typedef long double type; };


// ======== NonCopiable ===============================================
/**
 * NonCopiable makes a type non copyable.
 *
 */

class AUSTRIA_EXPORT NonCopiable
{
    public:

    inline NonCopiable() {}
    
    private:

    /**
     * NonCopiable
     *
     */
    NonCopiable( const NonCopiable & );
    NonCopiable & operator=( const NonCopiable & );
};


}; // namespace

#endif // x_at_types_h_x


