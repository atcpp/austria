//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
//
//
// at_url_parse.h
//
//
//

#ifndef x_at_url_parse_h_x
#define x_at_url_parse_h_x

#include "at_exports.h"
#include <string>

// Austria namespace
namespace at
{

// ======== UrlString ==============================================
/**
 *  UrlString is a std::string but also contains a flag to indicate
 *  if it is set or not.  Hence a null string vs an empty string.
 *  It contains a couple of methods to automatically set the 
 *  m_is_set flag but is by no means complete.  It is used as
 *  a way for the UrlParser to indicate if a field is set
 *  or not.
 */

class AUSTRIA_EXPORT UrlString
  : public std::string
{
public:

    bool            m_is_set;

    UrlString( const std::string & i_string )
      : std::string( i_string ),
        m_is_set( true )
    {
    }

    UrlString()
      : m_is_set( false )
    {
    }

    UrlString & assign( UrlString::const_iterator i1, UrlString::const_iterator i2 )
    {
        m_is_set = true;
        std::string::assign( i1, i2 );

        return * this;
    }

    UrlString & operator=( const std::string & i_str )
    {
        m_is_set = true;
        std::string::assign( i_str );

        return * this;
    }

    /**
     * UrlDecode
     * This will decode this string replacing \%xx and + characters
     * with the pre-encoded equivalents.
     */

    void UrlDecode();

    /**
	 * UrlEncode
     * This will encode this string replacing characters that need to be escaped with their
	 * \%xx equivalents.
	 */

	void UrlEncode();

    UrlString & assign_encoded( const std::string & i_str )
    {
        * this = i_str;
        UrlDecode();
        return * this;
    }
    
    UrlString & assign_encoded( UrlString::const_iterator i1, UrlString::const_iterator i2 )
    {
        assign( i1, i2 );
        UrlDecode();
        return * this;
    }
    
};


// ======== UrlParser ============================================
/**
 *      Parsing class for the basic elemnts of a network URL
 *
 * See RFC 1738:
 *
 */

class AUSTRIA_EXPORT UrlParser
{
public:

    UrlParser();

    UrlParser(
        const UrlString      & i_url,
        std::string             * o_error_message = 0
    );

    UrlParser(
        const char              * i_url,
        std::string             * o_error_message = 0
    );
        
    /**
     * Parse the passed in URL.
     *
     * @param url is the url string to be parsed
     * @return true if the url parsing was successful
     */

    bool Parse(
        const UrlString      & i_url,
        std::string             * o_error_message = 0
    );

    bool Parse(
        const char              * url,
        std::string             * o_error_message = 0
    );

    
    /**
     * CombineHostURL
     * Combine this URL with the URL of the hosturl.  This merges
     * 2 urls as though they are the normalized or host (hosturl) of a
     * web page and an embedded (this) url in a web page.
     *
     * @param hosturl the normalized url to fill in the blanks of this.
     */

    void CombineHostURL( const UrlParser & hosturl );


    /**
     * WriteURL
     * Write a URL given the data in this URL object
     *
     * @return a string of the generated url.
     */

    std::string WriteURL();
    
    enum {
        InitialDefaultPortNo = 80
    };

    UrlString                m_scheme;
    UrlString                m_host;
    UrlString                m_port;
    UrlString                m_user;
    UrlString                m_pass;
    UrlString                m_path;

    UrlString                m_parse_error;


};

}; // namespace

#endif // x_at_url_parse_h_x
