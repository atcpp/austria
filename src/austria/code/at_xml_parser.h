// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_parser.h
 *
 * \author Guido Gherardi
 *
 * The Austria SAX parser.
 * 
 */

#ifndef x_at_xml_parser_h_x
#define x_at_xml_parser_h_x 1

#include "at_xml_document.h"
#include "at_file_status_report_basic.h"
#include <list>

// Austria namespace
namespace at
{
    /**
     *  Parses an in-file XML document.
     *  @param i_filename The name of the file containing the XML document.
     *  @param o_errlist A reference to a list of file status reports that is
     *                   updated with (if any) parsing error or warning.
     *  @return A pointer to the resulting XmlDocument.
     */
    PtrDelegate< XmlDocument* > XmlParseFile(
        const char*                             i_filename,
        std::list< FileStatusReport_Basic >*    o_errlist
        );

    /**
     *  Parses an in-memory XML document.
     *  @param i_buffer An in-memory XML document input.
     *  @param i_size The length of the XML document in bytes.
     *  @param o_errlist A pointer to a list of file status reports that is
     *                   updated with parsing errors and warnings.
     *  @return A pointer to the resulting XmlDocument.
     */
    PtrDelegate< XmlDocument* > XmlParseMemory(
        const char*                             i_buffer,
        size_t                                  i_size,
        std::list< FileStatusReport_Basic >*    o_errlist
        );

    /**
     *  @deprecated Parses an XML file.
     *  @param  i_filename  The name of the file to parse.
     *  @return A pointer to the resulting XmlDocument.
     *  @exception ExceptionDerivation<XmlParserErr> if a parser error occurs.
     */
    PtrDelegate< XmlDocument* > XmlParseFile( const char* i_filename );

    /**
     *  @deprecated Parses an XML in-memory buffer.
     *  @param  i_buffer An in-memory XML document input.
     *  @param  i_size   The length of the XML document in bytes.
     *  @return A pointer to the resulting XmlDocument.
     *  @exception ExceptionDerivation<XmlParserErr> if a parser error occurs.
     */
    PtrDelegate< XmlDocument* > XmlParseMemory( const char* i_buffer, size_t i_size );

    /**
     * XmlParserErr is an exception class used with AT_ThrowDerivation.
     *
     * ExceptionDerivation<XmlParserErr> is thrown if a error occurs while parsing an XML document.
     */
    struct XmlParserErr {};

}; // namespace

#endif // x_at_xml_parser_h_x
