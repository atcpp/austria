//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_activity.cpp
 *
 */

#pragma warning (disable : 4355)

#include "at_exports.h"
#include "at_os.h"
#include "at_activity.h"
#include "at_start_up.h"
#include "at_factory.h"

#include <cstdlib>

namespace at
{

// DecrementHelper is used to decrement the
// variable referenced if SetOk is not called
// before the destruction of the DecrementHelper
// object.  This avoids a try-catch block
// to clean things up.

struct DecrementHelper
{
    DecrementHelper( int & o_val )
      : m_val( o_val ),
        m_ok( false )
    {
    }

    ~DecrementHelper()
    {
        if ( ! m_ok )
        {
            -- m_val;
        }
    }

    void SetOk()
    {
        m_ok = true;
    }

    int             & m_val;
    bool              m_ok;
};



// ======== ActivityTools =============================================
/**
 * ActivityTools is a container of various tools.
 *
 */

class ActivityTools
{
    public:

    inline static bool DoVerify()
    {

        static bool    s_do_verify = ( 0 != std::getenv( "ACT_VERIFY" ) );

        return s_do_verify;
    }

    // ======== VerifyQueue ===========================================
    /**
     * Verify linked list integrity 
     *
     * @param i_queue is the tool for verifying 
     * @return nothing
     */

    static void VerifyQueue( PtrView< const ActivityList * > i_al )
    {

        if ( ( ! OSTraitsBase::DebugBuild ) || ! DoVerify() )
        {
            return;
        }
        
        Activity * l_front = i_al->m_queue.m_links[ 0 ];
        int  l_count = 0;

        if ( ! l_front )
        {
            AT_Assert( ! i_al->m_queue.m_links[ 0 ] );
            AT_Assert( 0 == i_al->m_queue_count );
            
            return;
        }

        ++ l_count;

        do {
            
            AT_Assert( l_front->m_state == & Activity::s_enqueued );
        
            Activity * l_next = l_front->m_link.m_links[ 0 ];

            if ( ! l_next )
            {
                AT_Assert( l_front == i_al->m_queue.m_links[ 1 ] );
                break;
            }

            AT_Assert( l_front == l_next->m_link.m_links[ 1 ] );
            
            ++ l_count;
            
            l_front = l_next;

        } while ( true );

        AT_Assert( l_count == int( i_al->m_queue_count ) );
    }
    
};

    
// ======== DefaultThreadPoolCount ====================================
/**
 * DefaultThreadPoolCount determines the maximum number of threads
 * that should be spawned.
 *
 * @return The deafault maximum number of thread in a thread pool.
 */

int DefaultThreadPoolCount()
{
    return 4;
}


// ======== ActivityList ==============================================

ActivityList::ActivityList()
  : m_cond( m_mutex ),
    m_cond_waiters( 0 ),
    m_cond_queued( m_mutex ),
    m_cond_queued_waiters( 0 ),
    m_queue(),
    m_queue_count( 0 ),
    m_terminated( false ),
    m_closed_count( 0 ),
    m_close_in_progress( false ),
    m_suspend_count( 0 ),
    m_perform_in_progress( 0 )
{
}    

    
// ======== PopNext ===============================================

Activity * ActivityList::PopNext()
{
    // The m_mutex is assumed locked !

    Activity * l_front = m_queue.m_links[ 0 ];

    if ( ! l_front )
    {
        return 0;
    }

    ActivityTools::VerifyQueue( this );
    -- m_queue_count;
    Activity * l_after_front = l_front->m_link.m_links[ 0 ];

    if ( ! l_after_front )
    {
        // We have reached the end of the queue

        AT_Assert( m_queue.m_links[ 1 ] == l_front );
        
        // if that's the case we nuke the before and after links
        m_queue.m_links[ 0 ] = 0;
        m_queue.m_links[ 1 ] = 0;

        return l_front;
    }

    // fix up the head and after front links.
    m_queue.m_links[ 0 ] = l_after_front;
    l_after_front->m_link.m_links[ 1 ] = 0;

    ActivityTools::VerifyQueue( this );
    
    return l_front;
}

// ======== ProcessActivity =======================================
void ActivityList::ProcessActivity( Activity * i_act )
{

    // The only way an activity can be in the queue is if it
    // is in the s_enqueued state
    AT_Assert( i_act->m_state == & Activity::s_enqueued );
    
    // The perform method may possibly end up deleting the activity,
    // it it does the element pointed to by m_perform_ptr is set to null.
    
    i_act->m_perform_ptr = & i_act;

    // The thread ID is needed so we can avoid waiting for ourselves
    // and hence avoid deadlock.
    i_act->m_perform_task = Task::GetSelfId();

    // we're in the perform state
    i_act->m_state = & Activity::s_perform;

    // keep on processing this activity until it's not longer
    // in one of the "enqueue" states and it's cancel count is zero.
    do {

        // perform simply performs while it is unlocked ...
        
        if ( i_act->m_state == & Activity::s_perform )
        {
            Unlock<Mutex>           l_unlock( m_mutex );
            i_act->Perform();
        }
        else
        {
            unsigned int l_cancel_count = i_act->m_cancel_count;
            i_act->m_cancel_count = 0;
            
            Unlock<Mutex>           l_unlock( m_mutex );
            i_act->Cancelled( l_cancel_count );
        }
        
        // mutex should be locked again here

        // activity could possiby delete itself (bad karam but need to deal with it
        // in case of activity being deleted i_act becomes null - cute !

        if ( ! i_act )
        {
            break;
        }

        // keep executing this activity while it is enqueued or we have
        // pending cancel counts.  The sum of the number of calls to
        // Enqueue that return true must be equal to the number of
        // times Perform is called plus the sum of the i_count parameter
        // to Cancelled.

        if (
            i_act->m_state == & Activity::s_perform_enqueue
            || i_act->m_state == & Activity::s_cancelling_enqueue
        ) {

            // Can't be enqueued unless the activity is enabled
            AT_Assert( i_act->m_enabled );

            // Can't enqueue unless the activity list is not
            // terminated or closed
            if ( m_terminated || ( m_closed_count > 0 ) )
            {
                ++ ( i_act->m_cancel_count );
                i_act->m_state = & Activity::s_cancelling;

                continue;
            }

            // if there are any cancellations then do that now.
            if ( i_act->m_cancel_count )
            {
                i_act->m_state = & Activity::s_cancelling_enqueue;
                continue;
            }

            // We have an enqueue request pending
            i_act->m_state = & Activity::s_perform;
            continue;
        }
        
        // if there are any cancellations then do that now.
        if ( i_act->m_cancel_count )
        {
            i_act->m_state = & Activity::s_cancelling;
            continue;
        }

        // Processing of the activity is complete

        // is anything waiting ?
        if ( i_act->m_have_waiters )
        {
            // wake them up
            i_act->m_have_waiters = 0;
            i_act->m_condz.PostAll();
        }

        // remove this task as the processor
        // of this activity.

        i_act->m_perform_task = Task::TaskID();
        i_act->m_perform_ptr = 0;

        // back to the start state
        i_act->m_state = & Activity::s_start;

        if( i_act->m_have_waiters )
        {
            i_act->m_have_waiters = 0;
            i_act->m_condz.PostAll();
        }
        
        break;
        
    } while ( true );

}

// ======== Close =================================================

void ActivityList::Close()
{

    Lock<Mutex>         l_lock( m_mutex );

    CloseAlreadyLocked();
    
}

// ======== Open =================================================

void ActivityList::Open()
{

    Lock<Mutex>         l_lock( m_mutex );

    // decrement the closed count
    -- m_closed_count;

    // in theory, there should be nothing to do here because there
    // should be no activities queued.  Worst case, there are activities
    // queued and there is still a close in progress (m_close_in_progress
    // is set), in theory this should not happen.
    
}

// ======== Terminate =============================================

void ActivityList::Terminate()
{

    Lock<Mutex>         l_lock( m_mutex );

    // Set the terminate flag
    m_terminated = true;

    // terminate is like close except the terminate flag is set
    CloseAlreadyLocked();
}

// ======== WaitCompletionLocked ========================================
void ActivityList::WaitCompletionLocked()
{
    
    if ( m_perform_in_progress != 0 )
    {
        ++ m_cond_waiters;
        m_cond.Wait();
    }
}

// ======== WaitCompletion ========================================
void ActivityList::WaitCompletion()
{
    Lock<Mutex>         l_lock( m_mutex );
    
    if ( ( m_perform_in_progress != 0 ) || ( m_queue.m_links[0] && ( m_suspend_count > 0 ) ) )
    {
        ++ m_cond_queued_waiters;
        m_cond_queued.Wait();
    }
}

// ======== ProcessingStarted =====================================

void ActivityList::ProcessingStarted( int i_num_start )
{
    m_perform_in_progress += i_num_start;
}

    
// ======== ProcessingEnded =======================================
void ActivityList::ProcessingEnded()
{
    -- m_perform_in_progress;
    
    if ( m_perform_in_progress == 0 )
    {
        if (
            m_cond_queued_waiters
            && ( ( ! m_queue.m_links[0] ) || ( m_suspend_count <= 0 ) )
        )
        {
            m_cond_queued_waiters = 0;
            m_cond_queued.PostAll();
        }
        
        if ( m_cond_waiters )
        {
            m_cond_waiters = 0;
            m_cond.PostAll();
        }
    }
}

// ======== ProcessingTerminated ==================================
void ActivityList::ProcessingTerminated()
{
    AT_Assert( m_perform_in_progress == 0 );
    
    if ( m_cond_queued_waiters )
    {
        m_cond_queued_waiters = 0;
        m_cond_queued.PostAll();
    }
    
    if ( m_cond_waiters )
    {
        m_cond_waiters = 0;
        m_cond.PostAll();
    }
}

// ======== CloseAlreadyLocked ========================================
void ActivityList::CloseAlreadyLocked()
{
    // m_mutex should be locked here ...

    ++ m_closed_count;
    

    // if this is truly closed
    if ( m_terminated || ( m_closed_count > 0 ) )
    {

        // if there is no close in progress
        if ( ! m_close_in_progress )
        {
            m_close_in_progress = true;
            
            // keep knowledge of wether activity list is suspened
            bool l_is_suspended = m_suspend_count > 0;
            
            ProcessingStarted();

            Activity * l_act;

            // continue the process only while things are closed
            //
            while ( ( m_terminated || ( m_closed_count > 0 ) ) && ( l_act = PopNext() ) )
            {
                AT_Assert( l_act->m_state == & Activity::s_enqueued );
        
                l_act->m_perform_ptr = & l_act;
                l_act->m_perform_task = Task::GetSelfId();
                l_act->m_state = & Activity::s_cancelling;
                ++ ( l_act->m_cancel_count );

                do {
                    

                    // Perform the cancel (and only the cancel)
                    {
                        unsigned int l_cancel_count = l_act->m_cancel_count;
                        l_act->m_cancel_count = 0;
                        
                        Unlock<Mutex>       l_unlock( m_mutex );
                        l_act->Cancelled( l_cancel_count );
                    }
                    
                    // mutex should be locked again here
    
                    // activity could possiby delete itself (bad karam but need to deal with it
                    // in case of activity being deleted l_act becomes null - cute !
    
                    if ( ! l_act )
                    {
                        break;
                    }
    
                    // If this activity is re-enqueued, then this close operation
                    // needs to be terminated and the current activity
                    // needs to be added back into the queue.

                    AT_Assert(
                        ( l_act->m_state == & Activity::s_cancelling )
                        || ( l_act->m_state == & Activity::s_cancelling_enqueue )
                    );

                    // we got enqueued while cancelling and then the activity
                    // list became terminated or closed
                    if (
                        ( l_act->m_state == & Activity::s_cancelling_enqueue )
                        && ( m_terminated || ( m_closed_count > 0 ) )
                    ) {

                        // need to do some more cancelling
                        l_act->m_state = & Activity::s_cancelling;

                        ++ ( l_act->m_cancel_count );

                        continue;
                    }

                    // The m_cancel_count incremented while we were in Cancelled ...
                    // This can happen in weird situations where the activity list
                    // is closed, activity cancelled which opens the activty list
                    // enqueue's itself then disables itself.
                    if ( l_act->m_cancel_count )
                    {
                        continue;
                    }

                    // we're done with "performing" this activity
                    l_act->m_perform_ptr = 0;
                    l_act->m_perform_task = Task::TaskID();
                    
                    // If the activity is truly re-enqueued - need to
                    // put the activity back into the queue.
                    
                    if ( l_act->m_state == & Activity::s_cancelling_enqueue ) {
    
                        // Can't be enqueued unless the activity is enabled
                        AT_Assert( l_act->m_enabled );

                        // Some really hairy code happened here.  This
                        // is a case where some serious open/enqueue
                        // calls in the cancel method.  Not reccomended
                        // but it happens.


                        // do the notification only if it was not suspened
                        if ( ( ! l_is_suspended ) && ( m_suspend_count <= 0 ) )
                        {
                            if ( ! ActivityEnqueuedNotification( 1 ) )
                            {
                                // serious problem here ... for whatever
                                // reason we tried wake up the activity list
                                // thread and it said no way !
    
                                ++ l_act->m_cancel_count;
                                continue;
                            }
                        }
                        
                        ActivityTools::VerifyQueue( this );
    
                        // go ahead and enqueue this.
                        ++ m_queue_count;
                        
                        Activity * l_back = m_queue.m_links[ 1 ];

                        // Is the queue empty ?
                        if ( ! l_back )
                        {
                            // the forward and back pointers should be 
                            m_queue.m_links[ 0 ] = l_act;
                            m_queue.m_links[ 1 ] = l_act;
                            l_act->m_link = ActivityPointers();
                        }
                        else
                        {
                            // place this activity at the end of the queue
                            l_act->m_link.m_links[ 1 ] = 0;
                            l_act->m_link.m_links[ 0 ] = l_back;
                            l_back->m_link.m_links[ 0 ] = l_act;
                            m_queue.m_links[ 1 ] = l_act;
                        }
                                        
                    } else {
                    
                        l_act->m_state = & Activity::s_start;

                        // Must only awake waiters if the activity gets
                        // back into the start state.
                        
                        if ( l_act->m_have_waiters )
                        {
                            // wake up waiters on this activity
                            l_act->m_have_waiters = 0;
                            l_act->m_condz.PostAll();
                        }

                    }

                    ActivityTools::VerifyQueue( this );
                        
                    break;
                    
                } while ( true );
            };
    
            ProcessingEnded();
            m_close_in_progress = false;

            // There was a transition from being suspended
            // to not being suspened. While processing this
            // activity - need to resume.
            if ( m_queue_count && l_is_suspended && ( m_suspend_count <= 0 ) )
            {
                if ( ! ActivityEnqueuedNotification( m_queue_count ) )
                {
                    // TODO : Threads don't like waking up ...
                    // not much we can do
                    // Probably need a special handler
                }
            }

        }

        WaitCompletionLocked();
    }
    
}



// ======== Suspend ===============================================

void ActivityList::Suspend()
{
    Lock<Mutex>         l_lock( m_mutex );

    ++ m_suspend_count;
}


// ======== Resume ================================================

void ActivityList::Resume()
{
    Lock<Mutex>         l_lock( m_mutex );

    -- m_suspend_count;

    // if there is somthing in the queue and the m_suspend_count is 0 again
    // there is not a m_close_in_progress then resume activity processing
    if ( ( m_suspend_count <= 0 ) && m_queue.m_links[ 0 ] && ! m_close_in_progress )
    {
        // if any activities are enqueued, then re-start things ...
        if ( ! ActivityEnqueuedNotification( m_queue_count ) )
        {
            // TODO : Threads don't like waking up ...
            // not much we can do
            // Probably need a special handler
        }
    }
    
}

// ======== GetQueueCount ==============================================

unsigned ActivityList::GetQueueCount ()
{ 
    Lock<Mutex> l_lock( m_mutex );

    return m_queue_count; 
}


// ======== GetOwner ==============================================

PtrDelegate< ActivityListOwner * > ActivityListOwner::GetOwner()
{
    return PtrView< ActivityListOwner * >( this );
}



// ======== Initializer_LibraryPool ===================================
/**
 * Initializer_LibraryPool will initialize the ActivityList_Pool.
 *
 */

class AUSTRIA_EXPORT Initializer_LibraryPool
  : public Initializer
{
public:

    Initializer_LibraryPool( int & argc, const char ** & argv )
    {
        // do start-up things here
        
        Ptr< ActivityList * >  l_al = new ActivityList_Pool();

        m_owner = l_al->GetOwner();
        // Owner now contains a Ptr to the ActivityList so
        // the Ptr( l_al ) will not result in the ActivityList
        // destruction.
    }

    ~Initializer_LibraryPool()
    {
        // do exit things here
    }

    static at::Ptr< ActivityListOwner * >   m_owner;
};

AT_MakeFactory2P( "Initializer_LibraryPool", Initializer_LibraryPool, Initializer, DKy, int & , const char ** & );

at::Ptr< ActivityListOwner * > Initializer_LibraryPool::m_owner;

// ======== LibraryPool ===============================================

PtrDelegate< ActivityList * > LibraryPool()
{
    AT_Assert( Initializer_LibraryPool::m_owner );
    return Initializer_LibraryPool::m_owner->GetActivityList();
}


// ======== ActivityState_Solid =======================================
/**
 *
 *
 */

class ActivityState_Hidden
  : public Activity::ActivityState
{
    public:

    ActivityState_Hidden(
        const char * i_name
    )
      : m_name( i_name )
    {
    }

    const char * String() const
    {
        return m_name;
    }

    const char              * m_name;
};

#define NewState( A ) \
    static ActivityState_Hidden g_ ## A( #A ); \
    const Activity::ActivityState   & Activity::A = g_ ## A; \
// end macro

NewState( s_start )
NewState( s_enqueued )
NewState( s_perform )
NewState( s_perform_enqueue )
NewState( s_cancelling )
NewState( s_cancelling_enqueue )
NewState( s_deleted )

#undef NewState

// ======== Activity ==============================================

Activity::Activity( PtrDelegate< ActivityList * > i_al )
  : m_enabled( true ),
    m_cancel_count( 0 ),
    m_al( i_al ),
    m_link(),
    m_condz( m_al->m_mutex ),
    m_have_waiters( 0 ),
    m_perform_ptr( 0 ),
    m_perform_task(),
    m_state( & s_start )
{
    // the activity had better be there,
    AT_Assert( m_al );
}

// ======== Cancelled =============================================
void Activity::Cancelled( unsigned int i_count )
{
}


// ======== Enqueue ===============================================

Activity::EnqueueStatus Activity::Enqueue()
{
    // Enqueue will perform a lock of the mutex before proceeding

    PtrView< ActivityList * >   l_al = m_al;
    
    Lock<Mutex>                 l_lock( l_al->m_mutex );

    AT_Assert( m_state != & s_deleted );

    // If this activity is not enabled or the activity list is closed
    // (or terminated), then don't enqueue
    
    if ( ! m_enabled )
    {
        return ActivityDisabled;
    }
    
    if ( l_al->m_terminated || ( l_al->m_closed_count > 0 ) )
    {
        return ListClosed;
    }

    // only really enqueue in the start state
    if ( m_state == & s_start )
    {
        // do the notification - this could fail (because the
        // activity list that feeds this one is closed)

        if ( ! l_al->ActivityEnqueuedNotification( 1 ) )
        {
            return ThreadWakeupFailed;
        }

        ActivityTools::VerifyQueue( l_al );
                    
        // go ahead and enqueue this.
        ++ l_al->m_queue_count;
        
        // ok - now we're in enqueued state
        m_state = & s_enqueued;
        
        Activity * l_back = l_al->m_queue.m_links[ 1 ];

        // Is the queue empty ?
        if ( ! l_back )
        {
            // the forward and back pointers should be 
            l_al->m_queue.m_links[ 0 ] = this;
            l_al->m_queue.m_links[ 1 ] = this;
            m_link.m_links[ 1 ] = 0;
            m_link.m_links[ 0 ] = 0;
        }
        else
        {
            // place this activity at the end of the queue
            m_link.m_links[ 0 ] = 0;
            m_link.m_links[ 1 ] = l_back;
            l_back->m_link.m_links[ 0 ] = this;
            l_al->m_queue.m_links[ 1 ] = this;
        }

        ActivityTools::VerifyQueue( l_al );
        
        return ActivityAdded;
    }
    else if ( m_state == & s_perform )
    {
        // All we need to do here is requeue
        m_state = & s_perform_enqueue;
        return ActivityAdded;
    }
    else if ( m_state == & s_cancelling )
    {
        // All we need to do here is requeue
        m_state = & s_cancelling_enqueue;
        return ActivityAdded;
    }

    // don't enqueue in any other states
    return ActivityAlreadyInQueue;
}

// ======== Wait ==================================================

void Activity::Wait()
{
    PtrView< ActivityList * >   l_al = m_al;

    // Need to lock to wait
    Lock<Mutex>             l_lock( l_al->m_mutex );
    
    // no point in waiting if it'a already in the start state
    if ( m_state == & s_start )
    {
        return;
    }

    // a thread can't wait on itself
    if ( m_perform_task == Task::GetSelfId() )
    {
        return;
    }

    while ( m_state != & s_start )
    {
        ++ m_have_waiters;
    
        m_condz.Wait();
    }

    return;
}



// ======== Disable ===============================================

void Activity::Disable( bool i_wait )
{    
    PtrView< ActivityList * >  l_al = m_al;

    // Need to lock before we can do anything
    
    Lock<Mutex>             l_lock( l_al->m_mutex );

    m_enabled = false;
    
    // no point in waiting if it'a already in the start state
    if ( m_state == & s_enqueued )
    {
        
        ActivityTools::VerifyQueue( m_al );
        
        // pulling this out of the linked list
        -- l_al->m_queue_count;
        Activity * l_next = m_link.m_links[ 0 ];
        Activity * l_prev = m_link.m_links[ 1 ];

        // are we at the end ?
        if ( ! l_next )
        {
            // must be the end
            AT_Assert( this == l_al->m_queue.m_links[ 1 ] );

            l_al->m_queue.m_links[ 1 ] = l_prev;
        }
        else
        {
            AT_Assert( this == l_next->m_link.m_links[ 1 ] );
            
            l_next->m_link.m_links[ 1 ] = l_prev;
        }

        // Are we at the beginning
        if ( ! l_prev )
        {
            // must be the end
            AT_Assert( this == l_al->m_queue.m_links[ 0 ] );

            l_al->m_queue.m_links[ 0 ] = l_next;
        }
        else
        {
            AT_Assert( this == l_prev->m_link.m_links[ 0 ] );
            
            l_prev->m_link.m_links[ 0 ] = l_next;
        }
            
        ActivityTools::VerifyQueue( m_al );
        
        // Going back to the start state
        m_state = & s_start;
        
        // wake up waiters ...
        if ( m_have_waiters )
        {
            // wake up waiters on this activity
            m_have_waiters = 0;
            m_condz.PostAll();
        }

        // Nothing to wait for here !
        return;

    }
    else if ( m_state == & s_perform_enqueue )
    {
        ++ m_cancel_count;
        m_state = & s_perform;
    }
    else if ( m_state == & s_cancelling_enqueue )
    {
        ++ m_cancel_count;
        m_state = & s_cancelling;
    }
    else if ( m_state == & s_start )
    {
        return;
    }

    if ( ! i_wait )
    {
        return;
    }
    
    // thread can't wait on itself
    if ( m_perform_task == Task::GetSelfId() )
    {
        return;
    }
    
    ++ m_have_waiters;
    m_condz.Wait();
    
}


// ======== Activity::~Activity =======================================
Activity::~Activity()
{
    // This should wait here if there is some processing
    // underway.
    Wait();

    if ( m_perform_ptr )
    {
        * m_perform_ptr = 0;
    }
}

// ======== Enable ================================================

void Activity::Enable()
{
    PtrView< ActivityList * >  l_al = m_al;

    // Need to lock before we can do anything
    
    Lock<Mutex>             l_lock( l_al->m_mutex );

    m_enabled = true;
}


// ======== Task_ThreadPool =========================================
/**
 * Task_ThreadPool implements the Task of a thread pool.
 *
 */

class AUSTRIA_EXPORT Task_ThreadPool
  : public Task
{
    public:

    /**
     * Task_ThreadPool
     *
     */
    Task_ThreadPool( ThreadList * i_threadlist )
      : m_threadlist( i_threadlist )
    {
        Start();
    }


    // ======== Work ==================================================
    /**
     * This implements the work thread.
     *
     */

    void Work();
    
    /**
     * m_threadlist contains the ThreadList that this
     * thread-pool thread belongs to. 
     */
    ThreadList                      * m_threadlist;

};

// ======== ThreadList ================================================
/**
 * Threadlist manages a list of threads for the ActivityList_Pool
 * implementation.
 *
 */

class AUSTRIA_EXPORT ThreadList
{
    public:

    /**
     * ThreadList takes a maximum count of threads to spawn.
     *
     */
    ThreadList(
        PtrView< ActivityList * >       i_act_list,
        int                             i_thread_count
    )
      : m_act_list( i_act_list ),
        m_thread_count( i_thread_count == 0 ? DefaultThreadPoolCount() : i_thread_count ),
        m_thread_created( 0 ),
        m_thread_pending( 0 ),
        m_thread_processing_start( 0 ),
        m_thread_waiting( 0 ),
        m_thread_finished( 0 ),
        m_shutting_down( false ),
        m_tlist_cond( m_act_list->m_mutex ),
        m_list_o_tasks( new Pointer< Task_ThreadPool * >[ m_thread_count ] )
    {
    }

    // ======== ~ThreadList ===========================================
    /**
     * The ThreadList destructor will terminate the loop
     *
     */

    ~ThreadList()
    {
        // should never call destructor twice
        AT_Assert( false == m_shutting_down );

        // tell all threads to exit
        m_shutting_down = true;

        if ( m_thread_waiting )
        {
            // ok no choice need to do the fancy wake-up.
            Lock<Mutex> l_lock( m_act_list->m_mutex );
            if ( m_thread_waiting )
            {
                // this should pull all the threads out of their
                // slumber.
                m_tlist_cond.PostAll();
            }
        }
        
        // This will wait until all threads have completed.
        delete[] m_list_o_tasks;

        AT_Assert( m_thread_created == m_thread_finished );

        // the number of threads created must equal the number
        // of threads finished.
        AT_Assert( m_thread_finished == m_thread_created );

        // All shut down - yipee
    }
    
    // ======== ProcessList ===========================================
    /**
     * ProcessList is called by all threads to process any activities.
     *
     */

    void ProcessList()
    {
        PtrView< ActivityList * >   l_al = m_act_list;

        Lock<Mutex> l_lock( l_al->m_mutex );
        
        if ( m_thread_processing_start )
        {
            -- m_thread_processing_start;
        }
        else
        {
            l_al->ProcessingStarted();
        }
        
        do {
            Activity    * l_act;

            // get the next activity out of the queue

            do {

                // only pull activities out if we're not closed and
                // not suspended

                if (
                    ( ! l_al->m_terminated )
                    && ( l_al->m_closed_count <= 0 )
                    && ( l_al->m_suspend_count <= 0 )
                )
                {
                    // try to pull somthing ...
                    l_act = l_al->PopNext();

                    if ( l_act )
                    {
                        // The only way an activity can be in the queue is if it
                        // is in the s_enqueued state
                        AT_Assert( l_act->m_state == & Activity::s_enqueued );
    
                        // break out of the loop  and perform the activity we just popped
                        // as well as loop around and do it all again.
                        break;
                    }
                }
                
                if ( m_shutting_down )
                {
                    // OK - no longer waiting - get outa here
                    ++ m_thread_finished;

                    l_al->ProcessingEnded();

                    if ( m_thread_finished == m_thread_created )
                    {
                        l_al->ProcessingTerminated();
                    }
                    
                    return;
                }

                // increment the count of threads waiting ...
                ++ m_thread_waiting;
                
                l_al->ProcessingEnded();

                m_tlist_cond.Wait(); // This unlocks the mutex while it waits

                if ( m_thread_processing_start )
                {
                    -- m_thread_processing_start;
                }
                else
                {
                    l_al->ProcessingStarted();
                }
                
            } while ( true );

            l_al->ProcessActivity( l_act );

        } while ( true );
    }

    // ======== ActivityEnqueuedNotification ==========================
    /**
     * ActivityEnqueuedNotification is called by
     * ActivityList::ActivityEnqueuedNotification to indicate that
     * an element is available in the queue. This would initiate a
     * new Task_ThreadPool if there were no more available or
     * 
     * NOTE THAT THE MUTEX SHOULD STILL BE LOCKED ...
     * @param i_count is the count of the number of elements to
     *          prepare for.
     */

    bool ActivityEnqueuedNotification( unsigned i_count )
    {
        // you can't enqueue anything when we're shutting down
        AT_Assert( ! m_shutting_down );

        unsigned l_queue_count = i_count;

        if ( m_thread_waiting && ( m_thread_waiting <= l_queue_count ) )
        {
            m_tlist_cond.PostAll();
            l_queue_count -= m_thread_waiting;
            m_act_list->ProcessingStarted( m_thread_waiting );
            m_thread_processing_start += m_thread_waiting;
            m_thread_waiting = 0;

            return true;
        }

        while ( m_thread_waiting && l_queue_count )
        {
            m_tlist_cond.Post();
            -- m_thread_waiting;
            -- l_queue_count;
            ++ m_thread_processing_start;
            m_act_list->ProcessingStarted();

        }

        while ( l_queue_count && ( m_thread_count > m_thread_created ) )
        {
            // decrement the queue count
            -- l_queue_count;
            
            // Get the new thread index
            ++ m_thread_created;
            DecrementHelper     l_dechelp( m_thread_created );

            ++ m_thread_processing_start;
            DecrementHelper     l_dechelp2( m_thread_processing_start );

            // We really should not hold the lock while
            // creating a thread as this might take a while.

            // Assuming the thread actually starts, we're notifying
            // of processing start.

            ActivityList::ProcessingStart_RAII  l_ps( m_act_list );
            
            Pointer< Task_ThreadPool * >  l_new_task;
            
            l_new_task = new Task_ThreadPool( this );
            
            m_list_o_tasks[ m_thread_pending ++ ] = l_new_task;

            l_ps.SetOk();
            l_dechelp2.SetOk();
            l_dechelp.SetOk();
        }

        return true;
    }

    /**
     * The activity list connected to this pointer
     */
    PtrView< ActivityList * >       m_act_list;
    
    /**
     * m_thread_count is the maximum number of threads to create
     */
    int                             m_thread_count;

    /**
     * m_thread_created is the number of actual threads created.
     */
    int                             m_thread_created;

    /**
     * m_thread_pending is the index of the actual thread
     */
    int                             m_thread_pending;

    /**
     * m_thread_processing_start is the number of
     * times m_thread_processing_start has been called
     * on behalf of processing threads.
     */
    int                             m_thread_processing_start;

    /**
     * m_thread_waiting is the number of threads that are waiting
     */
    unsigned                        m_thread_waiting;

    /**
     * m_thread_finished is the number of threads to have left the
     * main processing loop.
     */
    int                             m_thread_finished;

    /**
     * m_shutting_down indicates that the ThreadList destructor has been called
     * and that all threads should now exit.
     */
    bool                            m_shutting_down;

    /**
     * m_tlist_cond is used to synchronize the threads in this threadlist.
     */
    Conditional                     m_tlist_cond;

    /**
     * m_list_o_tasks implements the list of tasks.  These are basically
     * pointers to Task_ThreadPool that get created on demand.  The
     * length of the array is determined by m_thread_count.
     */
    Pointer< Task_ThreadPool * >  * m_list_o_tasks;
    
};


// ======== Task_ThreadPool::Work =====================================
void Task_ThreadPool::Work()
{
    // all the thread does is process the list.
    m_threadlist->ProcessList();
}


// ======== ActivityList_Pool::ActivityList_Pool ======================

ActivityList_Pool::ActivityList_Pool( int i_thread_count )
  : m_thread_list( new ThreadList( PtrView< ActivityList_Pool *>( this ), i_thread_count ) )
{
}

// ======== ActivityList_Pool::~ActivityList_Pool =====================

ActivityList_Pool::~ActivityList_Pool()
{
    delete m_thread_list;
}

// ======== ActivityEnqueuedNotification ==========================

bool ActivityList_Pool::ActivityEnqueuedNotification( unsigned i_count )
{
    return m_thread_list->ActivityEnqueuedNotification( i_count );
}


// ======== ProcessList =============================================
void ActivityList_Exclusive::ProcessList()
{
    Lock<Mutex> l_lock( m_mutex );
    
    // ProcessingStarted(); has been called 
    
    do {
        
        Activity    * l_act;
    
        // get the next activity out of the queue

        do {
    
            // only pull activities out if we're not closed and
            // not suspended
    
            if (
                ( ! m_terminated )
                && ( m_closed_count <= 0 )
                && ( m_suspend_count <= 0 )
            )
            {
                // try to pull somthing ...
                l_act = PopNext();
    
                if ( l_act )
                {
                    // break out of the loop  and perform the activity we just popped
                    // as well as loop around and do it all again.
                    break;
                }
            }
                        
            ProcessingEnded();

            return;
    
        } while ( true );

        ProcessActivity( l_act );
    
    } while ( true );

}

// ======== Perform ===============================================
void ActivityList_FedEx::Perform()
{
    ProcessList();
}

// ======== Cancelled =============================================
void ActivityList_FedEx::Cancelled(
    unsigned int                i_count
) {
    while ( i_count -- )
    {
        ProcessingEnded();
    }
    
    // close this activity list
    Close();
}

// ======== ActivityEnqueuedNotification ==========================
bool ActivityList_FedEx::ActivityEnqueuedNotification( unsigned i_count )
{
    // only ask for more help if there is nothing in progress.
    if ( m_perform_in_progress == 0 )
    {
        // try to enqueue
        EnqueueStatus l_status = Enqueue();
    
        if ( l_status == Activity::ActivityAdded )
        {
            ProcessingStarted();
            return true;
        }
        
        return false;
    }

    return true;
}


// ======== DisableAll ============================================
void ActivityController::DisableAll( bool i_wait, bool i_inhibit )
{
    if ( m_inhibit )
    {
        return;
    }

    m_inhibit = i_inhibit;

    for ( int i = m_list_o_activities.size() - m_destruct_count; i --; )
    {
        m_list_o_activities[ i ]->Disable( i_wait );
    }
}

    
// ======== EnableAll ============================================
void ActivityController::EnableAll()
{
    if ( m_inhibit )
    {
        return;
    }

    for ( int i = 0; i < int( m_list_o_activities.size() - m_destruct_count ); ++ i )
    {
        m_list_o_activities[ i ]->Enable();
    }
}



} // namespace at
