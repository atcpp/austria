//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_stack_trace.h
 *
 */

#ifndef x_at_stack_trace_h_x
#define x_at_stack_trace_h_x 1

#include "at_exports.h"
#include "at_lifetime.h"

// Austria namespace
namespace at
{

// ======== StackFrame ================================================
/**
 * StackFrame describes a single stack frame.
 *
 */

struct AUSTRIA_EXPORT StackFrame
{
    public:

    void                          * m_address;
    std::string                     m_symbol_name;

};


// ======== StackIterator =============================================
/**
 * Will iterate a StackTrace and povide all the elements in the
 * trace.
 *
 */

class AUSTRIA_EXPORT StackIterator
  : virtual public PtrTarget
{
    public:


    // ======== Next ==================================================
    /**
     * Next will move this iterator to point to the next frame.
     *
     *
     * @return True if the iterator points to a valid frame
     *      after the move.
     */

    virtual bool Next() = 0;


    // ======== Previous ==============================================
    /**
     * Previous will move this iterator to point to the previous frame.
     *
     *
     * @return True if the iterator points to a valid frame
     *      after the move.
     */

    virtual bool Previous() = 0;

    

    // ======== Last ==================================================
    /**
     * Last will move the iterator to one past the end of the stack
     * of frames.
     *
     * @return nothing
     */

    virtual void Last() = 0;


    // ======== First =================================================
    /**
     * First will move the iterator to the first frame of the stack.
     *
     *
     * @return nothing
     */

    virtual void First() = 0;


    // ======== IsValid ===============================================
    /**
     * IsValid returns true if the iterator is currenly pointing to
     * a valid stack frame.
     *
     * @return True if pointing to valid frame.
     */

    virtual bool IsValid() const = 0;


    // ======== DeReference ===========================================
    /**
     * DeReference will return the current stack frame.  An attempt to
     * dereference an invalid pointer is undefined and most likely
     * assert.
     *
     * @return A reference to the stack frame
     */

    virtual const StackFrame & DeReference() const = 0;
    
};

// ======== StackTrace =============================================
/**
 * StackTrace will produce a list that contains debugging information
 * that will show the back trace of function calls.
 *
 */

class AUSTRIA_EXPORT StackTrace
  : virtual public PtrTarget
{

    public:

    /**
     * m_line_prefix can be used to prefix each line of a
     * stack trace.
     */

    std::string                             m_line_prefix;


    // ======== Begin =================================================
    /**
     * Begin returns a new StackIterator for the bottom-most stack frame.
     *
     *
     * @return Pointer to a StackIterator
     */

    virtual PtrDelegate<StackIterator *> Begin() const = 0;


    // ======== operator< =============================================
    /**
     * This compares two stack traces, this is such that it may be used
     * in standard containers.
     *
     * @param i_rhs The rhs of "<"
     * @return true if i_rhs is less than this
     */

    virtual bool operator< ( const StackTrace & i_rhs ) const = 0;

    
};


// ======== NewStackTrace =============================================
/**
 * NewStackTrace is implementation dependant, it will create a new
 * stack trace.
 *
 * @return A pointer to a new stack trace.
 */

extern AUSTRIA_EXPORT PtrDelegate< StackTrace * > NewStackTrace();


} // namespace

namespace std {

// ======== basic_ostream<i_char_type, i_traits>& operator << =========
/**
 * Standard method for displaying a at::StackFrame
 *
 * &param i_ostream is the ostream
 * &param i_value The StackFrame to print
 * &return reference to ostream
 */

template<
    typename        i_char_type,
    class           i_traits
>                   
basic_ostream<i_char_type, i_traits>& operator << (
    basic_ostream<i_char_type, i_traits>        & i_ostream,
    const at::StackFrame                        & i_value
) {

    i_ostream << i_value.m_address << " " << i_value.m_symbol_name;

    return i_ostream;
}

// ======== basic_ostream<i_char_type, i_traits>& operator << =========
/**
 * Standard method for displaying a at::StackTrace
 *
 * &param i_ostream is the ostream
 * &param i_value The StackFrame to print
 * &return reference to ostream
 */

template<
    typename        i_char_type,
    class           i_traits
>
basic_ostream<i_char_type, i_traits>& operator << (
    basic_ostream<i_char_type, i_traits>        & i_ostream,
    const at::StackTrace                        & i_value
) {

    at::Ptr< at::StackIterator * > l_sitr = i_value.Begin();

    if ( l_sitr->IsValid() )
    {
        do {
            i_ostream << i_value.m_line_prefix << l_sitr->DeReference() << "\n";
        } while ( l_sitr->Next() );
    }

    return i_ostream;
}

} // namespace

#endif // x_at_stack_trace_h_x


