//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_twin.h
 *
 */

#ifndef x_at_twin_h_x
#define x_at_twin_h_x 1

// Austria namespace
namespace at
{
    
/**
 * @defgroup TheTwinInterface The Twin interface

<h2>Introduction</h2>

<p>In complex systems, objects may need to co-operate as peers since
neither object is owned by the other.  In this scenario, either
peer may notify the other that the association is broken.
</p>
<p>
The concept of a Twin interface is where a pair of objects need
to comply with an interface where either lead or aide may initiate
a cancellation of the association at any point.
</p>
<p> 
LeadTwin and AideTwin provide a weak association methodology.
These allow the association of two co-operating objects (a twin).
The association is initiated by the LeadTwin interface on some
application defined service.  The application will ask the LeadTwin
to associate with an aide twin interface.  The association may be
terminated by calling either a LeadCancel() or AideCancel() or
association with a new aide.
</p>
<p>
This concept is not dissimilar to a weak pointer and may be used
in the same kind of scenario.  The Twin interface also provides a
more robust tear down functionality.
</p>
 *
 */

/**
 * @defgroup TwinHOWTO Twin-HOWTO: a step-by-step guide to the Twin interface
<h2>Twin-HOWTO</h2>
<p>The Twin-HOWTO aims to guide the programmer through using the Twin
system step by step.  Note that this HOWTO does not apply verbatim to
multi-threaded programs; a similar but distinct system exists for
multi-threaded programs and is defined in at_twinmt.h.
</p>
<ol>
<li>
  <h3>Decide whether you need to use the Twin system.</h3>

  <p>The twin system is meant to make it easy to create two
  hierarchies of classes that interact with each other.  What's so
  hard about having two hierarchies of classes that interact with each
  other? you ask.  And how can any system composed of so many classes
  make anything easy? you demand to know.</p>

  <p>When two classes wish to interact, managing their lifetimes
  can be difficult if the classes are reference counted: if they
  maintain references to each other, there will be a cycle in the
  reference graph, and pairings of objects from these two classes
  will never be destroyed.  If the classes do not maintain references
  to each other (that is, maintain pointers but don't mess with the
  reference count) then each object cannot know whether the other may
  have been destroyed.</p>

  <p>The twin system provides notification to each half of a pairing
  of when the other half has come or gone.  If this sounds like it
  could make your life easier, then you're a candidate for using the
  Twin system.</p>
</li>


<li>
  <h3>Declare interface classes.</h3>

  <p>The Twin system enforces use of the interface/implementation
  style of programming: one must first define a pure abstract
  interface class, and then provide a concrete implementation of
  it.</p>

  <p>Two classes (or class hierarchies) will participate in the
  relationship.  When using the Twin system, the halves of the pairing
  formed are not symmetric.  One of the classes plays the roll
  of <i>lead class</i> and the other plays the roll of <i>aide
  class</i>.  The difference between these is that a lead has the last
  opportunity to take action when the relationship between a lead and
  an aide is broken.  You should, therefore, decide which class is
  your lead and which is your aide.</p>

  <p><b>REV:</b> What other differences are there?</p>

  <p>For the purposes of this illustration, we will assume that the
  lead class interface is called <code>FooLeadIf</code> and the aide
  class is called <code>FooAideIf</code>.</p>

  <p>Define these interfaces as you would ordinarily; no change need
  be made here.</p>

  <p>But wait! you say, Implementations of these interfaces will
  participate in lead-aide pairings.  How will that become part of my
  interface?  The answer is that rather than
  passing <code>FooLeadIf</code> references, you will pass
  <code>LeadTwin&lt;AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt;</code> references, and instead of
  passing <code>FooAideIf</code> references, you will
  pass <code>AideTwin&lt;FooAideIf&gt;</code> references.  (These
  templates are defined in <code>at_twin.h</code>.)  You see,
  <code>LeadTwin&lt;AideTwin&lt;X&gt;,Y&gt;</code> inherits from <code>Y</code>
  and <code>AideTwin&lt;X&gt;</code> inherits from <code>X</code>.
  So, by passing these classes, you get the whole story.</p>

  <p><b>REV:</b> It is arguably a design flaw that the interfaces you
  define are necessarily incomplete and references to a subclass
  (which in this case happens to be one of the lead or aide templates)
  must be passed to see the whole interface.  In documenting your
  interfaces, consider making a habit of writing a comment explaining
  not to take/pass references to your interface itself but
  to <code>LeadTwin&lt;AideTwin&lt;YourAideIf&gt;,YourLeadIf&gt;</code>
  or <code>AideTwin&lt;YourAideIf&gt;</code>.</p>

  <p>Now that we know to pass
  around <code>LeadTwin&lt;AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt;</code> and
  <code>AideTwin&lt;FooAideIf&gt;</code>, we may begin to wonder,
  what are the methods in these template interfaces that correspond
  to the twin pairing functionality?</p>

  <p>In <code>LeadTwin</code>, there are the methods
  <code>LeadCancel</code>, <code>LeadAssociate</code>,
  and <code>LeadCompleted</code>.  <code>LeadCancel</code> is public
  and causes the lead/aide relationship to
  dissolve.  <code>LeadAssociate</code> and <code>LeadCompleted</code>
  are protected, but they can be called by the aide class, because it
  is a friend.  <code>LeadAssociate</code> takes an aide as an argument
  and establishes a lead/aide relationship between the callee and the
  argument.  <code>LeadCompleted</code> is called by the aide to let
  the lead know that it thinks the pairing is now over.  This is the
  aforementioned last chance to take action as a result of the
  breakup.</p>

  <p>In <code>AideTwin</code>, there are the
  methods <code>AideCancel</code>, <code>WrapLeadAssociate</code>,
  and <code>WrapLeadCancel</code>.  <code>AideCancel</code> is public
  and causes the lead/aide relationship to
  dissolve.  <code>WrapLeadAssociate</code>
  and <code>WrapLeadCompleted</code> are protected and are only
  present to get around the fact that friendship is not inherited by
  subclasses: as an aide, you'll call <code>WrapLeadAssociate</code>
  to call <code>LeadAssociate</code> on a lead
  and <code>WrapLeadCompleted</code> to
  call <code>LeadCompleted</code> on a lead.  Again, they're only
  there so that you, as an aide, can call the corresponding
  protected methods on <code>LeadTwin</code>.  They don't perform any
  useful work except to relay the call.</p>

  <p>Above, I spoke of the methods in these interfaces as though they
  were implemented.  In fact, what I described is what these
  methods <i>should</i> do.  With the exception
  of <code>WrapLeadAssociate</code>
  and <code>WrapLeadCompleted</code>, they're all pure virtual
  methods.  We'll talk about where they get implemented in the next
  step.</p>

</li>

<li>
  <h3>Define implementation(s).</h3>

  <p>Now that you have an interface, you would probably like to
  supply an implementation.  But remember, you're not implementing
  <code>FooLeadIf</code> and <code>FooAideIf</code>, you're
  implementing <code>LeadTwin&lt;AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt;</code> and 
  <code>AideTwin&lt;FooAideIf&gt;</code>.</p>

  <p>But if I must implement the <code>LeadTwin</code>
  and <code>AideTwin</code> methods, you say, then what have I gained
  by using these templates except a restriction on the interface for
  my pairing-related functions?  A good question!
  Fortunately, <code>LeadTwin</code> and <code>AideTwin</code> are not
  the whole story.  Let us assume for the sake of discussion that your
  implementation classes are to be named <code>FoodLeadImpl</code>
  and <code>FooAideImpl</code>.  When defining these classes, rather
  than inheriting from <code>LeadTwin&lt;AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt;</code> and
  <code>AideTwin&lt;FooAideIf&gt;</code>, you can inherit
  from <code>LeadTwin_Basic&lt;AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt;</code> and
  <code>AideTwin_Basic&lt;FooAideIf,FooLeadIf&gt;</code>.  These two templates
  implement the pure virtual methods declared
  in <code>LeadTwin&lt;AideTwin&lt;A&gt;,L&gt;</code>
  and <code>AideTwin&lt;A&gt;</code>.</p>

  <p>As a subclass of <code>LeadTwin_Basic&lt;AideTwin&lt;A&gt;,L&gt;</code>, you can
  get your aide by calling the <code>GetAide()</code> method with a
  reference to the pointer variable where you would like the aide
  pointer stored.</p>

  <p>REV: What about <code>LeadAssociate</code>
  and <code>LeadCompleted</code>?  I know there are these traits
  classes, so I suspect that just implementing those methods is not
  the correct way to receive notification of these events, but I'm
  still not quite sure what the correct thing to do is.</p>

  <p>As a subclass of <code>AideTwin_Basic&lt;A,L&gt;</code>, you can
  get a pointer to your lead by passing a reference to a pointer
  variable where you would like the lead pointer stored to the
  method <code>GetLead</code>.</p>

  <p>Note that in our example the <code>GetLead()</code>
  and <code>GetAide()</code> methods provide you with pointers
  to <code>FooLeadIf</code> and <code>FooAideIf</code>,
  respectively, not <code>LeadTwin&lt;AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt;</code>
  and <code>AideTwin&lt;FooAideIf&gt;</code>.</p>

  <p>REV: I guess something should be said about completion codes,
  but I can't really be bothered to just now.</p>

</ol>

<p>Here is a minimal definition of the four classes we hypothesized
  that we were implementing during this HOWTO.</p>

<pre><code>
#include "at_twin_basic.h"
#include <iostream>

class FooLeadIf
{
public:
    virtual void Blah() = 0;
};

class FooAideIf
{
public:
    virtual void Mumble() = 0;
};

// These typedefs suitably define types that you can use to pass
// around pointers or references to concrete instances of these
// interfaces.
typedef at::LeadTwin&lt;at::AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt; FooLead;
typedef at::AideTwin&lt;FooAideIf&gt; FooAide;

class FooLeadImpl : public at::LeadTwin_Basic&lt;at::AideTwin&lt;FooAideIf&gt;,FooLeadIf&gt;
{
public:
    virtual void Blah()
    {
        FooAideIf *p;
        this->GetAide(p);
        std::cout << "My aide is " << (void*)p << std::endl;
    }
};

class FooAideImpl : public at::AideTwin_Basic&lt;FooAideIf,FooLeadIf&gt;
{
public:
    virtual void Mumble()
    {
        FooLeadIf *p;
        this->GetLead(p);
        std::cout << "My lead is " << (void*)p << std::endl;
    }
};
</code></pre>
 *
 */

//
// forward declaration
//

template<
    typename    w_aide_twin_interface,
    typename    w_lead_interface
>
class LeadTwin;

template<
    typename    w_aide_interface
>
class AideTwin;



// ======== TwinTraits ============================================
/**
 * TwinTraits contains various Twin interface details.
 * 
 */

class TwinTraits
{

    public:

    /**
     * TwinCode is used to inform the LeadTwin of the reason for
     * cancellation.
     */
    
    enum TwinCode
    {
        /**
         * Uninitialized indicates a not yet set value
         */
        
        Uninitialized,
        
        /**
         * CancelRequested indicates that the aide was cancelled
         *
         */

        CancelRequested,
        
        /**
         * AideDelete indicates that the aide was removed.
         * 
         */

        AideDelete,
        
        /**
         * AideGrabbed indicates that the aide was associated
         * with a new Lead.
         */

        AideGrabbed,
        
        /**
         * AideSetupError indicates that an error was encountered.
         *
         */

        AideSetupError
        
    };
    
};


// ======== LeadTwin ===============================================
/**
 * The LeadTwin is the initiator of the association.
 *
 * The w_aide_twin_interface template parameter would usually be
 * an AideTwin< w_aide_interface > but the only requirement is that
 * it provides an interface containing the AideCancel method
 * and for correctness does not allow its destructor to be called.
 */

template<
    typename    w_aide_twin_interface,
    typename    w_lead_interface
>
class LeadTwin
  : public w_lead_interface
{
    
    friend class AideTwin< typename w_aide_twin_interface::t_aide_interface >;
    
    public:

    /**
     * LeadCancel is called by anyone to initiate a cancel operation.
     * This will terminate any service currently in progress.
     */
    
    virtual void LeadCancel() = 0;

    protected :

    /**
     * LeadAssociate is only called by the Aide to associate the
     * Lead with the appropriate Aide.  Hence this method is
     * protected.
     */

    virtual bool LeadAssociate( w_aide_twin_interface * i_aide ) = 0;

    /**
     * LeadCompleted is only called by the Aide to notify that the
     * association is now broken. Hence this method is
     * protected.
     */

    virtual void LeadCompleted( TwinTraits::TwinCode i_completion_code ) = 0;

    /**
     * Destructors must not be called on the LeadTwin interface.
     */

    virtual ~LeadTwin() {}
};



// ======== AideTwin ===============================================
/**
 * The AideTwin interface is basically a cancel method.  Other parts
 * of the interface may be added through the w_aide_interface
 * interface but the only requirement is that it contains an AideCancel
 * method.
 *
 */

template<
    typename    w_aide_interface
>
class AideTwin
  : public w_aide_interface
{
    public:

    virtual void AideCancel() = 0;

    /**
     * 
     */

    typedef w_aide_interface            t_aide_interface;

    protected :

    /**
     * WrapLeadAssociate allows subclasses to access the LeadAssociate function.
     */
    template <typename w_lead >
    inline bool WrapLeadAssociate( w_lead * i_lead )
    {
        return i_lead->LeadAssociate( this );
    }

    /**
     * WrapLeadCompleted allows subclasses to access the LeadCompleted function.
     */

    template <typename w_lead >
    inline void WrapLeadCompleted( w_lead * i_lead, TwinTraits::TwinCode i_completion_code )
    {
        i_lead->LeadCompleted( i_completion_code );
    }

    virtual ~AideTwin() {}
};


}; // namespace

#endif // x_at_twin_h_x
