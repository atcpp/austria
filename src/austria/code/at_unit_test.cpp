//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_unit_test.cpp
 *
 */

#include <cstring>
#include <iostream>
#include <map>
#include <list>
#include <cstdlib>
#include <sstream>

#include "at_os.h"
#include "at_unit_test.h"
#include "at_lifetime.h"
#include "at_exception.h"

using namespace ::at;
using namespace ::std;

// Austria namespace
namespace at
{

using ::std::operator <<;

// TestCase_Exception::s_abort defintiion ...

bool TestCase_Exception::s_abort = false;


// ======== UnitTestAssertModifier ====================================
/**
 * UnitTestAssertModifier modifies the bahaviour of AT_Assert.
 *
 */

class UnitTestAssertModifier
{
    public:

    // ======== SetUnitTestAssert =====================================
    /**
     *  This will set the OSTraitsBase::m_running_test_case variable
     *  to modify the AT_Assert behaviour to throw a test case
     *  exception or perform as usual.
     *
     * @param i_throw_tc_except
     */

    static void SetUnitTestAssert( bool i_throw_tc_except )
    {
        OSTraitsBase::m_running_test_case = i_throw_tc_except;
    }

};

// ======== UnitTestKey_Basic::operator< ===========================
/**
 *  A key comparison operator for registry map
 *
 * &param i_value the value being compared against
 * &return true if *this < i_value
 */

bool UnitTestKey_Basic::operator<( UnitTestKey_Basic const & i_value ) const
{

    if ( m_area != i_value.m_area )
    {
        return m_area < i_value.m_area;
    }

    return ::strcmp( m_name, i_value.m_name ) < 0;

} // end UnitTestKey_Basic::operator<



// ======== UnitTestExecutor_Basic =================================
/**
 * UnitTestExecutor_Basic is a basic unit test executor - it performs
 * all the selected tests and reports the result.
 *
 */

class AUSTRIA_EXPORT UnitTestExecutor_Basic
{
    public:

    typedef ::std::map<AT_String, AT_String>              t_StringMap;

    typedef FactoryRegister<
        UnitTest,
        UnitTestKey_Basic,
        Creator0P< UnitTest, UnitTestKey_Basic >
    >                                                 t_UnitTestRegistryType;
    
    enum Operation
    {
        RunTests,
        ListTests
    };


    // ======== t_ExecutorTester ======================================
    /**
     * t_ExecutorTester stores information about the given test
     *
     */

    class t_ExecutorTester
    {
        public:


        t_ExecutorTester()
          : m_key( 0 ),
            m_entry( 0 ),
            m_result( UnitTestTraits::NotTested ),
            m_expected_result( UnitTestTraits::NotTested ),
            m_exception_valid( false ),
            m_delete_exception_valid( false )
        {
        }


        // ======== SetTester =========================================
        /**
         *  SetTester will set the test parameters
         *
         * &param i_X
         * &return nothing
         */

        void    SetTester(
            const UnitTestKey_Basic               * i_key,
            t_UnitTestRegistryType::t_MapEntry        * i_entry
        )
        {

            m_key = i_key;
            m_entry = i_entry;

            return;

        } // end SetTester



        // ======== DescribeSelf ======================================
        /**
         *  DescribeSelf prints out information about ourself
         *
         * &param i_ostr - the output stream
         * &return nothing
         */

        void    DescribeSelf( ::std::ostream & i_ostr )
        {
            i_ostr << * m_key << " - " << m_key->m_description;

        } // end DescribeSelf


        // ======== DescribeExceptions ================================
        /**
         *  DescribeExceptions will print a a description of the
         *  exceptions taken.
         *
         * &param i_ostr - the output stream
         * &return nothing
         */

        void    DescribeExceptions( ::std::ostream & i_ostr )
        {

            if ( m_exception_valid )
            {
                i_ostr << "                " << m_exception << "\n";
            }
                
            if ( m_delete_exception_valid )
            {
                i_ostr << "                Delete " << m_delete_exception << "\n";
            }

            return;

        } // end DescribeExceptions

        

        // ======== Announce ==========================================
        /**
         *  Announce writes a description of what is about to happen
         *
         * &param i_ostr - the output stream
         * &param i_description - what is about to happen
         * &return nothing
         */

        void    Announce(
            ::std::ostream                            & i_ostr,
            const char                                * i_description
        ) {

            i_ostr << "      - " << i_description;
            i_ostr.flush();

            return;

        } // end Announce


        // ======== RunTester =========================================
        /**
         * RunTester performs the actual test.
         *
         * &param i_ostr is the stream to send progress output
         * &return nothing
         */

        void RunTester( ::std::ostream & i_ostr )
        {

            UnitTest                * l_test = 0;

            // Get the expected result value
            //
            m_expected_result = m_key->m_expected_result;

            i_ostr << "Unit Test: ";
            DescribeSelf( i_ostr );

            i_ostr << "\n";

            Announce( i_ostr, "Construct" );

            /**
             * Attempt to construct the unit test;
             */
            m_result = UnitTestTraits::TestConstructorFailure;

            if ( m_nocatch )
            {
                // Invoke the factory creator
                l_test = m_entry->m_factory->Create();
            }
            else try
            {
                // Invoke the factory creator
                l_test = m_entry->m_factory->Create();
            }
            catch ( const TestCase_Exception & l_exception )
            {
                m_exception = l_exception;
                m_exception_valid = true;

                i_ostr << " - Failed : " << m_exception << "\n";
                
                return;
            }
            catch ( const Exception & l_at_exception )
            {
                i_ostr << " - at::Exception : " << l_at_exception.what();

                const Full_Exception * l_full = l_at_exception.Full();

                if ( l_full )
                {
                    i_ostr << ":" << l_full->Location() << "\n";

                    at::Ptr< const StackTrace * > l_trace = l_full->Trace();

                    i_ostr << "Stack trace:\n";
                    i_ostr << * l_trace;
                }
                
                return;
            }
            catch ( ... )
            {
                i_ostr << " - Failed : unknown exception\n";
                
                return;
            }

            i_ostr << " - Done\n";

            //
            // Make sure we have a pointer
            if ( l_test == 0 )
            {
                return;
            }

            /**
             * Attempt to run the test
             */

            m_result = UnitTestTraits::TestRunFailure;
            
            bool                      l_has_failed = false;

            Announce( i_ostr, "Run" );

            if ( m_nocatch )
            {
                // Invoke the test
                l_test->Run();
            }
            else try
            {
                // Invoke the test
                l_test->Run();
            }
            catch ( const TestCase_Exception & l_exception )
            {
                m_exception = l_exception;
                m_exception_valid = true;
                l_has_failed = true;
                
                i_ostr << " - Failed : " << m_exception << "\n";
                
            }
            catch ( const Exception & l_at_exception )
            {
                l_has_failed = true;

                i_ostr << " - at::Exception : " << l_at_exception.what();

                const Full_Exception * l_full = l_at_exception.Full();

                if ( l_full )
                {
                    i_ostr << ":" << l_full->Location() << "\n";

                    at::Ptr< const StackTrace * > l_trace = l_full->Trace();

                    i_ostr << "Stack trace:\n";
                    i_ostr << * l_trace;
                }
                
                return;
            }
            catch ( ... )
            {
                l_has_failed = true;
                
                i_ostr << " - Failed : unknown exception";
                
            }

            if ( ! l_has_failed )
            {
                i_ostr << " - Done\n";
            }

            //
            // Get the new expected result from the test case itself

            if ( l_test->m_expected_result != UnitTestTraits::DefaultResult )
            {
                m_expected_result = l_test->m_expected_result;
            }
            
            /**
             * Attempt to delete the test
             * 
             */

            // if a failure has already occurred - then leave the current failure
            // code in place.
            if ( ! l_has_failed )
            {
                m_result = UnitTestTraits::TestDestructorFailure;
            }

            bool                      l_delete_failed = false;
            
            Announce( i_ostr, "Delete" );

            if ( m_nocatch )
            {
                // Invoke the virtual destructor
                delete l_test;
            }
            else try
            {
                // Invoke the virtual destructor
                delete l_test;
            }
            catch ( const TestCase_Exception & l_exception )
            {
                m_delete_exception = l_exception;
                m_delete_exception_valid = true;
                l_has_failed = true;
                l_delete_failed = true;
                
                i_ostr << " - Failed : " << m_delete_exception << "\n";
                
            }
            catch ( const Exception & l_at_exception )
            {
                l_has_failed = true;
                l_delete_failed = true;
                
                i_ostr << " - at::Exception : " << l_at_exception.what();

                const Full_Exception * l_full = l_at_exception.Full();

                if ( l_full )
                {
                    i_ostr << ":" << l_full->Location() << "\n";

                    at::Ptr< const StackTrace * > l_trace = l_full->Trace();

                    i_ostr << "Stack trace:\n";
                    i_ostr << * l_trace;
                }
                
                return;
            }
            catch ( ... )
            {
                l_has_failed = true;
                l_delete_failed = true;
                
                i_ostr << " - Failed : unknown exception\n";
                
            }

            if ( ! l_delete_failed )
            {
                i_ostr << " - Done\n";
            }

            if ( ! l_has_failed )
            {
                m_result = UnitTestTraits::TestSuccess;
            }


            if ( TestOk() )
            {
                i_ostr << "        Finished - Result is OK";

                if ( m_result != UnitTestTraits::TestSuccess )
                {
                    i_ostr << " - Expected result is " << m_expected_result;
                }

                i_ostr << "\n";
            }
            else
            {
                i_ostr << "        Finished - Test has Failed";
                i_ostr << " - Expected result is '" << m_expected_result << "' got result '" << m_result << "'\n";
            }

            return;

        } // end RunTester

        
        // ======== TestOk ============================================
        /**
         *  Determines if the expected result returned.
         *
         *
         * &return true if as expected - false otherwise.
         */

        bool TestOk()
        {
            return m_result == m_expected_result;

        } // end TestOk

        //
        // Test parameters
        //
        const UnitTestKey_Basic                       * m_key;
        t_UnitTestRegistryType::t_MapEntry            * m_entry;

        //
        UnitTestTraits::t_TestResult                    m_result;
        UnitTestTraits::t_TestResult                    m_expected_result;

        // Exception information
        bool                                            m_exception_valid;
        TestCase_Exception                              m_exception;
        
        // Deletion exception information
        bool                                            m_delete_exception_valid;
        TestCase_Exception                              m_delete_exception;

        // m_nocatch is true if the unit test exceptions should not be caught.
        bool                                            m_nocatch;
    };

    /**
     * t_ListResults maintains a list of test results
     */

    typedef ::std::list<t_ExecutorTester>               t_ListResults;


    UnitTestExecutor_Basic()
      : m_operation( RunTests ),
        m_error_count( 0 ),
        m_level( UnitTestKey_Basic::m_level_default ),
        m_nocatch( false )
    {
    }


    // ======== RunTest ===============================================
    /**
     *  RunTest will run
     *
     * &return an error code - 0 on success, 1 on failure
     */

    int RunTest( ::std::ostream & i_ostr );


    // ======== CheckAreaSelection ====================================
    /**
     *  Check to see if the area being tested
     *
     * &param i_area is a pointer
     * &return true if it is selected
     */

    bool CheckAreaSelection(
        const UnitTestArea_Basic                        * i_area
    )
    {

        // if the map of areas is empty - then by default
        // it is selected

        if ( m_areas.size() == 0 )
        {
            return true;
        }

        // attempt to find the entry
        t_StringMap::iterator l_entry = m_areas.find( AT_String( i_area->m_name ) );

        // if the itereator is not the end then it is found
        return l_entry != m_areas.end();

    } // end CheckAreaSelection

    

    // ======== CheckTestSelection ====================================
    /**
     *  Check to see if the test is selected for testing
     *
     * &param i_key is a pointer to the test key
     * &return true if selected
     */

    int CheckTestSelection(
        const UnitTestKey_Basic         * i_key
    )
    {

        // if the map of areas is empty - then by default
        // it is selected

        if ( m_tests.size() == 0 )
        {
            return 1;
        }

        // attempt to find the entry
        t_StringMap::iterator l_entry = m_tests.find( AT_String( i_key->m_name ) );

        // if the itereator is not the end then it is found
        return ( l_entry != m_tests.end() ) ? 2 : 0;


    } // end CheckTestSelection


    // ======== PerformTest ===========================================
    /**
     *  PerformTest will perform a test an create a summary record.
     *  If in ListTests mode then a summary record is created only.
     *
     * &param i_X
     * &return nothing
     */

    void PerformTest(
        const UnitTestKey_Basic                       * i_key,
        t_UnitTestRegistryType::t_MapEntry            * i_entry,
        ::std::ostream                                & i_ostr,
        int                                             i_selected
    )
    {

        // create a new element at the back of the list
        m_results.push_back( t_ExecutorTester() );
        
        t_ListResults::iterator l_tr = m_results.end();

        -- l_tr;
        
        l_tr->SetTester( i_key, i_entry );
        l_tr->m_nocatch = m_nocatch;

        if ( m_operation != ListTests )
        {
            if ( ( i_selected == 2 ) || ( i_key->m_level <= m_level ) )
            {
                l_tr->RunTester( i_ostr );
    
                if ( ! l_tr->TestOk() )
                {
                    m_error_count ++;
                }
            } else {

                // not running test

                i_ostr << "    NOT SELECTED (level=" << i_key->m_level << ") ";
                i_ostr << * i_key << " - " << i_key->m_description << "\n";
            }
        }

        return;

    } // end PerformTest

    

    // ======== SummarizeResults ======================================
    /**
     * SummarizeResults will print in tabular form a list of test
     * results.
     *
     * &param i_X
     * &return nothing
     */

    void    SummarizeResults(
        ::std::ostream                                & i_ostr
    )
    {

        const UnitTestArea_Basic                      * l_area = 0;
        
        t_ListResults::iterator l_end = m_results.end();
        t_ListResults::iterator i = m_results.begin();

        i_ostr << "Test summary - " << m_results.size() << " test/s\n";
        
        for ( ; i != l_end; i ++ )
        {

            // Don't print test results for tests not tested.
            if ( i->m_result == UnitTestTraits::NotTested && m_operation != ListTests )
            {
                continue;
            }

            // see if the area has changed 
            if ( l_area != i->m_key->m_area )
            {
                //
                // new area - need to see if testing is selected for
                // this area
                //
                l_area = i->m_key->m_area;

                i_ostr << "Unit Test Area Summary: " << * l_area << "\n";
            }
            
            if ( m_operation != ListTests )
            {
                if ( i->TestOk() )
                {
                    i_ostr << "    OK ";
                }
                else
                {
                    i_ostr << "    FAILED ";
                }
                
                i->DescribeSelf( i_ostr );
                i_ostr << "\n";
                
                if ( i->m_result != UnitTestTraits::TestSuccess )
                {
                    i_ostr << "                Result is '" << i->m_result << "'\n";
                }
                
                i->DescribeExceptions( i_ostr );
                
            }
            else
            {
                i_ostr << "    ";
                i->DescribeSelf( i_ostr );
                i_ostr << "\n";
            }

        }

        if ( m_results.size() != 0 )
        {
            if ( m_error_count )
            {
                i_ostr << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
                i_ostr << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
                i_ostr << "%%%%%%%%%%%%%%%%%%%%% There is/are " << m_error_count << " test failure/s %%%%%%%%%%%%%%%%%%%%%%%%\n";

                // iterate and print just the errors - it's hard to see this intermingled with all the
                // successes
                for ( i = m_results.begin(); i != l_end; i ++ )
                {
                    if ( ! i->TestOk() )
                    {
                        i_ostr << "        vvvvvvv Failed vvvvvv\n   Area: " << * i->m_key->m_area << "\n";
                        i_ostr << "   Test: ";
                        i->DescribeSelf( i_ostr );
                        i_ostr << "\n        ^^^^^^^ FAILED ^^^^^^\n";
                    }
                }
                i_ostr << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
                i_ostr << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
                
            }
            else if ( m_operation != ListTests )
            {
                i_ostr << "All tests succeeded\n";
            }
        }
        else
        {
            i_ostr << "No tests selected\n";
        }

        i_ostr << "=================\n";
        i_ostr << "End unit test run\n";
        
        return;

    } // end SummarizeResults

            
    /**
     * m_operation indicates the operation to perform.
     */
    
    Operation                                         m_operation;

    /**
     * m_areas contains a map of areas to perform
     */
    t_StringMap                                       m_areas;

    /**
     * m_tests contains a map of tests to perform
     */
    t_StringMap                                       m_tests;

    /**
     * m_error_count contains a count of the number of errors.
     */
    
    int                                               m_error_count;

    /**
     * m_level is the test level
     */
    int                                               m_level;

    /**
     * m_results stores the test results
     */
    
    t_ListResults                                     m_results;

    /**
     * m_nocatch indicates that the unit test should not
     * be in a try block.  For debugging ...
     */
    bool                                              m_nocatch;
};


int UnitTestExecutor_Basic::RunTest( ::std::ostream & i_ostr )
{

    //
    // go fetch the register of tests

    t_UnitTestRegistryType                          & l_registry =
        t_UnitTestRegistryType::Get()
    ;

    //
    // the map is sorted by area - as the map is traversed
    // a change in the map triggers a grouping print
    const UnitTestArea_Basic                        * l_area = 0;
    
    const UnitTestArea_Basic                        * l_last_area = 0;

    bool                                              l_show_area = false;

    bool                                              l_shown_title = false;
    
    //
    // traverse the map - get the end 
    t_UnitTestRegistryType::t_MapIterator             l_end = l_registry.m_map.end();

    //
    // now traverse
    for (
        t_UnitTestRegistryType::t_MapIterator i = l_registry.m_map.begin();
        i != l_end;
        i ++
    )
    {

        // get the map entry
        t_UnitTestRegistryType::t_MapEntry        * l_entry = & (*i).second;

        // get the key
        const UnitTestKey_Basic               * l_key = (*i).first;

        // see if the area has changed 
        if ( l_area != l_key->m_area )
        {
            //
            // new area - need to see if testing is selected for
            // this area
            //
            l_area = l_key->m_area;

            l_show_area = CheckAreaSelection( l_area );

        }

        //
        // if the area is selected - then see next
        if ( l_show_area )
        {
            //
            // Ok check the test selection - if it's selected   
            if ( int l_selected = CheckTestSelection( l_key ) )
            {

                if ( m_operation != ListTests )
                {
                    //
                    // check to see an area change
                    if ( l_last_area != l_area )
                    {
                        //
                        // show the title
                        if ( ! l_shown_title )
                        {
                            i_ostr << "Unit test run\n"
                                      "=============\n";
                            l_shown_title = true;
                        }
    
                        l_last_area = l_area;
    
                        // print the area header for tests
    
                        i_ostr << "Unit Test Area Start: " << * l_area << "\n";
                        
                    }
                }
            
                // Perform the test on the unit test entry
                PerformTest( l_key, l_entry, i_ostr, l_selected );
            }
        }
    }

    // Now that the tests are complete - summarize the results

    SummarizeResults( i_ostr );

    return m_error_count != 0;

} // end RunTest

    

// ======== UnitTestExec ===========================================


int UnitTestExec(
    int             i_argc,
    const char   ** i_argv
)
{
    ::std::ostream                  & l_ostr = ::std::cerr;

    UnitTestExecutor_Basic            l_utce;

    UnitTestExecutor_Basic::t_StringMap * l_pmap = & l_utce.m_tests;

    bool                              l_has_error = false;
    bool                              l_throw_on_assert = true;

    int                               l_level = UnitTestKey_Basic::m_level_default;

    //
    // tell the LifeLine instrumented pointers to not
    // assert but throw an exception instead.
    //
    
    for ( int i = 1; i < i_argc; i ++ )
    {

        AT_String       l_value( i_argv[ i ] );
        
        if ( l_value == "--area" )
        {
            l_pmap = & l_utce.m_areas;
        }
        else if ( l_value == "--level" )
        {
            if ( ++ i >= i_argc )
            {
                ::std::cerr << "Not enough arguments after --level - need an int\n";
                ::std::exit( 1 );
            }

            ::std::istringstream     l_stream( i_argv[ i ] );
            l_stream >> l_utce.m_level;

            if ( ! l_stream )
            {
                ::std::cerr << "Error converting '" << i_argv[ i ] << "' to level (integer)\n";
                ::std::exit( 1 );
            }
        }
        else if ( l_value == "--test" )
        {
            l_pmap = & l_utce.m_tests;
        }
        else if ( l_value == "--nocatch" )
        {
            l_utce.m_nocatch = true;
        }
        else if ( l_value == "--leak_assert" )
        {
        }
        else if ( l_value == "--abort" )
        {
            // must set both ...
            TestCase_Exception::s_abort = true;
            l_throw_on_assert = false;
        }
        else if ( l_value == "--assert_aborts" )
        {
            l_throw_on_assert = false;
        }
        else if ( l_value == "--exception_aborts" )
        {
            Exception::s_abort_on_assert = true;
        }
        else if ( l_value == "--list" )
        {
            l_utce.m_operation = UnitTestExecutor_Basic::ListTests;
        }
        else if ( i_argv[ i ][ 0 ] == '-' )
        {
            l_has_error = true;
            l_ostr << "Unknown option " << l_value << "\n";
        }
        else
        {

            ::std::pair< UnitTestExecutor_Basic::t_StringMap::iterator, bool > l_result =
                l_pmap->insert(
                    UnitTestExecutor_Basic::t_StringMap::value_type(
                        l_value,
                        l_value
                    )
                )
            ;

            if ( ! l_result.second )
            {
                l_ostr << "value : " << l_value << " - already inserted\n";
            }
        }
    }

    if ( l_has_error )
    {
        return 1;
    }

    UnitTestAssertModifier::SetUnitTestAssert( l_throw_on_assert );

    return l_utce.RunTest( l_ostr );

} // end UnitTestExec

}; // namespace
