//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

#ifndef at_exports_h__included
#define at_exports_h__included

#if defined(_MSC_VER)

#pragma warning (disable : 4251)
#pragma warning (disable : 4231)

#if defined(austria_DLL)
#if defined(austria_EXPORTS)
#define AUSTRIA_EXPORT                                  __declspec( dllexport )
#define AUSTRIA_TEMPLATE_EXPORT(tmpl, param)            template class __declspec( dllexport ) tmpl < param >;
#define AUSTRIA_TEMPLATE_EXPORT2(tmpl, param, param2)   template class __declspec( dllexport ) tmpl < param, param2 >;
#else
#define AUSTRIA_EXPORT                                  __declspec( dllimport )
#define AUSTRIA_TEMPLATE_EXPORT(tmpl, param)            extern template class __declspec( dllimport ) tmpl < param >;
#define AUSTRIA_TEMPLATE_EXPORT2(tmpl, param, param2)   extern template class __declspec( dllimport ) tmpl < param, param2 >;
#endif //defined(austria_EXPORTS)
#endif

#endif // defined(_MSC_VER)

#if ! defined(AUSTRIA_EXPORT)

#define AUSTRIA_EXPORT
#define AUSTRIA_TEMPLATE_EXPORT(tmpl, param)
#define AUSTRIA_TEMPLATE_EXPORT2(tmpl, param, param2)

#endif // defined(_MSC_VER)


#endif // at_exports_h__included
