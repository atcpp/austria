// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_document.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_document_h_x
#define x_at_xml_document_h_x 1

#include "at_xml_composite.h"
#include "at_xml_element.h"
#include "at_xml_text.h"
#include "at_xml_comment.h"
#include "at_xml_proc_instr.h"

// Austria namespace
namespace at
{
    /**
     *  Objects of the XmlDocument class represent entire HTML or XML documents. Conceptually, an XmlDocument
     *  object is the root of the document tree, and provides the primary access to the document's data.
     *
     *  Since elements, text nodes, comments, etc. cannot exist outside the context of a Document, the Document
     *  interface also contains the factory methods needed to create these objects. The Node objects created
     *  have a OwnerDocument() attribute which associates them with the XmlDocument within whose context they
     *  were created. 
     */
    class XmlDocument : public XmlComposite
    {
    public:

        /**
         *  @brief  Creates an empty XML document.
         */
        XmlDocument() : XmlComposite( 0 ), m_rootElement( 0 ) {}

        /**
         *  @brief  The name of the document, which is equal to #document for all the documents.
         */
        virtual std::string Name() const;

        /**
         *  @brief  The type code of an XmlDocument node.
         *  @return XmlNode::DOCUMENT_NODE.
         */
        virtual unsigned short Type() const;

        /**
         *  @see XmlNode::Clone.
         */
        virtual PtrDelegate< XmlNode* > Clone( bool i_deep ) const;

        /**
         *  @see XmlNode::InsertBefore.
         */
        virtual PtrView< XmlNode* > InsertBefore( PtrDelegate< XmlNode* > i_newChild,
                                                  PtrView< XmlNode* >     i_refChild );

        /**
         *  @see XmlNode::AppendChild.
         */
        virtual PtrView< XmlNode* > AppendChild( PtrDelegate< XmlNode* > i_newChild );

        /**
         *  @see XmlNode::RemoveChild.
         */
        virtual PtrDelegate< XmlNode* > RemoveChild( PtrDelegate< XmlNode* > i_oldChild );

        /**
         *  @see XmlNode::ReplaceChild.
         */
        virtual PtrDelegate< XmlNode* > ReplaceChild( PtrDelegate< XmlNode* > i_newChild,
                                                      PtrDelegate< XmlNode* > i_oldChild );

        /**
         *  @brief  The root element of the document.
         *
         *  This is a convenience method that allows direct access to the child node that is the root element
         *  of the document.
         */
        PtrView< XmlElement* > DocumentElement() const { return m_rootElement; }

        /**
         *  @brief  Creates an element of the type specified.
         *  @param  i_tagName The name of the element type to instantiate.
         *  @return A new XmlElement object with the NodeName attribute set to i_tagName.
         *  @exception XmlInvalidCharacterErr Raised if the specified name contains an illegal character.
         */
        PtrDelegate< XmlElement* > CreateElement( const std::string& i_tagName );

        /**
         *  @brief  Creates a Text node given the specified string.
         *  @param  i_data The data for the node.
         *  @return The new XmlText object.
         */
        PtrDelegate< XmlText* > CreateTextNode( const std::string& i_data );

        /**
         *  @brief  Creates an XmlComment node given the specified string.
         *  @param  i_data The data for the node.
         *  @return The new XmlComment object.
         */
        PtrDelegate< XmlComment* > CreateComment( const std::string& i_data );

        /**
         *  @brief  Creates a processing instruction.
         *  @param  i_ownerDocument The XmlDocument that creates the processing instruction.
         *  @param  i_target        The target name of the processing instruction.
         *  @param  i_data          The content data of theprocessing instruction.
         */
        PtrDelegate< XmlProcessingInstruction* > CreateProcessingInstruction( const std::string& i_target,
                                                                              const std::string& i_data );

        /**
         *  @brief  Builds a list of all the elements with a given tag name.
         *  @param  i_tagName The name of the tag to match on. The special value "*" matches all tags.
         *  @return An XmlNodeList of all the XmlElement nodes with the given tag name, in the order in which
         *          they are encountered in a preorder traversal of the XmlDocument tree.
         */
        XmlNodeList GetElementsByTagName( const std::string& i_tagName ) const
            { return m_rootElement ? m_rootElement->GetElementsByTagName( i_tagName ) : XmlNodeList(); }

        /**
         *  @brief  Runtime type convertion to XmlDocument pointer.
         */
        virtual PtrView< XmlDocument* > AsDocument();

        /**
         *  @brief  Runtime type convertion to XmlDocument pointer.
         */
        virtual PtrView< const XmlDocument* > AsDocument() const;

        /**
         *  @brief Accepts a visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op );

        /**
         *  @brief Accepts a non-modifying, visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op ) const;

    protected:

        /**
         *  @brief  Checks if a node can be a child of this document.
         *  @exception ExceptionDerivation<XmlWrongDocumentErr>
         *             Raised if i_newChild was not created by this document.
         *  @exception ExceptionDerivation<XmlHierarchyRequestErr>
         *             Raised if the type of i_newChild is not one of ELEMENT_NODE, COMMENT_NODE,
         *             PROCESSING_INSTRUCTION_NODE or DOCUMENT_TYPE_NODE, or if it is ELEMENT_NODE,
         *             but the document already has a node of this type.
         */
        virtual void CheckHierarchyRequest( PtrView< const XmlNode* > i_newChild ) const;

    private:

        /**
         *  The copy constructor is declared as private and is not defined anywhere in the code. This is
         *  to prevent the compiler from defining a bitwise default copy constructor and using it.
         *  To make a copy of an XmlDocument, the Clone method must be used.
         */
        XmlDocument( const XmlDocument& );

        /**
         *  A pointer to the child node that is the root element of the document.
         *  @note We use as PtrView pointer since the element is already referenced by a Ptr pointer
         *        from the m_childNodes list.
         */
        Ptr< XmlElement* >  m_rootElement;
    };

    inline PtrDelegate< XmlElement* > XmlDocument::CreateElement( const std::string& i_tagName )
    {
        return PtrDelegate< XmlElement* >( new XmlElement( this, i_tagName ) );
    }

    inline PtrDelegate< XmlText* > XmlDocument::CreateTextNode( const std::string& i_data )
    {
        return PtrDelegate< XmlText* >( new XmlText( this, i_data ) );
    }

    inline PtrDelegate< XmlComment* > XmlDocument::CreateComment( const std::string& i_data )
    {
        return PtrDelegate< XmlComment* >( new XmlComment( this, i_data ) );
    }

    inline PtrDelegate< XmlProcessingInstruction* >
    XmlDocument::CreateProcessingInstruction( const std::string& i_target, const std::string& i_data )
    {
        return PtrDelegate< XmlProcessingInstruction* >( new XmlProcessingInstruction( this, i_target, i_data ) );
    }

}; // namespace

#endif // x_at_xml_node_h_x
