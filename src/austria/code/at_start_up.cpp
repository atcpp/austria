//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_start_up.cpp
 *
 */

#include "at_start_up.h"
#include "at_factory.h"
#include <stdlib.h>


// Austria namespace
namespace at
{

// ======== MainInitializer_Basic ===========================================
/*
 * MainInitializer_Basic is the basic implementation for the initializer.
 * This is implementation detail.
 */

class AUSTRIA_EXPORT MainInitializer_Basic
{
    friend class MainInitializer;


    // ======== MainInitializer ========================================
    /*
     *  MainInitializer_Basic constructor
     *
     *
     * @param argc  Reference to main's argc
     * @param argv  Reference to main's argv
     */

    MainInitializer_Basic( int & argc, const char ** & argv );

    // ======== ~MainInitializer_Basic =======================================
    /*
     * MainInitializer_Basic's destructor.
     *
     */

    ~MainInitializer_Basic();


    private:
    /*
     * MainInitializer_Basic is not allowed to be copy constructed
     * or assigned.
     */
    MainInitializer_Basic( const MainInitializer_Basic & );
    MainInitializer_Basic & operator=( const MainInitializer_Basic & );
    
    /*
     * A private list of initializers - nothing is allowed 
     * to touch these.
     */
    typedef Pointer<Initializer *>        t_array_elem;

    t_array_elem                        * m_array_of_initializers;

};


typedef FactoryRegister<
    Initializer,
    DKy,
    Creator2P< Initializer, DKy, int &, const char ** & >
>                                                 t_StartUpFactoryRegisterType;
    
    
// ======== GetInitializerCount =======================================
/**
 * GetInitializerCount will interrogate the initializer factory
 * for the count of initializers.
 *
 * @return The count of initializers to create
 */

unsigned GetInitializerCount()
{
    t_StartUpFactoryRegisterType        & l_registry =
        t_StartUpFactoryRegisterType::Get();

    return l_registry.m_map.size();
}



// ======== MainInitializer_Basic::MainInitializer_Basic ==========================

MainInitializer_Basic::MainInitializer_Basic( int & argc, const char ** & argv )
  : m_array_of_initializers( new t_array_elem[ GetInitializerCount() ] )
{
    t_StartUpFactoryRegisterType        & l_registry =
        t_StartUpFactoryRegisterType::Get();

    //
    // traverse the map - get the end 
    t_StartUpFactoryRegisterType::t_MapIterator l_end = l_registry.m_map.end();
    t_array_elem * l_itr = m_array_of_initializers;

    //
    // now traverse
    for (
        t_StartUpFactoryRegisterType::t_MapIterator i = l_registry.m_map.begin();
        i != l_end;
        i ++
    ) {
        // get the map entry
        t_StartUpFactoryRegisterType::t_MapEntry * l_entry = & (*i).second;

        * ( l_itr ++ ) = l_entry->m_factory->Create( argc, argv );
    }

}


// ======== ~MainInitializer_Basic::MainInitializer_Basic ==========================

MainInitializer_Basic::~MainInitializer_Basic()
{
    delete [] m_array_of_initializers;
}


// ======== MainInitializer::MainInitializer ==========================

MainInitializer::MainInitializer( int & argc, const char ** & argv )
{
    if ( ! m_mi )
    {
        m_mi = new MainInitializer_Basic( argc, argv );

        //
        // Register the MainInitializerCleanUp function to be called
        // at exit time.
        atexit( & MainInitializerCleanUp );
    }
}



// ======== MainInitializerCleanUp =====================================

void MainInitializer::MainInitializerCleanUp(void)
{
    MainInitializer_Basic        * l_li = m_mi;
    m_mi = 0;
    delete l_li;
}


// ======== ~MainInitializer::MainInitializer ==========================

MainInitializer::~MainInitializer()
{
    MainInitializerCleanUp();
}

//
// initialize the m_mi static variable.
//
MainInitializer_Basic * MainInitializer::m_mi = 0;


}; // namespace

