//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_factory.h
 *
 */

#ifndef x_at_factory_h_x
#define x_at_factory_h_x 1

#include "at_exports.h"

#include "at_source_locator.h"
#include "at_status_report.h"

#include "at_types.h"
#include "at_lifetime.h"

#include <map>
#include <sstream>

// Austria namespace
namespace at
{

/**
 * @defgroup GenericFactories Generic Factories

<h2>Introduction</h2>

<p>This module supports the factory pattern in a generic C++ way.
The Austria Factory module supports generic interfaces but also
supports generic construction paramters.  This can be used to
support plug-ins from shared libraries or DLL's.

<h2>Linking</h2>

<p>An Austria factory registers itself with the in-process
factory registry at initialization time.  This means that
factories are not useful until after main is called.

<p>There are a number of issues to be aware of with austria factories.
Because the mechanism for registering factories is truly generic, the
system linker may not be able to observe a requirement to link in
a factory from a factory library.  Austria's factories provide
a simple mechanism to pull in factories.

<p>To provide a mechanism that allows for hard-linking an unfortunate
side-effect is that the final factory name will tanscend namespaces.
The factory name is determined at the invocation of the AT_MakeFactory* or the
AT_MakeNamedFactory* macros.  For AT_MakeFactory*, the name is derived
from the implementation class name while AT_MakeNamedFactory* allows
the use of any valid C language identifier as a name.  Factory
names are only relevant in the case where hard linking is important.
The bottom line is that the choice of factory name must be such that
they do not collide with factories anywhere else regardless of namespace
or interface names.

<p>The generic method for hard-linking factories is to provide
a list of all the factories that are required in an application
specific "hard_linked.cpp" file in the form of a macro named HardLink.
The code below shows a simple example.

<CODE>
<pre>
// this file is hard_link.cpp

#define HardLink(A)         \
    A(FactoryName,0)        \
    A(FactoryFoo,2)         \
    A(FactoryBar,1)         \

// Include the factory hard link magic
// THIS MUST BE INCLUDED AFTER HardLink is defined
#include "at_factory_link.h"

</pre>
</CODE>

<p>The file "hard_link.cpp" above will create references to
the factory named "FactoryName" taking 0 constructor parameters,
the factory named "FactoryFoo" taking 2 constructor parameters etc.
Linking the result of this file into the application will cause the
linker to pull in the factories with the names specified in the
HardLink macro above.  There is no limit to the number of
hard_link.cpp files in an application but a different name for
the resulting hard linked symbol will be required.  The macro
AT_HardLinkSymbolName may be provided before including
at_factory_link.h to change the name of the resulting symbol.
Some linkers may still deduce that AT_HardLinkSymbolName is
not being used.  In this case,  there are 2 possible
solutions.  The first is to use a linker option that specifically
defines the hard-link symbol as required, or to simply reference
the symbol in your code using somthing like this:


<CODE>
<pre>
// this file is hard_link.cpp

#define HardLink(A)         \
    A(FactoryName,0)        \
    A(FactoryFoo,2)         \
    A(FactoryBar,1)         \

// Inlcude the factory hard link magic
// THIS MUST BE INCLUDED AFTER HardLink is defined
#include "at_factory_link.h"

bool SmartLinkerWorkAround( int i_n )
{
   return AT_HardLinkSymbolName[i_n] == 0;
}

</pre>
</CODE>

<p>Followed by placing a call to SmartLinkerWorkAround
in main for example.

<CODE>
<pre>
// this is main
extern bool SmartLinkerWorkAround( int i_n );

int main()
{
    SmartLinkerWorkAround(0);

    .... application code.
}

</pre>
</CODE>

<p>The alternative is to use the linker to force the linking.
On Visual Studio, the linker option is "/INCLUDE:symbol".  The
GNU linker supports a similar option "--undefined=SYMBOL".

<h2>Plug-Ins</h2>

<p>Austria factories may also be used with dynamic libraries.  This
issue to be careful here is to make sure that the factory registry
method is linked in only one place.  The registration of new
libraries is (currently) not thread safe, so if factory registries
are being modified (by loading or unloading a new .so or .dll) while they are also
being searched, unpredictable issues will occur.  The thread safety
issue will be addressed later.


 *
 * 
 *  @{
 */

// ======== DKy ====================================================
/**
 * A default key type for factories.  The reason for the short key name
 * is to reduce complaints from various compilers about generated
 * symbols being excessively large.
 *
 * This is *not* a generic string class.  The value passed into the
 * DKy constructor must exist for the lifetime of constructed
 * DKy object.  For literal strings, this is always true which
 * is the more common (if not only) use of this class.
 *
 * $title
 */

class AUSTRIA_EXPORT DKy
{
    public:

    /**
     * constructed from a statically allocated char *.  The
     * pointer passed as i_value must exist for the life of this
     * object.
     *
     * @param i_value Pointer to a null terminated string - usually
     *                a string literal.
     */ 

    inline DKy( const char * i_value )
      : m_value( i_value )
    {
    }

    virtual ~DKy() {}

    /**
     * Conversion from DKy to 'cont char *'.
     */
    
    inline operator const char * () const
    {
        return m_value;
    }

    /**
     * Conversion from DKy to AT library string type (std::string).
     */
    
    inline operator AT_String () const
    {
        return AT_String( m_value );
    }

    /**
     * Compare two DKy for equality
     * @param i_value is the RHS of the operator==.
     */

    bool operator==( const DKy &i_value ) const;

    /**
     * Compare two DKy for less than
     * @param i_value is the RHS of operator<.
     */

    bool operator<( const DKy &i_value ) const;

    /**
     * Compare two DKy for less than or equal
     * @param i_value is the RHS of operator<=
     */

    bool operator<=( const DKy &i_value ) const;

    /**
     * Compare two DKy for greater than 
     * @param i_value is the RHS of operator>
     */

    bool operator>( const DKy &i_value ) const;

    /**
     * Compare two DKy for greater than or equal
     * @param i_value is the RHS of operator>=
     */

    bool operator>=( const DKy &i_value ) const;

    private:

    const char        * m_value;    // contains a simple pointer

};

//
// ============================================================================
// ============================================================================
//
// The following classes define templates for passing parameters to
// constructors.
//
// ============================================================================
// ============================================================================
//


// ======== FactoryTraits ==========================================
/**
 * FactoryTraits contains basic enums and interfaces for all 
 * factories.
 */

class AUSTRIA_EXPORT FactoryTraits
{
    public:

    /**
     * FactoryErrorCodes is a set of result codes for status
     * conditions in the factory mechanism.
     */

    enum FactoryErrorCodes
    {
        /**
         * FactoryNotFound indicates that the desired factory
         * was not found.
         */
        
        FactoryNotFound,

        /**
         * FactoryCreateException is reported when the factory Create method
         * throws an exception.
         */

        FactoryCreateException,

        /**
         * FactoryFailure is reported when the factory Create method returns
         * null.
         */
        
        FactoryFailure
    };

    /**
     * FactoryCreateReportingOption is a set of codes 
     * that describe which action to take when creating
     * a factory object.
     */
    
    enum FactoryCreateReportingOption
    {

        /**
         * AlwaysReturn requests that a create request does not throw
         * and always returns.  If an error occurs a null pointer
         * is returned.
         */

        AlwaysReturn,

        /**
         * ThrowOnError requests that an exception is thrown (of type
         * StatusReport ).
         */

        ThrowOnError,

        /**
         * AssertOnError requests that the program aborts on error
         * via an assert.
         */

        AssertOnError
    };
        
        
};


// ======== Wrapper_Basic ==========================================
/**
 * Wrapper_Basic provides some basic wrapper functions methods
 * for reporting errors, throwing exceptions etc.  It is the base
 * class for all factories.
 */

class AUSTRIA_EXPORT Wrapper_Basic
{
    public:

    // ======== ReportError ===========================================
    /**
     * ReportError will perform the error reporting options requested.
     *
     * @param i_code this code indicates the mode of failure
     * @param i_msg this is the descriptive message associated
     * @return nothing
     */

    void ReportError(
        FactoryTraits::FactoryErrorCodes         i_code,
        const AT_String                           & i_msg
    );

    // ======== ReportFactoryNotFound =================================
    /**
     * ReportFactoryNotFound reports a "FactoryNotFound" error.
     */
    
    void ReportFactoryNotFound();
    
    // ======== ReportCreateException =================================
    /**
     * ReportCreateException reports an exception thrown error.
     *
     * @param i_keystr is the key converted to a human readable string.
     */
    
    void ReportCreateException( const AT_String & i_keystr );


    // ======== ReportFactoryFailed ===================================
    /**
     * ReportFactoryFailed reports a factory failed error,
     *
     * @param i_keystr is the key converted to a human readable string.
     */
     
    void ReportFactoryFailed( const AT_String & i_keystr );


    // ======== Wrapper_Basic ======================================
    /**
     * Wrapper_Basic constructor
     */

    inline Wrapper_Basic(
        StatusReport                                 * io_srep,
        FactoryTraits::FactoryCreateReportingOption    i_create_report_option
    )
      : m_srep( io_srep ),
        m_create_report_option( i_create_report_option )
    {
    }

    virtual ~Wrapper_Basic()
    {
    }

    /**
     * m_srep is set to the user requested status report interface.
     */
    StatusReport                                 * m_srep;

    /**
     * m_create_report_option indicates the type of action to take when
     * reporting an error.
     */
    FactoryTraits::FactoryCreateReportingOption    m_create_report_option;

    
};


// ======== KeyToStr ===============================================
/**
 *  Convert the given key to a string for status reports or other uses.
 *
 * @param i_key is the key to convert to a string
 * @return the string representation of the key
 */

template< typename  w_KeyType >
AT_String KeyToStr( const w_KeyType & i_key )
{
    std::ostringstream  l_ostream;

    l_ostream << i_key;
    
    return l_ostream.str();

} // end KeyToStr



// ======== FactoryRegistryEntry ===================================
/**
 * This interface allows a factory to unregister from a registry.
 * This is needed to clean up when a dso/dll is unloaded.  It also
 * cleans up leaks so that a memory leak trace is not so cluttered.
 * This needs to be an interface because it's implementation cannot
 * be known because it's nigh impossible to define an implemnetation
 * here.
 */

class AUSTRIA_EXPORT FactoryRegistryEntry
{
    public:

    /**
     * virtual destructor required here because the object
     * may be deleted.
     * Deleting this object type will *not* remove the entry,
     * RemoveAndDelete should be called to delete AND remove
     * and entry.
     *
     * This would be used when the factory registry is being deleted
     * and the factory itself is not.
     */

    virtual ~FactoryRegistryEntry()
    {
    }


    // ======== RemoveAndDelete =======================================
    /**
     * RemoveAndDelete *will* remove an entry AND delete the
     * registry entry.  This is usually called by the factory when
     * the factory is going away.
     *
     * @return nothing
     */

    virtual void RemoveAndDelete() = 0;
    
};


// ======== FactoryEntryLocator_Base ===============================
/**
 * A factory entry locator manages the removal of the registry
 * entry from a factory registry.
 */

class AUSTRIA_EXPORT FactoryEntryLocator_Base
{
    public:

    FactoryEntryLocator_Base()
      : m_registry_entry( 0 )
    {
    }

    // ======== ~FactoryEntryLocator_Base ===========================
    /**
     * upon destruction of the FactoryEntryLocator_Base object
     * the entry needs to be removed.
     */

    virtual ~FactoryEntryLocator_Base()
    {   
        FactoryRegistryEntry * l_registry_entry( m_registry_entry );
        
        if ( l_registry_entry != 0 )
        {
            m_registry_entry = 0;

            // this may cause FactoryEntryLocator_Base::RemoveEntry
            // to be called but that's ok since m_registry_entry will
            // be 0 and hence will not do anything.
            l_registry_entry->RemoveAndDelete();
        }
        
    } // end ~FactoryEntryLocator_Base


    // ======== RemoveEntry ===========================================
    /**
     * RemoveEntry removes an entry in a factory - it does not
     * delete the factory itself. This happens in the case where
     * a system is being shut down and the registry is being
     * destroyed before the factory itself.
     *
     * @return nothing
     */

    void RemoveEntry()
    {

        FactoryRegistryEntry * l_registry_entry( m_registry_entry );

        if ( l_registry_entry != 0 )
        {
            // we can get called recusively.  Setting m_registry_entry
            // breaks the cycle.
            m_registry_entry = 0;

            delete l_registry_entry;
        }

        return;

    } // end RemoveEntry
    
    /**
     * m_registry_entry is used to point back to the registry
     * that this factory is inserted into.  This is to manage the
     * unloading of the dso/dll.
     */
    FactoryRegistryEntry                         * m_registry_entry;

};



// ======== Factory_Base ================================================
/**
 * Factory_Base is the base type for a factory.
 * type.  For each implementation, a factory derived from this one is created
 * that implements the respective Create method.
 */

template<
    typename    w_KeyType
>
class Factory_Base
  : public SourceLocator_Basic,
    public FactoryEntryLocator_Base
{
    public:

    Factory_Base(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description     
    )
      : SourceLocator_Basic( i_filename, i_lineno ),
        m_description( i_description ),
        m_factory_key( i_key )
    {
    }

    virtual ~Factory_Base() {}

    // ======== Size ==================================================
    /**
     * This will return the size of the object created - this is to
     * support the PIMPL idiom.
     *
     * @return size of object
     */
    
    virtual int Size() = 0;

    
    // ======== GetKey ================================================
    /**
     *  GetKey returns a const reference to the key for this factory
     *
     */

    inline const w_KeyType & GetKey() const
    {
        return m_factory_key;
    } // end GetKey

    /**
     * factory description
     */
    const char                                      * m_description;

    /**
     * m_factory_key contains the key for this factory.
     */
    w_KeyType                                         m_factory_key;

};


// ======== Creator0P ===============================================
/**
 * Interface for creating objects using constructors with 0
 * parameters.
 */

template<
    typename    w_InterfaceType,
    typename    w_KeyType
>
class Creator0P
  : public Factory_Base< w_KeyType >
{
    public:


    // ======== Creator0P ===========================================
    /**
     * All creator constructors take their key.
     *
     * @param i_key is the key of this factory
     */

    inline Creator0P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description     
    )
      : Factory_Base< w_KeyType >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    } // end Creator0P

    virtual ~Creator0P()
    {
    }
    

    // ======== Create ================================================
    /**
     * Create will invoke new directly - this method is
     * overridden by the implementation specific class.
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create() = 0;

    // ======== Create ================================================
    /**
     * Create will invoke placement new - this method is
     * overridden by the implementation specific class.
     * 
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create( void * i_location ) = 0;
    

    // ======== Destroy ===============================================
    /**
     * This supports objects with no virtual destructor (which is obviously
     * a very rare case).  This may be used in the case where a virtual
     * method is not applicable, e.g. using shared memory.
     *
     * More support for the PIMPL idiom.
     *
     * @return size of object
     */
    
    virtual void Destroy( w_InterfaceType * i_ptr ) = 0;

    
    // ======== CreateWrapper =========================================
    /**
     * CreateWrapper is a wrapper to control how objects are created.
     * This is mainly a convenience to allow one liner factory find
     * and create.
     */

    struct CreateWrapper
      : public Wrapper_Basic
    {

        /**
         * CreateWrapper constructor takes a factory interface a
         * status reporting interface and a modifier to indicate
         * what to do on error.
         */

        CreateWrapper(
            Creator0P                                    * i_creator,
            StatusReport                                 * o_srep = 0,
            FactoryTraits::FactoryCreateReportingOption    i_option = FactoryTraits::AlwaysReturn
        )
          : Wrapper_Basic( o_srep, i_option ),
            m_creator( i_creator )
        {
        }

        virtual ~CreateWrapper() {}

        /**
         * operator w_InterfaceType * () invokes the factory create method.
         * Unfortunately, this is specific to each type of paramters.
         */

        w_InterfaceType * operator() ()
        {
            if ( m_creator == 0 )
            {
                ReportFactoryNotFound();
                return 0;
            }

            w_InterfaceType                         * l_interface;
            
            try
            {
                l_interface = m_creator->Create();
            }
            catch ( ... )
            {
                ReportCreateException( KeyToStr( m_creator->GetKey() ) );

                if ( m_create_report_option == FactoryTraits::ThrowOnError )
                {
                    throw;  // this should re-throw the same exception that was
                            // caught.
                }

                return 0;   // finished.
            }

            if ( l_interface == 0 )
            {
                ReportFactoryFailed( KeyToStr( m_creator->GetKey() ) );
            }

            return l_interface;
        }

        /**
         * m_creator contains a pointer to the actual factory.
         */
        
        Creator0P                                    * m_creator;

    }; // end CreateWrapper

    //
    // Member variables for Creator0P
    //

};


// ======== CreatorImpl0P ===========================================
/**
 * Implementations for creating objects using constructors with 0
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType
>
class CreatorImpl0P
  : public Creator0P< w_InterfaceType, w_KeyType >
{
    public:

    typedef Creator0P< w_InterfaceType, w_KeyType > t_CreatorType;

    CreatorImpl0P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : Creator0P< w_InterfaceType, w_KeyType >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    }

    virtual ~CreatorImpl0P()
    {
    }
    
    virtual w_InterfaceType * Create()
    {
        return new w_ImplementorType();
    }
    
    virtual int Size()
    {
        return sizeof( w_ImplementorType );
    }

    virtual w_InterfaceType * Create( void * i_location )
    {
        return new ( i_location ) w_ImplementorType;
    }

    virtual void Destroy( w_InterfaceType * i_ptr )
    {
        w_ImplementorType * l_impl = static_cast< w_ImplementorType * >( i_ptr );

        l_impl->~w_ImplementorType();
    }

    
};


// ======== Creator1P ===============================================
/**
 * Interface for creating objects using constructors with 1
 * parameters.
 */

template<
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1
>
class Creator1P
  : public Factory_Base< w_KeyType >
{
    public:


    // ======== Creator1P ===========================================
    /**
     * All creator constructors take their key.
     *
     * @param i_key
     */

    inline Creator1P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description     
    )
      : Factory_Base< w_KeyType >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    } // end Creator1P

    virtual ~Creator1P()
    {
    }
    

    // ======== Create ================================================
    /**
     * Create will invoke the contructor directly - this method is
     * overridden by the implementation specific class.
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1 ) = 0;

    // ======== Destroy ===============================================
    /**
     * This supports objects with no virtual destructor (which is obviously
     * a very rare case).  This may be used in the case where a virtual
     * method is not applicable, e.g. using shared memory.
     *
     * More support for the PIMPL idiom.
     *
     * @return size of object
     */
    
    virtual void Destroy( w_InterfaceType * i_ptr ) = 0;
    
    // ======== Create ================================================
    /**
     * Create will invoke placement new - this method is
     * overridden by the implementation specific class.
     * 
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, void * i_location ) = 0;
    

    // ======== CreateWrapper =========================================
    /**
     * CreateWrapper is a wrapper to control how objects are created.
     * This is mainly a convenience to allow one liner factory find
     * and create.
     */

    struct CreateWrapper
      : public Wrapper_Basic
    {

        /**
         * CreateWrapper constructor takes a factory interface a
         * status reporting interface and a modifier to indicate
         * what to do on error.
         */

        CreateWrapper(
            Creator1P                                    * i_creator,
            StatusReport                                 * o_srep = 0,
            FactoryTraits::FactoryCreateReportingOption    i_option = FactoryTraits::AlwaysReturn
        )
          : Wrapper_Basic( o_srep, i_option ),
            m_creator( i_creator )
        {
        }

        virtual ~CreateWrapper() {}

        /**
         * operator w_InterfaceType * () invokes the factory create method.
         * Unfortunately, this is specific to each type of paramters.
         */

        w_InterfaceType * operator() ( w_arg_1 i_arg_1 )
        {
            if ( m_creator == 0 )
            {
                ReportFactoryNotFound();
                return 0;
            }

            w_InterfaceType                         * l_interface;
            
            try
            {
                l_interface = m_creator->Create( i_arg_1 );
            }
            catch ( ... )
            {
                ReportCreateException( KeyToStr( m_creator->GetKey() ) );

                if ( m_create_report_option == FactoryTraits::ThrowOnError )
                {
                    throw;  // this should re-throw the same exception that was
                            // caught.
                }

                return 0;   // finished.
            }

            if ( l_interface == 0 )
            {
                ReportFactoryFailed( KeyToStr( m_creator->GetKey() ) );
            }

            return l_interface;
        }

        /**
         * m_creator contains a pointer to the actual factory.
         */
        
        Creator1P                                    * m_creator;

    }; // end CreateWrapper

    //
    // Member variables for Creator1P
    //

};


// ======== CreatorImpl1P ===========================================
/**
 * Implementations for creating objects using constructors with 1
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1
>
class CreatorImpl1P
  : public Creator1P< w_InterfaceType, w_KeyType, w_arg_1 >
{
    public:

    typedef Creator1P< w_InterfaceType, w_KeyType, w_arg_1 > t_CreatorType;

    CreatorImpl1P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : Creator1P< w_InterfaceType, w_KeyType, w_arg_1 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    }

    virtual ~CreatorImpl1P()
    {
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1 )
    {
        return new w_ImplementorType( i_arg_1 );
    }
    
    virtual int Size()
    {
        return sizeof( w_ImplementorType );
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, void * i_location )
    {
        return new ( i_location ) w_ImplementorType( i_arg_1 );
    }

    virtual void Destroy( w_InterfaceType * i_ptr )
    {
        w_ImplementorType * l_impl = static_cast< w_ImplementorType * >( i_ptr );

        l_impl->~w_ImplementorType();
    }

};



// ======== Creator2P ===============================================
/**
 * Interface for creating objects using constructors with 2
 * parameters.
 */

template<
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2
>
class Creator2P
  : public Factory_Base< w_KeyType >
{
    public:


    // ======== Creator2P ===========================================
    /**
     * All creator constructors take their key.
     *
     * @param i_key
     */

    inline Creator2P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description     
    )
      : Factory_Base< w_KeyType >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    } // end Creator2P

    virtual ~Creator2P()
    {
    }
    

    // ======== Create ================================================
    /**
     * Create will invoke the contructor directly - this method is
     * overridden by the implementation specific class.
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2 ) = 0;

    
    // ======== Destroy ===============================================
    /**
     * This supports objects with no virtual destructor (which is obviously
     * a very rare case).  This may be used in the case where a virtual
     * method is not applicable, e.g. using shared memory.
     *
     * More support for the PIMPL idiom.
     *
     * @return size of object
     */
    
    virtual void Destroy( w_InterfaceType * i_ptr ) = 0;
    
    
    // ======== Create ================================================
    /**
     * Create will invoke placement new - this method is
     * overridden by the implementation specific class.
     * 
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, void * i_location ) = 0;


    // ======== CreateWrapper =========================================
    /**
     * CreateWrapper is a wrapper to control how objects are created.
     * This is mainly a convenience to allow one liner factory find
     * and create.
     */

    struct CreateWrapper
      : public Wrapper_Basic
    {

        /**
         * CreateWrapper constructor takes a factory interface a
         * status reporting interface and a modifier to indicate
         * what to do on error.
         */

        CreateWrapper(
            Creator2P                                    * i_creator,
            StatusReport                                 * o_srep = 0,
            FactoryTraits::FactoryCreateReportingOption    i_option = FactoryTraits::AlwaysReturn
        )
          : Wrapper_Basic( o_srep, i_option ),
            m_creator( i_creator )
        {
        }

        virtual ~CreateWrapper() {}

        /**
         * operator w_InterfaceType * () invokes the factory create method.
         * Unfortunately, this is specific to each type of paramters.
         */

        w_InterfaceType * operator() ( w_arg_1 i_arg_1, w_arg_2 i_arg_2 )
        {
            if ( m_creator == 0 )
            {
                ReportFactoryNotFound();
                return 0;
            }

            w_InterfaceType                         * l_interface;
            
            try
            {
                l_interface = m_creator->Create( i_arg_1, i_arg_2 );
            }
            catch ( ... )
            {
                ReportCreateException( KeyToStr( m_creator->GetKey() ) );

                if ( m_create_report_option == FactoryTraits::ThrowOnError )
                {
                    throw;  // this should re-throw the same exception that was
                            // caught.
                }

                return 0;   // finished.
            }

            if ( l_interface == 0 )
            {
                ReportFactoryFailed( KeyToStr( m_creator->GetKey() ) );
            }

            return l_interface;
        }

        /**
         * m_creator contains a pointer to the actual factory.
         */
        
        Creator2P                                    * m_creator;

    }; // end CreateWrapper

    //
    // Member variables for Creator2P
    //

};


// ======== CreatorImpl2P ===========================================
/**
 * Implementations for creating objects using constructors with 2
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2
>
class CreatorImpl2P
  : public Creator2P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2 >
{
    public:

    typedef Creator2P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2 > t_CreatorType;

    CreatorImpl2P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : Creator2P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    }

    virtual ~CreatorImpl2P()
    {
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2 )
    {
        return new w_ImplementorType( i_arg_1, i_arg_2 );
    }
    
    virtual int Size()
    {
        return sizeof( w_ImplementorType );
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, void * i_location )
    {
        return new ( i_location ) w_ImplementorType( i_arg_1, i_arg_2 );
    }

    virtual void Destroy( w_InterfaceType * i_ptr )
    {
        w_ImplementorType * l_impl = static_cast< w_ImplementorType * >( i_ptr );

        l_impl->~w_ImplementorType();
    }


};

// ======== Creator3P ===============================================
/**
 * Interface for creating objects using constructors with 2
 * parameters.
 */

template<
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2,
    typename    w_arg_3
>
class Creator3P
  : public Factory_Base< w_KeyType >
{
    public:


    // ======== Creator3P ===========================================
    /**
     * All creator constructors take their key.
     *
     * @param i_key
     */

    inline Creator3P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description     
    )
      : Factory_Base< w_KeyType >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    } // end Creator3P

    virtual ~Creator3P()
    {
    }

    // ======== Create ================================================
    /**
     * Create will invoke the contructor directly - this method is
     * overridden by the implementation specific class.
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3 ) = 0;

    
    // ======== Destroy ===============================================
    /**
     * This supports objects with no virtual destructor (which is obviously
     * a very rare case).  This may be used in the case where a virtual
     * method is not applicable, e.g. using shared memory.
     *
     * More support for the PIMPL idiom.
     *
     * @return size of object
     */
    
    virtual void Destroy( w_InterfaceType * i_ptr ) = 0;
    
    
    // ======== Create ================================================
    /**
     * Create will invoke placement new - this method is
     * overridden by the implementation specific class.
     * 
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create(
        w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3, void * i_location
    ) = 0;


    // ======== CreateWrapper =========================================
    /**
     * CreateWrapper is a wrapper to control how objects are created.
     * This is mainly a convenience to allow one liner factory find
     * and create.
     */

    struct CreateWrapper
      : public Wrapper_Basic
    {

        /**
         * CreateWrapper constructor takes a factory interface a
         * status reporting interface and a modifier to indicate
         * what to do on error.
         */

        CreateWrapper(
            Creator3P                                    * i_creator,
            StatusReport                                 * o_srep = 0,
            FactoryTraits::FactoryCreateReportingOption    i_option = FactoryTraits::AlwaysReturn
        )
          : Wrapper_Basic( o_srep, i_option ),
            m_creator( i_creator )
        {
        }

        virtual ~CreateWrapper() {}

        /**
         * operator w_InterfaceType * () invokes the factory create method.
         * Unfortunately, this is specific to each type of paramters.
         */

        w_InterfaceType * operator() ( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3 )
        {
            if ( m_creator == 0 )
            {
                ReportFactoryNotFound();
                return 0;
            }

            w_InterfaceType                         * l_interface;
            
            try
            {
                l_interface = m_creator->Create( i_arg_1, i_arg_2, i_arg_3 );
            }
            catch ( ... )
            {
                ReportCreateException( KeyToStr( m_creator->GetKey() ) );

                if ( m_create_report_option == FactoryTraits::ThrowOnError )
                {
                    throw;  // this should re-throw the same exception that was
                            // caught.
                }

                return 0;   // finished.
            }

            if ( l_interface == 0 )
            {
                ReportFactoryFailed( KeyToStr( m_creator->GetKey() ) );
            }

            return l_interface;
        }

        /**
         * m_creator contains a pointer to the actual factory.
         */
        
        Creator3P                                    * m_creator;

    }; // end CreateWrapper

    //
    // Member variables for Creator3P
    //

};


// ======== CreatorImpl3P ===========================================
/**
 * Implementations for creating objects using constructors with 2
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2,
    typename    w_arg_3
>
class CreatorImpl3P
  : public Creator3P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3 >
{
    public:

    typedef Creator3P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3 > t_CreatorType;

    CreatorImpl3P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : Creator3P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    }

    virtual ~CreatorImpl3P()
    {
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3 )
    {
        return new w_ImplementorType( i_arg_1, i_arg_2, i_arg_3 );
    }
    
    virtual int Size()
    {
        return sizeof( w_ImplementorType );
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3, void * i_location )
    {
        return new ( i_location ) w_ImplementorType( i_arg_1, i_arg_2, i_arg_3 );
    }

    virtual void Destroy( w_InterfaceType * i_ptr )
    {
        w_ImplementorType * l_impl = static_cast< w_ImplementorType * >( i_ptr );

        l_impl->~w_ImplementorType();
    }


};


// ======== Creator4P ===============================================
/**
 * Interface for creating objects using constructors with 2
 * parameters.
 */

template<
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2,
    typename    w_arg_3,
    typename    w_arg_4
>
class Creator4P
  : public Factory_Base< w_KeyType >
{
    public:


    // ======== Creator4P ===========================================
    /**
     * All creator constructors take their key.
     *
     * @param i_key
     */

    inline Creator4P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description     
    )
      : Factory_Base< w_KeyType >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    } // end Creator4P

    virtual ~Creator4P()
    {
    }

    // ======== Create ================================================
    /**
     * Create will invoke the contructor directly - this method is
     * overridden by the implementation specific class.
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3, w_arg_4 i_arg_4 ) = 0;

    
    // ======== Destroy ===============================================
    /**
     * This supports objects with no virtual destructor (which is obviously
     * a very rare case).  This may be used in the case where a virtual
     * method is not applicable, e.g. using shared memory.
     *
     * More support for the PIMPL idiom.
     *
     * @return size of object
     */
    
    virtual void Destroy( w_InterfaceType * i_ptr ) = 0;
    
    
    // ======== Create ================================================
    /**
     * Create will invoke placement new - this method is
     * overridden by the implementation specific class.
     * 
     *
     * @return pointer to new object
     */
    
    virtual w_InterfaceType * Create(
        w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3, w_arg_4 i_arg_4, void * i_location
    ) = 0;


    // ======== CreateWrapper =========================================
    /**
     * CreateWrapper is a wrapper to control how objects are created.
     * This is mainly a convenience to allow one liner factory find
     * and create.
     */

    struct CreateWrapper
      : public Wrapper_Basic
    {

        /**
         * CreateWrapper constructor takes a factory interface a
         * status reporting interface and a modifier to indicate
         * what to do on error.
         */

        CreateWrapper(
            Creator4P                                    * i_creator,
            StatusReport                                 * o_srep = 0,
            FactoryTraits::FactoryCreateReportingOption    i_option = FactoryTraits::AlwaysReturn
        )
          : Wrapper_Basic( o_srep, i_option ),
            m_creator( i_creator )
        {
        }

        virtual ~CreateWrapper() {}

        /**
         * operator w_InterfaceType * () invokes the factory create method.
         * Unfortunately, this is specific to each type of paramters.
         */

        w_InterfaceType * operator() ( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3, w_arg_4 i_arg_4 )
        {
            if ( m_creator == 0 )
            {
                ReportFactoryNotFound();
                return 0;
            }

            w_InterfaceType                         * l_interface;
            
            try
            {
                l_interface = m_creator->Create( i_arg_1, i_arg_2, i_arg_3, i_arg_4 );
            }
            catch ( ... )
            {
                ReportCreateException( KeyToStr( m_creator->GetKey() ) );

                if ( m_create_report_option == FactoryTraits::ThrowOnError )
                {
                    throw;  // this should re-throw the same exception that was
                            // caught.
                }

                return 0;   // finished.
            }

            if ( l_interface == 0 )
            {
                ReportFactoryFailed( KeyToStr( m_creator->GetKey() ) );
            }

            return l_interface;
        }

        /**
         * m_creator contains a pointer to the actual factory.
         */
        
        Creator4P                                    * m_creator;

    }; // end CreateWrapper

    //
    // Member variables for Creator4P
    //

};


// ======== CreatorImpl4P ===========================================
/**
 * Implementations for creating objects using constructors with 2
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2,
    typename    w_arg_3,
    typename    w_arg_4
>
class CreatorImpl4P
  : public Creator4P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3, w_arg_4 >
{
    public:

    typedef Creator4P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3, w_arg_4 > t_CreatorType;

    CreatorImpl4P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : Creator4P< w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3, w_arg_4 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
    }

    virtual ~CreatorImpl4P()
    {
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3, w_arg_4 i_arg_4 )
    {
        return new w_ImplementorType( i_arg_1, i_arg_2, i_arg_3, i_arg_4 );
    }
    
    virtual int Size()
    {
        return sizeof( w_ImplementorType );
    }
    
    virtual w_InterfaceType * Create( w_arg_1 i_arg_1, w_arg_2 i_arg_2, w_arg_3 i_arg_3, w_arg_4 i_arg_4, void * i_location )
    {
        return new ( i_location ) w_ImplementorType( i_arg_1, i_arg_2, i_arg_3, i_arg_4 );
    }

    virtual void Destroy( w_InterfaceType * i_ptr )
    {
        w_ImplementorType * l_impl = static_cast< w_ImplementorType * >( i_ptr );

        l_impl->~w_ImplementorType();
    }


};


//
// ============================================================================
// ============================================================================
//
// The basic factory class.
//
// ============================================================================
// ============================================================================
//



// ======== FactoryRegisterBase =======================================
/**
 * FactoryRegisterBase is the base class to all FactoryRegister's.
 * A pointer to one of these is placed in the FactoryRegisterRegistry.
 *
 * In general, a FactoryRegister must be a singleton.  Unfortunately
 * this will not happen in all circumstances, especially with
 * dynamically linked libraries.  The solution to this is to provide
 * a single registry of registries.
 *
 * This is a reference counted class because DLL's may be loaded and
 * unloaded.
 */

class AUSTRIA_EXPORT FactoryRegisterBase
  : public PtrTarget_Basic
{
    public:

    // ======== Key ===================================================
    /**
     *  Key returns a string representing the key of this registry.
     *
     * @return a pointer to DKy
     */

    virtual const DKy * Key() = 0;


    // ======== Version ===============================================
    /**
     *  Version will return an integer that indicates the version
     *  of a factory registry.  This is to eliminate issues regarding
     *  version problems.
     *
     * @return An integer version number.
     */

    virtual int Version() = 0;

    virtual ~FactoryRegisterBase() {}
};


// ======== FactoryRegisterRegistry ===================================
/**
 * FactoryRegisterRegistry is the registry of registries and 
 * is accessible from the single static function.
 */

class AUSTRIA_EXPORT FactoryRegisterRegistry
  : public PtrTarget_Basic
{
    public:

    typedef PtrDelegate<FactoryRegisterBase *> ( * t_creator )();

    // ======== FindOrCreate ==========================================
    /**
     *  Find will search the registry for the key required.
     *
     * @param i_key is the key of the factory being requested.
     * @param i_create_meth is a static method that returns
     *        a factory of the given type.
     * @return A pointer to the factory register.
     */

    virtual PtrDelegate<FactoryRegisterBase *> FindOrCreate(
        const DKy               & i_key,
        t_creator                 i_create_meth
    ) = 0;

    // ======== Shutdown ==============================================
    /**
     * Shutdown will clear the factory registry.  This can be used to
     * guarentee references to factories are removed on shutdown.
     */
    virtual void Shutdown() = 0;

    // ======== Get ===================================================
    /**
     * Get will return a pointer to the single factory register registry.
     * This method is only available from the Austria library.
     *
     * @return The factory register registry
     */

    static PtrView<FactoryRegisterRegistry *> Get();

    virtual ~FactoryRegisterRegistry() {}
};


// ======== FactoryRegister ========================================
/**
 * This is a registry for all factories that implement a specific
 * interface and constructor signature.  The key type to find a given
 * entry is also part of the given definition.
 */

template<
    typename    w_InterfaceType,
    typename    w_KeyType = DKy,
    typename    w_CreatorType = Creator0P< w_InterfaceType, w_KeyType >
>
class FactoryRegister
  : public FactoryRegisterBase
{
public:

    virtual ~FactoryRegister() {}

private:

    FactoryRegister( const FactoryRegister & );
    
    FactoryRegister()
      : m_register_key( ( typeid( FactoryRegister ) ).name() )
    {
    }

    const DKy                               m_register_key;

    // ======== Create ================================================
    /**
     * Create will create a new FactoryRegister.  This should only be called
     * by the FactoryRegisterRegistry FindOrCreate function.
     *
     * @return A new factory register
     */

    static PtrDelegate<FactoryRegisterBase *> Create()
    {
        return new FactoryRegister();
    }


    // ======== FactoryRegisterVersion ================================
    /**
     *  This is the factory version number.
     *
     * @return a version number.
     */

    static int FactoryRegisterVersion()
    {
        return sizeof( FactoryRegister );
    }

    // ======== Key ===================================================
    /**
     *  Key returns a string representing the key of this registry.
     *
     * @return a DKy
     */

    virtual const DKy * Key()
    {
        return & m_register_key;
    }


    // ======== Version ===============================================
    /**
     *  Version will return the version of this FactoryRegister
     *
     * @return The version of this FactoryRegister
     */
    
    virtual int Version()
    {
        return FactoryRegisterVersion();
    }

public:

    /**
     * t_MapEntry is the entry type.
     * 
     */

    struct t_MapEntry
    {
        /**
         * t_MapEntry destructor - this removes the factory
         * registry entry pointer because there is nothing
         * more to delete.
         */

        virtual ~t_MapEntry()
        {
            if ( m_factory != 0 )
            {
                m_factory->RemoveEntry();
            }
        }

        /**
         * construct a map entry from another map entry.
         */
        
        t_MapEntry( const t_MapEntry & i_map_entry )
          : m_factory( i_map_entry.Transfer() )
        {
        }

        t_MapEntry( w_CreatorType * i_factory = 0 )
          : m_factory( i_factory )
        {
        }

        // ======== Transfer ==========================================
        /**
         * This is a helper method
         * @return w_CreatorType (factory) pointer
         */

        w_CreatorType * Transfer() const
        {
            w_CreatorType * l_factory( m_factory );

            m_factory = 0;
            
            return l_factory;

        } // end Transfer

        /**
         * m_factory points to the factory.
         */
        mutable w_CreatorType                       * m_factory;

    };

    /**
     * t_EntryCompare is used to compare keys in the given map.
     * A key type must al least implement operator<()
     */
    
    struct t_EntryCompare
    {
        bool operator()( const w_KeyType * s1, const w_KeyType * s2 ) const
        {
            return (*s1) < (*s2);
        }
    };

    /**
     * t_MapType is the map type to store various maps.
     * t_MapIterator is the iterator
     */

    typedef std::map< const w_KeyType *, t_MapEntry, t_EntryCompare >   t_MapType;
    typedef typename t_MapType::iterator                        t_MapIterator;


    // ======== t_FactoryRegistryEntry ================================
    /**
     * This implements the FactoryRegistryEntry entry.
     *
     */

    class t_FactoryRegistryEntry
      : public FactoryRegistryEntry
    {
        public:
        
        // described in FactoryRegistryEntry
        //
        virtual void RemoveAndDelete()
        {
            m_pmap->erase( m_location );
            delete this;
        }

        // t_FactoryRegistryEntry constructor
        //
        t_FactoryRegistryEntry(
            t_MapType                                   * i_pmap,
            t_MapIterator                                 i_location
        )
          : m_pmap( i_pmap ),
            m_location( i_location )
        {
        }

        virtual ~t_FactoryRegistryEntry() {}

        /**
         * m_pmap is a pointer to the map that this entry is contained.
         */
        
        t_MapType                                   * m_pmap;

        /**
         * m_location is the iterator that points to the entry
         * within the map.
         */
        
        t_MapIterator                                 m_location;

    };

    // ======== t_RegistrySingleton ===================================
    /**
     * This manages the factory registry object.  In particular
     * it deletes the registry upon system exit.  Even though this
     * is not critical, it does reduce the noise when checking
     * for memory leaks.
     */

    class t_RegistrySingleton
    {
        public:

        // ======== Get ===============================================
        /**
         *  This does a construct-on-reference
         *
         * @return a reference to the registry.
         */     

        FactoryRegister & Get()
        {
            PtrView<FactoryRegister *>       l_register = m_register;
    
            if ( ! l_register )
            {
                PtrView<FactoryRegisterRegistry *> l_frr = FactoryRegisterRegistry::Get();

                Ptr<FactoryRegisterBase *> l_val = l_frr->FindOrCreate(
                    DKy( typeid( FactoryRegister ).name() ),
                    & FactoryRegister::Create
                );

                // nasty down cast - right here !!!
                // Since we know that the pointer is being delegated, we must
                // make sure that the reference count is not increased.  In this
                // case, the reference count of the pointer is not incremented at
                // all.
                m_register = PtrDelegate<FactoryRegister *>(
                    static_cast<FactoryRegister *>( l_val.Transfer() ),
                    false
                );
                
                l_register = m_register;
                
            }

            return * l_register;

        } // end Get

        Ptr<FactoryRegister * >             m_register;

    };
    

    // ======== Get ===================================================
    /**
     * This method will return a singleton FactoryRegister for this type
     * set of template parameters.
     *
     * The library that contains the instantiation of this method is the one
     * that contains the registry.  To make sure that all factories use a single
     * registry, it's important to be selective of where this method
     * is instantiated. The AT_Factory_Home macro is intended to create
     * a way to isolate which compilation units contain the definition
     * and which do not.
     *
     */
    
    static FactoryRegister & Get();
    
    // ======== Find ==================================================
    /**
     * Find will find a factory entry in the registry.
     */

    w_CreatorType * Find( const w_KeyType & i_key )
    {
        t_MapIterator   l_location = m_map.find( & i_key );

        if ( l_location == m_map.end() )
        {
            return 0;
        }

        return ( * l_location ).second.m_factory;
    }


    // ======== RegisterInsert ========================================
    /**
     * RegisterInsert inserts a factory into the registry.
     *
     * @param i_factory is a pointer to the factory to be inserted
     * @return nothing
     */

    void RegisterInsert( w_CreatorType * i_factory )
    {

        //
        // attempt to insert the factory into the map
        //
        
        std::pair<t_MapIterator, bool> l_insert_result =
            m_map.insert( typename t_MapType::value_type( & i_factory->GetKey(), t_MapEntry( i_factory ) ) );

        //
        // If the map already has an entry of this type - return, lack of a
        // m_registry_entry indicates failure.
        //
        if ( ! l_insert_result.second )
        {
            return;
        }

        //
        // create the registry entry - this allows correct clean-up
        i_factory->m_registry_entry = new t_FactoryRegistryEntry(
            & m_map,
            l_insert_result.first
        );
    }


    // ======== Create ==============================================
    /**
     *  Create a w_CreatorType::CreateWrapper object.  This allows
     *  passing all the constructor parameters to this object.
     *
     * @param i_key The factory key for selecting the correct factory
     * @param o_srep Where to write the status report (in case of failure)
     * @param i_option How to handle a failure, See at::FactoryTraits::FactoryCreateReportingOption
     * @return w_CreatorType::CreateWrapper object.
     */

    static typename w_CreatorType::CreateWrapper Create(
        const w_KeyType                              & i_key,
        StatusReport                                 * o_srep = 0,
        FactoryTraits::FactoryCreateReportingOption    i_option = FactoryTraits::AlwaysReturn
    )
    {
        return typename w_CreatorType::CreateWrapper(
            Get().Find( i_key ),
            o_srep,
            i_option
        );
    } // end Initiate

    
    /**
     * m_map contains the map of factories.
     */
    
    t_MapType                                         m_map;

};

// =====================================================================
//
//

#ifndef AT_Factory_Home
#define AT_Factory_Home 1
#endif

#if AT_Factory_Home

// ======== FactoryRegister<w_InterfaceType, w_KeyType, w_CreatorType>::Get() ==========
/**
 * Implements static FactoryRegister & Get();
 *
 * The library that contains this method is the one that contains
 * the registry.  To make sure that all factories use a single
 * registry, it's important to be selective of where this method
 * is linked.
 */

template<
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_CreatorType
>
FactoryRegister<w_InterfaceType, w_KeyType, w_CreatorType>
    & FactoryRegister<w_InterfaceType, w_KeyType, w_CreatorType>::Get()
{
    static t_RegistrySingleton        s_reg;

    return s_reg.Get();

} // end FactoryRegister<w_InterfaceType, w_KeyType, w_CreatorType>::Get()

#endif

//
//
// =====================================================================

// ======== FactoryImpl0P ===========================================
/**
 * Implementations for creating objects using constructors with 0
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType = DKy
>
class FactoryImpl0P
  : public CreatorImpl0P< w_ImplementorType, w_InterfaceType, w_KeyType >
{
    public:

    typedef typename FactoryImpl0P::t_CreatorType        t_RegistryCreatorType;

    FactoryImpl0P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : CreatorImpl0P< w_ImplementorType, w_InterfaceType, w_KeyType >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
        //
        // Insert this factory into the factory register.
        //
        FactoryRegister< w_InterfaceType, w_KeyType, t_RegistryCreatorType >::Get().RegisterInsert(
            this
        );
    }
    
    virtual ~FactoryImpl0P()
    {
    }
};

// ======== FactoryImpl1P ===========================================
/**
 * Implementations for creating objects using constructors with 1
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1
>
class FactoryImpl1P
  : public CreatorImpl1P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1 >
{
    public:

    typedef typename FactoryImpl1P::t_CreatorType        t_RegistryCreatorType;

    FactoryImpl1P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : CreatorImpl1P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
        //
        // Insert this factory into the factory register.
        //
        FactoryRegister< w_InterfaceType, w_KeyType, t_RegistryCreatorType >::Get().RegisterInsert(
            this
        );
    }
    
    virtual ~FactoryImpl1P()
    {
    }
};

// ======== FactoryImpl2P ===========================================
/**
 * Implementations for creating objects using constructors with 2
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2
>
class FactoryImpl2P
  : public CreatorImpl2P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1, w_arg_2 >
{
    public:

    typedef typename FactoryImpl2P::t_CreatorType        t_RegistryCreatorType;

    FactoryImpl2P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : CreatorImpl2P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1, w_arg_2 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
        //
        // Insert this factory into the factory register.
        //
        FactoryRegister< w_InterfaceType, w_KeyType, t_RegistryCreatorType >::Get().RegisterInsert(
            this
        );
    }

    virtual ~FactoryImpl2P()
    {
    }

};

// ======== FactoryImpl3P ===========================================
/**
 * Implementations for creating objects using constructors with 3
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2,
    typename    w_arg_3
>
class FactoryImpl3P
  : public CreatorImpl3P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3 >
{
    public:

    typedef typename FactoryImpl3P::t_CreatorType        t_RegistryCreatorType;

    FactoryImpl3P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : CreatorImpl3P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
        //
        // Insert this factory into the factory register.
        //
        FactoryRegister< w_InterfaceType, w_KeyType, t_RegistryCreatorType >::Get().RegisterInsert(
            this
        );
    }

    virtual ~FactoryImpl3P()
    {
    }

};

// ======== FactoryImpl4P ===========================================
/**
 * Implementations for creating objects using constructors with 3
 * parameters.
 */

template<
    typename    w_ImplementorType,
    typename    w_InterfaceType,
    typename    w_KeyType,
    typename    w_arg_1,
    typename    w_arg_2,
    typename    w_arg_3,
    typename    w_arg_4
>
class FactoryImpl4P
  : public CreatorImpl4P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3, w_arg_4 >
{
    public:

    typedef typename FactoryImpl4P::t_CreatorType        t_RegistryCreatorType;

    FactoryImpl4P(
        w_KeyType                                     i_key,
        const char                                  * i_filename,
        int                                           i_lineno,
        const char                                  * i_description
    )
      : CreatorImpl4P< w_ImplementorType, w_InterfaceType, w_KeyType, w_arg_1, w_arg_2, w_arg_3, w_arg_4 >(
            i_key,
            i_filename,
            i_lineno,
            i_description
        )
    {
        //
        // Insert this factory into the factory register.
        //
        FactoryRegister< w_InterfaceType, w_KeyType, t_RegistryCreatorType >::Get().RegisterInsert(
            this
        );
    }

    virtual ~FactoryImpl4P()
    {
    }

};


// ======== AT_FactorySymbolRef?P =====================================
/**
 * AT_FactorySymbolRef?P macros create a name for symbols to be exported
 * so that linking of factories can be forced into certain executables.
 */

#define AT_FactorySymbolRef(x_Name,x_NumParam) x_g_f ## x_Name ## _ ## x_NumParam ## P_ref

// ======== AT_MakeFactory?P ==========================================
/**
 * The AT_MakeFactory?P macros create a factory for a given number
 * of constructor paramaters.
 */

#define AT_MakeFactory0P(x_Key, x_Impl, x_Interface, x_KeyType)             \
    at::FactoryImpl0P<x_Impl, x_Interface, x_KeyType>                        \
        x_g_f ## x_Impl ## _0P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key ); \
    extern "C" { void * AT_FactorySymbolRef(x_Impl,0) = static_cast<void *>( & x_g_f ## x_Impl ## _0P ); };

#define AT_MakeFactory1P(x_Key, x_Impl, x_Interface, x_KeyType, x_arg1)     \
    at::FactoryImpl1P<x_Impl, x_Interface, x_KeyType, x_arg1 >                \
        x_g_f ## x_Impl ## _1P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key ); \
    extern "C" { void * AT_FactorySymbolRef(x_Impl,1) = static_cast<void *>( & x_g_f ## x_Impl ## _1P ); };

#define AT_MakeFactory2P(x_Key, x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2)     \
    at::FactoryImpl2P<x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2 >                \
        x_g_f ## x_Impl ## _2P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key ); \
    extern "C" { void * AT_FactorySymbolRef(x_Impl,2) = static_cast<void *>( & x_g_f ## x_Impl ## _2P ); };

#define AT_MakeFactory3P(x_Key, x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3)     \
    at::FactoryImpl3P<x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3 >                \
        x_g_f ## x_Impl ## _3P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key ); \
    extern "C" { void * AT_FactorySymbolRef(x_Impl,3) = static_cast<void *>( & x_g_f ## x_Impl ## _3P ); };

#define AT_MakeFactory4P(x_Key, x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3, x_arg4)     \
    at::FactoryImpl4P<x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3, x_arg4 >                   \
        x_g_f ## x_Impl ## _4P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key );\
    extern "C" { void * AT_FactorySymbolRef(x_Impl,4) = static_cast<void *>( & x_g_f ## x_Impl ## _4P ); };

// ======== AT_MakeNamedFactory?P =============================
/**
 * The AT_MakeNamedFactory?P macros create a factory for a given number
 * of constructor paramaters - but the name is selectable so that an
 * implementation may be registered in a number of factories.
 */

#define AT_MakeNamedFactory0P(x_Name, x_Key, x_Impl, x_Interface, x_KeyType)                \
    at::FactoryImpl0P<x_Impl, x_Interface, x_KeyType>                        \
        x_g_f ## x_Name ## _0P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key " " # x_Name ); \
    extern "C" { void * AT_FactorySymbolRef(x_Name,0) = static_cast<void *>( & x_g_f ## x_Name ## _0P ); };

#define AT_MakeNamedFactory1P(x_Name, x_Key, x_Impl, x_Interface, x_KeyType, x_arg1)        \
    at::FactoryImpl1P<x_Impl, x_Interface, x_KeyType, x_arg1 >                \
        x_g_f ## x_Name ## _1P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key " " # x_Name ); \
    extern "C" { void * AT_FactorySymbolRef(x_Name,1) = static_cast<void *>( & x_g_f ## x_Name ## _1P ); };

#define AT_MakeNamedFactory2P(x_Name, x_Key, x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2) \
    at::FactoryImpl2P<x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2 >                \
        x_g_f ## x_Name ## _2P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key " " # x_Name ); \
    extern "C" { void * AT_FactorySymbolRef(x_Name,2) = static_cast<void *>( & x_g_f ## x_Name ## _2P ); };

#define AT_MakeNamedFactory3P(x_Name, x_Key, x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3) \
    at::FactoryImpl3P<x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3 >                \
        x_g_f ## x_Name ## _3P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key " " # x_Name ); \
    extern "C" { void * AT_FactorySymbolRef(x_Name,3) = static_cast<void *>( & x_g_f ## x_Name ## _3P ); };

#define AT_MakeNamedFactory4P(x_Name, x_Key, x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3, x_arg4) \
    at::FactoryImpl4P<x_Impl, x_Interface, x_KeyType, x_arg1, x_arg2, x_arg3, x_arg4 >                \
        x_g_f ## x_Name ## _4P( x_Key, __FILE__, __LINE__, # x_Interface " " # x_Impl " " # x_Key " " # x_Name ); \
    extern "C" { void * AT_FactorySymbolRef(x_Name,4) = static_cast<void *>( & x_g_f ## x_Name ## _4P ); };


// ======== AT_MakeCUnitLink ==================================
/**
 * AT_MakeCUnitLink is used to define a symbol for linking a compile
 * unit.  Since factories are loosly linked, it may be impossible
 * to force a linker to link them into an executable. Using these
 * macros, a compile unit may be forced to link in.
 */
        
#define AT_MakeCUnitLink_Name(name)     x_AT_CUnit_##name
#define AT_MakeCUnitLink(name)          int AT_MakeCUnitLink_Name(name)[1];
#define AT_MakeCUnitLinkExtern(name)    extern int AT_MakeCUnitLink_Name(name)[];


// ======== AT_HardLinkExtern =========================================
/**
 * AT_HardLinkExtern is a macro that provides an extern for the symbol
 * that is generated when AT_Make*Factory* macros are used.
 */

#define AT_HardLinkExtern(x_Name,x_NumParam)    \
    extern "C" { extern void * AT_FactorySymbolRef(x_Name,x_NumParam); };


// ======== AT_HardLinkArrayElement ===================================
/**
 * AT_HardLinkArrayElement creates an entry to an list suitable for
 * initializing an array
 */

#define AT_HardLinkArrayElement(x_Name,x_NumParam)  \
    & AT_FactorySymbolRef(x_Name,x_NumParam),


/** @} */ // end of GenericFactories ( Doxygen group )

}; // namespace
#endif // x_at_factory_h_x



