#ifndef x_at_http_misc_h_x
#define x_at_http_misc_h_x 1


#include <string>
#include <vector>
#include <utility>
#include <locale>
#include "at_types.h"
#include "at_range_set.h"


namespace at
{


    // ======== VectorPairAccessor ============================================
    /**
     * VectorPairAccessor provides base functionality for managing a container
     * that has 2 values per element.  Primary use is to specialize for the
     * ParseHttpRange function.
     */
    template <
        typename w_container
    >
    class VectorPairAccessor;


    // ======== VectorPairAccessor ============================================
    /**
     * VectorPairAccessor specialization for
     * std::vector< std::pair< w_type, w_type > >.
     *
     */
    template <
        typename w_type
    >
    class VectorPairAccessor< std::vector< std::pair< w_type, w_type > > >
    {
        public:

        /**
         * t_Container is the container type of this VectorPairAccessor.
         */
        typedef  std::vector< std::pair< w_type, w_type > >  t_Container;

        /**
         * t_ElemType is the base element type
         */
        typedef  w_type  t_ElemType;

        // ======== Add ===================================================
        /**
         * Add will add a new element at the end of the container.  Will throw
         * if a new element could not be allocated.
         *
         * @return Nothing.
         */
        static inline void Add(
            t_Container & o_container,
            const w_type & i_first,
            const w_type & i_second
        )
        {
            o_container.push_back(
                typename t_Container::value_type( i_first, i_second )
            );
        }

    };


    template<
        typename w_element_type
    >
    bool accumulate(
        w_element_type & i_val,
        const w_element_type & i_file_size,
        char i_digit
    )
    {
        bool retval = true;

        int digit = 0;
        switch (i_digit) {
            case '0': digit = 0; break;
            case '1': digit = 1; break;
            case '2': digit = 2; break;
            case '3': digit = 3; break;
            case '4': digit = 4; break;
            case '5': digit = 5; break;
            case '6': digit = 6; break;
            case '7': digit = 7; break;
            case '8': digit = 8; break;
            case '9': digit = 9; break;
            default:
                retval = false;
                break;
        }
        if (retval)
        {
            if ( i_val < i_file_size )
            {
                w_element_type maxPreVal = i_file_size;
                maxPreVal -= digit;
                maxPreVal /= 10;
                if ( maxPreVal >= i_val)
                {
                    i_val = i_val * 10 + digit;
                }
                else
                {
                    i_val = i_file_size;
                }
            }
        }
        return retval;
    }

    template<
        typename w_element_type
    >
    bool adjustRange(
        const w_element_type & i_file_size,
        w_element_type & i_first,
        w_element_type & i_second
    )
    {
        bool l_retval = true;
        if ( i_first >= i_file_size )
        {
            l_retval = false;
        }
        else if ( i_first > i_second )
        {
            l_retval = false;
        }
        else if ( i_second >= i_file_size )
        {
            i_second = i_file_size - 1;
        }
        return l_retval;
    }


    /*
	   parse a vector of range specifiers from input string --- http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.35.1

           ranges-specifier = byte-ranges-specifier
           byte-ranges-specifier = bytes-unit "=" byte-range-set
           byte-range-set  = 1#( byte-range-spec | suffix-byte-range-spec )
           byte-range-spec = first-byte-pos "-" [last-byte-pos]
           first-byte-pos  = 1*DIGIT
           last-byte-pos   = 1*DIGIT

           suffix-byte-range-spec = "-" suffix-length
           suffix-length = 1*DIGIT
    */

    template<
        typename w_container
    >
    bool ParseHttpRange(
        const std::string & i_input_header,
        const typename at::VectorPairAccessor< w_container >::t_ElemType & i_file_size,
        w_container & i_return_ranges
    )
    {
        typedef  typename w_container::value_type  t_range_pair;
        typedef  at::VectorPairAccessor< w_container >  t_accessor;
        typedef  typename t_accessor::t_ElemType  t_element_type;

        std::locale loc = std::locale();
        enum { suffix_or_range, range_begin, range_hyphen, range_end, suffix_range, error_found };
        int state = suffix_or_range;

        t_element_type l_first( 0 );
        t_element_type l_second( 0 );

		std::string::size_type l_pos = i_input_header.find_first_not_of( "bytes=" );
		if ( l_pos > i_input_header.size() )
		{
			l_pos = 0;
		}

        std::string::const_iterator it =
            i_input_header.begin() + l_pos;

        while ( it != i_input_header.end() )
        {
            switch(state)
            {
            case suffix_or_range:
                if (isdigit(*it,loc))
                {
                    if ( accumulate( l_first, i_file_size, *it ) )
                    {
                        state = range_begin;
                    }
                    else
                    {
                        state = error_found;
                    }
                }
                else if ('-' == *it)
                {
                    l_second = i_file_size - 1;
                    state = suffix_range;
                }
                else
                {
                    state = error_found;
                }
                break;
            case range_begin:
                if (isdigit(*it,loc))
                {
                    if ( ! accumulate( l_first, i_file_size, *it ) )
                    {
                        state = error_found;
                    }
                }
                else if ('-' == *it)
                {
                    state = range_hyphen;
                }
                else
                {
                    state = error_found;
                }
                break;
            case range_hyphen:
                if (isdigit(*it,loc))
                {
                    if ( accumulate( l_second, i_file_size, *it ) )
                    {
                        state = range_end;
                    }
                    else
                    {
                        state = error_found;
                    }
                }
                else if (',' == *it)
                {
                    state = suffix_or_range;
                    l_second = i_file_size - 1;
                    if ( adjustRange( i_file_size, l_first, l_second ) )
                    {
                        t_accessor::Add( i_return_ranges, l_first, l_second );
                    }
                    else
                    {
                        state = error_found;
                    }
                    l_first = 0;
                    l_second = 0;
                }
                else
                {
                    state = error_found;
                }
                break;

            case range_end:
                if (isdigit(*it,loc))
                {
                    if ( ! accumulate( l_second, i_file_size, *it ) )
                    {
                        state = error_found;
                    }
                }
                else if (',' == *it)
                {
                    state = suffix_or_range;
                    if ( adjustRange( i_file_size, l_first, l_second ) )
                    {
                        t_accessor::Add( i_return_ranges, l_first, l_second );
                    }
                    else
                    {
                        state = error_found;
                    }
                    l_first = 0;
                    l_second = 0;
                }
                else
                {
                    state = error_found;
                }
                break;
            case suffix_range:
                if (isdigit(*it,loc))
                {
                    if ( ! accumulate( l_first, i_file_size, *it ) )
                    {
                        state = error_found;
                    }
                }
                else if (',' == *it)
                {
                    state = suffix_or_range;
                    if ( l_first > i_file_size )
                    {
                        state = error_found;
                    }
                    else
                    {
                        l_first = i_file_size - l_first;
                        if ( adjustRange( i_file_size, l_first, l_second ) )
                        {
                            t_accessor::Add( i_return_ranges, l_first, l_second );
                        }
                        else
                        {
                            state = error_found;
                        }
                    }
                    l_first = 0;
                    l_second = 0;
                }
                else
                {
                    state = error_found;
                }
                break;
            case error_found:
            default:
                break;
            }
            ++it ;
        }
        switch(state)
        {
        case suffix_or_range:
            state = error_found;
            break;
        case range_begin:
            state = error_found;
            break;
        case range_hyphen:
            l_second = i_file_size - 1;
            if ( adjustRange( i_file_size, l_first, l_second ) )
            {
                t_accessor::Add( i_return_ranges, l_first, l_second );
            }
            else
            {
                state = error_found;
            }
            break;
        case range_end:
            if ( adjustRange( i_file_size, l_first, l_second ) )
            {
                t_accessor::Add( i_return_ranges, l_first, l_second );
            }
            else
            {
                state = error_found;
            }
            break;
        case suffix_range:
            if ( l_first > i_file_size )
            {
                state = error_found;
            }
            else
            {
                l_first = i_file_size - l_first;
                if ( adjustRange( i_file_size, l_first, l_second ) )
                {
                    t_accessor::Add( i_return_ranges, l_first, l_second );
                }
                else
                {
                    state = error_found;
                }
            }
            break;
        case error_found:
        default:
            break;
        }

        return (state != error_found);
    }


}


#endif
