//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_gray_code.h
 *
 */


#ifndef x_at_gray_code_h_x
#define x_at_gray_code_h_x 1

#include <limits>

// Austria namespace
namespace at
{
    
/**
 * The gray_tools namespace exposes some tools for converting to and from
 * the common gray code.
 *
 */

namespace gray_tools
{

/**
 * signed_tester is used to provide a comparison function for
 * "T >= 0" for signed or unsigned types.  This is used to
 * avoid some silly compiler warnings.  There is no reason
 * other than this.
 */

template <typename T, bool is_signed=std::numeric_limits<T>::is_signed >
struct signed_tester
{
    /**
     * gtez compares the parameter for "greater than or equal to 0".
     * This version of this method is ONLY for signed types of T.
     *
     * @param i_rhs The rhs of " >= 0"
     * @return the result of the expression
     */
    inline static bool gtez( const T & i_rhs )
    {
        return i_rhs >= 0;
    }
};

/**
 * signed_tester<T, false> "unsigned" specialization
 */

template <typename T>
struct signed_tester<T, false>
{
    /**
     * gtez compares the parameter for "greater than or equal to 0".
     * This version of this method is ONLY for unsigned types which means
     * the result of the expression is allways true (hence the compiler
     * warning).
     *
     * @param i_rhs Ignored
     * @return true
     */
    inline static bool gtez( const T & i_rhs )
    {
        return true;
    }
};

/**
 * bin_to_gray converts any integer POD type from a binary (2s complement for
 * signed numbers) to the run-of-the mill gray code commonly found.
 *
 */

template <typename T>
T bin_to_gray( const T & i_value )
{
    
    if ( signed_tester<T>::gtez( i_value ) )
    {
        return i_value ^ ( i_value >> 1 );
    }
    else
    {
//        return ( ( ~i_value ) ^ ( ( ~i_value ) >> 1 ) ) + std::numeric_limits<T>::min();
        return ( ( ~i_value ) ^ ( ( ~i_value ) >> 1 ) ); // + std::numeric_limits<T>::min();
    }
}


/**
 * gray_to_bin_helper contains a static helper method to
 * generate binary code from gray code. This is a recursive template
 * that will terminate when the specialization below is used.
 */

template <
    typename T,
    int shift_count = 1,
    bool terminal_calc = ( shift_count >= ( std::numeric_limits<T>::digits ) )
>
struct gray_to_bin_helper
{
    /**
     * calculate will calculate the inverse of the gray code
     * conversion above.  The value is passed by reference and is
     * modified in place.
     *
     * In general the computation is :
     *
     *      value ^= value >> 1;
     *      value ^= value >> 2;
     *      value ^= value >> 4; // terminate here for 8 bit
     *      value ^= value >> 8; // terminate here for 16 bit
     *      value ^= value >> 16; // terminate here for 32 bit
     *      value ^= value >> 32; // terminate here for 64 bit
     *
     * @param io_value the value being computed
     *
     */
    
    inline static void calculate( T & io_value )
    {
        io_value ^= io_value >> shift_count;
        gray_to_bin_helper<T,shift_count*2>::calculate( io_value );
    }
};

/**
 * gray_to_bin_helper specialization, contains the terminal recursive
 * function.
 */

template <
    typename T,
    int shift_count
>
struct gray_to_bin_helper<T,shift_count,true>
{
    /**
     * This is a "do nothing" specialization of calculate above.
     */
    inline static void calculate( T & value )
    {
    }
};
    
/**
 * gray_to_bin computes the gray binary code given the gray code value.
 * Even though this function exists, it is far more appropriate to use
 * class gray_code below as this will render code more readable and far
 * less prone to error.
 *
 * @param value contains the value to be converted.
 *
 */
    
template <typename T>
T gray_to_bin( T value )
{
    if ( signed_tester<T>::gtez( value ) )
    {
        gray_to_bin_helper<T>::calculate( value );
    }
    else
    {
        //value = value - std::numeric_limits<T>::min();
        gray_to_bin_helper<T>::calculate( value );
        value = ~ value;
    }
    
    return value;
}

};

/**
 * The class gray_code encapsulates the "special" integer type that
 * represents a "gray_code".  Because of the potential for confusion
 * between regular data types and gray_code encoded values it is more
 * appropriate to encapsulate gray code encoded values in your code
 * than to expose them as POD integers.  This class performs this
 * encapsulation and provides some convenience functions.
 *
 */


template <typename T>
class gray_code
{
    public:
    typedef T           value_type;

    protected:
    value_type          m_value;

    public:
    template <typename T2> friend class gray_code;

    /**
     * conversion operator for a gray_code to a regular binary number.
     */
    operator value_type () const throw()
    {
        return getbin();
    }

    value_type getbin() const throw()
    {
        return gray_tools::gray_to_bin( m_value );
    }

    value_type getgray() const throw()
    {
        return m_value;
    }

    void setgray( value_type value ) throw()
    {
        m_value = value;
    }

    /**
     * Allow construction of a gray code without any conversion.
     *
     */

    enum ArgType
    {
        /**
         * pass is_gray as the second parameter if the value is a gray code.
         */
        is_gray,
        
        /**
         * pass is_binary as the second parameter if the value is a binary code.
         * Omitting this parameter will use the default type T constructor that
         * is the same as is_binary.
         */
        is_binary
    };

    /**
     * gray_code constructor from a either a binary value or a binary form of a gray code.
     *
     * To construct a gray_code object with a gray_code value, then do the following:
     *<pre>
     *      gray_code( 0x234235, gray_code::is_gray )
     *
     * To construct a gray_code object with a binary value then:
     *
     *      gray_code( 555, gray_code::is_binary )
     * or
     *      gray_code( 555 ) // implicit conversions allowed.
     *
     *</pre>
     *
     * @param i_value a value of type T that is used to initialize this code
     * @param i_arg_type determine the representation of the value being passed in
     *              as either gray_code::is_gray or gray_code::is_binary
     */

    gray_code( const T & i_value, ArgType i_arg_type )
      : m_value( i_arg_type == is_gray ? i_value : gray_tools::bin_to_gray( i_value ) )
    {
    }

    /**
     * gray_code constructor from a regular binary value.
     *
     * @param i_value a value of type T that is interpretted as a binary value.
     * 
     */

    gray_code( const T & i_value )
      : m_value( gray_tools::bin_to_gray( i_value ) )
    {
    }

    /**
     * Construct a gray_code from another gray code of different type.
     *
     * @param i_other_gray the gray_code being copied.
     */
    
    template <typename RHS_T>
    gray_code( const gray_code<RHS_T> & i_other_gray )
      : m_value(
            std::numeric_limits<value_type>::is_signed == std::numeric_limits<RHS_T>::is_signed
            ? i_other_gray.m_value
            : gray_tools::bin_to_gray( value_type( i_other_gray.getbin() ) )
        )
    {
    }

    /**
     * Assign a gray_code from a gray code of a different type.
     *
     * @param i_other_gray the gray_code being read.
     */
    
    template <typename RHS_T>
    gray_code & operator= (const RHS_T & i_other_gray )
    {

        m_value = (
            std::numeric_limits<value_type>::is_signed == std::numeric_limits<RHS_T>::is_signed
            ? i_other_gray.m_value
            : gray_tools::bin_to_gray( value_type( i_other_gray.getbin() ) )
        );
        
        return * this;
    }

    /**
     * Since there is not much meaning in compareing gray codes, other than special
     * things like being equal to zero or sign, this is an efficient method for doing this.
     * This method compares for "greater than or equal to zero".  This is allways true for
     * unsigned values.
     */

    bool gtez()
    {
        return gray_tools::signed_tester<T>::gtez( m_value );
    }
    
    /**
     * Since there is not much meaning in compareing gray codes, other than special
     * things like being equal to zero or sign, this is an efficient method for doing this.
     * This method compares for equal to zero.
     */

    bool equal_to_zero()
    {
        return m_value == 0;
    }
};
    
}; // namespace
#endif // x_at_gray_code_h_x

