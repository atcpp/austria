//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * \file at_bw_transform.h
 *
 * \author Gianni Mariani
 *
 * at_bw_transform.h provides an interface to a Burrows Wheeler transform.
 * This transform is used to transform text into somthing that is easier to
 * compress.  However, this can also be used to create an isomorphic "image"
 * of data.
 *
 * Ref: "A Block-sorting Lossless Data Compression Algorithm" by M. Burrows and D.J. Wheeler
 *      May 10, 1994
 */

#ifndef x_at_bw_transform_h_x
#define x_at_bw_transform_h_x

#include <vector>
#include <algorithm>

#include "at_assert.h"

// Austria namespace
namespace at
{

/**
 * BWComparator is a special contextual comparator for a
 * BW transform.
 */

template <
    typename    w_str_iterator
>
class BWComparator
{
    w_str_iterator                m_str_begin;
    w_str_iterator                m_str_end;
        
    public:
    BWComparator( 
        w_str_iterator          & i_str_begin,  
        w_str_iterator          & i_str_end
    )
      : m_str_begin( i_str_begin ),
        m_str_end( i_str_end )
    {
    }

    inline bool operator()(
        w_str_iterator            i_lhs,    
        w_str_iterator            i_rhs
    ) {
        if ( i_lhs == i_rhs )
        {
            return false;
        }

        w_str_iterator            l_ilhs = i_lhs;
        w_str_iterator            l_irhs = i_rhs;

        w_str_iterator            l_slhs = m_str_end;
        w_str_iterator            l_srhs = m_str_end;

        while ( 1 )
        {

            if ( * l_ilhs < * l_irhs )
            {
                return true;
            }
            
            if ( * l_irhs < * l_ilhs )
            {
                return false;
            }

            ++ l_ilhs;

            if ( l_ilhs == l_slhs )
            {
                if ( l_slhs == m_str_end )
                {
                    l_slhs = i_lhs;
                    l_ilhs = m_str_begin;
                }
                else
                {
                    return false;
                }
            }
            
            ++ l_irhs;

            if ( l_irhs == l_srhs )
            {
                if ( l_srhs == m_str_end )
                {
                    l_srhs = i_rhs;
                    l_irhs = m_str_begin;
                }
                else
                {
                    return false;
                }
            }
            
        }
    }
};

/**
 * BWTEncode performs a conversion from regular text space to BW space.
 *
 * @param w_str_iterator iterator type for input string
 * @param w_out_iterator iterator type for output string
 * @param w_ptr_diff_type iterator difference type
 * @param i_str_begin the beginning of the string to decode
 * @param i_str_end the end of the string to decode (1 beyond)
 * @param i_out_begin the beginning of where to write the output
 * @param i_count the number of elements in the input and output strings
 */

template <
    typename    w_str_iterator,
    typename    w_out_iterator,
    typename    w_ptr_diff_type
>
unsigned BWTEncode(
    w_str_iterator            i_str_begin,  
    w_str_iterator            i_str_end,
    w_out_iterator            i_out_begin,
    w_ptr_diff_type           i_count
)
{

    // create a vector to store for all the iterators to all the elements.
    // except for the last one which we will treat in a special way.

    std::vector<w_str_iterator>     l_strs( i_count );

    w_str_iterator  l_str_iter = i_str_begin;
    typename std::vector<w_str_iterator>::iterator p_strs = l_strs.begin();
    
    for ( w_ptr_diff_type l_counter = i_count; l_counter; -- l_counter ) {
        * ( p_strs ++ ) = l_str_iter ++;
    }

    BWComparator< w_str_iterator >  l_comparator( i_str_begin, i_str_end );

    std::sort( l_strs.begin(), l_strs.end(), l_comparator );

    w_out_iterator            l_out = i_out_begin;
    
    l_str_iter = i_str_begin;
    p_strs = l_strs.begin();

    unsigned row_sel = ~0U;
    
    for ( w_ptr_diff_type l_counter = i_count; l_counter; -- l_counter ) {
        w_str_iterator      l_iter = * ( p_strs ++ );
        
        if ( l_iter == i_str_begin )
        {
            AT_Assert( (row_sel == ~0U) );
            row_sel = p_strs - l_strs.begin() - 1;
            l_iter = i_str_end;
        }
        -- l_iter;
        
        * ( l_out ++ ) = * l_iter;
    }

    return row_sel;
}


/**
 * BWTEncode performs a conversion from regular text space to BW space.
 * This interface is an overload of BWTEncode for iterators that support
 * operator-.
 *
 * @param w_str_iterator iterator type for input string
 * @param w_out_iterator iterator type for output string
 * @param i_str_begin the beginning of the string to decode
 * @param i_str_end the end of the string to decode (1 beyond)
 * @param i_out_begin the beginning of where to write the output
 */

template <
    typename    w_str_iterator,
    typename    w_out_iterator
>   
unsigned BWTEncode(
    w_str_iterator            i_str_begin,  
    w_str_iterator            i_str_end,
    w_out_iterator            i_out_begin
)
{
    return BWTEncode( i_str_begin, i_str_end, i_out_begin, i_str_end - i_str_begin );
}


/**
 * BWDecComparator is a special contextual comparator for a
 * BW transform.
 */

template <
    typename    w_str_iterator
>
class BWDecComparator
{
    w_str_iterator                m_str_begin;
    w_str_iterator                m_str_end;

    public:
    BWDecComparator( 
        w_str_iterator          & i_str_begin,  
        w_str_iterator          & i_str_end
    )
      : m_str_begin( i_str_begin ),
        m_str_end( i_str_end )
    {
    }

    inline bool operator()(
        w_str_iterator            i_lhs,    
        w_str_iterator            i_rhs
    ) {

        w_str_iterator            l_ilhs = i_lhs;
        w_str_iterator            l_irhs = i_rhs;

        w_str_iterator            l_slhs = m_str_end;
        w_str_iterator            l_srhs = m_str_end;

        if ( * l_ilhs < * l_irhs )
        {
            return true;
        }
        
        if ( * l_irhs < * l_ilhs )
        {
            return false;
        }

        return ( l_ilhs <  l_irhs );

    }
};


/**
 * BWTDecode performs a conversion from BW space to regular text.
 * This implementation depends on iterators that can be differenced.
 *
 * @param w_str_iterator iterator type for input string
 * @param w_out_iterator iterator type for output string
 * @param w_ptr_diff_type iterator difference type
 * @param i_I the value returned from BWTEncode
 * @param i_str_begin the beginning of the string to decode
 * @param i_str_end the end of the string to decode (1 beyond)
 * @param i_out_begin the beginning of where to write the output
 * @param i_count the number of elements in the input and output strings
 */

template <
    typename    w_str_iterator,
    typename    w_out_iterator,
    typename    w_ptr_diff_type
>   
void BWTDecode(
    unsigned                  i_I,
    w_str_iterator            i_str_begin,  
    w_str_iterator            i_str_end,
    w_out_iterator            i_out_begin,
    w_ptr_diff_type           i_count
)
{

    // create a vector to store for all the iterators to all the elements.
    // except for the last one which we will treat in a special way.

    std::vector<w_str_iterator>     l_strs( i_count );

    w_str_iterator  l_str_iter = i_str_begin;
    typename std::vector<w_str_iterator>::iterator p_strs = l_strs.begin();
    
    for ( w_ptr_diff_type l_counter = i_count; l_counter; -- l_counter ) {
        * ( p_strs ++ ) = l_str_iter ++;
    }

    BWDecComparator< w_str_iterator >   l_comparator( i_str_begin, i_str_end );

    std::sort( l_strs.begin(), l_strs.end(), l_comparator );

    unsigned index = i_I;

    for ( w_ptr_diff_type i = i_count; i; -- i )
    {
        l_str_iter = l_strs[ index ];

        * ( i_out_begin ++ ) = * l_str_iter;
        
        index = l_str_iter - i_str_begin;

    }
    
}



/**
 * BWTDecode performs a conversion from regular BW space to regular text.
 * 
 * @param w_str_iterator iterator type for input string
 * @param w_out_iterator iterator type for output string
 * @param i_I the value returned from BWTEncode
 * @param i_str_begin the beginning of the string to decode
 * @param i_str_end the end of the string to decode (1 beyond)
 * @param i_out_begin the beginning of where to write the output
 */

template <
    typename    w_str_iterator,
    typename    w_out_iterator
>   
void BWTDecode(
    unsigned                  i_I,
    w_str_iterator            i_str_begin,  
    w_str_iterator            i_str_end,
    w_out_iterator            i_out_begin
)
{
    BWTDecode( i_I, i_str_begin, i_str_end, i_out_begin, ( i_str_end - i_str_begin ) );
}

}; // namespace

#endif // x_at_bw_transform_h_x

