/**
 *  \file at_log_rotating_files.h
 *
 *  \author Bradley Austin
 *
 */

#ifndef x_at_log_rotating_files_h_x
#define x_at_log_rotating_files_h_x 1


#include "at_log.h"
#include "at_crtp.h"
#include "at_types.h"

#include <string>
#include <sstream>
#include <algorithm>


namespace at
{

    class LogWriterOutputFileOpsException {};


    class LogWriterOutputFileNamePolicy_Default
    {

    public:

        static std::string ArchiveFileName(
            const std::string i_base_name,
            int i_archive_number
        )
        {
            std::ostringstream i_oss;
            i_oss << i_base_name << "." << i_archive_number;
            return i_oss.str();
        }

    protected:

        inline LogWriterOutputFileNamePolicy_Default() {}
        inline ~LogWriterOutputFileNamePolicy_Default() {}

    private:

        /* Unimplemented. */
        LogWriterOutputFileNamePolicy_Default
            ( const LogWriterOutputFileNamePolicy_Default & );
        LogWriterOutputFileNamePolicy_Default & operator=
            ( const LogWriterOutputFileNamePolicy_Default & );

    };


    class LogWriterOutputFileNamePolicy_InsertNumberBeforeExtension
    {

    public:

        static std::string ArchiveFileName(
            const std::string i_base_name,
            int i_archive_number
        )
        {
            std::ostringstream i_oss;
            std::string::const_iterator l_dot(
                std::find( i_base_name.begin(), i_base_name.end(), '.' )
            );
            i_oss <<
                std::string( i_base_name.begin(), l_dot ) <<
                "." << i_archive_number <<
                std::string( l_dot, i_base_name.end() );
            return i_oss.str();
        }

    protected:

        inline LogWriterOutputFileNamePolicy_InsertNumberBeforeExtension() {}
        inline ~LogWriterOutputFileNamePolicy_InsertNumberBeforeExtension() {}

    private:

        /* Unimplemented. */
        LogWriterOutputFileNamePolicy_InsertNumberBeforeExtension
            ( const LogWriterOutputFileNamePolicy_InsertNumberBeforeExtension & );
        LogWriterOutputFileNamePolicy_InsertNumberBeforeExtension & operator=
            ( const LogWriterOutputFileNamePolicy_InsertNumberBeforeExtension & );

    };


    template<
        class w_inner_output_policy,
        class w_file_name_policy,
        class w_file_ops_policy
    >
    class LogWriterOutputPolicy_RotatingFiles
      : public w_inner_output_policy,
        public w_file_name_policy,
        public w_file_ops_policy,
        public ThisInBaseInitializerHack<
            LogWriterOutputPolicy_RotatingFiles<
                w_inner_output_policy,
                w_file_name_policy,
                w_file_ops_policy
            >
        >
    {

    public:

        typedef
            typename w_inner_output_policy::t_ostream_type
                t_inner_ostream_type;

        typedef
            LogWriterOutputPolicy_Stream< LogWriterOutputPolicy_RotatingFiles >
                t_ostream_type;

    private:

        t_ostream_type m_ostream;
        std::ostringstream m_buffer;
        int m_file_count;
        bool m_opened;
        std::string m_base_file_name;
        bool m_inner_opened;

    protected:

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_RotatingFiles
        >::This;

        inline LogWriterOutputPolicy_RotatingFiles()
          : m_ostream( This() ),
            m_file_count( 0 ),
            m_opened( false ),
            m_inner_opened( false )
        {
        }

        inline ~LogWriterOutputPolicy_RotatingFiles() {}

    public:

        using w_file_name_policy::ArchiveFileName;

        using w_file_ops_policy::FileExists;
        using w_file_ops_policy::FileSize;
        using w_file_ops_policy::DeleteFile;
        using w_file_ops_policy::RenameFile;

        template < typename w_type >
        inline void Write( const w_type & i_value )
        {
            if ( ( ! m_opened ) || ( ! m_inner_opened ) || m_file_count == 0 )
            {
                throw LogWriterCantWriteException();
            }
            m_buffer << i_value;
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        void Flush()
        {
            std::string l_string = m_buffer.str();
            m_buffer.str( std::string() );

            if ( ( ! m_opened ) || ( ! m_inner_opened ) || m_file_count == 0 )
            {
                throw LogWriterCantWriteException();
            }

            t_inner_ostream_type & l_inner_ostream = InnerOStream();
            l_inner_ostream << l_string;
            w_inner_output_policy::Flush();
        }

        size_t StartingBytesWritten(
            const std::string & i_file_name,
            size_t i_page_size
        )
        {
            try
            {
                if ( ! FileExists( i_file_name ) )
                {
                    return 0;
                }

                Int64 l_result = FileSize( i_file_name );
                if ( l_result > Int64( i_page_size ) )
                {
                    return i_page_size;
                }
                return static_cast< size_t >( l_result );

            }
            catch ( LogWriterOutputFileOpsException )
            {
                return 0;
            }
        }

        void NewPage()
        {
            if ( ( ! m_opened ) || ( ! m_inner_opened ) || m_file_count == 0 )
            {
                throw LogWriterCantWriteException();
            }

            w_inner_output_policy::CloseFile();
            m_inner_opened = false;

            std::string l_from, l_to;
            AT_Assert( m_file_count > 0 );
            for ( int l_i = m_file_count; l_i != -1; --l_i )
            {
                l_to = l_from;
                if ( l_i == 0 )
                {
                    l_from = m_base_file_name;
                }
                else
                {
                    l_from = ArchiveFileName( m_base_file_name, l_i );
                }

                try
                {
                    if ( FileExists( l_from ) )
                    {
                        if ( l_i == m_file_count )
                        {
                            DeleteFile( l_from );
                        }
                        else
                        {
                            RenameFile( l_from, l_to );
                        }
                    }
                }
                catch ( LogWriterOutputFileOpsException )
                {
                    throw LogWriterCantWriteException();
                }

            }

            w_inner_output_policy::OpenFile( m_base_file_name );
            m_inner_opened = true;
        }

        void SetFileCount( int i_file_count )
        {
            AT_Assert( i_file_count > 0 );

            if ( i_file_count < m_file_count )
            {
                for ( int l_i = m_file_count; l_i != i_file_count; --l_i )
                {
                    std::ostringstream i_oss;
                    i_oss << m_base_file_name << "." << l_i;
                    std::string l_from = i_oss.str();

                    try
                    {
                        if ( FileExists( l_from ) )
                        {
                            DeleteFile( l_from );
                        }
                    }
                    catch ( LogWriterOutputFileOpsException )
                    {
                        // Can't delete the extra archived log files.
                        // C'est la vie...
                    }
                }
            }

            m_file_count = i_file_count;
        }

        void OpenFile( const std::string & i_file_name )
        {
            AT_Assert( ! m_opened );
            m_base_file_name = i_file_name;
            w_inner_output_policy::OpenFile( m_base_file_name );
            m_inner_opened = true;
            m_opened = true;
        }

        void CloseFile()
        {
            AT_Assert( m_opened );
            if ( m_inner_opened )
            {
                w_inner_output_policy::CloseFile();
                m_inner_opened = false;
            }
            m_opened = false;             
        }

        inline t_inner_ostream_type & InnerOStream()
        {
            return w_inner_output_policy::OStream();
        }

    private:

        /* Unimplemented */
        LogWriterOutputPolicy_RotatingFiles(
            const LogWriterOutputPolicy_RotatingFiles &
        );
        LogWriterOutputPolicy_RotatingFiles & operator=(
            const LogWriterOutputPolicy_RotatingFiles &
        );

    };


}


#endif
