//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_factory_funcs.cpp
 *
 */


#include "at_assert.h"
#include "at_factory.h"
#include "at_status_report_basic.h"

#include <cstring>

// Austria namespace
namespace at
{

// ======== Wrapper_Basic::ReportError =============================
//
//

void    Wrapper_Basic::ReportError(
    FactoryTraits::FactoryErrorCodes             i_code,
    const AT_String                             & i_msg
)
{
    
    switch ( m_create_report_option )
    {
        case FactoryTraits::AlwaysReturn :
        default :
        {

            if ( m_srep != 0 )
            {
                m_srep->ReportStatus(
                    StatusReport::ErrorStatus,
                    i_code,
                    i_msg
                );
            }

            AT_Assert( FactoryTraits::AssertOnError != m_create_report_option );
            
            break;
        }

        case FactoryTraits::ThrowOnError :
        {

            StatusReport_Basic l_sreport(
                StatusReport::ErrorStatus,
                i_code,
                i_msg
            );

            if ( m_srep != 0 )
            {
                m_srep->ReportStatus( & l_sreport );
            }

            throw l_sreport;

            break;
        }
    }
    
    return;

} // end Wrapper_Basic::ReportError


// ======== Wrapper_Basic::ReportFactoryNotFound ===================
//
//

void Wrapper_Basic::ReportFactoryNotFound()
{

    ReportError(
        FactoryTraits::FactoryNotFound,
        "Requested factory was not found"
    );
    
} // end Wrapper_Basic::ReportFactoryNotFound



// ======== Wrapper_Basic::ReportCreateException ===================
//

void Wrapper_Basic::ReportCreateException( const AT_String & i_keystr )
{

    ReportError(
        FactoryTraits::FactoryCreateException,
        AT_String( "Exception thrown when creating using factory :" ) + i_keystr
    );
    
    return;

} // end Wrapper_Basic::ReportCreateException



// ======== Wrapper_Basic::ReportFactoryFailed =====================
//

void Wrapper_Basic::ReportFactoryFailed( const AT_String & i_keystr )
{

    ReportError(
        FactoryTraits::FactoryFailure,
        AT_String( "Object not created from factory :" ) + i_keystr
    );
    
} // end Wrapper_Basic::ReportFactoryFailed


/**
 * Compare two DKy for equality
 * @param i_value is the RHS of the operator==.
 */

bool DKy::operator==( const DKy & i_value ) const
{
    return std::strcmp( m_value, i_value ) == 0;
}

/**
 * Compare two DKy for less than
 * @param i_value is the RHS of operator<.
 */

bool DKy::operator<( const DKy & i_value ) const
{
    return std::strcmp( m_value, i_value ) < 0;
}

/**
 * Compare two DKy for less than or equal
 * @param i_value is the RHS of operator<=
 */

bool DKy::operator<=( const DKy & i_value ) const
{
    return std::strcmp( m_value, i_value ) <= 0;
}

/**
 * Compare two DKy for greater than
 * @param i_value is the RHS of operator>
 */

bool DKy::operator>( const DKy & i_value ) const
{
    return std::strcmp( m_value, i_value ) > 0;
}

/**
 * Compare two DKy for greater than or equal
 * @param i_value is the RHS of operator>=
 */

bool DKy::operator>=( const DKy & i_value ) const
{
    return std::strcmp( m_value, i_value ) >= 0;
}



// ======== FactoryRegisterRegistry_Impl ==============================
/**
 * This is the root implementation of the FactoryRegisterRegistry.
 *
 */

class FactoryRegisterRegistry_Impl
  : public FactoryRegisterRegistry
{
    public:

    // t_RREntryCompare compares the two keys
    //
    struct t_RREntryCompare
    {
        bool operator()( const DKy s1, const DKy s2 ) const
        {
            return s1 < s2;
        }
    };

    //
    // typedef the map
    typedef std::map<
        const DKy,
        Ptr<FactoryRegisterBase *>,
        t_RREntryCompare
    >                                           t_FRRMapType;
    typedef t_FRRMapType::iterator              t_FRRMapIterator;
    typedef t_FRRMapType::value_type            t_FRRMapValue;

    // ======== FindOrCreate ==========================================
    virtual PtrDelegate<FactoryRegisterBase *> FindOrCreate(
        const DKy               & i_key,
        t_creator                 i_create_meth
    ) {

        // attempt to insert the value in the registry - this will fail
        // if it already exists.
        std::pair<t_FRRMapIterator, bool> l_result = m_registy_registry.insert(
            t_FRRMapValue(
                i_key,
                Ptr<FactoryRegisterBase *>()
            )
        );

        // check to see if the insert worked (which means it did not exist!)
        if ( l_result.second )
        {

            // we need to create the registry now using the method
            // that the caller passed in.
            ( * l_result.first ).second = ( * i_create_meth )();
        }

        // ready to pass back
        return ( * l_result.first ).second;
        
    }

    virtual void Shutdown()
    {
        m_registy_registry.clear();
    }

    //
    // the actual registry of registries map
    t_FRRMapType                                m_registy_registry;
};


// ======== FactoryRegisterRegistry::Get ==============================

PtrView<FactoryRegisterRegistry *> FactoryRegisterRegistry::Get()
{
    // create the registry if it has not already been created
    static Ptr<FactoryRegisterRegistry *>   s_frr( new FactoryRegisterRegistry_Impl );

    return s_frr;
}


}; // namespace
