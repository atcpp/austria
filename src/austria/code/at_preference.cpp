/**
 *  \file at_preference.cpp
 *
 *  \author Bradley Austin
 *
 *  at_preference.cpp is a preferences library.
 *
 */


#include "at_preference.h"


namespace at
{


    void DummyMutex::Lock()
    {
    }


    void DummyMutex::Unlock()
    {
    }


    void PreferenceValueTable::SetValue(
        const std::string & i_name,
        const std::string & i_context,
        const std::string & i_type_name,
        const MultiString & i_external_value
    )
    {
        m_table[ i_name ].insert(
            std::pair< const std::string, t_table_value_type >(
                i_context,
                t_table_value_type(
                    i_type_name,
                    i_external_value
                )
            )
        );
    }

    PreferenceValueTable::LookupResult PreferenceValueTable::Lookup(
        const std::string & i_name,
        const std::string & i_context
    ) const
    {
        t_table_const_iterator l_name_iter = m_table.find( i_name );
        if ( l_name_iter == m_table.end() )
        {
            return LookupResult( 0, 0 );
        }
        const t_context_subtable_type & l_subtable = l_name_iter->second;


        /*
         *  The following code finds the entry in l_subtable with
         *  the longest key that is a prefix of i_context, if one
         *  exists.  The algorithm used is:
         *
         *      Set l_prefix equal to i_context.
         *
         *      Find the entry with the (alphabetically) largest
         *      key less-than or equal to l_prefix.
         *
         *      if the key is equal to l_prefix, return the
         *      associated entry.  Otherwise:
         *
         *          If the two strings have no common prefix
         *          (longer than zero characters), return failure.
         *          Otherwise, set l_prefix equal to the longest
         *          common prefix and repeat.
         */

        const t_context_subtable_const_iterator l_subtable_begin =
            l_subtable.begin();
        const t_context_subtable_const_iterator l_subtable_end =
            l_subtable.end();
        std::string l_prefix = i_context;
        size_t l_prefix_size = l_prefix.size();
        for (;;)
        {
            t_context_subtable_const_iterator l_context_iter =
                l_subtable.lower_bound( l_prefix );
            if ( l_context_iter != l_subtable_end &&
                 l_context_iter->first == l_prefix
               )
            {
                return LookupResult( &( l_name_iter->first ), &*l_context_iter );
            }
            if ( l_context_iter == l_subtable_begin )
            {
                return LookupResult( 0, 0 );
            }
            --l_context_iter;
            const std::string & l_table_prefix = l_context_iter->first;
            const size_t l_table_prefix_size = l_table_prefix.size();
            const std::string::const_iterator l_prefix_begin = l_prefix.begin();
            const size_t l_match_length =
                mismatch(
                    l_prefix_begin,
                    l_prefix_begin +
                        ( l_table_prefix_size < l_prefix_size ?
                          l_table_prefix_size :
                          l_prefix_size
                        ),
                    l_table_prefix.begin()
                ).first - l_prefix_begin;
            if ( l_match_length == 0 )
            {
                return LookupResult( 0, 0 );
            }
            l_prefix.resize( l_prefix_size = l_match_length );
        }

    }


}
