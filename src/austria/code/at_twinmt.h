//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//      http://www.gnu.org/copyleft/lesser.txt
// 


/**
 *  at_twinmt.h
 *
 */


#ifndef x_at_twinmt_h_x
#define x_at_twinmt_h_x 1


#include "at_twin.h"
#include "at_thread.h"


// Austria namespace
namespace at
{

/**
 * @defgroup TheTwinMTInterface The Multithreaded Twin interface

<h2>Multi-Threaded Twin Interface</h2>

<p>This is a new set of interfaces akin to the at::AideTwin and
at::LeadTwin interfaces that are thread safe.
</p>
<p>
Multi-threading imposes significant constraints on the
operations of a Lead and Aide and the TwinMT interface
provides mechanisms for synchronizing operations.  Perhaps
the most significant issue is regarding deadlocks which can
only be addressed by correct ordering of operations between
threads.
</p>
<p>
The TwinMT share a reference counted mutex.  The mutex needs to be
locked before any calls to the peer to ensure that the peer
is not in the process of destruction while its peer is making a
request.  Each peer may have other mutexes to manage its internal
operation.  To avoid deadlock, it is critical that the order in
which each thread attempts to lock each mutex remains the same for
every thread.
</p>

<p>An example where deadlock is ensured: if each of the lead and aide
objects has its own mutex, if the aide notifies the lead by locking its
mutex first followed by locking the mutex of the aide and the aide
performs by taking the mutex of the aide followed by taking the
mutex of the lead, this will ensure that deadlock will occur.
</p>

<p>
It is up to the application writer to ensure that a deadlock is
avoided.  Care must be taken to enure that mutex locking order is
preserved.


 *
 */


//
// forward declaration
//
template<
    class w_lead_interface,
    class w_aide_twin_interface
>
class LeadTwinMT;

template<
    class w_aide_interface
>
class AideTwinMT;


// ======== TwinMTNullAide ============================================
/**
 *  TwinMTNullAide is a "do nothing"
 *
 */
struct AUSTRIA_EXPORT TwinMTNullAide
{
};


// ======== LeadTwinMT ================================================
/**
 *  The LeadTwinMT is the initiator of the association.
 *
 *  The w_aide_twin_interface template parameter would usually be an
 *  AideTwinMT< w_aide_interface > but the only requirement is that it
 *  provides an interface containing the AideCancel method and for
 *  correctness does not allow its destructor to be called.
 */
template<
    class w_lead_interface,
    class w_aide_twin_interface = AideTwinMT< TwinMTNullAide >
>
class LeadTwinMT
  : public w_lead_interface
{

    friend class AideTwinMT< typename w_aide_twin_interface::t_aide_interface >;

  public:

    /**
     *  LeadCancel is called by anyone to initiate a cancel operation.
     */
    virtual void LeadCancel() = 0;

    // ======== LeadCancelPrivate =====================================
    /**
     * LeadCancelPrivate must only be called by the associated 
     * aide and will disassociate and not call back the aide 
     * after this has been called.
     *
     * @return nothing
     */

    virtual void LeadCancelPrivate() = 0;

  protected:

    /**
     *  LeadAssociate is only called by the Aide to associate the Lead
     *  with the appropriate Aide.  Hence this method is protected.
     *
     *  @param i_aide The aide pointer.
     *  @param i_mutex A reference counted mutex that must be locked
     *          prior to accessing the i_aide interface.  You're probably
     *          going to shoot yourself in the foot if you don't use a
     *          recursive mutex.
     */
    virtual bool LeadAssociate(
        w_aide_twin_interface  * i_aide,
        PtrView< MutexRefCount * >  i_mutex
    ) = 0;

    /**
     *  Destructors must not be called on the LeadTwinMT interface.
     */
    virtual ~LeadTwinMT() {}

};


// ======== AideTwinMT ================================================
/**
 *  The AideTwinMT interface is basically a cancel method.  Other parts
 *  of the interface may be added through the w_aide_interface
 *  interface but the only requirement is that it contains an
 *  AideCancel method.
 */
template< typename w_aide_interface >
class AideTwinMT
  : public w_aide_interface
{

  public:

    // ======== AideCancel ============================================
    /**
     * AideCancel must only be called by the aide side of this
     * interface.  It will notify the lead that the aide is no
     * longer available by calling LeadCancelPrivate on the
     * associated lead.  Calling AideCancel on an unassociated
     * AideTwin will do nothing.
     *
     * @return nothing
     */

    virtual void AideCancel() = 0;


    // ======== AideCancelPrivate =====================================
    /**
     * AideCancelPrivate must only be called by the associated 
     * lead and will disassociate and not call back the lead 
     * after this has been called.
     * 
     * @return nothing
     */

    virtual void AideCancelPrivate() = 0;

    typedef  w_aide_interface  t_aide_interface;

  protected:

    /**
     *  WrapLeadAssociate allows subclasses to access the LeadAssociate
     *  function.
     *
     *  @param i_lead The lead associated with this
     *  @param i_mutex The Mutex shared with the lead.  You're probably
     *  going to shoot yourself in the foot if this is not a recursive
     *  mutex.
     */
    template < typename w_lead >
    inline bool WrapLeadAssociate(
        w_lead  * i_lead,
        PtrView< MutexRefCount * >  i_mutex
    )
    {
        return i_lead->LeadAssociate( this, i_mutex );
    }

    virtual ~AideTwinMT() {}

};


}; // namespace


#endif // x_at_twinmt_h_x
