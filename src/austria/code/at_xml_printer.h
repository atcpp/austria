// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_printer.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_printer_h_x
#define x_at_xml_printer_h_x 1

#include "at_xml_node.h"
#include <ostream>

// Austria namespace
namespace at
{
    /**
     *  Writes an XML document to an output stream.
     *  @param i_outStream The output stream.
     *  @param i_xmlDoc    A pointer to the XML document.
     */
    void XmlPrint( std::ostream& i_outStream, PtrDelegate< XmlDocument* > i_xmlDoc );

    /**
     *  Writes an XML document to an output stream.
     *  @param i_outStream The output stream.
     *  @param i_xmlDoc    The XML document.
     */
    void XmlPrint( std::ostream& i_outStream, const XmlDocument& i_xmlDoc );

    /**
     *  Writes an XML document to an output stream.
     *  @param i_outStream The output stream.
     *  @param i_xmlDoc    A pointer to the XML document.
     */
    void XmlPrettyPrint( std::ostream& i_outStream, PtrDelegate< XmlDocument* > i_xmlDoc );

    /**
     *  Writes an XML document to an output stream.
     *  @param i_outStream The output stream.
     *  @param i_xmlDoc    The XML document.
     */
    void XmlPrettyPrint( std::ostream& i_outStream, const XmlDocument& i_xmlDoc );

    /**
     * XmlPrinterErr is an exception class used with AT_ThrowDerivation.
     *
     * ExceptionDerivation<XmlPrinterErr> is thrown if a error occurs while printing an XML document.
     */
    struct XmlPrinterErr {};

}; // namespace

#endif // x_at_xml_printer_h_x
