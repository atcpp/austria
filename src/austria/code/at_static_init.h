/**
 *  \file at_static_init.h
 *
 *  \author Bradley Austin
 *
 *  at_static_init.h contains a template useful for streamlining the process
 *  of initializing statically-allocated objects, especially STL containers.
 */

#ifndef x_at_static_init_h_x
#define x_at_static_init_h_x 1

namespace at
{

    template<
        class w_type,
        class w_initializer_type
    >
    struct Initialized
      : public w_type
    {

        Initialized()
        {
            w_initializer_type::Initialize( * static_cast< w_type * >( this ) );
        }

        Initialized( const w_initializer_type & i_initializer )
        {
            i_initializer( * static_cast< w_type * >( this ) );
        }

    };

}

#endif
