//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_composite.cpp
 *
 * \author Guido Gherardi
 *
 */

#include "at_xml_composite.h"
#include "at_exception.h"
#include "at_assert.h"

#define AT_ThrowXmlHierarchyRequestErr(msg) AT_ThrowDerivation( at::XmlHierarchyRequestErr, msg )
#define AT_ThrowXmlWrongDocumentErr(msg)    AT_ThrowDerivation( at::XmlWrongDocumentErr, msg )
#define AT_ThrowXmlNotFoundErr(msg)         AT_ThrowDerivation( at::XmlNotFoundErr, msg )

// Austria namespace
namespace at
{
    // ========== Class XmlComposite Implementation =============================================================

    bool XmlComposite::HasChildNodes() const
    {
        return m_firstChild;
    }

    XmlNodeList XmlComposite::ChildNodes() const
    {
        XmlNodeList l_result;
        PtrView< XmlNode* > l_child = m_firstChild;
        while ( l_child )
        {
            l_result.push_back( l_child );
            l_child = l_child->m_nextSibling;
        }
        return l_result;
    }

    PtrView< XmlNode* > XmlComposite::FirstChild() const
    {
        return m_firstChild;
    }

    PtrView< XmlNode* > XmlComposite::LastChild() const
    {
        return m_lastChild;
    }

    void XmlComposite::CheckHierarchyRequest( PtrView< const XmlNode* > i_newChild ) const
    {
        if ( i_newChild->IsAncestorOf( this ) )
        {
            AT_ThrowXmlHierarchyRequestErr( "i_newChild is an ancestor node of this node" );
        }
        if ( OwnerDocument() && i_newChild->OwnerDocument() != OwnerDocument() )
        {
            AT_ThrowXmlWrongDocumentErr( "i_newChild was not created by the document that created this node" );
        }
        if ( ! OwnerDocument() && i_newChild->OwnerDocument() != this->AsDocument() )
        {
            AT_ThrowXmlWrongDocumentErr( "i_newChild was not created by this document" );
        }
    }

    PtrView< XmlNode* > XmlComposite::InsertBefore( PtrDelegate< XmlNode* > i_newChild,
                                                    PtrView< XmlNode* >     i_refChild )
    {
        if ( ! i_refChild )
        {
            return AppendChild( i_newChild );
        }
        if ( ! i_newChild )
        {
            return i_newChild;
        }
        if ( i_refChild->ParentNode().Get() != this )
        {
            AT_ThrowXmlNotFoundErr( "i_refChild is not a child node of this node" );
        }
        CheckHierarchyRequest( i_newChild );
        if ( PtrView< XmlComposite* > l_parent = i_newChild->ParentNode() )
        {
            i_newChild = l_parent->RemoveChild( i_newChild );
        }
        i_newChild->m_parentNode = this;
        i_newChild->m_nextSibling = i_refChild;
        i_refChild->m_previousSibling = i_newChild;
        if ( i_refChild == m_firstChild )
        {
            AT_Assert( i_newChild->m_previousSibling == 0 );
            return m_firstChild = i_newChild;
        }
        PtrView< XmlNode* > l_previousSibling = ( i_newChild->m_previousSibling = i_refChild->m_previousSibling );
        return l_previousSibling->m_nextSibling = i_newChild;
    }

    PtrView< XmlNode* > XmlComposite::AppendChild( PtrDelegate< XmlNode* > i_newChild )
    {
        if ( ! i_newChild )
        {
            return i_newChild;
        }
        CheckHierarchyRequest( i_newChild );
        if ( PtrView< XmlComposite* > l_parent = i_newChild->ParentNode() )
        {
            i_newChild = l_parent->RemoveChild( i_newChild );
        }
        i_newChild->m_parentNode = this;
        AT_Assert( i_newChild->m_nextSibling == 0 && i_newChild->m_previousSibling == 0 );
        if ( m_lastChild )
        {
            i_newChild->m_previousSibling = m_lastChild;
            m_lastChild = i_newChild;
            return m_lastChild->m_previousSibling->m_nextSibling = i_newChild;
        }
        return m_lastChild = ( m_firstChild = i_newChild );
    }

    PtrDelegate< XmlNode* > XmlComposite::RemoveChild( PtrDelegate< XmlNode* > i_oldChild )
    {
        if ( ! i_oldChild || i_oldChild->ParentNode().Get() != this )
        {
            AT_ThrowXmlNotFoundErr( "i_oldChild is null or is not a child node of this node" );
        }
        if ( i_oldChild->m_previousSibling )
        {
            i_oldChild->m_previousSibling->m_nextSibling = i_oldChild->m_nextSibling;
        }
        else
        {
            m_firstChild = i_oldChild->m_nextSibling;
        }
        if ( i_oldChild->m_nextSibling )
        {
            i_oldChild->m_nextSibling->m_previousSibling = i_oldChild->m_previousSibling;
        }
        else
        {
            m_lastChild = i_oldChild->m_previousSibling;
        }
        i_oldChild->m_parentNode = 0;
        i_oldChild->m_previousSibling = 0;
        i_oldChild->m_nextSibling = 0;
        return i_oldChild;
    }

    PtrDelegate< XmlNode* > XmlComposite::ReplaceChild( PtrDelegate< XmlNode* > i_newChild,
                                                        PtrDelegate< XmlNode* > i_oldChild )
    {
        if ( ! i_newChild )
        {
            return this->RemoveChild( i_oldChild );
        }
        if ( ! i_oldChild || i_oldChild->ParentNode().Get() != this )
        {
            AT_ThrowXmlNotFoundErr( "i_oldChild is null or is not a child node of this node" );
        }
        CheckHierarchyRequest( i_newChild );
        if ( PtrView< XmlComposite* > l_parent = i_newChild->ParentNode() )
        {
            i_newChild = l_parent->RemoveChild( i_newChild );
        }
        i_newChild->m_parentNode = this;
        if ( PtrView< XmlNode* > l_nextSibling = ( i_newChild->m_nextSibling = i_oldChild->m_nextSibling ) )
        {
            l_nextSibling->m_previousSibling = i_newChild; // does not invalidate i_newChild
        }
        else
        {
            m_lastChild = i_newChild;
        }
        if ( PtrView< XmlNode* > l_prevSibling = (i_newChild->m_previousSibling = i_oldChild->m_previousSibling) )
        {
            l_prevSibling->m_nextSibling = i_newChild; // invalidates i_newChild
        }
        else
        {
            m_firstChild = i_newChild; // invalidates i_newChild
        }
        return i_oldChild;
    }

}; // namespace
