//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_thread.h
 *
 */

#ifndef x_at_thread_h_x
#define x_at_thread_h_x 1

#include "at_exports.h"
#include "at_types.h"
#include "at_lifetime.h"
#include "at_lifetime_mt.h"
#include "at_attr_mask.h"

//
// Include system specific definitions
// *note* No system header files (i.e. linux, windows etc system headers)
//
#include AT_THREAD_H


// Austria namespace
namespace at
{

class Conditional;
struct TaskContext;

// ======== Task ======================================================
/**
 * Task is the base "task" class that implements the threading
 * interface for Austria threads.  The application is required to
 * implement the "Work" method.  Once the class has been created
 * the "Start" method may be called to commence the new thread.
 *
 */

class AUSTRIA_EXPORT Task
{
    public:
    
    /**
     * TaskID is system specific.  The only operations
     * that will be supported across all platforms are
     * ostream << TaskID, equality and less than.
     *
     */

    typedef SYS_TaskID     TaskID;


    // ======== Task ==================================================
    /**
     * Task constructor.
     *
     */

    Task();
    
    // ======== ~Task =================================================
    /**
     * The Task destructor will implicitly wait for the thread to
     * terminate.
     *
     */

    virtual ~Task();


    // ======== Work ==================================================
    /**
     * Work is derived by the application to perform the work required
     * by the task.
     *
     * @return nothing
     */

    virtual void Work() = 0;

    // ======== Start =================================================
    /**
     * Start() will initiate the created thread.  Start() must be
     * called at least once.  (i.e. never create a Task object and then
     * destroy it without ever having started the thread.)  Calling
     * Start() on a task that has already been started will have no
     * effect.
     *
     * It may be convenient to call Start() from the constructor for a
     * class deriving from Task.  However, since Start() causes Work()
     * to be called, and Work() is a virtual method, the usual caveats
     * regarding calling virtual methods from constructors apply.
     * (i.e. Start() should only be called from a constructor for the
     * same class in which Work() is defined, or a decendant of that
     * class.)
     *
     * @return nothing
     */

    void Start();

    // ======== Wait ==================================================
    /**
     * Wait() causes the thread that calls it to go to sleep until the
     * Task thread terminates.  (This is also called "joining").  If
     * the Task thread has already terminated, Wait() returns
     * immediately.
     *
     * Wait() is called in the Task destructor, in order to guarantee
     * that the Task object won't be destroyed before the Task thread
     * terminates.  Wait() can also be called directly (outside the
     * Task destructor), in order to join a thread with the Task thread
     * without destructing the Task object.  (This is useful, for
     * example, when multiple threads need to join with the same Task
     * thread.)
     *
     * When Wait() is called directly, the user is responsible for
     * guaranteeing that all direct calls to Wait() have completed
     * before the Task object destructor is invoked.
     *
     * @return nothing
     */

    void Wait();

    // ======== Wait ==================================================
    /**
     * See description of the zero-argument version of Wait(), above.
     *
     * This version of Wait() will not wait more than i_time.
     *
     * @param i_time The maximum time to wait.
     * @return bool   return true if the thread completed before the alloted time
     */

    bool Wait( const TimeInterval & i_time );



    // ======== GetThisId =============================================
    /**
     * Get the thread ID of this thread.
     *
     * @return The thread ID of this thread.
     */

    TaskID GetThisId();


    // ======== GetSelfId =============================================
    /**
     * Get the thread ID of the calling thread.
     *
     * @return nothing
     */

    static TaskID GetSelfId();

    // ======== Sleep =================================================
    /**
     * Sleep pauses execution of the calling thread until one of the following 
     * occurs
     * An I/O completion callback function is called 
     * An asynchronous procedure call (APC) is queued to the thread. 
     * The time-out interval elapses 
     *
     * @param i_time Is the period of time to wait
     *
     * @return if the time interval expired before an I/O or APC trigger then the return 
     *         is true.
     */
    static bool Sleep( const TimeInterval & i_time );

    // ======== Sleep =================================================
    /**
     * Sleep pauses execution of the calling thread until one of the following 
     * occurs
     * An I/O completion callback function is called 
     * An asynchronous procedure call (APC) is queued to the thread. 
     * The time-out interval elapses 
     *
     * @param i_time Is the absolute time to wait until
     *
     * @return if the time interval expired before an I/O or APC trigger then the return 
     *         is true.
     */
    static bool Sleep( const TimeStamp & i_time );

    private:

    //
    // Can't copy a task.
    Task( const Task & );
    Task & operator= ( const Task & );

    friend struct TaskContext;

    /**
     * m_task_context contains information specific to this implementation.
     */
    
    TaskContext                         * m_task_context;

};


// ======== Mutex =====================================================
/**
 * Mutex is a "mutual exclusion" lock.  The goal here is that the
 * Mutex is able to be constructed anywhere, hopefully not requiring
 * an allocation of memory.
 */

class AUSTRIA_EXPORT Mutex
{
    
    public:
    friend class Conditional;
//    friend class ConditionalMutex;


    // ======== MutexType =============================================
    /**
     * A Mutex may behave in different ways depending on type.
     *
     */

    enum MutexType
    {
        /**
         * NonRecursive is a "fast" lock.  It does not allow a thread
         * to attain the mutex more than once.
         */
        NonRecursive,

        /**
         * Recursive allows the process that attains (owns) a lock, to re-attain 
         * the lock more than once (while still locked).  When the number
         * of calls to Unlock equals the number of calls to Lock, the mutex
         * is considered released.
         */
        Recursive,

        /**
         * Checking is an error checking lock.
         */
        Checking
    };
    

    // ======== Mutex =================================================
    /**
     * Mutex constructor initializes the m_mutex_context for this
     * object based on the mutex type.
     *
     */

    Mutex( MutexType i_type = NonRecursive );


    // ======== Mutex =================================================
    /**
     * Mutex destructor.
     *
     */

    virtual ~Mutex();


    // ======== Lock ==================================================
    /**
     * Lock will wait indefinitely to attain the lock.
     *
     * @return nothing
     */

    void Lock();


    // ======== TryLock ===============================================
    /**
     * TryLock will attempt to attain the mutex.  If mutex is currently
     * taken, TryLock will return immediatly with a "false" return value,
     * otherwise it will attain the lock and return true.
     *
     * @return true when lock is successfully attained, false otherwise.
     */

    bool TryLock();    


    // ======== Unlock ================================================
    /**
     * Unlock will release the mutex.  It is an error to call unlock
     * if the calling thread has not previously locked the mutex.
     *
     * @return nothing
     */

    void Unlock();
    

    /**
     * MutexContext is opaque but it occupies the same size as the
     * implementation dependant (posix) mutex.
     */

    struct MutexContext
    {
        SystemMutexContextType                  m_data;

    };

    private:

    /**
     * m_mutex_context is a system dependant context variable.
     */

    MutexContext                    m_mutex_context;

    // copy constructor and assignment operator are private and
    // unimplemented.  It is illegal to copy a mutex.
    Mutex( const Mutex & );
    Mutex & operator= ( const Mutex & );
};


// ======== MutexRefCount =============================================
/**
 * MutexRefCount is a reference counted mutex.  This can be used
 * to manage cases where two or more objects need to share a
 * Mutex and any of the objects may be deleted in any order.
 *
 */

class AUSTRIA_EXPORT MutexRefCount
  : public Mutex,
    public PtrTarget_MT
{
    public:

    /**
     * MutexRefCount constructor has the same signature as
     * a regular mutex.
     */
    MutexRefCount( MutexType i_type = NonRecursive )
      : Mutex( i_type )
    {
    }

    virtual ~MutexRefCount() {}
};

// ======== Conditional ===============================================
/**
 * Conditional implements the "Conditional Variable" semantics of
 * posix pthreads.
 *
 * Conditional has the functionality of a mutex *and* the ability to
 * notify a number of threads on a change "or wake them all up"
 * functionality.  Threads waiting are not scheduled to run, while
 * a mutex is usually employed as a spin-lock.
 *
 */

class AUSTRIA_EXPORT Conditional
{
    public:
//    friend class ConditionalMutex;


    // ======== Conditional ===========================================
    /**
     * Conditional constructs a "Conditional Variable" access
     * device.
     * 
     */

    Conditional( Mutex & i_mutex );

    // destructor
    virtual ~Conditional();

    // ======== Wait ==================================================
    /**
     * Wait() will wait until another thread "notifies" this thread
     * (the thread calling Wait()).
     *
     * IMPORTANT! - The thread calling Wait() must be holding a lock on
     * the Conditional object's mutex when Wait() is called.  Calling
     * Wait() without locking the mutex first will result in undefined
     * behavior.  Wait() releases the lock on the mutex while it is
     * waiting, and restores it before returning.
     *
     * IMPORTANT! - Wait() may return prematurely (i.e. without any
     * thread having signalled the condition).  This is because
     * allowing (infrequent) spurious wakeups allows condition
     * variables to be implemented much more simply, and more
     * efficiently.  Because of this, Wait() should only be called
     * inside a loop, which is exited only when the condition is
     * verified true.
     *
     * In other words, code that calls Wait() should always follow the
     * following basic template:
     *
     *     Conditional cv;
     *
     *     ...
     *
     *     {
     *         Lock< Conditional > lock( cv );
     *         while ( ! condition )
     *         {
     *             cv.Wait();
     *         }
     *     }
     *
     * @return nothing
     */

    void Wait();

    // ======== Wait ==================================================
    /**
     * Wait() will wait until another thread "notifies" this thread
     * (the thread calling Wait()), or until the given amount of time
     * has elapsed.  The time i_time is relative to the time that this
     * function is called.
     *
     * IMPORTANT! - See the important notes in the zero-argument
     * version of Wait() above.
     *
     * @return true if the condition was signalled, false if it timed out
     */

    bool Wait( const TimeInterval & i_time );


    // ======== Wait ==================================================
    /**
     * Wait() will wait until another thread "notifies" this thread
     * (the thread calling Wait()), or until the given time has been
     * reached.  The time i_time is absolute.
     *
     * IMPORTANT! - See the important notes in the zero-argument
     * version of Wait() above.
     *
     * @return true if the condition was signalled, false if it timed out
     */

    bool Wait( const TimeStamp & i_time );


    // ======== Post ==================================================
    /**
     * Post a notification to a single waiting thread.
     *
     */

    void Post();


    // ======== PostAll ===============================================
    /**
     * Post a notification to all waiting threads.
     *
     */

    void PostAll();    
    

    /**
     * ConditionalContext is opaque but it occupies the same size as the
     * implementation dependant (posix) pthread_cond_t.
     */

    struct ConditionalContext
    {
        SystemConditionalContextType            m_data;
    };

    private:

    /**
     * m_conditional_context is a system dependant context variable.
     */

    ConditionalContext                 m_conditional_context;


    // copy constructor and assignment operator are private and
    // unimplemented.  It is illegal to copy a Conditional.
    Conditional( const Conditional & );
    Conditional & operator= ( const Conditional & );

};


// ======== ConditionRefCount =========================================
/**
 * ConditionRefCount is basically the at::Conditional with reference
 * counting.
 */

class AUSTRIA_EXPORT ConditionRefCount
  : public Conditional,
    public PtrTarget_MT
{
    public:

    /**
     * ConditionRefCount requires the same constructor as
     * Conditional
     *
     */
    ConditionRefCount( Mutex & i_mutex )
      : Conditional( i_mutex )
    {
    }

    virtual ~ConditionRefCount() {}

};


// ======== ConditionLazy =============================================
/**
 * Lazy is used in those circumstances where creating a
 * Conditional is usually unecessary. The operation is not dissimilar
 * to at::Conditional but the at::Conditional is created only when a waiter
 * is needed.  The Mutex must exist and be locked before the conditional
 * wait is called.
 *
 * The other advantage of this is that the ConditionLazy object need not exist
 * after calling Wait so it may be used in cases where the ConditionLazy
 * object is destroyed (the mutex must not be destroyed).
 */

class AUSTRIA_EXPORT ConditionLazy
{
    public:

    /**
     * ConditionLazy
     *
     */
    ConditionLazy( Mutex & i_mutex )
      : m_mutex( i_mutex )
    {
    }

    virtual ~ConditionLazy() {}

    // ======== Lock ==================================================
    /**
     * Lock will wait indefinitely to attain the lock.
     *
     * @return nothing
     */

    inline void Lock()
    {
        m_mutex.Lock();
    }


    // ======== TryLock ===============================================
    /**
     * TryLock will attempt to attain the mutex.  If mutex is currently
     * taken, TryLock will return immediatly with a "false" return value,
     * otherwise it will attain the lock and return true.
     *
     * @return true when lock is successfully attained, false otherwise.
     */

    inline bool TryLock()
    {
        return m_mutex.TryLock();
    }


    // ======== Unlock ================================================
    /**
     * Unlock will release the mutex.  It is an error to call unlock
     * if the calling thread has not previously locked the mutex.
     *
     * @return nothing
     */

    inline void Unlock()
    {
        m_mutex.Unlock();
    }


    // ======== Wait ==================================================
    /**
     * Wait will wait until another thread "notifies" this thread
     * (the thread calling Wait()).
     *
     * @return nothing
     */

    inline void Wait()
    {
        // mutex should be locked !
        Ptr< ConditionRefCount * >  l_crc = FetchOrCreateCond();

        l_crc->Wait();
    }

    // ======== Wait ==================================================
    /**
     * Wait will wait until another thread Post()'s i.e. "notifies"
     * the thread calling Wait(). The time i_time is relative to 
     * the time that this function is called.
     *
     * @return true if the condition was signalled, false if it timed out
     */

    bool Wait( const TimeInterval & i_time )
    {
        // mutex should be locked !
        Ptr< ConditionRefCount * >  l_crc = FetchOrCreateCond();

        return l_crc->Wait( i_time );
    }


    // ======== Wait ==================================================
    /**
     * Wait will wait until another thread "notifies"
     * (the thread calling Wait()). The time i_time is absolute.
     *
     * @return true if the condition was signalled, false if it timed out
     */

    bool Wait( const TimeStamp & i_time )
    {
        // mutex should be locked !
        Ptr< ConditionRefCount * >  l_crc = FetchOrCreateCond();

        return l_crc->Wait( i_time );
    }


    // ======== Post ==================================================
    /**
     * Post a notification to a single waiting thread.
     *
     */

    void Post()
    {
        if ( m_cond != 0 )
        {
            m_cond->Post();
        }
    }

    // ======== PostAll ===============================================
    /**
     * Post a notification to all waiting threads.
     *
     */

    void PostAll()
    {
        if ( m_cond != 0 )
        {
            m_cond->PostAll();
        }
    }

    private:

    // ======== FetchOrCreateCond =========================================
    /**
     * FetchOrCreateCond fetches the ConditionRefCount if it has
     * already been created, and creates it if not.
     */

    inline Ptr< ConditionRefCount * > & FetchOrCreateCond()
    {
        if ( ! m_cond )
        {
            m_cond = new ConditionRefCount( m_mutex );
        }

        return m_cond;
    }
    

    /**
     * The mutex must be some other mutex.
     */
    Mutex                               & m_mutex;

    /**
     * m_cond is a pointer to the condition variable.
     *
     */
    Ptr< ConditionRefCount *>           m_cond;
};


// ======== ConditionalMutex ==========================================
/**
 * ConditionalMutex combines a mutex with a conditional variable.
 *
 *
 */


class AUSTRIA_EXPORT ConditionalMutex
  : public Mutex,
    public Conditional
{
    public:


    // ======== ConditionalMutex ======================================
    /**
     * Constructor for ConditionalMutex.
     *
     * @param i_type select the MutexType of this mutex
     *
     */

    ConditionalMutex( MutexType i_type = NonRecursive );

    virtual ~ConditionalMutex() {}

    private:
    ConditionalMutex( const ConditionalMutex & );
    ConditionalMutex & operator= ( const ConditionalMutex & );
};


// ======== ConditionalMutex ==========================================
/**
 * ConditionalMutexRefCount combines a mutex with a conditional variable.
 * with reference counting
 *
 */


class AUSTRIA_EXPORT ConditionalMutexRefCount
  : public MutexRefCount,
    public Conditional
{
    public:

    // ======== ConditionalMutex ======================================
    /**
     * Constructor for ConditionalMutex.
     *
     * @param i_type select the MutexType of this mutex
     *
     */

    ConditionalMutexRefCount( MutexType i_type = NonRecursive );

    virtual ~ConditionalMutexRefCount() {}

    private:
    ConditionalMutexRefCount( const ConditionalMutexRefCount & );
    ConditionalMutexRefCount & operator= ( const ConditionalMutexRefCount & );
};


// ======== Lock ================================================
/**
 * Lock implements a basic lock using the acquisition is
 * initialization paradigm.
 *
 */

template <typename w_MutexType>
class Lock
{
    public:

    w_MutexType             & m_mutex;

    Lock( w_MutexType & io_mutex )
      : m_mutex( io_mutex )
    {
        m_mutex.Lock();
    }

    ~Lock()
    {
        m_mutex.Unlock();
    }

    void Wait()
    {
        return m_mutex.Wait();
    }

    bool Wait( const TimeInterval & i_time )
    {
        return m_mutex.Wait( i_time );
    }

    bool Wait( const TimeStamp & i_time )
    {
        return m_mutex.Wait( i_time );
    }

    void Post()
    {
        m_mutex.Post();
    }

    void PostAll()
    {
        m_mutex.PostAll();
    }

    private:

    // must not allow copy or assignment so make
    // these methods private.
    Lock( const Lock & );
    Lock & operator=( const Lock & );
};

// ======== Lock<Ptr<T> > =======================================
/**
 * Lock implements a basic lock using the acquisition is initialization
 * paradigm.  This specialization is for reference counted locks.  If a
 * null reference is passed, no locking/unlocking will be performed.
 */

template <typename w_MutexType>
class Lock<Ptr<w_MutexType> >
{
public:
    Ptr<w_MutexType> m_mutex;

    Lock(const Ptr<w_MutexType> &io_mutex) : m_mutex(io_mutex)
    {
        if (m_mutex) m_mutex->Lock();
    }

    ~Lock()
    {
        if (m_mutex) m_mutex->Unlock();
    }

    void Wait()
    {
        if (m_mutex) m_mutex->Wait();
    }

    bool Wait(const TimeInterval &i_time)
    {
        return m_mutex ? m_mutex->Wait(i_time) : true;
    }

    bool Wait(const TimeStamp &i_time)
    {
        return m_mutex ? m_mutex->Wait(i_time) : true;
    }

    void Post()
    {
        if (m_mutex) m_mutex->Post();
    }

    void PostAll()
    {
        if (m_mutex) m_mutex->PostAll();
    }

private:
    /** Not implemented. */
    Lock(const Lock & );
    /** Not implemented. */
    Lock& operator=(const Lock &);
};

// ======== Unlock =============================================
/**
 * Unlock implements an unlock device where a mutex is unlocked
 * on construction and locked again on destruction.  This allows
 * implementation of code regions where unlocking is required
 * but after leaving the region, the lock must be re-aquired.
 */

template <typename w_MutexType>
class Unlock
{
    public:

    w_MutexType             & m_mutex;

    Unlock( w_MutexType & io_mutex )
      : m_mutex( io_mutex )
    {
        m_mutex.Unlock();
    }

    ~Unlock()
    {
        m_mutex.Lock();
    }

    private:

    // must not allow copy or assignment so make
    // these methods private.
    Unlock( const Unlock & );
    Unlock & operator=( const Unlock & );
};

template <typename w_MutexType>
class Unlock<Ptr<w_MutexType> >
{
    public:

    Ptr<w_MutexType>             m_mutex;

    Unlock( Ptr<w_MutexType> io_mutex )
      : m_mutex( io_mutex )
    {
        m_mutex->Unlock();
    }

    ~Unlock()
    {
        m_mutex->Lock();
    }

    private:

    // must not allow copy or assignment so make
    // these methods private.
    Unlock( const Unlock & );
    Unlock & operator=( const Unlock & );
};

// ======== SwapLock ======================================
/**
 * SwapLock takes two locks, the first of which must already
 * be locked, and the second of which needs to be locked.
 * it performs the following sequence:
 * SwapLock(Lock<2nd>, Unlock<1st>), caller code section, ~SwapLock(Unlock<2nd>, Lock<1st>)
 *
 * it is needed to preserve state when the caller holds a mutex
 * but needs to hold a separate shared/predefined mutex into
 * a call such as Wait for condition variable.
 *
 * The sequence can not be correctly implemented with the
 * Lock/Unlock templates because of the reverse sequence on
 * unwind using those which can cause deadlocks
 * ie: the code with Lock/Unlock templates would try:
 * Lock<2nd>, Unlock<1st>, ~Unlock<1st>, ~Lock<2nd>,
 * with the overall effect of L1, L2, U1, L1, U2
 * and since the seqeunce of locking is reversed, it is
 * deadlock waiting to happen.
 *
 * upon destruction of the SwapLock, the caller must revalidate
 * any state which was protected by the first mutex
 *
 * the SwapLock provides all methods of ConditionalMutex, which
 * will be called on the 2nd parameter Mutex if it supports
 * those methods
 *
 */

template <
 typename w_MutexTypeFirst,
 typename w_MutexTypeSecond
>
class SwapLock
{
public:

    w_MutexTypeFirst & m_mutexFirst;
    w_MutexTypeSecond & m_mutexSecond;

    SwapLock( w_MutexTypeFirst & i_mutexFirst, w_MutexTypeSecond & i_mutexSecond ) :
  m_mutexFirst( i_mutexFirst ),
  m_mutexSecond( i_mutexSecond )
    {
        m_mutexSecond.Lock();
  m_mutexFirst.Unlock();
    }

    ~SwapLock()
    {
  m_mutexSecond.Unlock();
  m_mutexFirst.Lock();
    }

    void Wait()
    {
        return m_mutexSecond.Wait();
    }

    bool Wait( const TimeInterval & i_time )
    {
        return m_mutexSecond.Wait( i_time );
    }

    bool Wait( const TimeStamp & i_time )
    {
        return m_mutexSecond.Wait( i_time );
    }

    void Post()
    {
        m_mutexSecond.Post();
    }

    void PostAll()
    {
        m_mutexSecond.PostAll();
    }

    private:

    // must not allow copy or assignment so make
    // these methods private.
    SwapLock( const SwapLock & );
    SwapLock & operator=( const SwapLock & );
};

// specializations for Ptr to Mutex type, by argument position
template <
 typename w_MutexTypeFirst,
 typename w_MutexTypeSecond
>
class SwapLock<w_MutexTypeFirst, Ptr<w_MutexTypeSecond> >
{
public:

    w_MutexTypeFirst  &m_mutexFirst;
    Ptr<w_MutexTypeSecond> m_mutexSecond;

    SwapLock( w_MutexTypeFirst & i_mutexFirst, Ptr<w_MutexTypeSecond> i_mutexSecond ) :
  m_mutexFirst( i_mutexFirst ),
  m_mutexSecond( i_mutexSecond )
    {
        m_mutexSecond->Lock();
  m_mutexFirst.Unlock();
    }

    ~SwapLock()
    {
  m_mutexSecond->Unlock();
  m_mutexFirst.Lock();
    }

    void Wait()
    {
        return m_mutexSecond->Wait();
    }

    bool Wait( const TimeInterval & i_time )
    {
        return m_mutexSecond->Wait( i_time );
    }

    bool Wait( const TimeStamp & i_time )
    {
        return m_mutexSecond->Wait( i_time );
    }

    void Post()
    {
        m_mutexSecond->Post();
    }

    void PostAll()
    {
        m_mutexSecond->PostAll();
    }

    private:

    // must not allow copy or assignment so make
    // these methods private.
    SwapLock( const SwapLock & );
    SwapLock & operator=( const SwapLock & );
};

// specializations for Ptr to Mutex type, by argument position
template <
 typename w_MutexTypeFirst,
 typename w_MutexTypeSecond
>
class SwapLock<Ptr<w_MutexTypeFirst>, w_MutexTypeSecond >
{
public:

    Ptr<w_MutexTypeFirst> m_mutexFirst;
    w_MutexTypeSecond  & m_mutexSecond;

    SwapLock( Ptr<w_MutexTypeFirst> i_mutexFirst, w_MutexTypeSecond & i_mutexSecond ) :
  m_mutexFirst( i_mutexFirst ),
  m_mutexSecond( i_mutexSecond )
    {
        m_mutexSecond.Lock();
  m_mutexFirst->Unlock();
    }

    ~SwapLock()
    {
  m_mutexSecond.Unlock();
  m_mutexFirst->Lock();
    }

    void Wait()
    {
        return m_mutexSecond.Wait();
    }

    bool Wait( const TimeInterval & i_time )
    {
        return m_mutexSecond.Wait( i_time );
    }

    bool Wait( const TimeStamp & i_time )
    {
        return m_mutexSecond.Wait( i_time );
    }

    void Post()
    {
        m_mutexSecond.Post();
    }

    void PostAll()
    {
        m_mutexSecond.PostAll();
    }

    private:

    // must not allow copy or assignment so make
    // these methods private.
    SwapLock( const SwapLock & );
    SwapLock & operator=( const SwapLock & );
};

// specializations for Ptr to Mutex type, by argument position
template <
 typename w_MutexTypeFirst,
 typename w_MutexTypeSecond
>
class SwapLock<Ptr<w_MutexTypeFirst>, Ptr<w_MutexTypeSecond> >
{
public:

    Ptr<w_MutexTypeFirst> m_mutexFirst;
    Ptr<w_MutexTypeSecond> m_mutexSecond;

    SwapLock( Ptr<w_MutexTypeFirst> i_mutexFirst, Ptr<w_MutexTypeSecond> i_mutexSecond ) :
  m_mutexFirst( i_mutexFirst ),
  m_mutexSecond( i_mutexSecond )
    {
        m_mutexSecond->Lock();
  m_mutexFirst->Unlock();
    }

    ~SwapLock()
    {
  m_mutexSecond->Unlock();
  m_mutexFirst->Lock();
    }

    void Wait()
    {
        return m_mutexSecond->Wait();
    }

    bool Wait( const TimeInterval & i_time )
    {
        return m_mutexSecond->Wait( i_time );
    }

    bool Wait( const TimeStamp & i_time )
    {
        return m_mutexSecond->Wait( i_time );
    }

    void Post()
    {
        m_mutexSecond->Post();
    }

    void PostAll()
    {
        m_mutexSecond->PostAll();
    }

    private:

    // must not allow copy or assignment so make
    // these methods private.
    SwapLock( const SwapLock & );
    SwapLock & operator=( const SwapLock & );
};

// ======== PtrStyleLocked ============================================
/**
 * PtrStyleLocked defines a style of reference counted object that 
 * is like PtrStyle but uses a mutex to lock the count and
 * possibly other things during the increment and decrement
 * of reference counts.  This is not usually needed.
 */

class PtrStyleLocked
{
    public:

    virtual ~PtrStyleLocked() {}

    enum {

        // ======== InitCount =========================================
        /**
         * InitCount is the initial count setting.
         *
         */

        InitCount = 0,

        // ======== DeleteCheck =======================================
        /**
         * DeleteCheck may be 0 or 1 which indicates whether incrementing
         * a reference count on a deleted object is tested.
         */
        
        DeleteCheck = 1
    };


    // ======== BaseType ==============================================
    /**
     * BaseType allows the PtrStyle to define a base type (if needed). 
     *
     */

    struct BaseType
    {

        // ======== GetPtrMutex =======================================
        /**
         * GetPtrMutex is to be implemented by the deriving class 
         * to get access to a mutex.
         *
         * @return Reference to the controlling mutex is returned.
         */

        virtual Mutex & GetPtrMutex() const = 0;

        virtual ~BaseType() {}
    };


    // ======== LockStyleType ==============================================
    /**
     * LockStyleType is the lock used to protect the object during
     * an increment or decrement.  This is not normally needed
     * so this is a dummy implementation.
     *
     */

    struct LockStyleType
    {
        template <typename w_T>
        inline LockStyleType( w_T i_base )
          : m_lock( i_base->GetPtrMutex() )
        {
        }

        Lock<Mutex>             m_lock;
    };

    // ======== ZeroReferenceAction ===================================
    /**
     * ZeroReferenceAction overrides what must happen when the
     * reference count drops to zero. This allows the
     * PtrTarget_Generic template to use other mechanisms.
     * 
     * @param i_obj is the this pointer of the 
     * @param w_type is the type of the pointer being managed
     */

    template <typename w_type>
    inline static void ZeroReferenceAction( w_type i_obj )
    {
        delete i_obj;
    }
    
};


// ======== PtrTarget_Locked =========================================
/**
 * PtrTarget_Locked implements a locked reference counted pointer.
 * This should only be used in few circumstances where upon destruction
 * of an object, the lock must first be taken because it interacts
 * with other objects.
 */

AUSTRIA_TEMPLATE_EXPORT2(PtrTarget_Generic, int, PtrStyleLocked)
typedef PtrTarget_Generic<int, PtrStyleLocked> PtrTarget_Locked;



// ======== TryLock ===================================================
/**
 *  The TryLock class is similar to the Lock class, but is designed for
 *  situations in which the mutex may be unattainable because the
 *  thread currently holding it might be waiting for the thread that
 *  creates the TryLock to do something (i.e. what would be a deadlock
 *  situation if a normal Lock object were used.)
 *
 *  The motivating scenario is where a thread wants to destroy a Task
 *  object while holding a lock on a mutex which the Task object's
 *  thread must also be able to lock.  The thread calling the Task
 *  object's destructor must wait for the Task thread to return from
 *  Work(), but if the Task thread tries to lock the mutex with a
 *  regular Lock object while the other thread is holding it, waiting
 *  for the Task thread to return from Work(), it never will, and the
 *  threads will be deadlocked.
 *
 *  The TryLock constructor is passed a reference to a mutex, and a
 *  reference to a volatile bool, and spins until it either acquires the lock,
 *  or the bool variable evaluates true.  If the latter happens,
 *  construction of the TryLock object "succeeds", but the Mutex is not
 *  locked.  Whether the lock was aquired can be tested by calling the
 *  TryLock object's IsAcquired() method.  When the TryLock object goes
 *  out of scope, the lock, if held, will be released.
 *
 *  Since the TryLock constructor spins while it tries to acquire the
 *  lock, other threads that lock the mutex should avoid holding it for
 *  very long.
 *
 */
template< typename w_MutexType >
class TryLock
{
    /**
     * m_mutex references the mutex being aquired.
     */

    w_MutexType             & m_mutex;

    /**
     * m_is_acquired indicates wether the mutex was taken.
     */
    bool                      m_is_acquired;

public:

    /**
     * The TryLock constructor will only return if either the mutex 
     * referenced by io_mutex is aquired or if i_flag is true.
     *
     * @param io_mutex A reference to the mutex being aquired.
     * @param i_flag A reference to the flag being checked.
     */

    TryLock( w_MutexType & io_mutex, const volatile bool & i_flag )
      : m_mutex( io_mutex ),
        m_is_acquired( false )
    {
        while (
            ( ! i_flag ) &&
            ( m_is_acquired = m_mutex.TryLock() ) == false
        ) {
            OSTraitsBase::SchedulerYield();
        }
    }

    inline ~TryLock()
    {
        if ( m_is_acquired )
        {
            m_mutex.Unlock();
        }
    }

    void SetAquired( bool i_is_acquired )
    {
        m_is_acquired = i_is_acquired;
    }

    bool IsAcquired() const
    {
        return m_is_acquired;
    }

private:

    /* Unimplemented. */
    TryLock( const TryLock & );
    TryLock & operator=( const TryLock & );

};


// ======== Exclusion =================================================
/**
 * Exclusion is like a lock but only locks out this one section
 * w_MutexType must be somthing that conforms to the ConditionalMutex
 * flavour.
 *
 */

template< typename w_MutexType >
class Exclusion
{

    /**
     * m_mutex references the mutex being aquired.
     */

    w_MutexType             & m_mutex;

    /**
     * m_is_acquired indicates wether the mutex was taken.
     */
    volatile unsigned       & m_acquired_count;

    /**
     * m_is_aquired indicates wether the Exclusion was aquired.
     */
    bool                      m_is_aquired;

public:

    /**
     * The Exclusion constructor will only return if either the mutex 
     * referenced by io_mutex is aquired or if i_flag is true.
     *
     * @param io_mutex          A reference to the mutex being aquired.
     * @param i_acquired_count  Is a reference to a state variable to
     *                          manage the Exclusion state.
     * @param i_no_wait         Is an optional pointer to a bool that if 
     *                          set will indicate not to wait for the
     *                          exclusion to be freed, if not set the
     *                          exclusion will will pause the thread.
     */

    Exclusion(
        w_MutexType         & io_mutex,
        volatile unsigned   & i_acquired_count,
        bool                * i_no_wait = 0
    )
      : m_mutex( io_mutex ),
        m_acquired_count( i_acquired_count ),
        m_is_aquired()
    {
        Lock< w_MutexType >       l_lock( m_mutex );

        while ( m_acquired_count & 1U )
        {
            if ( i_no_wait )
            {
                * i_no_wait = false;
                return;
            }

            m_acquired_count += 2;

            m_mutex.Wait();

            m_acquired_count -= 2;
        }

        m_is_aquired = true;
        m_acquired_count |= 1U;

        if ( i_no_wait )
        {
            * i_no_wait = true;
        }
    }

    inline ~Exclusion()
    {
        Lock< w_MutexType >       l_lock( m_mutex );

        m_acquired_count &= ~1U;

        if ( m_acquired_count )
        {
            m_mutex.PostAll();
        }
    }

private:

    /* Unimplemented. */
    Exclusion( const Exclusion & );
    Exclusion & operator=( const Exclusion & );

};


/**
 * ThreadSafeMutexPtr is a smart pointer that allows the atomic copy of
 * a the managed pointer to another smart pointer.
 */

typedef Ptr<MutexRefCount *, PtrTraits< MutexRefCount *, PtrClassRefWrapperMT< MutexRefCount * > > > ThreadSafeMutexPtr;


// ======== ThreadSafePtrLock =========================================
/**
 * ThreadSafePtrLock performs the mechanics of aquiring a lock
 * from a thread-safe ptr.  This uses the RAII idiom.
 */

class AUSTRIA_EXPORT ThreadSafePtrLock
{
    public:

    /**
     * ThreadSafePtrLock contructor will safely aquire a 
     * mutex contained within a thread-safe lock.
     */
    ThreadSafePtrLock(
        ThreadSafeMutexPtr              & i_mutex
    )
      : m_mutex( i_mutex ),      // Make a reference to the thread safe smart pointer
        m_local_mutex( i_mutex ) // atomic pointer copy happens here
    {
        // Loop until we get the lock.
        while ( 1 )
        {
            // Is there actually a mutex there ? If so, lock it
            if ( m_local_mutex )
            {
                m_local_mutex->Lock();
            }
            else
            {
                // no mutex to lock here 
                return;
            }

            // Need to check that the mutex is still the same
            // because the mutex pointer that we copied may
            // have been changed before we managed to get it locked.
            Ptr<MutexRefCount *>        l_mutex( m_mutex );

            // If the new copy of the pointer is the same as the
            // old one we have already locked, then we have
            // TRULY locked the mutex and so we're done.
            if ( m_local_mutex == l_mutex )
            {
                // very cool - we have the lock
                return;
            }

            // Nope - need to release this mutex
            m_local_mutex->Unlock();

            // and try the whole thing again on the
            // mutex we just copied.
            m_local_mutex = l_mutex.Transfer();
        }
    }

    /**
     * ThreadSafePtrLock destructor will unlock any currently
     * acquired mutex.
     */

    ~ThreadSafePtrLock()
    {
        // unlock the thing if we have the lock.
        if ( m_local_mutex )
        {
            m_local_mutex->Unlock();
        }
    }

    /**
     * IsAcquired returns true if a lock was available and
     * acquired.
     */

    bool IsAcquired() const
    {
        return m_local_mutex; 
    }

    /**
     * ReplaceMutex will replace the mutex being locked
     * with i_mutex.  i_mutex will be acquired if necessary.
     */

    void ReplaceMutex( PtrView<MutexRefCount *> i_mutex )
    {
        if ( i_mutex )
        {
            i_mutex->Lock();
        }

        m_mutex = i_mutex;

        if ( IsAcquired() )
        {
            m_local_mutex->Unlock();
        }
        
        m_local_mutex = i_mutex;
    }


    // ======== Unlock ================================================
    /**
     * Unlock will unlock this lock if already acquired.
     *
     */

    void Unlock()
    {
        if ( IsAcquired() )
        {
            m_local_mutex->Unlock();
            m_local_mutex = 0;
        }
    }

private:

    /**
     * m_mutex is a reference to the mutex
     */
    ThreadSafeMutexPtr            & m_mutex;

    /**
     * m_local_mutex is a working copy of the
     * mutex pointed to by m_mutex.
     */
    Ptr<MutexRefCount *>            m_local_mutex;

    // No copy or assignment
    ThreadSafePtrLock( const ThreadSafePtrLock & );
    ThreadSafePtrLock & operator = ( const ThreadSafePtrLock & );
};

// ======== ThreadSafePtrSwapLock =====================================
/**
 * ThreadSafePtrSwapLock performs swapping of a ThreadSafeMutexPtr
 * and a lock passwd in as i_locked_mutex. i_locked_mutex must already
 * be locked when it is passed into the constructor.
 *
 */

template<
    typename w_mutex_type
>
class ThreadSafePtrSwapLock
{
    public:

    /**
     * ThreadSafePtrSwapLock
     *
     */
    ThreadSafePtrSwapLock(
        w_mutex_type                        & i_locked_mutex,
        ThreadSafeMutexPtr                  & i_safe_mutex
    )
      : m_locked_mutex( i_locked_mutex ),
        m_safelock( i_safe_mutex )
    {
        m_locked_mutex->Unlock();
    }

    ~ThreadSafePtrSwapLock()
    {
        // need to perform locking in reverse order of aquisition
        m_safelock.Unlock();
        m_locked_mutex->Lock();
    }

    w_mutex_type                          & m_locked_mutex;
    ThreadSafePtrLock                       m_safelock;
};


// ======== ThreadSafePtrTryLock =========================================
/**
 * ThreadSafePtrTryLock performs the mechanics of aquiring a lock
 * from a thread-safe ptr and deals with a flag that will detect the
 * change.
 */

template <typename w_flag_t>
class AUSTRIA_EXPORT ThreadSafePtrTryLock
{
    public:

    /**
     * ThreadSafePtrTryLock contructor will safely aquire a 
     * mutex contained within a thread-safe lock.
     */
    ThreadSafePtrTryLock(
        ThreadSafeMutexPtr              & i_mutex,
        volatile w_flag_t               & i_flag
    )
      : m_mutex( i_mutex ),      // Make a reference to the thread safe smart pointer
        m_local_mutex( i_mutex ),// atomic pointer copy happens here
        m_flag( i_flag )
    {
        // Loop until we get the lock.
        while ( 1 )
        {
            // Is there actually a mutex there ? If so, lock it
            if ( m_local_mutex )
            {
                // Attempt to attain the lock.
                while ( ! m_local_mutex->TryLock() )
                {
                    if ( ! m_flag )
                    {
                        // flag became false - no longer want to wait
                        m_local_mutex = 0;
                        return;
                    }

                    // go ahead and yield the puppy
                    OSTraitsBase::SchedulerYield();
                }
            }
            else
            {
                // no mutex to lock here 
                return;
            }

            // Need to check that the mutex is still the same
            // because the mutex pointer that we copied may
            // have been changed before we managed to get it locked.
            Ptr<MutexRefCount *>        l_mutex( m_mutex );

            // If the new copy of the pointer is the same as the
            // old one we have already locked, then we have
            // TRULY locked the mutex and so we're done.
            if ( m_local_mutex == l_mutex )
            {
                // very cool - we have the lock
                return;
            }

            // Nope - need to release this mutex
            m_local_mutex->Unlock();

            // and try the whole thing again on the
            // mutex we just copied.
            m_local_mutex = l_mutex.Transfer();
        }
    }

    /**
     * ThreadSafePtrTryLock destructor will unlock any currently
     * acquired mutex.
     */

    ~ThreadSafePtrTryLock()
    {
        // unlock the thing if we have the lock.
        if ( m_local_mutex )
        {
            m_local_mutex->Unlock();
        }
    }

    /**
     * IsAcquired returns true if a lock was available and
     * acquired.
     */

    bool IsAcquired() const
    {
        return m_local_mutex; 
    }

    /**
     * ReplaceMutex will replace the mutex being locked
     * with i_mutex.  i_mutex will be acquired if necessary.
     */

    void ReplaceMutex( PtrView<MutexRefCount *> i_mutex )
    {
        if ( i_mutex )
        {
            i_mutex->Lock();
        }

        m_mutex = i_mutex;

        if ( IsAcquired() )
        {
            m_local_mutex->Unlock();
        }
        
        m_local_mutex = i_mutex;
    }


    // ======== Unlock ================================================
    /**
     * Unlock will unlock this lock if already acquired.
     *
     */

    void Unlock()
    {
        if ( IsAcquired() )
        {
            m_local_mutex->Unlock();
            m_local_mutex = 0;
        }
    }

private:

    /**
     * m_mutex is a reference to the mutex
     */
    ThreadSafeMutexPtr            & m_mutex;

    /**
     * m_local_mutex is a working copy of the
     * mutex pointed to by m_mutex.
     */
    Ptr<MutexRefCount *>            m_local_mutex;

    /**
     * m_flag is checked for non falseness 
     */
    volatile w_flag_t             & m_flag;


    // No copy or assignment
    ThreadSafePtrTryLock( const ThreadSafePtrTryLock & );
    ThreadSafePtrTryLock & operator = ( const ThreadSafePtrTryLock & );
};



// ======== ThreadSystemTime ==========================================
/**
 *  ThreadSystemTime returns a TimeStamp representing the current
 *  system time, derived from the same source as is used by at_thread
 *  methods that take TimeStamp arguments.
 */

TimeStamp ThreadSystemTime();



// ======== SystemCommand =============================================
/**
 * SystemCommand will create a process in the background.  The result
 * of the process start can be read by calling methods on this class
 * as well as the result of the spawned command.  The treatment of the
 * arguments is system specific.
 */

struct SystemCommandContext;
#define SystemCommandAttr_List_x( A )                                   \
        /* Detach will create a completly detached process which */     \
        /* means that the SystemCommand object will not be able  */     \
        /* to interact with the process.                         */     \
        A( SystemCommandAttr, Detach, 0 )                               \
                                                                        \
        /* Create a read/write pipe to the created process.      */     \
        A( SystemCommandAttr, Pipe, 1 )                                 \
// end of macro
// ======== File Attributes Illegal Combinations =======================
/**
 *       Attribute   Attribute   Reason
 *       Detach      Pipe        detatched processes close their stdio
 */

#define SystemCommandAttr_Illegal                   \
    AT_IllegalCombo( w_Attr, Detach,  Pipe )        \
// end of macro

AT_DefineAttribute( SystemCommandAttr, SystemCommandAttr_Illegal )

class SystemCommand
{
    public:


    // ======== SystemCommand =========================================
    /**
     * This will initiate the creation of a separate process that
     * will run in the background.
     *
     * @param i_command_name        The name of the command to execute
     * @param i_command_argv        The array of args (c-string pointers) to the args for the command.
     *                              The array of pointers must be terminated by a NULL pointer.
     *                              
     */

    SystemCommand(
        const char                  * i_command_name,
        const char                  * const i_command_argv[],
        const SystemCommandAttr     & i_attr = SystemCommandAttr()
    );

    ~SystemCommand();

    // ======== InitStatus ============================================
    /**
     * InitStatus returns true if the command was executed.
     *
     * @param o_error   If o_error is not null, a message indicating the
     *                  error is written to *o_error.
     * @return true if initialization was successful.
     */

    bool InitStatus( std::string * o_error = 0 );


    // ======== ProcessState ==========================================
    /**
     * 
     *
     */

    enum ProcessState
    {
        /**
         * Process is running
         */
        Running,

        /**
         * The process has been stopped. (it is paused)
         */
        Stopped,

        /**
         * The process has exit
         */
        Exited,

        /**
         * The process was interrupted.
         */
        Interrupted,

        /**
         * Failed indicates that the program failed
         * to be started.
         */
        Failed
    };
    

    // ======== ExitStatus ============================================
    /**
     * ExitStatus will return true if the process has completed and the
     * exit status in the exit status object passed in.  For detached
     * processes, this will return true immediatly but it is not useful.
     *
     * @param o_info is written to only in the case of ExitStatus
     *          returning Exited or Interrupted and cotains the exit
     *          code or interrupt code respectively.
     * @return true if the o_exit_info structure is filled in indicating
     *      that the process has terminated.
     */

    ProcessState ExitStatus(
        int             & o_info,
        bool              i_wait = false
    );

    // TODO  - mechanism to get an at::File to the input and output
    // pipes for this process.

    
    private:
    
    // impl needs friend
    friend struct SystemCommandContext;

    /**
     * m_task_context contains information specific to this implementation.
     */
    
    SystemCommandContext                * m_command_context;


    // not allowed to copy this class
    SystemCommand( const SystemCommand & );
    SystemCommand & operator=( const SystemCommand & );

};



}; // namespace


#endif // x_at_thread_h_x


