/*
 *
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *  at_file.h
 *
 *  at_file.h declares the following classes
 *
 *  FilePath        - A system specific File Location handler
 *  FileError       - A class to wrap File Errors
 *  FilePermission  - File Permissions and Security
 *  FileAttr        - A class to wrap File Attributes
 *  FileHandle      - A unions to wrap System Specific File Handles
 *
 *  BaseFile        - Files Base Class  
 *  RFile           - A Read only Synchronous file
 *  WFile           - A Write only Sychronous file
 *  RWFile          - A ReadWrite Synchronous file
 */

#ifndef x_at_file_h_x
#define x_at_file_h_x 1

// Austria includes
#include "at_assert.h"
#include "at_exports.h"
#include "at_buffer.h"
#include "at_types.h"
#include "at_attr_mask.h"
#include "at_twinmt_basic.h"
#include "at_factory.h"
#include "at_activity.h"
#include "at_start_up.h"
#include "at_os.h"
#include "at_types.h"

// External includes
#include <string>
#include <map>

// Austria namespace
namespace at
{

class BaseFile;
    
// ======== FilePath =============================================
/**
 *  FilePath is class for handling system specific file names and
 *  paths.
 */

class AUSTRIA_EXPORT FilePath
{
    friend class BaseFile;
    /**
     * m_name is the string version of the file name
     */
    std::string m_name;
    
    public:

    // ======== FilePath =============================================
    /**
     *  Construct a FilePath from a string.
     *
     * @param i_file_path Where the file lives
     */

    FilePath( const std::string & i_file_path )
      : m_name( i_file_path )
    {
    }

    // ======== FilePath =============================================
    /**
     *  Construct a FilePath from a string.
     *
     * @param i_file_path Where the file lives
     */

    FilePath( const char * i_file_path )
      : m_name( i_file_path )
    {
    }

    // ======== FilePath =============================================
    /**
     *  Construct a FilePath from a string.
     *
     * @param i_file_path Where the file lives
     */

    FilePath( const std::string & i_file_path, bool i_clean_it )
      : m_name( i_file_path )
    {
        if ( i_clean_it )
        {
          Clean();
        }
    }

    // ======== FilePath =============================================
    /**
     * Construct a FilePath
     */

    FilePath ()
    {
    }

    virtual ~FilePath() {};

    // ======== operator= ============================================
    /**
     * Assign a string to a FilePath.
     *
     * @param  i_file_path Where the file lives.
     * @return A reference to the modified FilePath.
     */

    FilePath& operator=( const std::string& i_file_path )
    {
        m_name = i_file_path;
        return *this;
    }

    // ======== operator= ============================================
    /**
     * Assign a string to a FilePath.
     *
     * @param  i_file_path Where the file lives.
     * @return A reference to the modified FilePath.
     */

    FilePath& operator=( const char * i_file_path )
    {
        m_name = i_file_path;
        return *this;
    }

    // ======== s_directory_separator ================================
    /**
     * The character used to separate the file path components.
     * Default value is system-specific, typically '/' on Unix systems
     * and '\' on Windows systems.
     */

    static char s_directory_separator;

    // ======== s_extension_separator ================================
    /**
     * The character used to separate the file name extension.
     * Default value is '.'.
     */

    static char s_extension_separator;

    // ======== s_has_drive_letters ==================================
    /**
     * s_has_drive_letters is true if a root based file path
     * may include a "L:" prefix.
     */

    static bool s_has_drive_letters;

    
    // ======== operator/ ============================================
    /**
     * This performs a joining of 2 file paths.
     *
     * @param  i_rhs The FilePath being appended to this.
     * @return A new FilePath
     */

    FilePath operator/( const FilePath & i_rhs ) const
    {
        if ( i_rhs.m_name.empty() ) return *this;
        if ( m_name.empty() ) return i_rhs;
        return FilePath ( m_name + s_directory_separator + i_rhs.m_name );
    }

    /**
     * This performs a joining of 2 file paths.
     *
     * @param  i_rhs The file path to append to this file path.
     * @return A new FilePath
     */
    FilePath operator/( const std::string & i_rhs ) const
    {
        if ( i_rhs.empty() ) return *this;
        if ( m_name.empty() ) return i_rhs;
        return FilePath ( m_name + s_directory_separator + i_rhs );
    }

    /**
     * This performs a joining of 2 file paths.
     *
     * @param  i_rhs The file path to append to this file path.
     * @return A new FilePath
     */
    FilePath operator/( const char * i_rhs ) const
    {
        return operator/( FilePath( i_rhs ) );
    }

    // ======== operator+ ============================================
    /**
     * This appends a string to a file path.
     *
     * @param  i_rhs The string to appended.
     * @return A new FilePath
     */
    FilePath operator+( const std::string & i_rhs ) const
    {
        return FilePath ( m_name + i_rhs );
    }

    // ======== operator< ============================================
    /**
     * This performs a string comparison of two file paths.
     *
     * @param  i_rhs The FilePath being compared.
     * @return true if *this < i_rhs and false otherwise.
     */

    bool operator<( const FilePath& i_rhs ) const
    {
        return m_name < i_rhs.m_name;
    }

    // ======== operator== ===========================================
    /**
     * This performs a string equality test of two file paths.
     *
     * @param  i_rhs The FilePath being compared.
     * @return true if *this == i_rhs and false otherwise.
     */

    bool operator==( const FilePath& i_rhs ) const
    {
        return m_name == i_rhs.m_name;
    }

    // ======== operator!= ===========================================
    /**
     * This performs a string inequality test of two file paths.
     *
     * @param  i_rhs The FilePath being compared.
     * @return true if *this != i_rhs and false otherwise.
     */

    bool operator!=( const FilePath& i_rhs ) const
    {
        return m_name != i_rhs.m_name;
    }

    // ======== String ===============================================
    /**
     * This returns the internal as a const char* 
     *
     * @return const char*
     */

    const char* String () const { return static_cast<const char*> ( m_name.c_str () ); }

    // ======== StlString ===============================================
    /**
     * This returns the internal string
     *
     * @return the string
     */

    const std::string & StlString () const { return m_name; }

    // ======== Empty ================================================
    /**
     * This tells if the path is empty.
     *
     * @return \c true if the path is empty, \c false otherwise.
     */

    bool Empty() const { return m_name.empty(); }

    // ======== Clear ================================================
    /**
     * This clears the path (making it empty).
     */

    void Clear() { m_name.clear(); }

    // ======== Head =================================================
    /**
     * This returns the file path obtained after removing the trailing
     * path component (if any).  The value of s_directory_separator is
     * used to identify path components.
     *
     * @return A new FilePath
     */

    FilePath Head() const;

    // ======== Tail ====================================================
    /**
     * This returns the file path obtained after removing all the leading
     * path components, leaving the last component only.  The value of
     * s_directory_separator is used to identify path components.
     *
     * @return A new FilePath
     */

    FilePath Tail() const;

    // ======== Root ====================================================
    /**
     * This returns the file path obtained after removing the filename
     * extension (if any), leaving the root.
     * The values of both s_directory_separator and s_extension_separator
     * are used to identify the filename extension.
     *
     * @return A new FilePath
     */

    FilePath Root() const;

    // ======== Extension ===============================================
    /**
     * This returns the filename extension.
     * The values of both s_directory_separator and s_extension_separator
     * are used to identify the filename extension.
     * 
     * @return A std::string containing the filename extension.
     * @note The character used as the extension separator (i.e.
     * s_extension_separator) is not included in the extension.
     */

    std::string Extension() const;

    // ======== Match ===================================================
    /**
     * This checks whether the file path matches the pattern argument,
     * which is a shell wildcard pattern.
     *
     * @param  i_pattern The shell wildcard pattern argument.
     * @return true if the file path matches i_pattern, false otherwise.
     */

    bool Match( const std::string i_pattern ) const;

    // ======== Clean ===================================================
    /**
     * This removes all ".." and "." elements in the path.
     * The value of s_directory_separator is used to identify path
     * components.
     *
     * @return this's reference.
     */

    FilePath & Clean();

    // ======== RelativePath ============================================
    /**
     * This returns a relative path from an absolute path.
     *
     * @param  i_path_from The path where the relative path is targetted for.
     * @param  i_path_stem The minimum path to be considered.
     * @return A new FilePath
     */

    FilePath RelativePath( const FilePath &  i_path_from,
                                     const FilePath &  i_path_stem ) const;

};

} // namespace at

namespace std
{
    
inline ostream& operator<<( ostream & i_os, const at::FilePath & i_path )
{
    if ( i_path.StlString().length() )
    {
        i_os << i_path.StlString();
    }
    else
    {
        i_os << ".";
    }
    return i_os;
}

} // namespace std

namespace at
{

// ======== CurrentDirectory ==========================================
/**
 * CurrentDirectory returns a filepath of the current directory
 *
 * @return The current directory.
 */

FilePath CurrentDirectory();


// ======== FileError ==================================================
/**
 * FileError is a statically allocated class that describes a file I/O
 * error of some kind.  Consider this a replacement for error ID's
 */
class FileErrorBasic;

class AUSTRIA_EXPORT FileError
{
    public:
        friend class FileErrorBasic; 

    virtual ~FileError() {};

    // ======== What ==================================================
    /**
     * What returns a textual description of the error.
     *
     *
     * @return a statically allocated const char * string
     * that describes the error.
     */

    virtual const char * What() const = 0;

    // ======== ErrorCode ==================================================
    /**
     * ErrorCode returns an internal ErrorCode of the error.
     *
     *
     * @return an ErrorCode for this Error
     */

    virtual long ErrorCode () const = 0;

    // ======== Operator ==  =============================================
    /**
     * Operator == Compares two FileError's
     *
     */

    virtual bool operator == ( const FileError& fe ) const = 0; 

    /**
     * s_success represents a no error situation.
     */
    static const FileError & s_success;

    /**
     * s_endOfFile EOF. A read read hit the end of the file
     */
    static const FileError & s_end_of_file;

    /**
     * s_waitingread represents an ASync state of waiting to read bytes
     */
    static const FileError & s_waiting_read;

    /**
     * s_waitingwrite represent a state of waiting to write bytes
     */
    static const FileError & s_waiting_write;

    /**
     * s_waitingopen represents a state of opening a file
     */
    static const FileError & s_waiting_open;

    /**
     * s_waitingclose represents a state of closing a file
     */
    static const FileError & s_waiting_close;

    /**
     * s_fileNotFound The named file was not found
     */
    static const FileError & s_file_not_found;

    /**
     * s_badPath The path does not exist
     */
    static const FileError & s_bad_path;

    /**
     * s_tooManyOpenFiles Ran out of file handles
     */
    static const FileError & s_too_many_open_files;

    /**
     * s_accessDenied File is read only (Called for a write operation)
     */
    static const FileError & s_access_denied;

    /**
     * s_invalidFile File is locked
     */
    static const FileError & s_invalid_file;

    /**
     * s_removeCurrentDir OS Error
     */
    static const FileError & s_remove_current_dir;

    /**
     * s_directoryFull No more files allowed in this folder
     */
    static const FileError & s_directory_full;

    /**
     * s_badSeek Seeked past end or begining of file
     */
    static const FileError & s_bad_seek;

    /**
     * s_hardIO OS hard crash
     */
    static const FileError & s_hard_io;

    /**
     * s_sharingViolation File is locked for writing
     */
    static const FileError & s_sharing_violation;

    /**
     * s_lockViolation Attempt to lock a locked file
     */
    static const FileError & s_lock_violation;

    /**
     * s_diskFull Too many Pr0n Downloads
     */
    static const FileError & s_disk_full;

    /**
     * s_bad_param Bad Parameter to a function
     */
    static const FileError & s_bad_param;

    /**
     * s_overflow buffer overflow in a write or a read
     */
    static const FileError & s_overflow;

    /**
     * s_too_many_async_calls too many outstanding read or write async calls
     */
    static const FileError & s_too_many_async_calls;

    /**
     * s_user_aborted user aborted the read or write operation
     */
    static const FileError & s_user_aborted;

    /**
     * s_already_exists Cannot create a file when that file already exists.
     */
    static const FileError & s_already_exists;

	/**
	 * TODO ... have no idea what this error is ...
	 */
	static const FileError & s_not_supported;

	/**
	 * A file was attempted to be created but it already exists
	 * probably a use of FileAttr::New
	 */
	static const FileError & s_file_exists;

    private:
    
    // Only the FileError can define new FileErrors
    FileError () {}
};

// ===============================================================
// ======== FilePermission========================================
/**
 * File Permission sets the Read/Write/Exec persmission on a file    
 *
 * Read          Read Only file
 * Executable    File is an Executable
 * Write         Write Only file
 * User          User Security Level
 * Group         Group Security Level
 * World         World (Other) Security Level
 */

class AUSTRIA_EXPORT FilePermission
{
    public:

    enum {
        UserRead        = 0x00001,
        UserWrite       = 0x00002,
        UserExecutable  = 0x00004,
        GroupRead       = 0x00010,
        GroupWrite      = 0x00020,
        GroupExecutable = 0x00040,
        WorldRead       = 0x00100,
        WorldWrite      = 0x00200,
        WorldExecutable = 0x00400
    };
};

// ======== File Attribute List ============================================
/**
*
*  Mode                     Unix              Windows             Function
*
*  Read                     O_RDONLY          GENERIC_READ        (Access) Read Only,  Defaults to 'Preserve' ( Preserves file contents )
*  Write                    O_WRONLY          GENERIC_WRITE       (Access) Write Only, Defaults to 'Preserve' ( Preserves file contents )
*  ReadWrite                O_RDWRT           GENERIC_READ |
*                                             GENERIC_WRITE       (Access) Read and Write, Defaults to 'Preserve' ( Preserves file contents )
*  Preserve                                   OPEN_EXISTING       (Disposition) Used with to Preserve File Contents (Default)
*  Create                   O_CREAT           CREATE_ALWAYS       (Disposition) Used to Either Truncate an Existing File to Zero or Create a New File.
*  Async                    O_ASYNC           FILE_FLAG_OVERLAPPED(Operation) Read and Write commands are performed Asyncronously
*  DirectIO                 O_DIRECT          FILE_FLAG_NO_BUFFERING(Operation) Defers activity to Memory and Aligns Reads and Writes on boundaries
*  Mapped                   ?                 ?                   (Operation) Create a file that is mapped to memory [[ Not supported for Phase 1 ]]
*  SyncWrite                O_SYNC            FILE_FLAG_WRITE_THROUGH (Operation) Writes to a disk happen before it returns.
*  New                      O_CREAT|O_EXCL    CREATE_NEW          (Disposition) Used to Create a New File, which must not already exist
*/

#define FileAttr_List_x( A )            \
        A( FileAttr, Read, 0 )          \
        A( FileAttr, Write, 1 )         \
        A( FileAttr, ReadWrite, 2 )     \
        A( FileAttr, Preserve, 3 )      \
        A( FileAttr, Create, 4 )        \
        A( FileAttr, Async, 5 )         \
        A( FileAttr, DirectIO, 6 )      \
        A( FileAttr, Mapped, 7 )        \
        A( FileAttr, SyncWrite, 8 )     \
        A( FileAttr, New, 9 )           \
// End                                   //                  

// ======== File Attributes Illegal Combinations =======================
/**
*       Attribute   Attribute   Reason
*       Read        Write       Please Use the Attribute ReadWrite
*       Read        Create      Reading a newly Created file is fruitless
*       Read        ReadWrite   Read already defined in ReadWrite
*       Write       ReadWrite   Write already defined in ReadWrite
*       Preserve    Create      Either you want a new file or an existing one, can't have both
*/

#define FileAttr_Illegal                          \
    AT_IllegalCombo( w_Attr, Read,  Write )       \
    AT_IllegalCombo( w_Attr, Read,  Create )      \
    AT_IllegalCombo( w_Attr, Read,  ReadWrite )   \
    AT_IllegalCombo( w_Attr, Write, ReadWrite )   \
    AT_IllegalCombo( w_Attr, Preserve, Create )   \
    AT_IllegalCombo( w_Attr, Read,  SyncWrite )   \
    AT_IllegalCombo( w_Attr, Read,  New )         \
    AT_IllegalCombo( w_Attr, New,  Preserve )     \
    AT_IllegalCombo( w_Attr, New,  Create )       \
// End                                                     //

/**
 * Define the Illegal Combinations (Creates classes)
 *
 */

AT_DefineAttribute( FileAttr, FileAttr_Illegal )

// ======== FileHandle =============================================
/**
 * FileHandle is a union used to store of different OS file handle 
 * types.
 *
 */

union AUSTRIA_EXPORT FileHandle
{
    void* handle;    // Windows File and Dir handle
    int   fildes;    // GX86 File Handle
    void* dirhandle; // GX86 Directory Handle
};

// Forward declaration
class BaseFile;

// ======== FileContext ====================================
class AUSTRIA_EXPORT FileContext
{
    public:

        virtual ~FileContext() {}

        // ======== GetRawBytes =============================================
        /**
        *       @return returns the raw bytes from inside the at::Buffer
        */
        
        virtual void* GetRawBytes  () const { return static_cast<void*>(m_buffer.Get ()->Region ().m_mem); }

        // ======== GetBufferSize =============================================
        /**
        *       @return returns the buffer 
        *       NOTE: The buffer is now invalid
        */
        
        virtual at::PtrDelegate<at::Buffer*> GetBuffer  () const { return m_buffer; }

        // ======== GetBufferSize =============================================
        /**
        *       @return returns the number of bytes that the buffer holds
        */
        
        virtual long GetBufferSize () const { return m_buffer.Get ()->Region ().m_allocated; }

        // ======== SetBuffer =============================================
        /**
        *       @param i_buffer Accepts a buffer and takes control of it
        *                       this buffer usually contains the result of a read
        */
        
        virtual void SetBuffer ( at::PtrDelegate<at::Buffer*> i_buffer ) { m_buffer = i_buffer; } 
        
        // ======== SetThis =============================================
        /**
        *       @param i_basefile The this pointer of th calling class
        */
    
        virtual void SetThis ( at::BaseFile* i_basefile ) { m_basefile = i_basefile; }
        
        // ======== GetThis =============================================
        /**
        *       @return return m_basefile
        */
    
        virtual BaseFile* GetThis () const { return m_basefile; }
        

    protected:
    
    /**
    *    m__buffer a delegate buffer used to store data during async reads
    */
    
    at::Ptr< at::Buffer * >                     m_buffer;

    /**
    *       m_basefile is a pointer to the this pointer of the calling class
    */

    at::BaseFile* m_basefile;
};

// ======== FileNotifyLeadInterface ====================================
/**
 * This describes file notification lead interface
 * This should be implemented by the user when Async
 * file system calls are required.
 */

class FileNotifyLeadInterface  
{
    protected:

    virtual ~FileNotifyLeadInterface () {}
    
    public:

    // ======== IOCompleted ================================================
    /**
    *  This is called when either a read of write operation 
    *  Complete
    *   
    * @param i_code is an application dependant code
    * @param i_fe a FileError if any
    * @return nothing
    *
    *   NOTE: o_filecontext needs to be *deleted* by the user. This context was
    *         created in the Read or Write Call.
    */
    virtual void IOCompleted ( FileContext* o_FileContext, const FileErrorBasic &i_fe ) = 0;

    // ======== Cancel ================================================
    /**
     *  
     */
    // @@ REV -- need to disambiguate between this Cancel, and Twin(LeadCancel, AideCancel)
    // @@ REV -- if it is to be separate, it should only be called with Lead/Aide "VoidCall"
    virtual void Cancel () = 0;

    /**
     *  
     */
    FileContext* m_filecontext;
};

// ======== BaseFile =============================================
/**
 * This describes the base class for various file types. This class
 * cannot be instantiated you must use one of its derivitives.
 *
 * Notes:
 * <li>This class is not re-entrant, i.e. not thread safe.  i.e. No
 * simultaneous calls to any of the methods to an object of this class
 * is supported. You must implement your own threading model.</li>
 *
 */

class AUSTRIA_EXPORT BaseFile 
{
    public:
        friend class FileInitializer;

   // ========= DateTime Options ====================================
   /**
    * DateTime Options return date info about the file       
    *
    * LastWrite    Last time File was Written
    * Created      When File was Created
    * Accessed     When File was Last Accessed
    *
    */

    enum {
        LastWrite = 1,
        Created,
        Accessed
    };

    // ===============================================================
    /**
    *      Seek Parameters
    *          SeekBegin - Seek from the begining of the file
    *          SeekCurrent - Seek from the Current position
    *          SeekEnd - Seek from the end
    *
    *          Each OS defines these values.
    */

    static const int SeekBegin; 
    static const int SeekCurrent;
    static const int SeekEnd; 

    public:

    // ======== Exists ===============================================
    /**
    *  Exists is a static Function that checks for the existence of a file.
    *
    * @return True if the File exists, False if it does not.
    *
    */

    static bool Exists ( const FilePath & i_file_path );
    
    // ======== Rename ===============================================
    /**
    *  Rename is a static Function that Changes the neame of a file.
    *
    * @return True if Rename was successful. False use FileErrorWin32 to get the error
    *
    */

    static bool Rename ( const FilePath & i_file_pathold, const FilePath & i_file_pathnew );

    // ======== HardLink =============================================
    /**
    *  HardLink creates a new hard link to an existing file.
    *
    *  @param i_file_path Existing path to the file
    *  @param i_link_path New path linking to the file
    *  @return True if creation was successful, false otherwise
    */

    static bool HardLink ( const FilePath & i_file_path, const FilePath & i_link_path );

    // ======== SymLink =============================================
    /**
    *  SymLink creates a symbolic link.  The symlink pathname must not
    *  exist or must be an existing symlink for this api to work on win32/NTFS.
    *  RemoveSymLink must also be called to remove the symbolic link for complete
    *  portability with Win32/NTFS.  Symbolic links should also
    *  be links for directories only for true win32 portability.
    *
    *  @param i_file_path Existing path to a directlory
    *  @param i_link_path New path being linked
    *  @return True if creation was successful, false otherwise
    */

    static bool SymLink ( const FilePath & i_file_path, const FilePath & i_link_path );

    // ======== ReadSymLink =============================================
    /**
    *  ReadSymLink will read the pathname in an existing symlink.
    *
    *  @param i_link_path The path to the existing link
    *  @return True if creation was successful, false otherwise
    */

    static bool ReadSymLink ( const FilePath & i_link_path, std::string & o_symlink_val );

    inline static bool ReadSymLink ( const FilePath & i_link_path, FilePath & o_symlink_val )
    {
        return ReadSymLink( i_link_path, o_symlink_val.m_name );
    }
    
    // ======== RemoveSymLink =============================================
    /**
    *  RemoveSymLink will remove an existing symlink.  For portability, it is
    *  important to use RemoveSymLink for any links created using SymLink.
    *
    *  @param i_link_path The path to the existing link
    *  @return True if creation was successful, false otherwise
    */

    static bool RemoveSymLink ( const FilePath & i_link_path );
    

    // ======== Remove ===============================================
    /**
    *  Remove is a static Function that deletes a file.
    *
    * @return True if Remove was successful. False use FileErrorWin32 to get the error
    *
    */

    static bool Remove ( const FilePath & i_file_path );

    

    // ======== Open =================================================
    /**
    *  *Atomically* Opens the File for Read or Write Access
    *  NOTE: Throws an exception if the open Fails for any reason
    *       
    * @param i_file_path The full path to the file 
    * @param i_mode Can be a combination of the modes above
    *
    * @return True for Success. False there was an error. Use *GetError*
    *         to retrieve Error information.
    */

    virtual bool Open ( const FilePath & i_file_path, const FileAttr &i_attr );

    // ========= Open ================================================
    /**
    *  *Atomically* Opens the File for Read or Write Access
    *  This version uses the filepath and mode passed to the 2 argument
    *  constructor. This open shold call the one above.
    *       
    * @return True for Success. False there was an error use *GetError*
    *  to retrieve error information
    *
    */

    virtual bool Open ();

    // ========= IsOpen ==============================================
    /**
    * @return True if the file is already Open 
    */

    virtual bool IsOpen ();

    // ========= AbortAllBlockingCalls ===================================
    /**
    *  AbortAllBlockingCalls will end *all* pending Async Calls
    *
    * @return True, call was aborted. False, there was an error use 
    *         GetError to retrieve error info.
    */

    virtual bool AbortAllBlockingCalls ();

    // ========= Flush ===============================================
    /**
    *  Flush Writes deffered/cached data to disk 
    *
    *  Notes: Only useful for files opened in DirectIO mode
    *
    * @return True, flush was successful. False, an error occured 
    *         use GetError to retrieve the error.
    */

    virtual bool Flush ();

    // ========= Close ===============================================
    /**
    *  Close - Closes an open file
    *
    *  Notes: Throws an FileError Exception if there was an Error closing the file.
    *
    * @return Nothing!
    */

    virtual void Close ();

    // ========= GetError ============================================
    /**
    *  GetError returns the last error
    *
    * @return Returns the last file error if any
    */

    virtual const FileError& GetError () const;
    
    // ========= Seek ================================================
    /**
    *  Seek seeks to the given position within a file from the offset
    *  specified.
    *
    * @param i_off  Number of bytes to seek from i_from. Use a negative 
    *               number to seek backwards. Throws and exception if you 
    *               seek past the end or begining of the file.
    *
    * @param i_from Where to start seeking from possible values are
    *           SeekBegin   Begining of the File
    *           SeekEnd     End of the File
    *           SeekCurrent The Current File Positin
    *
    * @return Returns the current position in the file
    */

    virtual Int64 Seek ( Int64 i_off, int i_from ) const;

    // ========= GetPosition =========================================
    /**
    *  GetPosition returns the current file position.
    *
    *  Notes: Be Careful using this in Async mode as the position changes dynamically
    *
    * @return Returns the current position in the file, throws an exception if there
    *         is an error.
    */

    virtual Int64 GetPosition () const;

    // ========= Setlength ===========================================
    /**
    *  SetLength changes the length of the file.
    *
    * @param i_newlen The new length of the file
    *
    *  Notes: If the length is set to shorter than the current file length
    *         data will be lost.
    *
    * @return True if the file length was changed. Other wise False use 
    *         GetError to retrieve the error.
    *
    */

    virtual bool SetLength ( Int64 i_newlen );

    // ========= GetLength ===========================================
    /**
    *  GetLength returns the current length of the file in bytes.
    *
    * @return The Length of the file in bytes.
    *
    */

    virtual Int64 GetLength () const;

    // ========= GetLength ===========================================
    /**
    *  Static version of GetLength().  Returns the length of the file
    *  at the given path argument.
    *
    * @param i_file_path The path to the file.
    *
    * @return The Length of the file in bytes.
    *
    */

    static Int64 GetLength( const FilePath & i_file_path );

    // ========= LockRange ===========================================
    /**
    *  LockRange locks a particular range of bytes in a file. If that
    *  range is accessed an exception will be thrown. [[ TODO WHICH EXCEPTION !! ]]
    *
    *  Notes: Each time you lock a region the lock count is increased.
    *
    * @param i_pos From where to start locking 
    * @param i_count Number of Bytes to lock
    * @param i_wait If true the LockRange call is required to wait
    * @return true if the region was successfully locked false otherwise.
    */

    virtual bool LockRange( Int64 i_pos, Int64 i_count, bool i_wait = false );
    
    // ========= UnLockRange =========================================
    /**
    * UnLockRange unlocks a previously locked range
    *
    * @param i_pos From where to start un-locking 
    * @param i_count Number of Bytes to unlock
    * 
    *  Notes: Each time Unlock range is called the lock count is decreased.
    *
    *  Notes: WARNING. Unlocking a range that was not previously locked or
    *         not fully unlocking a range that previously locked
    *         can lead to unpredictable results
    */

    virtual bool UnlockRange( Int64 i_pos, Int64 i_count );

    // ======== SetPermissions =======================================
    /**
    *  SetPermissions - Set the permissions of a file
    *
    * @param i_permissions select from the permissions below, or them together
    *    FilePermission::UserRead        
    *    FilePermission::UserWrite       
    *    FilePermission::UserExecutable  
    *    FilePermission::GroupRead       
    *    FilePermission::GroupWrite      
    *    FilePermission::GroupExecutable 
    *    FilePermission::WorldRead       
    *    FilePermission::WorldWrite      
    *    FilePermission::WorldExecutable 
    */

    void SetPermissions ( int i_permissions );

    // ======== GetPermissions =======================================
    /**
    *  GetPermissions - Gets the permissions of a file
    *
    * @return or'ed permissions or 0 if no permissions are set
    *    FilePermission::UserRead        
    *    FilePermission::UserWrite       
    *    FilePermission::UserExecutable  
    *    FilePermission::GroupRead       
    *    FilePermission::GroupWrite      
    *    FilePermission::GroupExecutable 
    *    FilePermission::WorldRead       
    *    FilePermission::WorldWrite      
    *    FilePermission::WorldExecutable 
    */

    int GetPermissions ();

    // ========= GetDateTime =========================================
    /**
    *  GetDateTime will procure some time informations from a file
    *     
    * @param i_flag Can one of the following 
    *        <b>Create</b> Creation date/time of file
    *        <b>LastWrite</b> Last modification date/time of file
    *        <b>Accessed</b> Last access date/time of file
    *
    * @param o_timestamp will contain the date/time info if successful
    *
    * @return true success, false an error
    */

    bool GetDateTime ( int i_flag, TimeStamp & o_timestamp ) const;     

    // ========= GetDateTime =========================================
    /**
    *  Static version of GetDateTime.  Returns time information for
    *  the file at the given path argument.
    *     
    * @param i_file_path The path to the file.
    * @param i_flag Can one of the following 
    *        <b>Create</b> Creation date/time of file
    *        <b>LastWrite</b> Last modification date/time of file
    *        <b>Accessed</b> Last access date/time of file
    *
    * @param o_timestamp will contain the date/time info if successful
    *
    * @return true success, false an error
    */

    static bool GetDateTime(
        const FilePath & i_file_path,
        int i_flag,
        TimeStamp & o_timestamp
    );


    // ======= GetFileHandle =========================================
    /**
    *  GetFileHandle returns the internal file handle
    *
    * @return The Internal FileHandle 
    *
    */

    virtual FileHandle GetFileHandle () const { return m_filehandle; }

    // ======= ~BaseFile ========================================================
    virtual ~BaseFile ();

    protected:

    /**
    *  BaseFile Constructs a BaseFile Object. A BaseFile object cannot be 
    *  instantiated.
    *
    * @param i_file_path The full path to the file 
    * @param i_attr File Attributes 
    *
    */

    BaseFile ( const FilePath & i_file_path, const FileAttr &i_attr );

    /**
    *  BaseFile Constructs a BaseFile. 
    *
    *  Notes: The Two Argument Open function should be 
    *         used in conjunction with this constructor.
    *
    */

    BaseFile ();

    /**
    *  SetError Converts a long to a FileError and Stores it
    *
    * @param i_error the error code
    */

    void SetError ( long i_error );

    /**
    *  SetError ( const FileError& i_error  ) Stores the 
    *  last error
    *
    * @param i_error the error object
    */

    void SetError ( const FileError& i_error );

    /**
    *  CheckAttributes Dynamically checks a files open attributes 
    *
    * @param i_attr The stored internal open attributes
    * @return True if attributes are valid. False there is an invalid combination
    */

    virtual bool CheckAttributes ( FileAttr *i_attr );

    protected:

    /**
    *  File Handle
    */
    FileHandle m_filehandle; 

    /**
    *  Mode the file was opened in
    */
    FileAttr m_mode;

    /**
    *  Absolute path name
    */
    FilePath m_fullname;   

    /**
    *  Last error that occured
    */
    FileError* m_lasterror; 

    /**
    *  Permissions on this file 
    */
    int m_permission;

    /**
    *  Close when this Object is destructed?
    */
    bool m_closeondelete;

    /**
    *  Mutex used for MT LeadAide
    */
    Ptr< MutexRefCount * > m_mutex;

    /**
     *  m_filelock is used to lock the BaseFile static functions
     */
    static at::Mutex m_filelock;

    /**
    *  Number of locked region calls pending.  protect with above mutex
    */
    long m_locked;
};

// ======== RFile =============================================
/**
 *  An RFile is a *Read Only* file. Use an RFile to open
 *  a file specifically for reading. This is a virtual Base 
 *  class as RWFile multiply inherts from it.
 *
 */

class AUSTRIA_EXPORT RFile : virtual public BaseFile
{
    public:

    // ======= RFile =================================================
    /**
    *  Constructs a Reader File Object
    *
    * @param i_file_path The full path to the file 
    * @param i_attr FileAttr::Read | FileAttr::Preserve by default
    */

    RFile ( const FilePath & i_file_path, const FileAttr &i_attr = FileAttr::Read | FileAttr::Preserve );

    // ======= RFile =================================================
    /**
    *  RFile Constructs a RFile Object. Uses FileAttr::Read and FileAttr::Preserve 
    *  by default
    */

    RFile ();

    // ======= ~RFile =================================================
    /**
    *  Destructor does nothing
    */

    virtual ~RFile () {};

    // ======= Read ===================================================
    /**
    *  Read Fills a buffer with a certain number of bytes read from a file
    *
    * @param i_br A buffer allocated by the caller. The size parameter is 
    *             read from the buffer and that number of bytes is read. 
    *             Use GetLength to find the size of the file and use that 
    *             return the entire contents of the file.
    *
    * @return bool True: Returns with the specified buffer containing the bytes 
    *              within the file. 
    *              False means there was an error use GetError to retrieve the
    *              Error.
    */

    virtual bool Read ( at::PtrView< Buffer * > i_br );

    // ======= Read ===================================================
    /**
    *  Attempts to fill up a memory region with bytes read from the file.
    *
    *  This method attempts to read up to
    *    <tt> i_br.m_max_available - i_br.m_allocated </tt>
    *  bytes into the memory area starting at
    *    <tt> i_br.m_mem + i_br.m_allocated </tt>.
    *  On success, the number of allocated (used) bytes in the memory
    *  region (<tt>i_br.m_allocated</tt>) is augmented by the number of
    *  bytes read.
    *
    * @param i_br A memory region allocated by the caller.
    *
    * @return \p true on success, \p false on error, and GetError() can
    *         be called to retrieve the error.
    */

    virtual bool Read ( Buffer::t_Region& i_br );

    // ======== ReadAsync  =============================================
    /**
    *  @param i_br A buffer that will be filled with the read information
    *              the size of this buffer also contains how much to read.
    *  @param i_lead This is a lead class that implements the FileNotifyLeadInterface
    *                The function IOCompletion is called when the Read is complete.
    *  @param i_off Position to seekto prior to reading. -1 by default (No Seek)
    *  @param i_from Position to seek from SeekBegin by default.
    *
    *   Notes: WARNING Do not use the passed in buffer until this call is complete
    *          You may call i_lead::Cancel to cancel this call. [[ TO DO ]]
    */

    virtual bool ReadAsync ( at::PtrDelegate< Buffer* > i_br, 
                             AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead,
                             Int64 i_off = -1,
                             int i_from = BaseFile::SeekBegin ); 

    // ======== Open =================================================
    /**
    *  Open *Atomically* Opens the File for Read Access using 
    *  File Attributes FileAttr::Read | FileAttr::Preserve by Default
    *       
    * @param i_file_path The full path to the file 
    * @param i_attr FileAttr::Read | FileAttr::Preserve by default
    *
    * @return True for Success. False there was an error use *GetError*.
    */

    virtual bool Open ( const FilePath & i_file_path,
                        const FileAttr & i_attr = FileAttr::Read | FileAttr::Preserve );

    // ======== Open =================================================
    /**
    *  Calls BaseFile::Open
    *  This open is a companion to the two argument Constructor
    */

    virtual bool Open ();

    protected:

    /**
    *  CheckAttributes Dynamically check file attributes for a read file
    *
    *  @param i_attr The mode passed to open or the constructor
    */

    virtual bool CheckAttributes ( FileAttr *i_attr );
};

// ======== WFile =============================================
/**
 *  WFile is a Write only file. Use a WFile if you want to create
 *  a new file or open a file for wrting but not reading.
 */

class AUSTRIA_EXPORT WFile : virtual public BaseFile
{
    public:

    // ======== WFile ================================================
    /**
    *  WFile Constructs a WFile Object. File Attributes FileAttr::Write 
    *  and FileAttr::Preserve are set by default
    * 
    *  Notes: to create a file pass FileAttr::Create to this Constructor
    *
    * @param i_file_path The full path to the file 
    * @param i_attr File Attributes FileAttr::Write and FileAttr::Preserve
    *
    */

    WFile ( const FilePath & i_file_path, const FileAttr &i_attr = FileAttr::Write | FileAttr::Preserve );

    // ===============================================================
    /**
    *  WFile Constructs a WFile Object. Uses File Attributes 
    *  FileAttr::Write and FileAttr::Preserve.
    *
    *  Notes: Use the two parameter Open function with this constructor
    */

    WFile ();

    virtual ~WFile() {};

    // ========== Write ==============================================
    /**
    *  Write - Write the bytes in a buffer to the file
    *
    * @param - i_br A buffer allocated by the caller. The size parameter 
    *               is read from the buffer and that number of bytes is
    *               written. Use Seek to position the file point at a specific 
    *               location within the file before writing.
    *
    *  Notes: If the number of bytes written is less than the number requested
    *         an s_disk_full FileError is thrown.
    *
    * @return Writes the buffer then returns the number of bytes written. 
    *         If there is an error then -1 is returned.
    */

    virtual long Write ( at::PtrView< Buffer * > i_br );

    // ========== Write ==============================================
    /**
    *  Write - Write the bytes from a memory region to the file.
    *
    *  This method writes up to <tt>i_br.m_allocated</tt> bytes to
    *  the file from the memrory area starting at <tt>i_br.m_mem</tt>
    *  and returns \p true if it succeeds writing all the bytes.
    *
    * @param - i_br A memory region allocated by the caller.
    *
    * @return \p true on success, \p false on error, and GetError() can
    *         be called to retrieve the error.
    *
    * @note Use Seek to position the file point at a specific location
    *       within the file before writing.
    */

    virtual bool Write ( const Buffer::t_Region& i_br );

    // ========== Write ==============================================
    /**
    *  Write - Write the bytes from a memory region to the file.
    *
    *  This method writes up to <tt>i_br.m_allocated</tt> bytes to
    *  the file from the memrory area starting at <tt>i_br.m_mem</tt>
    *  and returns \p true if it succeeds writing all the bytes.
    *
    * @param - i_br A memory region allocated by the caller.
    *
    * @return \p true on success, \p false on error, and GetError() can
    *         be called to retrieve the error.
    *
    * @note Use Seek to position the file point at a specific location
    *       within the file before writing.
    */

    virtual bool Write ( const Buffer::t_ConstRegion& i_br );

    // ======== WriteAsync  =============================================
    /**
    *  @param i_br A buffer that will be used to write data. The length of the buffer 
    *              is used.
    *  @param i_lead This is a lead class that implements the FileNotifyLeadInterface
    *                The function IOCompletion is called when the Write is complete.
    *
    *   Notes: WARNING Do not use the passed in buffer until this call is complete
    *          You may call i_lead::Cancel to cancel this call. [[ TO DO ]]
    */

    virtual bool WriteAsync ( const at::PtrDelegate< Buffer * > i_br, 
        AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer    i_lead );

    // ========== Open ===============================================
    /**
    *  Open *Atomically* Opens the File for Write Access.
    *       
    * @param i_file_path The full path to the file 
    * @param i_attr File Atributes FileAttr::Write and FileAttr::Preserve 
    *               are used 
    *
    * @return True for Success. False there was an error use *GetError*.
    */

    virtual bool Open ( const FilePath & i_file_path, const FileAttr &i_attr = FileAttr::Write | FileAttr::Preserve );

    // ========= Open ================================================
    /**
    *  Calls BaseFile::open
    *  This open is a companion to the two argument Constructor
    */

    virtual bool Open ();

    protected:

    /**
    *    CheckAttributes Dynamically check a write files attributes 
    *
    *    @param i_attr The mode passed to Open or the Constructor
    */

    virtual bool CheckAttributes ( FileAttr *i_attr );
};

// ======== RWFile =============================================
/**
 *  An RWFile is a Read and Write File.
 */

class AUSTRIA_EXPORT RWFile : public RFile, public WFile
{
    public:

    // ====== RWFile =================================================
    /**
    *  RWFile Constructs a read/write file. Attributes FileAttr::ReadWrite 
    *         | FileAttr:Preserve are set by default
    *
    * @param i_file_path The full path to the file 
    * @param i_attr FileAttr::ReadWrite | FileAttr::Preserve by default
    */

    RWFile ( const FilePath & i_file_path, const FileAttr &i_attr = FileAttr::ReadWrite | FileAttr::Preserve );

    // ======= RWFile ================================================
    /**
    *  RWFile Constructs an RWFile Object. Attributes FileAttr::ReadWrite
    *         | FileAttr:Preserve are set by default.
    *
    *  Notes: This is a companion to the Two argument Open
    */

    RWFile ();

    virtual ~RWFile() {};

    // ======= Open ==================================================
    /**
    *  Open Atomically* Opens the File for Read/Write Access using 
    *  
    *  Notes: Companion to the Default Constructor.
    *       
    * @param i_file_path The full path to the file 
    * @param i_attr File Attributes, FileAttr::ReadWrite | FileAttr::Preserve 
    *        by default
    *
    * @return True for Success. False there was an error use *GetError*.
    */

    virtual bool Open ( const FilePath & i_file_path, const FileAttr &i_attr = FileAttr::ReadWrite | FileAttr::Preserve );

    // ======= Open ==================================================
    /**
    *  This open is a companion to the two argumment Constructor
    *       
    * @return True for Success. False there was an error use *GetError*.
    */

    virtual bool Open ();

    protected:
    /**
    *  CheckAttributes Dynamically checks a RWFile files attributes 
    *
    * @param i_attr the mode passed to open or the constructor
    */

    virtual bool CheckAttributes ( FileAttr *i_attr );
};


// ======== CreatePath ================================================
/**
 * CreatePath will create all non-existant directories right up to
 * but not including the filename.  It does this by checking existance
 * of the path name.
 *
 * Ex 1)
 *      e.g. i_path_from = "/a/b/c/f.txt"
 *           i_path_stem = "/a/b"
 *
 * if "/a/b" does not exist, CreatePath will fail.
 *
 * Ex 2)
 *      
 *      e.g. i_path_from = "/x/y/z/f.txt"
 *           i_path_stem = "/a/b"
 *
 * i_path_from is not an extension of i_path_stem and so CreatePath will fail
 *
 * throws at::ExceptionDerivation<DirectoryCreateFailed> if an attempt
 * to create a directory failed.
 *
 * @param i_path_from Is the file name whose path is needed
 * @param i_path_stem Is the last path that will be considered.
 * @param i_path_is_dir indicates that i_path_from is supposed to be a
 *              directory so no removal of the file name is needed.
 * @return True if successful, false otherwise.
 */

struct DirectoryCreateFailed;

bool CreatePath(
    const FilePath &  i_path_from,
    const FilePath &  i_path_stem,
    bool              i_path_is_dir = false
);


// ======== SetBinaryMode =============================================
/**
 * SetBinaryMode sets the binary mode on a file descriptor.  This
 * is needed for applications that read and write binary files to stdin or
 * stdout.  On Unix systems this does nothing, on windows it calls. 
 *     int _setmode ( int handle, int mode );
 *
 * @param i_filedes
 * @return nothing
 */

bool SetBinaryMode( int i_filedes, bool i_set_binary = true );


// ======== FileRemover ===============================================
/**
 * FileRemover will remove a file on the destruction of this object.
 * Useful as a cleanup for temporary files.
 *
 */

class AUSTRIA_EXPORT FileRemover
{
    public:

    /**
     * FileRemover constructor takes a file name.
     *
     */
    inline FileRemover( const FilePath & i_filename )
      : m_filename( i_filename )
    {
    }

    inline ~FileRemover()
    {
        BaseFile::Remove( m_filename );
    }

    /**
     * m_filename - the filename of the path to remove.
     */
    FilePath                m_filename;

};


// ======== FileLockRange =============================================
/**
 * FileLockRange will attempt to lock a range of a file and unlock
 * the same range upon destruction if the lock was successful.
 */

class AUSTRIA_EXPORT FileLockRange
  : public NonCopiable
{
    public:

    /**
     * FileLockRange will lock the file upn creation and unlock upon destruction.
     *
     */
    FileLockRange(
        BaseFile                   & i_file,
        Int64                         i_pos,
        Int64                         i_count,
        bool                          i_wait = false,
        bool                          i_defer = false
    )
     :  m_file( i_file ),
        m_pos( i_pos ),
        m_count( i_count ),
        m_is_locked( (!i_defer) && i_file.IsOpen() ? m_file.LockRange( m_pos, m_count, i_wait ) : false )
    {
    }

    ~FileLockRange()
    {
        Unlock();
    }


    // ======== IsLocked ==============================================
    /**
     * IsLocked can be used to enquire wether the file is locked.
     *
     *
     * @return True if the file is locked.
     */

    bool IsLocked()
    {
        return m_is_locked;
    }

    // ======== Unlock ================================================
    /**
     * Unlock will unlock the region.
     *
     *
     * @return True if the region was previously locked.
     */

    bool Unlock()
    {
        if ( m_is_locked && m_file.IsOpen() )
        {
            m_is_locked = false;
            return m_file.UnlockRange( m_pos, m_count );
        }
        return m_is_locked = false;
    }

    // ======== Lock ==================================================
    /**
     * Lock will attempt to lock the region if it is not currently locked.
     *
     * @param i_wait True if call is to wait for availability of lock
     * @return nothing
     */

    bool Lock( bool i_wait = false )
    {
        if ( m_file.IsOpen() )
        {
            if ( ! m_is_locked )
            {
                return m_is_locked = m_file.LockRange( m_pos, m_count, i_wait );
            }

            return true;
        }
        else
        {
            return m_is_locked = false;
        }
    }

    /**
     * m_file - the file being locked.
     */
    BaseFile                        & m_file;
    Int64                             m_pos;
    Int64                             m_count;
    bool                              m_is_locked;

};



}; // namespace

//
// AT_FILE_H is defined in at_os.h and provides a platform specific
// header file name. (at_gx86_file.h or at_win32_file.h)
#include AT_FILE_H

#endif // x_at_file_h_x


