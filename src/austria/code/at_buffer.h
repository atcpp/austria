//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_buffer.h
 *
 */

#ifndef x_at_buffer_h_x
#define x_at_buffer_h_x 1

#include "at_exports.h"
#include "at_exception.h"
#include <string>
#include <cstring>

// Austria namespace
namespace at
{


// ======== BufferRegion ==============================================
/**
 * BufferRegion describes a memory region.  Basically it is a pointer
 * to the beginning of memory and a length with a concept of
 * "used" so that the relevant chunk of the buffer is well known
 *
 */

template <typename w_Type>
class BufferRegion
{
    public:

    /**
     * value_type is the base type of this memory region
     */
    typedef w_Type                                  value_type;
    typedef typename NonConst<w_Type>::t_Type       t_non_const_value_type;

    // ======== BufferRegion ==========================================
    /**
     * Construct an empty buffer region.
     *
     * @return nothing
     */

    BufferRegion()
      : m_mem( 0 ),
        m_allocated( 0 ),
        m_max_available( 0 )
    {
    }


    // ======== BufferRegion ==========================================
    /**
     * Construct a buffer region from various components.
     *
     * @param i_mem Pointer to the beginning of the region
     * @param i_allocated The number of bytes used in this region
     * @param i_max_available The total number of bytes allocated
     * @return nothing
     */

    BufferRegion(
        w_Type                                  * i_mem,
        SizeMem                                   i_allocated,
        SizeMem                                   i_max_available
    )
      : m_mem( i_mem ),
        m_allocated( i_allocated ),
        m_max_available( i_max_available )
    {
    }
    
    // ======== BufferRegion ==========================================
    /**
     * Construct a buffer region from std::string.
     *
     * @param i_str std::string to be converted 
     * @return nothing
     */

    BufferRegion(
        const std::basic_string<typename NonConst<w_Type>::t_Type>         &i_str
    )
      : m_mem( i_str.data() ),
        m_allocated( i_str.length() ),
        m_max_available( i_str.length() )
    {
    }

    
    // ======== BufferRegion ==========================================
    /**
     * Construct a BufferRegion<T> from a non BufferRegion< NonCost<T>::w_Type > .
     * This is a case where conversion from a non-const region is being
     * made to a const region although it also works as a copy
     * constructor.
     *
     * @param i_rhs The 
     * @param i_allocated The number of bytes used in this region
     * @param i_max_available The total number of bytes allocated
     * @return nothing
     */

    BufferRegion(
        const BufferRegion< t_non_const_value_type > & i_rhs
    )
      : m_mem( i_rhs.m_mem ),
        m_allocated( i_rhs.m_allocated ),
        m_max_available( i_rhs.m_max_available )
    {
    }


    // ======== Compare ===============================================
    /**
     * Compare will return the same result as memcmp would if applied to
     * the buffers.
     *
     * @param i_rhs The rhs buffer region being compared
     * @return (like memcmp)
     */

    int Compare(
        const BufferRegion< t_non_const_value_type > & i_rhs
    ) const
    {
        SizeMem         l_minsize = i_rhs.m_allocated < m_allocated ? i_rhs.m_allocated : m_allocated;

        int l_result = ::memcmp( m_mem, i_rhs.m_mem, l_minsize );

        if ( l_result )
        {
            return l_result;
        }

        if ( i_rhs.m_allocated == m_allocated )
        {
            return 0;
        }

        if ( m_allocated < i_rhs.m_allocated )
        {
            return -1;
        }
        return 1;
    }

    
    // ======== String ================================================
    /**
     * Convert BufferRegion into a std::string.  This will likely only
     * work when w_Type is char. 
     *
     * @return A newly constructed string
     */

    std::string String() const
    {
        return std::string( m_mem, m_mem + m_allocated );
    }
    
    /**
     * m_mem points to the first element of the reserved data
     */
    w_Type                                    * m_mem;

    /**
     * m_allocated indicates the number of elements that are
     * spoken for in the region.
     */
    SizeMem                                     m_allocated;

    /**
     * m_max_available are the maximum number of elements allocated
     * to this region.
     */
    SizeMem                                     m_max_available;

    
};


// ======== Buffer_ConversionFailure ==================================
/**
 * ExceptionDerivation<Buffer_ConversionFailure> is thrown when
 * not enough memory is available for performing the conversion.
 *
 */

struct Buffer_ConversionFailure {};

// ======== Buffer_BadAllocation ======================================
/**
 * Buffer_BadAllocation is an exception class used with AT_ThrowDerivation.
 *
 * ExceptionDerivation<Buffer_BadAllocation> is thrown whenever
 * a buffer memory allocation fails.
 */

struct Buffer_BadAllocation {};


// ======== InsecureConversionProxy ===================================
/**
 * InsecureConversionProxy is used to convert a BufferRegion into
 * a destination type.  This allows a transparent type-cast
 * and performs all the correct pointer conversion.<br>
 * WARNING - this bypasses compiler type safety.  Use only if
 * the client code ensures correctness of the conversion.
 */

template <typename w_Type >
class InsecureConversionProxy
{
    public:

    // ======== InsecureConversionProxy ===============================
    /**
     * Constructor for InsecureConversionProxy takes a region.
     *
     * @param i_region To be type-cast
     */

    InsecureConversionProxy(
        const BufferRegion< w_Type >          & i_region,
        SizeMem                                 i_byte_offset = 0
    )
      : m_region( i_region ),
        m_byte_offset( i_byte_offset )
    {
    }


    // ======== operator<T> T =========================================
    /**
     * The conversion operator. In theory, this conversion may
     * be able to perform a conversion to other than pointer types
     * if only an appropriate constructor could be determined.
     *
     * @return A pointer
     */

    template <typename w_T>
    operator w_T ()
    {

        // If the allocated size is not large enough - throw a Buffer_ConversionFailure
        if ( SizeMem(m_region.m_allocated * sizeof( w_Type )) < SizeMem( m_byte_offset + DereferenceTraits< w_T >::m_sizeof ) )
        {
            AT_ThrowDerivation( Buffer_ConversionFailure, "Buffer region too small to perform requested conversion" );
        }

        // perform the conversion
        return static_cast< w_T >(
            static_cast< void * >( 
                m_byte_offset + static_cast< char * >(
                    static_cast< void * >( m_region.m_mem )
                )
            )
        );
    }

    private :

    /**
     * The buffer region being converted
     */
    
    BufferRegion< w_Type >                      m_region;

    /**
     * The offset (in bytes) into the buffer region.
     */
    
    SizeMem                                     m_byte_offset;
};


// ======== Buffer ====================================================
/**
 * This defines the basic buffer interface.  Buffers may be implemented
 * in various ways and may not support some of the features.
 *
 */

class Buffer
  : public virtual PtrTarget
{
    public:

    /**
     * t_BaseType is the base type of the buffer (char).
     */
    typedef char                                t_BaseType;

    /**
     * t_Region is the non-const region type that is returned
     * from Buffer::Region().
     */
    typedef BufferRegion< t_BaseType >          t_Region;
    typedef BufferRegion< const t_BaseType >    t_ConstRegion;

    // ======== Region ================================================
    /**
     * Region will return the buffer's memory as a region.  The memory
     * is managed internally as a region.
     *
     * Calls that may change the internal allocation of memory may
     * invalidate the region.
     *
     * @return A reference to the internal region
     */

    virtual const t_Region & Region() = 0;

    // ======== Region ================================================
    /**
     * Region (const) will return the buffer's memory as a region. 
     *
     * Calls that may change the internal allocation of memory may
     * invalidate the region.
     *
     * @return A region with a const base type - indicating that buffer
     *          contents may not be modified.
     */

    virtual const t_ConstRegion Region() const = 0;


    // ======== SetAvailability =======================================
    /**
     * SetAvailability will set the amount of memory available.  This
     * may reallocate the buffer.  The returned region will contain
     * the new buffer location.  If the new size is less than the
     * currently allocated memory, the allocated region is truncated.
     *
     * @param i_new_size Is the new available size for the buffer
     * @return The new buffer region
     */

    virtual const t_Region & SetAvailability(
        SizeMem                 i_new_size
    ) = 0;


    // ======== SetAllocated ==========================================
    /**
     * SetAllocated set the allocated region.  If the current region
     * is too small, it is grown, which may cause a reallocation of
     * the memory.
     *
     * @param i_new_size
     * @return The new buffer region
     */

    virtual const t_Region & SetAllocated(
        SizeMem                 i_new_size
    ) = 0;


    // ======== RegionExtend ==========================================
    /**
     * RegionExtend will extend the availablity of the message
     * to accomodate 
     *
     * @param i_min_unused is the minumum number of bytes to be set to the
     *              availablility.
     * @param i_surplus_allocation is the number of bytes above
     *              i_min_unused to set availability if a reallocation
     *              is required.
     * @return The current region.
     */

    const t_Region & RegionExtend(
        SizeMem                 i_min_unused,
        SizeMem                 i_surplus_allocation
    ) {
        const t_Region & l_region = Region();

        // Check to see if there are enough bytes available.
        if ( i_min_unused <= l_region.m_max_available - l_region.m_allocated )
        {
            return l_region;
        }

        return SetAvailability(
            i_surplus_allocation
            + i_min_unused
            + l_region.m_allocated
        );
    }
    

    // ======== IncrementalAllocate ===================================
    /**
     * IncrementalAllocate will allocate memory and return a pointer
     * to the location of the newly allocated location in the buffer.
     *
     * @param i_new_size The size to incrementally allocate
     * @return Pointer to the first location of the newly allocated i_new_size
     *          sized region.
     */

    virtual t_BaseType * IncrementalAllocate(
        SizeMem                 i_new_size
    ) = 0;

    // ======== Append ================================================
    /**
     * Append will append data at the end of a buffer and grow the 
     * buffer appropriately
     *
     * @param i_data Pointer to the data being appended
     * @param i_length Length of the data being appended
     * @return nothing
     */

    void Append(
        const t_BaseType        * i_data,
        SizeMem                   i_length
    ) {
        std::memcpy(
            IncrementalAllocate( i_length ),
            i_data,
            i_length
        );
    }


    // ======== Append ================================================
    /**
     * Append( std::string ) will append the contents of a string
     * to this buffer.
     *
     * @param i_str The string being added
     * @return nothing
     */

    void Append(
        const std::string       & i_str
    ) {
        Append( i_str.data(), SizeMem( i_str.length() ) );
    }

    // ======== Append ================================================
    /**
     * Append( BufferRegion )
     *
     * @param i_buffer The buffer to be added
     * @return nothing
     */

    template <typename w_InType>
    void Append(
        const BufferRegion< w_InType > & i_breg
    ) {        

        // could be appending to self - need to
        // get the length now and later get the
        // pointer because IncrementalAllocate
        
        const t_Region      * l_region = & ( this->Region() );
        SizeMem               l_length = i_breg.m_allocated;
            
        const void          * l_src = static_cast< const void * >( i_breg.m_mem );

        t_BaseType          * l_new_ptr;
        
        if ( static_cast< void * >( l_region->m_mem ) == l_src )
        {
            // self assignment ... may need to move the
            // src pointer after IncrementalAllocate.
            l_new_ptr = IncrementalAllocate( l_length );
            l_src = static_cast< void * >( this->Region().m_mem );
        }
        else
        {
            l_new_ptr = IncrementalAllocate( l_length );
        }


        // now we can copy the data...
        std::memcpy(
            l_new_ptr,
            l_src,
            l_length
        );
    }

    // ======== operator+= ============================================
    /**
     * This will append another buffer to this buffer.
     *
     * @param i_buffer Is the buffer being copied from
     * @return nothing
     */

    void operator+=(
        const Buffer & i_buffer
    ) {
        // This strange sequence of operations deals with self
        // append.
        SizeMem i_size = i_buffer.Region().m_allocated;

        t_BaseType * l_new_location = IncrementalAllocate( i_size );

        memcpy( l_new_location, i_buffer.Region().m_mem, i_size );
    }

    // ======== Set ===================================================
    /**
     * Set the bytes in the region to the given values.  This
     * will overwrite the given region.
     *
     * @param i_data Pointer to the data to be copied into the buffer
     * @param i_length The length of the data to be copied
     * @return nothing
     */

    inline void Set(
        const t_BaseType        * i_data,
        SizeMem                   i_length
    ) {

        const BufferRegion< t_BaseType > & l_breg = SetAllocated( i_length );
        std::memcpy( l_breg.m_mem, i_data, i_length );
    }


    // ======== Set ===================================================
    /**
     * Set( std::string ) will assign the contents of a string to
     * this buffer.
     *
     * @param i_str The string being added
     * @return nothing
     */

    inline void Set(
        const std::string         i_str
    ) {
        Set( i_str.data(), SizeMem( i_str.length() ) );
    }


    // ======== Erase =================================================
    /**
     * Erase will remove a region of the buffer.
     *
     * @param i_from is the first character in the region to
     *              be erased.
     * @param i_to is one beyond the last character in the region
     *              to be erased.
     * @return nothing
     */

    void Erase(
        SizeMem                   i_from,
        SizeMem                   i_to
    ) {

        if ( i_from >= i_to )
        {
            // not much to do
            return;
        }

        SizeMem l_size = i_to - i_from;

        const t_Region & l_breg = Region();

        if ( i_from >= l_breg.m_allocated )
        {
            // not much to do
            return;
        }

        if ( i_to > l_breg.m_allocated )
        {
            i_to = l_breg.m_allocated;
        }

        // move the contents
        std::memmove(
            l_breg.m_mem + i_from,
            l_breg.m_mem + i_to,
            l_breg.m_allocated - i_to
        );

        AT_Assert( Region().m_allocated - l_size >= 0 );

        SetAllocated( Region().m_allocated - l_size );

    }

    // ======== Erase =================================================
    /**
     * Erase will remove a region of the buffer to the end of
     * the contents
     *
     * @param i_from is the first character in the region to
     *              be erased.
     * @return nothing
     */

    void Erase(
        SizeMem                   i_from
    ) {

        Erase( i_from, Region().m_allocated );
    }

    // ======== Erase =================================================
    /**
     * Erase will all the contents of a region
     *
     * @return nothing
     */

    void Erase()
    {
        SetAllocated( 0 );
    }

    // ======== Set ===================================================
    /**
     * Set( BufferRegion )
     *
     * @param i_buffer The buffer to be assigned to this
     * @return nothing
     */

    template <typename w_InType>
    void Set(
        const BufferRegion< const w_InType > & i_breg
    ) {

        // Self append is not allowed.
        AT_Assert(
            static_cast<const void *>( i_breg.m_mem )
            != static_cast<const void *>( Region().m_mem )
        );
        
        Set(
            reinterpret_cast<const t_BaseType *>( i_breg.m_mem ),
            sizeof( * i_breg.m_mem ) * i_breg.m_allocated
        );
    }

    // ======== operator= =============================================
    /**
     * This will assign another buffer to this buffer.
     *
     * @param i_buffer Is the buffer being copied from
     * @return nothing
     */

    void operator=(
        const Buffer & i_buffer
    ) {
        // This strange seqence of operations deals with self
        // assign.

        SizeMem l_size = i_buffer.Region().m_allocated;
        
        const BufferRegion< t_BaseType > & l_breg = SetAllocated( l_size );
        
        std::memcpy( l_breg.m_mem, i_buffer.Region().m_mem, l_size );
    }


    // ======== InsecureAssign ========================================
    /**
     * InsecureAssign allows the construction of objects within the
     * buffer memory.  This is unsafe because this should only be
     * performed using POD types, or types that will cope with
     * being moved.  In other words, don't use this unless you're
     * absolutely sure you need to and you know nothing bad will
     * happen.
     *
     * @param i_value           The value being assigned
     * @param i_byte_offset     The offset into the buffer where the varible
     *                          will be copy constructed.
     * @return A reference to the object within the buffer
     */

    template <typename w_AssignType>
    typename NonConst<w_AssignType>::t_Type & InsecureAssign(
        w_AssignType                & i_value,
        SizeMem                       i_byte_offset = 0
    ) {

        typedef typename NonConst<w_AssignType>::t_Type t_ReturnType;

        const BufferRegion< t_BaseType > * l_breg = & Region();

        SizeMem l_min_size = ( sizeof( w_AssignType ) + i_byte_offset );
        if ( l_breg->m_allocated < l_min_size )
        {
            l_breg = & SetAllocated( l_min_size );
        }

        // perform copy construction using placement new
        t_ReturnType * l_ptr = new ( i_byte_offset + l_breg->m_mem ) t_ReturnType( i_value );

        return * l_ptr;
    }


    // ======== InsecureCast ==========================================
    /**
     * InsecureCast will return any pointer being assigned to.  The buffer
     * will require that the memory required is allocated, i.e. the object
     * should already exist within the buffer.
     *
     * @param i_byte_offset     The offset into the buffer where the varible
     *                          will be cast from.
     * @return This returns a conversion proxy that will be used to
     *          identify the desired result type.
     */

    InsecureConversionProxy<t_BaseType> InsecureCast(
        SizeMem                       i_byte_offset = 0
    ) {

        /**
         * The conversion proxy does all the magic.
         */
        return InsecureConversionProxy< t_BaseType >( Region(), i_byte_offset );
    }



    // ======== The STL-like interface ================================
    // Below are some methods that are reminiscent of the STL, for those of
    // us who can't be bothered to remember more than one style of
    // interface for things.

    typedef SizeMem size_type;

    const t_BaseType* begin() const;
    const t_BaseType* end() const;
    t_BaseType* begin();
    t_BaseType* end();
    size_type size() const;
    size_type capacity() const;
    void reserve(size_type);
    t_BaseType* append(const t_BaseType *begin, const t_BaseType *end);
};


/**
 * Sometimes, you just want a buffer.  If you're not concerned with pools,
 * factories, etc, this may suit your needs.
 */
Ptr<Buffer*> NewBuffer();


// ======== Pool ======================================================
/**
 * Pool is an interface that manages a "Pool".  There may be alternate
 * implementatios of at::Pool, however, this essentially creates new
 * buffers.
 */

class AUSTRIA_EXPORT Pool
  : public virtual PtrTarget
{
    public:


    // ======== Create ================================================
    /**
     * Create() will create a new and empty buffer.
     *
     * @return A new buffer.
     */

    virtual at::PtrDelegate< Buffer * > Create() = 0;

};


// ======== Implementation of the STL-like interface for Buffer ===
inline const Buffer::t_BaseType*
Buffer::begin() const
{
    return this->Region().m_mem;
}

inline const Buffer::t_BaseType*
Buffer::end() const
{
    t_ConstRegion r = this->Region();
    return r.m_mem + r.m_allocated;
}

inline Buffer::t_BaseType*
Buffer::begin()
{
    return this->Region().m_mem;
}

inline Buffer::t_BaseType*
Buffer::end()
{
    t_Region r = this->Region();
    return r.m_mem + r.m_allocated;
}

inline Buffer::size_type
Buffer::size() const
{
    return this->Region().m_allocated;
}

inline Buffer::size_type
Buffer::capacity() const
{
    return this->Region().m_max_available;
}

inline void
Buffer::reserve(size_type sz)
{
    t_Region r = this->Region();
    if (sz > r.m_max_available)
    {
        this->SetAvailability(sz);
    }
}

inline Buffer::t_BaseType*
Buffer::append(const t_BaseType *begin, const t_BaseType *end)
{
    t_Region r = this->Region();
    this->Append(begin, end - begin);
    return this->Region().m_mem + r.m_allocated;
}

}; // namespace

#endif // x_at_buffer_h_x
