//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
//
// at_url_parse.cpp 
//
//
//

#include "at_url_parse.h"

#include <sstream>

// Austria namespace
namespace at
{
static bool UrlIsXDigit( UrlString::value_type i_char )
{
    switch ( i_char )
    {
        case '0' :
        case '1' :
        case '2' :
        case '3' :
        case '4' :
        case '5' :
        case '6' :
        case '7' :
        case '8' :
        case '9' :
        case 'A' :
        case 'B' :
        case 'C' :
        case 'D' :
        case 'E' :
        case 'F' :
        case 'a' :
        case 'b' :
        case 'c' :
        case 'd' :
        case 'e' :
        case 'f' :
            return true;
    }
    return false;
}


// ======== UrlString::UrlDecode ===================================
// PURPOSE:
//  decode the string in place.
//

void UrlString::UrlDecode()
{
    UrlString::const_iterator    p_read;
    UrlString::const_iterator    p_end;
    UrlString::iterator          p_write;

    value_type                      l_char;
    size_type                       l_num_reduce = 0;

    p_write = begin();
    p_read = begin();
    p_end = end();

    while ( p_read != p_end ) {

        l_char = * p_read;

        if (
            ( l_char == '%' ) &&
            ( p_read+1 != p_end ) && UrlIsXDigit( p_read[1] ) &&
            ( p_read+2 != p_end ) && UrlIsXDigit( p_read[2] )
        ) {
            
            // Quickly convert from two hex digits to one character.  
            * p_write =
                ( ( (p_read[1] & 0xf) + ((p_read[1] >= 'A') ? 9 : 0) ) << 4 )
                | ( (p_read[2] & 0xf) + ((p_read[2] >= 'A') ? 9 : 0) )
            ;
            
            p_read += 2;
            l_num_reduce += 2;

        } else if ( l_char == '+' ) {
            // Undo the encoding that replaces spaces with plus signs.
            * p_write = ' ';
        } else {
            * p_write = l_char; 
        }
        
        p_write ++;
        p_read ++;
    }

    if ( l_num_reduce ) {
        resize( size() - l_num_reduce );
    }
}

static void appendEscape( std::string &i_dest, unsigned char i_c )
{
	static char *hex = "0123456789abcdef";

	i_dest.append( 1u, '%' );
	i_dest.append( 1u, hex[ 0x0f & (i_c >> 4) ] );
	i_dest.append( 1u, hex[ 0x0f & i_c ] );

	return;
}

void UrlString::UrlEncode()
{
	if( !m_is_set ) return;	// nothing to do, do nothing

	UrlString::const_iterator	p_read;
	UrlString::const_iterator	p_end;
	std::string					l_dest;
	value_type					l_char;
	unsigned char				l_uchar;

	p_read = begin();
	p_end = end();

	while( p_read != p_end )
	{
		// go through the string and copy over non-escape chars and escape the rest
		l_char = *p_read;
		l_uchar = (unsigned char) l_char;

		if( !(l_uchar < 0x20 || l_uchar == 0x7f) )	// check for illegal control characters
		{
			// ok so far
			if( l_uchar > 0x7f )	// one of those euro-trash chars...
			{
				appendEscape( l_dest, l_uchar );
			}
			else
			{
				switch( l_uchar )
				{
					case ' ':
					case '<':
					case '>':
					case '#':
					case '%':
					case '\"':
					case '{':
					case '}':
					case '|':
					case '\\':
					case '^':
					case '[':
					case ']':
					case '`':
					case '~':
					case ':':
					case ';':
					case '?':
					case '&':
					case '@':
					case '=':
					case '+':
					case '$':
					case ',':
							appendEscape( l_dest, l_uchar );
							break;
					default:
							l_dest.append( 1u, l_char );
				}
			}
		}

		p_read++;
	}

	*this = l_dest;
	return;
}

UrlParser::UrlParser(
) {
}

UrlParser::UrlParser(
    const UrlString      & i_url,
    std::string             * o_error_message
) {
    Parse( i_url, o_error_message );
}

UrlParser::UrlParser(
    const char              * i_url,
    std::string             * o_error_message
) {
    Parse( i_url, o_error_message );
}
    

// ======== RemoveDotDot ======================================
//
// Remove ".." - xxx/../ and /./ parts of the path.  Some servers don't
// allow '/..' sequences since it's a potential security threat.  This
// mimics what netscape does.  The string passed is modified in place.
//

static void RemoveDotDot( UrlString & path )
{

    if ( path.length() == 0 ) {
        return;
    }

    UrlString::iterator ostr = path.begin() - 1;
    UrlString::iterator istr = path.begin();
    UrlString::iterator end_str = path.end();

    // copy the string over itself - removing junk
    while ( istr != end_str ) {

        // if we have a /./ or a /../ string then do some fixing
redo:   
        if ( * istr == '/' ) {
            if ( * ( istr + 1 ) == '.' ) {
                if ( * ( istr + 2 ) == '/' ) {
                    istr += 2;
                    goto redo;
                } else if ( * ( istr + 2 ) == '.' ) {
                    if ( * ( istr + 3 ) == '/' ) {
                        // we have a /../
                        istr += 3;
                        while ( ostr >= path.begin() ) {
                            ostr --;
                            if ( * ostr == '/' ) {
                                ostr --;
                                goto redo;
                            }
                        }
                    }
                }
            }
        }

        ostr ++;
        * ostr = * istr;
        istr ++;
    }

    path.erase( ostr + 1, end_str );
    
    return;

} // end RemoveDotDot

// ======== UrlParser::Parse =============================================
// PURPOSE:
//      Constructor - parses a URL into it's parts
//

bool UrlParser::Parse(
    const char              * url,
    std::string             * o_error_message
) {
    UrlString url_str( url );

    return Parse( url_str, o_error_message );
}

bool UrlParser::Parse(
    const UrlString      & url_str,
    std::string             * o_error_message
) {

    if ( url_str == "" )
    {

        m_parse_error = "Empty url string";
        
        if ( o_error_message )
        {
            * o_error_message = m_parse_error;
        }
        return false;
    }

    UrlString::const_iterator    url = url_str.begin();
    
    // initialize all the parts.
    m_parse_error =
    m_scheme =
    m_host = 
    m_port = 
    m_user = 
    m_pass = 
    m_path = UrlString();

    url_str.c_str(); // null terminate the string
    UrlString::const_iterator    str = url;
    UrlString::const_iterator    ostr = url;
    int                 state = 0;

    UrlString        user_or_host;
    bool                passwd_or_port = false;
    
    char                ch = 1;
    
    // looking for scheme:
    while ( 1 ) {

        ch = * str;

        //
        // The following state machine scans URL's - the following is
        // an extended BNF of the syntax
        //
        //    user_opt_pass = user [ ':' password ] .
        //
        //    host_opt_port = host [ ':' port ] .
        //
        //    net_spec =
        //      ( "//" user_opt_pass '@' host_opt_port )
        //      | ( "//" host_opt_port )
        //      .
        //
        //    url = ( scheme ":" net_spec '/' url_path )
        //          | ( net_spec '/' url_path )
        //          | ( '/' url_path )
        //          | ( scheme ":" '/' url_path )
        //          | ( scheme ":" url_path )
        //          | ( url_path )
        //          .
        //

#define grab( part ) part.assign_encoded( ostr, str )

        switch ( state ) {
            case 21 : {
                // scanning port
                switch ( ch ) {
                    case '/' : {
                        grab( m_port );
                        ostr = str; // include '/' in path
                        goto grab_rest_as_path;
                    }
                    case '\0' : {
                        grab( m_port );
                        goto done;
                    }
                }
                break;
            }
            case 13 : {
                // scanning host
                switch ( ch ) {
                    case '/' : {
                        goto grab_host_grab_rest_as_path;
                    }
                    case ':' : {
                        state = 21;
                        grab( m_host );
                        ostr = str + 1; // discard ':'
                        break;
                    }
                    case '\0' : {
                        grab( m_host );
                        goto done;
                    }
                }
                break;
            }
            case 12 : {
                // scanning password or port
                switch ( ch ) {
                    case '/' : {
                        m_host = user_or_host;
                        grab( m_port );
                        m_port.m_is_set = passwd_or_port;
                        ostr = str;
                        goto grab_rest_as_path;
                    }
                    case '@' : {
                        state = 13;
                        // user or host is really user
                        m_user = user_or_host;
                        m_pass.m_is_set = passwd_or_port;
                        // collect the password
                        grab( m_pass );
                        ostr = str + 1; // discard the '@'
                        break;
                    }
                    case '\0' : {
                        // no path was set !
                        m_host = user_or_host;
                        m_port.m_is_set = passwd_or_port;
                        grab( m_port );
                        goto done;
                    }
                }
                break;
            }
            case 9 : {
                // scanning user or host
                switch ( ch ) {
                    case '/' : {
grab_host_grab_rest_as_path:
                        grab( m_host );
                        ostr = str;
                        goto grab_rest_as_path;
                    }
                    case ':' : {
                        state = 12;
                        grab( user_or_host );
                        passwd_or_port = true;
                        ostr = str + 1; // skip over the ':'
                        break;
                    }
                    case '@' : {
                        state = 13;
                        grab( m_user );
                        ostr = str + 1; // skip over the '@'
                        break;
                    }
                    case '\0' : {
                        grab( m_host );
                        goto done;
                    }
                }
                break;
            }
            case 1 : {
                // scanning a '//' or '/path'
                switch ( ch ) {
                    case '/' : {
                        // this is the second '/' in '//'
                        state = 9;
                        // the '//' is not significant - need to
                        // move the output pointer
                        ostr = str + 1;
                        break;
                    }
                    default : {
                        goto grab_rest_as_path;
                    }
                }
                break;
            }
            case 0 : {
                // start state - possibly a '//' or '/' or 'scheme:' or path
                switch ( ch ) {
                    case '/' : {
                        // a url beginning with '/'
                        state = 1;
                        break;
                    }
                    case ':' : {
                        // Strings that start with ':' are paths - weird
                        // but that's what happens
                        goto grab_rest_as_path;
                    }
                    case '\0' : {
                        // the empty string is significant as an empty path
                        goto grab_rest_as_path;
                    }
                    default : {
                        state = 3;
                    }
                }
                break;
            }
            case 3 : {
                // scanning a path or scheme
                switch ( ch ) {
                    case ':' : {
                        state = 2;
                        grab( m_scheme );
                        ostr = str + 1; // skip over the ':'
                        break;
                    }
                    case '\0' : {
                        // no ':' or in url and does not start with /
                        goto grab_rest_as_path;
                    }
                }
                break;
            }
            case 2 : {
                switch ( ch ) {
                    case '/' : {
                        // this is the first '/' in '://'
                        state = 1;
                        break;
                    }
                    default : {
                        // the rest is url_path
grab_rest_as_path:
                        m_path.assign_encoded( ostr, url_str.end() );
                        goto done;
                    }
                }
                break;
            }
        
        } // switch ( state )
        
        str ++;
    }

done:
    // le parse s'est fini

    RemoveDotDot( m_path );

    return true;

} // end HA_UrlParser_Main


// ======== UrlParser::CombineHostURL ========================
// PURPOSE:
//      Complete the bits of a url.
//
// RETURNS:
//      
//

void UrlParser::CombineHostURL( const UrlParser & host )
{
    // use the host scheme if one is not defined
    if ( ( ! m_scheme.m_is_set ) && host.m_scheme.m_is_set ) {
        m_scheme = host.m_scheme;
    }

    // use the host network specifier if one is not defined
    if ( ( ! m_host.m_is_set && ! m_user.m_is_set ) && host.m_host.m_is_set ) {
        
        m_host = host.m_host;
        
        if ( host.m_port.m_is_set ) {
            m_port = host.m_port;
        } else {
            m_port = UrlString();
        }

        m_user = host.m_user;

        // use the same password as the host.
        if ( host.m_pass.m_is_set ) {
            m_pass = host.m_pass;
        } else {
            m_pass = UrlString();
        }
    }

    // Path is special since we need to combine it by using
    // file path rules.

    if ( ! m_path.m_is_set ) {
        m_path = host.m_path;
    } else if ( host.m_path.m_is_set ) {
        if ( m_path[ 0 ] != '/' ) {

            // we have a relative path - need to combine it with the
            // host path.

            UrlString::const_iterator str = host.m_path.begin();
            UrlString::const_iterator endstr = host.m_path.end();
            
            endstr --; // point to the last valid character

            for (
                ;
                ( endstr >= str ) && ( * endstr != '/' );
                endstr --
            ) ;

            endstr ++;

            std::string newpath;

            newpath.assign( str, endstr );
            newpath.append( m_path );
            m_path = newpath;
            
            RemoveDotDot( m_path );
        }
    }

    return;

} // end UrlParser::CombineHostURL


// ======== UrlParser::WriteURL ==============================
// PURPOSE:
//      Create a string that reflects this URL.  The string is
//      needs to be free()'d by the caller.
//
// RETURNS:
//      std::string that contains url
//

std::string UrlParser::WriteURL()
{

    std::ostringstream  l_ostrm;
        
    // Need to construct a url string

    std::string l_slashes;
    std::string l_atsign;
    
    if ( m_scheme.m_is_set ) {
        l_ostrm << m_scheme << ":";
        l_slashes = "//";
    }
    
    if ( m_user.m_is_set ) {
        l_ostrm << l_slashes;
        l_slashes = "";
        l_atsign = "@";
        l_ostrm << m_user;
    }
    
    if ( m_pass.m_is_set ) {
        l_ostrm << l_slashes;
        l_slashes = "";
        l_atsign = "@";
        l_ostrm << ":" << m_pass;
    }
    
    if ( m_host.m_is_set ) {
        l_ostrm << l_slashes;
        l_slashes = "";
        l_ostrm << l_atsign << m_host;
        l_atsign = "";
    }
    
    if ( m_port.m_is_set ) {
        l_ostrm << l_slashes;
        l_slashes = "";
        l_ostrm << l_atsign << ":" << m_port;
    }
    
    if ( m_path.m_is_set ) {
        l_ostrm << m_path;
    }
    
    return l_ostrm.str();

} // end UrlParser::WriteURL

}; // namespace
