/**
 * \file at_log_cerr.h
 *
 * \author Bradley Austin
 *
 * at_log_cerr.h contains an extension to the Austria logging module to
 * support writing log output to cerr.
 *
 */

#ifndef x_at_log_cerr_h_x
#define x_at_log_cerr_h_x 1


#include <iostream>


// Austria namespace
namespace at
{


    class LogWriterOutputPolicy_Cerr
    {

    public:

        typedef  std::ostream  t_ostream_type;

    protected:

        LogWriterOutputPolicy_Cerr() {}
        ~LogWriterOutputPolicy_Cerr() {}

    public:

        inline t_ostream_type & OStream()
        {
            return std::cerr;
        }

        inline void Flush()
        {
            std::cerr.flush();
        }

    private:

        /* Unimplemented */
        LogWriterOutputPolicy_Cerr( const LogWriterOutputPolicy_Cerr & );
        LogWriterOutputPolicy_Cerr & operator=( const LogWriterOutputPolicy_Cerr & );

    };


}


#endif
