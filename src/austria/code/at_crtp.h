/**
 *  at_crtp.h
 *
 */


#ifndef x_at_crtp_h_x
#define x_at_crtp_h_x 1


namespace at
{


    /**
     *  @defgroup AustriaCRTP Curiously Recursive Template Pattern support
     *
     *  @{
     */


    // ======== Downcastable ==========================================
    /**
     *  The Downcastable template is primarily useful as a base type
     *  for class templates utilizing the "Curiously Recursive Template
     *  Pattern".
     *
     *  When a class 'Foo' derives from Downcastable< Foo, Bar >, it
     *  signifies that any Foo object is (or should be) a Bar object as
     *  well as a Foo object.  In other words, methods in Foo should be
     *  able to validly static_cast the 'this' pointer to a Bar *.
     *  (Downcastable provides a method named 'This()' which performs
     *  exactly this operation.)
     *
     *  In typical usage, Foo will be a template, and Bar will be a
     *  class derived from Foo< Bar >.  In that case, the appearance of
     *  Downcastable< Foo< w_type >, w_type > in the inheritance list
     *  for Foo< w_type > serves as in-code documentation that any
     *  class deriving from Foo is expected to pass itself as Foo's
     *  parameter.
     *
     *  Downcastable doesn't actually need the first template parameter
     *  (w_from_type), but it serves to disambiguate when Downcastable
     *  appears multiple times in some class's inheritance tree using
     *  the same second parameter.
     *
     *  @param w_from_type The class deriving from Downcastable.
     *  @param w_to_type The class that the class deriving from Downcastable is downcastable to.
     */
    template<
        class w_from_type,
        class w_to_type
    >
    class Downcastable
    {

    protected:

        // ======== This ==============================================
        /**
         *  This() returns a reference to "this" object, downcasted to
         *  w_to_type.
         *
         *  Const and non-const versions provided.
         *
         *  @return A reference to "this" object, downcasted to w_to_type.
         */

        inline w_to_type & This()
        {
            return * static_cast< w_to_type * >( this );
        }

        inline const w_to_type & This() const
        {
            return * static_cast< const w_to_type * >( this );
        }

    };


    // ======== ThisInBaseInitializerHack =============================
    /**
     *  Use this to eliminate "'this' used in base member initializer
     *  list" compiler warnings.  Derive your class from
     *  'ThisInBaseInitializerHack< your class >', add a
     *  'using ThisInBaseInitializerHack< your class >::This'
     *  declaration in your class, and replace '*this' with 'This()' in
     *  your constructor's base member initializer list.
     */
    template< class w_type >
    class ThisInBaseInitializerHack
      : public Downcastable< ThisInBaseInitializerHack< w_type >, w_type >
    {
    };


    /** @} */


}


#endif
