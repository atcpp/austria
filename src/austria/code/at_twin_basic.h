//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_twin_basic.h
 *
 */

#ifndef x_at_twin_basic_h_x
#define x_at_twin_basic_h_x 1

#include "at_exports.h"
#include "at_twin.h"
#include "at_assert.h"

// Austria namespace
namespace at
{

/**
 * In complex designs, implementations may have a large number of
 * aides or twins.  This implementation is designed to reduce some
 * of the errors and complexity of implementing such classes.  The
 * trade-off of making the more complex classes easier to design
 * is that the simple classes pay the penalty of the extra complexity.
 * There are however no run-time costs for this complexity.
 * 
 * Lead/Aide twins participate in a Requestor/Server relationship.
 * The at::LeadTwin provides an interface to be notified of various
 * results.  The AideTwin provides a method to have a service cancelled.
 * 
 * The application may provide significantly more co-operation between
 * Lead and Aide through the use of template parameters however the
 * Aide interface is usually the 
 * 
 */


// ======== AppLeadTwin_EmptyTraits =====================================
/**
 * AppLeadTwin_EmptyTraits is an application end of a Lead.  The application
 * will provide various method of notification.
 *
 */

template<
    typename    w_aide_twin_interface,
    typename    w_lead_interface
>
class AppLeadTwin_EmptyTraits
{
    public:

    typedef LeadTwin< w_aide_twin_interface, w_lead_interface > t_LeadTwinInterface;


    // ======== AppLeadCompletedNotify ================================
    /**
     *  AppLeadCompletedNotify does nothing in this version.
     *
     * @param i_lead
     * @return nothing
     */

    static inline void AppLeadCompletedNotify( t_LeadTwinInterface * i_lead )
    {
        //
        // DO NOTHING HERE : the EmptyTraits version does not
        // inform an application.
        //

        return;
        
    } // end AppLeadCompletedNotify 

};


// ======== AppLeadTwin_ForwardTraits =====================================
/**
 * AppLeadTwin_ForwardTraits is an application end of a Lead.  The application
 * is provided various method of notification.
 *
 */

template<
    typename    w_aide_twin_interface,
    typename    w_lead_interface
>
class AppLeadTwin_ForwardTraits
{
    public:

    // ======== t_LeadTwinInterface ===================================
    /**
     * t_LeadTwinInterface supplies the interface for a LeadTwin_Basic
     * to manage the notification to the application of various events
     * like LeadCompleted events.
     */

    class t_LeadTwinInterface
      : public LeadTwin< w_aide_twin_interface, w_lead_interface >
    {
        public:

        // ======== AppLeadCompleted ==================================
        /**
         *  AppLeadCompleted is provided by the application to get
         *  notified when the associated interface completes.
         *
         *  IMPORTANT: In the event of the destructor of the
         *  derived class being called, this method will be called
         *  and not the derived method (because it would have been
         *  already destroyed).
         *
         * @param i_completion_code indicates the completion reason
         */

        virtual void AppLeadCompleted( TwinTraits::TwinCode i_completion_code )
        {
            
        }

    };

    // ======== AppLeadCompletedNotify ================================
    /**
     *  This method is called by LeadCompleted to perform application
     *  notification.
     *
     * @param i_lead
     */

    static inline void AppLeadCompletedNotify(
        t_LeadTwinInterface                           * i_lead,
        TwinTraits::TwinCode                            i_completion_code
    ) {

        i_lead->AppLeadCompleted( i_completion_code );

        return;

    } // end AppLeadCompletedNotify 

};



// ======== LeadTwin_Basic =========================================
/**
 * LeadTwin_Basic provides a base implementation of the LeadTwin
 * interface.
 * 
 */

template<
    typename    w_aide_twin_interface,
    typename    w_lead_interface,
    typename    w_app_lead_twin_interface = AppLeadTwin_ForwardTraits< w_aide_twin_interface, w_lead_interface >
>
class LeadTwin_Basic
  : public w_app_lead_twin_interface::t_LeadTwinInterface
{
public:
    typedef w_aide_twin_interface               * t_AidePointer;
    
private:
    // ======== LeadBasicCancel =======================================
    /**
     *  LeadBasicCancel will cancel any associated aide.
     */

    inline void LeadBasicCancel()
    {
        w_aide_twin_interface                   * l_aide( m_aide );

        if ( l_aide != 0 )
        {
            //
            // Before cancellation is done, the aide pointer
            // must be removed.
            //
            m_aide = 0;

            l_aide->AideCancel();
        }

        return;
        
    } // end LeadBasicCancel

    public:

    //
    // described in LeadTwin
    //
    virtual bool LeadAssociate( w_aide_twin_interface * i_aide )
    {
        LeadBasicCancel();

        AT_Assert( m_aide == 0 );

        m_aide = i_aide;

        return true;
    }

    //
    // described in LeadTwin - to be called ONLY from
    // Aide when completed.
    //
    virtual void LeadCompleted( TwinTraits::TwinCode i_completion_code )
    {
        //
        // 
        m_aide = 0;

        //
        // perform application specific notification
        w_app_lead_twin_interface::AppLeadCompletedNotify( this, i_completion_code );
    }


    //
    // described in LeadTwin
    //
    virtual void LeadCancel()
    {
        LeadBasicCancel();
    }


    /**
     * LeadTwin_Basic constructor.
     */

    LeadTwin_Basic()
      : m_aide( 0 )
    {
    }

    /**
     * LeadTwin_Basic destructor.
     */

    ~LeadTwin_Basic()
    {
        // calling LeadBasicCancel here is the same as calling LeadCancel.
        LeadBasicCancel();
    }


    // ======== GetAide ===============================================
    /**
     *  GetAide will place a pointer to the AideTwin interface in
     *  o_aide_ptr.  It will be null if no aide has been established.
     *
     * @param o_aide_ptr This pointer will point to the aide, if any, on
     * return.
     */

    inline void GetAide( t_AidePointer & o_aide_ptr )
    {
        o_aide_ptr = m_aide;
    }

    protected:
    t_AidePointer                                 m_aide;

};



// ======== AppAideTwinTraits_EmptyTraits ==========================
/**
 * AppAideTwinTraits_EmptyTraits provides no new interface
 * points.
 */

template<
    typename    w_aide_twin_interface
>
class AppAideTwinTraits_EmptyTraits
{
    public:

    typedef w_aide_twin_interface                   t_AideTwinInterface;



    // ======== AppAideCloseNotify_Internal ==============================
    /**
     *  AppAideCloseNotify performs notification of the AideTwin
     *  application layer.
     */

    inline static void AppAideCloseNotify_Internal(
        t_AideTwinInterface             * i_aide_ptr,
        TwinTraits::TwinCode              i_completion_code
    ) {

        //
        // EmptyTraits does no notifications
        //

        return;

    } // end AppAideCloseNotify

    

};


// ======== AppAideTwinTraits_ForwardTraits ==========================
/**
 * AppAideTwinTraits_ForwardTraits provides no new interface
 * points.
 */

template<
    typename    w_aide_twin_interface
>
class AppAideTwinTraits_ForwardTraits
{
    public:


    // ======== t_AideTwinInterface ===================================
    /**
     * This adds a few more methods to the aide interface implementation.
     *
     *
     */

    class t_AideTwinInterface
      : public w_aide_twin_interface
    {
        public:
        
        // ======== AppAideCloseNotify ======================================
        /**
            * AppAideCloseNotify is overridden by the deriving classes
            * and will notify the closing of the Aide.
            *
            * @param i_completion_code is the completion code.
            * @return nothing
            */
    
        virtual void    AppAideCloseNotify(
            TwinTraits::TwinCode              i_completion_code
        ) {
        }

    };

    // ======== AppAideCloseNotify_Internal =============================
    /**
     *  AppAideCloseNotify performs notification of the AideTwin
     *  application layer.
     *
     * @param i_completion_code is the completion code.
     * @param i_aide_ptr is the interface this this
     */

    inline static void AppAideCloseNotify_Internal(
        t_AideTwinInterface             * i_aide_ptr,
        TwinTraits::TwinCode              i_completion_code
    ) {

        i_aide_ptr->AppAideCloseNotify( i_completion_code );

        return;

    } // end AppAideCloseNotify

    

};


// ======== AideTwin_Basic =========================================
/**
 * AideTwin_Basic implements a basic version of the AideTwin
 * part of the system.
 *
 */

template<
    typename    w_aide_interface,
    typename    w_lead_interface,
    typename    w_app_aide_twin_traits = AppAideTwinTraits_ForwardTraits< AideTwin<w_aide_interface> >,
    typename    w_lead_twin_interface = LeadTwin<
        AideTwin<w_aide_interface>,
        w_lead_interface
    >
>
class AideTwin_Basic
  : public w_app_aide_twin_traits::t_AideTwinInterface
{
    public:

    typedef w_lead_twin_interface                      * t_LeadPointer;

    ~AideTwin_Basic()
    {
        AideClose( TwinTraits::AideDelete );
    }


    // ======== AideAssociate =========================================
    /**
     *  AideAssociate is called by the aide application layer to
     *  associate with a lead.
     *
     * @param i_lead is the lead to associate with
     */

    void    AideAssociate(
        t_LeadPointer                                     i_lead
    )
    {
        AideClose( TwinTraits::AideGrabbed );

        m_lead = i_lead;

        WrapLeadAssociate( i_lead );

        return;

    } // end AideAssociate

    

    // ======== AideClose =============================================
    /**
     *  Is called within the Aide to close up.
     *
     * @param i_completion_code is the completion reason
     */

    void    AideClose(
        TwinTraits::TwinCode i_completion_code
    )
    {
        t_LeadPointer           l_lead = m_lead;

        if ( l_lead != 0 )
        {
            m_lead = 0;
            w_app_aide_twin_traits::AppAideCloseNotify_Internal( this, i_completion_code );
            
            WrapLeadCompleted( l_lead, i_completion_code );
        }

        return;

    } // end AideClose
    
    //
    // defined in AideTwin
    // 
    virtual void AideCancel()
    {

        AideClose( TwinTraits::CancelRequested );
    }

    inline void GetLead( t_LeadPointer & o_lead_ptr )
    {
        o_lead_ptr = m_lead;
    }

    AideTwin_Basic()
      : m_lead( 0 )
    {
    }

    protected:

    t_LeadPointer                                       m_lead;

};



// ======== NotifyLeadInterface ====================================
/**
 * This defines a simple interface for a notification callback.
 * The Notify method must be implemented by the provider.
 * This interface is provided more as an example but has some chance
 * of utility.
 */

class NotifyLeadInterface
{
    protected:

    virtual ~NotifyLeadInterface() {}
    
    public:


    // ======== Notify ================================================
    /**
     *
     *  
     * @param i_code is an application dependant code
     */

    virtual void Notify( int i_code ) = 0;
};



// ======== NullTwinInterface ======================================
/**
 * This is the special - Null interface class complete with a 
 * protected virtual destructor.
 */

class NullTwinInterface
{
    protected:

    virtual ~NullTwinInterface() {}

};

}; // namespace

#endif // x_at_twin_basic_h_x


