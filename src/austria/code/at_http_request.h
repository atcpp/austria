/*
 * at_http_request.h
 *
 * Scanner state machine 
 * ALEX - Lexical Scanner Generator  Version 1.04
 */
/* ================ STATISTICS =============== *

Source file = 'at_http_request.aa'
Output file = '/dev/null'
Optimization = Full optimization


  50 NFA states
 133 NFA transitions
  30 DFA states created
 209 DFA transitions created
   7 DFA states merged
   7 DFA transition sets merged
 132 DFA transitions merged


 * ================ statistics =============== */


namespace at_http
{


class HttpRequest_LexerInfo
{
    public:

    enum ParserState
    {
        State_020 = 0,
        State_023 = 1,
        State_017 = 2,
        State_012 = 3,
        State_007 = 4,
        State_004 = 5,
        State_000 = 6,
        State_002 = 7,
        State_001 = 8,
        State_003 = 9,
        State_005 = 10,
        State_006 = 11,
        State_008 = 12,
        State_009 = 13,
        State_010 = 14,
        State_011 = 15,
        State_013 = 16,
        State_015 = 17,
        State_014 = 18,
        State_022 = 19,
        State_018 = 20,
        State_019 = 21,
        State_016 = 22,
        ErrorState = 23,
        NumStates = ErrorState + 1,
        StartState = State_000
    };

    enum Acceptor
    {
        NullAcceptor = -1,
        a_Request
    };

    enum OutputValues
    {
        NullOutputValue = -1,
        v_FieldName,
        v_FieldValue,
        v_HTTPVersionStr,
        v_Method,
        v_RequestURI
    };

    static const int v_ValueCount = 5;

};
// ======== HttpRequest_Lexer ============================================
/**
 * HttpRequest_Lexer is generated from at_http_request.aa
 *
 * @param w_Base is a class that provides all the accessor
 *      routines.
 */
template <
    typename    w_Base
>
class HttpRequest_Lexer
  : public w_Base
{
    public:

    typedef typename w_Base::BufferType     t_BufferType;
    typedef typename w_Base::ParseReturn    t_ParseReturn;
    typedef typename w_Base::ElementType    t_ElementType;
    typedef typename w_Base::IteratorType   t_IteratorType;
    typedef typename w_Base::SizeType       t_SizeType;
    typedef typename w_Base::ElementLocal   t_ElementLocal;

    protected:
    typename w_Base::ParserState            m_current_state;

    public:


    // ======== HttpRequest_Lexer ========================================
    /**
     * HttpRequest_Lexer contructor.
     *
     */


    HttpRequest_Lexer()
      : m_current_state( w_Base::StartState )
    {
    }


    // ======== Reset =================================================
    /**
     * Reset restores the Lexer state to it's start state.
     *
     * @return nothing
     */
    
    void Reset()
    {
        this->BaseReset();
        m_current_state = w_Base::StartState;
    }


    // ======== ParseChunk ============================================
    /**
     * ParseChunk will parse a block of data with the given state
     * machine.  The context of the data is within the base class
     * that is provided as a template parameter to this class.
     *
     * @param   i_buffer The buffer containing the data to be parsed
     * @param   i_is_last Indicates that there is no more data after
     *                   the current chunk.
     * @return Determined by base class.
     */

    t_ParseReturn ParseChunk(
        t_BufferType    i_buffer,
        bool            i_is_last
    )
    {

        typedef unsigned char t_uchar;
        t_IteratorType        l_val = this->GetData( i_buffer );
        t_SizeType            l_len = this->GetSize( i_buffer );
        t_SizeType            l_original_len = l_len;

        while ( l_len )
        {
            
            switch ( m_current_state )
            {
                case w_Base::State_020 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == 0x9U )
                        || ( this->GetValue( l_tmp ) >= t_uchar( ' ' ) )
                    ) {
                        this->Collect( w_Base::v_FieldValue, l_tmp );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == 0xaU )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_022 );
                        m_current_state = w_Base::State_022;
                        this->Collect( w_Base::v_FieldValue, l_tmp );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == 0xdU )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_023 );
                        m_current_state = w_Base::State_023;
                        this->Collect( w_Base::v_FieldValue, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_020

                case w_Base::State_023 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == 0xaU )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_022 );
                        m_current_state = w_Base::State_022;
                        this->Collect( w_Base::v_FieldValue, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_023

                case w_Base::State_017 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == 0xaU )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_016 );
                        m_current_state = w_Base::State_016;
                        this->Accept( w_Base::a_Request );
                        return this->Consume( i_buffer, l_original_len - l_len );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_017

                case w_Base::State_012 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_013 );
                        m_current_state = w_Base::State_013;
                        this->Collect( w_Base::v_HTTPVersionStr, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_012

                case w_Base::State_007 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( 'T' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_008 );
                        m_current_state = w_Base::State_008;
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_007

                case w_Base::State_004 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) >= 0x1U && this->GetValue( l_tmp ) <= 0x8U )
                        || ( this->GetValue( l_tmp ) >= 0xbU && this->GetValue( l_tmp ) <= 0xcU )
                        || ( this->GetValue( l_tmp ) >= 0xeU && this->GetValue( l_tmp ) <= 0x1fU )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '!' ) )
                    ) {
                        this->Collect( w_Base::v_RequestURI, l_tmp );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( ' ' ) )
                    ) {
                        this->EndCollect( w_Base::v_RequestURI, l_val );
                        this->TransitionState( m_current_state, w_Base::State_005 );
                        m_current_state = w_Base::State_005;
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_004

                case w_Base::State_000 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( '!' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '#' ) && this->GetValue( l_tmp ) <= t_uchar( '\'' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '*' ) && this->GetValue( l_tmp ) <= t_uchar( '+' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '-' ) && this->GetValue( l_tmp ) <= t_uchar( '.' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( 'A' ) && this->GetValue( l_tmp ) <= t_uchar( 'Z' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '^' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_001 );
                        m_current_state = w_Base::State_001;
                        this->StartCollect( w_Base::v_Method, l_val );
                        this->Collect( w_Base::v_Method, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_000

                case w_Base::State_002 :
                {
                    return this->NothingMoreToScan();
                } // end case w_Base::State_002

                case w_Base::State_001 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( ' ' ) )
                    ) {
                        this->EndCollect( w_Base::v_Method, l_val );
                        this->TransitionState( m_current_state, w_Base::State_003 );
                        m_current_state = w_Base::State_003;
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( '!' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '#' ) && this->GetValue( l_tmp ) <= t_uchar( '\'' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '*' ) && this->GetValue( l_tmp ) <= t_uchar( '+' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '-' ) && this->GetValue( l_tmp ) <= t_uchar( '.' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( 'A' ) && this->GetValue( l_tmp ) <= t_uchar( 'Z' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '^' ) )
                    ) {
                        this->Collect( w_Base::v_Method, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_001

                case w_Base::State_003 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) >= 0x1U && this->GetValue( l_tmp ) <= 0x8U )
                        || ( this->GetValue( l_tmp ) >= 0xbU && this->GetValue( l_tmp ) <= 0xcU )
                        || ( this->GetValue( l_tmp ) >= 0xeU && this->GetValue( l_tmp ) <= 0x1fU )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '!' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_004 );
                        m_current_state = w_Base::State_004;
                        this->StartCollect( w_Base::v_RequestURI, l_val );
                        this->Collect( w_Base::v_RequestURI, l_tmp );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( ' ' ) )
                    ) {
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_003

                case w_Base::State_005 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( ' ' ) )
                    ) {
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( 'H' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_006 );
                        m_current_state = w_Base::State_006;
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_005

                case w_Base::State_006 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( 'T' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_007 );
                        m_current_state = w_Base::State_007;
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_006

                case w_Base::State_008 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( 'P' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_009 );
                        m_current_state = w_Base::State_009;
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_008

                case w_Base::State_009 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( '/' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_010 );
                        m_current_state = w_Base::State_010;
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_009

                case w_Base::State_010 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_011 );
                        m_current_state = w_Base::State_011;
                        this->StartCollect( w_Base::v_HTTPVersionStr, l_val );
                        this->Collect( w_Base::v_HTTPVersionStr, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_010

                case w_Base::State_011 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( '.' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_012 );
                        m_current_state = w_Base::State_012;
                        this->Collect( w_Base::v_HTTPVersionStr, l_tmp );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                    ) {
                        this->Collect( w_Base::v_HTTPVersionStr, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_011

                case w_Base::State_013 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == 0xaU )
                    ) {
                        this->EndCollect( w_Base::v_HTTPVersionStr, l_val );
                        this->TransitionState( m_current_state, w_Base::State_014 );
                        m_current_state = w_Base::State_014;
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == 0xdU )
                    ) {
                        this->EndCollect( w_Base::v_HTTPVersionStr, l_val );
                        this->TransitionState( m_current_state, w_Base::State_015 );
                        m_current_state = w_Base::State_015;
                    }
                    else if (
                        ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                    ) {
                        this->Collect( w_Base::v_HTTPVersionStr, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_013

                case w_Base::State_015 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == 0xaU )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_014 );
                        m_current_state = w_Base::State_014;
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_015

                case w_Base::State_014 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == 0xaU )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_016 );
                        m_current_state = w_Base::State_016;
                        this->Accept( w_Base::a_Request );
                        return this->Consume( i_buffer, l_original_len - l_len );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == 0xdU )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_017 );
                        m_current_state = w_Base::State_017;
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( '!' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '#' ) && this->GetValue( l_tmp ) <= t_uchar( '\'' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '*' ) && this->GetValue( l_tmp ) <= t_uchar( '+' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '-' ) && this->GetValue( l_tmp ) <= t_uchar( '.' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( 'A' ) && this->GetValue( l_tmp ) <= t_uchar( 'Z' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '^' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_018 );
                        m_current_state = w_Base::State_018;
                        this->StartCollect( w_Base::v_FieldName, l_val );
                        this->Collect( w_Base::v_FieldName, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_014

                case w_Base::State_022 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == 0x9U )
                        || ( this->GetValue( l_tmp ) == t_uchar( ' ' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_020 );
                        m_current_state = w_Base::State_020;
                        this->Collect( w_Base::v_FieldValue, l_tmp );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == 0xaU )
                    ) {
                        this->EndCollect( w_Base::v_FieldValue, l_val );
                        this->TransitionState( m_current_state, w_Base::State_016 );
                        m_current_state = w_Base::State_016;
                        this->Accept( w_Base::a_Request );
                        return this->Consume( i_buffer, l_original_len - l_len );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == 0xdU )
                    ) {
                        this->EndCollect( w_Base::v_FieldValue, l_val );
                        this->TransitionState( m_current_state, w_Base::State_017 );
                        m_current_state = w_Base::State_017;
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( '!' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '#' ) && this->GetValue( l_tmp ) <= t_uchar( '\'' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '*' ) && this->GetValue( l_tmp ) <= t_uchar( '+' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '-' ) && this->GetValue( l_tmp ) <= t_uchar( '.' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( 'A' ) && this->GetValue( l_tmp ) <= t_uchar( 'Z' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '^' ) )
                    ) {
                        this->EndCollect( w_Base::v_FieldValue, l_val );
                        this->TransitionState( m_current_state, w_Base::State_018 );
                        m_current_state = w_Base::State_018;
                        this->StartCollect( w_Base::v_FieldName, l_val );
                        this->Collect( w_Base::v_FieldName, l_tmp );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_022

                case w_Base::State_018 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( ' ' ) )
                    ) {
                        this->EndCollect( w_Base::v_FieldName, l_val );
                        this->TransitionState( m_current_state, w_Base::State_019 );
                        m_current_state = w_Base::State_019;
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( '!' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '#' ) && this->GetValue( l_tmp ) <= t_uchar( '\'' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '*' ) && this->GetValue( l_tmp ) <= t_uchar( '+' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '-' ) && this->GetValue( l_tmp ) <= t_uchar( '.' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '0' ) && this->GetValue( l_tmp ) <= t_uchar( '9' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( 'A' ) && this->GetValue( l_tmp ) <= t_uchar( 'Z' ) )
                        || ( this->GetValue( l_tmp ) >= t_uchar( '^' ) )
                    ) {
                        this->Collect( w_Base::v_FieldName, l_tmp );
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( ':' ) )
                    ) {
                        this->EndCollect( w_Base::v_FieldName, l_val );
                        this->TransitionState( m_current_state, w_Base::State_020 );
                        m_current_state = w_Base::State_020;
                        this->StartCollect( w_Base::v_FieldValue, l_val );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_018

                case w_Base::State_019 :
                {
                    t_ElementLocal  l_tmp = this->GetNext( l_val, l_len );
                    if (
                        ( this->GetValue( l_tmp ) == t_uchar( ' ' ) )
                    ) {
                    }
                    else if (
                        ( this->GetValue( l_tmp ) == t_uchar( ':' ) )
                    ) {
                        this->TransitionState( m_current_state, w_Base::State_020 );
                        m_current_state = w_Base::State_020;
                        this->StartCollect( w_Base::v_FieldValue, l_val );
                    }
                    else
                    {
                        this->TransitionState( m_current_state, w_Base::ErrorState );
                        m_current_state = w_Base::ErrorState;
                        return this->InvalidInput( i_buffer, l_val );
                    }
                    break;
                } // end case w_Base::State_019

                case w_Base::State_016 :
                {
                    return this->NothingMoreToScan();
                } // end case w_Base::State_016

                case w_Base::ErrorState :
                {
                    return this->ParsingFromError();
                }
                    
            } // switch
        } // while

        if ( i_is_last )
        {
            switch ( m_current_state )
            {
                case w_Base::State_016 :
                {
                    this->Accept( w_Base::a_Request );
                    break;
                }
                default :
                {
                    return this->PrematureEnd();
                }
            }
        }

        // Parsing this chunk is complete - expecting next chunk
        return this->Consume( i_buffer, l_original_len );
    }
};

} // namespace at_http

