/**
 * /file at_crypto.cpp
 *
 * 
 */

#include "at_crypto.h"
#include "at_factory.h"

#include <algorithm>


namespace at
{

// ======== s_no_property =============================================
/**
 * s_no_property is returned from GetProperty is called and the
 * property is not supported
 *
 */

struct NoPropertyType
{
    bool operator<( const NoPropertyType & ) const
    {
        return false;
    }
    bool operator==( const NoPropertyType & ) const
    {
        return true;
    }
};

}; // namespace at


// ======== operator<< ================================================
/**
 * This is the std::ostream::operator<< for Any objects.
 *
 *
 */

namespace std {

template<
    typename        w_char_type,
    class           w_traits
>                   
basic_ostream<w_char_type, w_traits>& operator << (
    basic_ostream<w_char_type, w_traits>        & o_ostream,
    const NoPropertyType                        & i_value
) {
    o_ostream << "s_no_property";
    return o_ostream;
    
} // end basic_ostream<w_char_type, w_traits>& operator <<

} // namespace std

namespace at {

static const CipherProperty s_x_no_property( NoPropertyType(), at::AnyInit );
const CipherProperty & s_no_property = s_x_no_property;

// ======== NewCipher =================================================

AUSTRIA_EXPORT PtrDelegate<Cipher *> NewCipher(
    const std::string           & i_key_type_n_params
) {

    std::string l_tok(
        i_key_type_n_params.begin(),
        std::find(
            i_key_type_n_params.begin(),
            i_key_type_n_params.end(),
            ':'
        )
    );

    PtrDelegate<Cipher *> l_cipher =
        FactoryRegister< Cipher, DKy >::Get().Create( l_tok.c_str() )();

    if ( ! l_cipher )
    {
        return l_cipher;
    }

    l_cipher->SetCipherParameters( i_key_type_n_params );

    return l_cipher;
}

// ======== NewKeyGenSymmetric =================================================

AUSTRIA_EXPORT PtrDelegate<CipherSymmetricKey *> NewKeyGenSymmetric(
    const std::string           & i_key_type_n_params
) {
    std::string l_tok(
        i_key_type_n_params.begin(),
        std::find(
            i_key_type_n_params.begin(),
            i_key_type_n_params.end(),
            ':'
        )
    );

    PtrDelegate<CipherSymmetricKey *> l_key_genr =
        FactoryRegister< CipherSymmetricKey, DKy >::Get().Create( l_tok.c_str() )();

    if ( ! l_key_genr )
    {
        return l_key_genr;
    }

    l_key_genr->SetCipherParameters( i_key_type_n_params );

    return l_key_genr;
}

// ======== NewKeyGenPublic =================================================

AUSTRIA_EXPORT PtrDelegate<CipherPublicKey *> NewKeyGenPublic(
    const std::string           & i_key_type_n_params
) {
    std::string l_tok(
        i_key_type_n_params.begin(),
        std::find(
            i_key_type_n_params.begin(),
            i_key_type_n_params.end(),
            ':'
        )
    );

    PtrDelegate<CipherPublicKey *> l_key_genr =
        FactoryRegister< CipherPublicKey, DKy >::Get().Create( l_tok.c_str() )();

    if ( ! l_key_genr )
    {
        return l_key_genr;
    }

    l_key_genr->SetCipherParameters( i_key_type_n_params );

    return l_key_genr;
}



} // end at namespace

namespace at_cipher
{
    
#define AT_HardLinkSymbolName AustriaCipher_FactoryHardLinkSymbol
#define HardLink(A)                     \
    A(CipherPublicKey_Ossl_RSA,0)       \
//end

#include "at_factory_link.h"

};
