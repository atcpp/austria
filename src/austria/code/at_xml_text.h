// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_text.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_text_h_x
#define x_at_xml_text_h_x 1

#include "at_xml_char_data.h"

// Austria namespace
namespace at
{
    /**
     *  The XmlText class inherits from XmlCharacterData and represents the textual content (termed
     *  character data in XML) of an XmlElement or XmlAttr. If there is no markup inside an element's
     *  content, the text is contained in a single object implementing the Text interface that is the
     *  only child of the element. If there is markup, it is parsed into the information items
     *  (elements, comments, etc.) and Text nodes that form the list of children of the element.
     */
    class XmlText : public XmlCharacterData
    {
        friend class XmlDocument;

    public:

        /**
         *  @brief  The name of a text node, which is #text for all the text nodes.
         */
        virtual std::string Name() const;

        /**
         * @brief  The enum value representing the XmlText node type.
         */
        virtual unsigned short Type() const;

        /**
         *  @brief  Duplicates this text node.
         *  @param  i_deep Not used.
         *  @return A duplicate of this text node, containing a copy of the character data.
         */
        virtual PtrDelegate< XmlNode* > Clone( bool i_deep ) const;

        /**
         *  Breaks this node into two nodes at the specified offset, keeping both in the tree as siblings.
         *  This node then only contains all the content up to the offset point. A new node of the same
         *  type, which is inserted as the next sibling of this node, contains all the content at and after
         *  the offset point. When the offset is equal to the length of this node, the new node has no data.
         *  @param  i_offset The character offset at which to split, starting from 0.
         *  @return A pointer to the new node, of the same type as this node.
         */
        PtrView< XmlText* > SplitText( std::string::size_type i_offset );

        /**
         *  @brief Accepts a visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op );

        /**
         *  @brief Accepts a non-modifying, visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op ) const;

    protected:

        /**
         *  @brief  Creates a text node.
         *  @param  i_ownerDocument The XmlDocument that creates the node.
         *  @param  i_content       The content of the textual node.
         */
        XmlText( PtrView< XmlDocument* > i_ownerDocument, const std::string& i_content ) :
            XmlCharacterData( i_ownerDocument, i_content ) {}

    private:

        /**
         *  The copy constructor is declared as private and is not defined anywhere in the code. This is to
         *  prevent the compiler from defining a bitwise default copy constructor and using it.
         *  To make a copy of an XmlText, the Clone method must be used.
         */
        XmlText( const XmlText& );

    };

}; // namespace

#endif // x_at_xml_text_h_x
