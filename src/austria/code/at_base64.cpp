//
// The Austria library is copyright (c) 2006
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
#include "at_base64.h"
#include <cstring>
#include <algorithm>

#include "at_assert.h"


namespace at
{

const BaseNKey<6, BaseNEndianLittle> g_urlsafe_base64_key = {
    {
        // m_key
        {
            // "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@-"
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
            'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '@', '-',
        }
    },
    {
        // m_key_inverse
        {
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 63, -1, -1,
             0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
            62, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
            25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1,
            -1, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
            51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        }
    },
    {
    }

}; // Base64Key::s_default_key


const Base64Key g_default_base64_key = {
    {
        // m_key
        {
            // "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
        },
    },
    {
        // m_key_inverse
        {
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1,
            -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
            -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        },
    },
    {
        '=',
        24 // m_bit_bundle
    }
    
};


// ======== GenerateBase64Key =========================================

AUSTRIA_EXPORT void GenerateBase64Key( const char * i_key, unsigned i_bit_bundle, Base64Key & o_result )
{
    // The string MUST be 64 chars in length
    // AT_Assert( std::strlen( i_key ) == 65 );

    std::fill_n(
        o_result.m_key_inverse.m_data,
        sizeof( o_result.m_key_inverse.m_data ),
        (signed char) -1
    );

    for ( unsigned i = 0; i < sizeof o_result.m_key.m_data; ++i )
    {
        // check key
        AT_Assert( i_key[ i ] > 0 );

        // check for uniqueness
        AT_Assert( ( (signed char) -1 ) == o_result.m_key_inverse.m_data[ i_key[ i ] ] );

        o_result.m_key_inverse.m_data[ i_key[ i ] ] = (signed char) i;
    }

    // copy the key in place
    std::copy( i_key, i_key + sizeof o_result.m_key.m_data, o_result.m_key.m_data );

    // Fill in the pad character info - it is marked by a -2 instead of a -1
    AT_Assert( ( (signed char) -1 ) == o_result.m_key_inverse.m_data[ i_key[ sizeof o_result.m_key.m_data ] ] );
    
    o_result.m_key_inverse.m_data[ i_key[ sizeof o_result.m_key.m_data ] ] = -2;

    o_result.m_pad_char.m_pad_char = i_key[ sizeof o_result.m_key.m_data ];

    o_result.m_pad_char.m_bit_bundle = i_bit_bundle;
}

// ======== GenerateBase64Key =========================================

AUSTRIA_EXPORT void GenerateBase64Key( const char * i_key, BaseNKey<6, BaseNEndianLittle> & o_result )
{
    // The string MUST be 64 chars in length
    // AT_Assert( std::strlen( i_key ) == 64 );

    std::fill_n(
        o_result.m_key_inverse.m_data,
        sizeof( o_result.m_key_inverse.m_data ),
        (signed char) -1
    );

    for ( unsigned i = 0; i < sizeof o_result.m_key.m_data; ++i )
    {
        // check key
        AT_Assert( i_key[ i ] > 0 );

        // check for uniqueness
        AT_Assert( ( (signed char) -1 ) == o_result.m_key_inverse.m_data[ i_key[ i ] ] );

        o_result.m_key_inverse.m_data[ i_key[ i ] ] = (signed char) i;
    }

    // copy the key in place
    std::copy( i_key, i_key + sizeof o_result.m_key.m_data, o_result.m_key.m_data );
}

} // namespace
