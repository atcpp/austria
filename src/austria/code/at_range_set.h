/**
 *  \file at_range_set.h
 *
 *  \author Bradley Austin
 *
 */


#ifndef x_at_range_set_h_x
#define x_at_range_set_h_x


#include <algorithm>
#include <map>
#include <vector>
#include "at_assert.h"


namespace at
{


    /**
     *  @defgroup AustriaRangeSet The RangeSet class
     *
     *  @{
     */


    // Forward declarations.

    template<
        class w_type
    >
    struct HalfOpenInterval;

    template<
        class w_type
    >
    struct ClosedInterval;


    // ======== HalfOpenInterval ==============================================
    /**
     *  Utility struct, useful for in-code documentation and error avoidance.
     *  Wraps two values of arbirary integral type, which are meant to
     *  represent a half-open interval of values (i.e. a contiguous range of
     *  values between 'begin' and 'end', including 'begin' but not including
     *  'end').  If an interface requires passing two values representing a
     *  half-open interval, defining the interface in terms of HalfOpenInterval
     *  makes the meaning of the values obvious at the call site, and makes
     *  off-by-one bugs due to confusion between different interval types
     *  *much* less likely.
     *
     *  See also:  ClosedInterval
     *
     *  @param w_type The integral type of which the interval represents a set
     *
     */
    template<
        class w_type
    >
    struct HalfOpenInterval
    {
        w_type begin;
        w_type end;

        inline HalfOpenInterval()
          : begin(),
            end()
        {
        }

        inline HalfOpenInterval(
            const w_type & i_begin,
            const w_type & i_end
        )
          : begin( i_begin ),
            end( i_end )
        {
        }

        inline explicit HalfOpenInterval(
            const ClosedInterval< w_type > & i_closed_interval
        )
          : begin( i_closed_interval.first ),
            end( i_closed_interval.last + 1 )
        {
        }

    };


    // ======== ClosedInterval ================================================
    /**
     *  Utility struct, useful for in-code documentation and error avoidance.
     *  Wraps two values of arbirary integral type, which are meant to
     *  represent a closed interval of values (i.e. a contiguous range of
     *  values between 'first' and 'last', inclusive).  If an interface
     *  requires passing two values representing a closed interval, defining
     *  the interface in terms of ClosedInterval makes the meaning of the
     *  values obvious at the call site, and makes off-by-one bugs due to
     *  confusion between different interval types *much* less likely.
     *
     *  WARNING! - Never set 'last' equal to the largest value representable
     *  by 'w_type', as it will overflow if the ClosedInterval is converted to
     *  a HalfOpenInterval.
     *
     *  WARNING! - Never convert a HalfOpenInterval representing an empty
     *  interval to a ClosedInterval, as such an interval cannot be represented
     *  by a ClosedInterval.
     *
     *  See also:  HalfOpenInterval
     *
     *  @param w_type The integral type of which the interval represents a set
     *
     */
    template<
        class w_type
    >
    struct ClosedInterval
    {
        w_type first;
        w_type last;

        inline ClosedInterval()
          : first(),
            last()
        {
        }

        inline ClosedInterval(
            const w_type & i_first,
            const w_type & i_last
        )
          : first( i_first ),
            last( i_last )
        {
        }

        inline explicit ClosedInterval(
            const HalfOpenInterval< w_type > & i_half_open_interval
        )
          : first( i_half_open_interval.begin ),
            last( i_half_open_interval.end - 1 )
        {
        }

    };


    // ======== RangeSet ==========================================================
    /**
     *  A RangeSet represents a mathematical set of integral values (of arbitrary
     *  type) organized as a sequence of contiguous blocks of values.  (So it's
     *  appropriate for situations where the number of such blocks is small
     *  compared to the number of individual values.)
     *
     *  @param w_type The integral type of which the RangeSet represents a set
     *
     */
    template<
        class w_type
    >
    class RangeSet
    {
        typedef  std::map< w_type, w_type >  t_map_type;
        typedef  typename t_map_type::iterator  t_map_iterator;
        typedef  typename t_map_type::const_iterator  t_map_const_iterator;

        /*
         *  Each element in 'm_map' represents one discrete range of values,
         *  with the index value representing the beginning of the range, and
         *  the data value representing the end, as a half-open interval.
         *  Operations that change the state of the map must maintain the
         *  invariant that each range in the map is non-empty, and no range
         *  overlaps or abuts any other range in the map.
         */
        t_map_type m_map;

    public:


        // ======== Add =======================================================
        /**
         *  Add a range of values to the range set.  Essentially a bitwise-or
         *  operation.
         *
         *  Versions taking both half-open and closed intervals provided.
         *
         *  @param i_range The range of values to add to the set
         *  @return nothing
         */

        void Add(
            const HalfOpenInterval< w_type > & i_range
        )
        {
            const w_type & l_begin = i_range.begin;
            const w_type & l_end = i_range.end;

            // Must not insert empty ranges into the map.
            if ( l_end <= l_begin )
            {
                return;
            }

            // l_insert is true if we need to insert a new element into the map
            // (i.e. l_begin is not inside an existing block).
            bool l_insert = true;

            t_map_iterator l_iter = m_map.lower_bound( l_begin );

            // If there's an existing block starting at l_begin, l_iter points to
            // it.  Otherwise l_iter points one position ahead of the last block
            // whose start value is < l_begin, if there is one.

            if ( l_iter != m_map.end() && l_iter->first == l_begin )
            {
                // The block pointed to by l_iter starts at l_begin.
                l_insert = false;
            }
            else
            {
                // Try the preceding block, if there is one.
                if ( l_iter != m_map.begin() )
                {
                    --l_iter;
                    if ( l_iter->second >= l_begin )
                    {
                        // l_begin is inside the block pointed to by l_iter.
                        l_insert = false;
                    }
                }
            }

            if ( l_insert )
            {
                l_iter = m_map.insert(
                    typename t_map_type::value_type( l_begin, l_end )
                ).first;
            }
            else
            {
                // l_begin is inside the block pointed to by l_iter.  Extend it
                // if necessary.
                if ( l_end > l_iter->second )
                {
                    l_iter->second = l_end;
                }
            }

            // If we inserted a new block or extended an existing one, it might
            // overlap with one or more subsequent blocks.  If so, merge them into
            // the new/extended block.
            t_map_iterator l_iter2( l_iter );
            ++l_iter2;
            while ( l_iter2 != m_map.end() && l_iter2->first <= l_iter->second )
            {
                if ( l_iter2->second > l_iter->second )
                {
                    l_iter->second = l_iter2->second;
                }

                // Increment the iterator before erasing, so the erase doesn't
                // invalidate the iterator.
                t_map_iterator l_iter3( l_iter2 );
                ++l_iter2;
                m_map.erase( l_iter3 );

            }

        }

        inline void Add(
            const ClosedInterval< w_type > & i_range
        )
        {
            Add( HalfOpenInterval< w_type >( i_range ) );
        }


        // ======== Add =======================================================
        /**
         *  Add the ranges in another range set to this range set.  Essentially
         *  a bitwise-or operation.
         *
         *  @param i_range The RangeSet to add to this RangeSet
         *  @return nothing
         *
         */
        void Add( const RangeSet & i_range_set )
        {
            for ( t_map_const_iterator
                      l_iter = i_range_set.begin(),
                      l_limit = i_range_set.end();
                  l_iter != l_limit;
                  ++l_iter
                )
            {
                Add(
                    HalfOpenInterval< w_type >( l_iter->first, l_iter->second )
                );
            }
        }


        // ======== GranularMask ==============================================
        /**
         *  GranularMask() creates a bit mask representation of the range set
         *  according to the following formula:
         *
         *      1) The size of the bit mask is total_size / grain_size, rounded
         *         up.
         *
         *      2) Bit N in the bit mask is set to 1 iff there exists at least
         *         one value V, such that:
         *
         *             a)  V is a member of the range set.
         *
         *             b)  0 <= V < total_size
         *
         *             c)  grain_size * N  <=  V  <  grain_size * ( N + 1 )
         *
         *  total_size must be >= 0.  grain_size must be > 0.
         *
         *  @param i_total_size The total number of values represented by the bit mask
         *  @param i_grain_size The number of values represented by each bit in the bit mask
         *  @param o_mask The bit mask
         *  @return nothing
         *
         */
        void GranularMask(
            const w_type & i_total_size,
            const w_type & i_grain_size,
            std::vector< bool > & o_mask
        )
        {
            AT_Assert( i_total_size >= 0 );
            AT_Assert( i_grain_size > 0 );

            w_type l_mask_size(
                ( i_total_size / i_grain_size ) +
                    ( i_total_size % i_grain_size ? 1 : 0 )
            );

            o_mask.resize( l_mask_size, false );

            for ( t_map_const_iterator
                      l_iter = m_map.begin(),
                      l_limit = m_map.end();
                  l_iter != l_limit;
                  ++l_iter
                )
            {
                if ( l_iter->first >= i_total_size )
                {
                    return;
                }
                w_type l_end( l_iter->second );
                if ( l_end > i_total_size )
                {
                    l_end = i_total_size;
                }
                w_type l_bit_begin = l_iter->first / i_grain_size;
                w_type l_bit_end = ( ( l_end - 1 ) / i_grain_size ) + 1;
                std::fill(
                    o_mask.begin() + l_bit_begin,
                    o_mask.begin() + l_bit_end,
                    true
                );
            }
        }


    };


    /**  @} */


}


#endif
