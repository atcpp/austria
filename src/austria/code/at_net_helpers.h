/*
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 *
 *  Herein lie a few things to make using at_net.h less of a pain.
 */

#ifndef x_at_net_helpers_h_x
#define x_at_net_helpers_h_x 1

#include "at_net.h"
#include "at_twinmt_basic.h"
#include <string>
#include <utility>
#include <list>

namespace at
{
    /**
     * Perform a ListenDatagram on the Net object passed and wait for it to
     * succeed or fail.  On success, the resulting NetDatagramChannel is
     * returned to you and *o_error is set to zero.  If it fails, zero is
     * returned and o_error is set accordingly.
     */
    PtrDelegate<NetDatagramChannel*> SyncListenDatagram(
        PtrDelegate<Net*> i_net,
        PtrDelegate<const NetAddress*> i_address,
        PtrDelegate<const NetParameters*> i_parameters,
        PtrDelegate<Pool*> i_buffer_pool,
        const NetError **o_error);

    /**
     * Perform a synchronous lookup of an address.  Presently, DNS lookups
     * are performed synchronously anyhow, so this just eliminates some
     * code, but users of this function should be prepared to modify their
     * work when an asynchronous DNS implementation becomes available.
     */
    PtrDelegate<const NetAddress*> SyncResolve(
        PtrDelegate<Net*> i_net,
        PtrDelegate<const NetAddress*> i_addr);

    /**
     * Make an HTTP request.
     * @param uri The URI to which you would like to make the request.
     * @param method You had better be able to guess.
     * @param contentType If you're sending some stuff, this is the content
     * type.  If you're not sending anything, you can pass whatever you
     * like here.
     * @param content The content you're sending.  If you're using a method
     * that is not supposed to send content, you should make sure that this
     * is empty, 'cause otherwise, what you pass will be sent, breaking the
     * protocol.
     * @param statusOut The response status code will be put here.
     * @param responseOut The response message will be put in this buffer.
     * @param contentTypeOut The response message's content type will be
     * put here.
     * @return True iff the whole process went smoothly.
     */
     bool SyncHTTPRequest(
         const char *uri, const char *method,
         const char *contentType, Ptr<Buffer*> content,
         int *statusOut, at::Ptr<at::Buffer*> responseOut,
         std::string *contentTypeOut);

    /**
     * XConnectLead implements NetConnectLead.  Each time a connection is
     * received, a copy of the template argument is constructed with the
     * NetConnection as an argument.  When its aide goes away, the
     * XConnectLead object destroys itself (but leaves the objects it
     * created untouched).  You can use instances of XConnectLead directly
     * or subclass from it if you need more control (see the method
     * descriptions for hooks that XConnectLead gives to subclasses).
     */
    template<typename w_X>
    class XConnectLead :
        public LeadTwinMT_Basic<NetConnectLeadIf>
    {
    private:
        typedef std::pair<Ptr<w_X *>, const NetError*> Result;
    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_mutexResults);
            delete m_newResult;
            m_newResult = new Conditional(*i_sharedMutex);
            m_mutexResults = i_sharedMutex; // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectLeadIf>::LeadAssociate(i_aide, i_sharedMutex);
        }
        virtual void AppLeadCompleted(TwinTraits::TwinCode i_completion_code)
        {
            // wake up all waiters if there is no possibility of future events
            Lock<Ptr<MutexRefCount *> > l(m_mutexResults);
            m_results.push_back(Result(0, &NetError::s_unexpected_failure));
            m_failed = true;
            m_newResult->Post();
        }


    public:
        typedef w_X X;

        /**
         * Construct a new lead
         */
        XConnectLead() :
            m_mutexResults(new MutexRefCount(Mutex::Recursive)),
            m_newResult(new Conditional(*m_mutexResults)),
            m_results(),
            m_failed(false)
        {
        }

        virtual ~XConnectLead()
        {
            LeadCancel();
            delete m_newResult;
        }

        /**
         * Wait for a connection or an error, whichever comes first.
         * @param x_out On return, an instance of the template argument if
         * Established was called or zero if Failed was called.
         * @param err_out On return, an pointer to an error if Failed was
         * called or zero if Established was called.
         * @param i_MaxWaitTime maximum TimeInterval to wait for connection, default
         * is Infinite (-1).
         * @return false if wait timed out, true otherwise
         */
        bool Wait(Ptr<X *>*x_out, const NetError **err_out, const TimeInterval &i_MaxWaitTime = TimeInterval(-1))
        {
            Lock<Ptr<MutexRefCount *> > l(m_mutexResults);
            while ((m_results.size() == 0) && !m_failed) {
                if (!m_newResult->Wait(i_MaxWaitTime)) {
                    return false;
                }
            }
            const NetError *r_err = NULL;
            Ptr<X *> r_out;
            if (!m_results.empty()) {
                Result r = m_results.front();
                m_results.pop_front();
                r_out = r.first;
                r_err = r.second;
            } else {
                r_err = &NetError::s_unexpected_failure;
            }
            
            if ( x_out )
            {
                *x_out = r_out;
            }
            
            if ( err_out )
            {
                *err_out = r_err;
            }
            return true;
        }

        /**
         * NewHandler is called each time an instance of the template
         * argument is created.  This method is a hook for subclasses.
         * @param x The newly constructed object
         */
        virtual void NewHandler(PtrView<X *> x)
        {
        }

    public: // NetConnectLead
        virtual void Failed(const NetError &i_err)
        {
            Lock<Ptr<MutexRefCount *> > l(m_mutexResults);
            m_results.push_back(Result(0, &i_err));
            m_failed = true;
            m_newResult->Post();
        }

        virtual void Established(PtrDelegate<NetConnection*> io_endpoint)
        {
            Ptr<X *>p = new X(io_endpoint);
            {
                Lock<Ptr<MutexRefCount *> > l(m_mutexResults);
                m_results.push_back(Result(p, 0));
            }
            this->NewHandler(p);
            {
                Lock<Ptr<MutexRefCount *> > l(m_mutexResults);
                m_newResult->Post();
            }
        }

    protected:
        Ptr<MutexRefCount *>            m_mutexResults;
        Conditional *                   m_newResult;
        std::list<Result> m_results;
        bool m_failed;
    };

    struct NetConnectionEvent
    {
        enum Type {
            Invalid, ReceiveFailure, SendFailure, SendCompleted,
            DataReady, Closed, ReceivedProperties
        };
        int seqno;
        Type type;
        bool hasProperties;
        bool hasAllProperties;
        const NetError *error;
        Ptr<const Buffer*> const_buffer;
        const char *begin;
        const char *end;
        NetConnectionEvent() : type(Invalid),
                            hasProperties(false),
                            hasAllProperties(false),
                            error(0),
                            begin(0),
                            end(0)
        { }

        static const char* TypeToString(Type t);
    };

    std::ostream& operator<<(std::ostream &os, const NetConnectionEvent &ev);

    /**
     * NetConnectionRecorder helps one to write linear code using the
     * non-linear Austria networking interfaces.  It registers itself as
     * the responder for a connection and records all the calls that it
     * receives on its NetConnectionResponderIf interface.
     */
    class NetConnectionRecorder :
        public LeadTwinMT_Basic<NetConnectionResponderIf>,
        public PtrTarget_MT
    {
    protected:
        int m_seq;
        Ptr<NetConnection*> m_conn;
        std::list<NetConnectionEvent>   events;
        Ptr<MutexRefCount *>            m_stateMutex;
        Conditional *                   m_newEvent;

    public:
    // accessors
        Ptr<MutexRefCount *> StateMutex(void) { return m_stateMutex;}
        Conditional & GetConditional(void) { return *m_newEvent;}       // REV: need a releasor, or refcounted conditional
        std::list<NetConnectionEvent>  &EventList(void)  { return events;}
        PtrView<NetConnection *> Connection(void) { return m_conn;}

    // interface
        NetConnectionRecorder(PtrDelegate<NetConnection*> conn);
        virtual ~NetConnectionRecorder()
        {
            {
                Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
                delete m_newEvent;
                m_newEvent = NULL;
            }
            m_stateMutex = NULL;
            m_conn = NULL;
        }

        void ReceiveFailure(const NetConnectionError &i_error);
        void SendCompleted(PtrDelegate<const Buffer*> i_buffer,
                           const char *i_end);
        void SendFailure(PtrDelegate<const Buffer*> i_buffer,
                         const char *i_begin,
                         const NetConnectionError &i_error);
        void DataReady();
        void OutOfBandDataReady();
        void ReceivedPropertiesNotification(
            bool i_complete, bool i_props_in_queue);

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            delete m_newEvent;
            m_newEvent = new Conditional(*i_sharedMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectionResponderIf>::LeadAssociate(i_aide, i_sharedMutex);
        }
    };

    /**
     * Find the content length in a set of properties from an HTTP message.
     * @param i_props The properties to search.
     * @param o_len On return, the content-length, if found.
     * @return True iff the content length was found.
     */
    bool FindContentLength(
        const NetConnection::t_PropList &i_props, size_t *o_len);

    /**
     * Find the response status in a set of properties from an HTTP message.
     * @param i_props The properties to search.
     * @param o_status On return, the status, if found.
     * @return True iff the status was found.
     */
    bool FindHTTPStatus(
        const NetConnection::t_PropList &i_props, int *o_status);

    /**
    * case insensitive ascii string compare -- ignoring locale
    * @param i_first std::string
    * @param i_second std::string
    * @return true if strings match with case insensitive compare
    */
    bool NoCaseCompareExact(const std::string s1, const std::string s2);

} // namespace at

#endif // #ifndef x_at_net_helpers_h_x 
