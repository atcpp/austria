//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_status_report_basic.h
 *
 */

#ifndef x_at_status_report_basic_h_x
#define x_at_status_report_basic_h_x 1

#include "at_exports.h"
#include "at_status_report.h"

// Austria namespace
namespace at
{

// ======== StatusReport_Basic =====================================
/**
 * StatusReport_Basic ia a basic implementation of the StatusReport
 * interface.
 */

class AUSTRIA_EXPORT StatusReport_Basic
  : public StatusReport
{
public:
    virtual ~StatusReport_Basic() {}

    StatusReport_Basic();

    StatusReport_Basic(
        StatusTypeCode                                i_typecode,
        int                                           i_status,
        const std::string                           & i_description
    );

    // override StatusReport::ReportStatus
    virtual void ReportStatus(
        StatusTypeCode                                i_typecode,
        int                                           i_status,
        const std::string                           & i_description
    );

    virtual void ReportStatus(
        const StatusReport                      * i_sreport
    );

    // override StatusReport::ClearStatus
    virtual void ClearStatus();

    // override StatusReport::GetStatus
    virtual bool GetStatus() const;

    // override StatusReport::GetStatus
    virtual bool GetStatus(
        StatusTypeCode                              & o_typecode,
        int                                         & o_status,
        std::string                                 & o_description
    ) const;
    
    // override StatusReport::GetStatus
    virtual bool GetStatus(
        StatusReport                                * o_srep
    ) const;
    
    // 
    // member variables
    
    StatusTypeCode                                    m_status_type;
    int                                               m_status_code;
    std::string                                       m_description;

};

}; // namespace
#endif // x_at_status_report_basic_h_x


