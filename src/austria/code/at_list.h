//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_list.h
 *
 */

#ifndef x_at_list_h_x
#define x_at_list_h_x 1

#include "at_exports.h"
#include "at_unionptr.h"

// Austria namespace
namespace at
{

/**
 * @defgroup FlexibleList Generic List Support
 *
 * List is an alternative to std::list when the need of self-aware
 * class is more appropriate.  Hence is a class is to be "aware" of it's
 * container so that it may affect how the container behaves, (like for
 * example move itself from one container to another or erase itself from
 * a container) OR when the need for a class to be associated with one or
 * more containers is required, then this list implementation will provide
 * the appropriate building blocks for creating these class structures.
 * 
 * The basic philosophy of this library is that there are components that
 * are assemebled to create the desired structure.  A container aware
 * class is built by assembling various components to create an "apogee"
 * class containing list elements.
 * 
 * This concept may be extended by creating objects that are contained
 * in various other containers, for example collections of maps and lists.
 * 
 * Lifetimes of objects are determined by various traits class like
 * for example ListAccessorDeclare below.
 * 
 *  @{
 */


// ======== List_Positions =========================================
/**
 * List_Positions are identifiers of position.  These are
 * used to determine where the iterator is pointing.
 * An iterator points to the beginning end or in the middle.
 * If an iterator points to the middle the object is a
 * xC_List_Entry otherwise it is a xC_List_Head
 */

enum List_Positions
{
    /**
     * This indicates that the node is an element.
     */
    xE_Element = 0,

    /**
     * This indicates that the node is the end of the list.
     */
    xE_End = 1,

    /**
     * This indicates that the node is the beginning of the list.
     */
    xE_Beginning = 2
};


// ======== List_EmptyMutex =========================================
/**
 * The List_EmptyMutex class emulates a no-op lock.  This is used
 * in the case of lists that have no locks.
 * An implemented lock class will need to implement the t_Lock
 * class.
 */

class AUSTRIA_EXPORT List_EmptyMutex
{
    public:

    inline void Lock() {}
    inline void UnLock() {}

    // ======== t_Lock ================================================
    /**
     * t_Lock implements a basic lock using the aquisition is
     * initialization paradigm interface but does nothing for the
     * nothing empty lock.
     */

    class t_Lock
    {
        public:

        t_Lock( List_EmptyMutex & io_mutex )
        {
        }

        ~t_Lock()
        {
        }

        private:
        
        // must not allow the copy or assignment so make
        // these methods private.
        t_Lock( const t_Lock & );
        t_Lock & operator=( const t_Lock & );
    };
};


// ======== List_Mutex =============================================
/**
 * A list mutex. This is a convenience containter containing the
 * appropriate interfaces.
 *
 */
template <
    typename    w_lock_base
>
class List_Mutex
  : public w_lock_base
{
    public:

    // ======== t_Lock ================================================
    /**
     * t_Lock implements a basic lock using the aquisition is
     * initialization paradigm.
     *
     */

    class t_Lock
    {
        public:

        List_EmptyMutex              * m_lock;

        t_Lock( List_EmptyMutex & io_mutex )
          : m_lock( & io_mutex )
        {
            m_lock->Lock();
        }

        ~t_Lock()
        {
            m_lock->UnLock();
        }

        private:
        
        // must not allow the copy or assignment so make
        // these methods private.
        t_Lock( const t_Lock & );
        t_Lock & operator=( const t_Lock & );
    };

};


// ======== ListNothing_Basic =====================================
/**
 *  ListNothing_Basic a basic NOTHING base class used as a template
 *  parameter when no base class behaviour is needed.
 */

class AUSTRIA_EXPORT ListNothing_Basic
{
    public:

};


// ======== List_Error_Traits ======================================
/**
 * List_Error_Traits defined various actions to be taken on
 * error conditions.
 */

class List_Error_Traits
{
    public:

    static inline void IteratorUndefined()
    {
        AT_Assert( false );
    }

};


// ======== Iterator ===============================================
/**
 * Iterator defines an STL like iterator.  The template is created
 * from a forward declared class that will derive the entire object.
 */

template <
    typename    w_base_class
>
class Iterator
{
    public:
    typedef typename w_base_class::txC_List_Traits         xC_List_Traits;
    typedef typename w_base_class::txC_List_Entry          xC_List_Entry;
    typedef typename w_base_class::txC_List_Head           xC_List_Head;
    typedef typename w_base_class::txW_PayloadType         xW_PayloadType;
    typedef UnionPtrType<List_Positions, xC_List_Entry *, xC_List_Head * >
                                                            xC_PointerType;

    public:

    xC_PointerType                                          m_pointer;

    //
    // empty constructor
    Iterator()
    {
    }

    inline static xC_PointerType xF_EndIterator( xC_List_Head * l_head )
    {
        xC_PointerType l_next_ptr;

        l_next_ptr = l_head;

        return l_next_ptr;
    }
    
    xC_PointerType xF_Iterate(
        typename xC_List_Entry::txC_Entry_MemberPtr w_elem_pos,
        typename xC_List_Entry::txC_Entry_MemberPtr w_elem_neg,
        typename xC_List_Head::txC_Head_MemberPtr   w_head_pos,
        typename xC_List_Head::txC_Head_MemberPtr   w_head_neg,
        List_Positions                           w_posi_pos,
        List_Positions                           w_posi_neg
    )
    {
        //
        // create places to put data
        //
        List_Positions                           l_type;
        xC_List_Entry                             * l_entry;
        xC_List_Head                              * l_head;

        //
        // go read the data
        //
        l_type = m_pointer.Get( l_entry, l_head );

        xC_List_Entry * l_next_entry;
        //
        // if this is an element, we can iterate
        if ( l_type == xE_Element )
        {
            l_next_entry = l_entry->*w_elem_pos;

            //
            // We have reached the end of the list.
            if ( l_next_entry == 0 )
            {
                return xF_EndIterator( l_entry->m_head );
            }

        }
        else
        {
            // OK - we're already at the end of the list
            // if we're asked to interate in the correct
            // direction (back into the list) then do so.
            // Otherwise we assert.

            AT_Assert( l_type != w_posi_pos );

            l_next_entry = l_head->*w_head_pos;
            
            //
            // We have reached the end of the list.
            if ( l_next_entry == 0 )
            {
                return xF_EndIterator( l_head );
            }

        }
        
        xC_PointerType l_next_ptr = l_next_entry;

        //
        // return the pointer to the next object
        return l_next_ptr;
            
    }

    //
    // Determiine if the iterator it at the end.
    //

    inline bool is_end()
    {
        
        List_Positions                           l_type;
        xC_List_Entry                             * l_entry;
        xC_List_Head                              * l_head;

        //
        // go read the data
        //
        l_type = m_pointer.Get( l_entry, l_head );

        //
        // if this is an element, we can iterate
        return l_type != xE_Element;

    }
    
    //
    // Determiine if the iterator is linked
    //

    inline bool is_linked()
    {
        
        xC_List_Entry                             * l_entry = Dereference();

        return l_entry->m_head != 0;
    }
    
    inline Iterator & operator -- ()
    {
        m_pointer = xF_Iterate( &xC_List_Entry::m_back, &xC_List_Entry::m_forw, &xC_List_Head::m_begin, &xC_List_Head::m_end, xE_End, xE_Beginning );

        return * this;
    }
    
    inline Iterator operator -- ( int )
    {
        Iterator     l_tmp = * this;
        
        m_pointer = xF_Iterate( &xC_List_Entry::m_back, &xC_List_Entry::m_forw, &xC_List_Head::m_begin, &xC_List_Head::m_end, xE_End, xE_Beginning );

        return l_tmp;
    }
    
    inline Iterator & operator ++ ()
    {
        m_pointer = xF_Iterate( &xC_List_Entry::m_forw, &xC_List_Entry::m_back, &xC_List_Head::m_end, &xC_List_Head::m_begin, xE_Beginning, xE_End );

        return * this;
    }
    
    inline Iterator operator ++ ( int )
    {
        Iterator     l_tmp = * this;
        
        m_pointer = xF_Iterate( &xC_List_Entry::m_forw, &xC_List_Entry::m_back, &xC_List_Head::m_end, &xC_List_Head::m_begin, xE_Beginning, xE_End );

        return l_tmp;
    }

    inline xC_List_Entry *  Dereference()
    {
        //
        //
        if ( xC_List_Traits::DoDereferenceCheck )
        {
            //
            // create places to put data
            //
            List_Positions                           l_type;
            xC_List_Entry                             * l_entry;
            xC_List_Head                              * l_head;
    
            //
            // go read the data
            //
            l_type = m_pointer.Get( l_entry, l_head );
    
            AT_Assert( l_type == xE_Element );
            
            return l_entry;
        }
        else
        {
            // this just pulls the value directly out of the pointer
            // this only works with the first value.
            return m_pointer.m_value.m_value_0;
        }
    }

    inline xW_PayloadType & operator * ()
    {
        return * static_cast<xW_PayloadType *>( Dereference() );
    }

    inline xW_PayloadType * operator -> ()
    {
        return static_cast<xW_PayloadType *>( Dereference() );
    }

    inline Iterator operator + ( Int32 i_val ) const
    {
        Iterator    l_rval = * this;
        
        while ( i_val -- )
        {
            l_rval->operator ++ ();
        }

        return l_rval;
    }
    
    inline Iterator operator - ( Int32 i_val ) const
    {
        Iterator    l_rval = * this;
        
        while ( i_val -- )
        {
            l_rval->operator -- ();
        }

        return l_rval;
    }
    
    template< typename w_base_rhs > inline bool operator == ( const Iterator<w_base_rhs> & i_rhs ) const
    {
        return m_pointer == i_rhs.m_pointer;
    }

    template< typename w_base_rhs > inline bool operator != ( const Iterator<w_base_rhs> & i_rhs ) const
    {
        return !( m_pointer == i_rhs.m_pointer );
    }

    template< typename w_base_rhs > Iterator & operator = ( Iterator<w_base_rhs> i_rhs )
    {
        m_pointer = i_rhs.m_pointer;

        return * this;
    }
    
    template< typename w_base_rhs > Iterator( Iterator<w_base_rhs> i_rhs )
      : m_pointer( i_rhs.m_pointer )
    {
    }

    inline void unlink()
    {
        xC_List_Entry                                   * l_entry = Dereference();

        l_entry->xF_Unlink( false );
    }
};


// ======== ListAccessorDeclare ===================================
/**
 * This declares all the accessor methods for a particular iterator
 * level.
 *
 */

template<
    typename    w_entry_class_name,
    typename    w_base_class = ListNothing_Basic,
    typename    w_error_traits = List_Error_Traits
>
class ListAccessorDeclare
  : public w_base_class
{
    public:

    virtual bool GetIterator( Iterator<w_entry_class_name> & o_iterator )
    {
        w_error_traits::IteratorUndefined();
        return false;
    }

};


// ======== ListElementBase ========================================
/**
 * The list element base method.
 *
 */

template<
    typename    w_payload_class,
    typename    w_default_mutex_type = List_EmptyMutex
>
class ListElementBase
{
    public:

    typedef w_default_mutex_type                xW_DefaultMutexType;
    typedef w_payload_class                     xW_PayloadType;

    virtual ~ListElementBase()
    {
    }

};

// ======== ListElementBase ========================================
/**
 * The list element base class with an extra base element.
 *
 */

template<
    typename    w_payload_class,
    typename    w_base_class,
    typename    w_default_mutex_type = List_EmptyMutex
>
class ListElementBaseEx
  : public w_base_class
{
    public:

    typedef w_default_mutex_type                xW_DefaultMutexType;
    typedef w_payload_class                     xW_PayloadType;
    typedef w_base_class                        xW_ExBaseClass;

    virtual ~ListElementBaseEx()
    {
    }
};


// ======== List_Traits ============================================
/**
 * This class determines hoe various features of the list behave.
 *
 * The user may supply their own version of this class.
 */

template <typename w_payload_type, typename w_accessor_traits>
class List_Traits
{
    public:

    enum {
        
        /**
         * DoDereferenceCheck set to a non-zero value will
         * perform a consistantcy check on dereferencing an iterator.
         */
        DoDereferenceCheck = 1
    };


    // ======== ContainerDestructionNotification ===============================
    /**
     *  ContainerDestructionNotification is a method that allows an element
     *  to perform special functions on destruction of it's container.
     *
     *  On a container that has an "OWNER" relationship, it is desirable
     *  that this method would cause the element to be deleted.
     *
     *  This method MUST at least unlink the object from the list.
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline bool ContainerDestructionNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
        i_entry->xF_RemoveSelf( false );

        return true;
    } // end ContainerDestructionNotification


    // ======== ContainerInsertionNotification ========================
    /**
     * This is called to prior to insertion the item into the container.
     *
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline void ContainerInsertionNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
    }
    
    // ======== ContainerRemovalNotification ========================
    /**
     * This is called to prior post removal of the container
     *
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline void ContainerRemovalNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
    }
    
    // ======== DestructContainerRemovalNotification ========================
    /**
     * Called when the the entry has been removed but while the object
     * is in destruction.
     *
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline void DestructContainerRemovalNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
    }
    

};

// ======== List_RefCountTraits ============================================
/**
 * Reference counted traits calls the "AddRef" and "Release" methods on the
 * object being inserted or deletd.
 *
 */

template <typename w_payload_type, typename w_accessor_traits>
class List_RefCountTraits
{
    public:

    enum {
        
        /**
         * DoDereferenceCheck set to a non-zero value will
         * perform a consistantcy check on dereferencing an iterator.
         */
        DoDereferenceCheck = 1
    };


    // ======== ContainerDestructionNotification ===============================
    /**
     *  ContainerDestructionNotification is a method that allows an element
     *  to perform special functions on destruction of it's container.
     *
     *  On a container that has an "OWNER" relationship, it is desirable
     *  that this method would cause the element to be deleted.
     *
     *  This method MUST at least unlink the object from the list.
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline bool ContainerDestructionNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
        i_entry->xF_RemoveSelf( false );

        return true;
    } // end ContainerDestructionNotification


    // ======== ContainerInsertionNotification ========================
    /**
     * This is called to prior to insertion the item into the container.
     *
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline void ContainerInsertionNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
        i_entry->AddRef();
    }
    
    // ======== ContainerRemovalNotification ========================
    /**
     * This is called to prior post removal of the container
     *
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline void ContainerRemovalNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
        i_entry->Release();
    }
    
    // ======== ContainerRemovalNotification ========================
    /**
     * This is called to prior post removal of the container
     *
     *
     * @param i_entry is the entry
     * @return nothing
     */ 

    template <
        typename w_list_types
    >
    static inline void DestructContainerRemovalNotification(
        typename w_list_types::xC_List_Entry        * i_entry
    )
    {
        i_entry->Release();
    }
    

};



// ======== List_Types =============================================
/**
 * List_Types contains component types for creating lists.  Depending
 * on parameters, it will define various types for managing a doubly linked
 * list.
 */

template <
    typename w_foward_class,
    typename w_base_class,
    typename w_accessor_traits,
    typename w_payload_type = typename w_base_class::xW_PayloadType,
    typename w_mutex_type = typename w_payload_type::xW_DefaultMutexType
>
class List_Types
{
    public:

    class xC_List_Head;
    typedef List_Types   xC_List_Types;

        
    // ======== xC_List_Entry =========================================
    /**
     * This is the list entry.  The entry know how to insert itself
     * remove itself, delete itself as well as knowing how to find the 
     * payload it inherits.
     */

    class xC_List_Entry
      : public w_base_class
    {
        public:

        typedef xC_List_Entry                               txC_List_Entry;
        typedef xC_List_Head                                txC_List_Head;
        typedef w_payload_type                              txW_PayloadType;
        typedef w_mutex_type                                txC_MutexType;
        typedef w_accessor_traits                           txC_List_Traits;
        typedef w_foward_class                              txW_Forward_Class;

        typedef xC_List_Head                                list;

        typedef xC_List_Entry * xC_List_Entry::*            txC_Entry_MemberPtr;
        
        inline bool xF_InList()
        {
            return m_head != 0;
        }


        // ======== xC_List_Entry =====================================
        /**
         * copy constructor - this must not be linked
         *
         */

        xC_List_Entry( const xC_List_Entry & i_other )
          : m_head( 0 )
        {
        } // end xC_List_Entry

        // ======== operator = ( const xC_List_Entry & ) ==============
        /**
         * assignment operator must do nothing.  The object remains in
         * list it is in.
         */

        xC_List_Entry & operator = ( const xC_List_Entry & i_other )
        {
            return * this;
        }
        

        // ======== xF_Inserter =======================================
        /**
         *  Inserts an element on either side of an element already in the list.
         *
         * @param i_entry is the entry to insert
         * @return nothing
         */
        
        void xF_Inserter(
            bool                                        w_call_notifier,
            xC_List_Entry * xC_List_Entry::*            w_elem_pos,
            xC_List_Entry * xC_List_Entry::*            w_elem_neg,
            xC_List_Entry * xC_List_Head::*             w_head_pos,
            xC_List_Entry * xC_List_Head::*             w_head_neg,
            xC_List_Entry                             * i_entry
        )
        {

            if ( w_call_notifier )
            {
                // notify the object that it is about to be added to a container
                // this is done prior to removal from a different container
                // so that reference counted objects won't get deleted
                // and then inserted.
                w_accessor_traits::template ContainerInsertionNotification< xC_List_Types >( i_entry );

                // if w_call_notifier is false - we assume that it is already removed
                if ( i_entry->xF_InList() )
                {
                    i_entry->xF_RemoveSelf( false );
                }
            }

            xC_List_Head  * l_head = m_head;
                
            AT_Assert( l_head != 0 );

            // this portion has to be performed in a lock
            {
                typename txC_MutexType::t_Lock     l_lock( l_head->m_mutex );

                //
                // Use the same head
                //
                i_entry->m_head = l_head;
                
                // increment count
                l_head->m_count ++;

                // get what we're pointing to in our direction
                //
                xC_List_Entry * l_tmp = this->*w_elem_pos;

                // we now point to the new object
                this->*w_elem_pos = i_entry;
                
                // the new entry can now insert itself
                i_entry->*w_elem_neg = this;
                i_entry->*w_elem_pos = l_tmp;

                // Now, if we were at the end of the list
                // we need to fix the head
                if ( l_tmp == 0 )
                {
                    l_head->*w_head_pos = i_entry;
                }
                else
                {
                    l_tmp->*w_elem_neg = i_entry;
                }

            }

        } // end xF_Inserter
        
        inline void xF_InsertBack( xC_List_Entry * i_entry )
        {
            xF_Inserter(true,&xC_List_Entry::m_forw,&xC_List_Entry::m_back,&xC_List_Head::m_end,&xC_List_Head::m_begin, i_entry );
        }       
        
        inline void xF_InsertForw( xC_List_Entry * i_entry )
        {
            xF_Inserter(true,&xC_List_Entry::m_back,&xC_List_Entry::m_forw,&xC_List_Head::m_begin,&xC_List_Head::m_end, i_entry );
        }       
        
        inline void xF_RemoveSelf( const bool i_in_destruct )
        {
            // this portion has to be performed in a lock
            {
                typename txC_MutexType::t_Lock     l_lock( m_head->m_mutex );
                

                xC_List_Entry * l_back = m_back;
                xC_List_Entry * l_forw = m_forw;
                xC_List_Head  * l_head = m_head;

                //
                // remove it
                m_head = 0;
                l_head->m_count --;

                if ( l_back == 0 )
                {
                    l_head->m_begin = l_forw;
                }
                else
                {
                    l_back->m_forw = l_forw;
                }

                if ( l_forw == 0 )
                {
                    l_head->m_end = l_back;
                }
                else
                {
                    l_forw->m_back = l_back;
                }

            }

            // notify the object of it's removal
            if ( ! i_in_destruct )
            {
                w_accessor_traits::template ContainerRemovalNotification< xC_List_Types >( this );
            }
            else
            {
                w_accessor_traits::template DestructContainerRemovalNotification< xC_List_Types >( this );
                
            }
        }


        // ======== xF_Unlink =========================================
        /**
         *  This will check for the entry's allocation in a list and
         *  will remove itself.
         *
         * @return result of unlink operation:
         *          - true if the element was actually removed
         *          - false if the element was not within a list
         *            hence no removal was needed.
         */
        
        inline bool xF_Unlink( bool i_in_destruct )
        {
            if ( xF_InList() )
            {
                xF_RemoveSelf( i_in_destruct );
                return true;
            }

            return false;
            
        } // end xF_Unlink
        

        ~xC_List_Entry()
        {
            xF_Unlink( true );
        }

        xC_List_Entry()
          : m_head()
        {
        }

        //
        // these three pointers point to the list entries.
        //

        xC_List_Entry                               * m_back;
        xC_List_Entry                               * m_forw;
        xC_List_Head                                * m_head;

    };
    

    // ======== xC_List_Head ==========================================
    /**
     * xC_List_Head is the basic list head type.  This is used as the
     * anchor of the list.
     *
     */

    class xC_List_Head
    {
        public:

        typedef xC_List_Entry                       txC_List_Entry;
        typedef w_mutex_type                        xW_MutexType;
        typedef xC_List_Entry * xC_List_Head::*     txC_Head_MemberPtr;

        // ======== iterator ==========================================
        /**
         * This is not exactly the same type as the
         * ListAccessorDeclare::GetIterator() iterator but it base
         * types are assignable since it contains the same UnionPtrType.
         */

        typedef Iterator<xC_List_Entry>          iterator;

        void xF_Pusher(
            xC_List_Entry * xC_List_Entry::*            w_elem_pos,
            xC_List_Entry * xC_List_Entry::*            w_elem_neg,
            xC_List_Entry * xC_List_Head::*             w_head_pos,
            xC_List_Entry * xC_List_Head::*             w_head_neg,
            xC_List_Entry                             * i_entry
        )
        {

            // notify the object that it is about to be added to a container
            // this is done prior to removal from a different container
            // so that reference counted objects won't get deleted
            // and then inserted.
            w_accessor_traits::template ContainerInsertionNotification< xC_List_Types >( i_entry );
            
            if ( i_entry->xF_InList() )
            {
                i_entry->xF_RemoveSelf( false );
            }

            {
                typename xW_MutexType::t_Lock       l_lock( m_mutex );
                
                xC_List_Entry * l_pos = this->*w_head_pos;

                //
                // if either pointer is nil, it indicates that the
                // list is empty - this being the first, we add it...
                if ( l_pos == 0 )
                {

                    AT_Assert( m_count == 0 );

                    i_entry->m_head = this;
                    i_entry->m_back = 0;
                    i_entry->m_forw = 0;
                    
                    this->m_begin = i_entry;
                    this->m_end = i_entry;
                    this->m_count = 1;
                    
                }
                else
                {
                    //
                    // use the inserter to insert now.
                    l_pos->xF_Inserter( false, w_elem_pos, w_elem_neg, w_head_pos, w_head_neg, i_entry );
                }

            }
            
        }

        inline void push_back( typename xC_List_Entry::txW_Forward_Class & i_entry )
        {
            xF_Pusher(
                &xC_List_Entry::m_forw,&xC_List_Entry::m_back,&xC_List_Head::m_end,&xC_List_Head::m_begin,
                static_cast< xC_List_Entry * >( & i_entry )
            );
        }       
        
        inline void push_front( typename xC_List_Entry::txW_Forward_Class & i_entry )
        {
            xF_Pusher(
                &xC_List_Entry::m_back,&xC_List_Entry::m_forw,&xC_List_Head::m_begin,&xC_List_Head::m_end,
                static_cast< xC_List_Entry * >( & i_entry )
            );
        }

        inline void push_back( iterator i_entry )
        {
            xF_Pusher(
                &xC_List_Entry::m_forw,&xC_List_Entry::m_back,&xC_List_Head::m_end,&xC_List_Head::m_begin,
                i_entry.Dereference()
            );
        }       
        
        inline void push_front( iterator i_entry )
        {
            xF_Pusher(
                &xC_List_Entry::m_back,&xC_List_Entry::m_forw,&xC_List_Head::m_begin,&xC_List_Head::m_end,
                i_entry.Dereference()
            );
        }

        inline iterator begin()
        {
            iterator        l_iter;

            xC_List_Entry * l_begin = m_begin;

            if ( l_begin == 0 )
            {
                l_iter.m_pointer = iterator::xF_EndIterator( this );
            }
            else
            {
                l_iter.m_pointer = l_begin;
            }

            return l_iter;
        }

        inline iterator end()
        {
            iterator        l_iter;

            l_iter.m_pointer = iterator::xF_EndIterator( this );

            return l_iter;
        }

        inline Uint32 size()
        {
            return m_count;
        }       

        xC_List_Head()
          : m_begin( 0 ),
            m_end( 0 ),
            m_count( 0 )
        {
        }


        ~xC_List_Head()
        {
            // this is where we keep on asking each entry to
            // delete/remove themselves - it may do whatever
            // it likes just as long as it unlinks itself.

            for ( xC_List_Entry * l_entry = m_begin; l_entry != 0; l_entry = m_begin )
            {
                //
                // this is where we call a user provided method to perform
                // the action.
                //

//              List_Traits<w_payload_type,ListNothing_Basic>
                w_accessor_traits::template ContainerDestructionNotification< xC_List_Types >( l_entry );
//              l_entry->xF_RemoveSelf();

            }
            
        }

        //
        // member data for the list head
        //
        
        xC_List_Entry                               * m_begin;
        xC_List_Entry                               * m_end;
        Uint32                                     m_count;
        xW_MutexType                                  m_mutex;

    };

};



// ======== List1 ==================================================
/**
 * List1 creates a class that is a member a single list at a time.
 *
 * The AT_ListN classes are traits classes, they contain no data, their
 * purpose is to provide a method of deriving more complex objects.
 */

template <
    typename    w_app_class,
    typename    w_default_mutex_type = List_EmptyMutex
>
class List1
{
    public:

    typedef w_app_class                             xW_App_Class;
    
    //
    // Forward declare some critical classes
    //
    class xC_Parent;
    typedef typename xW_App_Class::template Payload< xC_Parent > Payload;
    class xC_ListEntry_1;
    typedef xC_ListEntry_1 Apogee;

    //
    // create a few typedefs for making things visible
    typedef typename xW_App_Class::Root_Base        xC_RootBase;
    typedef typename xW_App_Class::Error_Traits     xC_Error_Traits;
    typedef typename xW_App_Class::template Entry_1_Traits<Payload> xC_Entry_1_Traits;

    class xC_Parent
      : public xC_RootBase
    {
        public:

        typedef w_default_mutex_type                xW_DefaultMutexType;
        typedef Payload                             xW_PayloadType;
        typedef List1<w_app_class,w_default_mutex_type> xW_Wapper;

        virtual ~xC_Parent() {}
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator ) = 0;

    };

    class xC_ListEntry_1
        : public List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Entry
    {
        public:
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator )
        {
            o_iterator.m_pointer = this;
            return true;
        }
    
    };

    typedef typename List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Head          list_1;
    
};

// ======== List2 ==================================================
/**
 * List2 creates a class that is a member of 2 lists at a time.
 *
 * The AT_ListN classes are traits classes, they contain no data, their
 * purpose is to provide a method of deriving more complex objects.
 */

template <
    typename    w_app_class,
    typename    w_default_mutex_type = List_EmptyMutex
>
class List2
{
    public:

    typedef w_app_class                             xW_App_Class;
    
    //
    // Forward declare some critical classes
    //
    class xC_Parent;
    typedef typename xW_App_Class::template Payload< xC_Parent > Payload;
    class xC_ListEntry_1;
    class xC_ListEntry_2;
    class Apogee;
    
    //
    // create a few typedefs for making things visible
    typedef typename xW_App_Class::Root_Base        xC_RootBase;
    typedef typename xW_App_Class::Error_Traits     xC_Error_Traits;
    typedef typename xW_App_Class::template Entry_1_Traits<Payload> xC_Entry_1_Traits;
    typedef typename xW_App_Class::template Entry_2_Traits<Payload> xC_Entry_2_Traits;

    class xC_Parent
      : public xC_RootBase
    {
        public:

        typedef w_default_mutex_type                xW_DefaultMutexType;
        typedef Payload                             xW_PayloadType;
        typedef List2<w_app_class,w_default_mutex_type> xW_Wapper;

        virtual ~xC_Parent() {}
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator ) = 0;
        virtual bool GetIterator( Iterator<xC_ListEntry_2> & o_iterator ) = 0;

    };

    class xC_ListEntry_1
        : public List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Entry
    {
        public:
        typedef Payload xW_PayloadType; //-- workaround VC++7.1 bug
    };

    class xC_ListEntry_2
        : public List_Types< xC_ListEntry_2, xC_ListEntry_1, xC_Entry_2_Traits >::xC_List_Entry
    {
        public:
        
    };

    class Apogee
      : public xC_ListEntry_2
    {
        public:
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_1 * >( this );
            return true;
        }
    
        virtual bool GetIterator( Iterator<xC_ListEntry_2> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_2 * >( this );
            return true;
        }
    };

    typedef typename List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Head          list_1;
    typedef typename List_Types< xC_ListEntry_2, xC_ListEntry_1, xC_Entry_2_Traits >::xC_List_Head   list_2;

    template <
        typename    w_list_type
    >
    static typename w_list_type::iterator GetIterator( xC_Parent & i_base )
    {
        typedef typename w_list_type::txC_List_Entry::txW_Forward_Class   t_entry_base;
        Iterator< t_entry_base >                                         l_iterator;

        i_base.GetIterator( l_iterator );

        return l_iterator;
    }   
    
    template <
        typename    w_list_type
    >
    static typename w_list_type::iterator GetIterator( const w_list_type & i_list, xC_Parent & i_base )
    {
        return GetIterator<w_list_type>( i_base );
    }
};

// ======== List3 ==================================================
/**
 * List3 creates a class that is a member of 3 lists.
 *
 * The AT_ListN classes are traits classes, they contain no data, their
 * purpose is to provide a method of deriving more complex objects.
 */

template <
    typename    w_app_class,
    typename    w_default_mutex_type = List_EmptyMutex
>
class List3
{
    public:

    typedef w_app_class                             xW_App_Class;
    
    //
    // Forward declare some critical classes
    //
    class xC_Parent;
    typedef typename xW_App_Class::template Payload< xC_Parent > Payload;
    class xC_ListEntry_1;
    class xC_ListEntry_2;
    class xC_ListEntry_3;
    class Apogee;
    
    //
    // create a few typedefs for making things visible
    typedef typename xW_App_Class::Error_Traits     xC_Error_Traits;
    typedef typename xW_App_Class::Root_Base        xC_RootBase;
    typedef typename xW_App_Class::template Entry_1_Traits<Payload> xC_Entry_1_Traits;
    typedef typename xW_App_Class::template Entry_2_Traits<Payload> xC_Entry_2_Traits;
    typedef typename xW_App_Class::template Entry_3_Traits<Payload> xC_Entry_3_Traits;

    class xC_Parent
      : public xC_RootBase
    {
        public:

        typedef w_default_mutex_type                xW_DefaultMutexType;
        typedef Payload                             xW_PayloadType;
        typedef List3<w_app_class,w_default_mutex_type> xW_Wapper;

        virtual ~xC_Parent() {}
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator ) = 0;
        virtual bool GetIterator( Iterator<xC_ListEntry_2> & o_iterator ) = 0;
        virtual bool GetIterator( Iterator<xC_ListEntry_3> & o_iterator ) = 0;

    };

    class xC_ListEntry_1
        : public List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Entry
    {
        public:
        typedef Payload xW_PayloadType; //-- workaround VC++7.1 bug        
    };

    class xC_ListEntry_2
        : public List_Types< xC_ListEntry_2, xC_ListEntry_1, xC_Entry_2_Traits >::xC_List_Entry
    {
        public:
        typedef Payload xW_PayloadType; //-- workaround VC++7.1 bug
    };

    class xC_ListEntry_3
        : public List_Types< xC_ListEntry_3, xC_ListEntry_2, xC_Entry_3_Traits >::xC_List_Entry
    {
        public:
        
    };

    class Apogee
      : public xC_ListEntry_3
    {
        public:
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_1 * >( this );
            return true;
        }
    
        virtual bool GetIterator( Iterator<xC_ListEntry_2> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_2 * >( this );
            return true;
        }
        
        virtual bool GetIterator( Iterator<xC_ListEntry_3> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_3 * >( this );
            return true;
        }
    };

    typedef typename List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Head          list_1;
    typedef typename List_Types< xC_ListEntry_2, xC_ListEntry_1, xC_Entry_2_Traits >::xC_List_Head   list_2;
    typedef typename List_Types< xC_ListEntry_3, xC_ListEntry_2, xC_Entry_3_Traits >::xC_List_Head   list_3;

    template <
        typename    w_list_type
    >
    static typename w_list_type::iterator GetIterator( xC_Parent & i_base )
    {
        typedef typename w_list_type::txC_List_Entry::txW_Forward_Class   t_entry_base;
        Iterator< t_entry_base >                                         l_iterator;

        i_base.GetIterator( l_iterator );

        return l_iterator;
    }   
    
    template <
        typename    w_list_type
    >
    static typename w_list_type::iterator GetIterator( const w_list_type & i_list, xC_Parent & i_base )
    {
        return GetIterator<w_list_type>( i_base );
    }
};

// ======== List4 ==================================================
/**
 * List4 creates a class that is a member of 4 lists.
 *
 * The AT_ListN classes are traits classes, they contain no data, their
 * purpose is to provide a method of deriving more complex objects.
 */

template <
    typename    w_app_class,
    typename    w_default_mutex_type = List_EmptyMutex
>
class List4
{
    public:

    typedef w_app_class                             xW_App_Class;
    
    //
    // Forward declare some critical classes
    //
    class xC_Parent;
    typedef typename xW_App_Class::template Payload< xC_Parent > Payload;
    class xC_ListEntry_1;
    class xC_ListEntry_2;
    class xC_ListEntry_3;
    class xC_ListEntry_4;
    class Apogee;
    
    //
    // create a few typedefs for making things visible
    typedef typename xW_App_Class::Root_Base        xC_RootBase;
    typedef typename xW_App_Class::Error_Traits     xC_Error_Traits;
    typedef typename xW_App_Class::template Entry_1_Traits<Payload> xC_Entry_1_Traits;
    typedef typename xW_App_Class::template Entry_2_Traits<Payload> xC_Entry_2_Traits;
    typedef typename xW_App_Class::template Entry_3_Traits<Payload> xC_Entry_3_Traits;
    typedef typename xW_App_Class::template Entry_4_Traits<Payload> xC_Entry_4_Traits;

    class xC_Parent
      : public xC_RootBase
    {
        public:

        typedef w_default_mutex_type                xW_DefaultMutexType;
        typedef Payload                             xW_PayloadType;
        typedef List4<w_app_class,w_default_mutex_type> xW_Wapper;

        virtual ~xC_Parent() {}
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator ) = 0;
        virtual bool GetIterator( Iterator<xC_ListEntry_2> & o_iterator ) = 0;
        virtual bool GetIterator( Iterator<xC_ListEntry_3> & o_iterator ) = 0;
        virtual bool GetIterator( Iterator<xC_ListEntry_4> & o_iterator ) = 0;

    };

    class xC_ListEntry_1
        : public List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Entry
    {
        public:        
        typedef Payload xW_PayloadType; //-- workaround VC++7.1 bug
    };

    class xC_ListEntry_2
        : public List_Types< xC_ListEntry_2, xC_ListEntry_1, xC_Entry_2_Traits >::xC_List_Entry
    {
        public:
        typedef Payload xW_PayloadType; //-- workaround VC++7.1 bug
    };

    class xC_ListEntry_3
        : public List_Types< xC_ListEntry_3, xC_ListEntry_2, xC_Entry_3_Traits >::xC_List_Entry
    {
        public:
        typedef Payload xW_PayloadType; //-- workaround VC++7.1 bug
    };

    class xC_ListEntry_4
        : public List_Types< xC_ListEntry_4, xC_ListEntry_3, xC_Entry_4_Traits >::xC_List_Entry
    {
        public:
        typedef Payload xW_PayloadType; //-- workaround VC++7.1 bug
    };

    class Apogee
      : public xC_ListEntry_4
    {
        public:
        
        virtual bool GetIterator( Iterator<xC_ListEntry_1> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_1 * >( this );
            return true;
        }
    
        virtual bool GetIterator( Iterator<xC_ListEntry_2> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_2 * >( this );
            return true;
        }
        
        virtual bool GetIterator( Iterator<xC_ListEntry_3> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_3 * >( this );
            return true;
        }
        
        virtual bool GetIterator( Iterator<xC_ListEntry_4> & o_iterator )
        {
            o_iterator.m_pointer = static_cast< xC_ListEntry_4 * >( this );
            return true;
        }
    };

    typedef typename List_Types< xC_ListEntry_1, Payload, xC_Entry_1_Traits >::xC_List_Head          list_1;
    typedef typename List_Types< xC_ListEntry_2, xC_ListEntry_1, xC_Entry_2_Traits >::xC_List_Head   list_2;
    typedef typename List_Types< xC_ListEntry_3, xC_ListEntry_2, xC_Entry_3_Traits >::xC_List_Head   list_3;
    typedef typename List_Types< xC_ListEntry_4, xC_ListEntry_3, xC_Entry_4_Traits >::xC_List_Head   list_4;

    template <
        typename    w_list_type
    >
    static typename w_list_type::iterator GetIterator( xC_Parent & i_base )
    {
        typedef typename w_list_type::txC_List_Entry::txW_Forward_Class   t_entry_base;
        Iterator< t_entry_base >                                         l_iterator;

        i_base.GetIterator( l_iterator );

        return l_iterator;
    }   
    
    template <
        typename    w_list_type
    >
    static typename w_list_type::iterator GetIterator( const w_list_type & i_list, xC_Parent & i_base )
    {
        return GetIterator<w_list_type>( i_base );
    }
};

// ======== Default_List_Traits ====================================
/**
 * This contains a set of default definitions of class traits to be used
 * with the AT_ListN templates.
 */

class Default_List_Traits
{
    public:

    // base class for all.
    typedef ListNothing_Basic                               Root_Base;

    typedef List_Error_Traits                               Error_Traits;

    template <
        typename    w_container_wrapper
    >
    class Entry_1_Traits
      : public List_Traits< w_container_wrapper, ListNothing_Basic >
    {
    };
    
    template <
        typename    w_container_wrapper
    >
    class Entry_2_Traits
      : public List_Traits< w_container_wrapper, ListNothing_Basic >
    {
    };
    
    template <
        typename    w_container_wrapper
    >
    class Entry_3_Traits
      : public List_Traits< w_container_wrapper, ListNothing_Basic >
    {
    };
    
    template <
        typename    w_container_wrapper
    >
    class Entry_4_Traits
      : public List_Traits< w_container_wrapper, ListNothing_Basic >
    {
    };
    
};


// end FlexibleList



// ======== ListHead_Basic ============================================
/**
 * This creates a basic "head" class
 *
 */

template <
    typename w_PayloadClass,
    typename w_BaseClass,
    typename w_TraitsClass = List_Traits< w_PayloadClass, ListNothing_Basic >
>
class ListHead_Basic
  : public List_Types< ListHead_Basic<w_PayloadClass, w_BaseClass, w_TraitsClass >, w_BaseClass, w_TraitsClass >::xC_List_Entry
{
	public:

	virtual bool GetIterator( Iterator<ListHead_Basic> & o_iterator )
	{
		o_iterator.m_pointer = this;
		
		return true;
	}

};


/**
 * @}
 */

}; // namespace
#endif // x_at_list_h_x


