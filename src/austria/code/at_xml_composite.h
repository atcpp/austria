// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_composite.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_composite_h_x
#define x_at_xml_composite_h_x 1

#include "at_xml_node.h"

// Austria namespace
namespace at
{
    /**
     *  XmlComposite is the base class for XML nodes that can have child nodes, i.e. it is the
     *  base class of classes that implement composite objects in the Composite design pattern.
     *  
     */
    class XmlComposite : public XmlNode
    {
    public:

        /**
         *  @see XmlNode::HasChildNodes.
         */
        virtual bool HasChildNodes() const;

        /**
         *  @see XmlNode::ChildNodes.
         */
        virtual XmlNodeList ChildNodes() const;

        /**
         *  @see XmlNode::FirstChild.
         */
        virtual PtrView< XmlNode* > FirstChild() const;

        /**
         *  @see XmlNode::LastChild.
         */
        virtual PtrView< XmlNode* > LastChild() const;

        /**
         *  @see XmlNode::InsertBefore.
         */
        virtual PtrView< XmlNode* > InsertBefore( PtrDelegate< XmlNode* > i_newChild,
                                                  PtrView< XmlNode* >     i_refChild );

        /**
         *  @see XmlNode::AppendChild.
         */
        virtual PtrView< XmlNode* > AppendChild( PtrDelegate< XmlNode* > i_newChild );

        /**
         *  @see XmlNode::RemoveChild.
         */
        virtual PtrDelegate< XmlNode* > RemoveChild( PtrDelegate< XmlNode* > i_oldChild );

        /**
         *  @see XmlNode::ReplaceChild.
         */
        virtual PtrDelegate< XmlNode* > ReplaceChild( PtrDelegate< XmlNode* > i_newChild,
                                                      PtrDelegate< XmlNode* > i_oldChild );

    protected:

        /**
         *  @brief  The node constructor.
         *  @param  i_ownerDocument The XmlDocument that creates this node.
         */
        XmlComposite( PtrView< XmlDocument* > i_ownerDocument ) :
            XmlNode( i_ownerDocument ),
            m_firstChild( 0 ),
            m_lastChild( 0 )
            {}

        /**
         *  @brief  Checks if a node can be a child of this node.
         *  @exception ExceptionDerivation<XmlHierarchyRequestErr>
         *             Raised if i_newChild is an ancestor of this node.
         *  @exception ExceptionDerivation<XmlWrongDocumentErr>
         *             Raised if i_newChild and this node were not created by the same document.
         */
        virtual void CheckHierarchyRequest( PtrView< const XmlNode* > i_newChild ) const;

    private:

        /**
         *  m_firstChild
         */
        Ptr< XmlNode* > m_firstChild;

        /**
         *  m_lastChild
         */
        PtrView< XmlNode* > m_lastChild;

    };

}; // namespace

#endif // x_at_xml_composite_h_x
