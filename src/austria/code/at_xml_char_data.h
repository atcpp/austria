// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_char_data.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_char_data_h_x
#define x_at_xml_char_data_h_x 1

#include "at_xml_node.h"

// Austria namespace
namespace at
{
    /**
     * The XmlCharacterData class is the base class for XmlNode classes that share a common set of
     * attributes and methods for accessing character data in the XML document model.
     * 
     * @note All offsets in the XmlCharacterData interface start from 0.
     */
    class XmlCharacterData : public XmlNode
    {
    public:

        /**
         *  @brief  The value of the node, which is the character data of the node.
         */
        virtual std::string Value() const;

        /**
         *  @brief  The character data of the node.
         */
        const std::string& Data() const { return m_data; }

        /**
         *  @brief  The number of characters in the data, not including any null-termination.
         */
        std::string::size_type Length() const { return m_data.length(); }

        /**
         *  @brief  Appends a string to the end of the character data of the node.
         *  @param  i_str  The string to append.
         */
        void AppendData( const std::string& i_str ) { m_data.append( i_str ); }

        /**
         *  @brief  Remove a range of characters.
         *  @param  i_offset The character offset from which to start removing.
         *  @param  i_count  The number of charaters to delete.
         *  @exception ExceptionDerivation<XmIndexSizeErr> if i_offset is negative or greater than the
         *             number of characters in the data.
         *
         *  If the sum of i_offset and i_count exceeds Length() then all charaters from i_offset to
         *  the end of the data are deleted.
         */
        void DeleteData( std::string::size_type i_offset, std::string::size_type i_count );

        /**
         *  @brief  Inserts a string.
         *  @param  i_offset The character offset at which to insert.
         *  @param  i_str    The string to insert.
         *  @exception ExceptionDerivation<XmIndexSizeErr> if i_offset is negative or greater than the
         *             number of characters in the data.
         */
        void InsertData( std::string::size_type i_offset, const std::string& i_str );

        /**
         *  @brief Replaces a range of characters with a string.
         *  @param  i_offset The character offset from which to start replacing.
         *  @param  i_count  The number of charaters to replace.
         *  @param  i_str    The string with which the range must be replaced.
         *  @exception ExceptionDerivation<XmIndexSizeErr> if i_offset is negative or greater than the
         *             number of characters in the data.
         *
         *  If the sum of i_offset and i_count exceeds Length(), then all the characters up to the end of
         *  the data are replaced.
         */
        void ReplaceData( std::string::size_type i_offset,
                          std::string::size_type i_count,
                          const std::string&     i_str );

        /**
         *  @brief  Extracts a range of characters.
         *  @param  i_offset The character offset from which to start extracting.
         *  @param  i_count  The number of charaters to extract.
         *  @return The specified substring.
         *  @exception ExceptionDerivation<XmIndexSizeErr> if i_offset is negative or greater than the
         *             number of characters in the data.
         *
         *  If the sum of i_offset and i_count exceeds Length(), then all the characters up to the end of
         *  the data are returned.
         */
        std::string SubstringData( std::string::size_type i_offset, std::string::size_type i_count ) const;

    protected:

        /**
         *  @brief  The constructor.
         *  @param  i_ownerDocument The XmlDocument that creates this node.
         *  @param  i_data          The character data of the node.
         */
        XmlCharacterData( PtrView< XmlDocument* > i_ownerDocument, const std::string& i_data ) :
            XmlNode( i_ownerDocument ), m_data( i_data ) {}

    private:

        /**
         *  The character data of the node.
         */
        std::string m_data;

    };

}; // namespace

#endif // x_at_xml_char_data_h_x
