/**
 * /file at_crypto_base.h
 *
 * 
 */

/**
 * at_crypto_base.h is NOT intended as a public interface.  It is used
 * as a base for writing Cipher wrappers only.
 */


#include "at_crypto.h"

#define x_at_crypto_base_h_x 1
#ifndef x_at_crypto_base_h_x
#define x_at_crypto_base_h_x 1

namespace at_crypto
{

// forward declare the Cipher class
class Cipher;

// ======== Cipher_BadKey =============================================
/**
 * ExceptionDerivation<Cipher_BadKey> is thrown when SetKey is unable
 * to use the given key.
 *
 */

struct Cipher_BadKey {};

// ======== Cipher_ModeNotSupported ===================================
/**
 * ExceptionDerivation<Cipher_ModeNotSupported> is thrown when SetKey 
 * is called and the Cipher does not support the requested mode.
 *
 */

struct Cipher_ModeNotSupported {};


// ======== CipherSymmetricKey ========================================
/**
 * CipherSymmetricKey is a key generator for a symmetric key Cipher.
 *
 */

class AUSTRIA_EXPORT CipherSymmetricKey
  : public virtual PtrTarget_MT
{
    public:


    // ======== GenerateKey ===========================================
    /**
     * GenerateKey will create a key for the Cipher associated with
     * this key.
     *
     * @return nothing
     */

    virtual std::string GenerateKey() = 0;


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses a std::string parameter.
     *
     * @param o_new_key  A new key is written to this param
     * @return nothing
     */

    virtual void	GenerateKey(
        std::string             & o_new_key
    ) = 0;


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses Buffer parameters
     *
     * @param o_new_key     A new key is written to this param
     * @return nothing
     */

    virtual void	GenerateKey(
        PtrView<Buffer *>         o_new_public_key
    ) = 0;


    // ======== NewCipher =============================================
    /**
     * NewCipher returns a new cipher associated with this key generator
     *
     * @return nothing
     */

    virtual PtrDelegate<Cipher *> NewCipher() = 0;

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier() = 0;

};


// ======== CipherPublicKey ===========================================
/**
 * CipherPublicKey is a key generator for the public key cryptosystem.
 *
 */

class AUSTRIA_EXPORT CipherPublicKey
  : public virtual PtrTarget_MT
{
    public:

    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses a std::string parameter.
     *
     * @param o_new_public_key  A new public key is written to this param
     * @param o_new_private_key A new public key is written here
     * @return nothing
     */

    virtual void	GenerateKey(
        std::string             & o_new_public_key,
        std::string             & o_new_private_key
    ) = 0;


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses Buffer parameters
     *
     * @param o_new_public_key  A new public key is written to this param
     * @param o_new_private_key A new public key is written here
     * @return nothing
     */

    virtual void	GenerateKey(
        PtrView<Buffer *>         o_new_public_key,
        PtrView<Buffer *>         o_new_private_key
    ) = 0;


    // ======== NewCipher =============================================
    /**
     * NewCipher returns a new cipher associated with this key generator
     *
     * @return nothing
     */

    virtual PtrDelegate<Cipher *> NewCipher() = 0;

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier() = 0;
    
};


// ======== CipherProperty ============================================
/**
 * CipherProperty is returned from Cipher::GetProperty and depends
 * on the property being requested.
 *
 */

typedef at::Any<>           CipherProperty;

// ======== Cipher ====================================================
/**
 * This is the public cipher interface.  It these are generated from
 * a factory, seeded with a key and then can be used to encrypt or
 * decrypt data based on that key.
 *
 */

class AUSTRIA_EXPORT Cipher
  : public virtual PtrTarget_MT
{
    public:


    // ======== Mode ==================================================
    /**
     * Mode are the cipher modes
     *
     */

    enum Mode
    {
        /**
         * Scramble is the mode to scramble the input
         */
        Scramble,

        /**
         * Unscramble is the mode to decrypt the input
         */
        Unscramble 
    };


    // ======== Property ==============================================
    /**
     * Property is the list of Cipher properties that can be
     * requested.
     */

    enum Property
    {
        /**
         * IsSymmetric is the "symmetric" key cryptosystem property
         *
         */
        IsSymmetric,

        /**
         * IsPublic is the "public" key cryptosystem property
         *
         */
        IsPublic,

        /**
         * KeyGenerator returns a key generator for this Cipher.
         * The type of the key generator depends on the cipher type.
         *
         * GetProperty will return a CipherProperty object containing either
         * of the following types.
         *  at::Ptr<CipherSymmetricKey*>
         *  at::Ptr<CipherPublicKey*>
         *
         */

        KeyGenerator,
    };


    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this Cipher.
     *
     *
     * @param i_property is the requested property
     * @return A CipherProperty "any" type
     */

    virtual CipherProperty GetProperty(
        Property            i_property
    ) = 0;
    

    // ======== SetKey ================================================
    /**
     * SetKey is typically the first call to the Cipher.  This sets the
     * key to use for this cipher.
     *
     * SetKey also sets the "directionality" of the cipher i.e. Scramble
     * or Unscramble.  A Cipher may only support one or the other in the
     * case of public keys.  It is up to the application to use the
     * appropriate sense.  If a key is incapable of performing the said
     * mode a 
     *
     * @param i_key     Contains the key to use
     * @param i_mode    The type of Cipher wanted
     * @return nothing
     */

    virtual void SetKey(
        const std::string     i_key,
        Mode                  i_mode = Scramble
    ) = 0;


    // ======== SetKey ================================================
    /**
     * This is an overload of SetKey that takes an at::Buffer as a 
     * key.
     *
     * @param i_key     Contains the key to use
     * @return nothing
     */

    virtual void SetKey(
        PtrDelegate<const Buffer *>   i_key,
        Mode                          i_mode = Scramble
    ) = 0;


    // ======== WriteTo ===============================================
    /**
     * WriteTo sets the the buffer to write (append) the output
     * of the encryption.  The contents of the buffer are incomplete
     * unless "Flush" is called.  However, if Flushed is called, the contents
     * the buffer can no longer be appended to hence WriteTo must be called
     * again with a new buffer to write the encryption contents.
     *
     * @param i_output  The buffer to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        PtrDelegate<Buffer *>         o_output
    ) = 0;


    // ======== WriteTo ===============================================
    /**
     * Overloaded version of WriteTo for a std::string writer.  The
     * string passed to WriteTo will be accessed by Cipher when
     * data is added.
     *
     * @param i_output  The std::string to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        std::string                 & o_output
    ) = 0;


    // ======== Flush =================================================
    /**
     * Flush will finalize this cipher and pass the remaining data
     * to the output "WriteTo" buffer.
     *
     * @return nothing
     */

    virtual void Flush() = 0;
    

    // ======== AddData ===============================================
    /**
     * AddData will pass the added data through the Cipher
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        PtrView<const Buffer *>     i_data
    ) = 0;

    // ======== AddData ===============================================
    /**
     * Overload of AddData for std::string
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        const std::string     i_key
    ) = 0;

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier() = 0;


    // ======== OnePass ===============================================
    /**
     * OnePass is a simplified helper class to use to encrypt or decrypt.
     *
     * @param i_key     The key to use.
     * @param i_data    The data to encrypt
     * @param i_mode    The type of Cipher wanted
     * @return nothing
     */

    std::string OnePass(
        const std::string           & i_key,
        const std::string           & i_data,
        Mode                          i_mode
    ) {
        std::string     l_result;
        
        SetKey( i_key, i_mode );

        WriteTo( l_result );

        AddData( i_data );

        Flush();

        return l_result;
    }    

    // ======== Encrypt ===============================================
    /**
     * Encrypt is a helper function that takes a single key and input
     * buffer and performs a encryption.
     *
     * @param i_key     The key to use.
     * @param i_data    The data to encrypt
     * @return nothing
     */

    std::string Encrypt(
        const std::string           & i_key,
        const std::string           & i_data
    ) {
        return OnePass( i_key, i_data, Scramble );
    }
    
    // ======== Decrypt ===============================================
    /**
     * Decrypt is a helper function that takes a single key and input
     * buffer and performs a decryption.
     *
     * @param i_key     The key to use.
     * @param i_data    The data to encrypt
     * @return nothing
     */

    std::string Decrypt(
        const std::string           & i_key,
        const std::string           & i_data
    ) {
        return OnePass( i_key, i_data, Unscramble );
    }
    
};


} // end namespace


#endif // x_at_crypto_base_h_x

