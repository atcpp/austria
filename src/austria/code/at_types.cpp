/*
 * This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 * Any unauthorized use of this file is prohibited and will be prosecuted
 * to the full extent of the law.
 */
#include "at_types.h"

namespace at
{
    const SizeMem MaxSizeMem = (~(unsigned long)0)>>1;
}
