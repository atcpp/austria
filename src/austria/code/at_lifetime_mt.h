//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_lifetime_mt.h
 *
 */

#ifndef x_at_lifetime_mt_h_x
#define x_at_lifetime_mt_h_x 1

#include "at_exports.h"
#include "at_lifetime.h"
#include "at_atomic.h"


// Austria namespace
namespace at
{

// ======== PtrClassRefWrapperMT ========================================
/**
 * PtrClassRefWrapper contains the state to manage access to the
 * reference counted pointer for multi-threaded programs.  This allows
 * multiple threads to access a pointer atomically.  Non multi-thread safe
 * accesses are disallowed.
 *
 */

template <typename w_ClassRef>
class PtrClassRefWrapperMT
{
    /**
     * m_ptr contains the pointer to the reference
     * counted object.
     */
    AtomicExchange_Ptr<w_ClassRef>          m_ptr;

    public:
    /**
     * t_ClassRefWrapper default constructor
     */
    inline PtrClassRefWrapperMT()
      : m_ptr( 0 )
    {
    }

    /**
     * t_ClassRefWrapper default constructor
     */
    inline PtrClassRefWrapperMT( w_ClassRef i_ptr )
      : m_ptr( i_ptr )
    {
    }

    /**
     * SafeAccess is usually used in the destructor.
     */
    inline w_ClassRef SafeAccess()
    {
        w_ClassRef              l_ptr( m_ptr.Get() );

        // Should never be locked...
        AT_Assert( reinterpret_cast<w_ClassRef>( 1 ) != l_ptr );

        return l_ptr;
    }

    /**  
     * PurgePointer is used to clear the pointer for
     * instrumented pointers. 
     * 
     */
    
    inline void PurgePointer()
    {
    }

    /**
     * IncRefCountTest will test that the object exists
     * before incrementing the reference count.
     */
    template <typename w_ClassRefLhs, typename w_TraitsLhs>
    inline w_ClassRefLhs IncRefCountTest()
    {
        unsigned l_spin_count = 0;

        while ( 1 )
        {
            w_ClassRef              l_ptr( m_ptr.Get() );

            // if the pointer is currently null, then there is nothing to
            // do.
            if ( ! l_ptr )
            {
                return 0;
            }

            if ( reinterpret_cast<w_ClassRef>( 1 ) == l_ptr )
            {
                // Yield the CPU here if we spin too much
                if ( OSTraitsBase::s_smp_memory_latency < ++ l_spin_count )
                {
                    OSTraitsBase::SchedulerYield();
                }
            }
            else
            {
                
                // See if we can atomically set this thing
                if ( m_ptr.CompareExchange( reinterpret_cast<w_ClassRef>( 1 ), l_ptr ) == l_ptr )
                {
                    // Woo hoo - this pointer is ours.
    
                    w_ClassRefLhs       l_lhs_ptr = l_ptr;
                    w_TraitsLhs::IncRefCount( l_lhs_ptr );
    
                    // unlock the pointer by simply putting it back
                    m_ptr.Set( l_ptr );
    
                    return l_lhs_ptr;
                }
            }

        }
    }

    /**
     * SwapPtr will swap the i_ptr and the local pointer.
     * In a Ptr accessed in a multithreaded way, this must be
     * single threaded.
     */
    inline w_ClassRef SwapPtr( w_ClassRef i_ptr )
    {
        unsigned l_spin_count = 0;
        while ( 1 )
        {
            w_ClassRef              l_ptr( m_ptr.Get() );

            if ( reinterpret_cast<w_ClassRef>( 1 ) == l_ptr )
            {
                // Yield the CPU here if we spin too much
                if ( OSTraitsBase::s_smp_memory_latency < ++ l_spin_count )
                {
                    OSTraitsBase::SchedulerYield();
                }
            }
            else
            {
                // See if we can atomically set this thing
                if ( m_ptr.CompareExchange( i_ptr, l_ptr ) == l_ptr )
                {
                    return l_ptr;
                }
            }

        }
    }

    /**
     * SwapPtrCompare will perform a compare/exchange on the
     * pointer - use in cases where you want to provide
     * an atomic exchange on the pointer when it is expected
     * to be a particular value.
     *
     * @param i_new_ptr The pointer to be placed here.
     * @param i_cmp_ptr The pointer expected to be currently set
     * @return The value of m_ptr before this operation took place
     */

    inline w_ClassRef SwapPtrCompare(
        w_ClassRef          i_new_ptr,
        w_ClassRef          i_cmp_ptr
    ) {
        return m_ptr.CompareExchange( i_new_ptr, i_cmp_ptr );
    }


    private :
    // PtrClassRefWrapper does not allow copy construction or
    //

    // no copy constructor
    PtrClassRefWrapperMT( const PtrClassRefWrapperMT & i_val );

    // no assignment
    PtrClassRefWrapperMT & operator = ( const PtrClassRefWrapperMT & i_val );

    // No single threaded access for multi threaded pointer
    w_ClassRef & SingleThreadedAccess();

};


// ======== PtrTarget_MT ==============================================
/**
 * PtrTarget_MT is a multi-threaded reference counting class.
 *
 * Reference counts will use the AtomicCount object that performs
 * thread safe increment and decrements of counts.
 */

AUSTRIA_TEMPLATE_EXPORT(PtrTarget_Generic, AtomicCount)
typedef PtrTarget_Generic<AtomicCount> PtrTarget_MT;

template <typename w_ptr_style_traits>
class PtrTarget_Helpers< AtomicCount, w_ptr_style_traits >
{
    public:


    // ======== IsRefCountOne =========================================
    /**
     * IsRefCountOne check to see if there is a single reference to an
     * object.  This can be used in optimizations where objects can't
     * be shared but this check can eliminate the unnecessary creation
     * of a new object.
     *
     * @param i_ptr is the PtrTarget_Generic being checked
     * @return True is the reference count is currently 1
     */

    static bool IsRefCountOne(
        const PtrTarget_Generic<AtomicCount, w_ptr_style_traits> * i_ptr
    )
    {
        return 1 == i_ptr->m_ref_count.Get();
    }
};


// ======== PtrTarget_MTVirtual =======================================
/**
 * PtrTarget_MTVirtual is an implementation of PtrTarget using the
 * fully abstract interface PtrTarget.
 */

AUSTRIA_TEMPLATE_EXPORT2(PtrTarget_Generic, AtomicCount, PtrVirtualStyle)
typedef PtrTarget_Generic<AtomicCount, PtrVirtualStyle> PtrTarget_MTVirtual;


}; // namespace
#endif // x_at_lifetime_mt_h_x

