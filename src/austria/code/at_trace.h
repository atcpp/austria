//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_trace.h
 *
 * Support for tracing system or library calls.
 */

#ifndef x_at_trace_h_x
#define x_at_trace_h_x 1

#include "at_thread.h"

// Austria namespace
namespace at
{


// ======== Trace =====================================================
/**
 * Trace is a template function that can be used to print the input
 * and output of arbitrary function calls (up to 4 parameters.
 *
 * @return the return value of the function
 */


template <
    typename w_TR,
    typename w_T1,
    typename w_T2,
    typename w_T3,
    typename w_T4,
    typename w_Tp1,
    typename w_Tp2,
    typename w_Tp3,
    typename w_Tp4,
    int w_N
>
inline w_TR Trace(
    w_TR ( * i_func )( w_T1, w_T2, w_T3, w_T4 ),
    const char ( & i_str )[w_N],
    int                     i_line,
    w_Tp1                   i_v1,
    w_Tp2                   i_v2,
    w_Tp3                   i_v3,
    w_Tp4                   i_v4
)
{
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "- " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << ", "
            << i_v2 << ", "
            << i_v3 << ", "
            << i_v4 << " ):"
        << i_line << std::endl;
        std::cerr << l_strm.str();
    }
    
    w_TR l_result = ( * i_func )( i_v1, i_v2, i_v3, i_v4 );
    
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "+ " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << ", "
            << i_v2 << ", "
            << i_v3 << ", "
            << i_v4 << " ):"
        << i_line << " = " << l_result << std::endl;
        std::cerr << l_strm.str();
    }
    
    return l_result;
}


template <
    typename w_TR,
    typename w_T1,
    typename w_T2,
    typename w_T3,
    typename w_Tp1,
    typename w_Tp2,
    typename w_Tp3,
    int w_N
>
inline w_TR Trace(
    w_TR ( * i_func )( w_T1, w_T2, w_T3 ),
    const char ( & i_str )[w_N],
    int                     i_line,
    w_Tp1                   i_v1,
    w_Tp2                   i_v2,
    w_Tp3                   i_v3
)
{
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "- " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << ", "
            << i_v2 << ", "
            << i_v3 << " ):"
        << i_line << std::endl;
        std::cerr << l_strm.str();
    }
    
    w_TR l_result = ( * i_func )( i_v1, i_v2, i_v3 );
    
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "+ " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << ", "
            << i_v2 << ", "
            << i_v3 << " ):"
        << i_line << " = " << l_result << std::endl;
        std::cerr << l_strm.str();
    }
    
    return l_result;
}

template <
    typename w_TR,
    typename w_T1,
    typename w_T2,
    typename w_Tp1,
    typename w_Tp2,
    int w_N
>
inline w_TR Trace(
    w_TR ( * i_func )( w_T1, w_T2 ),
    const char ( & i_str )[w_N],
    int                     i_line,
    w_Tp1                   i_v1,
    w_Tp2                   i_v2
)
{
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "- " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << ", "
            << i_v2 << " ):"
        << i_line << std::endl;
        std::cerr << l_strm.str();
    }
    
    w_TR l_result = ( * i_func )( i_v1, i_v2 );
    
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "+ " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << ", "
            << i_v2 << " ):"
        << i_line << " = " << l_result << std::endl;
        std::cerr << l_strm.str();
    }
    
    return l_result;
}

template <
    typename w_TR,
    typename w_T1,
    typename w_Tp1,
    int w_N
>
inline w_TR Trace(
    w_TR ( * i_func )( w_T1 ),
    const char ( & i_str )[w_N],
    int                     i_line,
    w_Tp1                   i_v1
)
{
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "- " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << " ):"
        << i_line << std::endl;
        std::cerr << l_strm.str();
    }
    
    w_TR l_result = ( * i_func )( i_v1 );
    
    if ( g_tracing )
    {
        std::ostringstream l_strm;
        l_strm << "+ " << Task::GetSelfId() << " " << i_str << "( "
            << i_v1 << " ):"
        << i_line << " = " << l_result << std::endl;
        std::cerr << l_strm.str();
    }
    
    return l_result;
}

}; // namespace

#endif // x_at_trace_x

