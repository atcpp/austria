//
// The Austria library is copyright (c) Gianni Mariani 2004.
//
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the
// Austria library subject to the terms of the LGPL.
//
// A copy of the license is available in this directory or one may be found
// at this URL: http://www.gnu.org/copyleft/lesser.txt
//
/**
 * at_os.h
 *
 */

#ifndef x_at_os_h_x
#define x_at_os_h_x 1

#include "at_exports.h"

// Austria namespace
namespace at
{

/**
 * AT_DebugBuild is a macro that is 0 if no debug code is required
 * or 1 if debug code is required.  By default is will bet set
 * to 1 if the macro NDEBUG is not set.
 *
 */

#ifndef AT_DebugBuild
#ifdef NDEBUG
#define AT_DebugBuild 0
#else
#define AT_DebugBuild 1
#endif
#endif

// ======== OSTraitsBase ============================================
/**
 * The OSTraitsBase class is a base class defining some basic
 * OS-specific traits.
 */

class AUSTRIA_EXPORT OSTraitsBase
{
    friend class UnitTestAssertModifier;

    // An address that is guaranteed to cause an access violation.
    static char *m_accessViolationAddr;

    static bool m_running_test_case;

public:
    // ======== GetAccVioAddr =========================================
    /**
     * GetAccVioAddr returns an address which, if dereferenced, will give
     * an access violation.
     *
     * @return m_accessViolationAddr
     */

    static inline char * GetAccVioAddr()
    {
        return m_accessViolationAddr;
    }

    // ======== GetRunningTestCase ====================================
    /**
     * GetRunningTestCase returns true iff we are running a test case
     *
     * @return m_running_test_case
     */
    static inline bool GetRunningTestCase()
    {
        return m_running_test_case;
    }

    // ======== AT_OSAssert ===========================================
    /**
     * The AT_OSAssert method that the current process if an assertion
     * fails.
     */
    static inline bool AT_OSAssert(
        bool i_assertion, const char * i_file, int i_line )
    {
        if ( ! i_assertion )
        {
            if ( m_running_test_case )
            {
                TestCaseAssert( i_file, i_line );
            } else {
              * m_accessViolationAddr = 0;
            }
        }
        return true;
    }

    // ======== TestCaseAssert ========================================
    /**
     * TestCaseAssert will throw a test case assertion.
     *
     *
     * @param i_file The file name that is asserting
     * @param i_line The line number where the assertion occurred
     */
    static void TestCaseAssert( const char * i_file, int i_line );

    // ======== TestCaseAbort =========================================
    /**
     * TestCaseAbort will throw a test case assertion.
     *
     * @param i_file The file name that is asserting
     * @param i_line The line number where the assertion occurred
     */
    static void TestCaseAbort( const char * i_file, int i_line );

    enum {
        /**
         * DebugBuild is a constant that will be used to
         * check for debug builds.  This will enable extra
         * consistantcy checks.
         */
        DebugBuild = AT_DebugBuild
    };

    /**
     * m_default_temp_dir is a directory to use if no "TMP" or
     * "TEMP" environment variable exists.
     */
    static const char *m_default_temp_dir;

    // ======== IsBigEndian ===========================================
    /**
     * IsBigEndian is a function that can be optimized into a constant
     * on smarter compilers and hence eliminate dead code in the optimizer.
     *
     * @return True if the CPU is big endian
     */
    static inline bool IsBigEndian()
    {
        union { int m_ival; char m_cval; } l_uval;
        l_uval.m_ival = 1;
        return 0 == l_uval.m_cval;
    }

    // ======== SchedulerYield =======================================
    /**
     * SchedulerYield will yeild the CPU to another running process
     * or thread.  This can be used in spinlocks.
     *
     * @return Nothing
     */
    static void SchedulerYield();


    // ======== s_smp_memory_latency =================================
    /**
     * SMPMemoryLatency is used to tune spin loops.  A higher 
     * SMPMemoryLatency, the higher the spin count.  On single CPU/core
     * machines, the SMP latentcy should be 0 (meaning yield immediatly).
     *
     */

    static const unsigned s_smp_memory_latency;

    // ======== s_cpu_count ==========================================
    /**
     * The number of CPU (or cores) available.
     */

    static const unsigned s_cpu_count;

    
    
#if defined(WIN32) || defined(_WIN32)
        #define os_assert(A)    ::ExitProcess(-2)
        #define os_abort(A)     ::ExitProcess(-2)
#else
        #define os_assert(A)    assert(A)
        #define os_abort(A)     abort(A)
#endif


#if defined(WIN32) || defined(_WIN32)

#ifdef I_DONT_WANT_ANNOYING_WARNINGS
// Disable annoying warnings
// conversion from '__w64 int' to 'at::SizeMem', possible loss of data
#pragma warning (disable:4244)
// no override available for virtual member function
#pragma warning (disable:4266)
// conversion from 'size_t' to 'int', possible loss of data
#pragma warning (disable:4267)
// conversion from 'int' to 'UINT', signed/unsigned mismatch
#pragma warning (disable:4245)
// local variable is initialized but not referenced
#pragma warning (disable:4189)
// unreferenced inline function has been removed
#pragma warning (disable:4514)
// conversion from 'int' to 'unsigned short', possible loss of data
#pragma warning (disable:4242)
// assignment within conditional expression
#pragma warning (disable:4706)
// qualifier applied to reference type; ignored
#pragma warning (disable:4181)
// enumerate 'xxx' in switch of enum 'xxxx' is not handled
#pragma warning (disable:4062)
// '!=' : signed/unsigned mismatch
#pragma warning (disable:4389)
// '<' : signed/unsigned mismatch
#pragma warning (disable:4018)
// '>=' : expression is always true
#pragma warning (disable:4296)
// no function prototype given: converting '()' to '(void)'
#pragma warning (disable:4255)
// 'fnmatch' : uses old-style declarator
#pragma warning (disable:4131)
// truncation from 'at::AtomicExchangeablePointer' to 'LONG'
#pragma warning (disable:4302)
#endif

#define AT_OS_CPP_I             "at_win32_os.cpp.i"
#define AT_ATOMIC_H             "at_win32_atomic.h"
#define AT_THREAD_H             "at_win32_thread.h"
#define AT_THREAD_CCP_I         "at_win32_thread_cpp.i"
#define AT_STACK_TRACE_CPP_I    "at_win32_stack_trace_cpp.i"
#define AT_FILE_H               "at_win32_file.h"
#define AT_FILE_CPP_I           "at_win32_file.cpp"
#define AT_DIR_H                "at_win32_dir.h"
#define AT_AIO_I                "at_win32_aio.cpp"
#define AT_DIR_CPP_I            "at_win32_dir.cpp"
#define AT_ID_GEN_CPP_I         "at_win32_id_generator.cpp"

// Gx86 uses DIR there is no such thing on Win32
#define DIR void
#undef DIRINCLUDE

    static const char s_directory_separator = '\\';
    static const char s_has_drive_letters = true;

#define AT_LOG_TIME_H           "at_win32_log_time.h"
#define AT_LOG_CTIME_H          "at_win32_log_ctime.h"

#define AT_DEFINE_VIRTINHR_CONSTRUCTOR { AT_Abort(); }

#elif defined(__APPLE__)

#define AT_OS_CPP_I             "at_darwin_os.cpp.i"
#define AT_ATOMIC_H             "at_darwin_atomic.h"
#define AT_THREAD_H             "at_darwin_thread.h"
#define AT_THREAD_CCP_I         "at_darwin_thread_cpp.i"
#define AT_STACK_TRACE_CPP_I    "at_darwin_stack_trace_cpp.i"
#define AT_FILE_H               "at_darwin_file.h"
#define AT_FILE_CPP_I           "at_darwin_file.cpp"
#define AT_DIR_H                "at_darwin_dir.h"
#define AT_DIR_CPP_I            "at_darwin_dir.cpp.i"
#define AT_AIO_I                "at_darwin_aio.cpp.i"
#define AT_ID_GEN_CPP_I         "at_gx86_id_generator.cpp"

    static const char s_directory_separator = '/';
    static const char s_has_drive_letters = false;

#define AT_LOG_TIME_H           "at_darwin_log_time.h"
#define AT_LOG_CTIME_H          "at_darwin_log_ctime.h"

#define AT_DEFINE_VIRTINHR_CONSTRUCTOR ;

#else

#define AT_OS_CPP_I             "at_gx86_os.cpp.i"
#define AT_ATOMIC_H             "at_gx86_atomic.h"
#define AT_THREAD_H             "at_gx86_thread.h"
#define AT_THREAD_CCP_I         "at_gx86_thread_cpp.i"
#define AT_STACK_TRACE_CPP_I    "at_gx86_stack_trace_cpp.i"
#define AT_FILE_H               "at_gx86_file.h"
#define AT_FILE_CPP_I           "at_gx86_file.cpp"
#define AT_DIR_H                "at_gx86_dir.h"
#define AT_DIR_CPP_I            "at_gx86_dir.cpp"
#define AT_AIO_I                "at_gx86_aio.cpp"
#define AT_ID_GEN_CPP_I         "at_gx86_id_generator.cpp"

    static const char s_directory_separator = '/';
    static const char s_has_drive_letters = false;

#define AT_LOG_TIME_H           "at_gx86_log_time.h"
#define AT_LOG_CTIME_H          "at_gx86_log_ctime.h"

#define AT_DEFINE_VIRTINHR_CONSTRUCTOR ;

#endif

};

} // namespace at

#endif // #ifndef x_at_os_h_x

