//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_twin_list.h
 *
 */

#include "at_twin.h"
#include "at_list.h"

#ifndef x_at_twin_list_h_x
#define x_at_twin_list_h_x 1

// Austria namespace
namespace at
{



// ======== LeadTwinList ==============================================
/**
 * LeadTwinList implements a list of LeadTwin objects that associate
 * with various AideTwin objects.  This is useful when an requiring to
 * associate with a list of other like services.
 *
 */

template<
    typename    w_aide_twin_interface,
    typename    w_lead_interface
>
class LeadTwinList
{
    public:


    // ======== LeadListTraits ========================================
    /**
     * LeadListTraits creates a traits class for at::list2.
     *
     */

    class LeadListTraits
      : public Default_List_Traits
    {


        // ======== Payload ===========================================
        /**
         * Payload is the class that is used to put the payload onto.
         *
         */

        class Payload
          : public base
        {
            public:

            ...                 xxx_...;        //

        };
        
        public:
        template <
            typename        w_inheritor
        >   
        class Payload
          : public w_inheritor,
            public LeadTwin_Basic< w_aide_twin_interface, w_lead_interface >
        {
    
            public:
        
        };
        
    };
    

    // ======== LeadTwinEntry =========================================
    /**
     * 
     *
     */

    class LeadTwinEntry
      : public base
    {
        public:

        ...                 xxx_...;        //

    };    
    

    typedef List2<

};


}; // namespace


#endif // x_at_twin_list_h_x


