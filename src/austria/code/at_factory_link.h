//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_factory_link.h
 *
 */

#include "at_factory.h"

#ifndef AT_HardLinkSymbolName
#define AT_HardLinkSymbolName FactoryHardLinkSymbol
#endif

HardLink(AT_HardLinkExtern)

void ** AT_HardLinkSymbolName[] = {
    HardLink(AT_HardLinkArrayElement)
    0
};


