//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_start_up.h
 *
 */

#ifndef x_at_start_up_h_x
#define x_at_start_up_h_x 1

#include "at_lifetime.h"

// Austria namespace
namespace at
{

/**
 * @defgroup InitializerGroup Initializer Factories

<h2>Introduction</h2>

<p>The Initializer is a simple interface for capturing control at the point
when main is called.  This is needed by various libraries where a call from
main() is the only viable solution.

<h3>Initializer Example</h3>

<p>The code below shows a simple initializer example. In this case, the
constructor for the Initializer_Example is called just after main, (among
other Initializer factories).

<CODE>
<pre>

class Initializer_Example
  : public Initializer
{
public:

    Initializer_Example( int & argc, const char ** & argv )
    {
        // do start-up things here
    }

    ~Initializer_Example()
    {
        // do exit things here
    }

};

// Register the factory for Initializer_Example.
AT_MakeFactory2P( "Initializer_Example", Initializer_Example, Initializer, DKy, int & , const char ** & );


</pre>
</CODE>

 *
 * 
 *  @{
 */


// ======== Initializer ===============================================
/**
 * The Initializer is an interface of classes that are created
 * just after the main() function has started and deleted just before
 * main exits.
 *
 * The factory used for this takes 2 parameters.
 * Initializer( int & argc, const char ** & argv ) - these allow
 * the initialize to read or modify the argc and argv variables in main if
 * desired.
 */

class Initializer
{
    public:

    /**
     * Initializer is virtual.
     */

    virtual ~Initializer(){}

};


class MainInitializer_Basic;

// ======== MainInitializer ===========================================
/**
 * MainInitializer is an implemented class that should be placed
 * as the first class created in main.  This will cause all
 * Initializer factories taking a ( int & argc, const char ** & argv )
 * as constructor to be created and destroyed in exit of main.
 *
 * This can be used as a hook for placing special initializers
 * for various libraries (like NSPR and winsock).  Unfortunately
 * for these libraries, they are unable to use the constructor
 * of statically allocated objects as hooks because of various
 * assumptions these libraries make.
 *
 * The most significant limitation is that these libraries must have
 * their "Initializer" factory registered before main is called.
 *
 */

class AUSTRIA_EXPORT MainInitializer
{
    public:


    // ======== MainInitializer ========================================
    /**
     *  This is the MainInitializer constructor to be called in main.
     *
     *
     * @param argc  Reference to main's argc
     * @param argv  Reference to main's argv
     */

    MainInitializer( int & argc, const char ** & argv );


    // ======== ~MainInitializer =======================================
    /**
     *  MainInitializer destructor.  Any MainInitializer destructor
     *  will initiate the destruction of all the Initializer objects.
     */

    ~MainInitializer();    

    private:
    /**
     * MainInitializer_Basic is not allowed to be copy constructed
     * or assigned.
     */
    MainInitializer( const MainInitializer & );
    MainInitializer & operator=( const MainInitializer & );

    /**
     * m_mi is a private pointer to the MainInitializer
     * class.
     */
    static MainInitializer_Basic                 * m_mi;


    // ======== MainInitializerCleanUp =================================
    /**
     *  MainInitializerCleanUp is a private function for internal use.
     *
     * @return nothing
     */

    static void MainInitializerCleanUp(void);
    
};

/** @} */ // end of InitializerGroup ( Doxygen group )


}; // namespace

#endif // x_at_start_up_h_x


