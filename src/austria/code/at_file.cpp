/*
 *
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
//
// at_file.cpp
//
//

#include "at_os.h"
#include AT_FILE_CPP_I
#include "at_dir.h"
#include "at_exception.h"
#include <fnmatch.h>
#include <cctype>

namespace at
{
    using namespace std;

    char FilePath::s_directory_separator = at::OSTraitsBase::s_directory_separator;
    char FilePath::s_extension_separator = '.';
    bool FilePath::s_has_drive_letters = at::OSTraitsBase::s_has_drive_letters;

    FilePath FilePath::Head() const
    {
        return FilePath( string( m_name, 0, m_name.find_last_of( s_directory_separator ) ) );
    }

    FilePath FilePath::Tail() const
    {
        const size_t l_pos = m_name.find_last_of( s_directory_separator );
        return FilePath( l_pos == string::npos ? m_name : string( m_name, l_pos + 1 ) );
    }

    FilePath FilePath::Root() const
    {
        const string::const_iterator l_begin = m_name.begin();
        string::const_iterator l_iter = m_name.end();

        while ( l_begin != l_iter )
        {
            const char c = *(--l_iter);
            if ( c == s_directory_separator ) break;
            if ( c == s_extension_separator ) return FilePath( string( l_begin, l_iter ) );
        }
        return FilePath( m_name );
    }
    
    string FilePath::Extension() const
    {
        const string::const_iterator l_begin = m_name.begin();
        string::const_iterator l_iter = m_name.end();

        while ( l_begin != l_iter )
        {
            const char c = *(--l_iter);
            if ( c == s_directory_separator ) break;
            if ( c == s_extension_separator ) return string( l_iter + 1, m_name.end() );
        }
        return string();
    }

    bool FilePath::Match( const string i_pattern ) const
    {
        if ( s_directory_separator == '\\' )
            return fnmatch( i_pattern.c_str(), m_name.c_str(), FNM_NOESCAPE ) == 0;
        return fnmatch( i_pattern.c_str(), m_name.c_str(), 0 ) == 0;
    }

    FilePath & FilePath::Clean()
    {
        if ( m_name.length() == 0 )
        {
            m_name = '.';
        }
        else
        {
            // remove all empty path components, e.g. /foo///bar/gee// -> /foo/bar/gee
            const string l_slashslash( 2, s_directory_separator );
            for ( size_t l_pos = 0; ( l_pos = m_name.find( l_slashslash, l_pos ) ) != string::npos; )
            {
                m_name.erase( l_pos, 1 );
            }
            if ( m_name.length() > 1 && * ( m_name.end() - 1 ) == s_directory_separator )
            {
                m_name.erase( m_name.end() - 1 );
            }
            // remove all unnecessary '.' components, e.g. ./foo/./bar/gee/. -> foo/bar/gee
            const string l_slashdot = string( 1, s_directory_separator ) + '.';
            for ( size_t l_pos = 0; ( l_pos = m_name.find( l_slashdot, l_pos ) ) != string::npos; )
            {
                if ( l_pos + 2 == m_name.length() || m_name[ l_pos + 2 ] == s_directory_separator )
                {
                    m_name.erase( l_pos, 2 );
                }
                else
                {
                    l_pos += 3;
                }
            }
            const string l_dotslash = string( 1, '.' ) + s_directory_separator;
            if ( m_name.length() > 1 && m_name.compare( 0, 2, l_dotslash ) == 0 )
            {
                if ( m_name.length() == 2 )
                {
                    m_name = '.';
                }
                else
                {
                    m_name.erase( 0, 2 );
                }
            }
            // remove all '..' components that can be removed
            const string l_slashdotdot = l_slashdot + '.';
            for ( size_t l_pos = 0; ( l_pos = m_name.find( l_slashdotdot, l_pos ) ) != string::npos; )
            {
                if ( l_pos + 3 == m_name.length() || m_name[ l_pos + 3 ] == s_directory_separator )
                {
                    if ( l_pos == 0 )
                    {
                        if ( m_name.length() == 3 )
                        {
                            m_name.erase( l_pos + 1, 2 );
                        }
                        else
                        {
                            m_name.erase( l_pos , 3 );
                        }
                    }
                    else
                    {
                        size_t l_len = 3;

                        while ( l_pos > 0 && m_name[ l_pos - 1 ] != s_directory_separator )
                        {
                            l_pos--;
                            l_len++;
                        }
                        if ( l_len == 5 && m_name.compare( l_pos, 2, ".." ) == 0 )
                        {
                            l_pos += l_len;
                        }
                        else if ( l_pos == 0 )
                        {
                            if ( l_len == m_name.length() )
                            {
                                m_name = '.';
                            }
                            else
                            {
                                m_name.erase( l_pos , l_len + 1);
                            }
                        }
                        else
                        {
                            m_name.erase( l_pos , l_len );
                            if ( --l_pos != 0 )
                            {
                                m_name.erase( l_pos , 1 );
                            }
                        }
                    }
                }
                else
                {
                    l_pos += 4;
                }
            }
        }

        return * this;
    }

    FilePath FilePath::RelativePath( const FilePath &  i_path_from,
                                     const FilePath &  i_path_stem ) const
    {
    if ( m_name.length() == 0 )
    {
            return FilePath( "." );
    }
    if (
        ( m_name[0] != s_directory_separator )
        && ! ( s_has_drive_letters && m_name.length() >1 && m_name[1] == ':' && std::isalpha( m_name[0] ) )
    )
    {
            return FilePath( m_name );
    }
    if ( i_path_from.m_name.length() == 0 )
    {
            return FilePath( m_name );
    }
    
    string::const_iterator l_fitr = i_path_from.m_name.begin();
    string::const_iterator l_titr = m_name.begin();
    string::const_iterator l_sitr = i_path_stem.m_name.begin();

    string::const_iterator l_last_slash_fitr = i_path_from.m_name.begin();
    string::const_iterator l_last_slash_titr = m_name.begin();

    string::const_iterator l_efitr = i_path_from.m_name.end();
    string::const_iterator l_etitr = m_name.end();
    string::const_iterator l_esitr = i_path_stem.m_name.end();

    // process the stem - all three strings need to be identical
    // up to the end of the stem.
    
    if ( l_sitr != l_esitr )
    {
            while ( ( l_titr != l_etitr ) && ( l_fitr != l_efitr ) && ( l_sitr != l_esitr ) )
            {
                if ( * l_titr != * l_fitr )
                {
                    return FilePath( m_name );
                }
            
                if ( * l_titr != * l_sitr )
                {
                    return FilePath( m_name );
                }
    
                ++ l_titr;
                ++ l_fitr;
                ++ l_sitr;
            }

            -- l_sitr;

            if ( * l_sitr != s_directory_separator )
            {
                if ( ( l_titr != l_etitr ) && ( * l_titr != s_directory_separator ) )
                {
                    return FilePath( m_name );
                }

                ++ l_titr;
                
                if ( ( l_fitr != l_efitr ) && ( * l_fitr != s_directory_separator ) )
                {
                    return FilePath( m_name );
                }

                ++ l_fitr;
            }

            l_last_slash_fitr = l_fitr;
            l_last_slash_titr = l_titr;

    }
    
    // copy the string over itself - removing junk
    while ( ( l_titr != l_etitr ) && ( l_fitr != l_efitr ) )
    {

            if ( * l_titr != * l_fitr )
            {
                break;
            }

            ++ l_fitr;
            if ( * ( l_titr ++ ) == s_directory_separator )
            {
                l_last_slash_fitr = l_fitr;
                l_last_slash_titr = l_titr;
            }

    }

    if ( l_titr == l_etitr )
    {
            if ( l_fitr == l_efitr )
            {
                return FilePath( "." );
            }
            else if ( s_directory_separator == * l_fitr )
            {
                ++ l_fitr;
                l_last_slash_fitr = l_fitr;
                l_last_slash_titr = l_titr;
            }
    }
    else
    {
            if ( s_directory_separator == * l_titr )
            {
                ++ l_titr;
                l_last_slash_fitr = l_fitr;
                l_last_slash_titr = l_titr;
            }
    }

    string l_rel_path;

    l_titr = l_last_slash_titr;
    l_fitr = l_last_slash_fitr;

    if ( l_fitr != l_efitr )
    {
            l_rel_path += "..";

            ++ l_fitr;
        
            while ( l_fitr != l_efitr )
            {
                if ( * ( l_fitr ++ ) == s_directory_separator )
                {
                    if ( l_fitr != l_efitr )
                    {
                        ( l_rel_path += s_directory_separator ) += "..";
                    }
                }
            }
    }

    if ( l_last_slash_titr != l_etitr )
    {
            if ( l_rel_path.length() != 0 )
            {
                l_rel_path += s_directory_separator;
            }
            l_rel_path.append( l_last_slash_titr, l_etitr );
    }

    if ( l_rel_path.length() == 0 )
    {
            return FilePath( "." );
    }
    
    return FilePath( l_rel_path );
    
    }

// ======== CreatePath ================================================
/**
 * CreatePath will create all non-existant directories right up to
 * but not including the filename.  It does this by checking existance
 * of the path name.
 *
 * Ex 1)
 *      e.g. i_path_from = "/a/b/c/f.txt"
 *           i_path_stem = "/a/b"
 *
 * if "/a/b" does not exist, CreatePath will fail.
 *
 * Ex 2)
 *      
 *      e.g. i_path_from = "/x/y/z/f.txt"
 *           i_path_stem = "/a/b"
 *
 * i_path_from is not an extension of i_path_stem and so CreatePath will fail
 *
 * @param i_path_from Is the file name whose path is needed
 * @param i_path_stem Is the last path that will be considered.
 * @param i_path_is_dir indicates that i_path_from is supposed to be a
 *              directory so no removal of the file name is needed.
 * @return True if successful, false otherwise.
 */

AUSTRIA_EXPORT bool CreatePath(
    const FilePath &  i_path_from,
    const FilePath &  i_path_stem,
    bool              i_path_is_dir
) {

    FilePath    l_path_from( i_path_from );
    l_path_from.Clean();
    
    FilePath    l_path_stem( i_path_stem );
    l_path_stem.Clean();

    if ( ! i_path_is_dir )
    {
        l_path_from = l_path_from.Head();
    }

    const std::string & l_str_from = l_path_from.StlString();
    const std::string & l_str_stem = l_path_stem.StlString();

    if ( l_str_from == l_str_stem )
    {
        return Directory::Exists( l_path_stem );
    }

    if ( l_str_from.length() < l_str_stem.length() )
    {
        return false;
    }

    char c = l_str_from[ l_str_stem.length() ];
    if ( c != '/' && c != FilePath::s_directory_separator )
    {
        return false;
    }

    if ( Directory::Exists( l_path_from ) )
    {
        return true;
    }
    
    // we now know that the paths have the same stem
    // and the stem is a directory - from the full
    // length path, back off until the directory
    // exists.

    FilePath    l_path_work( l_path_from );

    std::vector< FilePath >     l_paths;

    do {
        
        l_paths.push_back( l_path_work );

        l_path_work = l_path_work.Head();

        if ( Directory::Exists( l_path_work ) )
        {
            break;
        }

    } while ( l_path_work != l_path_stem );

    // traverse from the shortest directory to the full path
    // and create every directory.
    for (
		std::vector< FilePath >::reverse_iterator i = l_paths.rbegin();
        i != l_paths.rend();
		++ i
    )
    {
        if ( ! Directory::Create( * i ) )
        {
            AT_ThrowDerivation(DirectoryCreateFailed, ( std::string("Directory::Create failed for " ) + i->StlString() ).c_str() );
        }
    }

    return true;
}

}; // namespace
