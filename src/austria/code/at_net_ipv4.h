//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the
// Austria library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found
// at this URL: http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_net.h - Austria network IPv4 interfaces
 */

#ifndef x_at_net_ipv4_h_x
#define x_at_net_ipv4_h_x 1

#include "at_net.h"

namespace at
{

/**
 * IPv4Address stores an IP address and port.  By convention, when stored
 * in an IPv4Address, these data are kept in host byte order, but note that
 * this is not (cannot be) enforced.
 */
class AUSTRIA_EXPORT IPv4Address : public NetAddress
{
public:
    virtual ~IPv4Address() {}

    typedef unsigned short t_Port;
    typedef unsigned long t_IP;

    /**
     * IPv4Address constructor.
     * Example usage: <code>IPv4Address(127,0,0,1,8080)</code>.
     *
     * @param i_octet_3 The first octet in the dotted quad form of the address
     * @param i_octet_2 The second octet ...
     * @param i_octet_1 The third octet ...
     * @param i_octet_0 The fourth octet ...
     * @param i_port The port, in host byte order
     */ 
    inline IPv4Address(
            unsigned char i_octet_3,
            unsigned char i_octet_2,
            unsigned char i_octet_1,
            unsigned char i_octet_0,
            t_Port i_port) : 
        m_ip((i_octet_3 << 24)
             | (i_octet_2 << 16)
             | (i_octet_1 << 8)
             | i_octet_0),
        m_port(i_port)
    {
    }

    /**
     * IPv4Address copy constructor.
     * @param i_addr Whence to copy
     */
    IPv4Address(const IPv4Address &i_addr) :
        NetAddress(),
        m_ip(i_addr.m_ip),
        m_port(i_addr.m_port)
    {
    }

    /**
     * IPv4Address constructor.
     * @param i_ip The IP in host byte order
     * @param i_port The port in host byte order
     */
    IPv4Address(t_IP i_ip, t_Port i_port) :
        m_ip(i_ip),
        m_port(i_port)
    {
    }

    /**
     * Return the IP, in host byte order
     * @return the IP, in host byte order
     */
    t_IP ip() const { return m_ip; }

    /**
     * Return the port, in host byte order
     * @return the port, in host byte order
     */
    t_Port port() const { return m_port; }

public: // NetAddress
    virtual bool operator==(const NetAddress&) const;

protected:
    const t_IP m_ip;
    const t_Port m_port;
};


/**
 * This enables you to pass <code>IPv4Address</code>es to output streams.
 * A human-readable form will be produced.  For example, "127.0.0.1:80".
 */
template<typename w_char_type, typename w_traits>                   
std::basic_ostream<w_char_type, w_traits>&
operator << (
    std::basic_ostream<w_char_type, w_traits>   & o_ostream,
    const IPv4Address                           & i_value
) {
    IPv4Address::t_IP ip = i_value.ip();
    o_ostream <<
        ( 0xff & ( ip >> 24 ) ) << '.' <<
        ( 0xff & ( ip >> 16 ) ) << '.' <<
        ( 0xff & ( ip >>  8 ) ) << '.' <<
        ( 0xff & ( ip >>  0 ) ) << ':' <<
        i_value.port();
    return o_ostream;
}

class IPv4DatagramListenParameters : public NetParameters
{
public:
    virtual ~IPv4DatagramListenParameters() {}
};

} // namespace at

#endif // #ifndef x_at_net_ipv4_h_x


