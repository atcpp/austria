//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * \file at_pointers.h
 *
 * \author Gianni Mariani
 *
 * at_pointers.h contains a set of pointer management templates used to hide the
 * details of iterators and pointers.  This may be used to create template code
 * that is interchangeable between iterators and POD pointers.
 *
 */

#ifndef x_at_pointers_h_x
#define x_at_pointers_h_x 1


#include "at_os.h"
#include "at_assert.h"

// Austria namespace
namespace at
{

// ======== PointerRange ===========================================
/**
 * PointerRange is an enum that indicates the types of value
 * a pointer may hold.
 */

enum PointerRange
{
    /**
     * Null relates to a pointer that is not pointing to
     * any object - i.e. the null pointer.
     */

    Null,
    
    /**
     * Invalid indicates that the pointer is uninitialized.  Using
     * a pointer in this range is considered an error.
     */

    Invalid,
    
    /**
     * The pointer is valid and pointing to an object.
     */

    Valid
    
};




// ======== IteratorWrapper ========================================
/**
 * Is a set of methods and classes that wrap an iterator.
 *
 */

template <typename w_T, typename w_value_type, bool w_debug_code>
class IteratorWrapper
{
    public:

    enum {
        /**
         * e_debug_code is true if this is a debug PointerWrapper
         * and this is a debug build.
         */
        e_debug_code = ( w_debug_code && OSTraitsBase::DebugBuild ) ? 1 : 0
    };


    /**
     * value_type is the value of the return
     * from w_T::operator*()
     */

    typedef w_value_type        value_type;

    /**
     * construct a default pointer_value. Null
     * is OK.
     */

    IteratorWrapper()
        : m_range( Null )
    {
    }

    /**
     * construct a pointer_value given a w_T value.
     * It is assumed that the pointer value is a
     * valid pointer.  (This is where this concept
     * breaks down a little.  There is no way of knowing
     * what kind of pointer is being passed.)
     *
     * @param i_value is the value of the pointer.
     */
    
    IteratorWrapper( const w_T & i_value )
      : m_value( i_value ),
        m_range( Valid )
    {
    }
    

    // ======== Assign ================================================
    /**
     *  The Assign method is the function used to assign pointers.
     *
     *
     * @param i_in The the value being assigned
     * @return the previous value of the pointer
     */

    inline IteratorWrapper Assign( w_T & i_in )
    {
        IteratorWrapper retval( * this );

        m_range = Valid;

        m_value = i_in;

        return retval;

    } // end Assign

    // ======== AssignNull ============================================
    /**
     *  Assign the pointer value to null
     *
     */

    inline void AssignNull()
    {
        m_range = Null;

    } // end AssignNull

    // ======== AssignInvalid =========================================
    /**
     *  AssignInvalid will set the pointer to an invalid entry.  Meaning
     *  attempts to use the pointer will assert.
     *
     */

    inline void AssignInvalid()
    {
        m_range = Invalid;

    } // end AssignInvalid


    // ======== IsNull ================================================
    /**
     *  Returns true if the value is null
     *
     * @return true if null, false otherwise
     */

    inline bool IsNull() const
    {

        if ( e_debug_code )
        {
            AT_Assert( m_range != Invalid );            
        }

        return m_range == Null;

    } // end IsNull

    
    // ======== IsValid ===============================================
    /**
     *  IsValid is used to determine if the pointer is valid.
     *
     * @return true if pointer is "valid"
     */

    inline bool IsValid() const
    {
        return m_range == IsValid;

    } // end IsValid


    // ======== Get ===================================================
    /**
     *  Get will retrieve from a pointer_value the value contained.
     *
     *
     * @return a reference to the pointer value 
     */

    inline const w_T & Get() const
    {

        return m_value;

    } // end GetPtr

    
    protected:
    
    w_T                         m_value;
    PointerRange                m_range;
        
};



// ======== PointerWrapper =========================================
/**
 * PointerWrapper provides methods and a type for managing
 * POD pointers.  It uses the same interface as the IteratorWrapper
 * and hence the PointerWrapper and IteratorWrapper classes be
 * used interchangeably as ways to manage pointers.
 *
 * @param w_TD is the type being pointed to - synonymous with the
 *              value_type.
 *
 * @param w_debug_code is set to true if "invalid" pointer checks
 *                     are being made.
 */

template <typename w_TD, bool w_debug_code>
class PointerWrapper
{

    enum {
        /**
         * e_debug_code is true if this is a debug PointerWrapper
         * and this is a debug build.
         */
        e_debug_code = ( w_debug_code && OSTraitsBase::DebugBuild ) ? 1 : 0
    };

    /**
     * m_value is the value of the pointer being managed here.
     *
     */

    w_TD                * m_value;
    
    public:

    /**
     * value_type is the value of the return
     * from w_T::operator*()
     */

    typedef w_TD        value_type;

    /**
     * construct a pointer_value given a w_T value.
     * It is assumed that the pointer value is a
     * valid pointer.  (This is where this concept
     * breaks down a little.  There is no way of knowing
     * what kind of pointer is being passed.)
     *
     * @param i_value is the value of the pointer.
     */
    
    PointerWrapper( w_TD * i_value = 0 )
      : m_value( i_value )
    {
    }
    

    // ======== Assign ================================================
    /**
     *  The Assign method is the function used to assign pointers.
     *
     *
     * @param i_in The the value being assigned
     */

    inline void Assign( w_TD * i_in )
    {
        m_value = i_in;

    } // end Assign

    // ======== AssignNull ============================================
    /**
     *  Assign the pointer value to null
     *
     */

    inline void AssignNull()
    {
        m_value = 0;

    } // end AssignNull

    // ======== AssignInvalid =========================================
    /**
     *  AssignInvalid will set the pointer to an invalid entry.  Meaning
     *  attempts to use the pointer will assert after AssignInvalid.
     * 
     */

    inline void AssignInvalid()
    {
        m_value = e_debug_code ? ( reinterpret_cast<w_TD *>( -1 ) ) : 0;

    } // end AssignInvalid


    // ======== IsNull ================================================
    /**
     *  Returns true if the value is null - if debug mode is on,
     *  this check for test of an "invalid" pointer.
     *
     * @return true if null, false otherwise
     */

    inline bool IsNull() const
    {
        if ( e_debug_code )
        {
            AT_Assert( m_value != ( reinterpret_cast<w_TD *>( -1 ) ) );
        }
        
        return m_value == 0;
        
    } // end IsNull

    
    // ======== IsValid ===============================================
    /**
     *  Determines in the pointer is a valid pointer.  In this case,
     *  the check for assert is not done because this is used when 
     *  to determine if the pointer needs releasing.
     *
     * @return true if pointer is "valid"
     */

    inline bool IsValid() const
    {

        if ( e_debug_code )
        {
            if ( m_value == ( reinterpret_cast<w_TD *>( -1 ) ) )
            {
                return false;
            }
        }

        return m_value != 0;

    } // end IsValid


    // ======== Get ===================================================
    /**
     *  Get will retrieve from a pointer_value the value contained.
     *
     *
     * @return a reference to the pointer value 
     */

    inline const w_TD * Get() const
    {
        // don't worry about assertion silliness.
        if ( e_debug_code )
        {
            AT_Assert( m_value != ( reinterpret_cast<w_TD *>( -1 ) ) );
        }
        
        return m_value;

    } // end GetPtr

    // ======== GetInnerAddr ============================================
    /**
     *  Return the address of the inner pointer.  This is used for
     *  legacy API's which require a pointer to the internal address.
     *
     *
     * @return a reference to the pointer value 
     */

    inline w_TD ** GetInnerAddr()
    {
        return & m_value;

    } // end GetPtr

};


// ======== PointerTo ==============================================
/**
 * This template will provide methods and classes to manage a
 * "pointer" like object.
 * 
 */

template <typename w_T>
class PointerTo
{
    public:

    /**
     * value_type is the dereferenced value of
     * the pointer.
     */
    
    typedef typename w_T::value_type value_type;

    /**
     * t_pointer_traits_debug is a typedef to a class that wraps
     * a pointer.  The debug version of the pointer provides
     * extra "invalid" pointer functionality.
     */
    typedef IteratorWrapper< w_T, value_type, true >        t_pointer_traits_debug;
    typedef IteratorWrapper< w_T, value_type, false >   t_pointer_traits_nodebug;
};



// ======== PointerTo ==============================================
/**
 * PointerTo<w_TD *> is a specialization for PointerTo for
 * POD pointers.  This determines the dereferenced type of a
 * plain pointer.
 *
 * This template will return the object being pointed to by T.  If
 * T is not pointed to i
 */

template <typename w_TD>
class PointerTo<w_TD *>
{
    public:

    /**
     * value_type is the dereferenced value of
     * the pointer.
     */
    
    typedef w_TD value_type;

    /**
     * t_pointer_traits_debug is a typedef to a class that wraps
     * a pointer.  The debug version of the pointer provides
     * extra "invalid" pointer functionality.
     */
    typedef PointerWrapper< w_TD, true >        t_pointer_traits_debug;
    
    /**
     * t_pointer_traits_nodebug is a typedef to a class that wraps
     * a pointer.
     */
    typedef PointerWrapper< w_TD, false >   t_pointer_traits_nondebug;
};


/**
 * MPT_False is a type used to detect a false meta template result.
 * The type is never instantiated at run-time, it is only used in
 * meta-programming.
 */
 
typedef char MPT_False;


/**
 * MPT_True is a type used to detect a true meta template result.
 * The type is never instantiated at run-time, it is only used in
 * meta-programming.
 */

struct MPT_True { int m[2]; };

/**
 * MPT_Finder is a temlate used in meta-programming.  It is never
 * instantiated.  It uses the C++ function resolution mechanism
 * and template instantiation to detect if the type D has
 * a member typedef named PtrTargetMarker with the type TD.
 *
 * @param w_D is the type is being checked for PtrTargetMarker
 * @param w_TD is the type of the PtrTargetMarker typedef
 */

template <typename w_D, typename w_TD>
struct MPT_Finder
{
    template <typename w_T>
    static MPT_True finder( const w_T *, typename w_T::PtrTargetMarker * );
        
    static MPT_False finder( const w_D *, w_TD * );

};

/**
 * MPT_ContainsMember is a temlate used in meta-programming. It is
 * never instantiated.  MPT_ContainsMember::value will evaluate to
 * 0 if the the type w_D contains a member type (or typedef) that
 * is the same as w_TD (or specifically a w_TD * is implicity
 * castable from the type of the w_T::PtrTargetMarker *.
 *
 * @param w_D is the type is being checked for containment of
 *          a member typedef named PtrTargetMarker
 * @param w_TD is the type of the member typedef named PtrTargetMarker.
 *          The default value for w_TD is int.
 */

template <typename w_D, typename w_TD = int >
struct MPT_ContainsMember
{
    /**
     * DerivedClass is used to create a level of conversion
     * so that the finder method resolution will favour the
     * finder<>() template method over the finder() method.
     */

    struct DerivedClass : w_D
    {
    };

    /**
     * DerivedClassp is a typedef pointer to DerivedClass
     * for readability.
     */

    typedef DerivedClass  * DerivedClassp;

    /**
     * t_TDp is a pointer to the w_TD template parameter
     */
    typedef w_TD  * t_TDp;

    enum {
       /**
        * value is the "value" of the MPT_ContainsMember template.
        * It will evaluate to 0 if the w_D type contains the
        * PtrTargetMarker type (or typedef) of type w_TD.
        */
        value = (
            sizeof( MPT_Finder<w_D,w_TD>::finder( DerivedClassp(), t_TDp() ) )
            == sizeof( MPT_True )
        )
    };
    
};

template <int w_select, typename w_T0, typename w_T1>
struct MPT_Select_Def
{
};

template <typename w_T0, typename w_T1>
struct MPT_Select_Def<0, w_T0, w_T1>
{
    typedef     w_T0    type;
};

template <typename w_T0, typename w_T1>
struct MPT_Select_Def<1, w_T0, w_T1>
{
    typedef     w_T1    type;
};



// ======== MPT_Select ================================================
/**
 * This uses partial template specialization to select one type or another.
 *
 * @param w_ValueT this is a type that contains a "value" indicating which
 *      type is desired.
 *
 * @param w_T0 is the first type to select
 *
 * @param w_T1 is the second type to select
 *
 */

template <typename w_ValueT, typename w_T0, typename w_T1>
struct MPT_Select
{
    typedef typename MPT_Select_Def<
        w_ValueT::value,
        w_T0,
        w_T1
    >::type                     type;
    
};



}; // namespace

#endif // x_at_pointers_h_x

