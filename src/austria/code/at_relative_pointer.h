//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_relative_pointer.h
 *
 */

#ifndef x_at_relative_pointer_h_x
#define x_at_relative_pointer_h_x 1

#include "at_types.h"

#include <cstddef>

// Austria namespace
namespace at
{
    
// ======== MapHeader ================================================
/**
 * Mapping header is the contents of the mapping location.
 *
 */

struct MapHeader
{
    Uint32                                      m_magic;
    Uint32                                      m_length;
};


// ======== RelativePointer ========================================
/**
 * RelativePointer is a template class that allows the storage
 * of relative pointers in permanent storage.
 *
 */

template <
    typename        w_pointer_type,
    typename        w_storage_type=Uint32,
    typename        w_header_type=MapHeader
>
struct RelativePointer
{
    w_storage_type                              m_value;

    typedef w_header_type                       t_HeaderType;
    typedef w_storage_type                      t_StorageType;

    // ======== t_RelativePointerReference ============================
    /**
     * A relative pointer reference contains all the information
     * to create and store values in a relative pointer.
     */

    struct t_RelativePointerReference
    {

        /**
         * need to know the actual pointer to the map location
         * and a pointer to the location in the map that
         * contains the desired relative pointer.
         */
        
        w_storage_type                       * m_value_ptr;
        const w_header_type                  * m_map_addr;


        // ======== t_RelativePointerReference ========================
        /**
         * Constructor
         *
         */

        t_RelativePointerReference(
            w_storage_type                   * i_value_ptr,
            const w_header_type              * i_map_addr
        )
          : m_value_ptr( i_value_ptr ),
            m_map_addr( i_map_addr )
        {
        };


        private:
        
        // ======== MapFileToMemory ===================================
        /**
         *  MapFileToMemory maps an address from the file address 
         *  space to process memory address space.
         *
         * &return a local pointer
         */

        inline w_pointer_type MapFileToMemory() const
        {

            w_storage_type                  l_file_addr = * m_value_ptr;

            if ( l_file_addr == 0 )
            {
                return 0;
            }
            
            return reinterpret_cast<w_pointer_type>(
                reinterpret_cast<std::ptrdiff_t>( m_map_addr ) + * m_value_ptr
            );

        } // end MapFileToMemory


        // ======== MapMemoryToFile ===================================
        /**
         *  MapMemoryToFile maps an address from process memory address
         *  space to file address space.
         *
         * &return a file address space pointer
         */

        inline w_storage_type MapMemoryToFile( w_pointer_type i_ptr ) const
        {

            if ( i_ptr == 0 )
            {
                return 0;
            }

            return static_cast<w_storage_type>(
                reinterpret_cast<std::ptrdiff_t>( i_ptr )
                - reinterpret_cast<std::ptrdiff_t>( m_map_addr )
            );

        } // end MapMemoryToFile

        public:
        
        // ======== operator w_pointer_type ===========================
        /**
         *  Cast operator - how to create a pointer
         *
         * &return the real pointer in local space
         */

        inline operator w_pointer_type () const
        {
            return MapFileToMemory();

        } // end operator w_pointer_type

        // ======== operator= ( w_pointer_type i_new_value ) ==========
        /**
         * Assign a new relative pointer.
         *
         * &return t_RelativePointerReference
         */

        inline t_RelativePointerReference operator= ( w_pointer_type i_new_value ) const
        {

            w_storage_type l_rel_ptr = MapMemoryToFile( i_new_value );
            
            //
            // could do a check here to make sure that the pointer
            // does in fact map to the address space
            
            * m_value_ptr = l_rel_ptr;

            return * this;

        } // end w_pointer_type operator= ( w_pointer_type i_new_value )


        // ======== w_pointer_type operator-> () const ================
        /**
         * Dereference operator.
         *
         * &return w_pointer_type
         */

        inline w_pointer_type operator-> () const
        {
            return MapFileToMemory();

        } // end w_pointer_type operator-> () const

    };
    

    inline const t_RelativePointerReference operator()( const w_header_type * i_map_addr )
    {
        return t_RelativePointerReference( & m_value, i_map_addr );
    }

    /**
     * A default value constructor should initialize the pointer to
     * null.  This would hardly ever get used but it's safe to point
     * to null.
     */
    
    inline RelativePointer()
      : m_value( 0 )
    {
    }

    private:

    /**
     * Disable the default assignment operator.  These objects must not be copied.
     * Copies can happen if the entire file is copied bit for bit but not
     * copying one to another.
     */
    RelativePointer & operator=( const RelativePointer & i_rel_ptr );

    /**
     * Disable the basic copy constuctor. Same logic applies to the copy
     * constructor.
     */
    RelativePointer( const RelativePointer & i_rel_ptr );
};

}; // namespace
#endif // x_at_relative_pointer_h_x


