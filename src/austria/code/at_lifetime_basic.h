//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_lifetime_basic.h
 *
 */

#ifndef x_at_lifetime_basic_h_x
#define x_at_lifetime_basic_h_x 1

#include "at_exports.h"
#include "at_lifetime.h"

// Austria namespace
namespace at
{

// ======== LifeControlPtrStyle =======================================
/**
 * This changes the PtrStyle traits
 *
 */

class LifeControlPtrStyle
  : public PtrStyle
{
    public:

    // ======== BaseType ==============================================
    /**
     * The base type for LifeControl_Basic should be the PtrTarget
     * abstract base class.
     */
    
    typedef PtrTarget       BaseType;

};

// ======== LifeControl_Basic ======================================
/**
 * LifeControl_Basic is an implementation of PtrTarget.  It will
 * actually delete itself once the reference count is zeroed.
 *
 * Objects that are reference counted rarely need to have a copy constructor
 * or an assignment operator, however, LifeControl_Basic defines these 
 * consistant with a model where objects that have references are independantly
 * referenced, i.e. have no effect from being copied.
 */

typedef PtrTarget_Generic<int,LifeControlPtrStyle>  LifeControl_Basic;


// ======== LifeControl_Lean ======================================
/**
 * LifeControl_Lean is an alternate implementation of PtrTarget.  It will
 * actually delete itself once the reference count is zeroed.
 *
 * This version of the PtrTarget interface can be used inside interface
 * descriptions and the increment and decrement functions are not virtual which
 * can lead to much faster execution times.
 */

typedef PtrTarget_Generic<int>  LifeControl_Lean;


}; // namespace
#endif // x_at_lifetime_basic_h_x

