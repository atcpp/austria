//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_dbghelp.h
 *
 */

#ifndef x_at_dbghelp_h_x
#define x_at_dbghelp_h_x 1

#include "at_thread.h"
#include "at_buffer.h"
#include "at_file.h"

// Austria namespace
namespace at
{

/**
 * @defgroup Debugging Help

<h2>Introduction</h2>

<p>It is often helpful to place temporary snippets of code to perform
an indeterminate temporary delay in execution so that a debugger can
be attached.

<h3>Debugging Help Example</h3>

<p>The code below shows how to insert a delay

<CODE>
<pre>

#include "at_dbghelp.h"

//
// s_debug_delay_trigger should be placed
namespace atdbg
{
const char * s_debug_delay_trigger = "c:\\app_trigger";
};

int main()
{
    // if the file named by s_debug_delay_trigger exists, DebugDelay
    // objects will be triggered to loop indefinitly wile the
    // hold file exists.
    
    // pause here for the debugger - process will
    // create a c:\\hold.<pid> with the process id of the caller
    // while it awaits the removal of the file.  If CreateFlagFile
    // is added, the file is created on every pass and the
    // user is expected to delete the file.
    at::DebugDelay      l_dbg( "c:\\hold", true );

    ... // execution continues after the c:\\hold file is deleted.
    etc


</pre>
</CODE>

 *
 * 
 *  @{
 */

} // namespace

namespace atdbg
{
extern const char * s_debug_delay_trigger;
};

namespace at
{

// ======== DebugDelay ================================================
/**
 * DebugDelay can be used to create points where execution of the
 * program is delayed upon existance of 
 *
 */


class AUSTRIA_EXPORT DebugDelay
{
    public:

    /**
     * DebugDelay
     *
     */
    DebugDelay(
        const char                  * i_hold_file,
        bool                          i_create_hold = false,
        const char                  * i_trigger_file = atdbg::s_debug_delay_trigger
    )
      : m_hold_file( i_hold_file )
    {
        FilePath        l_path( i_hold_file );

        if ( TriggerCheck( i_trigger_file ) )
        {
            if ( i_create_hold )
            {
                WFile   l_creater( l_path, FileAttr::Write | FileAttr::Create  );

                l_creater.Open();
            }

            while ( BaseFile::Exists( l_path ) )
            {
                at::Task::Sleep( at::TimeInterval::MilliSecs( 40 ) );
            }
        }
    }


    // ======== TriggerCheck ==========================================
    /**
     * TriggerCheck checks for the trigger file and returns true if
     * debugging is triggered.
     *
     * @return True if the file named by the global variable
     *          atdbg::s_debug_delay_trigger existed the first time TriggerCheck
     *          is called in a proces.
     */

    static bool TriggerCheck( const char * i_trigger )
    {
        static bool s_triggers = BaseFile::Exists( FilePath( i_trigger ) );

        return s_triggers;
    }


    /**
     * m_hold_file name
     */

    const char                      * m_hold_file;

};


/** @} */ // end of InitializerGroup ( Doxygen group )


}; // namespace

#endif // x_at_dbghelp_h_x


