// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_node.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_node_h_x
#define x_at_xml_node_h_x 1

#include "at_lifetime.h"
#include <vector>

// Austria namespace
namespace at
{
    // Forward Declarations
    class XmlNode;
    class XmlComposite;
    class XmlDocument;
    class XmlElement;
    class XmlText;
    class XmlComment;
    class XmlProcessingInstruction;

    /**
     *  XmlNodeList implements an ordered collection of nodes.
     *  
     *  In order to conform to W3C's DOM Level 1 Specification, items in an XmlNodeList are accessible via
     *  an integral index, starting from 0, hence the implementation of XmlNodeList as a specialization of
     *  the STL vector class template.
     */
    struct XmlNodeList : public std::vector< Ptr< XmlNode* > >
    {
        /**
         *  @brief  Default constructor creates no elements.
         */
        XmlNodeList() : std::vector< Ptr< XmlNode* > >() {}

        /**
         *  @brief  Copy constructor.
         *  @param  i_nodeList  An XmlNodeList.
         */
        XmlNodeList( const XmlNodeList& i_nodeList ) : std::vector< Ptr< XmlNode* > >( i_nodeList ) {}

        /**
         *  @brief  The number of nodes in the list.
         *
         *  The range of valid child node indices is 0 to Length()-1 inclusive.
         */
        size_type Length() const { return this->size(); }

        /**
         *  @brief  The indexth item in the collection.
         *  @param  i_index Index into the collection.
         *  @return The node at the indexth position in the collection, or null if that is not a valid index.
         */
        PtrView< XmlNode* > Item( size_type i_index ) const;
    };

    /**
     *  XmlNodeVisitor is the base class for all the operations on an XmlNode tree that use the Visitor
     *  design pattern. It defines a default Visit operation for each and every concrete class derived
     *  from XmlNode.
     */
    class XmlNodeVisitor
    {
    public:
        virtual void VisitDocument( XmlDocument* );
        virtual void VisitDocument( const XmlDocument* );
        virtual void VisitElement( XmlElement* );
        virtual void VisitElement( const XmlElement* );
        virtual void VisitText( XmlText* );
        virtual void VisitText( const XmlText* );
        virtual void VisitComment( XmlComment* );
        virtual void VisitComment( const XmlComment* );
        virtual void VisitProcessingInstruction( XmlProcessingInstruction* );
        virtual void VisitProcessingInstruction( const XmlProcessingInstruction* );
    protected:
        XmlNodeVisitor() {}
    };

    /**
     *  XmlAttributeMap
     */
    typedef std::vector< std::pair< std::string, std::string > > XmlAttributeMap;

    /**
     *  XmlNode is the base class of Austria's XML document object model.
     */
    class XmlNode : public PtrTarget_Basic
    {
    public:
        /**
         *  @brief An enum type used to identify which type a node is.
         *  @note  Not all types of nodes are currently implemented.
         */
        enum {
            ELEMENT_NODE = 1,
            ATTRIBUTE_NODE = 2,
            TEXT_NODE = 3,
            CDATA_SECTION_NODE = 4,
            ENTITY_REFERENCE_NODE = 5,
            ENTITY_NODE = 6,
            PROCESSING_INSTRUCTION_NODE = 7,
            COMMENT_NODE = 8,
            DOCUMENT_NODE = 9,
            DOCUMENT_TYPE_NODE = 10,
            DOCUMENT_FRAGMENT_NODE = 11,
            NOTATION_NODE = 12
        };

        /**
         *  @brief  The name of this node, depending on its type.
         */
        virtual std::string Name() const = 0;

        /**
         *  @brief  The value of this node, depending on its type.
         */
        virtual std::string Value() const;

        /**
         *  @brief  The type of this node.
         *  @return A code representing the type of the underlying object, as defined above.
         */
        virtual unsigned short Type() const = 0;

        /**
         * @brief  The parent of this node.
         */
        PtrView< XmlComposite* > ParentNode() const { return m_parentNode; }

        /**
         *  @brief  The list of children of this node.
         *  @return An XmlNodeList that contains all the children of this node.
         */
        virtual XmlNodeList ChildNodes() const;

        /**
         *  @brief  The first child of this node.
         *  @return A pointer to the first child of this node, or a null pointer if there is no such node.
         */
        virtual PtrView< XmlNode* > FirstChild() const;

        /**
         *  @brief  The last child of this node.
         *  @return A pointer to the last child of this node, or a null pointer if there is no such node.
         */
        virtual PtrView< XmlNode* > LastChild() const;

        /**
         *  @brief  The node immediately preceding this node.
         *  @return A pointer to the node immediately preceding this node, or a null pointer if there
         *          is no such node.
         */
        PtrView< XmlNode* > PreviousSibling() const { return m_previousSibling; }

        /**
         *  @brief  The node immediately following this node.
         *  @return A pointer to the node immediately following this node, or a null pointer if there
         *          is no such node.
         */
        PtrView< XmlNode* > NextSibling() const { return m_nextSibling; }

        /**
         *  @brief  The XmlDocument object associated with this node.
         *  @return A pointer to the XmlDocument that created this node, or a null pointer if this node
         *          is an XmlDocument or an XmlDocumentType that is not used with any XmlDocument yet.
         */
        PtrView< XmlDocument* > OwnerDocument() const { return m_ownerDocument; }

        /**
         *  @brief  Inserts a child node.
         *
         *  Inserts the node i_newChild before the existing child node i_refChild. If i_refChild is null,
         *  inserts i_newChild at the end of the list of children.  If i_newChild is already in the tree,
         *  it is first removed. If it is an XmlDocumentFragment object, the entire contents of the document
         *  fragment are moved into the child list of this node
         *
         *  @param  i_newChild The node to insert.
         *  @param  i_refChild The reference node, i.e., the node before which the new node must be inserted.
         *  @return The inserted node.
         *  
         *  @exception ExceptionDerivation<XmlHierarchyRequestErr> if this node is of a type that does
         *             not allow children, or if i_newChild is an ancestor of this node.
         *  @exception ExceptionDerivation<XmlWrongDocumentErr> if i_newChild and this node are from
         *             different documents.
         *  @exception ExceptionDerivation<XmlNotFoundErr> if i_refChild is not a child of this node.
         */
        virtual PtrView< XmlNode* > InsertBefore( PtrDelegate< XmlNode* > i_newChild,
                                                  PtrView< XmlNode* >     i_refChild );

        /**
         *  @brief  Appends a child node.
         *
         *  Adds the node i_newChild to the end of the list of children of this node. If i_newChild is
         *  already in the tree, it is first removed. If it is an XmlDocumentFragment object, the entire
         *  contents of the document fragment are moved into the child list of this node
         *  
         *  @param  i_newChild The node to add.
         *  @return The added node.
         *  
         *  @exception ExceptionDerivation<XmlHierarchyRequestErr> if this node is of a type that does
         *             not allow children, or if i_newChild is an ancestor of this node.
         *  @exception ExceptionDerivation<XmlWrongDocumentErr> if i_newChild and this node are from
         *             different documents.
         */
        virtual PtrView< XmlNode* > AppendChild( PtrDelegate< XmlNode* > i_newChild );

        /**
         *  @brief  Removes a child node.
         *
         *  Removes i_oldChild of the list if children of this node.
         *  
         *  @param  i_oldChild The node to remove.
         *  @return The removed node.
         *  
         *  @exception ExceptionDerivation<XmlNotFoundErr> if i_oldChild is not a child of this node.
         */
        virtual PtrDelegate< XmlNode* > RemoveChild( PtrDelegate< XmlNode* > i_oldChild );

        /**
         *  @brief  Replaces a child node.
         *
         *  Replaces the child node i_oldChild with i_newChild in the list of children, and returns
         *  the i_oldChild node. If i_newChild is an XmlDocumentFragment object, i_oldChild is replaced
         *  by all of the XmlDocumentFragment children, which are inserted in the same order. If the
         *  i_newChild is already in the tree, it is first removed.
         *  
         *  @param  i_newChild The replacement node.
         *  @param  i_oldChild The node to replace.
         *  @return The replaced node.
         *  
         *  @exception ExceptionDerivation<XmlHierarchyRequestErr> if this node is of a type that does
         *             not allow children, or if i_newChild is an ancestor of this node.
         *  @exception ExceptionDerivation<XmlWrongDocumentErr> if i_newChild and this node are from
         *             different documents.
         *  @exception ExceptionDerivation<XmlNotFoundErr> if i_oldChild is not a child of this node.
         */
        virtual PtrDelegate< XmlNode* > ReplaceChild( PtrDelegate< XmlNode* > i_newChild,
                                                      PtrDelegate< XmlNode* > i_oldChild );

        /**
         *  @brief  Duplicates this node.
         *  @param  i_deep If true, recursively clone the subtree under the specified node; clone only the
         *          node itself (and its attributes, if it is an Element) otherwise.
         *  @return The duplicate node.
         *  @note   The duplicate node has no parent -- i.e. ParentNode returns null.
         *
         *  Cloning an Element copies all attributes and their values, including those generated by the XML
         *  processor to represent defaulted attributes, but this method does not copy any text it contains
         *  unless it is a deep clone, since the text is contained in a child Text node.  Cloning any other
         *  type of node simply returns a copy of this node.
         */
        virtual PtrDelegate< XmlNode* > Clone( bool i_deep ) const = 0;

        /**
         *  @brief  Whether this node has children or not.
         *  @return true if this node has children, false otherwise.
         */
        virtual bool HasChildNodes() const;

        /**
         *  @brief  Whether this node has attributes or not.
         *  @return true if this node has attributes, false otherwise.
         */
        virtual bool HasAttributes() const;

        /**
         *  @brief  Whether this node is an ancestor of a given node.
         *  @param  i_node A pointer to an XmlNode.
         *  @return true if this node is an ancestor of i_node, false otherwise.
         *  @note   A node is an ancestor of itself, so this->IsAncestorOf( this ) == true.
         */
        bool IsAncestorOf( PtrView< const XmlNode* > i_node ) const;

        /**
         *  @brief  Runtime type convertion to a pointer to XmlDocument.
         */
        virtual PtrView< XmlDocument* > AsDocument();

        /**
         *  @brief  Runtime type convertion to a pointer to const XmlDocument.
         */
        virtual PtrView< const XmlDocument* > AsDocument() const;

        /**
         *  @brief  Runtime type convertion to a pointer to XmlElement.
         */
        virtual PtrView< XmlElement* > AsElement();

        /**
         *  @brief  Runtime type convertion to a pointer to const XmlElement.
         */
        virtual PtrView< const XmlElement* > AsElement() const;

        /**
         *  @brief Accepts a visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op ) = 0;

        /**
         *  @brief Accepts a non-modifying visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op ) const = 0;

    protected:

        /**
         *  @brief  The node constructor.
         *  @param  i_ownerDocument The XmlDocument that created this node.
         */
        XmlNode( PtrView< XmlDocument* > i_ownerDocument ) :
            PtrTarget_Basic(),
            m_ownerDocument( i_ownerDocument ),
            m_nextSibling( 0 ),
            m_previousSibling( 0 ),
            m_parentNode( 0 )
            {}

    private:

        friend class XmlComposite;

        /**
         *  m_ownerDocument is an explicit reference to the Document object associated with this node.
         *  This is also the Document object used to create new nodes.  When this node is a Document,
         *  this is null.
         */
        PtrView< XmlDocument* > m_ownerDocument;

        /**
         *  An at::Ptr smart pointer to the node immediately following this node.
         */
        Ptr< XmlNode* > m_nextSibling;

        /**
         *  A dumb pointer to the node immediately preceding this node.
         */
        PtrView< XmlNode* > m_previousSibling;

        /**
         *  A dumb pointer to the parent node.
         *  Maintaining references from child nodes to their parent simplifies the traversal and
         *  management of the composite structure. The parent reference simplifies moving up the
         *  structure and deleting a node.
         */
        PtrView< XmlComposite* > m_parentNode;
    };

    /**
     * XmlHierarchyRequestErr is an exception class used with AT_ThrowDerivation.
     *
     * ExceptionDerivation<XmlHierarchyRequestErr> is thrown if any node is inserted somewhere
     * it doesn't belong. 
     */
    struct XmlHierarchyRequestErr {};

    /**
     * XmlWrongDocumentErr is an exception class used with AT_ThrowDerivation.
     *
     * ExceptionDerivation<XmlWrongDocumentErr> is thrown if a node is used in a different
     * document than the one that created it (that doesn't support it).
     */
    struct XmlWrongDocumentErr {};

    /**
     * XmlNotFoundErr is an exception class used with AT_ThrowDerivation.
     *
     * ExceptionDerivation<XmlNotFoundErr> is thrown if an attempt is made to reference a node
     * in a context where it does not exist.
     */
    struct XmlNotFoundErr {};

    /**
     * XmIndexSizeErr is an exception class used with AT_ThrowDerivation.
     *
     * ExceptionDerivation<XmIndexSizeErr> is thrown if an index or size is negative, or greater
     * than the allowed value.
     */
    struct XmlIndexSizeErr {};

    /**
     * XmlInvalidCharacterErr is an exception class used with AT_ThrowDerivation.
     *
     * ExceptionDerivation<XmlInvalidCharacterErr> is thrown if an invalid or illegal character
     * is specified, such as in a name. See production 2 in the XML specification for the definition
     * of a legal character, and production 5 for the definition of a legal name character.
     */
    struct XmlInvalidCharacterErr {};


    // ========== Inline Function Definitions ================================================================

    inline PtrView< XmlNode* > XmlNodeList::Item( size_type i_index ) const
    {
        return i_index < size() ? operator[]( i_index ) : Ptr< XmlNode* >( 0 );
    }

}; // namespace

#endif // x_at_xml_node_h_x
