/**
 * \file at_log_fstream.h
 *
 * \author Bradley Austin
 *
 * at_log_fstream.h contains an extension to the Austria logging module to
 * support writing to log files using the C++ Standard Library fstream
 * facility.
 *
 */

#ifndef x_at_log_fstream_h_x
#define x_at_log_fstream_h_x 1


#include "at_log.h"
#include "at_crtp.h"


#include <fstream>


// Austria namespace
namespace at
{


    class LogWriterOutputPolicy_FStream
      : public ThisInBaseInitializerHack< LogWriterOutputPolicy_FStream >
    {

    public:

        typedef
            LogWriterOutputPolicy_Stream< LogWriterOutputPolicy_FStream >
                t_ostream_type;

    private:

        t_ostream_type m_ostream;
        std::ofstream m_fs;
        bool m_opened;

    protected:

        using ThisInBaseInitializerHack< LogWriterOutputPolicy_FStream>::This;

        LogWriterOutputPolicy_FStream()
          : m_ostream( This() ),
            m_opened( false )
        {
        }

        ~LogWriterOutputPolicy_FStream() {}

    public:

        template< typename w_type >
        inline void Write( const w_type & i_value )
        {
            if ( ! m_opened )
            {
                throw LogWriterCantWriteException();
            }

            m_fs << i_value;
            if ( ! ( m_fs.good() && m_fs.is_open() ) )
            {
                throw LogWriterCantWriteException();
            }
        }

        void OpenFile( const std::string & i_file_name )
        {
            AT_Assert( ! m_opened );
            m_fs.open( i_file_name.c_str(), std::ios_base::app );
            m_opened = true;
            m_fs.rdbuf()->pubsetbuf(NULL,0);
        }

        void CloseFile()
        {
 //           AT_Assert( m_opened );
            m_fs.close();
            m_fs.clear();
            m_opened = false;
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        inline void Flush()
        {
            if ( ! m_opened )
            {
                throw LogWriterCantWriteException();
            }

            m_fs.flush();
            if ( ! ( m_fs.good() && m_fs.is_open() ) )
            {
                throw LogWriterCantWriteException();
            }
        }

        inline std::ofstream & FStream()
        {
            return m_fs;
        }

    private:

        /* Unimplemented */
        LogWriterOutputPolicy_FStream( const LogWriterOutputPolicy_FStream & );
        LogWriterOutputPolicy_FStream & operator=( const LogWriterOutputPolicy_FStream & );

    };


}


#endif
