/**
 * \file at_env.h
 *
 * \author Bradley Austin
 *
 */


#ifndef x_at_env_h_x
#define x_at_env_h_x 1


#include "at_preference.h"
#include "at_log.h"
#include "at_lifetime.h"


namespace at
{


    class EnvironmentPtrPolicy_SingleThreaded;


    template<
        class w_pref_mgr_type = PreferenceManager<>,
        class w_log_mgr_type = LogManager<>,
        class w_ptr_policy = EnvironmentPtrPolicy_SingleThreaded
    >
    class Environment
    {

    public:

        typedef  w_pref_mgr_type  t_pref_mgr_type;
        typedef  w_log_mgr_type  t_log_mgr_type;

    private:

        class Impl;

        typedef  typename w_ptr_policy::template t_ptr_type< Impl >  t_ptr_type_f;
        typedef  typename t_ptr_type_f::t_type  t_ptr_type;
        typedef  typename w_ptr_policy::t_ptr_target_type  t_ptr_target_type;

        class Impl
          : public t_ptr_target_type
        {

        public:

            Ptr< w_pref_mgr_type * > m_pref_mgr;

            w_log_mgr_type m_log_mgr;

            Impl()
              : m_pref_mgr( new w_pref_mgr_type )
            {}

        private:

            /* Unimplemented */
            Impl( const Impl & );
            Impl & operator=( const Impl & );

        };

        t_ptr_type m_impl;

    public:

        inline Environment( const Environment & i_env )
          : m_impl( i_env.m_impl )
        {
        }

    private:

        inline explicit Environment( Impl * io_impl )
          : m_impl( io_impl )
        {
        }

    public:

        inline static Environment Make()
        {
            return Environment( new Impl );
        }

        inline PtrView< t_pref_mgr_type * > PrefMgr() const
        {
            return m_impl->m_pref_mgr;
        }

        inline t_log_mgr_type & LogMgr() const
        {
            return m_impl->m_log_mgr;
        }

    private:

        /* Unimplemented */
        Environment & operator=( const Environment & );

    };


    class EnvironmentPtrPolicy_SingleThreaded
    {

    public:

        template< class w_impl_type >
        class t_ptr_type
        {

        public:

            typedef  Ptr< w_impl_type * >  t_type;

            /* Unimplemented */
            t_ptr_type();
            t_ptr_type( const t_ptr_type & );
            ~t_ptr_type();
            t_ptr_type & operator=( const t_ptr_type & );

        };

        typedef  PtrTarget_Basic  t_ptr_target_type;

    private:

        /* Unimplemented */
        EnvironmentPtrPolicy_SingleThreaded();
        EnvironmentPtrPolicy_SingleThreaded
            ( const EnvironmentPtrPolicy_SingleThreaded & );
        ~EnvironmentPtrPolicy_SingleThreaded();
        EnvironmentPtrPolicy_SingleThreaded & operator=
            ( const EnvironmentPtrPolicy_SingleThreaded & );

    };


}


#endif
