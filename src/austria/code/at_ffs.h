//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_ffs.h
 *
 */

#ifndef x_at_ffs_h_x
#define x_at_ffs_h_x 1

#include "at_exports.h"
#include "at_assert.h"
#include "at_types.h"
#include <string>
#include <cmath>

// Austria namespace
namespace at
{


// ======== FindFirstSet ==============================================
/**
 * FindFirstSet will return the first set bit of a integer
 *
 * @param w_IntType The type of the input parameter
 * @param i_value
 * @return nothing
 */


template <typename w_IntType>
inline int FindFirstSet(
    const w_IntType         & i_value
) {

    // w_IntType must be an intger - do a static assert
    AT_StaticAssert( IsInteger< w_IntType >::m_value, w_IntType_must_be_integer );

    if ( i_value == 0 )
    {
        return 0;
    }
    
    // This uses a floating point operation - conversion from integer
    // to floating point double will cause the exponent to contain the
    // bit position of the first bit.

    int l_exponent;

    std::frexp( static_cast<double>(i_value), &l_exponent );

    return l_exponent;
}   

}; // namespace

#endif // x_at_ffs_h_x


