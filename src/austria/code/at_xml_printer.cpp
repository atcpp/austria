//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * \file at_xml_printer.cpp
 *
 * \author Guido Gherardi
 *
 * The Austria XML Parser implementation.
 *
 * The implementation of Austria's XML Parser is based libxml2's C translation of the SAX interface
 * and uses the State design pattern to represent parsing states.
 */

#include "at_xml_printer.h"
#include "at_xml_document.h"
#include "at_exception.h"
#include "at_assert.h"
#include <ctype.h>

#ifdef _WIN32
// isblank function is a GNU extension. It checks for a blank character; that is, a space or a tab.
inline bool isblank( const char c ) { return c == ' ' || c == '\t'; }
#endif

#define AT_ThrowXmlPrinterErr(msg) AT_ThrowDerivation( at::XmlPrinterErr, msg )

// Austria namespace
namespace at
{
    // ========== Class XmlPrinter ===============================================================================
    /**
     *
     */
    class XmlPrinter :
        public XmlNodeVisitor
    {
    public:

        XmlPrinter( std::ostream& i_os ) : XmlNodeVisitor(), m_os( i_os ) {}

        virtual ~XmlPrinter() {}

        virtual void VisitDocument( XmlDocument* );
        virtual void VisitDocument( const XmlDocument* );
        virtual void VisitElement( XmlElement* );
        virtual void VisitElement( const XmlElement* );
        virtual void VisitText( XmlText* );
        virtual void VisitText( const XmlText* );
        virtual void VisitComment( XmlComment* );
        virtual void VisitComment( const XmlComment* );
        virtual void VisitProcessingInstruction( XmlProcessingInstruction* );
        virtual void VisitProcessingInstruction( const XmlProcessingInstruction* );

    protected:

        void IncrIndent();
        void DecrIndent();
        void NewLine();
        void CharacterData( const char c );
        void StartTag( const std::string& i_name, const XmlAttributeMap& i_attrs );
        void EndTag( const std::string& i_name );
        void EmptyElementTag( const std::string& i_name, const XmlAttributeMap& i_attrs );
        void Attribute( const std::string& i_name, const std::string& i_value );
        void Attributes( const XmlAttributeMap& i_attrs );

    private:

        std::ostream& m_os;
        std::string   m_indent;

    };

    inline void XmlPrinter::IncrIndent()
    {
        m_indent.push_back( '\t' );
    }

    inline void XmlPrinter::DecrIndent()
    {
        AT_Assert( m_indent.size() > 0 );
        m_indent.erase( m_indent.size() - 1 );
    }

    inline void XmlPrinter::NewLine()
    {
        m_os << std::endl << m_indent;
    }

    inline void XmlPrinter::CharacterData( const char c )
    {
        switch ( c )
        {
        case '&':
            m_os << "&amp;";
            break;
        case '<':
            m_os << "&lt;";
            break;
        case '>':
            m_os << "&gt;";
            break;
        default:
            m_os << c;
            break;
        }
    }

    inline void XmlPrinter::Attribute( const std::string& i_name, const std::string& i_value )
    {
        m_os << i_name << "=\"";
        const std::string::const_iterator l_end = i_value.end();
        for ( std::string::const_iterator l_itr = i_value.begin(); l_itr != l_end; l_itr++ )
        {
            switch ( const char c = *l_itr )
            {
            case '\'':
                m_os << "&apos;";
                break;
            case '"':
                m_os << "&quot;";
                break;
            default:
                m_os << c;
                break;
            }
        }
        m_os << "\"";
    }

    inline void XmlPrinter::Attributes( const XmlAttributeMap& i_attrs )
    {
        IncrIndent();
        const XmlAttributeMap::const_iterator l_end = i_attrs.end();
        for ( XmlAttributeMap::const_iterator l_itr = i_attrs.begin(); l_itr != l_end; l_itr++ )
        {
            NewLine();
            Attribute( l_itr->first, l_itr->second );
        }
        DecrIndent();
    }

    inline void XmlPrinter::StartTag( const std::string& i_name, const XmlAttributeMap& i_attrs )
    {
        // FIXME: we should check if the target name is a valid Name, as specified by
        //        http://www.w3.org/TR/2004/REC-xml-20040204/#NT-Name, but that is quite
        //        some work, so leave it for later (maybe).
        m_os << '<' << i_name;
        Attributes( i_attrs );
        m_os << '>';
    }

    inline void XmlPrinter::EndTag( const std::string& i_name )
    {
        m_os << "</" << i_name << '>';
    }

    inline void XmlPrinter::EmptyElementTag( const std::string& i_name, const XmlAttributeMap& i_attrs )
    {
        m_os << '<' << i_name;
        Attributes( i_attrs );
        m_os << "/>";
    }

    void XmlPrinter::VisitDocument( XmlDocument* i_doc )
    {
        VisitDocument( (const XmlDocument*)i_doc );
    }

    void XmlPrinter::VisitDocument( const XmlDocument* i_doc )
    {
        if ( i_doc )
        {
            for ( PtrView< const XmlNode* > l_child = i_doc->FirstChild();
                  l_child != 0;
                  l_child = l_child->NextSibling() )
            {
                l_child->Accept( *this );
                NewLine();
            }
        }
    }

    void XmlPrinter::VisitElement( XmlElement* i_elt )
    {
        VisitElement( (const XmlElement*)i_elt );
    }

    void XmlPrinter::VisitElement( const XmlElement* i_elt )
    {
        if ( i_elt )
        {
            if ( i_elt->HasChildNodes() )
            {
                StartTag( i_elt->TagName(), i_elt->Attributes() );
                for ( PtrView< const XmlNode* > l_child = i_elt->FirstChild();
                      l_child != 0;
                      l_child = l_child->NextSibling() )
                {
                    l_child->Accept( *this );
                }
                EndTag( i_elt->TagName() );
            }
            else
            {
                EmptyElementTag( i_elt->TagName(), i_elt->Attributes() );
            }
        }
    }

    void XmlPrinter::VisitText( XmlText* i_text  )
    {
        VisitText( (const XmlText*)i_text  );
    }

    void XmlPrinter::VisitText( const XmlText* i_text )
    {
        if ( i_text )
        {
            const std::string& l_content = i_text->Data();
            const std::string::const_iterator l_beg = l_content.begin();
            const std::string::const_iterator l_end = l_content.end();
            std::string::const_iterator l_itr = l_beg;
            while ( l_itr != l_end )
            {
                CharacterData( *l_itr++ );
            }
            if ( l_itr != l_beg )
            {
                do l_itr--; while ( l_itr != l_beg && isblank( *l_itr ) );
                if ( *l_itr == '\n' )
                {
                    m_indent.assign( l_itr + 1, l_end );
                }
            }
        }
    }

    void XmlPrinter::VisitComment( XmlComment* i_comment )
    {
        VisitComment( (const XmlComment*)i_comment );
    }

    void XmlPrinter::VisitComment( const XmlComment* i_comment )
    {
        if ( i_comment )
        {
            m_os << "<!--";
            char c = 0;
            const std::string& l_content = i_comment->Data();
            const std::string::const_iterator l_end = l_content.end();
            for ( std::string::const_iterator l_itr = l_content.begin(); l_itr != l_end; l_itr++ )
            {
                if ( ( c = *l_itr ) == '-' )
                {
                    m_os << c;
                    if ( ++l_itr != l_end && ( c = *l_itr ) == '-' )
                    {
                        AT_ThrowXmlPrinterErr( "comment containing \"--\" (double-hyphen)" );
                    }
                }
                m_os << c;
            }
            if ( c == '-' )
            {
                AT_ThrowXmlPrinterErr( "comment content ending with a \"-\" (hyphen)" );
            }
            m_os << "-->";
        }
    }

    void XmlPrinter::VisitProcessingInstruction( XmlProcessingInstruction* i_procInstr )
    {
        VisitProcessingInstruction( (const XmlProcessingInstruction*)i_procInstr );
    }

    void XmlPrinter::VisitProcessingInstruction( const XmlProcessingInstruction* i_procInstr )
    {
        // FIXME: we should check if the target name is a valid Name, as specified by
        //        http://www.w3.org/TR/2004/REC-xml-20040204/#NT-Name, but that is quite
        //        some work, so leave it for later (maybe).
        if ( i_procInstr )
        {
            m_os << "<?" << i_procInstr->Target();
            const std::string& l_content = i_procInstr->Data();
            if ( ! l_content.empty() )
            {
                m_os << ' ';
                const std::string::const_iterator l_end = l_content.end();
                for ( std::string::const_iterator l_itr = l_content.begin(); l_itr != l_end; l_itr++ )
                {
                    char c = *l_itr;
                    if ( c == '?' )
                    {
                        m_os << '?';
                        if ( ++l_itr != l_end && ( c = *l_itr ) == '>' )
                        {
                            AT_ThrowXmlPrinterErr( "\"?>\" in processing instruction data" );
                        }
                    }
                    m_os << c;
                }
            }
            m_os << "?>";
        }
    }

    void XmlPrint( std::ostream& i_outStream, PtrDelegate< XmlDocument* > i_xmlDoc )
    {
        if ( i_xmlDoc )
        {
            XmlPrinter l_printer( i_outStream );
            i_xmlDoc->Accept( l_printer );
        }
    }

    void XmlPrint( std::ostream& i_outStream, const XmlDocument& i_xmlDoc )
    {
        XmlPrinter l_printer( i_outStream );
        i_xmlDoc.Accept( l_printer );
    }

    // ========== Class XmlPrettyPrinter =========================================================================
    /**
     */
    class XmlPrettyPrinter :
        public XmlPrinter
    {
    public:

        XmlPrettyPrinter( std::ostream& i_os ) : XmlPrinter( i_os ) {}
        virtual ~XmlPrettyPrinter() {}

        virtual void VisitDocument( const XmlDocument* );
        virtual void VisitElement( const XmlElement* );
        virtual void VisitText( const XmlText* );

    };

    void XmlPrettyPrinter::VisitDocument( const XmlDocument* i_doc )
    {
        if ( i_doc )
        {
            for ( PtrView< const XmlNode* > l_child = i_doc->FirstChild();
                  l_child != 0;
                  l_child = l_child->NextSibling() )
            {
                NewLine();
                l_child->Accept( *this );
            }
            NewLine();
        }
    }

    void XmlPrettyPrinter::VisitText( const XmlText* i_text )
    {
        if ( i_text )
        {
            const std::string& l_content = i_text->Data();
            const std::string::const_iterator l_end = l_content.end();
            std::string::const_iterator l_itr = l_content.begin();
            while ( l_itr != l_end && isspace( *l_itr ) ) l_itr++;
            while ( l_itr != l_end )
            {
                char c = *l_itr;
                AT_Assert( ! isspace( c ) );
                do
                {
                    CharacterData( c );
                }
                while ( ++l_itr != l_end && ! isspace( c = *l_itr ) );
                if ( l_itr != l_end )
                {
                    while ( l_itr != l_end && isspace( *l_itr ) ) l_itr++;
                    if ( l_itr != l_end )
                    {
                        if ( *l_itr++ == '\n' )
                        {
                            NewLine();
                        }
                        else
                        {
                            CharacterData( ' ' );
                        }
                    }
                }
            }
        }
    }

    inline bool isBlankTextNode( PtrView< const XmlNode* > i_node )
    {
        if ( i_node && i_node->Type() == XmlNode::TEXT_NODE )
        {
            const std::string& l_content = i_node->Value();
            const std::string::const_iterator l_end = l_content.end();
            std::string::const_iterator l_itr = l_content.begin();
            while ( l_itr != l_end && isspace( *l_itr ) ) l_itr++;
            return l_itr == l_end;
        }
        return false;
    }

    void XmlPrettyPrinter::VisitElement( const XmlElement* i_elt )
    {
        if ( i_elt )
        {
            if ( i_elt->HasChildNodes() )
            {
                StartTag( i_elt->TagName(), i_elt->Attributes() );
                IncrIndent();
                for ( PtrView< const XmlNode* > l_child = i_elt->FirstChild();
                      l_child != 0;
                      l_child = l_child->NextSibling() )
                {
                    if ( ! isBlankTextNode( l_child ) )
                    {
                        NewLine();
                        l_child->Accept( *this );
                    }
                }
                DecrIndent();
                NewLine();
                EndTag( i_elt->TagName() );
            }
            else
            {
                EmptyElementTag( i_elt->TagName(), i_elt->Attributes() );
            }
        }
    }

    void XmlPrettyPrint( std::ostream& i_outStream, PtrDelegate< XmlDocument* > i_xmlDoc )
    {
        if ( i_xmlDoc )
        {
            XmlPrettyPrinter l_printer( i_outStream );
            i_xmlDoc->Accept( l_printer );
        }
    }

    void XmlPrettyPrint( std::ostream& i_outStream, const XmlDocument& i_xmlDoc )
    {
        XmlPrettyPrinter l_printer( i_outStream );
        i_xmlDoc.Accept( l_printer );
    }

};
