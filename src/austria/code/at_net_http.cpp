/*
 * This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 * Any unauthorized use of this file is prohibited and will be prosecuted
 * to the full extent of the law.
 */
#include "at_net_http.h"
#include "at_twinmt_basic.h"
#include "at_http_lexer.h"
#include "at_buffer.h"
#include "at_factory.h"
#include "at_net_ipv4.h"
#include "at_net_helpers.h"
#include "at_url_parse.h"
#include <vector>
#include <utility>
#include <memory>
#include <cassert>
#include <sstream>

using namespace at;

namespace
{
    // The version in std wants both types to be the same, which is sort
    // of annoying.
    template<typename A, typename B>
    A min(A a, B b) { return a < b ? a : A(b); }

    class HTTPNet;

    /**
     * These guys make message bodies suitable for consumption as well as
     * telling us where one message ends and the next message begins.
     */
class BodyDecoder
{
public:
        virtual ~BodyDecoder();

        enum State { Incomplete, Complete, Malformed };

        /**
         * Append to out as much of the contents of in as pertains to this
         * message with any transfer encoding removed.  Remove from in what
         * was copied to out.  It is expected that the first byte of the
         * in buffer in the first call made is the first byte of the
         * message body.
         * @param in The input buffer.  Raw message data is read from here.
         * @param out The output buffer.  Message body data with its
         * transfer encoding removed is appended to here, up to the
         * maximum unused capacity of the buffer.  (Ie, it is not extended
         * to make room for more data even if more data could be read into
         * it were it larger.)
         * @param propOut Sometimes properties are transmitted
         * interspersed with the message body.  If any are found, they
         * will be appended to propOut.
         * @param isLast You must pass true iff there will be no more
         * message data.
         * @param more On return, this will be set to true iff more data
         * could have been read into out but was not because it lacked
         * sufficient capacity.
         * @return Incomplete if the end of the message has not been seen
         * (in which case in should be empty), Complete if it has been seen
         * (in which case in may not be empty), or Malformed if the message
         * is no good (in which case the contents of in are undefined, but
         * don't matter much anyhow).  Once Complete or Malformed has been
         * returned, the internal state of this object is undefined; it
         * should not be used to process another message.
         */
        virtual State operator()(
            PtrView<Buffer*> in, PtrView<Buffer*> out,
            NetConnection::t_PropList *propOut,
            bool isLast, bool *more) = 0;
};

/**
    * For when the length of the body is determined by a Content-Length
    * header (or is otherwise somehow known in advance).
    */
class LengthBodyDecoder : public BodyDecoder
{
public:
        LengthBodyDecoder(Buffer::size_type length);
        LengthBodyDecoder(std::string& lengthStr);

        virtual ~LengthBodyDecoder() {}

        virtual State operator()(
            PtrView<Buffer*> in, PtrView<Buffer*> out,
            NetConnection::t_PropList *propOut,
            bool isLast, bool *more);

    private:
        /** The number of bytes until the message is complete. */
        Buffer::size_type m_need;
};

/**
    * For when the message body is chunked.
    */
class ChunkedBodyDecoder : public BodyDecoder
{
    public:
        ChunkedBodyDecoder();
        virtual ~ChunkedBodyDecoder() {}

        virtual State operator()(
            PtrView<Buffer*> in, PtrView<Buffer*> out,
            NetConnection::t_PropList *propOut,
            bool isLast, bool *more);
};

/**
    * This class doles out HTTPServerConnection objects as clients connect to a
    * listening HTTP address.  It is the aide for clients of this library
    * and the lead in its relationship with the underlying network layer.
    */
class HTTPListenAide :
    public LeadTwinMT_Basic<NetConnectLeadIf>,
    public AideTwinMT_Basic<NetConnectLeadIf>
{
    private:
        typedef LeadTwinMT_Basic<NetConnectLeadIf> Lead;
        typedef AideTwinMT_Basic<NetConnectLeadIf> Aide;

    public:
        HTTPListenAide(NetConnectLead*, PtrView<MutexRefCount *>, HTTPNet &i_hub);
        virtual ~HTTPListenAide();

    public: // LeadTwinMT_Basic<NetConnectLeadIf>,
        void AppLeadCompleted(TwinTraits::TwinCode);

    public: // AideTwinMT_Basic<NetConnectLeadIf>
        void AppAideCloseNotify(TwinTraits::TwinCode);

    public: // NetConnectLeadIf
        void Failed(const NetError &i_err);
        void Established(PtrDelegate<NetConnection*> io_endpoint);

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectLeadIf>::LeadAssociate(i_aide, i_sharedMutex);
        }

    private:
        HTTPNet &m_hub;
        bool m_dead;
        Ptr<MutexRefCount *>    m_stateMutex;
};

/**
    * This class represents the server side of HTTP connections.  It is
    * the aide for clients of this library and the lead in its
    * relationship with the underlying network layer.
    * in addition, decoding of input buffers occurs asynchronously to the
    * lead and the aide, via an at::Activity
    */
class HTTPServerConnection :
    public NetConnection,
    public LeadTwinMT_Basic<NetConnectionResponderIf>,
    public AideTwinMT_Basic<NetConnectionResponderIf>,
    public Activity
{
private:
        typedef LeadTwinMT_Basic<NetConnectionResponderIf> Lead;
        typedef AideTwinMT_Basic<NetConnectionResponderIf> Aide;

    public: // NetConnection
        bool Receive(PtrView<Buffer*> o_data);
        void Send(PtrDelegate<Buffer*> i_buffer);
        bool ReceiveOutOfBand(PtrView<Buffer*> o_data);
        bool SendOutOfBand(PtrDelegate<Buffer*> i_buffer);
        void ConnectionNotify(NetConnectionResponder & i_lead);
        t_PropList GetReceivedProperties();
        void AddOutgoingProperties(t_PropList &i_proplist);
        PtrDelegate<NetAddress*> PeerAddress();
        PtrDelegate<NetAddress*> LocalAddress();

    public: // NetConnectionResponderIf
        void ReceiveFailure(const NetConnectionError &i_error);
        void SendCompleted(PtrDelegate<const Buffer*>, const char*);
        void SendFailure(PtrDelegate<const Buffer*>, const char*,
                         const NetConnectionError&);
        void DataReady();
        void OutOfBandDataReady();
        void ReceivedPropertiesNotification(bool, bool);

    public: // Activity
        void Perform();

    public:
        HTTPServerConnection(PtrDelegate<NetConnection*> endpoint,
                        PtrView<MutexRefCount *> sharedMutex, 
                        HTTPNet & hub,
                       PtrDelegate<ActivityList*> activityList);
        virtual ~HTTPServerConnection();

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectionResponderIf>::LeadAssociate(i_aide, i_sharedMutex);
        }

    private:
        void SendPreamble();

        enum ReadState { RSPreamble, RSBody, RSMalformed };
        enum WriteState { WSPreamble, WSBody };

        struct SendResultCall
        {
            Ptr<const Buffer*> buffer;
            const char *ptr;
            const NetConnectionError *error;
            SendResultCall(Ptr<const Buffer*> b, const char *p,
                           const NetConnectionError *e) :
                buffer(b), ptr(p), error(e) { }
        };

    private:
        HTTPNet & m_hub;
        Ptr<NetConnection*> m_endpoint;

        WriteState m_writeState;
        t_PropList m_responseProps;
        std::list<SendResultCall> m_sendResults;

        ReadState m_readState;
        t_PropList m_requestProps;
        int m_dataReady;
        bool m_requestEnded;
        Ptr<Buffer*> m_inBuffer;
        std::auto_ptr<BodyDecoder> m_bodyDecoder;

        HttpLexerTraits::t_MapType m_lexerOutput;   // must precede m_lexer
        HttpRequestLexer m_lexer;

#if PC_DONE
        int m_propsSeen;
#endif

        Ptr<MutexRefCount *> m_stateMutex;

};

/**
    * This class passes off a HTTPClientConnection object once a
    * connection is made with a server.  It is the aide for clients of
    * this library and the lead in its relationship with the underlying
    * network layer.
    */
class HTTPConnectAide :
    public LeadTwinMT_Basic<NetConnectLeadIf>,
    public AideTwinMT_Basic<NetConnectLeadIf>
{
    private:
        typedef LeadTwinMT_Basic<NetConnectLeadIf> Lead;
        typedef AideTwinMT_Basic<NetConnectLeadIf> Aide;

    public:
        HTTPConnectAide(NetConnectLead*, PtrView<MutexRefCount *>, HTTPNet & i_hub);
        virtual ~HTTPConnectAide();

    public: // LeadTwinMT_Basic<NetConnectLead>,
        void AppLeadCompleted(TwinTraits::TwinCode);

    public: // AideTwinMT_Basic<NetConnectLeadIf>
        void AppAideCloseNotify(TwinTraits::TwinCode);

    public: // NetConnectLeadIf
        void Failed(const NetError &i_err);
        void Established(PtrDelegate<NetConnection*> io_endpoint);

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectLeadIf>::LeadAssociate(i_aide, i_sharedMutex);
        }

    private:
        HTTPNet & m_hub;
        bool m_dead;
        Ptr<MutexRefCount *>    m_stateMutex;
};

/**
    * This class represents the client side of HTTP connections.  It is
    * the aide for clients of this library and the lead in its
    * relationship with the underlying network layer.
    */
class HTTPClientConnection :
    public NetConnection,
    public LeadTwinMT_Basic<NetConnectionResponderIf>,
    public AideTwinMT_Basic<NetConnectionResponderIf>,
    public Activity
{
    private:
        typedef LeadTwinMT_Basic<NetConnectionResponderIf> Lead;
        typedef AideTwinMT_Basic<NetConnectionResponderIf> Aide;

    public: // NetConnection
        bool Receive(PtrView<Buffer*> o_data);
        void Send(PtrDelegate<Buffer*> i_buffer);
        bool ReceiveOutOfBand(PtrView<Buffer*> o_data);
        bool SendOutOfBand(PtrDelegate<Buffer*> i_buffer);
        void ConnectionNotify(NetConnectionResponder & i_lead);
        t_PropList GetReceivedProperties();
        void AddOutgoingProperties(t_PropList &i_proplist);
        PtrDelegate<NetAddress*> PeerAddress();
        PtrDelegate<NetAddress*> LocalAddress();

    public: // NetConnectionResponderIf
        void ReceiveFailure(const NetConnectionError &i_error);
        void SendCompleted(PtrDelegate<const Buffer*>, const char*);
        void SendFailure(PtrDelegate<const Buffer*>, const char*,
                         const NetConnectionError&);
        void DataReady();
        void OutOfBandDataReady();
        void ReceivedPropertiesNotification(bool, bool);

    public: // Activity
        void Perform();

    public:
        HTTPClientConnection(PtrDelegate<NetConnection*> endpoint,
                             PtrView<MutexRefCount *> sharedMutex, 
                             HTTPNet & hub,
                             PtrDelegate<ActivityList*> activityList);
        virtual ~HTTPClientConnection();

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<NetConnectionResponderIf>::LeadAssociate(i_aide, i_sharedMutex);
        }

    private:
        void SendPreamble();

        enum ReadState { RSPreamble, RSBody, RSMalformed };
        enum WriteState { WSPreamble, WSBody };

        struct SendResultCall
        {
            Ptr<const Buffer*> buffer;
            const char *ptr;
            const NetConnectionError *error;
            SendResultCall(Ptr<const Buffer*> b, const char *p,
                           const NetConnectionError *e) :
                buffer(b), ptr(p), error(e) { }
        };

    private:
        HTTPNet & m_hub;
        Ptr<NetConnection*> m_endpoint;

        WriteState m_writeState;
        t_PropList m_requestProps;
        std::list<SendResultCall> m_sendResults;

        ReadState m_readState;
        t_PropList m_responseProps;
        int m_dataReady;
        bool m_responseEnded;
        Ptr<Buffer*> m_inBuffer;
        std::auto_ptr<BodyDecoder> m_bodyDecoder;

        HttpLexerTraits::t_MapType m_lexerOutput;
        HttpResponseLexer m_lexer;

#if PC_DONE
        int m_propsSeen;
#endif

        Ptr<MutexRefCount *> m_stateMutex;
};

class HTTPNet : public Net
{
public:
        HTTPNet() {}
        virtual ~HTTPNet() {
            m_ip = NULL;
            // 1st pass at cleanup of lifetime messes.  ideally the vectors would contain SMART pointers.
            while (!m_lav.empty()) {
                HTTPListenAide *la = m_lav[0];
                m_lav.erase(m_lav.begin());
                delete la;
            }
            while (!m_cav.empty()) {
                HTTPConnectAide *ca = m_cav[0];
                m_cav.erase(m_cav.begin());
                delete ca;
            }
        }

    public: // Net
        void Resolve(
            NetResolverLead *i_lead,
            PtrDelegate<const NetAddress*> i_address);
        void ListenDatagram(
            NetDatagramLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters,
            PtrDelegate<Pool*> i_buffer_pool = 0);
        void Listen(
            NetConnectLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters);
        void Connect(
            NetConnectLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters);
        PtrDelegate<at::ActivityListOwner*> GetActivityListOwner();
    private:
        std::vector<HTTPListenAide *>   m_lav;
        std::vector<HTTPConnectAide *>  m_cav;

        Ptr<Net*> m_ip;
};

/** Turn HTTP addresses into IP addresses. */
Ptr<const NetAddress*>
DissectAddress(Ptr<const NetAddress*> addr)
{
        const HTTPAddress *h;
        UrlParser parser;

        // If it's already an IPv4Address, we're done.
        if (dynamic_cast<const IPv4Address*>(addr.Get())) return addr;

        // If it's not an HTTPAddress, we fail.
        h = dynamic_cast<const HTTPAddress*>(addr.Get());
        if (!h) return 0;

        // If we can't parse the URI, we fail.
        if (!parser.Parse(h->uri)) return 0;

        // Stuff the relevant bits into something that will resolve to
        // what we want.
        Ptr<NetAddress_NamePort*> ptr;
        ptr = new NetAddress_NamePort(
            parser.m_host,
            (parser.m_port.m_is_set ? atoi(parser.m_port.c_str())
             : (int)parser.InitialDefaultPortNo));

        // And return whatever we get by trying to resolve it.
        Ptr<Net*> ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
        if (!ip) {
            return 0;
        }
        return SyncResolve(ip , ptr);
    }
}

const HTTPError HTTPError::s_malformed_request("Malformed request");

// BodyDecoder ////////////////////////////////////////////////////////////////
BodyDecoder::~BodyDecoder()
{
}


// LengthBodyDecoder //////////////////////////////////////////////////////////
LengthBodyDecoder::LengthBodyDecoder(Buffer::size_type length) :
    m_need(length)
{
}

LengthBodyDecoder::LengthBodyDecoder(std::string& lengthStr)
{
    std::istringstream ist(lengthStr);
    ist >> m_need;          // @@ TODO study use cases to see if strtoull was warranted
}

BodyDecoder::State
LengthBodyDecoder::operator()(
    PtrView<Buffer*> in, PtrView<Buffer*> out,
    NetConnection::t_PropList *propOut,
    bool isLast, bool *more)
{
    State st = Incomplete;
    Buffer::size_type cap = out->capacity();
    Buffer::size_type inAvail = min(m_need, in->size());
    Buffer::size_type outAvail = cap - out->size();
    Buffer::size_type toCopy = min(inAvail, outAvail);
    out->append(in->begin(), in->begin() + toCopy);
    in->Erase(0, toCopy);
    AT_Assert(out->capacity() <= cap); // Make sure we didn't grow the buffer
    m_need -= toCopy;
    if (isLast && m_need) st = Malformed;
    else if (m_need == 0) st = Complete;
    *more = inAvail > outAvail;
    return st;
}


// ChunkedBodyDecoder /////////////////////////////////////////////////////////
ChunkedBodyDecoder::ChunkedBodyDecoder()
{
}

ChunkedBodyDecoder::State
ChunkedBodyDecoder::operator()(
    PtrView<Buffer*> in, PtrView<Buffer*> out,
    NetConnection::t_PropList *propOut,
    bool isLast, bool *more)
{
    // TODO: Implement this.
    AT_Assert(!"HTTP::ChunkedBodyDecoder::operator() -- not supported");
    return Incomplete;
}


// HTTPListenAide /////////////////////////////////////////////////////////////
HTTPListenAide::HTTPListenAide(
    NetConnectLead *lead,
    PtrView<MutexRefCount *> m_sharedMutex,
    HTTPNet & i_hub
) :
    m_hub(i_hub),
    m_dead(false),
    m_stateMutex(m_sharedMutex)
{
    this->AideAssociate(lead, m_stateMutex);
}

HTTPListenAide::~HTTPListenAide()
{
//    AideCancel();
}

void
HTTPListenAide::AppLeadCompleted(TwinTraits::TwinCode)
{
//    AideCancel();
}

void
HTTPListenAide::AppAideCloseNotify(TwinTraits::TwinCode i_completion_code)
{
//    LeadCancel();
}

void
HTTPListenAide::Failed(const NetError &i_err)
{
    CallLead().VoidCall ( &NetConnectLeadIf::Failed,i_err);
}

void
HTTPListenAide::Established(PtrDelegate<NetConnection*> io_endpoint)
{
    Ptr<NetConnection*> grrr = io_endpoint;
    Ptr<HTTPServerConnection*> conn = new HTTPServerConnection(grrr, m_stateMutex, m_hub, LibraryPool());
    CallLead().VoidCall ( &NetConnectLeadIf::Established, conn);
    conn->Enqueue();
}


// HTTPConnectAide ////////////////////////////////////////////////////////////
HTTPConnectAide::HTTPConnectAide(
    NetConnectLead *lead,
    PtrView<MutexRefCount *> m_sharedMutex,
    HTTPNet & i_hub
) :
    m_hub(i_hub),
    m_dead(false),
    m_stateMutex(m_sharedMutex)
{
    this->AideAssociate(lead, m_stateMutex);
}

HTTPConnectAide::~HTTPConnectAide()
{
    LeadCancel();
    AideCancel();
}

void
HTTPConnectAide::AppLeadCompleted(TwinTraits::TwinCode)
{
    AideCancel();
}

void
HTTPConnectAide::AppAideCloseNotify(TwinTraits::TwinCode i_completion_code)
{
    LeadCancel();
}

void
HTTPConnectAide::Failed(const NetError &i_err)
{
    CallLead().VoidCall ( &NetConnectLeadIf::Failed,i_err);
}

void
HTTPConnectAide::Established(PtrDelegate<NetConnection*> io_endpoint)
{
    Ptr<NetConnection*> grrr = io_endpoint;
    Ptr<HTTPClientConnection*> conn = new HTTPClientConnection(grrr, m_stateMutex, m_hub, LibraryPool());
    CallLead().VoidCall ( &NetConnectLeadIf::Established,conn);
    conn->Enqueue();
    // TWIN: this->AideCancel();
}


// HTTPServerConnection /////////////////////////////////////////////////////////////
HTTPServerConnection::HTTPServerConnection(PtrDelegate<NetConnection*> endpoint,PtrView<MutexRefCount *> sharedMutex, 
                                            HTTPNet & hub,PtrDelegate<ActivityList*> activityList) :
    Activity(activityList),
    m_hub(hub),
    m_endpoint(endpoint),
    m_writeState(WSPreamble),
    m_responseProps(),
    m_sendResults(),
    m_readState(RSPreamble),
    m_requestProps(),
    m_dataReady(0),
    m_requestEnded(false),
    m_inBuffer(NewBuffer()),
    m_bodyDecoder(),
    m_lexerOutput(),
    m_lexer(&m_lexerOutput),
#if PC_DONE
    m_propsSeen(0),
#endif
    m_stateMutex(sharedMutex)
{
    m_endpoint->ConnectionNotify(*this);
}

HTTPServerConnection::~HTTPServerConnection()
{
    this->Disable(true);
    this->Lead::LeadCancel();
#if PC_DONE
    if (m_writeState != WSBody && m_responseProps.size())
        this->SendPreamble();
    m_inBuffer = 0;
    m_bodyDecoder.reset(0);
#endif
    // To ease debugging.
    m_endpoint = 0;
    m_stateMutex = 0;
}

bool
HTTPServerConnection::Receive(PtrView<Buffer*> o_data)
{
    Lock<Ptr<MutexRefCount*> > stateLock(m_stateMutex);
    if (m_readState != RSBody || m_requestEnded) {
        return false;
    }
    if (m_inBuffer->capacity() == 0) {
        m_inBuffer->reserve(1000);
    }
    while (m_endpoint->Receive(m_inBuffer))
    {
        m_inBuffer->reserve(m_inBuffer->capacity() * 2 + 1);
        // This Unlock object is here just to make sure that we don't
        // hog the lock through several iterations of this loop.  This
        // assumes fair locking, of course.  That's probably a poor
        // assumption.
        Unlock<Ptr<MutexRefCount*> > ul(m_stateMutex);
    }
    // Something might have changed, since we unlocked the mutex briefly
    // between loop iterations, so we check our preconditions again.
    if (m_readState != RSBody || m_requestEnded) {
        return false;
    }
    AT_Assert(m_bodyDecoder.get());

    t_PropList newProps;
    BodyDecoder::State st;
    bool more;
    st = (*m_bodyDecoder)(m_inBuffer, o_data, &newProps, false, &more);
    if (st == BodyDecoder::Complete) {
        m_requestEnded = true;
    } else if (st == BodyDecoder::Malformed) {
        m_readState = RSMalformed;
    }
    if (newProps.size()) {
        Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
        m_requestProps.insert(m_requestProps.end(), newProps.begin(), newProps.end());
        this->Enqueue();
    }
    return more;
}

void
HTTPServerConnection::Send(PtrDelegate<Buffer*> i_buffer)
{
    Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
    if (m_writeState != WSBody) {
        this->SendPreamble();
    }
    // FIX: Design oversight: Rather than sending the data directly as it
    // is received, it should be passed to a BodyEncoder that is akin to
    // the BodyDecoder classes but goes the other direction.
    m_endpoint->Send(i_buffer);
}

bool
HTTPServerConnection::ReceiveOutOfBand(PtrView<Buffer*> o_data)
{
    // We don't do that here.
    AT_Assert(!"HTTPServerConnection::ReceiveOutOfBand - Not supported");
    return false;
}

bool
HTTPServerConnection::SendOutOfBand(PtrDelegate<Buffer*> i_buffer)
{
    // We don't do that here.
    AT_Assert(!"HTTPServerConnection::SendOutOfBand - Not supported");
    return false;
}

void
HTTPServerConnection::ConnectionNotify(NetConnectionResponder & i_lead)
{
    this->AideAssociate(&i_lead, m_stateMutex);
}

HTTPServerConnection::t_PropList
HTTPServerConnection::GetReceivedProperties()
{
    Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
    return m_requestProps;
}

void
HTTPServerConnection::AddOutgoingProperties(t_PropList &i_proplist)
{
    Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
    // FIX: Design oversight: Rather than asserting that we have not yet
    // sent the preamble, outgoing properties should be sent to our
    // (presently nonexistent) BodyEncoder.  (See the comment in the Send
    // method.)  The BodyEncoder may be able to accept the property for
    // delivery interspersed with or after the body has been sent.
    AT_Assert(m_writeState == WSPreamble);
    m_responseProps.insert(m_responseProps.end(),
                           i_proplist.begin(),
                           i_proplist.end());
}

PtrDelegate<NetAddress*>
HTTPServerConnection::PeerAddress()
{
    return m_endpoint->PeerAddress();
}

PtrDelegate<NetAddress*>
HTTPServerConnection::LocalAddress()
{
    return m_endpoint->LocalAddress();
}

void
HTTPServerConnection::ReceiveFailure(const NetConnectionError &i_error)
{
    CallLead().VoidCall ( &NetConnectionResponder::ReceiveFailure,i_error);
}

void
HTTPServerConnection::SendCompleted(
    PtrDelegate<const Buffer*> i_buffer, const char *i_end)
{
    {
        Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
        m_sendResults.push_back(SendResultCall(i_buffer, i_end, 0));
    }
    this->Enqueue();
}

void
HTTPServerConnection::SendFailure(
    PtrDelegate<const Buffer*> i_buffer,
    const char *i_begin,
    const NetConnectionError &i_error)
{
    {
        Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
        m_sendResults.push_back(SendResultCall(i_buffer, i_begin, &i_error));
    }
    this->Enqueue();
}

void
HTTPServerConnection::DataReady()
{
    bool call = true;
    {
        Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
        if (m_requestEnded || m_readState == RSMalformed) call = false;
        else if (m_readState == RSBody) ++m_dataReady;
    }
    if (call) this->Enqueue();
}

void
HTTPServerConnection::OutOfBandDataReady()
{
    // We don't expect to receive any of these, but they are probably safe
    // to ignore.
    // TODO: Log something.
}

void
HTTPServerConnection::ReceivedPropertiesNotification(
    bool i_complete,
    bool i_props_in_queue)
{
    // We don't expect to receive any of these, but they are probably safe
    // to ignore.
    // TODO: Log something.
}

void
HTTPServerConnection::Perform()
{

    Lock<Ptr<MutexRefCount*> > l(m_stateMutex);

    while (m_sendResults.size())
    {
        SendResultCall c = m_sendResults.front();
        {
            if (c.error) {
                CallLead().VoidCallRMutex ( m_stateMutex, &NetConnectionResponder::SendFailure,c.buffer,c.ptr,*c.error);
            } else {
                CallLead().VoidCallRMutex ( m_stateMutex, &NetConnectionResponder::SendCompleted,c.buffer,c.ptr);
            }
        }
        m_sendResults.pop_front();
    }

// This is not necessary now (because we're only doing HTTP/1.0 so all of
// the properties come in as soon as we're done parsing the preamble) and
// will be inadequate later (because we might have seen all the properties
// before m_requestEnded == true--in fact, this will be the case all the
// time except when the client uses chunked encoding.  So I am leaving
// this here because it is nearly correct and will just require minor
// tweaking later.
#if see_comment_a_ways_below
    while (m_requestProps.size() > (unsigned) m_propsSeen)
    {
        {
            Lock<Ptr<MutexRefCount*> > aideLock(this->Aide::m_mutexPtr);
            Unlock<Ptr<MutexRefCount*> > ul(m_stateMutex);
            if (this->Aide::m_pointer)
            {
                this->Aide::m_pointer->ReceivedPropertiesNotification(
                    m_requestEnded, true);
            }
        }
        m_propsSeen = m_requestProps.size();
    }
#endif

    if (m_readState == RSPreamble && m_requestEnded == false)
    {
        if (m_inBuffer->capacity() == 0) m_inBuffer->reserve(1000);
        while (m_endpoint->Receive(m_inBuffer))
        {
            m_inBuffer->reserve(m_inBuffer->capacity() * 2 + 1);
            // This Unlock object is here just to make sure that we
            // don't hog the lock through several iterations of this
            // loop.  This assumes fair locking, of course.  That's
            // probably a poor assumption.
            Unlock<Ptr<MutexRefCount*> > ul(m_stateMutex);
        }
        // Since we unlocked the mutex briefly, something might have
        // changed, so we check our preconditions again.
        if (m_readState == RSPreamble && m_requestEnded == false) {
            HttpRequestLexer::ParseResult r;
            r = m_lexer.ParseSome(m_inBuffer, false);
            if (r == HttpLexerTraits::Invalid) {
                m_readState = RSMalformed;
                CallLead().VoidCallRMutex ( m_stateMutex,
                    &NetConnectionResponder::ReceiveFailure,HTTPError::s_malformed_request);
            } else if (r == HttpLexerTraits::Success) {
                std::string methodStr = m_lexerOutput[HttpRequestLexer::s_MethodKey];
                m_requestProps.push_back(new HTTPMethodProperty(methodStr));
                std::string uriStr = m_lexerOutput[HttpRequestLexer::s_RequestURIKey];
                m_requestProps.push_back(new HTTPURIProperty(uriStr));
                std::string versionStr = m_lexerOutput[HttpLexerTraits::s_HTTPVersionStrKey];
                m_requestProps.push_back(new HTTPVersionProperty(versionStr));
                m_lexerOutput.erase(HttpLexerTraits::s_HTTPVersionStrKey);
                m_lexerOutput.erase(HttpRequestLexer::s_MethodKey);
                m_lexerOutput.erase(HttpRequestLexer::s_RequestURIKey);
                HttpLexerTraits::t_MapType::iterator it;
                HttpLexerTraits::t_MapType::iterator end;
                for (it = m_lexerOutput.begin(), end = m_lexerOutput.end(); it != end; ++it) {
                    if (NoCaseCompareExact(it->first, "content-length")) {
                        m_bodyDecoder.reset(new LengthBodyDecoder(it->second));
                    }
                    m_requestProps.push_back(new HTTPHeaderProperty(it->first, it->second));
                }
                if (m_bodyDecoder.get() == 0) {
                    m_bodyDecoder.reset(new LengthBodyDecoder(0));
                }
                m_readState = RSBody;
                const bool lvalTrue = true; // workaround compiler
                CallLead().VoidCallRMutex ( m_stateMutex,
                    &NetConnectionResponder::ReceivedPropertiesNotification, lvalTrue, lvalTrue);
                if ( m_inBuffer->size()) {
                    CallLead().VoidCallRMutex ( m_stateMutex,&NetConnectionResponder::DataReady);
                }
            }
        }
    } else if (m_readState == RSBody && m_requestEnded == false) {
        // REV: I don't know whether we need to send multiple DataReady
        // calls if we have "buffered" multiple DataReady calls, but I
        // can't see why we would.
        CallLead().VoidCallRMutex ( m_stateMutex,&NetConnectionResponder::DataReady);
        m_dataReady = 0;
    }

}

void
HTTPServerConnection::SendPreamble()
{
    Lock<Ptr<MutexRefCount*> > l(m_stateMutex);
    if (m_writeState == WSBody) {
        return;
    }
    m_writeState = WSBody;
    int major = 1;
    int minor = 0;
    int statusCode = 200;
    std::string reasonPhrase;
    std::list<std::string> headers;
    Ptr<Buffer*> buf;
    t_PropList::iterator it = m_responseProps.begin();
    t_PropList::iterator end = m_responseProps.end();

    for (; it != end; ++it)
    {
        NetProperty *p = it->Get();
        HTTPVersionProperty *v;
        HTTPStatusProperty *s;
        HTTPHeaderProperty *h;
        if (( v = dynamic_cast<HTTPVersionProperty*>(p) ))
        {   
            major = v->major;
            minor = v->minor;
        }
        else if (( s = dynamic_cast<HTTPStatusProperty*>(p) ))
        {
            statusCode = s->statusCode;
            reasonPhrase = s->reasonPhrase;
        }
        else if (( h = dynamic_cast<HTTPHeaderProperty*>(p) ))
        {
            headers.push_back(h->name + std::string(": ") + h->value + "\r\n");
        }
        else
        {
            AT_Assert(!"HTTPServerConnection::SendPreamble -- unrecognized property");
        }
    }

    buf = NewBuffer();
    buf->reserve(reasonPhrase.size() + 100);
    int x = sprintf(buf->begin(), "HTTP/%d.%d %d %s\r\n",major, minor, statusCode, reasonPhrase.c_str());
    buf->SetAllocated(x);
    while (headers.size())
    {
        buf->Append(headers.front());
        headers.pop_front();
    }
    buf->Append(std::string("\r\n"));

    this->Send(buf);
}


// HTTPClientConnection ///////////////////////////////////////////////////////
HTTPClientConnection::HTTPClientConnection(PtrDelegate<NetConnection*> endpoint, PtrView<MutexRefCount *> sharedMutex, 
                                        HTTPNet & hub, PtrDelegate<ActivityList*> activityList) :
    Activity(activityList),
    m_hub(hub),
    m_endpoint(endpoint),
    m_writeState(WSPreamble),
    m_requestProps(),
    m_sendResults(),
    m_readState(RSPreamble),
    m_responseProps(),
    m_dataReady(0),
    m_responseEnded(false),
    m_inBuffer(NewBuffer()),
    m_bodyDecoder(),
    m_lexerOutput(),
    m_lexer(&m_lexerOutput),
#if PC_DONE
    m_propsSeen(0),
#endif
    m_stateMutex(sharedMutex)
{
    m_endpoint->ConnectionNotify(*this);
}

HTTPClientConnection::~HTTPClientConnection()
{
    this->Disable(true);
#if PC_DONE
    if (m_writeState != WSBody && m_requestProps.size())
        this->SendPreamble();
#endif
}

bool
HTTPClientConnection::Receive(PtrView<Buffer*> o_data)
{
    Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
    if (m_readState != RSBody || m_responseEnded) {
        return false;
    }
    if (m_inBuffer->capacity() == 0) m_inBuffer->reserve(1000);
    while (m_endpoint->Receive(m_inBuffer))
    {
        m_inBuffer->reserve(m_inBuffer->capacity() * 2 + 1);
        // This Unlock object is here just to make sure that we don't
        // hog the lock through several iterations of this loop.  This
        // assumes fair locking, of course.  That's probably a poor
        // assumption.
        Unlock<Ptr<MutexRefCount *> > ul(m_stateMutex);
    }
    // Something might have changed, since we unlocked the mutex briefly
    // between loop iterations, so we check our preconditions again.
    if (m_readState != RSBody || m_responseEnded) {
        return false;
    }

    AT_Assert(m_bodyDecoder.get());
    t_PropList newProps;
    BodyDecoder::State st;
    bool more;
    st = (*m_bodyDecoder)(m_inBuffer, o_data, &newProps, false, &more);
    if (st == BodyDecoder::Complete) {
        m_responseEnded = true;
    } else if (st == BodyDecoder::Malformed) {
        m_readState = RSMalformed;
    }
    if (newProps.size()) {
        Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
        m_requestProps.insert(m_requestProps.end(), newProps.begin(), newProps.end());
        this->Enqueue();
    }
    return more;
}

void
HTTPClientConnection::Send(PtrDelegate<Buffer*> i_buffer)
{
    Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
    if (m_writeState != WSBody) {
        this->SendPreamble();
    }
    // FIX: Design oversight: Rather than sending the data directly as it
    // is received, it should be passed to a BodyEncoder that is akin to
    // the BodyDecoder classes but goes the other direction.
    m_endpoint->Send(i_buffer);
}

bool
HTTPClientConnection::ReceiveOutOfBand(PtrView<Buffer*> o_data)
{
    // We don't do that here.
    AT_Assert(!"HTTPClientConnection::ReceiveOutOfBand - not supported");
    return false;
}

bool
HTTPClientConnection::SendOutOfBand(PtrDelegate<Buffer*> i_buffer)
{
    // We don't do that here.
    AT_Assert(!"HTTPClientConnection::SendOutOfBand - not supported");
    return false;
}

void
HTTPClientConnection::ConnectionNotify(NetConnectionResponder & i_lead)
{
    this->AideAssociate(&i_lead, m_stateMutex);
}

HTTPClientConnection::t_PropList
HTTPClientConnection::GetReceivedProperties()
{
    Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
    return m_responseProps;
}

void
HTTPClientConnection::AddOutgoingProperties(t_PropList &i_proplist)
{
    Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
    // FIX: Design oversight: Rather than asserting that we have not yet
    // sent the preamble, outgoing properties should be sent to our
    // (presently nonexistent) BodyEncoder.  (See the comment in the Send
    // method.)  The BodyEncoder may be able to accept the property for
    // delivery interspersed with or after the body has been sent.
    AT_Assert(m_writeState == WSPreamble);
    m_requestProps.insert(m_requestProps.end(),
                          i_proplist.begin(),
                          i_proplist.end());
}

PtrDelegate<NetAddress*>
HTTPClientConnection::PeerAddress()
{
    return m_endpoint->PeerAddress();
}

PtrDelegate<NetAddress*>
HTTPClientConnection::LocalAddress()
{
    return m_endpoint->LocalAddress();
}

void
HTTPClientConnection::ReceiveFailure(const NetConnectionError &i_error)
{
    CallLead().VoidCall ( &NetConnectionResponder::ReceiveFailure,i_error);
}

void
HTTPClientConnection::SendCompleted(
    PtrDelegate<const Buffer*> i_buffer, const char *i_end)
{
    {
        Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
        m_sendResults.push_back(SendResultCall(i_buffer, i_end, 0));
    }
    this->Enqueue();
}

void
HTTPClientConnection::SendFailure(
    PtrDelegate<const Buffer*> i_buffer,
    const char *i_begin,
    const NetConnectionError &i_error)
{
    {
        Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
        m_sendResults.push_back(SendResultCall(i_buffer, i_begin, &i_error));
    }
    this->Enqueue();
}

void
HTTPClientConnection::DataReady()
{
    bool call = true;
    {
        Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
        if (m_responseEnded || m_readState == RSMalformed) {
            call = false;
        } else if (m_readState == RSBody) {
            ++m_dataReady;
        }
    }
    if (call) {
        this->Enqueue();
    }
}

void
HTTPClientConnection::OutOfBandDataReady()
{
    // We don't expect to receive any of these, but they are probably safe
    // to ignore.
    // TODO: Log something.
}

void
HTTPClientConnection::ReceivedPropertiesNotification(
    bool i_complete,
    bool i_props_in_queue)
{
    // We don't expect to receive any of these, but they are probably safe
    // to ignore.
    // TODO: Log something.
}

void
HTTPClientConnection::Perform()
{
    try
    {
        Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);

        while (m_sendResults.size()) {
            SendResultCall c = m_sendResults.front();
            if (c.error) {
                CallLead().VoidCallRMutex ( m_stateMutex, &NetConnectionResponder::SendFailure, c.buffer, c.ptr, *c.error);
            } else {
                CallLead().VoidCallRMutex ( m_stateMutex, &NetConnectionResponder::SendCompleted, c.buffer, c.ptr);
            }
            m_sendResults.pop_front();
        }

// This is not necessary now (because we're only doing HTTP/1.0 so all of
// the properties come in as soon as we're done parsing the preamble) and
// will be inadequate later (because we might have seen all the properties
// before m_responseEnded == true--in fact, this will be the case all the
// time except when the server uses chunked encoding.  So I am leaving
// this here because it is nearly correct and will just require minor
// tweaking later.
#if see_comment_a_ways_below
        while (m_responseProps.size() > (unsigned) m_propsSeen)
        {
            {
                CallLead().VoidCallRMutex ( m_stateMutex,
                    &NetConnectionResponder::ReceivedPropertiesNotification, m_responseEnded, true);
                }
            }
            m_propsSeen = m_responseProps.size();
        }
#endif

        if (m_readState == RSPreamble && m_responseEnded == false) {
            if (m_inBuffer->capacity() == 0) {
                m_inBuffer->reserve(1000);
            }
            while (m_endpoint->Receive(m_inBuffer)) {
                m_inBuffer->reserve(m_inBuffer->capacity() * 2 + 1);
                // This Unlock object is here just to make sure that we
                // don't hog the lock through several iterations of this
                // loop.  This assumes fair locking, of course.  That's
                // probably a poor assumption.
                Unlock<Ptr<MutexRefCount *> > ul(m_stateMutex);
            }
            // Since we unlocked the mutex briefly, something might have
            // changed, so we check our preconditions again.
            if (m_readState == RSPreamble && m_responseEnded == false) {
                HttpRequestLexer::ParseResult r;
                r = m_lexer.ParseSome(m_inBuffer, false);
                if (r == HttpLexerTraits::Invalid) {
                    m_readState = RSMalformed;
                    CallLead().VoidCallRMutex ( m_stateMutex, 
                        &NetConnectionResponder::ReceiveFailure,HTTPError::s_malformed_request);
                } else if (r == HttpLexerTraits::Success) {
                    std::string statusCodeStr = m_lexerOutput[HttpResponseLexer::s_StatusCodeKey];
                    std::string statusTextStr = m_lexerOutput[HttpResponseLexer::s_StatusTextKey];
                    m_responseProps.push_back(new HTTPStatusProperty(atoi(statusCodeStr.c_str()),statusTextStr));
                    std::string versionStr = m_lexerOutput[HttpLexerTraits::s_HTTPVersionStrKey];
                    m_responseProps.push_back(new HTTPVersionProperty(versionStr));
                    m_lexerOutput.erase(HttpLexerTraits::s_HTTPVersionStrKey);
                    m_lexerOutput.erase(HttpResponseLexer::s_StatusCodeKey);
                    m_lexerOutput.erase(HttpResponseLexer::s_StatusTextKey);
                    HttpLexerTraits::t_MapType::iterator it;
                    HttpLexerTraits::t_MapType::iterator end;
                    for (it = m_lexerOutput.begin(), end = m_lexerOutput.end(); it != end; ++it) {
                        if (NoCaseCompareExact(it->first, "content-length")) {
                            m_bodyDecoder.reset(new LengthBodyDecoder(it->second));
                        }
                        m_responseProps.push_back(new HTTPHeaderProperty(it->first, it->second));
                    }
                    if (m_bodyDecoder.get() == 0) {
                        m_bodyDecoder.reset(new LengthBodyDecoder(s_receive_max_default()));
                    }
                    m_readState = RSBody;
                    {
                        // The first parameter to this call says that
                        // we have received all the properties.  This
                        // will not necessarily be true once we support
                        // chunked encoding, so we will need more
                        // intelligence here and the section above
                        // pertaining to making property notifications
                        // will need to be uncommented and have some
                        // work done on it.
                        const bool lvalTrue = true; // workaround compiler
                        CallLead().VoidCallRMutex ( m_stateMutex,
                            &NetConnectionResponder::ReceivedPropertiesNotification, lvalTrue, lvalTrue);
                        if ( m_inBuffer->size() ) {
                            CallLead().VoidCallRMutex ( m_stateMutex, &NetConnectionResponder::DataReady);
                        }
                    }
                }
            }
        }
        else if (m_readState == RSBody && m_responseEnded == false) {
            // REV: I don't know whether we need to send multiple DataReady
            // calls if we have "buffered" multiple DataReady calls, but I
            // can't see why we would.
            CallLead().VoidCallRMutex ( m_stateMutex, &NetConnectionResponder::DataReady);
            m_dataReady = 0;
        }
    }
    catch (const std::exception &e)
    {
        std::cout << "exception in the wrong place: " << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "exception in the wrong place!" << std::endl;
    }
}

void
HTTPClientConnection::SendPreamble()
{
    Lock<Ptr<MutexRefCount *> > stateLock(m_stateMutex);
    if (m_writeState == WSBody) {
        return;
    }
    m_writeState = WSBody;
    int major = 1;
    int minor = 0;
    std::string method("GET");
    std::string uri("/");
    std::list<std::string> headers;
    Ptr<Buffer*> buf;
    t_PropList::iterator it = m_requestProps.begin();
    t_PropList::iterator end = m_requestProps.end();

    for (; it != end; ++it) {
        NetProperty *p = it->Get();
        HTTPVersionProperty *v;
        HTTPMethodProperty *m;
        HTTPURIProperty *u;
        HTTPHeaderProperty *h;
        if (( v = dynamic_cast<HTTPVersionProperty*>(p) )) {   
            major = v->major;
            minor = v->minor;
        } else if (( m = dynamic_cast<HTTPMethodProperty*>(p) )) {
            method = m->method;
        } else if (( u = dynamic_cast<HTTPURIProperty*>(p) )) {
            uri = u->uri;
        } else if (( h = dynamic_cast<HTTPHeaderProperty*>(p) )) {
            headers.push_back(h->name + std::string(": ") + h->value + "\r\n");
        } else {
            AT_Assert(!"HTTPClientConnection::SendPreamble -- unrecognized property");
        }
    }

    buf = NewBuffer();
    buf->reserve(method.size() + uri.size() + 100);
    int x = sprintf(buf->begin(), "%s %s HTTP/%d.%d\r\n", method.c_str(), uri.c_str(), major, minor);
    buf->SetAllocated(x);
    while (headers.size()) {
        buf->Append(headers.front());
        headers.pop_front();
    }
    buf->Append(std::string("\r\n"));

    this->Send(buf);
}

// HTTPNet ////////////////////////////////////////////////////////////////////
void
HTTPNet::Resolve(
    NetResolverLead *i_lead,
    PtrDelegate<const NetAddress*> i_address)
{
    // We don't do that here.
    AT_Assert(!"HTTPNet::Resolve -- not supported");
}

void
HTTPNet::ListenDatagram(
    NetDatagramLead *i_lead,
    PtrDelegate<const NetAddress*> i_address,
    PtrDelegate<const NetParameters*> i_parameters,
    PtrDelegate<Pool*> i_buffer_pool)
{
    // We don't do that here.
    AT_Assert(!"HTTPNet::ListenDatagram -- not supported");
}

void
HTTPNet::Listen(
    NetConnectLead *i_lead,
    PtrDelegate<const NetAddress*> i_address,
    PtrDelegate<const NetParameters*> i_parameters)
{
    Ptr<const NetAddress*> a = i_address;
    Ptr<const NetParameters*> p = i_parameters;
    const HTTPAddress *ha;
    const NetAddress_NamePort *npa;
    const NetAddress *ipa;

    if (( ha = dynamic_cast<const HTTPAddress*>(a.Get()) ))
    {
        UrlParser delamerde(ha->uri);
        a = new NetAddress_NamePort(
            delamerde.m_host, atoi(delamerde.m_port.c_str()));
    }
    if (( npa = dynamic_cast<const NetAddress_NamePort*>(a.Get()) ))
    {
        if (!m_ip) {
            m_ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
        }
        a = SyncResolve(m_ip, npa);
    }
    if (( ipa = dynamic_cast<const IPv4Address*>(a.Get()) ))
    {
        if (!m_ip) {
            m_ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
        }
        HTTPListenAide *aide = new HTTPListenAide(i_lead, new MutexRefCount(Mutex::Recursive), *this);
        m_lav.push_back(aide);
        m_ip->Listen(aide, a, p);
    }
    else
    {
        AT_Assert(!"HTTPNet::Listen -- invalid IP address");
    }
}

void
HTTPNet::Connect(
    NetConnectLead *i_lead,
    PtrDelegate<const NetAddress*> i_address,
    PtrDelegate<const NetParameters*> i_parameters)
{
    Ptr<const NetAddress*> a = i_address;
    Ptr<const NetParameters*> p = i_parameters;
    const HTTPAddress *ha;
    const NetAddress_NamePort *npa;
    const NetAddress *ipa;

    if (( ha = dynamic_cast<const HTTPAddress*>(a.Get()) ))
    {
        UrlParser delamerde(ha->uri);
        a = new NetAddress_NamePort(
            delamerde.m_host, atoi(delamerde.m_port.c_str()));
    }
    if (( npa = dynamic_cast<const NetAddress_NamePort*>(a.Get()) ))
    {
        if (!m_ip) {
            m_ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
        }
        a = SyncResolve(m_ip, npa);
    }
    if (( ipa = dynamic_cast<const IPv4Address*>(a.Get()) ))
    {
        if (!m_ip) {
            m_ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
        }
        HTTPConnectAide *aide = new HTTPConnectAide(i_lead, new MutexRefCount(Mutex::Recursive), *this);
        m_cav.push_back(aide);
        m_ip->Connect(aide, a, p);
    }
    else
    {
        AT_Assert(!"HTTPNet::Connect -- invalid IP address");
    }
}

PtrDelegate<at::ActivityListOwner*>
HTTPNet::GetActivityListOwner()
{
    // TODO: Implement this.
    AT_Assert(!"HTTPNet::GetActivityListOwner -- not supported");
    return NULL;
}


// HTTPStatusProperty /////////////////////////////////////////////////////////
HTTPStatusProperty::HTTPStatusProperty(int c) : statusCode(c)
{
    switch (statusCode)
    {
    case 100: reasonPhrase = "Continue"; break;
    case 101: reasonPhrase = "Switching Protocols"; break;
    case 200: reasonPhrase = "OK"; break;
    case 201: reasonPhrase = "Created"; break;
    case 202: reasonPhrase = "Accepted"; break;
    case 203: reasonPhrase = "Non-Authoritative Information"; break;
    case 204: reasonPhrase = "No Content"; break;
    case 205: reasonPhrase = "Reset Content"; break;
    case 206: reasonPhrase = "Partial Content"; break;
    case 300: reasonPhrase = "Multiple Choices"; break;
    case 301: reasonPhrase = "Moved Permanently"; break;
    case 302: reasonPhrase = "Moved Temporarily"; break;
    case 303: reasonPhrase = "See Other"; break;
    case 304: reasonPhrase = "Not Modified"; break;
    case 305: reasonPhrase = "Use Proxy"; break;
    case 400: reasonPhrase = "Bad Request"; break;
    case 401: reasonPhrase = "Unauthorized"; break;
    case 402: reasonPhrase = "Payment Required"; break;
    case 403: reasonPhrase = "Forbidden"; break;
    case 404: reasonPhrase = "Not Found"; break;
    case 405: reasonPhrase = "Method Not Allowed"; break;
    case 406: reasonPhrase = "Not Acceptable"; break;
    case 407: reasonPhrase = "Proxy Authentication Required"; break;
    case 408: reasonPhrase = "Request Timeout"; break;
    case 409: reasonPhrase = "Conflict"; break;
    case 410: reasonPhrase = "Gone"; break;
    case 411: reasonPhrase = "Length Required"; break;
    case 412: reasonPhrase = "Precondition Failed"; break;
    case 413: reasonPhrase = "Request Entity Too Large"; break;
    case 414: reasonPhrase = "Request-URI Too Long"; break;
    case 415: reasonPhrase = "Unsupported Media Type"; break;
    case 500: reasonPhrase = "Internal Server Error"; break;
    case 501: reasonPhrase = "Not Implemented"; break;
    case 502: reasonPhrase = "Bad Gateway"; break;
    case 503: reasonPhrase = "Service Unavailable"; break;
    case 504: reasonPhrase = "Gateway Timeout"; break;
    case 505: reasonPhrase = "HTTP Version Not Supported"; break;
    default:  reasonPhrase = "Fancy-pants Status Code"; break;
    }
}


// HTTPVersionProperty ////////////////////////////////////////////////////////
HTTPVersionProperty::HTTPVersionProperty(const std::string &s)
{
    int x = sscanf(s.c_str(), "%d.%d", &major, &minor);
    AT_Assert(x == 2);
}

AT_MakeFactory0P( "net_http", HTTPNet, Net, DKy);
AT_MakeCUnitLink(net_http);
