//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_status_report_basic.cpp
 *
 * This contains the implementation of methods in at_status_report_basic.h.
 *
 */

#include "at_status_report_basic.h"

// Austria namespace
namespace at
{

//
//
//
StatusReport::~StatusReport()
{
}

//
//
//
StatusReport_Basic::StatusReport_Basic()
  : m_status_type( NoStatus ),
    m_status_code( 0 )
{
}

//
//
//
StatusReport_Basic::StatusReport_Basic(
    StatusTypeCode                                i_typecode,
    int                                           i_status,
    const std::string                           & i_description
)
  : m_status_type( i_typecode ),
    m_status_code( i_status ),
    m_description( i_description )
{
}

//
//
//
void StatusReport_Basic::ReportStatus(
    StatusTypeCode                                i_typecode,
    int                                           i_status,
    const std::string                           & i_description
) {

    //
    // just store the info
    //
    m_status_type = i_typecode;
    m_status_code = i_status;
    m_description = i_description;
    
}

//
//
//
void StatusReport_Basic::ReportStatus(
    const StatusReport                      * i_sreport
) {
    i_sreport->GetStatus( this );
}



//
//
//
void StatusReport_Basic::ClearStatus()
{
    //
    // make it look like a newly constructued object
    //
    
    m_status_type = NoStatus;
    m_status_code = 0;
    m_description.clear();
}

//
//
//
bool StatusReport_Basic::GetStatus() const
{
    // report if the status type has changed
    //
    return m_status_type != NoStatus;
}

//
//
//
bool StatusReport_Basic::GetStatus(
    StatusTypeCode                              & o_typecode,
    int                                         & o_status,
    std::string                                 & o_description
) const
{

    //
    // if the status code has not changed - do nothing and
    // otherwise set all the inputs to their appropriate values.
    //
    if ( m_status_type != NoStatus )
    {
        o_typecode = m_status_type;
        o_status = m_status_code;
        o_description = m_description;

        return true;
    }

    return false;
}
    
//
//
//
bool StatusReport_Basic::GetStatus(
    StatusReport                                * o_srep
) const
{

    //
    // ReportStatus reports only when the status is not NoStatus
    //
    
    if ( m_status_type != NoStatus )
    {
        o_srep->ReportStatus( m_status_type, m_status_code, m_description );
        
        return true;
    }

    return false;

}
    
}; // namespace
