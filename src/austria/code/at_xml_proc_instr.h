// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_proc_instr.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_proc_instr_h_x
#define x_at_xml_proc_instr_h_x 1

#include "at_xml_node.h"

// Austria namespace
namespace at
{
    /**
     *  The XmlProcessingInstruction class implements a "processing instruction", used in XML as a way
     *  to keep processor-specific information in the text of the document.
     *
     *  @note No lexical check is done on the content of a processing instruction, and it is therefore
     *  possible to have the character sequence "?>" in the content, which is illegal in a processing
     *  instruction per section 2.6 of XML 1.0. The presence of this character sequence must generate
     *  a fatal error during serialization.
     *
     *  @note The target names "XML", "xml", and so on are reserved for standardization in 1.0 or
     *  future versions of this specification.
     */
    class XmlProcessingInstruction :
        public XmlNode
    {
        friend class XmlDocument;

    public:

        /**
         *  The content of this processing instruction.
         *  This is from the first non white space character after the target to the character immediately
         *  preceding the ?>.
         */
        const std::string& Data() const { return m_data; }

        /**
         *  The target of this processing instruction.
         *  XML defines this as being the first token following the markup that begins the processing
         *  instruction.
         */
        const std::string& Target() const { return m_target; }

        /**
         *  @brief  The name of this processing instruction, which is also its target.
         *  @return this->Target()
         */
        virtual std::string Name() const;

        /**
         *  @brief  The value of this processing instruction, which is equal to its content.
         *  @return this->Data().
         */
        virtual std::string Value() const;

        /**
         *  @brief  The type of this node.
         *  @return XmlNode::PROCESSING_INSTRUCTION_NODE.
         */
        virtual unsigned short Type() const;

        /**
         *  @brief  Duplicates this processing instruction.
         *  @see XmlNode::Clone for a description of the boolean parameter i_deep.
         */
        virtual PtrDelegate< XmlNode* > Clone( bool i_deep ) const;

        /**
         *  @brief Accepts a visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op );

        /**
         *  @brief Accepts a non-modifying, visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op ) const;

    protected:

        /**
         *  @brief  Creates a processing instruction.
         *  @param  i_ownerDocument The XmlDocument that creates the processing instruction.
         *  @param  i_target        The target name of the processing instruction.
         *  @param  i_data          The content data of theprocessing instruction.
         */
        XmlProcessingInstruction( PtrView< XmlDocument* > i_ownerDocument,
                                  const std::string&      i_target,
                                  const std::string&      i_data ) :
            XmlNode( i_ownerDocument ),
            m_target( i_target ),
            m_data( i_data )
            {}

    private:

        /**
         *  The copy constructor is declared as private and is not defined anywhere in the code. This is to
         *  prevent the compiler from defining a bitwise default copy constructor and using it.
         *  To make a copy of an XmlProcessingInstruction, the Clone method must be used.
         */
        XmlProcessingInstruction( const XmlProcessingInstruction& );

        /**
         *  The content of this processing instruction.
         *  This is from the first non white space character after the target to the character immediately
         *  preceding the ?>.
         */
        std::string m_data;

        /**
         *  The target of this processing instruction.
         *  XML defines this as being the first token following the markup that begins the processing
         *  instruction.
         */
        std::string m_target;
    };

}; // namespace

#endif // x_at_xml_proc_instr_h_x
