//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * \file at_first_unique.h
 *
 * \author Gianni Mariani
 *
 */

#ifndef x_at_first_unique_h_x
#define x_at_first_unique_h_x

#include <vector>
#include <list>
#include <algorithm>

#include "at_assert.h"

// Austria namespace
namespace at
{


// ======== FirstUniqueDupeEliminator =================================
/**
 * Helper for FirstUnique().
 *
 */

template <
    typename    w_iterator
>   
class FirstUniqueDupeEliminator
{
    public:

    typedef 
        std::pair<
            bool,
            w_iterator
        >                                           Element;
            
    std::vector< Element >                          m_values;
    std::vector< Element * >                        m_values_ptrs;

    w_iterator                                      m_result;
    

    void Remove( Element * i_listiter )
    {
        i_listiter->first = false;
    }

    bool operator()( Element * i_rhs, Element * i_lhs )
    {
        // if we're pointing to the same element
        // we don't do anything special
        if ( i_lhs == i_rhs )
        {
            return false;
        }
        
        if ( *(i_lhs->second) < *(i_rhs->second) )
        {
            return true;
        }
        
        if ( *(i_rhs->second) < *(i_lhs->second) )
        {
            return false;
        }

        // oops they're the same eliminate the elements from the list
        //
        Remove( i_rhs );
        Remove( i_lhs );

        return false;
    }

    FirstUniqueDupeEliminator(
        const w_iterator        & i_begin,
        const w_iterator        & i_end
    )
    {
        if ( i_begin == i_end )
        {
            m_result = i_end;
            return;
        }
        
        w_iterator                  l_tmp = i_begin;

        while ( l_tmp != i_end )
        {
            m_values.push_back( Element( true, l_tmp ) );

            ++ l_tmp;
        }

        m_values_ptrs.resize( m_values.size() );

        for ( std::size_t i = 0; i < m_values.size(); ++i )
        {
            m_values_ptrs[ i ] = & m_values[ i ];
        }

        std::sort( m_values_ptrs.begin(), m_values_ptrs.end(), *this );

        for ( std::size_t i = 0; i < m_values.size(); ++i )
        {
            if ( m_values[ i ].first )
            {
                m_result = m_values[ i ].second;
                return;
            }
        }

        m_result = i_end;
    }

    operator w_iterator() const
    {
        return m_result;
    }

};



// ======== FirstUnique ===============================================
/**
 * FirstUnique returns the first unique element in a container
 *
 * @param i_begin The first element in the continer
 * @param i_end The last element in the container
 * 
 * @return The iterator to the first element in the
 *          range that is unique or i_end if no elements are unique
 */

template <
    typename    w_iterator
>   
w_iterator FirstUnique(
    const w_iterator        & i_begin,
    const w_iterator        & i_end
) {

    return FirstUniqueDupeEliminator<w_iterator>( i_begin, i_end );    
}



}; // namespace

#endif // x_at_first_unique_h_x

