/*
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 *
 *  The code herein attaches at_aio.h to at_net.h.  The result is a
 *  fancy-pants interface for asynchronous IP networking.
 */

#include "at_net_ipv4.h"
#include "at_aio.h"
#include "at_twinmt_basic.h"
#include "at_factory.h"
#include <memory>
#include <cerrno>
#include <cassert>

#ifndef WIN32
    #include <netdb.h>
    #include <netinet/in.h>
#endif

using namespace at;

/*
  Lead/Aide relationships

  NetResolverLead -> Trivial
  NetConnectionResponder -> IPConnection
  NetConnectLead -> IPListenAide/IPConnectAide
  NetDatagramChannelResponder -> IPDatagramChannel
  NetDatagramLead -> Trivial
*/

namespace
{
    class IPDatagramChannel :
        public AideTwinMT_Basic<NetDatagramChannelResponderIf>,
        public NetDatagramChannel,
        public LeadTwinMT_Basic<aio_monitor_if>
    {
    public:
        IPDatagramChannel(aio_hub *hub, aio_fd fd, PtrDelegate<Pool*>);
        virtual ~IPDatagramChannel();
        PtrView<MutexRefCount *> StateMutex() { return m_stateMutex; }

    public: // NetDatagramChannel
        virtual bool Receive(std::list<NetDatagramBuffer> &o_data);
        virtual void Send(
            PtrDelegate<Buffer*> i_buffer,
            PtrDelegate<NetAddress*> i_address);
        virtual void DatagramChannelNotify(
            NetDatagramChannelResponder *i_lead);

    public: // aio_monitor
        virtual void state_changed(
            aio_fd fd, aio_hub *source, int state, int flags);
        virtual void send_completed(
            aio_fd fd, aio_hub *source, Ptr<Buffer *> i_buffer, int i_sent_size,
            aio_ip4_addr dest, int err);

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<aio_monitor_if>::LeadAssociate(i_aide, i_sharedMutex);
        }

        aio_hub *m_hub; // unowned reference -- do not delete
        aio_fd m_fd;
        Ptr<Pool*>              m_buffer_pool;
        Ptr<MutexRefCount *>    m_stateMutex;

    private:
        /** Not implemented. */
        IPDatagramChannel(const IPDatagramChannel&);
        /** Not implemented. */
        IPDatagramChannel& operator=(const IPDatagramChannel&);
    };

    class IPConnection :
        public AideTwinMT_Basic<NetConnectionResponderIf>,
        public NetConnection,
        public LeadTwinMT_Basic<aio_monitor_if>
    {
    public:
        IPConnection();
        virtual ~IPConnection();
        PtrView<MutexRefCount *> StateMutex() { return m_stateMutex; }

    public: // NetConnection
        virtual bool Receive(PtrView<Buffer*> o_data);
        virtual void Send(PtrDelegate<Buffer*> i_buffer);
        virtual bool ReceiveOutOfBand(PtrView<Buffer*> o_data);
        virtual bool SendOutOfBand(PtrDelegate<Buffer*> i_buffer);
        virtual void ConnectionNotify(NetConnectionResponder & i_lead);
        virtual t_PropList GetReceivedProperties();
        virtual void AddOutgoingProperties(t_PropList &i_proplist);
        virtual PtrDelegate<NetAddress*> PeerAddress();
        virtual PtrDelegate<NetAddress*> LocalAddress();

    public: // aio_monitor
        virtual void become_monitor(aio_fd fd, aio_hub *source);
        virtual void state_changed(
            aio_fd fd, aio_hub *source, int state, int flags);
        virtual void send_completed(
            aio_fd fd, aio_hub *source, Ptr<Buffer *> i_buffer, int i_sent_size,
            aio_ip4_addr dest, int err);

    public: // AideTwinMT_Basic
        void AppAideCloseNotify(TwinTraits::TwinCode);

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<aio_monitor_if>::LeadAssociate(i_aide, i_sharedMutex);
        }

        aio_hub *m_hub; // unowned reference -- do not delete
        aio_fd m_fd;
        Ptr<MutexRefCount *>    m_stateMutex;

    private:
        /** Not implemented. */
        IPConnection(const IPConnection&);
        /** Not implemented. */
        IPConnection& operator=(const IPConnection&);
    };

    class IPListenAide :
        public AideTwinMT_Basic<NetConnectLeadIf>,
        public LeadTwinMT_Basic<aio_monitor_if>
    {
    public:
        IPListenAide(NetConnectLead*);
        virtual ~IPListenAide();
        PtrView<MutexRefCount *> StateMutex() { return m_stateMutex; }

    public: // aio_monitor
        virtual void become_monitor(aio_fd fd, aio_hub *source);
        virtual void state_changed(
            aio_fd fd, aio_hub *source, int state, int flags);

    public: // AideTwinMT_Basic
        void AppAideCloseNotify(TwinTraits::TwinCode);

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<aio_monitor_if>::LeadAssociate(i_aide, i_sharedMutex);
        }

    protected:
        aio_hub *m_hub; // unowned reference -- do not delete
        aio_fd m_fd;
        Ptr<MutexRefCount *>    m_stateMutex;

    private:
        /** Not implemented. */
        IPListenAide(const IPListenAide&);
        /** Not implemented. */
        IPListenAide& operator=(const IPListenAide&);
    };

    class IPConnectAide :
        public AideTwinMT_Basic<NetConnectLeadIf>,
        public LeadTwinMT_Basic<aio_monitor_if>
    {
    public:
        IPConnectAide(NetConnectLead*);
        virtual ~IPConnectAide();
        PtrView<MutexRefCount *> StateMutex() { return m_stateMutex; }

    public: // aio_monitor
        virtual void become_monitor(aio_fd fd, aio_hub *source);
        virtual void state_changed(
            aio_fd fd, aio_hub *source, int state, int flags);

    public: // AideTwinMT_Basic
        void AppAideCloseNotify(TwinTraits::TwinCode);

    protected:
        virtual bool LeadAssociate(AideTwinMT< TwinMTNullAide > * i_aide, PtrView< MutexRefCount * >  i_sharedMutex)
        {
            Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
            m_stateMutex = i_sharedMutex;   // catch a copy of the mutex, which is the entire purpose of this override
            return LeadTwinMT_Basic<aio_monitor_if>::LeadAssociate(i_aide, i_sharedMutex);
        }

        aio_hub *m_hub; // unowned reference -- do not delete
        aio_fd m_fd;
        Ptr<MutexRefCount *>    m_stateMutex;

    private:
        /** Not implemented. */
        IPConnectAide(const IPConnectAide&);
        /** Not implemented. */
        IPConnectAide& operator=(const IPConnectAide&);
    };

    template<typename LEADIF>
    class TrivialAide : public AideTwinMT_Basic<LEADIF>, public PtrTarget_MT
    {
    public:
        explicit TrivialAide(LeadTwinMT<LEADIF> *l) :
            m_stateMutex(new MutexRefCount(Mutex::Recursive))
        {
            this->AideAssociate(l, m_stateMutex);
        }

        using AideTwinMT_Basic<LEADIF>::AideCancel;

        virtual ~TrivialAide()
        {
            AideCancel();
        }
    private:
        Ptr<MutexRefCount *>    m_stateMutex;
    };

    class IP : public Net
    {
    public:
        IP();

        virtual ~IP() {
            // 1st pass at cleanup of lifetime messes.  ideally the vectors would contain SMART pointers.
            while (!m_lav.empty()) {
                IPListenAide *la = m_lav[0];
                m_lav.erase(m_lav.begin());
                delete la;
            }
            while (!m_cav.empty()) {
                IPConnectAide *ca = m_cav[0];
                m_cav.erase(m_cav.begin());
                delete ca;
            }
            m_hub = NULL;
        }

    public: // Net
        virtual void Resolve(
            NetResolverLead *i_lead,
            PtrDelegate<const NetAddress*> i_address);

        virtual void ListenDatagram(
            NetDatagramLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters,
            PtrDelegate<Pool*> i_buffer_pool = 0);

        virtual void Listen(
            NetConnectLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters);

        virtual void Connect(
            NetConnectLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters);

        virtual PtrDelegate< at::ActivityListOwner * > GetActivityListOwner();

    private:
        aio_hub *m_hub; // unowned reference -- do not delete
        std::vector<IPListenAide *>   m_lav;
        std::vector<IPConnectAide *>  m_cav;

    private:
        /** Not implemented. */
        IP(const IP&);
        /** Not implemented. */
        IP& operator=(const IP&);
    };
} // namespace

bool
IPv4Address::operator==(const NetAddress &rhs) const
{
    const IPv4Address *p = dynamic_cast<const IPv4Address*>(&rhs);
    return p != 0 && p->ip() == this->ip() && p->port() == this->port();
}

IP::IP() :
    m_hub(aio_hub::new_hub()),
    m_lav(),
    m_cav()

{
}

void
IP::Resolve(
    NetResolverLead *i_lead,
    PtrDelegate<const NetAddress*> i_address)
{
#ifndef WIN32
    // This is to make sure that the lead receives an aide.  The aide will
    // disassociate itself when it is destroyed at the end of this call.
    Ptr<TrivialAide<NetResolverLeadIf>*> aide;
    aide = new TrivialAide<NetResolverLeadIf>(i_lead);

    const NetAddress_NamePort *np = dynamic_cast<const NetAddress_NamePort*>(i_address.Get());
    if (!np)
    {
        // The address is not a NetAddress_NamePort.
        // REV: I don't feel that this warrants crashing, but Gianni does. -m
        AT_Abort();
    }

    int tries = 1;
    bool keepGoing = false;
    const int maxTries = 4;
    // We'll try up to four times to resolve a name.
    do
    {
        const char *name = np->Name().c_str();
        struct hostent entry;
        struct hostent *result;
        char aux[256];
        int auxSize = sizeof(aux);
        int herrno;
        int ret;
        ret = gethostbyname2_r( // This function is non-POSIX.
            name,       // the name we want to resolve
            AF_INET,    // the address family from which we care about results
            &entry,     // where to store the results
            aux,        // another result buffer...presumably for addresses
            auxSize,    // the size of the aux buffer
            &result,    // points to entry on success, 0 on failure
            &herrno);   // the gethostbyname error number, if any
        if (ret != 0 && herrno == ERANGE)
        {
            // We didn't allocate enough space to store the host entry.
            AT_Assert(false);
            // In production, we'll just call this a fatal error.
            i_lead->Failed(NetResolverError::s_fail);
        }
        else if (ret == 0)
        {
            std::list<Ptr<NetAddress*> > l;
            for (int i = 0; entry.h_addr_list[i] != 0; ++i)
            {
                IPv4Address::t_IP ip;
                memcpy((void*)&ip, (void*)entry.h_addr_list[i],
                       sizeof(IPv4Address::t_IP));
                l.push_back(new IPv4Address(ntohl(ip), np->Port()));
            }
            i_lead->NetResolverResults(l);
        }
        else if (herrno == TRY_AGAIN && tries < maxTries)
        {
            keepGoing = true;
        }
        else
        {
            switch (herrno)
            {
            case HOST_NOT_FOUND:
                i_lead->Failed(NetResolverError::s_noname);
                break;
            // NO_ADDRESS and NO_DATA are defined to be the same in
            // netdb.h.  If we list them both here, the compiler complains.
            //case NO_ADDRESS:
            case NO_DATA:
                i_lead->Failed(NetResolverError::s_noname);
                break;
            case NO_RECOVERY:
                i_lead->Failed(NetResolverError::s_fail);
                break;
            case TRY_AGAIN:
                AT_Assert(tries == maxTries);
                i_lead->Failed(NetResolverError::s_gaveup);
                break;
            }
        }
        ++tries;
    }
    while (keepGoing);
#else // WIN32
    AT_Abort();
#endif
}

void
IP::ListenDatagram(
    NetDatagramLead *i_lead,
    PtrDelegate<const NetAddress*> i_address,
    PtrDelegate<const NetParameters*> i_parameters,
    PtrDelegate<Pool*> i_pool)
{
    Ptr<TrivialAide<NetDatagramLeadIf>*> aide;
    PtrView<const IPv4Address*> ipaddr;

    aide = new TrivialAide<NetDatagramLeadIf>(i_lead);

    ipaddr = dynamic_cast<const IPv4Address*>(i_address.Get());
    if (!ipaddr)
    {
        AT_Assert(false);
        i_lead->Failed(NetError::s_unexpected_failure);
        return;
    }

    bool b;
    int err;
    aio_ip4_addr addr;
    aio_fd fd;

    addr.first = ipaddr->ip();
    addr.second = ipaddr->port();
    b = m_hub->listen_datagram(addr, 0, 0, &fd, &err);
    if (!b)
    {
        if (err == aio_error::bind_failure)
            i_lead->Failed(NetError::s_bind_failure);
        else
            i_lead->Failed(NetError::s_unexpected_failure);
        return;
    }
    try
    {
        Ptr<IPDatagramChannel*> channel;
        channel = new IPDatagramChannel(m_hub, fd, i_pool);
        b = m_hub->set_monitor(fd, channel.Get(), channel->StateMutex(), &err);
        AT_Assert(b);
        i_lead->ListenerReady(channel);
    }
    catch (...)
    {
        b = m_hub->close(fd, &err);
        if (!b)
        {
            // oh well...
            // TODO: Log something.
        }
    }
}

void
IP::Listen(
    NetConnectLead *i_lead,
    PtrDelegate<const NetAddress*> i_address,
    PtrDelegate<const NetParameters*> i_parameters)
{
    PtrView<const IPv4Address*> ipaddr;
    aio_ip4_addr addr;

    IPListenAide *aide = new IPListenAide(i_lead);

    ipaddr = dynamic_cast<const IPv4Address*>(i_address.Get());
    if (!ipaddr)
    {
        AT_Assert(false);
        i_lead->Failed(NetError::s_unexpected_failure);
        delete aide;
        return;
    }
    addr.first = ipaddr->ip();
    addr.second = ipaddr->port();

    bool b;
    int err;

    m_lav.push_back(aide);
    b = m_hub->listen(addr, aide, aide->StateMutex(), 0, &err);
    if (!b)
    {
        if (err == aio_error::bind_failure)
            i_lead->Failed(NetError::s_bind_failure);
        else
            i_lead->Failed(NetError::s_unexpected_failure);
    }
}

void
IP::Connect(
    NetConnectLead *i_lead,
    PtrDelegate<const NetAddress*> i_address,
    PtrDelegate<const NetParameters*> i_parameters)
{
    PtrView<const IPv4Address*> ipaddr;
    aio_ip4_addr addr;

    IPConnectAide *aide = new IPConnectAide(i_lead);

    ipaddr = dynamic_cast<const IPv4Address*>(i_address.Get());
    if (!ipaddr)
    {
        AT_Assert(false);
        i_lead->Failed(NetError::s_unexpected_failure);
        delete aide;
        return;
    }
    addr.first = ipaddr->ip();
    addr.second = ipaddr->port();

    bool b;
    int err;
    m_cav.push_back(aide);
    b = m_hub->connect(addr, aide, aide->StateMutex(), 0, &err);
    if (!b)
    {
        // REV: What other errors?
        i_lead->Failed(NetError::s_unexpected_failure);
    }
}

PtrDelegate<at::ActivityListOwner*>
IP::GetActivityListOwner()
{
    // BUG/FIX/TODO:
    AT_Abort();
    return 0;
}

IPDatagramChannel::IPDatagramChannel(
    aio_hub *hub, aio_fd fd, PtrDelegate<Pool*> buffer_pool) :
    m_hub(hub),
    m_fd(fd),
    m_buffer_pool(buffer_pool
                  ? buffer_pool
                  : FactoryRegister<at::Pool, DKy>::Get().Create("Basic")()),
    m_stateMutex(new MutexRefCount(Mutex::Recursive))
{
}

IPDatagramChannel::~IPDatagramChannel()
{
    {
        Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
        if (m_fd != aio_fd::invalid)
        {
            bool b;
            int err;
            b = m_hub->set_monitor(m_fd, 0, 0, &err);
            b = m_hub->close(m_fd, &err);
            m_fd = aio_fd::invalid;
        }
    }
    AideCancel();
}

bool
IPDatagramChannel::Receive(std::list<NetDatagramBuffer> &o_data)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    if (m_fd == aio_fd::invalid)
    {
        // REV: Should we make this call even when packet_count > 0?
        CallLead().VoidCall ( &NetDatagramChannelResponderIf::ReceiveFailure, NetDatagramError::s_unexpected_failure);
        return false;
    }

    bool b;
    int err;
    int packet_count = 0;
    aio_ip4_addr remote_address;

    do
    {
        int size = 64 * 1024;
        Ptr<Buffer*> buffer = m_buffer_pool->Create();
        buffer->SetAvailability(size);
        b = m_hub->recv(
            m_fd, buffer->Region().m_mem, &size, &remote_address, &err);
        if (b == false && err == aio_error::loser)
        {
            break;
        }
        else if (b == false)
        {
            // REV: Should we make this call even when packet_count > 0?
            CallLead().VoidCall ( &NetDatagramChannelResponderIf::ReceiveFailure, NetDatagramError::s_unexpected_failure);
        }
        else
        {
            buffer->SetAllocated(size);
            buffer->SetAvailability(size);
            Ptr<IPv4Address*> addr = new IPv4Address(remote_address.first,
                                                     remote_address.second);
            o_data.push_back(NetDatagramBuffer(buffer, addr));
            ++packet_count;
        }
    } while (b);
    return packet_count;
}

void
IPDatagramChannel::Send(
    PtrDelegate<Buffer*> i_buffer,
    PtrDelegate<NetAddress*> i_address)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    if (m_fd == aio_fd::invalid)
    {
        // REV: Should we make this call even when packet_count > 0?
        CallLead().VoidCall ( &NetDatagramChannelResponderIf::ReceiveFailure, NetDatagramError::s_unexpected_failure);
        return;
    }

    bool b;
    int err;
    aio_ip4_addr dest;
    const IPv4Address *ipaddr;

    ipaddr = dynamic_cast<const IPv4Address*>(i_address.Get());
    if (!ipaddr)
    {
        // TODO: Log something.
        // Odds are this is a programming error.
        AT_Assert(false);
        // But in production, let's not crash.
        // REV: Should this be a different error?
        CallLead().VoidCall ( &NetDatagramChannelResponderIf::ReceiveFailure, NetDatagramError::s_unexpected_failure);
        return;
    }

    dest.first = ipaddr->ip();
    dest.second = ipaddr->port();
    b = m_hub->send(m_fd, i_buffer, dest, &err);
    if (!b)
    {
        // TODO: Log something.
        // REV: Should this be a different error?
        CallLead().VoidCall ( &NetDatagramChannelResponderIf::ReceiveFailure, NetDatagramError::s_unexpected_failure);
        return;
    }
}

void
IPDatagramChannel::DatagramChannelNotify(
    NetDatagramChannelResponder *i_lead)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    this->AideAssociate(i_lead, m_stateMutex);
}

void
IPDatagramChannel::state_changed(
    aio_fd fd, aio_hub *source, int state, int flags)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    if (flags & aio_flag::data_available)
    {
        CallLead().VoidCall ( &NetDatagramChannelResponderIf::DataReady);
    }
    if (state == aio_state::closed)
    {
        m_fd = aio_fd::invalid;
        //this->AideClose(TwinTraits::AideDelete);
    }
}

void
IPDatagramChannel::send_completed(
    aio_fd fd, aio_hub *source, Ptr<Buffer *> i_buffer, int i_sent_size,
    aio_ip4_addr dest, int err)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    Buffer::t_Region r = i_buffer->Region();
    int unsent_size = r.m_allocated - i_sent_size;
    if (unsent_size) {
        CallLead().VoidCall ( &NetDatagramChannelResponderIf::SendFailure, 
                                i_buffer,
                                (err == aio_error::invalid_address
                                    ? NetDatagramError::s_address_unavailable
                                    : NetDatagramError::s_unexpected_failure));
    } else {
        CallLead().VoidCall ( &NetDatagramChannelResponderIf::SendCompleted, i_buffer);
    }
}

IPConnection::IPConnection() :
    m_hub(0),
    m_fd(aio_fd::invalid),
    m_stateMutex(new MutexRefCount(Mutex::Recursive))
{
}

IPConnection::~IPConnection()
{
    {
        Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
        if (m_fd != aio_fd::invalid)
        {
            bool b;
            int err;
            b = m_hub->set_monitor(m_fd, 0, 0, &err);
            b = m_hub->close(m_fd, &err);
            m_fd = aio_fd::invalid;
        }
    }
    AideCancel();
}

bool
IPConnection::Receive(PtrView<Buffer*> o_data)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    if (m_fd == aio_fd::invalid)
    {
        // REV: Should we make this call even when packet_count > 0?
        CallLead().VoidCall ( &NetConnectionResponder::ReceiveFailure, NetConnectionError::s_peer_reset);
        return false;
    }

    bool b;
    int err;
    bool ret;

    int size = o_data->capacity() - o_data->size();
    char *base = o_data->end();
    b = m_hub->read(m_fd, base, &size, &err);
    if (b == false && err == aio_error::loser)
    {
        ret = false;
    }
    else if (b == false)
    {
        // REV: Should we make this call even when packet_count > 0?
        CallLead().VoidCall ( &NetConnectionResponder::ReceiveFailure,NetConnectionError::s_unexpected_failure);
        ret = false;
    }
    else
    {
        o_data->SetAllocated(o_data->size() + size);
        if (size) {
            ret = true;
        } else {
            ret = false;
        }
    }
    return ret;
}

void
IPConnection::Send(PtrDelegate<Buffer*> i_buffer)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    Ptr<Buffer *> l_buffer = i_buffer;
    if (m_fd == aio_fd::invalid)
    {
        const char *l_bp = l_buffer->begin();
        // REV: Should we make this call even when packet_count > 0?
        CallLead().VoidCall ( &NetConnectionResponder::SendFailure,l_buffer,l_bp,NetConnectionError::s_peer_reset);
        return;
    }

    bool b;
    int err;
    b = m_hub->write(m_fd, l_buffer, &err);
    if (!b)
    {
        const char *l_bp = l_buffer->begin();
        // TODO: Log something.
        // REV: Should this be a different error?
        CallLead().VoidCall ( &NetConnectionResponder::SendFailure,l_buffer,l_bp,NetConnectionError::s_unexpected_failure);
        return;
    }
}

bool
IPConnection::ReceiveOutOfBand(PtrView<Buffer*> o_data)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    // TODO: Implement this.
    AT_Abort();
    return false;
}

bool
IPConnection::SendOutOfBand(PtrDelegate<Buffer*> i_buffer)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    // TODO: Implement this.
    AT_Abort();
    return false;
}

void
IPConnection::ConnectionNotify(NetConnectionResponder & i_lead)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    this->AideAssociate(&i_lead, m_stateMutex);
}

IPConnection::t_PropList
IPConnection::GetReceivedProperties()
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    return t_PropList();
}

void
IPConnection::AddOutgoingProperties(t_PropList &i_proplist)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    // REV: Should we do something nasty like abort, since we do not
    // actually accept properties?
}

PtrDelegate<NetAddress*>
IPConnection::PeerAddress()
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    aio_ip4_addr a = aio_ip4_addr();
    bool b;
    int err;

    b = m_hub->get_remote_address(m_fd, &a, &err);
    AT_Assert(b);
    return new IPv4Address(a.first, a.second);
}

PtrDelegate<NetAddress*>
IPConnection::LocalAddress()
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    aio_ip4_addr a = aio_ip4_addr();
    bool b;
    int err;

    b = m_hub->get_local_address(m_fd, &a, &err);
    AT_Assert(b);
    return new IPv4Address(a.first, a.second);
}

void
IPConnection::become_monitor(aio_fd fd, aio_hub *source)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub == 0); // We want this to be called exactly once
    m_fd = fd;
    m_hub = source;
}

void
IPConnection::state_changed(
    aio_fd fd, aio_hub *source, int state, int flags)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    if (flags & aio_flag::data_available)
    {
        CallLead().VoidCall ( &NetConnectionResponder::DataReady);
    }
    if (state == aio_state::closed)
    {
        m_fd = aio_fd::invalid;
        m_hub->set_monitor(fd, 0, 0, 0);
        this->AideCancel();
    }
}

void
IPConnection::send_completed(
    aio_fd fd, aio_hub *source, Ptr<Buffer *> i_buffer, int i_sent_size,
    aio_ip4_addr dest, int err)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub);
    Buffer::t_Region r = i_buffer->Region();
    int unsent_size = r.m_allocated - i_sent_size;
    const char *l_pUnsent = &r.m_mem[i_sent_size];
    if (i_sent_size) {
        CallLead().VoidCall ( &NetConnectionResponder::SendCompleted,i_buffer,l_pUnsent);
    }
    if (unsent_size) {
        CallLead().VoidCall ( &NetConnectionResponder::SendFailure,
                i_buffer,
                l_pUnsent,
                (err == aio_error::peer_reset
                 ? NetConnectionError::s_peer_reset
                 : NetConnectionError::s_unexpected_failure));
    }
}

IPListenAide::IPListenAide(NetConnectLead *i_lead) :
    m_hub(0),
    m_fd(aio_fd::invalid),
    m_stateMutex(new MutexRefCount(Mutex::Recursive))
{
    this->AideAssociate(i_lead, m_stateMutex);
}

IPListenAide::~IPListenAide()
{
    {
        Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
        if (m_fd != aio_fd::invalid)
        {
            bool b;
            int err;
            b = m_hub->set_monitor(m_fd, 0, 0, &err);
            b = m_hub->close(m_fd, &err);
            m_fd = aio_fd::invalid;
        }
    }
    AideCancel();
}

void
IPListenAide::become_monitor(aio_fd fd, aio_hub *hub)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub == 0); // We want this to be called exactly once
    m_fd = fd;
    m_hub = hub;
}

void
IPListenAide::state_changed(
    aio_fd fd, aio_hub *source, int state, int flags)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    if ((state ==  aio_state::listening) && (aio_flag::connection_waiting)) {
        int err;
        bool b;

        AT_Assert (m_hub == source);
        AT_Assert (m_fd == fd);
        Ptr<IPConnection*> conn = new IPConnection();
        b = m_hub->accept(m_fd, conn.Get(), conn->StateMutex(), 0, 0, &err);
        if (!b && err == aio_error::loser)
        {
            return; // false alarm.
        }
        if (!b) {
            m_hub->set_monitor(fd, 0, 0, 0);
            source->close(fd,0);

            CallLead().VoidCall ( &NetConnectLeadIf::Failed,NetConnectionError::s_unexpected_failure);

            this->AideCancel();
        } else {
            CallLead().VoidCall ( &NetConnectLeadIf::Established,conn);
        }
    } else if (state == aio_state::closed) {
        CallLead().VoidCall ( &NetConnectLeadIf::Failed,NetConnectionError::s_unexpected_failure);
        this->AideCancel();
    }
}

void
IPListenAide::AppAideCloseNotify(TwinTraits::TwinCode i_completion_code)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    if (m_hub && (m_fd != aio_fd::invalid)) {
        m_hub->close(m_fd,0);   // this will eventually result in state changed notification on separate thread.
    }
} 

IPConnectAide::IPConnectAide(NetConnectLead *i_lead) :
    m_hub(0),
    m_fd(aio_fd::invalid),
    m_stateMutex(new MutexRefCount(Mutex::Recursive))
{
    this->AideAssociate(i_lead, m_stateMutex);
}

IPConnectAide::~IPConnectAide()
{
    {
        Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
        if (m_fd != aio_fd::invalid)
        {
            bool b;
            int err;
            b = m_hub->set_monitor(m_fd, 0, 0, &err);
            b = m_hub->close(m_fd, &err);
            m_fd = aio_fd::invalid;
        }
    }
    AideCancel();
}

void
IPConnectAide::become_monitor(aio_fd fd, aio_hub *hub)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    AT_Assert(m_hub == 0); // We want this to be called exactly once
    m_fd = fd;
    m_hub = hub;
}

void
IPConnectAide::state_changed(
    aio_fd fd, aio_hub *source, int state, int flags)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    int err;
    bool b;

    AT_Assert(m_fd == fd);
    AT_Assert(m_hub == source);
    m_fd = aio_fd::invalid;
    if (state == aio_state::connected)
    {
        IPConnection* conn = new IPConnection();
        b = m_hub->set_monitor(fd, conn, conn->StateMutex(), &err);
        AT_Assert(b);
        CallLead().VoidCall ( &NetConnectLeadIf::Established,conn);
    }
    else
    {
        m_hub->set_monitor(fd, 0, 0, 0);
        source->close(fd,0);
        AT_Assert(state == aio_state::closed);
        // TODO/BUG/FIX: We're not getting appropriate error
        // information from aio_hub here.
        CallLead().VoidCall ( &NetConnectLeadIf::Failed,NetError::s_connection_refused);
    }
    this->AideCancel();
}

void
IPConnectAide::AppAideCloseNotify(TwinTraits::TwinCode i_completion_code)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
} 

void
IPConnection::AppAideCloseNotify(TwinTraits::TwinCode i_completion_code)
{
    Lock<Ptr<MutexRefCount *> > l_lock(m_stateMutex);
    if (m_hub && (m_fd != aio_fd::invalid)) {
        m_hub->set_monitor(m_fd, 0, 0, 0);
        m_hub->close(m_fd,0);
    }
} 

AT_MakeFactory0P( "net_ipv4", IP, Net, DKy);
