/**
 * \file at_log_mt.h
 *
 * \author Bradley Austin
 *
 * at_log_mt.h contains extensions to the Austria logging module to
 * support thread-safe logging.
 *
 */

#ifndef x_at_log_mt_h_x
#define x_at_log_mt_h_x 1


#include "at_thread.h"


// Austria namespace
namespace at
{


    class LogManagerLockPolicy_ATMutexLock
    {

        Mutex m_mutex;

    public:

        class t_lock_type
        {

            Lock< Mutex > m_lock;

        public:

            inline t_lock_type( LogManagerLockPolicy_ATMutexLock & io_mgr )
              : m_lock( io_mgr.m_mutex )
            {
            }

            inline ~t_lock_type() {}

        private:

            /* Unimplemented */
            t_lock_type( const t_lock_type & );
            t_lock_type & operator=( const t_lock_type & );

        };

        friend class t_lock_type;

    protected:

        inline LogManagerLockPolicy_ATMutexLock() {}
        inline ~LogManagerLockPolicy_ATMutexLock() {}

    private:

        /* Unimplemented */
        LogManagerLockPolicy_ATMutexLock
            ( const LogManagerLockPolicy_ATMutexLock & );
        LogManagerLockPolicy_ATMutexLock & operator=
            ( const LogManagerLockPolicy_ATMutexLock & );

    };


}


#endif
