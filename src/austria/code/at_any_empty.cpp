//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *
 * at_any_empty.cpp
 *
 *
 * A few things needed for the any class.
 *
 */


#include "at_any.h"

// Austria namespace
namespace at
{

const std::type_info & AnyManager_Empty::s_empty_type = typeid( AnyManager_Empty::Empty );

// ======== Any_ThrowConversionFailure =================================

void Any_ThrowConversionFailure()
{
    AT_ThrowDerivation(
        Any_ConversionFailure,
        "at::Any object conversion requested to a type not currently stored in this Any object"
    );
}

const AnyInitType AnyInit = AnyInitType();
    
}; // namespace
