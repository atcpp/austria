/**
 *  \file at_preference_mt.h
 *
 *  \author Bradley Austin
 *
 *  at_preference_mt.h contains extensions to the Austria preference
 *  module to support thread-safety.
 *
 */

#ifndef x_at_preference_mt_h_x
#define x_at_preference_mt_h_x


#include "at_preference.h"
#include "at_lifetime_mt.h"


namespace at
{


    class BasicMutex
      : public AbstractMutex
    {

        Mutex m_mutex;

    public:

        virtual void Lock()
        {
            m_mutex.Lock();
        }

        virtual void Unlock()
        {
            m_mutex.Unlock();
        }

        inline BasicMutex() {}

        virtual ~BasicMutex() {}

    private:

        /* Unimplemented */
        BasicMutex( const BasicMutex & );
        BasicMutex & operator=( const BasicMutex & );

    };


    class PreferenceManagerLockPolicy_ATMutexLock
    {

    public:

#ifndef AT_InstrumentPreferenceMgr
        typedef PtrTarget_MTVirtual  t_ptr_target_type;
#else
        struct NullRefCnt {};
        typedef NullRefCnt           t_ptr_target_type;
#endif

    private:

        BasicMutex m_mutex;

    public:

        inline AbstractMutex & ReadLock()
        {
            return m_mutex;
        }

        inline AbstractMutex & WriteLock()
        {
            return m_mutex;
        }

    protected:

        inline PreferenceManagerLockPolicy_ATMutexLock() {}
        inline ~PreferenceManagerLockPolicy_ATMutexLock() {}

    private:

        /* Unimplemented */
        PreferenceManagerLockPolicy_ATMutexLock
            ( const PreferenceManagerLockPolicy_ATMutexLock & );
        PreferenceManagerLockPolicy_ATMutexLock & operator=
            ( const PreferenceManagerLockPolicy_ATMutexLock & );

    };


}


#endif
