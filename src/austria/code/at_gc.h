/**
 *  \file at_gc.h
 *
 *  \author Bradley Austin
 *
 */


#include <set>

#include "at_policy.h"


#ifndef x_at_gc_h_x
#define x_at_gc_h_x


namespace at
{


    template<
        class w_vertex_policy,
        class w_error_policy
    >
    class SimpleGarbageCollector
      : public w_vertex_policy::template t_type<
            w_error_policy,
            PolicyCaster<
                SimpleGarbageCollector<
                    w_vertex_policy,
                    w_error_policy
                >
            >
        >,
        public w_error_policy
    {
    public:

        typedef
            typename w_vertex_policy::template t_type<
                w_error_policy,
                PolicyCaster< SimpleGarbageCollector >
            >
                t_vertex_policy;

        typedef  typename t_vertex_policy::t_vertex_ref  t_vertex_ref;
        typedef
            typename t_vertex_policy::t_vertex_dependencies_const_iterator
                t_vertex_dependencies_const_iterator;

        using t_vertex_policy::GetVertexDependencies;

        using w_error_policy::ReportAnchorNotInVertices;
        using w_error_policy::ReportDependantNotInVertices;


        template<
            typename w_vertex_iterator,
            typename w_anchor_iterator,
            typename w_linked_output_iterator,
            typename w_unlinked_output_iterator
        >
        void FindLinkedAndUnlinkedVertices(
            const w_vertex_iterator & i_vertices_begin,
            const w_vertex_iterator & i_vertices_end,
            const w_anchor_iterator & i_anchors_begin,
            const w_anchor_iterator & i_anchors_end,
            w_linked_output_iterator o_linked_output,
            w_unlinked_output_iterator o_unlinked_output
        )
        {

            typedef  std::set< t_vertex_ref >  t_vertex_set;
            typedef  typename t_vertex_set::iterator  t_vertex_set_iterator;

            t_vertex_set l_not_kept_vertices;

            {
                for ( w_vertex_iterator l_vertex_iter( i_vertices_begin );
                      !( l_vertex_iter == i_vertices_end );
                      ++l_vertex_iter
                    )
                {
                    l_not_kept_vertices.insert( *l_vertex_iter );
                }
            }

            t_vertex_set l_kept_vertices;

            {
                for ( w_anchor_iterator l_anchor_iter( i_anchors_begin );
                      !( l_anchor_iter == i_anchors_end );
                      ++l_anchor_iter
                    )
                {
                    t_vertex_ref l_anchor( *l_anchor_iter );
                    t_vertex_set_iterator l_not_kept_iter(
                        l_not_kept_vertices.find( l_anchor )
                    );
                    if ( l_not_kept_iter == l_not_kept_vertices.end() )
                    {
                        ReportAnchorNotInVertices( l_anchor );
                    }
                    else
                    {
                        l_kept_vertices.insert( l_anchor );
                        l_not_kept_vertices.erase( l_not_kept_iter );
                    }
                }
            }

            t_vertex_set l_done_vertices;

            while ( ! l_kept_vertices.empty() )
            {
                t_vertex_set_iterator l_kept_iter( l_kept_vertices.begin() );
                t_vertex_ref l_vertex( *l_kept_iter );
                l_kept_vertices.erase( l_kept_iter );
                l_done_vertices.insert( l_vertex );

                t_vertex_dependencies_const_iterator l_dependencies_begin;
                t_vertex_dependencies_const_iterator l_dependencies_end;
                GetVertexDependencies(
                    l_vertex,
                    l_dependencies_begin,
                    l_dependencies_end
                );

                for ( t_vertex_dependencies_const_iterator
                          l_dependencies_iter( l_dependencies_begin );
                      !( l_dependencies_iter == l_dependencies_end );
                      ++l_dependencies_iter
                    )
                {
                    t_vertex_ref l_dependant( *l_dependencies_iter );
                    if ( l_kept_vertices.find( l_dependant ) ==
                             l_kept_vertices.end() &&
                         l_done_vertices.find( l_dependant ) ==
                             l_done_vertices.end()
                       )
                    {
                        t_vertex_set_iterator l_not_kept_iter(
                            l_not_kept_vertices.find( l_dependant )
                        );
                        if ( l_not_kept_iter == l_not_kept_vertices.end() )
                        {
                            ReportDependantNotInVertices( l_dependant );
                        }
                        else
                        {
                            l_kept_vertices.insert( l_dependant );
                            l_not_kept_vertices.erase( l_not_kept_iter );
                        }
                    }
                }

            }
 
            {
                for ( t_vertex_set_iterator
                          l_linked_iter( l_done_vertices.begin() );
                      l_linked_iter != l_done_vertices.end();
                      ++l_linked_iter
                    )
                {
                    *o_linked_output = *l_linked_iter;
                    ++o_linked_output;
                }
            }

            {
                for ( t_vertex_set_iterator
                          l_unlinked_iter( l_not_kept_vertices.begin() );
                      l_unlinked_iter != l_not_kept_vertices.end();
                      ++l_unlinked_iter
                    )
                {
                    *o_unlinked_output = *l_unlinked_iter;
                    ++o_unlinked_output;
                }
            }

        }


        class t_null_output_iterator
        {
        public:

            class value_type
            {
            public:

                template< class w_rhs_type >
                inline void operator=( const w_rhs_type & )
                {
                }

            };

            inline value_type operator*() const
            {
                return value_type();
            }

            inline t_null_output_iterator & operator++()
            {
                return *this;
            }

        };


    };


    class SimpleGarbageCollector_ErrorPolicy_Null
    {
    public:

        template< class w_vertex_ref >
        inline void ReportAnchorNotInVertices( const w_vertex_ref & )
        {
        }

        template< class w_vertex_ref >
        inline void ReportDependantNotInVertices( const w_vertex_ref & )
        {
        }

    };


}


#endif
