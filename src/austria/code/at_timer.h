/**
 *  \file at_timer.h
 *
 *  \author Bradley Austin
 *
 */
#ifndef x_at_timer_h_x
#define x_at_timer_h_x 1


#include "at_lifetime_mt.h"
#include "at_twinmt.h"
#include "at_twinmt_basic.h"
#include "at_types.h"


namespace at
{

    /**
     *  @defgroup AustriaTimer Timer Service
     *
     *  @{
     */


    // ======== TimerLeadIf ===========================================
    /**
     *  Base class for client objects of classes deriving from
     *  TimerService.
     */
    class TimerLeadIf
    {

    public:

        // ======== Event() ===========================================
        /**
         *  Event() is the callback method, implemented by the user,
         *  that will be invoked by the timer service at the requested
         *  time(s).  The first time that Event() will be called is
         *  specified in the call to TimerService::StartTimer().  If
         *  Event() returns true, the timer service will re-schedule
         *  Event() to be called again at the time specified by
         *  o_reschedule_time.
         *
         *  Since all calls to Event() by a given timer service are
         *  made from the same thread, the timer service cannot do
         *  anything else until Event() returns.  So whatever Event()
         *  is implemented to do shouldn't be anything consuming a
         *  significant amount of time.
         *
         *  Event() is allowed to interact with the timer service,
         *  including registering new clients with StartTimer(), and/or
         *  cancelling clients, either the same one receiving the event
         *  or others registered with the same TimerService object.
         *  However, if Event() cancels its own client, it is *not*
         *  allowed to cause the TimerService object to be destroyed by
         *  deleting the last reference to it.  Also, if Event()
         *  cancels its own client, it is not allowed to return true.
         *
         *  Note that returning false from Event() does not cause the
         *  client's lead-aide relationship to be cancelled.
         *  LeadCancel() must still be called either prior to or during
         *  destruction of the client, whether or not there is a call
         *  to Event() by the timer service pending at the time.
         *
         *  The user is responsible for serializing access to any data
         *  in the client accessed both by Event() and by threads other
         *  than the timer service thread.
         *
         *  @param i_current_time The current thread system time (provided for convenience).
         *  @param i_reschedule_time The time the timer service should next call Event() for this client.
         *  @return True if the timer service should call Event() again, otherwise false.
         */
        virtual bool Event(
            const TimeStamp & i_current_time,
            TimeStamp & o_reschedule_time
        ) = 0;

    };

    // ======== TimerAideIf ===========================================
    /**
     *  Base class for the aide objects associated with TimerLeadIf
     *  objects by TimerService::StartTimer().
     */
    class TimerAideIf
    {
    public:
        // ======== GetNextEventTime() ================================
        /**
         *  GetNextEventTime() is a method, implemented by the timer
         *  service aide, that can be called by the associated timer
         *  service lead to know if the service is scheduled to call
         *  the lead's Event() method and, if it is, when the method
         *  will be called called.
         *
         *  @param o_next_event_time The time the timer service will
         *                           call the Event() method.
         *  @return \c true if the timer service is scheduled to call
         *          to call the Event() method of this client,
         *          \c false otherwise.
         */
        virtual bool GetNextEventTime(
            TimeStamp & o_next_event_time
        ) = 0;
    };

    // ======== TimerClient_Basic =====================================
    /**
     *  Basic (partial) implementation of TimerLeadIf interface.
     */
    class TimerClient_Basic
        : public LeadTwinMT_Basic< TimerLeadIf, AideTwinMT< TimerAideIf > >
    {
    public:
        // ======== GetNextEventTime() ================================
        /**
         *  GetNextEventTime() is a method that clients of the timer
         *  service (deriving from TimerClient_Basic) can call to know
         *  if the timer service is scheduled to call their Event()
         *  method, and, if it is, when the method will be called.
         *
         *  @param o_next_event_time The time the timer service will
         *                           call the Event() method.
         *  @return \c true if the timer service is scheduled to call
         *          to call the Event() method of this client,
         *          \c false otherwise.
         */
        bool GetNextEventTime(
            TimeStamp & o_next_event_time
        ) {
            bool l_is_event_scheduled;
            if (
                CallAide().ReturnCall(
                    l_is_event_scheduled,
                    &TimerAideIf::GetNextEventTime,
                    o_next_event_time
                    )
                )
            {
                return l_is_event_scheduled;
            }
            return false;
        }
    };
 
    typedef
        LeadTwinMT< TimerLeadIf, AideTwinMT< TimerAideIf > >
            TimerClientInterface;


    // ======== TimerSericeUnavailable ========================
    /**
     * at::ExceptionDerivation<TimerSericeUnavailable> is
     * thrown when the timer service has been shut down
     * and a StartTimer request is made.
     */

    struct TimerSericeUnavailable {};

    // ======== TimerService ==========================================
    /**
     *  Classes that implement the TimerService interface provide a
     *  service by which clients (which must derive from
     *  TimerClientInterface) can register a callback method to be
     *  called by the TimerService at a given time, or sequence of
     *  times.  This allows a single thread with an associated event
     *  queue to do what would otherwise require a potentially
     *  unbounded number of (sleeping) threads, each waiting for a
     *  single event.
     *
     *  The TimerService object is reference-counted, and each aide
     *  associated with a client holds a reference, so the TimerService
     *  object is guaranteed to live at least as long as any clients
     *  remain registered with it.
     */
    class TimerService
      : public PtrTarget_MT
    {

        public:

        // ======== Shutdown ==========================================
        /**
         * Shutdown causes the timer service to shut down all the 
         * timers clients and refuse to accept any new requests.
         *
         */

        virtual void	Shutdown() = 0;

        // ======== StartTimer() ======================================
        /**
         *  StartTimer() registers a TimerClient with the TimerService
         *  (i.e. causes the TimerService to invoke the TimerClient's
         *  Event() method when a specified thread system time has been
         *  reached).
         *
         *  The TimerClient passed to StartTimer() should be
         *  unassociated (i.e. not associated with an aide).
         *  StartTimer() creates an aide object and associates it with
         *  the TimerClient.  At the specified time, the TimerClient's
         *  Event() method will be invoked by the aide, across the
         *  lead-aide interface.  The TimerClient can atomically cancel
         *  a pending call to Event() (if any) by cancelling the lead-
         *  aide association.  On cancellation, the TimerService
         *  automatically destroys the aide.
         *
         *  @param i_lead The TimerClient.
         *  @param i_requested_time The time the TimerClient's Event() method should first be called by the Timer service.
         *  @return Nothing.
         *  @throws at::ExceptionDerivation<TimerSericeUnavailable>.
         */
        virtual void StartTimer(
            TimerClientInterface    & i_lead,
            const TimeStamp         & i_requested_time
        ) = 0;


        // ======== StartTimer() ======================================
        /**
         *  Relative-time version convenience wrapper for StartTimer().
         *
         *  @param i_lead The TimerClient.
         *  @param i_requested_interval The offset from the current time that the TimerClient's Event() method should first be called by the Timer service.
         *  @return Nothing.
         */
        inline void StartTimer(
            TimerClientInterface    & i_lead,
            const TimeInterval      & i_requested_interval
        )
        {
            return StartTimer( i_lead, ThreadSystemTime() + i_requested_interval );
        }

    };


    /** @} */


}


#endif
