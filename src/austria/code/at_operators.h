/**
 *  at_operators.h
 *
 */


#ifndef x_at_operators_h_x
#define x_at_operators_h_x 1


#include "at_crtp.h"


namespace at
{


    /**
     *  @defgroup AustriaOperators Overloaded Operator Support
     *
     *  @{
     */


    // ======== EqualOps ==============================================
    /**
     *  EqualOps is one of a series of class templates that utilize the
     *  Curiously Recursive Template Pattern to streamline the process
     *  of defining classes that use overloaded operators.
     *
     *  If a class provides a definition for the equality (==)
     *  operator, and also derives from EqualOps, then the inequality
     *  (!=) operator will be defined automatically.
     *
     *  @param w_type The deriving class, for which != is to be defined.
     */
    template< class w_type >
    class EqualOps
      : public Downcastable< EqualOps< w_type >, w_type >
    {

        using Downcastable< EqualOps, w_type >::This;

    public:

        // ======== operator!= ========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return True iff This() != i_rhs.
         */
        inline bool operator!=( const w_type & i_rhs ) const
        {
            return ! ( This() == i_rhs );
        }

    protected:

        EqualOps() {}
        ~EqualOps() {}

    };


    // ======== CompareOps ============================================
    /**
     *  CompareOps is one of a series of class templates that utilize
     *  the Curiously Recursive Template Pattern to streamline the
     *  process of defining classes that use overloaded operators.
     *
     *  If a class provides a definition for the less-than (<) and
     *  equality (==) operators, and also derives from CompareOps, then
     *  the greater-than (>), not-greater-than (<=), not-less-than
     *  (>=), and inequality (!=) operators will be defined
     *  automatically.
     *
     *  (Note that CompareOps does not automatically define equality in
     *  terms of less-than, since there are usually more efficient ways
     *  to test whether A == B than to test whether
     *  !( A < B || B < A ).)
     *
     *  @param w_type The deriving class, for which >, <=, >=, and != are to be defined.
     */
    template< class w_type >
    class CompareOps
      : public Downcastable< CompareOps< w_type >, w_type >,
        public EqualOps< w_type >
    {

        using Downcastable< CompareOps, w_type >::This;

    public:

        // ======== operator> =========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return True iff This() > i_rhs.
         */
        inline bool operator>( const w_type & i_rhs ) const
        {
            return i_rhs < This();
        }

        // ======== operator<= ========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return True iff This() <= i_rhs.
         */
        inline bool operator<=( const w_type & i_rhs ) const
        {
            return ! ( i_rhs < This() );
        }

        // ======== operator>= ========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return True iff This() >= i_rhs.
         */
        inline bool operator>=( const w_type & i_rhs ) const
        {
            return ! ( This() < i_rhs );
        }

    protected:

        CompareOps() {}
        ~CompareOps() {}

    };


    // ======== AddSubtractOps ========================================
    /**
     *  AddSubtractOps is one of a series of class templates that
     *  utilize the Curiously Recursive Template Pattern to streamline
     *  the process of defining classes that use overloaded operators.
     *
     *  If a class provides a definition for the plus-assign (+=) and
     *  minus-assign (-=) operators for some right-hand-side operand
     *  type (the delta type), and also derives from AddSubtractOps
     *  (using the delta type as the second parameter), then the plus
     *  (+) and minus (-) operators will be defined automatically.
     *  (Note that a class can derive from AddSubtractOps multiple
     *  times using different delta types.)
     *
     *  (Note that AddSubtractOps does not define -= in terms of +=
     *  and the delta type's negate (unary minus) operator.  While this
     *  might work for some delta types, it would be problematic for
     *  types that don't have exactly the same number of positive and
     *  negative values (two's complement integers for example).)
     *
     *  A class that derives from AddSubtractOps using a delta type
     *  different from itself will probably also want to define a minus
     *  operation for two objects of its own type, returning the delta
     *  type.  AddSubtractOps does not provide this, except in the case
     *  where w_type and w_delta_type are the same type.
     *
     *  A class that derives from AddSubtractOps using a delta type
     *  different from itself will probably also want to define a plus
     *  operation where the delta type is on the left-hand-side.
     *  AddSubtractOps does not provide this, except in the case where
     *  w_type and w_delta_type are the same type.
     *
     *  @param w_type The deriving class, for which + and - are to be defined.
     *  @param w_delta_type The offset magnitude and direction (delta) type.
     */
    template<
        class w_type,
        typename w_delta_type
    >
    class AddSubtractOps
      : public Downcastable< AddSubtractOps< w_type, w_delta_type >, w_type >
    {

        using Downcastable< AddSubtractOps, w_type >::This;

    public:

        // ======== operator+ =========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return The result of increasing This() by i_rhs.
         */
        inline w_type operator+( const w_delta_type & i_rhs ) const
        {
            w_type l_result = This();
            l_result += i_rhs;
            return l_result;
        }

        // ======== operator- =========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return The result of decreasing This() by i_rhs.
         */
        inline w_type operator-( const w_delta_type & i_delta ) const
        {
            w_type l_result = This();
            l_result -= i_delta;
            return l_result;
        }

    protected:

        AddSubtractOps() {}
        ~AddSubtractOps() {}

    };


    // ======== IncrementOps ==========================================
    /**
     *  IncrementOps is one of a series of class templates that utilize
     *  the Curiously Recursive Template Pattern to streamline the
     *  process of defining classes that use overloaded operators.
     *
     *  If a class provides a definition for the prefix version of
     *  increment, and also derives from IncrementOps, then the postfix
     *  version of increment will be defined automatically.
     *
     *  @param w_type The deriving class, for which postfix ++ is to be defined.
     */
    template< class w_type >
    class IncrementOps
      : public Downcastable< IncrementOps< w_type >, w_type >
    {

        using Downcastable< IncrementOps, w_type >::This;

    public:

        // ======== operator++ ========================================
        /**
         *  @return The value of This() before the increment.
         */
        inline w_type operator++( int )
        {
            w_type l_result = This();
            ++ This();
            return l_result;
        }

    protected:

        IncrementOps() {}
        ~IncrementOps() {}

    };


    // ======== IncrementDecrementOps =================================
    /**
     *  IncrementDecrementOps is one of a series of class templates
     *  that utilize the Curiously Recursive Template Pattern to
     *  streamline the process of defining classes that use overloaded
     *  operators.
     *
     *  If a class provides definitions for the prefix versions of
     *  increment and decrement, and also derives from
     *  IncrementDecrementOps, then the postfix versions of increment
     *  and decrement will be defined automatically.
     *
     *  @param w_type The deriving class, for which postfix ++ and -- are to be defined.
     */
    template< class w_type >
    class IncrementDecrementOps
      : public Downcastable< IncrementDecrementOps< w_type >, w_type >,
        public IncrementOps< w_type >
    {

        using Downcastable< IncrementDecrementOps, w_type >::This;

    public:

        // ======== operator-- ========================================
        /**
         *  @return The value of This() before the decrement.
         */
        inline w_type operator--( int )
        {
            w_type l_result = This();
            -- This();
            return l_result;
        }

    protected:

        IncrementDecrementOps() {}
        ~IncrementDecrementOps() {}

    };


    // ======== IntegerAddSubtractOps =================================
    /**
     *  IntegerAddSubtractOps is one of a series of class templates
     *  that utilize the Curiously Recursive Template Pattern to
     *  streamline the process of defining classes that use overloaded
     *  operators.
     *
     *  If a class provides a definition for the plus-assign (+=)
     *  operator for some right-hand-side operand type (the delta type,
     *  which must have a constructor taking the int value 1), and also
     *  derives from IntegerAddSubtractOps (using the delta type as the
     *  second parameter), then the plus (+) and minus (-) operators,
     *  and both prefix and postfix versions of increment (++) and
     *  decrement (--), will be defined automatically.
     *
     *  (Note that IntegerAddSubtractOps does not define -= in terms of
     *  += and the delta type's negate (unary minus) operator.  While
     *  this might work for some delta types, it would be problematic
     *  for types that don't have exactly the same number of positive
     *  and negative values (two's complement integers for example).)
     *
     *  A class that derives from IntegerAddSubtractOps using a delta
     *  type different from itself will probably also want to define a
     *  minus operation for two objects of its own type, returning the
     *  delta type.  IntegerAddSubtractOps does not provide this,
     *  except in the case where w_type and w_delta_type are the same
     *  type.
     *
     *  A class that derives from IntegerAddSubtractOps using a delta
     *  type different from itself will probably also want to define a
     *  plus operation where the delta type is on the left-hand-side.
     *  IntegerAddSubtractOps does not provide this, except in the
     *  case where w_type and w_delta_type are the same type.
     *
     *  @param w_type The deriving class, for which +, -, ++, and -- are to be defined.
     *  @param w_delta_type The offset magnitude and direction (delta) type.
     */
    template< class w_type, class w_delta_type >
    class IntegerAddSubtractOps
      : public Downcastable< IntegerAddSubtractOps< w_type, w_delta_type >, w_type >,
        public AddSubtractOps< w_type, w_delta_type >,
        public IncrementDecrementOps< w_type >
    {

        using Downcastable< IntegerAddSubtractOps, w_type >::This;

    public:

        // ======== operator++ ========================================
        /**
         *  @return A reference to This().
         */
        inline w_type & operator++()
        {
            return This() += w_delta_type( 1 );
        }

        /*  Disambiguation.  Necessary so that operator++( int ) isn't
         *  hidden by the definition of operator++().
         */
        inline w_type operator++( int i )
        {
            return IncrementDecrementOps< w_type >::operator++( i );
        }

        // ======== operator-- ========================================
        /**
         *  @return A reference to This().
         */
        inline w_type & operator--()
        {
            return This() -= w_delta_type( 1 );
        }

        /*  Disambiguation.  Necessary so that operator--( int ) isn't
         *  hidden by the definition of operator--().
         */
        inline w_type operator--( int i )
        {
            return IncrementDecrementOps< w_type >::operator--( i );
        }


    protected:

        IntegerAddSubtractOps() {}
        ~IntegerAddSubtractOps() {}

    };


    // ======== IntegerMultiplyDivideOps ==============================
    /**
     *  IntegerMultiplyDivideOps is one of a series of class templates
     *  that utilize the Curiously Recursive Template Pattern to
     *  streamline the process of defining classes that use overloaded
     *  operators.
     *
     *  If a class provides definitions for the multiply-assign (*=)
     *  and divide-assign (/=) operators for some right-hand-side
     *  operand type (the scalar type), and also derives from
     *  IntegerMultiplyDivideOps (using the scalar type as the second
     *  parameter), then the multiply (*) and divide (/) operators
     *  taking the scalar type for the right-hand-side argument will be
     *  defined automatically.  (Note that a class may derive from
     *  IntegerMultiplyDivideOps multiple times using different scalar
     *  types.
     *
     *  A class that derives from IntegerMultiplyDivideOps using a
     *  scalar type different from itself will also probably want to
     *  define a divide operation taking two objects of its own type,
     *  and returning the scalar type.  IntegerMultiplyDivideOps does
     *  not provide this, except in the case where w_type and
     *  w_scalar_type are the same type.
     *
     *  A class that derives from IntegerMultiplyDivideOps using a
     *  scalar type different from itself will also probably want to
     *  define a multipy operation where the scalar type is on the
     *  left-hand side.  IntegerMultiplyDivideOps does not provide
     *  this, except in the case where w_type and w_scalar_type are the
     *  same type.
     *
     *  A class that derives from IntegerMultiplyDivideOps may also
     *  want to derive from ModulusOps.
     *
     *  @param w_type The deriving class, for which * and / are to be defined.
     *  @param w_scalar_type The offset magnitude and direction (delta) type.
     */
    template< class w_type, class w_scalar_type >
    class IntegerMultiplyDivideOps
      : public Downcastable< IntegerMultiplyDivideOps< w_type, w_scalar_type >, w_type >
    {

        using Downcastable< IntegerMultiplyDivideOps, w_type >::This;

    public:

        // ======== operator* =========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return The result of multiplying This() by i_rhs.
         */
        inline w_type operator*( const w_scalar_type & i_rhs ) const
        {
            w_type l_result = This();
            l_result *= i_rhs;
            return l_result;
        }

        // ======== operator/ =========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return The result of dividing This() by i_rhs.
         */
        inline w_type operator/( const w_scalar_type & i_rhs ) const
        {
            w_type l_result = This();
            l_result /= i_rhs;
            return l_result;
        }

    protected:

        IntegerMultiplyDivideOps() {}
        ~IntegerMultiplyDivideOps() {}

    };


    // ======== ModulusOps ============================================
    /**
     *  ModulusOps is one of a series of class templates that utilize
     *  the Curiously Recursive Template Pattern to streamline the
     *  process of defining classes that use overloaded operators.
     *
     *  If a class provides a definition for the modulus-assign (%=)
     *  operator, and also derives from ModulusOps, then the modulus
     *  (%) operator will be defined automatically.
     *
     *  @param w_type The deriving class, for which % is to be defined.
     */
    template< class w_type >
    class ModulusOps
      : public Downcastable< ModulusOps< w_type >, w_type >
    {

        using Downcastable< ModulusOps, w_type >::This;

    public:

        // ======== operator% =========================================
        /**
         *  @param i_rhs The right-hand-side operand.
         *  @return The result of This() mod i_rhs.
         */
        inline w_type operator%( const w_type & i_rhs ) const
        {
            w_type l_result = This();
            l_result %= i_rhs;
            return l_result;
        }

    protected:

        ModulusOps() {}
        ~ModulusOps() {}

    };


    /** @} */


}


#endif
