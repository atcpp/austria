/*
 * This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 * Any unauthorized use of this file is prohibited and will be prosecuted
 * to the full extent of the law.
 *
 * Herein lie a few things to make using at_net.h less of a pain.
 */
#include "at_net_helpers.h"
#include "at_twinmt_basic.h"
#include "at_net_http.h"
#include "at_factory.h"
#include <sstream>

using namespace at;

namespace
{
    // Used by SyncListenDatagram
    class SyncDatagramLead : public LeadTwinMT_Basic<NetDatagramLeadIf>
    {
    public:
        SyncDatagramLead() : channel(0), error(0) { }

        ~SyncDatagramLead()
        {
            this->LeadCancel();
        }

    public: // NetDatagramLead
        virtual void Failed(const NetError &e)
        {
            Lock<ConditionalMutex> l(cond);
            error = &e;
            cond.Post();
        }

        virtual void ListenerReady(PtrDelegate<NetDatagramChannel*> c)
        {
            Lock<ConditionalMutex> l(cond);
            channel = c;
            cond.Post();
        }

        Ptr<NetDatagramChannel*> channel;
        const NetError *error;
        ConditionalMutex cond;
    };

    // Used by SyncResolve
    class SyncResolverLead : public LeadTwinMT_Basic<NetResolverLeadIf>
    {
    public:
        SyncResolverLead() : m_done(false), m_error(0) { }

        ~SyncResolverLead()
        {
            this->LeadCancel();
        }

    public: // NetResolverLead
        void Failed(const NetResolverError &e)
        {
            Lock<ConditionalMutex> l(m_cond);
            m_error = &e;
            m_done = true;
            m_cond.Post();
        }

        void NetResolverResults(std::list<at::Ptr<NetAddress*> > &r)
        {
            Lock<ConditionalMutex> l(m_cond);
            m_results = r;
            m_done = true;
            m_cond.Post();
        }

        bool Wait(const NetError **o_err, std::list<at::Ptr<NetAddress*> > *o_r)
        {
            Lock<ConditionalMutex> l(m_cond);
            while (m_done == false) m_cond.Wait();
            *o_err = m_error;
            *o_r = m_results;
            return m_error == 0;
        }

    private:
        bool m_done;
        const NetError *m_error;
        std::list<at::Ptr<NetAddress*> > m_results;
        ConditionalMutex m_cond;
    };
}

PtrDelegate<NetDatagramChannel*>
at::SyncListenDatagram(
    PtrDelegate<Net*> i_net,
    PtrDelegate<const NetAddress*> i_address,
    PtrDelegate<const NetParameters*> i_parameters,
    PtrDelegate<Pool*> i_buffer_pool,
    const NetError **o_error)
{
    SyncDatagramLead lead;
    i_net->ListenDatagram(&lead, i_address, i_parameters, i_buffer_pool);
    {
        Lock<ConditionalMutex> l(lead.cond);
        while (lead.channel == 0 && lead.error == 0) lead.cond.Wait();
    }
    if (lead.error) *o_error = lead.error;
    else *o_error = 0;
    return lead.channel;
}

PtrDelegate<const NetAddress*>
at::SyncResolve(
    PtrDelegate<Net*> i_net,
    PtrDelegate<const NetAddress*> i_addr)
{
    SyncResolverLead lead;
    const NetError *err;
    std::list<at::Ptr<NetAddress*> > results;

    i_net->Resolve(&lead, i_addr);
    if (lead.Wait(&err, &results)) return results.front();
    else return 0;
}

namespace
{
    /**
     * This lead just sucks up HTTP response bytes.  You need to set the
     * response variable.  (It is not set in the constructor so that you
     * can use XConnectLead with this class.)  The complete condition will
     * receive a notification once the response has been received or
     * failure has occurred.  In the case of failure, statusCode will be
     * zero, as will response.  On success, the response will be appended
     * to the buffer you set as response (as opposed to overwriting the
     * data that is already there).
     */
    class HTTPResponseRecorder :
        public LeadTwinMT_Basic<NetConnectionResponderIf>,
        public PtrTarget_MT
    {
    public:
        Ptr<NetConnection*> conn;
        ConditionalMutex complete;
        at::Ptr<at::Buffer*> response;
        std::string contentType;
        int statusCode;

        HTTPResponseRecorder(PtrDelegate<NetConnection*> conn) :
            conn(conn),
            statusCode(0)
        {
            conn->ConnectionNotify(*this);
        }

        void ReceiveFailure(const NetConnectionError &i_error)
        {
            Lock<ConditionalMutex> l(complete);
            response = 0;
            complete.Post();
        }

        void SendCompleted(PtrDelegate<const Buffer*> i_buffer,
                           const char *i_end)
        {
            // Great.
        }

        void SendFailure(PtrDelegate<const Buffer*> i_buffer,
                         const char *i_begin,
                         const NetConnectionError &i_error)
        {
            Lock<ConditionalMutex> l(complete);
            response = 0;
            complete.Post();
        }

        void DataReady()
        {
            Lock<ConditionalMutex> l(complete);
            bool b;
            do
            {
                size_t newCap
                    = (response->capacity() - response->size() + 1) * 2;
                response->reserve(newCap);
                b = conn->Receive(response);
            }
            while (b);
            AT_Assert(statusCode != 0);
            complete.Post();
        }

        void OutOfBandDataReady()
        {
            // Huh.
        }

        void ReceivedPropertiesNotification(
            bool i_complete, bool i_props_in_queue)
        {
            Lock<ConditionalMutex> l(complete);
            NetConnection::t_PropList pl = conn->GetReceivedProperties();
            NetConnection::t_PropList::iterator it = pl.begin();
            NetConnection::t_PropList::iterator end = pl.end();
            for (; it != end; ++it)
            {
                NetProperty *p = it->Get();
                HTTPStatusProperty *s;
                HTTPHeaderProperty *h;

                if (( s = dynamic_cast<HTTPStatusProperty*>(p) ))
                {
                    this->statusCode = s->statusCode;
                }
                else if (h = dynamic_cast<HTTPHeaderProperty*>(p))
                {
                    if (NoCaseCompareExact(h->name, "content-type"))
                    {
                        this->contentType = h->value;
                    }
                }
            }
        }
    };
}

bool
SyncHTTPRequest(
    const char *uri, const char *method,
    const char *contentType, Ptr<Buffer*> content,
    int *statusOut, at::Ptr<at::Buffer*> responseOut,
    std::string *contentTypeOut)
{
    Ptr<HTTPResponseRecorder *> rec;
    Ptr<NetConnection*> conn;
    XConnectLead<HTTPResponseRecorder> connectLead;
    Ptr<HTTPAddress*> addr = new HTTPAddress(uri);

    Ptr<Net*> ip = FactoryRegister<at::Net, DKy>::Get().Create("Net")();
    if (!ip) {
        return false;
    }
    ip->Connect(&connectLead, addr, 0);
    connectLead.Wait(&rec, 0);
    if (rec == 0) return false;
    conn = rec->conn;
    rec->response = responseOut;

    NetConnection::t_PropList pl;
    pl.push_back(new HTTPMethodProperty(method));
    if (content->size())
    {
        pl.push_back(new HTTPHeaderProperty("Content-type", contentType));
    }
    conn->AddOutgoingProperties(pl);
    conn->Send(content);
    rec->complete.Wait();
    if (rec->response == 0) return false;
    *statusOut = rec->statusCode;
    *contentTypeOut = rec->contentType;
    return true;
}

const char*
NetConnectionEvent::TypeToString(Type t)
{
    switch (t)
    {
    case ReceiveFailure:
        return "ReceiveFailure";
    case SendFailure:
        return "SendFailure";
    case SendCompleted:
        return "SendCompleted";
    case DataReady:
        return "DataReady";
    case Closed:
        return "Closed";
    case ReceivedProperties:
        return "ReceivedProperties";
    case Invalid:
    default:
        return "Invalid";
    }
}

std::ostream&
at::operator<<(std::ostream &os, const NetConnectionEvent &ev)
{
    os << "ConnEv(seq=" << ev.seqno
       << ", type=" << NetConnectionEvent::TypeToString(ev.type);
    // TODO: Output the other relevant fields.
    os << ")";
    return os;
}

NetConnectionRecorder::NetConnectionRecorder(PtrDelegate<NetConnection*> conn) :
    m_seq(0),
    m_conn(conn),
    events(),
    m_stateMutex(new MutexRefCount(Mutex::Recursive)),
    m_newEvent(new Conditional(*m_stateMutex))
{
    m_conn->ConnectionNotify(*this);
}

void
NetConnectionRecorder::ReceiveFailure(const NetConnectionError &i_error)
{
    NetConnectionEvent call = NetConnectionEvent();
    call.type = NetConnectionEvent::ReceiveFailure;
    call.error = &i_error;
    Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
    call.seqno = m_seq++;
    events.push_back(call);
    m_newEvent->Post();
}

void
NetConnectionRecorder::SendCompleted(
    PtrDelegate<const Buffer*> i_buffer, const char *i_end)
{
    NetConnectionEvent call = NetConnectionEvent();
    call.type = NetConnectionEvent::SendCompleted;
    call.const_buffer = i_buffer;
    call.end = i_end;
    Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
    call.seqno = m_seq++;
    events.push_back(call);
    m_newEvent->Post();
}

void
NetConnectionRecorder::SendFailure(
    PtrDelegate<const Buffer*> i_buffer,
    const char *i_begin,
    const NetConnectionError &i_error)
{
    NetConnectionEvent call = NetConnectionEvent();
    call.type = NetConnectionEvent::SendCompleted;
    call.const_buffer = i_buffer;
    call.begin = i_begin;
    call.error = &i_error;
    Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
    call.seqno = m_seq++;
    events.push_back(call);
    m_newEvent->Post();
}

void
NetConnectionRecorder::DataReady()
{
    NetConnectionEvent call = NetConnectionEvent();
    call.type = NetConnectionEvent::DataReady;
    Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
    call.seqno = m_seq++;
    events.push_back(call);
    m_newEvent->Post();
}

void
NetConnectionRecorder::OutOfBandDataReady()
{
    // TODO: Record this.
    AT_Assert(false);
}

void
NetConnectionRecorder::ReceivedPropertiesNotification(
    bool i_complete, bool i_props_in_queue)
{
    NetConnectionEvent call = NetConnectionEvent();
    call.type = NetConnectionEvent::ReceivedProperties;
    call.hasProperties = i_props_in_queue;
    call.hasAllProperties = i_complete;
    Lock<Ptr<MutexRefCount *> > l(m_stateMutex);
    call.seqno = m_seq++;
    events.push_back(call);
    m_newEvent->Post();
}

bool
at::FindContentLength(const NetConnection::t_PropList &i_props, size_t *o_len)
{
    NetConnection::t_PropList::const_iterator it = i_props.begin();
    NetConnection::t_PropList::const_iterator end = i_props.end();

    for (; it != end; ++it)
    {
        const HTTPHeaderProperty *h;
        h = dynamic_cast<const HTTPHeaderProperty*>(it->Get());
        if (!h) continue;
        if (NoCaseCompareExact(h->name, "content-length"))
        {
            std::stringstream ss;
            ss << h->value;
            ss >> *o_len;
            return true;
        }
    }
    return false;
}

bool
at::FindHTTPStatus(const NetConnection::t_PropList &i_props, int *o_status)
{
    NetConnection::t_PropList::const_iterator it = i_props.begin();
    NetConnection::t_PropList::const_iterator end = i_props.end();

    for (; it != end; ++it)
    {
        const HTTPStatusProperty *s;
        if (( s = dynamic_cast<const HTTPStatusProperty*>(it->Get()) ))
        {
            std::stringstream ss;
            *o_status = s->statusCode;
            return true;
        }
    }
    return false;
}

/**
* case insensitive ascii string compare -- ignoring locale
* @param i_first std::string
* @param i_second std::string
* @return true if strings match with case insensitive compare
*/
bool at::NoCaseCompareExact(const std::string s1, const std::string s2)
{
    bool retval = false;
    if (s1.size() == s2.size()) {
        retval = true; // will set to false when we find a mismatch
        for (size_t i=0; i < s1.size(); ++i) {
            unsigned char c1 = s1[i];
            unsigned char c2 = s2[i];
            if (c1 >= 'a' && c1 <= 'z') {
                c1 -= 'a' - 'A';
            }
            if (c2 >= 'a' && c2 <= 'z') {
                c2 -= 'a' - 'A';
            }
            if (c1 != c2) {
                retval = false;
                break;
            }
        }
    }
    return retval;
}
