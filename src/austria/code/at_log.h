/**
 * \file at_log.h
 *
 * \author Bradley Austin
 *
 * at_log.h is a logging library.
 *
 */

#ifndef x_at_log_h_x
#define x_at_log_h_x 1


#include "at_assert.h"
#include "at_policy.h"
#include "at_crtp.h"

#include <string>
#include <sstream>
#include <vector>
#include <utility>
#include <list>


/**
 * @defgroup AustriaLog Logging

Under construction!

 *
 */


namespace at
{
    
enum EnableLogLevelEnum
{
    ENABLE_LOGLEVEL_TRACE   = 1,
    ENABLE_LOGLEVEL_DEBUG   = 1,
    ENABLE_LOGLEVEL_INFO    = 1,
    ENABLE_LOGLEVEL_WARNING = 1,
    ENABLE_LOGLEVEL_ERROR   = 1,
    ENABLE_LOGLEVEL_SEVERE  = 1,
};

enum LogLevelEnum
{
    LOGLEVEL_TRACE   = ENABLE_LOGLEVEL_TRACE    ? 1 << 3  : 0,
    LOGLEVEL_DEBUG   = ENABLE_LOGLEVEL_DEBUG    ? 1 << 6  : 0,
    LOGLEVEL_INFO    = ENABLE_LOGLEVEL_INFO     ? 1 << 9  : 0,
    LOGLEVEL_WARNING = ENABLE_LOGLEVEL_WARNING  ? 1 << 12 : 0,
    LOGLEVEL_ERROR   = ENABLE_LOGLEVEL_ERROR    ? 1 << 15 : 0,
    LOGLEVEL_SEVERE  = ENABLE_LOGLEVEL_SEVERE   ? 1 << 18 : 0,

    DEFAULT_LOGLEVEL = ~((1 << 15) - 1),    // LOGLEVEL_ERROR and above
    GLOBAL_LOGLEVEL  = ~0,
};

}

enum AT_LogLevelEnum
{
    AT_LOGLEVEL_TRACE   = at::LOGLEVEL_TRACE,
    AT_LOGLEVEL_DEBUG   = at::LOGLEVEL_DEBUG,
    AT_LOGLEVEL_INFO    = at::LOGLEVEL_INFO,
    AT_LOGLEVEL_WARNING = at::LOGLEVEL_WARNING,
    AT_LOGLEVEL_ERROR   = at::LOGLEVEL_ERROR,
    AT_LOGLEVEL_SEVERE  = at::LOGLEVEL_SEVERE,

    AT_DEFAULT_LOGLEVEL = at::DEFAULT_LOGLEVEL,
    AT_GLOBAL_LOGLEVEL  = at::GLOBAL_LOGLEVEL,
};


// Austria namespace
namespace at
{


    template< class w_mgr_type >
    class LogSessionVarStream;


    template< class w_mgr_type >
    class LogSession
    {

        friend class LogSessionVarStream< w_mgr_type >;

        typedef  typename w_mgr_type::t_time_type  t_time_type;
        typedef  typename w_mgr_type::t_loglevel_type  t_loglevel_type;
        typedef  typename w_mgr_type::t_desc_type  t_desc_type;
        typedef  typename w_mgr_type::t_vars_type  t_vars_type;
        typedef  typename w_mgr_type::t_vars_value_type  t_vars_value_type;
        typedef  typename w_mgr_type::t_var_list_type  t_var_list_type;

        w_mgr_type & m_mgr;
        t_time_type m_time;
        const char * m_file_name;
        int          m_line_num;
        t_loglevel_type m_severity;
        t_desc_type m_desc;
        t_vars_type m_vars;

    public:

        LogSession(
            w_mgr_type & io_mgr,
            const char * i_file_name,
            int          i_line_num,
            const t_loglevel_type & i_severity,
            const t_desc_type & i_desc
        )
          : m_mgr( io_mgr ),
            m_time( io_mgr.GetTime() ),
            m_file_name( i_file_name ),
            m_line_num( i_line_num ),
            m_severity( i_severity ),
            m_desc( i_desc )
        {
        }

        inline LogSessionVarStream< w_mgr_type > operator()(
            const t_desc_type & i_desc
        )
        {
            m_vars.push_back
                ( t_vars_value_type( i_desc, t_var_list_type() ) );
            return LogSessionVarStream< w_mgr_type >
                ( *this, m_vars.size() - 1 );
        }

        ~LogSession()
        {
            m_mgr.LogEvent
                ( m_time, m_file_name, m_line_num,
                  m_severity, m_desc, m_vars );
        }

    private:

        /* Unimplemented */
        LogSession( const LogSession & );
        LogSession & operator=( const LogSession & );

    };

    template< class w_mgr_type >
    class LogSessionVarStream
    {

        friend class LogSession< w_mgr_type >;

        LogSession< w_mgr_type > & m_session;
        const size_t m_pos;

        LogSessionVarStream(
            LogSession< w_mgr_type > & io_session,
            size_t i_pos
        )
          : m_session( io_session ),
            m_pos( i_pos )
        {
        }

    public:

        template< typename w_type >
        inline LogSessionVarStream & operator<<( const w_type & i_rhs )
        {
            AT_Assert( m_pos < m_session.m_vars.size() );
            m_session.m_vars[m_pos].second.push_back
                ( m_session.m_mgr.NormalizeVar( i_rhs ) );
            return *this;
        }

    private:

        /* Unimplemented */
        LogSessionVarStream & operator=( const LogSessionVarStream & );

    };


    class LogManagerInputPolicy_Default;

    class LogManagerTimePolicy_NA;

    class LogManagerTaskIDPolicy_NA;

    template<
        class w_input_policy,
        class w_time_policy,
        class w_taskid_policy
    >
    class LogManagerTypes;

    class LogManagerLockPolicy_SingleThreaded;

    template<
        class w_types,
        class w_lock_policy,
        class w_caster
    >
    class LogManagerWriterPolicy_Default;


    template<
        class w_input_policy = LogManagerInputPolicy_Default,
        class w_time_policy = LogManagerTimePolicy_NA,
        class w_taskid_policy = LogManagerTaskIDPolicy_NA,
        class w_lock_policy = LogManagerLockPolicy_SingleThreaded,
        template<
            class w_types,
            class w_lock_policy,
            class w_caster
        > class w_writer_policy = LogManagerWriterPolicy_Default
    >
    class LogManager
      : public w_input_policy,
        public w_time_policy,
        public w_taskid_policy,
        public w_lock_policy,
        public w_writer_policy<
            LogManagerTypes<
                w_input_policy,
                w_time_policy,
                w_taskid_policy
            >,
            w_lock_policy,
            PolicyCaster<
                LogManager<
                    w_input_policy,
                    w_time_policy,
                    w_taskid_policy,
                    w_lock_policy,
                    w_writer_policy
                >
            >
        >
    {

    public:

        typedef  typename w_input_policy::t_loglevel_type  t_loglevel_type;
        typedef  typename w_input_policy::t_desc_type  t_desc_type;
        typedef  typename w_input_policy::t_normalized_var_type  t_normalized_var_type;
        typedef  typename w_input_policy::t_counter_type  t_counter_type;
        typedef  typename w_time_policy::t_time_type  t_time_type;
        typedef  typename w_taskid_policy::t_taskid_type  t_taskid_type;

        typedef
            LogManagerTypes<
                w_input_policy,
                w_time_policy,
                w_taskid_policy
            >
                t_types;

        typedef  typename t_types::t_var_list_type  t_var_list_type;
        typedef  typename t_types::t_vars_value_type  t_vars_value_type;
        typedef  typename t_types::t_vars_type  t_vars_type;

    private:

        typedef  typename w_lock_policy::t_lock_type  t_lock_type;

        typedef
            w_writer_policy<
                t_types,
                w_lock_policy,
                PolicyCaster< LogManager >
            >
                t_writer_policy;

        using w_taskid_policy::GetTaskID;
        using t_writer_policy::ReportEvent;

        volatile t_counter_type m_counter;

    public:

        LogManager()
          : m_counter( 0 )
        {
        }

        void LogEvent(
            const t_time_type & i_time,
            const char * i_file_name,
            int          i_line_num,
            const t_loglevel_type & i_severity,
            const t_desc_type & i_desc,
            const t_vars_type & i_vars
        )
        {
            t_taskid_type l_taskid = GetTaskID();
            t_lock_type l_lock( *this );
            t_counter_type l_event_num = m_counter++;
            ReportEvent
                ( l_event_num, i_time, l_taskid, i_file_name,
                  i_line_num, i_severity, i_desc, i_vars );
        }

    private:

        /* Unimplemented */
        LogManager( const LogManager & );
        LogManager & operator=( const LogManager & );

    };


    class LogManagerInputPolicy_Default
    {

    public:

        typedef  int  t_loglevel_type;
        typedef  const char *  t_desc_type;
        typedef  std::string  t_normalized_var_type;
        typedef  int  t_counter_type;

        template< typename w_type >
        static t_normalized_var_type NormalizeVar( const w_type & i_var )
        {
            std::ostringstream l_ss;
            l_ss << i_var;
            return l_ss.str();
        }

    protected:

        inline LogManagerInputPolicy_Default() {}
        inline ~LogManagerInputPolicy_Default() {}

    private:

        /* Unimplemented */
        LogManagerInputPolicy_Default
            ( const LogManagerInputPolicy_Default & );
        LogManagerInputPolicy_Default & operator=
            ( const LogManagerInputPolicy_Default & );

    };


    class LogManagerTimePolicy_NA
    {

    public:

        typedef  const char *  t_time_type;

        inline static t_time_type GetTime()
        {
            return "N/A";
        }

    protected:

        inline LogManagerTimePolicy_NA() {}
        inline ~LogManagerTimePolicy_NA() {}

    private:

        /* Unimplemented */
        LogManagerTimePolicy_NA( const LogManagerTimePolicy_NA & );
        LogManagerTimePolicy_NA & operator=
            ( const LogManagerTimePolicy_NA & );

    };


    class LogManagerTaskIDPolicy_NA
    {

    public:

        typedef  const char *  t_taskid_type;

        inline static t_taskid_type GetTaskID()
        {
            return "N/A";
        }

    protected:

        inline LogManagerTaskIDPolicy_NA() {}
        inline ~LogManagerTaskIDPolicy_NA() {}

    private:

        /* Unimplemented */
        LogManagerTaskIDPolicy_NA( const LogManagerTaskIDPolicy_NA & );
        LogManagerTaskIDPolicy_NA & operator=
            ( const LogManagerTaskIDPolicy_NA & );

    };


    template<
        class w_input_policy,
        class w_time_policy,
        class w_taskid_policy
    >
    class LogManagerTypes
    {

    public:

        typedef  typename w_input_policy::t_loglevel_type  t_loglevel_type;
        typedef  typename w_input_policy::t_desc_type  t_desc_type;
        typedef  typename w_input_policy::t_normalized_var_type  t_normalized_var_type;
        typedef  typename w_input_policy::t_counter_type  t_counter_type;
        typedef  typename w_time_policy::t_time_type  t_time_type;
        typedef  typename w_taskid_policy::t_taskid_type  t_taskid_type;

        typedef  std::vector< t_normalized_var_type >  t_var_list_type;

        typedef
            std::pair<
                t_desc_type,
                t_var_list_type
            >
                t_vars_value_type;

        typedef  std::vector< t_vars_value_type >  t_vars_type;

    private:

        /* Unimplemented */
        LogManagerTypes();
        LogManagerTypes( const LogManagerTypes & );
        ~LogManagerTypes();
        LogManagerTypes & operator=( const LogManagerTypes & );

    };


    class LogManagerLockPolicy_SingleThreaded
    {

    public:

        class t_lock_type
        {

        public:

            inline t_lock_type( LogManagerLockPolicy_SingleThreaded & io_mgr )
            {
            }

            inline ~t_lock_type() {}

        private:

            /* Unimplemented */
            t_lock_type( const t_lock_type & );
            t_lock_type & operator=( const t_lock_type & );

        };

    protected:

        inline LogManagerLockPolicy_SingleThreaded() {}
        inline ~LogManagerLockPolicy_SingleThreaded() {}

    private:

        /* Unimplemented */
        LogManagerLockPolicy_SingleThreaded
            ( const LogManagerLockPolicy_SingleThreaded & );
        LogManagerLockPolicy_SingleThreaded & operator=
            ( const LogManagerLockPolicy_SingleThreaded & );

    };


    template< template< class w_types > class w_writer_type >
    class LogManagerWriterPolicy_SingleWriter_SameThread
    {

    public:

        template<
            class w_types,
            class w_lock_policy,
            class w_caster
        >
        class t_template
        {

            typedef  w_writer_type< w_types >  t_writer_type;

            typedef  typename w_lock_policy::t_lock_type  t_lock_type;

            typedef  typename w_types::t_counter_type  t_counter_type;
            typedef  typename w_types::t_time_type  t_time_type;
            typedef  typename w_types::t_taskid_type  t_taskid_type;
            typedef  typename w_types::t_loglevel_type  t_loglevel_type;
            typedef  typename w_types::t_desc_type  t_desc_type;
            typedef  typename w_types::t_vars_type  t_vars_type;

            t_writer_type * volatile m_writer;

        protected:

            inline t_template()
              : m_writer( 0 )
            {
            }

            inline ~t_template()
            {
                AT_Assert( m_writer == 0 );
            }

        public:

            void RegisterWriter( t_writer_type & io_writer )
            {
                w_lock_policy & l_lock_policy =
                    PolicyCast< w_lock_policy, w_caster >::From( *this );
                t_lock_type l_lock( l_lock_policy );
                AT_Assert( m_writer == 0 );
                m_writer = &io_writer;
            }

            void UnregisterWriter( t_writer_type & io_writer )
            {
                w_lock_policy & l_lock_policy =
                    PolicyCast< w_lock_policy, w_caster >::From( *this );
                t_lock_type l_lock( l_lock_policy );
                AT_Assert( m_writer == &io_writer );
                m_writer = 0;
            }

        protected:

            inline void ReportEvent(
                const t_counter_type & i_event_num,
                const t_time_type & i_time,
                const t_taskid_type & i_taskid,
                const char *i_file_name,
                int         i_line_num,
                const t_loglevel_type & i_severity,
                const t_desc_type & i_desc,
                const t_vars_type & i_vars
            )
            {
                if ( m_writer != 0 )
                {
                    m_writer->ReportEvent
                        ( i_event_num, i_time, i_taskid, i_file_name,
                          i_line_num, i_severity, i_desc, i_vars );
                }
            }

        private:

            /* Unimplemented */
            t_template( const t_template & );
            t_template & operator=( const t_template & );

        };

    private:

        /* Unimplemented */
        LogManagerWriterPolicy_SingleWriter_SameThread();
        LogManagerWriterPolicy_SingleWriter_SameThread
            ( const LogManagerWriterPolicy_SingleWriter_SameThread & );
        ~LogManagerWriterPolicy_SingleWriter_SameThread();
        LogManagerWriterPolicy_SingleWriter_SameThread & operator=
            ( const LogManagerWriterPolicy_SingleWriter_SameThread & );

    };


    template< class w_types >
    class AbstractLogWriter;


    template<
        class w_types,
        class w_lock_policy,
        class w_caster
    >
    class LogManagerWriterPolicy_Default
      : public LogManagerWriterPolicy_SingleWriter_SameThread<
            AbstractLogWriter
        >::t_template<
            w_types,
            w_lock_policy,
            w_caster
        >
    {

    protected:

        inline LogManagerWriterPolicy_Default() {}
        inline ~LogManagerWriterPolicy_Default() {}

    private:

        /* Unimplemented */
        LogManagerWriterPolicy_Default( const LogManagerWriterPolicy_Default & );
        LogManagerWriterPolicy_Default & operator=( const LogManagerWriterPolicy_Default & );

    };


    template< class w_types >
    class AbstractLogWriter
    {

    protected:

        typedef  typename w_types::t_counter_type  t_counter_type;
        typedef  typename w_types::t_time_type  t_time_type;
        typedef  typename w_types::t_taskid_type  t_taskid_type;
        typedef  typename w_types::t_loglevel_type  t_loglevel_type;
        typedef  typename w_types::t_desc_type  t_desc_type;
        typedef  typename w_types::t_vars_type  t_vars_type;

    public:

        virtual void ReportEvent(
            const t_counter_type & i_event_num,
            const t_time_type & i_time,
            const t_taskid_type & i_taskid,
            const char * i_file_name,
            int          i_line_num,
            const t_loglevel_type & i_severity,
            const t_desc_type & i_desc,
            const t_vars_type & i_vars
        ) = 0;

    protected:

        inline AbstractLogWriter() {}
        inline virtual ~AbstractLogWriter() {}

    private:

        /* Unimplemented */
        AbstractLogWriter( const AbstractLogWriter & );
        AbstractLogWriter & operator=( const AbstractLogWriter & );

    };


    template<
        class w_mgr_type,
        class w_output_policy,
        class w_caster
    >
    class LogWriterFormatPolicy_Default;


    class LogWriterCantWriteException {};


    template<
        class w_mgr_type,
        class w_output_policy,
        template<
            class w_mgr_type,
            class w_output_policy,
            class w_caster
        > class w_format_policy = LogWriterFormatPolicy_Default
    >
    class LogWriter
      : public AbstractLogWriter< typename w_mgr_type::t_types >,
        public w_output_policy,
        public w_format_policy<
            w_mgr_type,
            w_output_policy,
            PolicyCaster<
                LogWriter<
                    w_mgr_type,
                    w_output_policy,
                    w_format_policy
                >
            >
        >
    {

        typedef
            w_format_policy<
                w_mgr_type,
                w_output_policy,
                PolicyCaster< LogWriter >
            >
                t_format_policy;

        typedef  typename w_mgr_type::t_counter_type  t_counter_type;
        typedef  typename w_mgr_type::t_time_type  t_time_type;
        typedef  typename w_mgr_type::t_taskid_type  t_taskid_type;
        typedef  typename w_mgr_type::t_loglevel_type  t_loglevel_type;
        typedef  typename w_mgr_type::t_desc_type  t_desc_type;
        typedef  typename w_mgr_type::t_vars_type  t_vars_type;

        using t_format_policy::WriteFormattedEvent;
        using w_output_policy::Flush;

        w_mgr_type * m_mgr;

    public:

        LogWriter()
          : m_mgr( 0 )
        {
        }

        void Register( w_mgr_type & io_mgr )
        {
            AT_Assert( m_mgr == 0 );
            m_mgr = & io_mgr;
            m_mgr->RegisterWriter( *this );
        }

        void Unregister()
        {
            AT_Assert( m_mgr != 0 );
            m_mgr->UnregisterWriter( *this );
            m_mgr = 0;
        }

        explicit LogWriter( w_mgr_type & io_mgr )
          : m_mgr( 0 )
        {
            Register( io_mgr );
        }

        ~LogWriter()
        {
            if ( m_mgr != 0 )
            {
                Unregister();
            }
        }

    private:

        virtual void ReportEvent(
            const t_counter_type & i_event_num,
            const t_time_type & i_time,
            const t_taskid_type & i_taskid,
            const char * i_file_name,
            int          i_line_num,
            const t_loglevel_type & i_severity,
            const t_desc_type & i_desc,
            const t_vars_type & i_vars
        )
        {
            try
            {
                WriteFormattedEvent
                    ( i_event_num, i_time, i_taskid, i_file_name,
                      i_line_num, i_severity, i_desc, i_vars );
                Flush();
            }
            catch ( LogWriterCantWriteException )
            {
                /*
                 *  Panic because we need to log an event, and can't write to
                 *  to the log output.  To avoid this happening, use an output
                 *  policy for your LogWriter that is guaranteed not to fail.
                 *  You can use LogWriterOutputPolicy_PrimaryAndAlternate to
                 *  have a fallible primary policy, and an "infallable"
                 *  alternate one (such as LogWriterOutPolicy_Cerr).
                 */
                AT_Assert( 0 );
            }
        }

        /* Unimplemented */
        LogWriter( const LogWriter & );
        LogWriter & operator=( const LogWriter & );

    };


    template< class w_output_policy_type >
    class LogWriterOutputPolicy_Stream
    {

        w_output_policy_type & m_output_policy;

    public:

        inline explicit LogWriterOutputPolicy_Stream(
            w_output_policy_type & io_output_policy
        )
          : m_output_policy( io_output_policy )
        {
        }

        template< typename w_rhs_type >
        inline LogWriterOutputPolicy_Stream & operator<<(
            const w_rhs_type & i_rhs
        )
        {
            m_output_policy.Write( i_rhs );
            return *this;
        }

    };


    class LogWriterOutputPolicy_DropOnFloor
      : public ThisInBaseInitializerHack< LogWriterOutputPolicy_DropOnFloor >
    {

    public:

        typedef
            LogWriterOutputPolicy_Stream< LogWriterOutputPolicy_DropOnFloor >
                t_ostream_type;

    private:

        t_ostream_type m_ostream;

    protected:

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_DropOnFloor
        >::This;

        inline LogWriterOutputPolicy_DropOnFloor()
          : m_ostream( This() )
        {
        }

        inline ~LogWriterOutputPolicy_DropOnFloor() {}

    public:

        template< typename w_type >
        inline void Write( const w_type & )
        {
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        inline void Flush()
        {
        }

    private:

        /* Unimplemented */
        LogWriterOutputPolicy_DropOnFloor(
            const LogWriterOutputPolicy_DropOnFloor &
        );
        LogWriterOutputPolicy_DropOnFloor & operator=(
            const LogWriterOutputPolicy_DropOnFloor &
        );

    };


    template<
        class w_primary_output_policy,
        class w_alternate_output_policy
    >
    class LogWriterOutputPolicy_PrimaryAndAlternate
      : public w_primary_output_policy,
        public w_alternate_output_policy,
        public ThisInBaseInitializerHack<
            LogWriterOutputPolicy_PrimaryAndAlternate<
                w_primary_output_policy,
                w_alternate_output_policy
            >
        >
    {

    public:

        typedef
            LogWriterOutputPolicy_Stream<
                LogWriterOutputPolicy_PrimaryAndAlternate
            >
                t_ostream_type;

        typedef
            typename w_primary_output_policy::t_ostream_type
                t_primary_ostream_type;

        typedef
            typename w_alternate_output_policy::t_ostream_type
                t_alternate_ostream_type;

    private:

        t_ostream_type m_ostream;
        std::ostringstream m_buffer;

    protected:

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_PrimaryAndAlternate
        >::This;

        inline LogWriterOutputPolicy_PrimaryAndAlternate()
          : m_ostream( This() )
        {
        }

        inline ~LogWriterOutputPolicy_PrimaryAndAlternate() {}

    public:

        template< typename w_type >
        inline void Write( const w_type & i_value )
        {
            m_buffer << i_value;
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        void Flush()
        {
            std::string l_string = m_buffer.str();
            m_buffer.str( std::string() );
            try
            {
                t_primary_ostream_type & l_primary_ostream = PrimaryOStream();
                l_primary_ostream << l_string;
                w_primary_output_policy::Flush();
            }
            catch( LogWriterCantWriteException )
            {
                t_alternate_ostream_type & l_alternate_ostream = AlternateOStream();
                l_alternate_ostream << l_string;
                w_alternate_output_policy::Flush();
            }
        }

        inline t_primary_ostream_type & PrimaryOStream()
        {
            return w_primary_output_policy::OStream();
        }

        inline t_alternate_ostream_type & AlternateOStream()
        {
            return w_alternate_output_policy::OStream();
        }

    private:

        /* Unimplemented. */
        LogWriterOutputPolicy_PrimaryAndAlternate(
            const LogWriterOutputPolicy_PrimaryAndAlternate &
        );
        LogWriterOutputPolicy_PrimaryAndAlternate & operator=(
            const LogWriterOutputPolicy_PrimaryAndAlternate &
        );

    };


    template<
        class w_first_output_policy,
        class w_second_output_policy
    >
    class LogWriterOutputPolicy_Splitter
      : public w_first_output_policy,
        public w_second_output_policy,
        public ThisInBaseInitializerHack<
            LogWriterOutputPolicy_Splitter<
                w_first_output_policy,
                w_second_output_policy
            >
        >
    {

    public:

        typedef
            typename w_first_output_policy::t_ostream_type
                t_first_ostream_type;

        typedef
            typename w_second_output_policy::t_ostream_type
                t_second_ostream_type;

        typedef
            LogWriterOutputPolicy_Stream<
                LogWriterOutputPolicy_Splitter
            >
                t_ostream_type;

    private:

        t_ostream_type m_ostream;

    protected:

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_Splitter
        >::This;

        inline LogWriterOutputPolicy_Splitter()
          : m_ostream( This() )
        {
        }

        inline ~LogWriterOutputPolicy_Splitter()
        {
        }

    public:

        template< typename w_type >
        inline void Write( const w_type & i_value )
        {
            try
            {
                w_first_output_policy::OStream() << i_value;
                w_second_output_policy::OStream() << i_value;
            }
            catch ( LogWriterCantWriteException )
            {
                /*
                 *  Both w_first_output_policy and w_second_output_policy
                 *  should be infallable.
                 */
                AT_Assert( 0 );
            }
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        inline void Flush()
        {
            try
            {
                w_first_output_policy::Flush();
                w_second_output_policy::Flush();
            }
            catch ( LogWriterCantWriteException )
            {
                /*
                 *  Both w_first_output_policy and w_second_output_policy
                 *  should be infallable.
                 */
                AT_Assert( 0 );
            }
        }

        inline t_first_ostream_type & FirstOStream()
        {
            return w_first_output_policy::OStream();
        }

        inline t_second_ostream_type & SecondOStream()
        {
            return w_second_output_policy::OStream();
        }

    private:

        /* Unimplemented. */
        LogWriterOutputPolicy_Splitter(
            const LogWriterOutputPolicy_Splitter &
        );
        LogWriterOutputPolicy_Splitter & operator=(
            const LogWriterOutputPolicy_Splitter &
        );

    };


    template<
        class w_primary_output_policy,
        class w_failsafe_output_policy
    >
    class LogWriterOutputPolicy_StoreBacklogIfCantWrite
      : public w_primary_output_policy,
        public w_failsafe_output_policy,
        public ThisInBaseInitializerHack<
            LogWriterOutputPolicy_StoreBacklogIfCantWrite<
                w_primary_output_policy,
                w_failsafe_output_policy
            >
        >
    {

    public:

        typedef
            typename w_primary_output_policy::t_ostream_type
                t_primary_ostream_type;

        typedef
            typename w_failsafe_output_policy::t_ostream_type
                t_failsafe_ostream_type;

        typedef
            LogWriterOutputPolicy_Stream<
                LogWriterOutputPolicy_StoreBacklogIfCantWrite
            >
                t_ostream_type;

        typedef  std::list< std::string >  t_backlog_type;

    private:

        t_ostream_type m_ostream;
        std::ostringstream m_buffer;
        t_backlog_type m_backlog;

    protected:

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_StoreBacklogIfCantWrite
        >::This;

        inline LogWriterOutputPolicy_StoreBacklogIfCantWrite()
          : m_ostream( This() )
        {
        }

        inline ~LogWriterOutputPolicy_StoreBacklogIfCantWrite()
        {
            TryFlushBacklog();
            DumpToFailsafe();
        }

    public:

        template< typename w_type >
        inline void Write( const w_type & i_value )
        {
            m_buffer << i_value;
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        void Flush()
        {
            m_backlog.push_back( m_buffer.str() );
            m_buffer.str( std::string() );
            TryFlushBacklog();
        }

        void TryFlushBacklog()
        {
            try
            {
                while ( ! m_backlog.empty() )
                {
                    t_primary_ostream_type & l_primary_ostream = PrimaryOStream();
                    l_primary_ostream << m_backlog.front();
                    w_primary_output_policy::Flush();
                    m_backlog.pop_front();
                }
            }
            catch( LogWriterCantWriteException )
            {
            }
        }

        void DumpToFailsafe()
        {
            while ( ! m_backlog.empty() )
            {
                t_failsafe_ostream_type & l_failsafe_ostream = FailsafeOStream();
                l_failsafe_ostream << m_backlog.front();
                w_failsafe_output_policy::Flush();
                m_backlog.pop_front();
            }            
        }

        inline t_primary_ostream_type & PrimaryOStream()
        {
            return w_primary_output_policy::OStream();
        }

        inline t_failsafe_ostream_type & FailsafeOStream()
        {
            return w_failsafe_output_policy::OStream();
        }

    private:

        /* Unimplemented. */
        LogWriterOutputPolicy_StoreBacklogIfCantWrite(
            const LogWriterOutputPolicy_StoreBacklogIfCantWrite &
        );
        LogWriterOutputPolicy_StoreBacklogIfCantWrite & operator=(
            const LogWriterOutputPolicy_StoreBacklogIfCantWrite &
        );

    };


    template<
        class w_inner_output_policy
    >
    class LogWriterOutputPolicy_Paginator
      : public w_inner_output_policy,
        public ThisInBaseInitializerHack<
            LogWriterOutputPolicy_Paginator<
                w_inner_output_policy
            >
        >
    {

    public:

        typedef
            typename w_inner_output_policy::t_ostream_type
                t_inner_ostream_type;

        typedef
            LogWriterOutputPolicy_Stream< LogWriterOutputPolicy_Paginator >
                t_ostream_type;

    private:

        t_ostream_type m_ostream;
        std::ostringstream m_buffer;
        size_t m_page_size;
        size_t m_bytes_written;

    protected:

        using ThisInBaseInitializerHack<
            LogWriterOutputPolicy_Paginator
        >::This;

        inline LogWriterOutputPolicy_Paginator()
          : m_ostream( This() ),
            m_page_size( 0 ),
            m_bytes_written( 0 )
        {
        }

        inline ~LogWriterOutputPolicy_Paginator() {}

    public:

        template< typename w_type >
        inline void Write( const w_type & i_value )
        {
            if ( m_page_size == 0 )
            {
                throw LogWriterCantWriteException();
            }
            m_buffer << i_value;
        }

        inline t_ostream_type & OStream()
        {
            return m_ostream;
        }

        void Flush()
        {
            std::string l_string = m_buffer.str();
            m_buffer.str( std::string() );

            if ( m_page_size == 0 )
            {
                throw LogWriterCantWriteException();
            }

            t_inner_ostream_type & l_inner_ostream = InnerOStream();
            l_inner_ostream << l_string;
            w_inner_output_policy::Flush();
            m_bytes_written += l_string.size();
            AT_Assert( m_page_size > 0 );
            if ( m_bytes_written >= m_page_size )
            {
                w_inner_output_policy::NewPage();
                m_bytes_written = 0;
            }
        }

        inline void SetPageSize( size_t i_page_size )
        {
            m_page_size = i_page_size;
        }

        inline void SetBytesWritten( size_t i_bytes_written )
        {
            m_bytes_written = i_bytes_written;
        }

        inline t_inner_ostream_type & InnerOStream()
        {
            return w_inner_output_policy::OStream();
        }

    private:

        /* Unimplemented. */
        LogWriterOutputPolicy_Paginator(
            const LogWriterOutputPolicy_Paginator &
        );
        LogWriterOutputPolicy_Paginator & operator=(
            const LogWriterOutputPolicy_Paginator &
        );

    };


    template< class w_time_format_policy >
    class LogWriterFormatPolicy_DefaultFormat
    {

    public:

        template<
            class w_mgr_type,
            class w_output_policy,
            class w_caster
        >
        class t_template
          : public w_time_format_policy
        {

            typedef  typename w_mgr_type::t_counter_type  t_counter_type;
            typedef  typename w_mgr_type::t_time_type  t_time_type;
            typedef  typename w_mgr_type::t_taskid_type  t_taskid_type;
            typedef  typename w_mgr_type::t_loglevel_type  t_loglevel_type;
            typedef  typename w_mgr_type::t_desc_type  t_desc_type;
            typedef  typename w_mgr_type::t_vars_type  t_vars_type;
            typedef  typename w_mgr_type::t_var_list_type  t_var_list_type;

            typedef  typename w_output_policy::t_ostream_type  t_ostream_type;

            using w_time_format_policy::FormatTime;

        protected:

            void WriteFormattedEvent(
                const t_counter_type & i_event_num,
                const t_time_type & i_time,
                const t_taskid_type & i_taskid,
                const char * i_file_name,
                int          i_line_num,
                const t_loglevel_type & i_severity,
                const t_desc_type & i_desc,
                const t_vars_type & i_vars
            )
            {
                t_ostream_type & l_ostream =
                    PolicyCast< w_output_policy, w_caster >::From( *this ).OStream();

                l_ostream <<
                    "EV" << i_event_num <<
                    " " << FormatTime( i_time ) <<
                    " TID" << i_taskid <<
                    " SV" << i_severity <<
                    " DX " << i_desc <<
                    " FN " << i_file_name << ":" << i_line_num << "\n";

                typename t_vars_type::const_iterator i = i_vars.begin();
                typename t_vars_type::const_iterator end = i_vars.end();

                if ( i != end )
                {
                    l_ostream << "    ";
                    for ( ; i != end; ++i )
                    {
                        l_ostream << "LG " << i->first << " ";
                        for ( typename t_var_list_type::const_iterator
                                    j = i->second.begin(),
                                    jend = i->second.end();
                                j != jend;
                                ++j
                            )
                        {
                            l_ostream << " VL " << *j << " ";
                        }
                    }
                    l_ostream << "\n";
                }
                
            }

            t_template() {}
            ~t_template() {}

        private:

            /* Unimplemented */
            t_template( const t_template & );
            t_template & operator=( const t_template & );

        };

    private:

        /* Unimplemented */
        LogWriterFormatPolicy_DefaultFormat();
        LogWriterFormatPolicy_DefaultFormat
            ( const LogWriterFormatPolicy_DefaultFormat & );
        ~LogWriterFormatPolicy_DefaultFormat();
        LogWriterFormatPolicy_DefaultFormat & operator=
            ( const LogWriterFormatPolicy_DefaultFormat & );

    };


    class LogWriterTimeFormatPolicy_NA
    {

    public:

        typedef  const char *  t_formatted_time_type;

        template< class w_type >
        inline static t_formatted_time_type FormatTime( w_type i_time )
        {
            return "N/A";
        }

    protected:

        inline LogWriterTimeFormatPolicy_NA() {}
        inline ~LogWriterTimeFormatPolicy_NA() {}

    private:

        /* Unimplemented */
        LogWriterTimeFormatPolicy_NA( const LogWriterTimeFormatPolicy_NA & );
        LogWriterTimeFormatPolicy_NA & operator=( const LogWriterTimeFormatPolicy_NA & );

    };


    template<
        class w_mgr_type,
        class w_output_policy,
        class w_caster
    >
    class LogWriterFormatPolicy_Default
      : public LogWriterFormatPolicy_DefaultFormat<
            LogWriterTimeFormatPolicy_NA
        >::t_template<
            w_mgr_type,
            w_output_policy,
            w_caster
        >
    {

    protected:

        inline LogWriterFormatPolicy_Default() {}
        inline ~LogWriterFormatPolicy_Default() {}

    private:

        /* Unimplemented */
        LogWriterFormatPolicy_Default( const LogWriterFormatPolicy_Default & );
        LogWriterFormatPolicy_Default & operator=( const LogWriterFormatPolicy_Default & );

    };


}


#define  AT_STARTLOG( x_mgr, x_pref, x_severity, x_desc )  \
         if ( ( (x_severity) & (AT_GLOBAL_LOGLEVEL) ) != 0 )  \
         {  \
             if ( ( ( (x_severity) & (AT_GLOBAL_LOGLEVEL) ) & (x_pref) ) != 0 )  \
             {  \
                 at::LogSession< AT_LOG_MANAGER_TYPE > at_log  \
                     ( x_mgr, __FILE__, __LINE__, x_severity, x_desc );

#define  AT_ENDLOG  \
             }  \
         }  \
         else {}

#define  AT_LOG( x_mgr, x_pref, x_severity, x_desc )  \
         AT_STARTLOG( x_mgr, x_pref, x_severity, x_desc ) \
         AT_ENDLOG


#ifndef  AT_LOG_MANAGER_TYPE
#define  AT_LOG_MANAGER_TYPE  at::LogManager<>
#endif


#endif
