
//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_translate_c.h
 *
 */

#ifndef x_at_translate_c_h_x
#define x_at_translate_c_h_x 1

#include "at_exports.h"

// Austria namespace
namespace at
{
    

// ======== TranslateElementC =====================================
/**
 * TranslateElementC defines a translation element.
 *
 */

struct AUSTRIA_EXPORT TranslateElementC
{
    /**
     * m_size is the numbr of elements in m_data that are significant.
     */
    unsigned char       m_size;

    /**
     * m_data is the string to use to output this character.
     */
    char                m_data[ 3 ];
};


// ======== g_TranslateElement ========================================
/**
 * g_TranslateElement is an array that allows indexing to a C safe
 * string representation for a character.
 */

AUSTRIA_EXPORT extern const TranslateElementC g_TranslateElement[256];

} // namespace

#endif // x_at_translate_c_h_x
