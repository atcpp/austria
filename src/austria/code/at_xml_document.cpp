//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_document.cpp
 *
 * \author Guido Gherardi
 *
 */

#include "at_xml_document.h"
#include "at_exception.h"

#define AT_ThrowXmlHierarchyRequestErr(msg) AT_ThrowDerivation( at::XmlHierarchyRequestErr, msg )
#define AT_ThrowXmlWrongDocumentErr(msg)    AT_ThrowDerivation( at::XmlWrongDocumentErr, msg )

// Austria namespace
namespace at
{
    std::string XmlDocument::Name() const
    {
        static std::string l_name( "#document" );
        return l_name;
    }

    unsigned short XmlDocument::Type() const
    {
        return DOCUMENT_NODE;
    }

    void XmlDocument::CheckHierarchyRequest( PtrView< const XmlNode* > i_newChild ) const
    {
        if ( i_newChild->OwnerDocument().Get() != this )
        {
            AT_ThrowXmlWrongDocumentErr( "i_newChild was not created by this document" );
        }
        switch ( i_newChild->Type() )
        {
        case ELEMENT_NODE:
            if ( m_rootElement != 0 && m_rootElement != i_newChild )
            {
                AT_ThrowXmlHierarchyRequestErr( "an XmlDocument can only have one child of type ELEMENT_NODE" );
            }
        case PROCESSING_INSTRUCTION_NODE: case COMMENT_NODE: case DOCUMENT_TYPE_NODE:
            break;
        default:
            AT_ThrowXmlHierarchyRequestErr( "i_newChild is of a type that is not allowed as a document child" );
        }
    }

    PtrView< XmlNode* > XmlDocument::InsertBefore( PtrDelegate< XmlNode* > i_newChild,
                                                   PtrView< XmlNode* >     i_refChild )
    {
        if ( i_newChild = XmlComposite::InsertBefore( i_newChild, i_refChild ) )
        {
            if ( PtrView< XmlElement* > l_newRoot = i_newChild->AsElement() )
            {
                m_rootElement = l_newRoot;
            }
        }
        return i_newChild;
    }

    PtrView< XmlNode* > XmlDocument::AppendChild( PtrDelegate< XmlNode* > i_newChild )
    {
        if ( i_newChild = XmlComposite::AppendChild( i_newChild ) )
        {
            if ( PtrView< XmlElement* > l_newRoot = i_newChild->AsElement() )
            {
                m_rootElement = l_newRoot;
            }
        }
        return i_newChild;
    }

    PtrDelegate< XmlNode* > XmlDocument::RemoveChild( PtrDelegate< XmlNode* > i_oldChild )
    {
        if ( ( i_oldChild = XmlComposite::RemoveChild( i_oldChild ) ) == m_rootElement )
        {
            m_rootElement = 0;
        }
        return i_oldChild;
    }

    PtrDelegate< XmlNode* > XmlDocument::ReplaceChild( PtrDelegate< XmlNode* > i_newChild,
                                                       PtrDelegate< XmlNode* > i_oldChild )
    {
        PtrDelegate< XmlElement* > l_newRootElement = i_newChild->AsElement();
        PtrDelegate< XmlElement* > l_oldRootElement = m_rootElement;
        if ( i_oldChild == m_rootElement )
        {
            m_rootElement = 0;
        }
        try
        {
            i_oldChild = XmlComposite::ReplaceChild( i_newChild, i_oldChild ); // invalidates i_newChild
        }
        catch ( Exception& e )
        {
            m_rootElement = l_oldRootElement;
            throw e;
        }
        if ( l_newRootElement )
        {
            m_rootElement = l_newRootElement;
        }
        return i_oldChild;
    }

    PtrDelegate< XmlNode* > XmlDocument::Clone( bool i_deep ) const
    {
        XmlDocument* l_clone = new XmlDocument();
        if ( m_rootElement && i_deep )
        {
            l_clone->m_rootElement = dynamic_cast< XmlElement* >( m_rootElement->Clone( true ).Get() );
        }
        return l_clone;
    }

    PtrView< XmlDocument* > XmlDocument::AsDocument()
    {
        return PtrView< XmlDocument* >( this );
    }

    PtrView< const XmlDocument* > XmlDocument::AsDocument() const
    {
        return PtrView< const XmlDocument* >( this );
    }

    void XmlDocument::Accept( XmlNodeVisitor& i_op )
    {
        i_op.VisitDocument( this);
    }

    void XmlDocument::Accept( XmlNodeVisitor& i_op ) const
    {
        i_op.VisitDocument( this );
    }
};
