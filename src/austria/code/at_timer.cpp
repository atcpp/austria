#include "at_factory.h"
#include "at_lifetime.h"
#include "at_thread.h"
#include "at_timer.h"
#include "at_twinmt.h"
#include "at_types.h"

#include <list>
#include <map>

namespace at_timer
{

    using namespace at;

    class TimerService_Basic;

    class TimerServiceAideImpl_Basic;

    class TimerService_Basic
      : public TimerService,
        public Task
    {

        friend class TimerServiceAideImpl_Basic;

    public:

        /*
         *  The event queue is a map, mapping TimeStamp's to lists of aide
         *  pointers.  We use a map of lists rather than a multimap so that we
         *  can give priority to earlier-registered events over later-
         *  registered ones, in order to avoid starvation when the timer
         *  service starts getting overloaded.
         */

        typedef
            std::list< TimerServiceAideImpl_Basic * >
                t_event_queue_list_type;

        typedef
            t_event_queue_list_type::iterator
                t_event_queue_list_iterator;

        typedef
            std::map< TimeStamp, t_event_queue_list_type >
                t_event_queue_type;

        typedef  t_event_queue_type::iterator  t_event_queue_iterator;

        struct t_event_queue_pointer
        {
            bool m_is_null;
            t_event_queue_iterator m_queue_iterator;
            t_event_queue_list_iterator m_list_iterator;

        public:

            inline t_event_queue_pointer()
              : m_is_null( true )
            {
            }

            inline t_event_queue_pointer(
                const t_event_queue_iterator & i_queue_iterator,
                const t_event_queue_list_iterator & i_list_iterator
            )
              : m_is_null( false ),
                m_queue_iterator( i_queue_iterator ),
                m_list_iterator( i_list_iterator )
            {
            }

            operator bool()
            {
                return ! m_is_null;
            }

        };


    private:

        /*
         *  IMPORTANT! - Always lock *m_mutex before m_cm to avoid deadlock.
         */

        Ptr< MutexRefCount * > m_mutex;

        ConditionalMutex m_cm;

        volatile bool m_shutdown;

        t_event_queue_type m_event_queue;

    public:

        TimerService_Basic()
          : m_mutex( new MutexRefCount( Mutex::Recursive ) ),
            m_cm( Mutex::NonRecursive ),
            m_shutdown( false )
        {
            Start();
        }

        virtual ~TimerService_Basic()
        {

            /*
             *  The timer service should not be getting destructed while there
             *  are events in the queue.
             */
            AT_Assert( m_event_queue.empty() );

            {
                Lock< Mutex > l_lock1( *m_mutex );
                Lock< ConditionalMutex > l_lock2( m_cm );
                m_shutdown = true;
                m_cm.Post();
            }
            Wait();

        }

        virtual void StartTimer(
            LeadTwinMT< TimerLeadIf, AideTwinMT< TimerAideIf > > & i_lead,
            const TimeStamp & i_requested_time
        );

        virtual void Shutdown();

    private:

        virtual void Work();

    };


    class TimerServiceAideImpl_Basic
      : public AideTwinMT_Basic< TimerLeadIf, TimerAideIf >
    {

        friend class TimerService_Basic;

        Ptr< TimerService_Basic * > m_ts;

        /*
         *  This marks our position in the event queue, so that we can
         *  efficiently delete ourself from it.
         */
        TimerService_Basic::t_event_queue_pointer m_queue_pointer;

        virtual bool GetNextEventTime(
            TimeStamp & o_next_event_time
        ) {
            if ( m_queue_pointer )
            {
                o_next_event_time = m_queue_pointer.m_queue_iterator->first;
                return true;
            }
            return false;
        }

    public:

        inline TimerServiceAideImpl_Basic(
            PtrDelegate< TimerService_Basic * > io_ts
        )
          : m_ts( io_ts )
        {
        }

        void AppAideCloseNotify( TwinTraits::TwinCode i_completion_code );

    };

    void TimerService_Basic::Shutdown()
    {
        m_shutdown = true;

        Lock< Mutex > l_lock1( *m_mutex );
        Lock< ConditionalMutex > l_lock2( m_cm );
            
        while ( ! m_event_queue.empty() )
        {
            t_event_queue_iterator l_iter = m_event_queue.begin();

            t_event_queue_list_iterator l_liter = l_iter->second.begin();

            if ( l_liter == l_iter->second.end() )
            {
                m_event_queue.erase( l_iter );
            }
            else
            {
                ( * l_liter )->AideCancel();
            }
        }
    }

    void TimerService_Basic::StartTimer(
        LeadTwinMT< TimerLeadIf, AideTwinMT< TimerAideIf > > & i_lead,
        const TimeStamp & i_requested_time
    )
    {

        if ( m_shutdown )
        {
            AT_ThrowDerivation( TimerSericeUnavailable, "Timer service is shutdown" );
        }

        /*
         *  If the requested time is in the past, register the event for
         *  the current time instead.  This prevents a newly-registered
         *  event from having priority over already-overdue events already
         *  in the queue.
         */
        TimeStamp l_priority = ThreadSystemTime();
        if ( l_priority < i_requested_time )
        {
            l_priority = i_requested_time;
        }


        TimerServiceAideImpl_Basic * l_aide =
            new TimerServiceAideImpl_Basic( this );

        Lock< Mutex > l_lock( *m_mutex );

        l_aide->AideAssociate( &i_lead, m_mutex );


        /*
         *  Add a slot in the queue for the given time, if it isn't there
         *  already.  After this, l_queue_iter will point to the event
         *  list for the given time, which will be empty if we just added
         *  it, non-empty otherwise.
         */
        t_event_queue_iterator l_queue_iter =
            m_event_queue.insert(
                t_event_queue_type::value_type(
                    l_priority,
                    t_event_queue_list_type()
                )
            ).first;


        /*
         *  Add the new event to the *end* of the event list for the given
         *  time.  (Since we process them from the front, this gives
         *  priority to earlier-registered events over later-registered
         *  ones, when the requested time is the same.)
         */
        l_queue_iter->second.push_back( &*l_aide );


        /*
         *  Save a pointer into the queue in the aide, so it can delete itself
         *  from it later.
         */
        l_aide->m_queue_pointer =
            t_event_queue_pointer(
                l_queue_iter,
                --t_event_queue_list_iterator(
                      l_queue_iter->second.end()
                  )
            );


        /*
         *  Wake up the timer service thread, since it may not currently
         *  be scheduled to wake up until after the time for the event we
         *  just added.
         *
         *  IMPORTANT! - Don't lock m_cm again if we already have it locked
         *  (which we will if we were called from the timer service thread
         *  (i.e. inside a call to Event() )).
         */
        if ( Task::GetSelfId() == GetThisId() )
        {
            m_cm.Post();
        }
        else
        {
            Lock< ConditionalMutex > l_cm_lock( m_cm );
            m_cm.Post();
        }

    }


    void TimerService_Basic::Work()
    {

        for (;;)
        {

            bool l_queue_empty;
            TimeStamp l_requested_time;

            {


                /*
                 *  On shutdown, the client thread locks *m_mutex, sets the
                 *  m_shutdown flag, and waits for this thread to die (while
                 *  still holding *m_mutex).  If we try to lock *m_mutex
                 *  normally here, it will be a deadlock.  So we spin until
                 *  either we get the lock, or m_shutdown is set.
                 */
                TryLock< Mutex > l_trylock( *m_mutex, m_shutdown );
                if ( ! l_trylock.IsAcquired() )
                {
                    AT_Assert( m_shutdown );
                    return;
                }


                m_cm.Lock();

                l_queue_empty = m_event_queue.empty();
                if ( ! l_queue_empty )
                {

                    t_event_queue_iterator l_queue_begin =
                        m_event_queue.begin();

                    l_requested_time = l_queue_begin->first;

                    TimeStamp l_current_time = ThreadSystemTime();
                    if ( l_current_time >= l_requested_time )
                    {

                        t_event_queue_list_type & l_queue_list =
                            l_queue_begin->second;


                        /*
                         *  If there's a slot in the queue for a given time,
                         *  there should be at least one event in the list for
                         *  that time.
                         */
                        AT_Assert( ! l_queue_list.empty() );


                        /*
                         *  Do the server at the *front* of the list.  (This
                         *  gives priority to earlier-registered events over
                         *  later-registered ones, when the requested time is
                         *  the same.)
                         */
                        TimerServiceAideImpl_Basic & l_aide =
                            **( l_queue_list.begin() );


                        /*
                         *  Remove the server from the queue.
                         */
                        l_queue_list.pop_front();
                        if ( l_queue_list.empty() )
                        {
                            m_event_queue.erase( l_queue_begin );
                        }


                        /*
                         *  Null out the aide's pointer into the queue, since
                         *  we just invalidated it.
                         */
                        l_aide.m_queue_pointer = t_event_queue_pointer();


                        bool l_reschedule;
                        bool l_lead_exists =
                            l_aide.CallLead().ReturnCall(
                                l_reschedule,
                                &TimerLeadIf::Event,
                                l_current_time,
                                l_requested_time
                            );


                        /*
                         *  The lead should still exist.  It couldn't have gone
                         *  away while we were holding the lock on *m_mutex,
                         *  and if it had gone away before that, we would never
                         *  have seen it in the queue.
                         */
                        AT_Assert( l_lead_exists );


                        if ( l_reschedule )
                        {

                            /*
                             *  The client re-scheduled.  Put the aide back
                             *  in the queue at the new time.
                             */

                            /*
                             *  If the requested time is in the past, register
                             *  the event for the current time instead.  This
                             *  prevents a newly-rescheduled event from having
                             *  priority over already-overdue events already
                             *  in the queue.
                             */
                            l_current_time = ThreadSystemTime();
                            if ( l_requested_time < l_current_time )
                            {
                                l_requested_time = l_current_time;
                            }


                            /*
                             *  Add a slot in the queue for the given time, if
                             *  it isn't there already.  After this,
                             *  l_queue_iter will point to the event list for
                             *  the given time, which will be empty if we just
                             *  added it, non-empty otherwise.
                             */
                            t_event_queue_iterator l_queue_iter =
                                m_event_queue.insert(
                                    t_event_queue_type::value_type(
                                        l_requested_time,
                                        t_event_queue_list_type()
                                    )
                                ).first;


                            /*
                             *  Add the rescheduled event to the *end* of the
                             *  event list for the given time.  (Since we
                             *  process them from the front, this gives
                             *  priority to earlier-registered events over
                             *  later-registered ones, when the requested time
                             *  is the same.)
                             */
                            l_queue_iter->second.push_back( &l_aide );


                            /*
                             *  Save a pointer into the queue in the aide, so
                             *  it can delete itself from it later.
                             */
                            l_aide.m_queue_pointer =
                                t_event_queue_pointer(
                                    l_queue_iter,
                                    --t_event_queue_list_iterator(
                                          l_queue_iter->second.end()
                                      )
                                );

                        }


                        /*
                         *  Update l_queue_empty and l_requested_time, while
                         *  we're still holding *m_mutex.
                         */
                        l_queue_empty = m_event_queue.empty();
                        if ( ! l_queue_empty )
                        {
                            l_requested_time = m_event_queue.begin()->first;
                        }


                    }

                }

            }


            /*
             *  Must re-check the shutdown flag, since the last time we
             *  checked it was before we locked m_cm.  (Otherwise we could
             *  miss the Post() and potentially Wait() forever.)
             */
            if ( m_shutdown )
            {
                m_cm.Unlock();
                return;
            }


            /*
             *  Wait() for the next event, if any.  (We Wait() even if the
             *  next event in the queue is already overdue, just to avoid
             *  starving other threads that are trying to add or cancel
             *  clients.)
             *
             *  Note that l_queue_empty may not be accurate anymore, since we
             *  released the lock on *m_mutex.  However, if someone added an
             *  event, they can't have Post()'ed yet, since we're still
             *  holding the lock on m_cm from before we last checked the
             *  queue.  So once they do, it will wake us up anyway, and we'll
             *  check the queue again then.
             */
            if ( l_queue_empty )
            {
                m_cm.Wait();
            }
            else
            {
                m_cm.Wait( l_requested_time );
            }


            /*
             *  Must unlock m_cm before trying to lock *m_mutex, to avoid
             *  deadlock.
             */
            m_cm.Unlock();


        }

    }


    void TimerServiceAideImpl_Basic::AppAideCloseNotify(
        TwinTraits::TwinCode i_completion_code
    )
    {

        /*
         *  We should already have *(m_ts->m_mutex) locked when this is
         *  called.
         *
         *  IMPORTANT! - We may also have m_ts->m_cv locked (if the aide got
         *  cancelled by the timer service thread, inside a call to Event() ).
         *  If we someday decide we need to lock it in this function for some
         *  reason, make sure we aren't in the timer service thread first.
         */


        /*
         *  Delete ourself from the queue, if we're in it.
         */
        if ( m_queue_pointer )
        {

            TimerService_Basic::t_event_queue_list_type & l_list =
                m_queue_pointer.m_queue_iterator->second;
            l_list.erase( m_queue_pointer.m_list_iterator );

            if ( l_list.empty() )
            {
                m_ts->m_event_queue.erase( m_queue_pointer.m_queue_iterator );
            }

        }


        /*
         *  Don't need to Post() the condition variable, since there's no
         *  point in waking up the timer service thread just to tell it it
         *  doesn't need to wake up.
         */


        /*
         *  If the timer service thread is executing this code (meaning
         *  we're inside a call to our own lead's Event() method), we
         *  had better not be holding the last reference to the
         *  TimerService object, or else this thread is going to be
         *  trying to delete its own Task object.
         */
        AT_Assert(
            ( ! IsRefCountOne( &*m_ts ) ) ||
            Task::GetSelfId() != m_ts->GetThisId()
        );


        delete this;

    }


    AT_MakeFactory0P( "Basic", TimerService_Basic, TimerService, DKy );

}
