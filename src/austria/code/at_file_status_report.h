// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_file_status_report.h
 *
 * 
 */

#ifndef x_at_file_status_report_h_x
#define x_at_file_status_report_h_x 1

#include "at_status_report.h"
#include <ostream>

// Austria namespace
namespace at
{

// ======== FileStatusReport ===================================================
/**
 *  FileStatusReport is a simple status report interface meant to be used as a
 *  generic extended form of file error/status reporting.
 */

class AUSTRIA_EXPORT FileStatusReport
{
public:

    virtual ~FileStatusReport() {}

    /**
     * The StatusTypeCode indicates the nature of the status being reported.
     */

    typedef StatusReport::StatusTypeCode StatusTypeCode;

    /**
     *  ReportStatus is used by the interface to report the status of a file --
     *  typically the result of a parsing error.
     *
     *  @param i_filename The name of the file.
     *  @param i_linenum The line number the status report is associated to
     *                   (0 if not associated to a specific line).
     *  @param i_typecode The nature of the status being reported.
     *  @param i_usercode A user-defined code.
     *  @param i_description A human-readable description of the report.
     */

    virtual void ReportStatus(
        const std::string     & i_filename,
        unsigned int            i_linenum,
        StatusTypeCode          i_typecode,
        int                     i_usercode,
        const std::string     & i_description
        ) = 0;

    /**
     *  ReportStatus is used by the interface to report the status of a file --
     *  typically the result of a parsing error -- by using an existing report.
     *
     *  @param i_sreport A reference to the original file status report to use.
     */

    virtual void ReportStatus(
        const FileStatusReport& i_sreport
        ) = 0;

    /**
     *  ClearStatus is used to clear the status.
     */

    virtual void ClearStatus() = 0;

    /**
     *  GetStatus is used to get a file status report (if any).
     *  @return \c true if a status was reported.
     */

    virtual bool GetStatus() const = 0;

    /**
     *  GetStatus is used to get a file status report (if any).  This version
     *  of GetStatus allows to get all the details of the report.
     *
     *  @param o_filename The name of the file the report is associated to.
     *  @param o_linenum The line number the report is associated to (0 if not
     *                   associated to a specific line).
     *  @param o_typecode The nature of the status being reported.
     *  @param o_usercode The object-specific code.
     *  @param o_description The human-readable description of the report.
     *  @return \c true if a status was reported.
     */

    virtual bool GetStatus(
        std::string           & o_filename,
        unsigned int          & o_linenum,
        StatusTypeCode        & o_typecode,
        int                   & o_usercode,
        std::string           & o_description
        ) const = 0;

    /**
     *  GetStatus is used to get a file status report (if any).  This version
     *  of GetStatus allows to get all the details of the report into a given
     *  FileStatusReport object.
     *
     *  @param o_sreport A reference to the FileStatusReport object where all
     *                   the details of the report are copied.
     *  @return \c true if a status was reported.
     */

    virtual bool GetStatus(
        FileStatusReport      & o_sreport
        ) const = 0;

    /**
     *  Print is used to write a file status report into a output stream.
     */
    virtual void Print( std::ostream& i_os ) const = 0;

};

inline std::ostream& operator<<( std::ostream & i_os, const FileStatusReport & i_sreport )
{
    i_sreport.Print( i_os );
    return i_os;
}

}; // namespace
#endif // x_at_file_status_report_h_x
