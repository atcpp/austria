//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_atomic.h
 *
 */

#ifndef x_at_atomic_h_x
#define x_at_atomic_h_x 1

#include "at_exports.h"
#include "at_os.h"

//
// AT_ATOMIC_H is defined in at_os.h and provides a platform specific
// header file name. (at_gx86_atomic.h or at_win32_atomic.h)
#include AT_ATOMIC_H

// Austria namespace
namespace at
{

// ======== AtomicCount ===============================================
/**
 * AtomicCount performs basic atomic count functionality.
 *
 */

class AUSTRIA_EXPORT AtomicCount
{

    /**
     * AtomicCountType is a fundamental compiler type.
     */

    volatile AtomicCountType                m_atomic_count;

public:

    // ======== AtomicCount ===========================================
    /**
     *  AtomicCount constructor.
     *
     * @param l_atomic_count allows the count to be initialized
     */

    inline AtomicCount( AtomicCountType l_atomic_count = 0 )
      : m_atomic_count( l_atomic_count )
    {
    }


    // ======== operator ++() =========================================
    /**
     *  
     * @return the count
     */

    inline AtomicCountType operator ++()
    {
        return AtomicIncrement( & m_atomic_count ) + 1;
    }

    // ======== operator --() =========================================
    /**
     *  
     * @return the count
     */

    inline AtomicCountType operator --()
    {
        return AtomicDecrement( & m_atomic_count ) - 1;
    }
    

    // ======== operator ++(int) ======================================
    /**
     *  
     * @return the count
     */

    inline AtomicCountType operator ++(int)
    {
        return AtomicIncrement( & m_atomic_count );
    }

    
    // ======== operator --(int) ======================================
    /**
     *  
     * @return the count
     */

    inline AtomicCountType operator --(int)
    {
        return AtomicDecrement( & m_atomic_count );
    }


    // ======== Get ===================================================
    /**
     * Get will get the current value of this AtomicCount.  This
     * is only safe to use when the application can assert that
     * the value will not be changed in a way that will cause it's
     * assumptions to fail.
     *
     * @return nothing
     */

    inline AtomicCountType Get() const
    {
        return m_atomic_count;
    }


    // ======== Bump ==================================================
    /**
     * Bump adds a value and returns the prior value.
     *
     * @param i_add_val The value to add
     * @return The value before adding
     */

    inline AtomicCountType Bump(
        AtomicCountType         i_add_val
    ) {
        return AtomicBump( & m_atomic_count, i_add_val );
    }


    // ======== Exchange ==============================================
    /**
     * Exchange will exchange a value with another value returning the
     * original value.
     *
     * @param i_new_val the new value to place in this AtomicCount
     * @return The previous of this
     */

    inline AtomicCountType Exchange(
        AtomicCountType         i_new_val
    ) {
        return AtomicExchange(
            & m_atomic_count,
            i_new_val
        );
    }


    // ======== CompareExchange =======================================
    /**
     * Compare exchange will perform a "test and set" operation.
     * If the value
     *
     * @param i_new_val the new value to place in this AtomicCount
     * @param i_compare_val the value to compare to this to decide
     *          to perform the exchange.
     * @return The previous of this
     */

    inline AtomicCountType CompareExchange(
        AtomicCountType         i_new_val,
        AtomicCountType         i_compare_val
    ) {
        return AtomicCompareExchange(
            & m_atomic_count,
            i_new_val,
            i_compare_val
        );
    }

    
};


// ======== AtomicExchangePtr =========================================
/**
 * This will exchange object inside the AtomicExchange class with the
 * object a different one.  This is ONLY USEFUL FOR POD types.  This will
 * not work for anything but pointers.
 */

template <typename w_type>
class AtomicExchange_Ptr
{
    volatile AtomicExchangeablePointer      m_data;
    public:


    // ======== AtomicExchange_Ptr ====================================
    /**
     * AtomicExchange_Ptr contructor takes a w_type to initialize this.
     *
     * @param i_init_val the initial value for this object
     */

    AtomicExchange_Ptr( w_type i_init_val )
      : m_data( static_cast<w_type>( i_init_val ) )
    {
    }

    AtomicExchange_Ptr()
      : m_data( 0 )
    {
    }

    // ======== Get ===================================================
    /**
     *  Get will return the value of the pointer
     *
     * @return the current value.
     */

    inline w_type Get() const
    {
        return static_cast<w_type>( m_data );
    }

    // ======== Set ===================================================
    /**
     *  Set will set the current value
     *
     * @return the current value.
     */

    inline void Set( w_type i_data )
    {
        m_data = i_data;
    }

    // ======== Exchange =============================================
    /**
     * Exchange will swap the contents of this object with the one passed
     * in using the architecture specific atomic exchange mechanisms.
     *
     * @param i_val the new value for this object
     * @return the previous value contained in this object
     */

    inline w_type Exchange( w_type i_val )
    {
        return static_cast<w_type>(
            AtomicExchange(
                & m_data,
                static_cast<AtomicExchangeablePointer>( i_val )
            )
        );
    }

    // ======== CompareExchange =======================================
    /**
     * CompareExchange will swap the contents of this object with the 
     * passed in using the architecture specific atomic compare
     * exchange mechanisms. This tests that the current value compares
     * equal to i_compare before performing the exchange.
     *
     * @param i_val The new value for this object
     * @param i_compare The value that is compared to the destination
     *          before the exchange is made.
     * @return The original value
     */

    inline w_type CompareExchange(
        w_type                  i_val,
        w_type                  i_compare
    ) {
        return static_cast<w_type>(
            AtomicCompareExchange(
                & m_data,
                static_cast<AtomicExchangeablePointer>( i_val ),
                static_cast<AtomicExchangeablePointer>( i_compare )
            )
        );
    }    
    
};

// ======== AtomicExchange_Val =========================================
/**
 * This will exchange object inside the AtomicExchangeVal class with the
 * object a different one.  This is ONLY USEFUL FOR POD types.  This will
 * not work for anything but values that are the same as the size of int.
 */

template <typename w_type>
class AtomicExchange_Val
{
    volatile AtomicExchangeableValue        m_data;
    public:


    // ======== AtomicExchange_Val ====================================
    /**
     * AtomicExchange_Val contructor takes a w_type to initialize this.
     *
     * @param i_init_val the initial value for this object
     */

    AtomicExchange_Val( w_type i_init_val )
      : m_data( AtomicExchangeableValue( i_init_val ) )
    {
    }

    AtomicExchange_Val()
      : m_data( 0 )
    {
    }

    // ======== Get ===================================================
    /**
     *  Get will return the value of the pointer
     *
     * @return the current value.
     */

    inline w_type Get() const
    {
        return m_data;
    }

    // ======== Set ===================================================
    /**
     *  Set will set the current value
     *
     * @return the current value.
     */

    inline void Set( w_type i_data )
    {
        m_data = i_data;
    }

    // ======== Exchange =============================================
    /**
     * Exchange will swap the contents of this object with the one passed
     * in using the architecture specific atomic exchange mechanisms.
     *
     * @param i_val the new value for this object
     * @return the previous value contained in this object
     */

    inline w_type Exchange( w_type i_val )
    {
        return w_type(
            AtomicExchange(
                & m_data,
                AtomicExchangeableValue( i_val )
            )
        );
    }
    
    // ======== CompareExchange =======================================
    /**
     * CompareExchange will swap the contents of this object with the 
     * passed in using the architecture specific atomic compare
     * exchange mechanisms. This tests that the current value compares
     * equal to i_compare before performing the exchange.
     *
     * @param i_val the new value for this object
     * @param i_compare The value that is compared to the destination
     *          before the exchange is made.
     * @return The original value
     */

    inline w_type CompareExchange(
        w_type                  i_val,
        w_type                  i_compare
    ) {
        return w_type(
            AtomicCompareExchange(
                & m_data,
                AtomicExchangeableValue( i_val ),
                AtomicExchangeableValue( i_compare )
            )
        );
    }    
    
};

}; // namespace

#endif // x_at_atomic_h_x


