/*
 *
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *  at_dir.h
 *
 *  at_dir.h declares the following classes
 *
 *  Directory - Class for Handling Directories
*/

#ifndef x_at_dir_h_x
#define x_at_dir_h_x 1

#include "at_file.h"

// Austria namespace
namespace at
{

// ======== DirEntry ================================================
/**
 * DirEntry describes an entry in Directory
 *
 */

struct AUSTRIA_EXPORT DirEntry
{
    public:

    virtual ~DirEntry() {}

    virtual bool IsDirectory () const;

    FilePath m_entry_name;

};

// ======== DirIterator =============================================
/**
 * Will iterate a Directory and provide all its entries. 
 *
 */

class AUSTRIA_EXPORT DirIterator
  : virtual public PtrTarget_MTVirtual
{
    public:

    DirIterator ( const FilePath& i_dir_name );
    ~DirIterator ();

    // ======== Next ==================================================
    /**
     * Next will move this iterator to point to the next entry.
     *
     *
     * @return True if the iterator points to a valid entry
     *      after the move.
     */

    virtual bool Next ();


    // ======== First =================================================
    /**
     * First will move the iterator to the first entry in the directory.
     *
     *
     * @return true if there was at least one file in the directory
     */

    virtual bool First ();


    // ======== GetEntry ===========================================
    /**
     * GetEntry will return the current File Entry.  
     *
     * @return A reference to the current Entry
     */

    virtual const DirEntry & GetEntry () const;
    
    private:

    FileHandle m_handle;
    FilePath m_dir_name;
    DirEntry m_dir_entry;
};

class AUSTRIA_EXPORT Directory
{
    public:

    // ======== Create =================================================
    /**
     * 
     *  Create will create a directory
     *
     * @param i_dir_name A valid directory path
     *
     * @return false if directory already exists or create failed
     */

    static bool Create ( const FilePath& i_dir_name );

    // ======== Remove =================================================
    /**
     * 
     *  RmDir will attempt to remove a directory
     *
     * @param i_dir_name A valid directory path
     * @param i_delete_if_not_empty true removes folder and all its contents
     *                              false fail if there is someting in the directory
     *
     * @return false if there are entries in the directory and i_deleteifnotempty is false
     *         false if the remove failed
     */

    static bool Remove ( const FilePath& i_dir_name, bool i_delete_if_not_empty = false );
    
    // ======== ChangeCurrent =================================================
    /**
     * 
     *  ChangeCurrent Changes the current directoy
     *
     * @param i_dir_name A valid directory path
     *
     * @return false if the directory does not exist 
     */

    static bool ChangeCurrent ( const FilePath& i_dir_name );

    // ======== Rename =================================================
    /**
     * 
     *  Rename will Rename a Directory
     *
     * @param i_dir_oldname A valid directory path
     * @param i_dir_newname A valid directory path
     *
     * @return false if the operation failed
     */

    static bool Rename ( const FilePath& i_dir_old_name, const FilePath& i_dir_new_name );

    // ======== Exists =================================================
    /**
     * 
     *  Exists checks to see if the Directoy exists
     *
     * @param i_dir_name A valid directory path
     *
     * @return false if the directory does not exist 
     */

    static bool Exists ( const FilePath& i_dir_name );

    public:

    Directory ( const FilePath& i_dir_name );

    // ======== IsEmpty =================================================
    /**
     * 
     *  Tests if a Directory is Empty
     *
     * @return true if the Directory is empty
     */

    bool IsEmpty ();

    // ======== IsDirectory =================================================
    /**
     * 
     *  Tests if a Directory is a Directory
     *
     * @return true if the Directory is in fact a Directory 
     */

    bool IsDirectory ();

    // ======== Clear =================================================
    /**
     * 
     *  Deletes all the entries in this directory
     *
     * @param i_delete_sub_directories true remove all subdirectories
     *                                 false only remove files
     *
     * @return true if the delete completed successfully 
     */

    bool Clear  ( bool i_delete_sub_directories = false );

    // ======== List =================================================
    /**
     * List returns a new DirIterator for the directory.
     *
     *
     * @return Pointer to a DirIterator
     */

    virtual PtrDelegate<DirIterator *> ListEntries () const;


    // ======== operator == =============================================
    /**
     * This compares the contents of two directories
     *
     * @param i_rhs The rhs of "=="
     * @return true if i_rhs matches this directories contents
     */

    virtual bool operator == ( const Directory & i_rhs ) const;

    // ======== ~Directory =============================================

    virtual ~Directory ();

    private:

    FilePath m_dir_name;
};

}; // namespace

//
// AT_DIR_H is defined in at_os.h and provides a platform specific
// header file name. (at_gx86_dir.h or at_win32_dir.h)
//#include AT_DIR_H

#endif // x_at_dir_h_x
