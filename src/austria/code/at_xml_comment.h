// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_comment.h
 *
 * \author Guido Gherardi
 *
 */

#ifndef x_at_xml_comment_h_x
#define x_at_xml_comment_h_x 1

#include "at_xml_char_data.h"

// Austria namespace
namespace at
{
    /**
     *  The XmlComment class inherits from XmlCharacterData and represents the content of a comment in
     *  XML, i.e., all the characters between the starting '<!--' and ending '-->'.
     *
     *  @note No lexical check is done on the content of a comment and it is therefore possible to have
     *        the character sequence "--" (double-hyphen) in the content, which is illegal in a comment
     *        per section 2.5 of XML 1.0. The presence of this character sequence must generate a fatal
     *        error during serialization. Note also that the grammar does not allow a comment ending in
     *        "--->", so the character '-' cannot be the last character of the content.
     */
    class XmlComment : public XmlCharacterData
    {
        friend class XmlDocument;

    public:

        /**
         *  @brief  The name of a comment node, which is #comment for all the comment nodes.
         */
        virtual std::string Name() const;

        /**
         * @brief  The enum value representing the XmlComment node type.
         */
        virtual unsigned short Type() const;

        /**
         *  @brief  Duplicates this comment node.
         *  @param  i_deep Not used.
         *  @return A duplicate of this comment node, containing a copy of the character data.
         */
        virtual PtrDelegate< XmlNode* > Clone( bool i_deep ) const;

        /**
         *  @brief Accepts a visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op );

        /**
         *  @brief Accepts a non-modifying, visitor operation.
         *  @param i_op The visiting operation.
         */
        virtual void Accept( XmlNodeVisitor& i_op ) const;

    protected:

        /**
         *  @brief  Creates a comment node.
         *  @param  i_ownerDocument The XmlDocument that creates this node.
         *  @param  i_content       The content of the comment.
         */
        XmlComment( PtrView< XmlDocument* > i_ownerDocument, const std::string& i_content ) :
            XmlCharacterData( i_ownerDocument, i_content ) {}

    private:

        /**
         *  The copy constructor is declared as private and is not defined anywhere in the code. This is to
         *  prevent the compiler from defining a bitwise default copy constructor and using it.
         *  To make a copy of an XmlText, the Clone method must be used.
         */
        XmlComment( const XmlComment& );

    };

}; // namespace

#endif // x_at_xml_comment_h_x
