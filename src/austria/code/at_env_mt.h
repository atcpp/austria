/**
 * \file at_env_mt.h
 *
 * \author Bradley Austin
 *
 * at_env_mt.h contains extensions to the Austria environment module to
 * support thread-safe environment passing.
 */


#ifndef x_at_env_mt_h_x
#define x_at_env_mt_h_x 1


#include "at_lifetime_mt.h"


namespace at
{


    class EnvironmentPtrPolicy_MT
    {

    public:

        template< class w_impl_type >
        class t_ptr_type
        {

        public:

            typedef  Ptr< w_impl_type * >  t_type;

        private:

            /* Unimplemented */
            t_ptr_type();
            t_ptr_type( const t_ptr_type & );
            ~t_ptr_type();
            t_ptr_type & operator=( const t_ptr_type & );

        };

        typedef  PtrTarget_MT  t_ptr_target_type;

    private:

        /* Unimplemented */
        EnvironmentPtrPolicy_MT();
        EnvironmentPtrPolicy_MT( const EnvironmentPtrPolicy_MT & );
        ~EnvironmentPtrPolicy_MT();
        EnvironmentPtrPolicy_MT & operator=
            ( const EnvironmentPtrPolicy_MT & );

    };


}


#endif
