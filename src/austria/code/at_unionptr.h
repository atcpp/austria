//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_unionptr.h
 *
 */

#ifndef x_at_unionptr_h_x
#define x_at_unionptr_h_x 1

#include "at_types.h"

#include "at_assert.h"

// Austria namespace
namespace at
{

// ======== AT_UnionPtr_Empty =========================================
/**
 * This is a do nothing class.  This is a place holder for
 * UnionPtr type 2.
 *
 */

class UnionPtr_Empty_A
{
};

/**
 * This is a do nothing class.  This is a place holder for
 * UnionPtr type 3.
 *
 */

class UnionPtr_Empty_B
{
};



// ======== UnionPtr ===============================================
/**
 * UnionPtr is simply a class that contains a single value of
 * multiple types.  Only 1 value may be stored but that value may have
 * 4 possible types.
 *
 * As an optimization, UnionPtr uses the unused bits in a pointer
 * to distinguish between the 4 types.  This may also be implemented
 * using member variables if this turns out to be a problem on some
 * architectures.  Obviously this will not work with structures that
 * allow alignment by byte or allocators that allow allocation on other
 * than multiples of 4.
 *
 * @param w_enum_type   contains the enumerator type that describes the types.
 *                      The enumerator values must be 0..3 that correspond to
 *                      each possible type.
 * 
 * @param w_type_0      Is the type corresponding to enumerator with value 0.
 * @param w_type_1      Is the type corresponding to enumerator with value 1.
 * @param w_type_2      Is the type corresponding to enumerator with value 2.
 * @param w_type_3      Is the type corresponding to enumerator with value 3.
 * 
 */

template <
    typename    w_enum_type,
    typename    w_type_0,
    typename    w_type_1,
    typename    w_type_2 = UnionPtr_Empty_A,
    typename    w_type_3 = UnionPtr_Empty_B
>
class UnionPtr
{
    public:


    // ======== t_PtrUnion ============================================
    /**
     * t_PtrUnion is the inion that contains the pointer value.
     *
     */

    union t_PtrUnion
    {
        w_type_0                                  m_value_0;
        w_type_1                                  m_value_1;
        w_type_2                                  m_value_2;
        w_type_3                                  m_value_3;
        PtrDiff                                   m_ptr_diff;
    };

    t_PtrUnion                                    m_value;


    // ======== t_Check ===============================================
    /**
     * t_Check provides no purpose but to cause an error if the t_PtrUnion
     * is larger than a PtrDiff.  If this happens then PtrDiff is
     * either not defined correctly or incorrect types were passed in the
     * parameters.
     */

    private:
    struct t_Check
    {
        char m_array_overflow[
            1/ int( !(
                sizeof( t_PtrUnion ) != sizeof( PtrDiff )
                || ( sizeof( ( static_cast< t_PtrUnion * >( 0 ) )->m_value_0 ) < 4 )
                || ( sizeof( ( static_cast< t_PtrUnion * >( 0 ) )->m_value_1 ) < 4 )
            ) )
        ];
    };
    

    public:
    // ======== Get ===================================================
    /**
     *  Will fetch the pointer in question and return the pointer
     *  type. This is just one inline to allow for compiler optimization.
     *
     * @param o_param_N are references to values.
     * @return nothing
     */

    inline w_enum_type Get(
        w_type_0                & o_param_0,
        w_type_1                & o_param_1,
        w_type_2                & o_param_2 = ( * static_cast< UnionPtr_Empty_A * >( 0 ) ),
        w_type_3                & o_param_3 = ( * static_cast< UnionPtr_Empty_B * >( 0 ) )
    ) const
    {

        t_PtrUnion l_ptr = m_value;

        int l_type = m_value.m_ptr_diff & PtrDiff( 0x3 );

        if ( l_type == 0 )
        {
            o_param_0 = m_value.m_value_0;
            return static_cast<w_enum_type>( 0 );
        }
        
        l_ptr.m_ptr_diff &= ~PtrDiff( 0x3 );

        switch ( l_type )
        {
            case 1 : o_param_1 = l_ptr.m_value_1; return static_cast<w_enum_type>( 1 );
            case 2 : o_param_2 = l_ptr.m_value_2; return static_cast<w_enum_type>( 2 );
            default : o_param_3 = l_ptr.m_value_3; return static_cast<w_enum_type>( 3 );
        }

    } // end Get

    inline UnionPtr & operator=( const w_type_0 i_value )
    {
        m_value.m_value_0 = i_value;
        
        return * this;
    }
    
    inline UnionPtr & operator=( const w_type_1 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_1 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x1 );

        m_value = l_ptr;

        return * this;
    }
    
    inline UnionPtr & operator=( const w_type_2 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_2 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x2 );

        m_value = l_ptr;

        return * this;
    }
    
    inline UnionPtr & operator=( const w_type_3 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_3 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x3 );

        m_value = l_ptr;

        return * this;
    }
    
    inline bool operator==( const UnionPtr & i_value ) const
    {
        return m_value.m_value_0 == i_value.m_value.m_value_0;
    }
    
    inline bool operator!=( const UnionPtr & i_value ) const
    {
        return m_value.m_value_0 != i_value.m_value.m_value_0;
    }
    
    inline bool operator<( const UnionPtr & i_value ) const
    {
        return m_value.m_value_0 < i_value.m_value.m_value_0;
    }
    
    inline UnionPtr()
    {
    }
    
    inline UnionPtr( const w_type_0 i_value )
    {
        m_value.m_value_0 = i_value;
        
    }
    
    inline UnionPtr( const w_type_1 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_1 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x1 );

        m_value = l_ptr;

    }
    
    inline UnionPtr( const w_type_2 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_2 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x2 );

        m_value = l_ptr;

    }
    
    inline UnionPtr( const w_type_3 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_3 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x3 );

        m_value = l_ptr;
        
    }
    
};



// ======== UnionPtrType ===========================================
/**
 * UnionPtrType allows the management of 2 pointer types.  The second type
 * can be stored with 3 different values (or i_type) values.
 *
 */

template <
    typename    w_enum_type,
    typename    w_type_0,
    typename    w_type_1
>
class UnionPtrType
{
    public:


    // ======== t_PtrUnion ============================================
    /**
     * t_PtrUnion is the inion that contains the pointer value.
     *
     */

    union t_PtrUnion
    {
        w_type_0                                  m_value_0;
        w_type_1                                  m_value_1;
        PtrDiff                                m_ptr_diff;
    };

    t_PtrUnion                                    m_value;


    // ======== t_Check ===============================================
    /**
     * t_Check provides no purpose but to cause an error if the t_PtrUnion
     * is larger than a PtrDiff.  If this happens then PtrDiff is
     * either not defined correctly or incorrect types were passed in the
     * parameters.
     */

    private:
    struct t_Check
    {
        char    m_array_overflow[
            1/ int( !(
                sizeof( t_PtrUnion ) != sizeof( PtrDiff )
                || ( sizeof( ( static_cast< t_PtrUnion * >( 0 ) )->m_value_0 ) < 4 )
                || ( sizeof( ( static_cast< t_PtrUnion * >( 0 ) )->m_value_1 ) < 4 )
            ) )
        ];
    };
    
    public:
    // ======== Get ===================================================
    /**
     *  Will fetch the pointer in question and return the pointer
     *  type. This is just one inline to allow for compiler optimization.
     *
     * @param o_param_0 reference to values 0
     * @param o_param_1 reference to values 1
     * @return nothing
     */

    inline w_enum_type Get(
        w_type_0                                & o_param_0,
        w_type_1                                & o_param_1
    ) const
    {

        t_PtrUnion l_ptr = m_value;

        int l_type = m_value.m_ptr_diff & PtrDiff( 0x3 );

        if ( l_type == 0 )
        {
            o_param_0 = m_value.m_value_0;
            return static_cast<w_enum_type>( 0 );
        }
        
        l_ptr.m_ptr_diff &= ~PtrDiff( 0x3 );

        o_param_1 = l_ptr.m_value_1;

        return static_cast<w_enum_type>( l_type );


    } // end Get

    /**
     * assignment to type 0.  This sets the value of this object
     * to the 0 type.
     *
     * @param i_value the value of the type
     */
    
    inline UnionPtrType & operator=( const w_type_0 i_value )
    {
        m_value.m_value_0 = i_value;
        
        return * this;
    }
    
    /**
     * Equality comparison to type 0.
     *
     * @param i_value the value of the type
     */
    
    inline bool operator==( const UnionPtrType & i_value ) const
    {
        return m_value.m_value_0 == i_value.m_value.m_value_0;
    }
    
    /**
     * Assignment to type 1.  This sets the value to the
     * input value and sets it to type 1.
     *
     * @param i_value the value of the type
     */
    
    inline UnionPtrType & operator=( const w_type_1 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_1 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x1 );

        m_value = l_ptr;

        return * this;
    }
    
    inline UnionPtrType()
    {
    }
    
    inline UnionPtrType( const w_type_0 i_value )
    {
        m_value.m_value_0 = i_value;
        
    }
    
    inline UnionPtrType( const w_type_1 i_value )
    {
        t_PtrUnion l_ptr;

        l_ptr.m_value_1 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( 0x1 );

        m_value = l_ptr;

    }
    
    inline UnionPtrType & Set( const w_type_1 i_value, w_enum_type i_type )
    {
        t_PtrUnion l_ptr;

        AT_Assert( i_type != 0 );

        l_ptr.m_value_1 = i_value;

        l_ptr.m_ptr_diff |= PtrDiff( i_type );

        m_value = l_ptr;

        return * this;
    }
    
};

}; // namespace
#endif // x_at_unionptr_h_x


