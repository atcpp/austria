/*
 * at_net.h - Austria network interfaces
 *
 * Portions of this file are subject to the following:
 *
 *   The Austria library is copyright (c) Gianni Mariani 2004.
 *
 *   Grant Of License.  Grants to LICENSEE the non-exclusive right to use
 *   the Austria library subject to the terms of the LGPL.
 *
 *   A copy of the license is available in this directory or one may be
 *   found at this URL: http://www.gnu.org/copyleft/lesser.txt
 *
 * While other portions are subject to the following:
 *
 *   This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *   Any unauthorized use of this file is prohibited and will be prosecuted
 *   to the full extent of the law.
 */

// REV:  this whole module needs some rethinking as to interface protocols and lifetime
// management -- there is no real clarity in the protocol or the lifetime issues.
//

#ifndef x_at_net_h_x
#define x_at_net_h_x 1

#include "at_exports.h"
#include "at_lifetime.h"
#include "at_buffer.h"
#include "at_activity.h"
#include "at_any.h"
#include "at_twinmt.h"

#include <list>

namespace at
{

/**
 * NetError describes a network error of some kind.  Statically allocated
 * instances of subclasses of this class take the place of numeric
 * constants for error codes.
 */
class AUSTRIA_EXPORT NetError
{
protected:
    const char *const m_what;
    NetError(const char *s) : m_what(s) { }

public:
    // enable virtual inheritance
    virtual ~NetError() {}

    /**
     * Return a textual description of the error.
     *
     * @return a statically allocated const char * string
     * that describes the error.
     */
    const char * What() const { return m_what; }

    /**
     * s_bind_failure indicates that the local port that was
     * requested was unable to be allocated.  This is usually
     * due to it having already been allocated by some other
     * process or another listener.
     */
    static const NetError       s_bind_failure;

    static const NetError       s_unexpected_failure;
    static const NetError       s_connection_refused;
    static const NetError       s_timed_out;
    static const NetError       s_network_unreachable;
};


/**
 * NetAddress is a generic address class.  Virtually any type of address
 * may be provided.  This provides a uniform API for various types of
 * network implementations.  The goal is to provide a single API that
 * is capable of interfacing with multiple network layers using
 * different implementations.  For example, if an application requires
 * IPv4 and IPv6 support, the only difference in client code is
 * the location where the NetAddress is created.  Otherwise, the rest
 * of the application code remains the same.
 * REV: @@ TODO 
 *
 * NetAddress is a reference counted class.
 */
class AUSTRIA_EXPORT NetAddress
  : public PtrTarget_MT
{
public:
    virtual ~NetAddress() {}
    virtual bool operator==(const NetAddress&) const;
};


/**
 * NetParameters is a generic "parameter" class.  This is paired with the
 * NetAddress to provide for connection specific parameters.
 *
 * NetParameters is a reference counted class.
 */
class AUSTRIA_EXPORT NetParameters
  : public PtrTarget_MT
{
public:
    virtual ~NetParameters() {}
};


/**
 * NetAddress_NamePort describes a name and port.
 *
 */
class AUSTRIA_EXPORT NetAddress_NamePort
  : public NetAddress
{
public:
    virtual ~NetAddress_NamePort() {}
    /**
     * NetAddress_NamePort produces a basic name:port net address.
     * This is one of the basic address types than may be passed
     * to Net::Resolve.
     *
     */
    NetAddress_NamePort(
        const std::string                   & i_name,
        unsigned short                      i_port
    )
        : m_name(i_name), m_port(i_port)
    {
    }

    const std::string& Name() const { return m_name; }
    const unsigned short Port() const { return m_port; }

protected:
    std::string m_name;
    unsigned short m_port;
};


/**
 * NetResolverError describes the various errors related with host name
 * resolution.
 */
class AUSTRIA_EXPORT NetResolverError
  : public NetError
{
protected:
    NetResolverError(const char *s) : NetError(s) { }

public:
    virtual ~NetResolverError() {}
    /**
     * Nonrecoverable failure in name resolution.
     */
    static const NetResolverError          s_fail;

    /**
     * No address associated with nodename.
     */
    static const NetResolverError          s_nodata;

    /**
     * Neither nodename nor servname provided, or not known.
     */
    static const NetResolverError          s_noname;

    /**
     * The name server said to try again, so we did.  (Probably a few
     * times, in fact.)  Alas, it just kept saying to try again.
     */
    static const NetResolverError          s_gaveup;
};


/**
 * NetDatagramError contains errors related to datagram receiving/sending
 */
class AUSTRIA_EXPORT NetDatagramError
  : public NetError
{
protected:
    NetDatagramError(const char *s) : NetError(s) { }

public:
    virtual ~NetDatagramError() {}

    /**
     * Requested address is unavailable
     */
    static const NetDatagramError          s_address_unavailable;

    static const NetDatagramError          s_unexpected_failure;
};


/**
 * NetConnectionError contains errors that can occur on connection-based
 * protocols.
 */
class AUSTRIA_EXPORT NetConnectionError
  : public NetError
{
protected:
    NetConnectionError(const char *s) : NetError(s) { }

public:
    virtual ~NetConnectionError() {}

    /**
     * s_peer_reset indicates that the peer has shut down
     * the connection.
     */
    static const NetConnectionError        s_peer_reset;

    static const NetConnectionError        s_unexpected_failure;
};


/**
 * NetResolverLeadIf is an interface for those wishing to receive address
 * resolver results from Net::Resolve.
 *
 * During calls to Net::Resolve, an aide is associated with the passed
 * NetResolverLeadIf.  The aide will disassociate itself once Failed or
 * NetResolverResults has been called.
 *
 * Do not inherit from or pass pointers to NetResolverLeadIf, but to
 * NetResolverLead, which is a typedef to LeadTwinMT<NetResolverLeadIf>,
 * defined immedately below this class.
 * REV: @@ TODO -- there are ways to enforce this policy
 */
class AUSTRIA_EXPORT NetResolverLeadIf
{
public:
    virtual ~NetResolverLeadIf() {}

    /**
     * Failed indicates that the resolver failed.
     *
     * @param i_error Indicates the type of error that occured.
     */
    virtual void Failed( const NetResolverError & i_error ) = 0;

    /**
     * NetResolverResults indicates that the resolver succeeded and
     * results are available.
     *
     * @param io_list Is a list of addresses.
     */
    virtual void NetResolverResults(std::list< at::Ptr< NetAddress * > >& io_list) = 0;
};

/**
 * This typedef is what you should pass pointers to and inherit from,
 * rather than NetResolverLeadIf (which, by itself, lacks the lead-related
 * methods).
 * REV: @@ TODO -- there are ways to enforce this policy
 */
typedef LeadTwinMT<NetResolverLeadIf> NetResolverLead;


class AUSTRIA_EXPORT NetProperty : public PtrTarget_MT 
{
public:
    virtual ~NetProperty() {}
};


/**
 * NetConnectionResponderIf is the interface implemented by a lead
 * when requesting notification of events on a connection.
 *
 * An aide, which is a NetConnection, is associated with a
 * NetConnectionResponderIf during a call to
 * NetConnection::ConnectionNotify.  The aide will disassociate itself
 * once the connection is closed.
 *
 * Please don't subclass from NetConnectionResponderIf directly, but from
 * NetConnectionResponder (which is a typedef found immedately below this
 * class) instead.  That way, you'll get the methods you need from
 * LeadTwinMT.
 * REV: @@ TODO -- there are ways to enforce this policy
 */
class AUSTRIA_EXPORT NetConnectionResponderIf
{
public:
    virtual ~NetConnectionResponderIf() {}

    /**
     * ReceiveFailure is called by NetConnection aide when there is a
     * read failure.
     *
     * @param i_error The error that occurred.
     */
    virtual void ReceiveFailure(
        const NetConnectionError    & i_error
    ) = 0;

    /**
     * SendCompleted is called by a NetConnection aide when bytes have
     * been sent to the underlying networking layer for delivery.
     *
     * @param i_buffer The buffer to which this failure pertains.
     * @param i_end One past the last byte that was sent successfully.
     * All bytes before this one were sent successfully.
     */
    virtual void SendCompleted(
        PtrDelegate<const Buffer*>    i_buffer,
        const char                  * i_end
    ) = 0;

    /**
     * SendFailure is called by NetConnection aide when there is a
     * write failure.
     *
     * @param i_buffer The buffer to which this failure pertains.
     * @param i_begin The first byte that was not sent.  All bytes in the
     * buffer after this byte were not sent.
     * @param i_error The error that occurred.
     */
    virtual void SendFailure(
        PtrDelegate<const Buffer*>    i_buffer,
        const char                  * i_begin,
        const NetConnectionError    & i_error
    ) = 0;

    /**
     * DataReady indicates that data may be available to read.
     *
     */
    virtual void DataReady() = 0;

    /**
     * OutOfBandDataReady indicates that data may be available to read
     * from ReceiveOutOfBand.
     *
     */
    virtual void OutOfBandDataReady() = 0;

    /**
     * ReceivedPropertiesNotification is called when properties
     * received from the client have changed.
     *
     * @param i_complete If true indicates that there are no more
     *                  properties available.  This is set in a
     *                  protocol specific way.
     * @param i_props_in_queue indicates that there are properties
     *                  available in the property queue.
     */
    virtual void ReceivedPropertiesNotification(
        bool                i_complete,
        bool                i_props_in_queue
    ) = 0;
};

/**
 * Pass pointers to and subclass from this typedef, rather than
 * NetConnectionResponderIf.
 * REV: @@ TODO -- there are ways to enforce this policy
 */
typedef LeadTwinMT<NetConnectionResponderIf> NetConnectionResponder;


/**
 * NetConnection is created once a connection or a data sink is
 * established. This allows a client to receive or send data.
 * the client owns the lifetime of the NetConnection
 *
 */
class AUSTRIA_EXPORT NetConnection
  : public virtual PtrTarget_MT
{
public:
    virtual ~NetConnection() {}

    inline SizeMem s_receive_max_default(void) {return 1<<18;};

    /**
     * t_PropList is the list of properties type used to set/
     * retrieve properties.
     */
    typedef std::list< at::Ptr< NetProperty * > >   t_PropList;

    /**
     * Receive will append data into the buffer passed as
     * "o_data".  Up to the at::BufferRegion::m_max_available
     * size. Calls to Receive will return false when no
     * further data is available.  When further data does
     * become available, a DataReady notification is made
     * on the currently registered NetConnectionResponder.
     *
     * @param o_data The buffer to be written to with data
     *                read from the connection.
     * @return True if more data could be available false otherwise - true
     *          indicates that no callback will occur.
     */
    virtual bool Receive( PtrView< Buffer * > o_data ) = 0;

    /**
     * Send will send data over this connection.  Note that WE OWN THE
     * CONTENTS OF i_buffer AFTER THIS CALL IS MADE.  If you need to retain
     * your own copy, please make one prior to calling this function.
     *
     * @param i_buffer The data to send.
     */
    virtual void Send(
        PtrDelegate< Buffer * >             i_buffer
    ) = 0;

    /**
     * ReceiveOutOfBand will append data into the buffer passed as
     * "o_data".  Up to the at::BufferRegion::m_max_available
     * size. Calls to Receive will return false when no
     * further data is available.  When further data does
     * become available, a DataReady notification is made
     * on the currently registered NetConnectionResponder.
     *
     * @param o_data The buffer to be written to with data
     *                read from the connection.
     * @return True if more data could be available false otherwise - true
     *          indicates that no callback will occur.
     */
    virtual bool ReceiveOutOfBand( PtrView< Buffer * > o_data ) = 0;

    /**
     * SendOutOfBand is sent using the "out-of-band" mechanism if
     * available.  This is usually only available for raw TCP connections.
     * Note that WE OWN THE CONTENTS OF i_buffer AFTER THIS CALL IS MADE.
     * If you need to retain your own copy, please make one prior to
     * calling this function.
     *
     * @param i_buffer The data to send.
     * @return True if this message was accepted or false
     *          if this connection does not support OOB data
     *          transmission.
     */
    virtual bool SendOutOfBand(
        PtrDelegate< Buffer * >             i_buffer
    ) = 0;

    /**
     * ConnectionNotify will notify the connection holder of
     * events on this connection.
     *
     * @param i_lead The lead to associate with the connection notifier
     *              aide.  There can be no more than one notifier lead
     *              per connection.
     */
    virtual void ConnectionNotify(NetConnectionResponder & i_lead) = 0;

    /**
     * GetReceivedProperties will retrieve all the properties set
     * since the previous call to GetReceivedProperties properties.
     *
     * @return The list of properties
     */
    virtual t_PropList GetReceivedProperties() = 0;

    /**
     * AddOutgoingProperties will pass properties to be associated
     * with the response.  If this is a HTTP connection, for example,
     * headers and HTTP response codes can be set.  This must be called
     * before any calls to Send are made.
     *
     * @param i_proplist The list of properties associated with
     *          data to be sent.
     */
    virtual void AddOutgoingProperties(
        t_PropList          & i_proplist
    ) = 0;

    /**
     * PeerAddress returns the NetAddress of the peer.
     *
     * @return The peer address (NetAddress)
     */
    virtual PtrDelegate< NetAddress * > PeerAddress() = 0;

    /**
     * LocalAddress returns the netaddress of the local interface.
     *
     * @return The local address (NetAddress)
     */
    virtual PtrDelegate< NetAddress * > LocalAddress() = 0;
};


/**
 * This lead is associated with Net::Connect or Net::Listen.  When
 * a connection is successfully established, the Established callback
 * provides the newly established NetConnection object.
 *
 * For NetConnectLeadIfs passed to Net::Connect, the aide (which is
 * associated with the lead during the Connect call) has a lifetime limited
 * to the duration of the connection attempt; once the attempt has
 * succeeded or failed, the aide disassociates itself.
 *
 * For NetConnectLeadIfs passed to Net::Listen, the aide (which is associated
 * with the lead during the Listen call) has a lifetime limited to the
 * lifetime of the underlying listening socket/whatever.  In the event that
 * the listening socket/whatever cannot be created, NetConnectLeadIf::Failed
 * will be called before the aide disassociates itself.
 *
 * You must not subclass from or pass to NetConnectLeadIf itself, but from
 * NetConnectLead, which is a typedef for LeadTwinMT<NetConnectLeadIf>,
 * defined immediately below this class.
 * REV: @@ TODO -- there are ways to enforce this policy
 */
class AUSTRIA_EXPORT NetConnectLeadIf
{
public:
    virtual ~NetConnectLeadIf() {}

    /**
     * Failed indicates the Listen or Connect operation failed.
     *
     */
    virtual void Failed( const NetError & i_err ) = 0;

    /**
     * Established indicates that a connection has been established.  If
     * this is the result of a Net::Connect call, this will happen only
     * once.  Net::Listen will result in a call for each incoming
     * connection that is established.
     *
     * @param io_endpoint The NetConnection that has been established.
     */
    virtual void Established( PtrDelegate< NetConnection * > io_endpoint ) = 0;
};

/**
 * Pass pointers to and subclass from this typedef, rather than
 * NetConnectLeadIf.
 * REV: @@ TODO -- there are ways to enforce this policy
 */
typedef LeadTwinMT<NetConnectLeadIf> NetConnectLead;


/**
 * NetDatagramBuffer is used to store information regarding data sent or
 * received from an address.
 */
class NetDatagramBuffer
{
public:
    virtual ~NetDatagramBuffer() {}

    /**
     * DatagramBuffer contains data and address information. This is
     * used to send or receive data from a NetDatagramChannel.
     */
    NetDatagramBuffer(
        PtrDelegate< const Buffer * >           i_buffer,
        PtrDelegate< NetAddress * >             i_address
    )
      : m_buffer( i_buffer ),
        m_address( i_address )
    {
    }

    /**
     * m_buffer contains the data (to send or received)
     */
    Ptr< const Buffer * >                       m_buffer;

    /**
     * m_address contains the address ( to send to or
     * received from )
     */
    Ptr< NetAddress * >                         m_address;
};


/**
 * NetDatagramChannelResponderIf is a lead interface, implemented by the
 * clients of this library, for recieving notification of events from a
 * NetDatagramChannel.
 *
 * The corresponding aide is NetDatagramChannel.  A NetDatagramChannel will
 * associate itself with a NetDatagramChannelResponderIf as a result of a
 * call to NetDatagramChannel::Notify.  The channel will disassociate
 * itself with the responder when the channel is closed.  Note that the
 * channel object will probably persist beyond the lead/twin association
 * (because its lifetime is determined by reference counting).
 * REV: @@ TODO -- there are ways to enforce this policy
 *
 */
class AUSTRIA_EXPORT NetDatagramChannelResponderIf
{
public:
    virtual ~NetDatagramChannelResponderIf() {}

    /**
     * ReceiveFailure is called by NetDatagramChannel aide when there is a
     * read failure.
     *
     * @param i_error The error that occurred.
     */
    virtual void ReceiveFailure(
        const NetDatagramError    & i_error
    ) = 0;

    /**
     * SendCompleted is called by a NetDatagramChannel aide when a
     * datagram has been sent to the underlying networking layer for
     * delivery (which does not necessarily mean that it will ever reach
     * its destination, only that it is out of our hands).
     *
     * @param i_buffer The buffer that was sent
     */
    virtual void SendCompleted(PtrDelegate<const Buffer*> i_buffer) = 0;

    /**
     * SendFailure is called by NetDatagramChannel aide when there is a
     * write failure.
     *
     * @param i_buffer The buffer to which this failure pertains.
     * @param i_error The error that occurred.
     */
    virtual void SendFailure(
        PtrDelegate<const Buffer*>    i_buffer,
        const NetDatagramError      & i_error
    ) = 0;

    /**
     * DataReady indicates that data may be available to read.  This call
     * is made once when data first becomes available.  It is not made
     * again until two conditions are met (in order): 1. No more data is
     * available; 2. More data is available.
     *
     */
    virtual void DataReady() = 0;
};

/**
 * This typedef is what you should pass pointers to and inherit from,
 * rather than NetDatagramChannelResponderIf (which, by itself, lacks the
 * lead-related methods).
 * REV: @@ TODO -- there are ways to enforce this policy
 */
typedef LeadTwinMT<NetDatagramChannelResponderIf> NetDatagramChannelResponder;


/**
 * NetDatagramChannel is an interface that is provided by the ListenDatagram
 * interface when a port is established
 *
 */
class AUSTRIA_EXPORT NetDatagramChannel
  : public virtual PtrTarget_MT
{
public:
    virtual ~NetDatagramChannel() {}

    /**
     * Receive will retrieve _all_ the data available on this
     * NetDatagramChannel.
     *
     * @param o_data The list that will have received data added to it.
     *
     * @return True if data was added to the list
     */
    virtual bool Receive(
        std::list< NetDatagramBuffer >     & o_data
    ) = 0;

    /**
     * Send will send data.  Note that WE OWN THE CONTENTS OF i_buffer
     * AFTER THIS CALL IS MADE.  If you need to retain your own copy,
     * please make one prior to calling this function.
     *
     * @param i_buffer The actual bytes to send on the wire.
     * @param i_address The address to send data to
     */
    virtual void Send(
        PtrDelegate< Buffer * >                 i_buffer,
        PtrDelegate< NetAddress * >             i_address
    ) = 0;

    /**
     * DatagramChannelNotify will notify the NetDatagramChannel
     * holder of events on this channel.
     *
     * @param i_lead The lead to associate with the DatagramChannelNotify
     *              aide.  There can be no more than one notifier lead
     *              per datagram channel.
     */
    virtual void DatagramChannelNotify(
        NetDatagramChannelResponder                  * i_lead
    ) = 0;
};


/**
 * NetDatagramLeadIf is the base interface that needs to be implemented
 * by clients wishing to make Net::DatagramListen calls; that is, by
 * anyone wishing to establish listening datagram sockets/whatever.
 *
 * An aide is associated with a NetDatagramLeadIf during Net::DatagramListen
 * calls.  The lifetime of the aide is limited to the duration of the
 * connection attempt; it will disassociate itself with the lead
 * once Failed or ListenerReady has been called.
 *
 * Do not inherit from or pass pointers to NetDatagramLeadIf.  Use
 * NetDatagramLead, which is a typedef to LeadTwinMT<NetDatagramLeadIf>,
 * defined immediately below this class.  This interface itself lacks the
 * lead-related methods.
  * REV: @@ TODO -- there are ways to enforce this policy
*/
class AUSTRIA_EXPORT NetDatagramLeadIf
{
public:
    virtual ~NetDatagramLeadIf() {}

    /**
     * Failed indicates there is an error in establishing the
     * DatagramListener
     */
    virtual void Failed( const NetError & i_err ) = 0;

    /**
     * ListenerReady is called when the listener is established and
     * available for reading and writing.
     *
     * @param o_dgl The datagram channel established from this request.
     */
    virtual void ListenerReady( PtrDelegate< NetDatagramChannel * > o_dgl ) = 0;
};

/**
 * This typedef is what you should pass pointers to and inherit from,
 * rather than NetDatagramLeadIf (which, by itself, lacks the lead-related
 * methods).
 * REV: @@ TODO -- there are ways to enforce this policy
 */
typedef LeadTwinMT<NetDatagramLeadIf> NetDatagramLead;

// ======== GetHostName ===========================================
/**
 * GetHostName returns the machine name of the local host.
 *
 * @return a std::string containing the host name.
 */

AUSTRIA_EXPORT std::string GetHostName();


/**
 * The Net class provides the starting point for the cross-platform network
 * interface.
 */
class AUSTRIA_EXPORT Net
  : public virtual PtrTarget_MT
{
public:
    virtual ~Net() {}


    /**
     * Resolve takes a host name and creates a NetAddress.
     *
     *
     * @param i_lead is the interface that accepts a response
     *              from the resolver.
     * @param i_address Is the address to be resolved
     */
    virtual void Resolve(
        NetResolverLead                     * i_lead,
        PtrDelegate< const NetAddress * >     i_address
    ) = 0;

    /**
     * ListenDatagram attempts to establish a listening endpoint for
     * datagrams.  This is usually UDP but the interface is
     * not specific to UDP, this is determined by the address
     * used to establish the connection.
     *
     * @param i_lead        The lead that is notified when the endpoint is established
     * @param i_address     The address to listen to
     * @param i_parameters  Parameters to apply to the listen request
     * @param i_buffer_pool The buffer pool to use for this datagram listener
     */
    virtual void ListenDatagram(
        NetDatagramLead                     * i_lead,
        PtrDelegate< const NetAddress * >     i_address,
        PtrDelegate< const NetParameters * >  i_parameters,
        PtrDelegate< Pool * >                 i_buffer_pool = 0
    ) = 0;

    /**
     * Listen is used to establish a listener for connectionions.
     * This method is for state-full connections (e.g. TCP or
     * higher level connections like HTTP).
     *
     * @param i_lead        The lead that is notified when a connection is established
     * @param i_address     The address to listen to
     * @param i_parameters  Parameters to apply to the listen request
     */
    virtual void Listen(
        NetConnectLead                      * i_lead,
        PtrDelegate< const NetAddress * >     i_address,
        PtrDelegate< const NetParameters * >  i_parameters
    ) = 0;

    /**
     * Connect will perform a "connect" operation asynchronously.
     * This method is for state-full connections (e.g. TCP or
     * higher level connections like HTTP).  In this case, the connection
     * notifies the lead exactly once of connection status.
     *
     * @param i_lead        The lead that is notified when a connection is established
     * @param i_address     The address to listen to
     * @param i_parameters  Parameters to apply to the listen request
     */
    virtual void Connect(
        NetConnectLead                        * i_lead,
        PtrDelegate< const NetAddress * >       i_address,
        PtrDelegate< const NetParameters * >    i_parameters
    ) = 0;

    /**
     * GetActivityListOwner returns the activity list owner for
     * the activity list for the thread inside the Net object.
     *
     * @return A pointer to the ActivityListOwner
     */
    virtual PtrDelegate< at::ActivityListOwner * > GetActivityListOwner() = 0;
};


} // namespace at

#endif // #ifndef x_at_net_h_x
