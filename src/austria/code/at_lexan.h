/**
 * \file at_lexan.h
 *
 */


#ifndef x_at_lexan_h_x
#define x_at_lexan_h_x 1

#include "at_lifetime_mt.h"
#include "at_rangemap.h"
#include "at_types.h"
#include <string>

namespace at
{
//

template <typename w_ScanType, typename w_StringType=std::basic_string<w_ScanType> >
class LexExprNodes;

template <typename w_ScanType, typename w_NodeType=LexExprNodes<w_ScanType> >
class LexRange;

template <typename w_ScanType, typename w_NodeType=LexExprNodes<w_ScanType> >
class LexExpr;


// ======== LexExprNodes ==================================================
/**
 * LexExprNodes defines all the types of nodes in an expression graph.
 *
 */

template <typename w_ScanType, typename w_StringType >
class LexExprNodes
{
    public:

    /**
     * t_ScanType is the type being analysed.
     */
    typedef w_ScanType      t_ScanType;

    /**
     * t_StringType is the string storage class for string nodes.  std::vector
     * or std::basic_string are supportable.
     */
    typedef w_StringType    t_StringType;

    // ======== BinaryOperators =======================================
    /**
     * BinaryOperators is an enum to distinguish the various binary
     * operators that are supported for regular expression.
     *
     */

    enum BinaryOperators
    {

        /**
         * Concatenation will concatenate two expressions
         *
         */
        Concatenation,

        /**
         * Either will create an expression that will match either
         * of the 
         */
        Either,

        /**
         * Context is a binary operator that matches the first
         * part when followed by the second part.  The second
         * part does not form part of the matched result and is
         * matched subsequently.
         */
        Context,

        /**
         * NoReturn defines a point at which the espression will
         * match and input matched to this point and further
         * will either be discarded or matched as part of this expression.
         */
        NoReturn,

        /**
         * CountedRepeat is for repeating an expression - >> indicates
         * repeat for 
         */
        CountedRepeat,
    };

    

    class LexExprProcessor;

    // ======== NodeBase ==============================================
    /**
     * Base class for all nodes of an expression
     *
     */

    class NodeBase
      : public PtrTarget_MT
    {
        public:


        // ======== Process ===========================================
        /**
         * Each type of node needs to define this function to call the
         * appropriate LexExprProcess::Process method.
         *
         * @param i_process
         * @return nothing
         */

        virtual void Process( LexExprProcessor & i_process ) const = 0;
        virtual void Process( LexExprProcessor & i_process ) = 0;
        
    };


    // ======== Node_Str ==============================================
    /**
     * Node_Str is a string node.  The meaning from a regular expression
     * perspective is that the string is matched.
     */

    class Node_Str
      : public NodeBase
    {
        public:

        /**
         * Node_Str
         *
         */
        Node_Str(
            const t_StringType &  i_string
        )
          : m_string( i_string )
        {
        }

        Node_Str(
            const w_ScanType & i_string
        )
          : m_string( 1, i_string )
        {
        }

        virtual void Process( LexExprProcessor & i_process ) const
        {
            i_process.Process( * this );
        }
        
        virtual void Process( LexExprProcessor & i_process )
        {
            i_process.Process( * this );
        }
        

        /**
         * m_node_str
         */
        const t_StringType      m_string;

    };

    static at::PtrDelegate< NodeBase * > String( const t_StringType & i_string )
    {
        return new Node_Str( i_string );
    }

    static at::PtrDelegate< NodeBase * > String( const w_ScanType * i_string )
    {
        return new Node_Str( i_string );
    }

    static at::PtrDelegate< NodeBase * > String( const w_ScanType & i_string )
    {
        return new Node_Str( i_string );
    }

    // ======== Node_Range ==============================================
    /**
     * Node_Range is for a class of input.
     */

    class Node_Range
      : public NodeBase
    {
        public:

        /**
         * Node_Range
         *
         */
        Node_Range()
          : m_not_in_range( false )
        {
        }

        virtual void Process( LexExprProcessor & i_process ) const
        {
            i_process.Process( * this );
        }

        virtual void Process( LexExprProcessor & i_process )
        {
            i_process.Process( * this );
        }

        void AddRange( const w_ScanType & i_v1, const w_ScanType & i_v2 )
        {
            m_map.AddRange( i_v1, i_v2 );
        }

        void AddRange( const w_ScanType & i_v1 )
        {
            m_map.AddRange( i_v1, i_v1 );
        }

        void Invert()
        {
            m_not_in_range = ! m_not_in_range;
        }
        

        /**
         * range node
         */
        bool                                m_not_in_range;
        at::RangeMap<w_ScanType>            m_map;

    };

    static at::PtrDelegate< Node_Range * > NewRange()
    {
        return new Node_Range();
    }

    static at::PtrDelegate< Node_Range * > NewRange( const Node_Range & i_node_range)
    {
        return new Node_Range( i_node_range );
    }


    // ======== Node_Binary ===========================================
    /**
     * Node_Binary is a template class that supports a binary operators.
     *
     */

    template <BinaryOperators w_operator>
    class Node_Binary
      : public NodeBase
    {
        public:

        /**
         * Node_Binary
         *
         */
        Node_Binary(
            at::PtrDelegate< NodeBase * >   i_lhs,
            at::PtrDelegate< NodeBase * >   i_rhs
        )
          : m_lhs( i_lhs ),
            m_rhs( i_rhs )
        {
        }

        virtual void Process( LexExprProcessor & i_process ) const
        {
            i_process.Process( * this );
        }
        
        virtual void Process( LexExprProcessor & i_process )
        {
            i_process.Process( * this );
        }
        
        /**
         * The left hand side and right hand side of the operator
         */
        at::Ptr< NodeBase * >               m_lhs;
        at::Ptr< NodeBase * >               m_rhs;

    };

    
    template <BinaryOperators w_operator>
    static at::PtrDelegate< NodeBase * > Binary(
        at::PtrDelegate< NodeBase * >       i_lhs,
        at::PtrDelegate< NodeBase * >       i_rhs
    )
    {
        return new Node_Binary<w_operator>( i_lhs, i_rhs );
    }

    
    // ======== Node_Repeat ===========================================
    /**
     * Node_Repeat is a node for supporting repeating of expressions
     */

    class Node_Repeat
      : public NodeBase
    {
        public:

        /**
         * Node_Repeat
         * @param i_lhs The expression being matched
         * @param i_at_least At least this many repeats
         */
        Node_Repeat(
            at::PtrDelegate< NodeBase * >   i_lhs,
            unsigned                        i_at_least
        )
          : m_lhs( i_lhs ),
            m_at_least( i_at_least ),
            m_at_most( 0 ),
            m_infinite( true )
        {
        }

        /**
         * Node_Repeat
         * @param i_lhs The expression being matched
         * @param i_at_least At least this many repeats
         * @param i_at_most At most this many repeats
         */
        Node_Repeat(
            at::PtrDelegate< NodeBase * >   i_lhs,
            unsigned                        i_at_least,
            unsigned                        i_at_most
        )
          : m_lhs( i_lhs ),
            m_at_least( i_at_least ),
            m_at_most( i_at_most ),
            m_infinite( true )
        {
        }

        virtual void Process( LexExprProcessor & i_process ) const
        {
            i_process.Process( * this );
        }
        
        virtual void Process( LexExprProcessor & i_process )
        {
            i_process.Process( * this );
        }
        
        /**
         * The left hand side and right hand side of the operator
         */
        at::Ptr< NodeBase * >               m_lhs;
        unsigned                            m_at_least;
        unsigned                            m_at_most;
        bool                                m_infinite;

    };

    static at::PtrDelegate< NodeBase * > NewRepeat(
        at::PtrDelegate< NodeBase * >       i_lhs,
        unsigned                            i_thismany_min,
        unsigned                            i_thismany_max
    ) {
        return new Node_Repeat( i_lhs, i_thismany_min, i_thismany_max );
    }

    static at::PtrDelegate< NodeBase * > NewRepeat(
        at::PtrDelegate< NodeBase * >       i_lhs,
        unsigned                            i_thismany_min
    ) {
        return new Node_Repeat( i_lhs, i_thismany_min );
    }

    
    
    // ======== LexExprProcessor ============================================
    /**
     * LexExprProcessor
     *
     */
    
    class LexExprProcessor
    {
        public:
    
        virtual ~LexExprProcessor() {}

        /**
         * A process method for all the types.
         */

        virtual void	Process( Node_Str & i_node ) = 0;
        virtual void	Process( const Node_Str & i_node ) = 0;
        virtual void	Process( Node_Range & i_node ) = 0;
        virtual void	Process( const Node_Range & i_node ) = 0;
        virtual void	Process( Node_Repeat & i_node ) = 0;
        virtual void	Process( const Node_Repeat & i_node ) = 0;
        virtual void	Process( Node_Binary<Concatenation> & i_node ) = 0;
        virtual void	Process( const Node_Binary<Concatenation> & i_node ) = 0;
        virtual void	Process( Node_Binary<Either> & i_node ) = 0;
        virtual void	Process( const Node_Binary<Either> & i_node ) = 0;
        virtual void	Process( Node_Binary<Context> & i_node ) = 0;
        virtual void	Process( const Node_Binary<Context> & i_node ) = 0;
        virtual void	Process( Node_Binary<NoReturn> & i_node ) = 0;
        virtual void	Process( const Node_Binary<NoReturn> & i_node ) = 0;
    
    };

    class LexExprProcessor_Basic
      : public LexExprProcessor
    {
        public:
    
        /**
         * A process method for all the types.
         */

        virtual void	Process( Node_Str & i_node ) {}
        virtual void	Process( const Node_Str & i_node ) {}
        virtual void	Process( Node_Range & i_node ) {}
        virtual void	Process( const Node_Range & i_node ) {}
        virtual void	Process( Node_Repeat & i_node ) {}
        virtual void	Process( const Node_Repeat & i_node ) {}
        virtual void	Process( Node_Binary<Concatenation> & i_node ) {}
        virtual void	Process( const Node_Binary<Concatenation> & i_node ) {}
        virtual void	Process( Node_Binary<Either> & i_node ) {}
        virtual void	Process( const Node_Binary<Either> & i_node ) {}
        virtual void	Process( Node_Binary<Context> & i_node ) {}
        virtual void	Process( const Node_Binary<Context> & i_node ) {}
        virtual void	Process( Node_Binary<NoReturn> & i_node ) {}
        virtual void	Process( const Node_Binary<NoReturn> & i_node ) {}
    
    
    };

    // ======== NodeCast_Tool ===========================================
    /* LexExprCast will perform a safe downcast of a node to it's 
     * particular type.
     */

    template <typename w_NodeType>
    class NodeCast_Tool
      : LexExprProcessor_Basic
    {
        public:

        typedef w_NodeType                              t_NodeType;
        typedef typename NonConst< t_NodeType >::t_Type t_NonConstNodeType;

        /**
         * t_ParamType becomes const NodeBase if w_NodeType is const, otherwise
         * t_ParamType is NodeBase
         */
        typedef typename TypeSelect<
            IsSame< t_NodeType, t_NonConstNodeType >::m_value,
            NodeBase,
            const NodeBase
        >::t_Type   t_ParamType;

        /**
         * NodeCast_Tool
         *
         */
        NodeCast_Tool(
            at::PtrView< t_ParamType * >   i_node
        )
        {
            i_node->Process( *this );
        }

        /**
         * Conversion operator to result.
         */
        template <typename w_Result>
        operator w_Result ()
        {
            return m_node;
        }

        /**
         * Process is called by the node's virtual method.
         */
        private:
        void    Process( w_NodeType & i_node )
        {
            m_node = & i_node;
        }
        /**
         * Pointer to node -
         */
        at::PtrView< w_NodeType * >       m_node;

    };


    // ======== NodeCast ==============================================
    /**
     * NodeCast performs a downcast through a conversion operator.
     * e.g.:
     * at::Ptr< Node_Str * > x = NodeCast( node_ptr );
     *
     */

    template <typename w_Type>
    class NodeCast_Basic
    {
        public:

        /**
         * NodeCast
         *
         */
        NodeCast_Basic(
            w_Type      & i_node_ptr
        )
          : m_node_ptr( i_node_ptr )
        {
        }

        w_Type          & m_node_ptr;


        template <typename w_TypeResult>
        operator at::Ptr< w_TypeResult *> ()
        {
            return NodeCast_Tool<w_TypeResult>( m_node_ptr );
        }

        template <typename w_TypeResult>
        operator at::PtrView< w_TypeResult *> ()
        {
            return NodeCast_Tool<w_TypeResult>( m_node_ptr );
        }

        template <typename w_TypeResult>
        operator at::PtrDelegate< w_TypeResult *> ()
        {
            return NodeCast_Tool<w_TypeResult>( m_node_ptr );
        }

        template <typename w_TypeResult>
        operator w_TypeResult * ()
        {
            return NodeCast_Tool<w_TypeResult>( m_node_ptr );
        }

    };

    template <typename w_Type>
    static NodeCast_Basic<w_Type> NodeCast( w_Type & i_node )
    {
        return NodeCast_Basic<w_Type>( i_node );
    }

    class LexExprPrinter_Basic
      : public LexExprProcessor
    {
        public:
    
        /**
         * A process method for all the types.
         */

        template <typename T>
        void ConstProcess( T & i_node )
        {
            const T & l_const_node = i_node;

            Process( l_const_node );
        }


        virtual void	Process( Node_Str & i_node )
        {
            ConstProcess( i_node );
        }
        
        virtual void	Process( const Node_Str & i_node ) {}

        
        virtual void	Process( Node_Range & i_node )
        {
            ConstProcess( i_node );
        }
        
        virtual void	Process( const Node_Range & i_node ) {}

        
        virtual void	Process( Node_Repeat & i_node )
        {
            ConstProcess( i_node );
        }
        
        virtual void	Process( const Node_Repeat & i_node ) {}


        virtual void	Process( Node_Binary<Concatenation> & i_node )
        {
            ConstProcess( i_node );
        }
        
        virtual void	Process( const Node_Binary<Concatenation> & i_node ) {}

        
        virtual void	Process( Node_Binary<Either> & i_node )
        {
            ConstProcess( i_node );
        }
        
        virtual void	Process( const Node_Binary<Either> & i_node ) {}

        
        virtual void	Process( Node_Binary<Context> & i_node )
        {
            ConstProcess( i_node );
        }
        
        virtual void	Process( const Node_Binary<Context> & i_node ) {}

        
        virtual void	Process( Node_Binary<NoReturn> & i_node )
        {
            ConstProcess( i_node );
        }
        
        virtual void	Process( const Node_Binary<NoReturn> & i_node ) {}
    
    
    };

    
};




// ======== LexExprRoot ===============================================
/**
 * 
 *
 */

template <typename w_ScanType, typename w_NodeType=LexExprNodes<w_ScanType> >
class LexExprRoot
{
    public:

    /**
     * t_ScanType is the type being analysed.
     */
    typedef w_ScanType      t_ScanType;
    
    /**
     * t_NodeType is the type being analysed.
     */
    typedef w_NodeType      t_NodeType;

};


template <typename w_ScanType>
class LexExprProcess;

// ======== LexExprBase ===============================================
/**
 * 
 *
 */

template <typename w_ScanType, typename w_NodeType=LexExprNodes<w_ScanType> >
class LexExprBase
  : public LexExprRoot< w_ScanType, w_NodeType >
{
    public:

    typedef LexExprRoot< w_ScanType, w_NodeType >   t_ExprRoot;
    
    /**
     * t_ScanType is the type being analysed.
     */
    typedef typename t_ExprRoot::t_ScanType         t_ScanType;
    
    /**
     * t_NodeType is the type being analysed.
     */
    typedef typename t_ExprRoot::t_NodeType         t_NodeType;
    
    typedef typename t_NodeType::NodeBase           t_NodeBase;

    LexExprBase( at::PtrDelegate< t_NodeBase * > i_expression )
      : m_expression( i_expression )
    {
    }

    LexExprBase( const LexRange<t_ScanType, t_NodeType> & i_range_type )
      : m_expression( w_NodeType::Range( i_range_type ) )
    {
    }

    /**
     * Any number of
     */
    LexExpr<t_ScanType, w_NodeType> operator++( int )
    {
        return MatchAtLeast( 0 );
    }

    /**
     * Match will match exactly the number of expression
     * @param i_thismany The number of times to match
     */
    LexExpr<t_ScanType, w_NodeType> Match( unsigned i_thismany )
    {
        return LexExpr<t_ScanType, w_NodeType>( w_NodeType::NewRepeat( m_expression, 0, i_thismany ) );
    }

    /**
     * Match will match a range of times
     * @param i_thismany_min The minimum number of times to match
     * @param i_thismany_max The maximum number of times to match
     */
    LexExpr<t_ScanType, w_NodeType> Match( unsigned i_thismany_min, unsigned i_thismany_max )
    {
        return LexExpr<t_ScanType, w_NodeType>( w_NodeType::NewRepeat( m_expression, i_thismany_min, i_thismany_max ) );
    }

    /**
     * Indicates at least this many of this expr to match
     * @param i_thismany_min The minimum number of times to match
     */
    LexExpr<t_ScanType, w_NodeType> MatchAtLeast( unsigned i_thismany_min )
    {
        return LexExpr<t_ScanType, w_NodeType>( w_NodeType::NewRepeat( m_expression, i_thismany_min ) );
    }

    /**
     * m_expression contains the expression tree root.
     */
    at::Ptr< t_NodeBase * >                     m_expression;

};

// ======== LexExpr ===================================================
/**
 * 
 *
 */

template <typename w_ScanType, typename w_NodeType >
class LexExpr
  : public LexExprBase< w_ScanType, w_NodeType >
{
    public:

    typedef LexExprBase< w_ScanType, w_NodeType >   t_Base;
    typedef typename t_Base::t_ScanType             t_ScanType;
    typedef typename t_Base::t_NodeType             t_NodeType;

    LexExpr( const LexExprBase<w_ScanType, w_NodeType> & i_expression )
      : t_Base( i_expression )
    {
    }

    LexExpr( const LexRange<w_ScanType, w_NodeType> & i_expression )
      : t_Base( i_expression )
    {
    }

    LexExpr( at::PtrDelegate< typename t_Base::t_NodeBase * > i_expression )
      : t_Base( i_expression )
    {
    }

    LexExpr( const w_ScanType & i_char )
      : t_Base(  )
    {
    }

    LexExpr( const w_ScanType * i_str )
      : t_Base( t_NodeType::String( i_str )  )
    {
    }

};


// ======== LexRange ==================================================
/**
 * Defines a regular expression "class".
 *
 */

template <typename w_ScanType, typename w_NodeType >
class LexRange
  : public LexExprBase< w_ScanType, w_NodeType >
{
    public:

    typedef LexExprBase< w_ScanType, w_NodeType >   t_Base;
    typedef typename t_Base::t_ScanType             t_ScanType;
    typedef typename t_Base::t_NodeType             t_NodeType;

    typedef at::Ptr< typename t_NodeType::Node_Range * > t_RangeType;

    t_RangeType GetRange()
    {
        at::PtrView< typename t_NodeType::Node_Range * > l_range = t_NodeType::NodeCast( this->m_expression );

        if ( ! l_range )
        {
            // abort here - maybe thow - abort for now
            AT_Abort();
        }

        if ( IsRefCountOne( this->m_expression ) )
        {
            return l_range;
        }

        at::Ptr< typename t_NodeType::Node_Range * > l_wrange = t_NodeType::NewRange( * l_range );
        this->m_expression = l_wrange;
        
        return l_wrange;
    }
    
    LexRange( const w_ScanType & i_v1, const w_ScanType & i_v2 )
      : LexExprBase< w_ScanType, w_NodeType >( t_NodeType::NewRange() )
    {
        (*this)( i_v1, i_v2 );
    }

    LexRange & operator()( const w_ScanType & i_v1, const w_ScanType & i_v2 )
    {
        t_RangeType l_ptr = GetRange();
        l_ptr->AddRange( i_v1, i_v2 );
        return * this;
    }

    LexRange & operator()( const w_ScanType & i_v1 )
    {
        t_RangeType l_ptr = GetRange();
        l_ptr->AddRange( i_v1, i_v1 );
        return * this;
    }
    
    LexRange & Not()
    {
        t_RangeType l_ptr = GetRange();
        l_ptr->Invert();
        return * this;
    }

};

#define DefineOperators( OPERATOR, NODETYPE )                           \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexExpr<w_ScanType, w_NodeType> & i_lhs, const LexExpr<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, i_rhs.m_expression ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexExpr<w_ScanType, w_NodeType> & i_lhs, const w_ScanType & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, w_NodeType::String( i_rhs ) ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const w_ScanType & i_lhs, const LexExpr<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( w_NodeType::String( i_lhs ), i_rhs.m_expression  ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexExpr<w_ScanType, w_NodeType> & i_lhs, const w_ScanType * i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, w_NodeType::String( i_rhs )  ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const w_ScanType * i_lhs, const LexExpr<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( w_NodeType::String( i_lhs ), i_rhs.m_expression  ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexExpr<w_ScanType, w_NodeType> & i_lhs, const LexRange<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, i_rhs.m_expression ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexRange<w_ScanType, w_NodeType> & i_lhs, const LexExpr<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, i_rhs.m_expression ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexRange<w_ScanType, w_NodeType> & i_lhs, const LexRange<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, i_rhs.m_expression ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexRange<w_ScanType, w_NodeType> & i_lhs, const w_ScanType & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, w_NodeType::String( i_rhs ) ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const w_ScanType & i_lhs, const LexRange<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( w_NodeType::String( i_lhs ), i_rhs.m_expression ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const LexRange<w_ScanType, w_NodeType> & i_lhs, const w_ScanType * i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( i_lhs.m_expression, w_NodeType::String( i_rhs ) ); \
}                                                                        \
                                                                         \
template <typename w_ScanType, typename w_NodeType>                      \
LexExpr<w_ScanType, w_NodeType> operator OPERATOR( const w_ScanType * i_lhs, const LexRange<w_ScanType, w_NodeType> & i_rhs ) \
{                                                                        \
    return w_NodeType::template Binary< w_NodeType::NODETYPE >( w_NodeType::String( i_lhs ), i_rhs.m_expression ); \
}                                                                        \
// End Macro

DefineOperators(+, Concatenation)
DefineOperators(|, Either)
DefineOperators(||, NoReturn)
DefineOperators(&&, Context)

#undef DefineOperators


// ======== LexBasicTraits =================================================
/**
 * LexBasicTraits describes a basic set of lexical components.
 *
 */

template <typename w_InTokenType, typename w_OutTokenType>
class LexBasicTraits
{
    public:

    typedef w_InTokenType       t_InTokenType;
    typedef w_OutTokenType      t_OutTokenType;
    

    

};



// ======== LexAnalyser ===============================================
/**
 * LexAnalyser contains the complete description of the lexical system
 * and can process it into a state machine.
 *
 */

template <typename w_Traits>
class LexAnalyser
{
    public:

    /**
     * ~LexAnalyser has a virtual destructor.
     */
    virtual ~LexAnalyser() {}

    /**
     * t_Traits is a typedef to the traits class used for this scanner.
     */
    typedef w_Traits                                t_Traits;

    /**
     * t_InTokenType defines the input token type for this scanner.
     */
    typedef typename t_Traits::t_InTokenType        t_InTokenType;

    /**
     * t_OutTokenType defines the output token type for this scanner.
     */
    typedef typename t_Traits::t_OutTokenType       t_OutTokenType;
    
    // ======== Terminal ==============================================
    /**
     * 
     *
     */

    class Terminal
    {
        public:

    };

    // ======== Add ===================================================
    /**
     * Add a rule to the lexical analyser system
     *
     * @param i_expression The expression of the rule being added.
     * @return nothing
     */

    Terminal Add(
        const LexExpr<t_InTokenType>            & i_input_type,
        const t_OutTokenType                    & o_output_type
    ) {
        return Terminal();
    }

};


// ======== LexProcessor ==============================================
/**
 * This provdes the basic processor logic for the lexical scanner.
 *
 */

template <typename w_Analyser, typename w_Context>
class LexProcessor
{
    public:
    
    /**
     * t_Analyser is a typedef to the Analyser class used for this processor.
     */
    typedef w_Analyser                              t_Analyser;


    /**
     * t_Context is the typedef of the context class.
     */
    typedef w_Context                               t_Context;
    
    /**
     * t_Traits is a typedef to the traits class used for this scanner.
     */
    typedef typename t_Analyser::t_Traits           t_Traits;

    /**
     * t_InTokenType defines the input token type for this scanner.
     */
    typedef typename t_Analyser::t_InTokenType      t_InTokenType;

    /**
     * t_OutTokenType defines the output token type for this scanner.
     */
    typedef typename t_Analyser::t_OutTokenType     t_OutTokenType;
    

    // ======== LexProcessor ==========================================
    /**
     * LexProcessor constructor takes an analyser and a context.
     *
     * @param i_analyser    The analyser system used for this processor
     * @param i_context     The context to be used for this scanner
     * @return nothing
     */

    LexProcessor(
        const t_Analyser            & i_analyser,
        t_Context                   & i_context
    );


    // ======== Process ===============================================
    /**
     * Process data coming from
     *
     * @param i_token
     * @return True if the token was processed.
     */

    bool Process(
        const t_InTokenType     & i_token
    );
    
    
};


} // namespace at


#endif


