/**
 *  at_crypto.h
 *
 */


#ifndef x_at_crypto_h_x
#define x_at_crypto_h_x 1

#include "at_buffer.h"
#include "at_any.h"
#include "at_lifetime.h"
#include "at_lifetime_mt.h"
#include <string>

namespace at
{

// forward declare the Cipher class
class Cipher;


// ======== Cipher_Exception ==========================================
/**
 * ExceptionDerivation<Cipher_Exception> is the base class for
 * for all austria crypto exceptions.
 *
 */

struct AUSTRIA_EXPORT Cipher_Exception {};


// ======== Cipher_BadKey =============================================
/**
 * ExceptionDerivBase<Cipher_BadKey> is thrown when SetKey is unable
 * to use the given key.
 *
 */

struct AUSTRIA_EXPORT Cipher_BadKey { typedef Cipher_Exception t_Base; };

// ======== Cipher_ModeNotSupported ===================================
/**
 * ExceptionDerivBase<Cipher_ModeNotSupported> is thrown when SetKey 
 * is called and the Cipher does not support the requested mode.
 *
 */

struct AUSTRIA_EXPORT Cipher_ModeNotSupported { typedef Cipher_Exception t_Base; };

// ======== Cipher_BadCipherParameters ================================
/**
 * ExceptionDerivBase<Cipher_BadCipherParameters> is thrown when
 * the cipher failed to work because of bad parameters.
 *
 */

struct AUSTRIA_EXPORT Cipher_BadCipherParameters { typedef Cipher_Exception t_Base; };

// ======== Cipher_BadInputData =======================================
/**
 * ExceptionDerivBase<Cipher_BadInputData> is thrown when the input
 * data length is not correct.
 */

struct AUSTRIA_EXPORT Cipher_BadInputData { typedef Cipher_Exception t_Base; };


// ======== Cipher_IVectorNotNeeded ===================================
/**
 * ExceptionDerivBase<Cipher_IVectorNotNeeded> is thrown when
 * SetInitVector is called and an initialization vector is not
 * used for that cipher.
 */

struct AUSTRIA_EXPORT Cipher_IVectorNotNeeded { typedef Cipher_Exception t_Base; };


// ======== Cipher_BadInitVector ======================================
/**
 * ExceptionDerivBase<Cipher_BadInitVector> is thrown when the
 * SetInitVector is called and an initialization vector is not
 * the right length.
 *
 */
struct AUSTRIA_EXPORT Cipher_BadInitVector { typedef Cipher_Exception t_Base; };


// ======== CipherProperty ============================================
/**
 * CipherProperty is returned from Cipher::GetProperty and depends
 * on the property being requested.
 *
 */

typedef Any<>           CipherProperty;

// ======== s_no_property =============================================
/**
 * s_no_property is returned from GetProperty is called and the
 * property is not supported
 *
 */

extern const CipherProperty & s_no_property;

// ======== CipherGenPropertyId =========================================
/**
 * CipherGenPropertyId are properties for CipherSymmetricKey or
 * CipherPublicKey.
 *
 */

class AUSTRIA_EXPORT CipherGenPropertyId
{
    public:

    virtual ~CipherGenPropertyId() {}


    // ======== GetId =================================================
    /**
     * GetId returns the identifier number of a property.
     *
     * @return An ID
     */

    virtual unsigned GetId() const = 0;
    
    
    // ======== s_InitVectorNeeded ========================================
    /**
     * This property is a bool that indicates true if the cipher uses
     * an initialization vector.
     *
     */
    static const CipherGenPropertyId & s_InitVectorNeeded;


    private:
    CipherGenPropertyId & operator=( const CipherGenPropertyId & );
    CipherGenPropertyId( const CipherGenPropertyId & );
};


// ======== CipherSymmetricKey ========================================
/**
 * CipherSymmetricKey is a key generator for a symmetric key Cipher.
 *
 */

class AUSTRIA_EXPORT CipherSymmetricKey
  : public virtual PtrTarget_MT
{
    public:


    // ======== InitVectorLength ======================================
    /**
     * InitVectorLength returns 0 if no init vector is needed or
     * the length of the required init vector.
     *
     * @return 0 if none or length
     */

    virtual SizeMem InitVectorLength() = 0;

    // ======== KeyLength =============================================
    /**
     * KeyLength returns 0 if the key can be a custom length or a
     * value indicating the required length of the key.
     *
     * @return nothing
     */

    virtual SizeMem KeyLength() = 0;

    // ======== SetCipherParameters =======================================
    /**
     * SetCipherParameters is used to add extra information about this key.
     * For example : SetCipherParameters( "RSA:512:65537" ) will set the key
     * to a 512 bit key with an exponent of 65537.
     *
     * @param i_key_params The key parameters
     * @return nothing
     */

    virtual void	SetCipherParameters(
        const std::string           & i_key_params
    ) = 0;

    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this key
     * generator.
     *
     * @param i_property The requested property identifer
     * @return A CipherProperty "any" type
     */

    virtual CipherProperty GetProperty(
        const CipherGenPropertyId   & i_property
    ) = 0;
    
    // ======== SetProperty ===========================================
    /**
     * SetProperty can set a property on the key generator.
     *
     * @param i_property The requested property identifer
     * @param o_value The at::Any object to write the value of the requested property
     * @return True if the property was set
     */

    virtual bool SetProperty(
        const CipherGenPropertyId   & i_property,
        CipherProperty              & i_value
    ) = 0;
    
    // ======== GenerateKey ===========================================
    /**
     * GenerateKey will create a key for the Cipher associated with
     * this key.
     *
     * @return nothing
     */

    virtual std::string GenerateKey() = 0;


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses a std::string parameter.
     *
     * @param o_new_key  A new key is written to this param
     * @return nothing
     */

    virtual void	GenerateKey(
        std::string             & o_new_key
    ) = 0;


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses Buffer parameters
     *
     * @param o_new_key     A new key is written to this param
     * @return nothing
     */

    virtual void	GenerateKey(
        PtrView<Buffer *>         o_new_public_key
    ) = 0;

    // ======== GenerateInitVector ====================================
    /**
     * GenerateInitVector is called when an ivector is needed
     *
     *
     * @param o_init_vector The initialization vector is placed
     *                      in this location.
     * @return nothing
     */

    virtual void	GenerateInitVector(
        PtrView<Buffer *>         o_init_vector
    ) = 0;    

    // ======== GenerateInitVector ====================================
    /**
     * GenerateInitVector is called when an initialization vector is needed
     *
     * @return The initialization vector or an empty string if none is required
     */

    virtual std::string GenerateInitVector() = 0;

    // ======== GenerateInitVector ====================================
    /**
     * GenerateInitVector is called when an ivector is needed
     *
     *
     * @param o_init_vector The initialization vector is placed
     *                      in this location.
     * @return nothing
     */

    virtual void	GenerateInitVector(
        std::string             & o_init_vector
    ) = 0;    

    // ======== NewCipher =============================================
    /**
     * NewCipher returns a new cipher associated with this key generator
     *
     * @return nothing
     */

    virtual PtrDelegate<Cipher *> NewCipher() = 0;

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier() = 0;

};


// ======== CipherPublicKey ===========================================
/**
 * CipherPublicKey is a key generator for the public key cryptosystem.
 *
 */

class AUSTRIA_EXPORT CipherPublicKey
  : public virtual PtrTarget_MT
{
    public:

    // ======== SetCipherParameters =======================================
    /**
     * SetCipherParameters is used to add extra information about this key.
     * For example : SetCipherParameters( "RSA:512:65537" ) will set the key
     * to a 512 bit key with an exponent of 65537.
     *
     * @param i_key_params The key parameters
     * @return nothing
     */

    virtual void	SetCipherParameters(
        const std::string           & i_key_params
    ) = 0;

    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this key
     * generator.
     *
     * @param i_property The requested property identifer
     * @return A CipherProperty "any" type
     */

    virtual CipherProperty GetProperty(
        const CipherGenPropertyId   & i_property
    ) = 0;
    
    // ======== SetProperty ===========================================
    /**
     * SetProperty can set a property on the key generator.
     *
     * @param i_property The requested property identifer
     * @param o_value The at::Any object to write the value of the requested property
     * @return True if the property was set
     */

    virtual bool SetProperty(
        const CipherGenPropertyId   & i_property,
        const CipherProperty        & i_value
    ) = 0;
    
    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses a std::string parameter.
     *
     * @param o_new_public_key  A new public key is written to this param
     * @param o_new_private_key A new public key is written here
     * @return nothing
     */

    virtual void	GenerateKey(
        std::string             & o_new_public_key,
        std::string             & o_new_private_key
    ) = 0;


    // ======== GenerateKey ===========================================
    /**
     * Overload of GenerateKey that uses Buffer parameters
     *
     * @param o_new_public_key  A new public key is written to this param
     * @param o_new_private_key A new public key is written here
     * @return nothing
     */

    virtual void	GenerateKey(
        PtrView<Buffer *>         o_new_public_key,
        PtrView<Buffer *>         o_new_private_key
    ) = 0;


    // ======== NewPublicCipher =============================================
    /**
     * NewPublicCipher returns a new cipher associated with this key generator
     * that takes a public key.
     *
     * @return nothing
     */

    virtual PtrDelegate<Cipher *> NewPublicCipher() = 0;

    // ======== NewPrivateCipher =============================================
    /**
     * NewPrivateCipher returns a new cipher associated with this key generator
     *
     * @return nothing
     */

    virtual PtrDelegate<Cipher *> NewPrivateCipher() = 0;

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier() = 0;
    
};


// ======== Cipher ====================================================
/**
 * This is the public cipher interface.  It these are generated from
 * a factory, seeded with a key and then can be used to encrypt or
 * decrypt data based on that key.
 *
 */

class AUSTRIA_EXPORT Cipher
  : public virtual PtrTarget_MT
{
    public:


    // ======== Mode ==================================================
    /**
     * Mode are the cipher modes
     *
     */

    enum Mode
    {
        /**
         * Scramble is the mode to scramble the input
         */
        Scramble,

        /**
         * Unscramble is the mode to decrypt the input
         */
        Unscramble 
    };


    // ======== Property ==============================================
    /**
     * Property is the list of Cipher properties that can be
     * requested.
     */

    enum Property
    {
        /**
         * IsSymmetric is the "symmetric" key cryptosystem property
         *
         */
        IsSymmetric,

        /**
         * IsPublic is the "public" key cryptosystem property
         *
         */
        IsPublic,

        /**
         * KeyGenerator returns a key generator for this Cipher.
         * The type of the key generator depends on the cipher type.
         *
         * GetProperty will return a CipherProperty object containing either
         * of the following types.
         *  at::Ptr<CipherSymmetricKey*>
         *  at::Ptr<CipherPublicKey*>
         *
         */

        KeyGenerator,

        /**
         * PublicKey returns a std::string containing a public key
         * if the cipher is a public key cipher.  A SetKey must be
         * called prior to calling GetProperty( PublicKey ).
         */
        PublicKey,
    };

    // ======== GetProperty ===========================================
    /**
     * GetProperty returns the requested property for this Cipher.
     *
     *
     * @param i_property is the requested property
     * @return A CipherProperty "any" type
     */

    virtual CipherProperty GetProperty(
        Property            i_property
    ) = 0;
    
    // ======== SetProperty ===========================================
    /**
     * SetProperty can set a property on the key generator.
     *
     * @param i_property The requested property identifer
     * @param o_value The at::Any object to write the value of the requested property
     * @return True if the property was set
     */

    virtual bool SetProperty(
        Property                      i_property,
        CipherProperty              & i_value
    ) = 0;

    // ======== SetCipherParameters =======================================
    /**
     * SetCipherParameters is used to add extra information about this key.
     * For example : SetCipherParameters( "RSA:512:65537" ) will set the key
     * to a 512 bit key with an exponent of 65537.
     *
     * @param i_key_params The key parameters
     * @return nothing
     */

    virtual void	SetCipherParameters(
        const std::string           & i_key_params
    ) = 0;

    // ======== SetKey ================================================
    /**
     * SetKey is typically the first call to the Cipher.  This sets the
     * key to use for this cipher.
     *
     * SetKey also sets the "directionality" of the cipher i.e. Scramble
     * or Unscramble.  A Cipher may only support one or the other in the
     * case of public keys.  It is up to the application to use the
     * appropriate sense.  If a key is incapable of performing the said
     * mode a 
     *
     * @param i_key     Contains the key to use
     * @param i_mode    The type of Cipher wanted
     * @return nothing
     */

    virtual void SetKey(
        const std::string   & i_key,
        Mode                  i_mode = Scramble
    ) = 0;


    // ======== SetKey ================================================
    /**
     * This is an overload of SetKey that takes an at::Buffer as a 
     * key.
     *
     * @param i_key     Contains the key to use
     * @return nothing
     */

    virtual void SetKey(
        PtrView<const Buffer *>       i_key,
        Mode                          i_mode = Scramble
    ) = 0;


    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * 
     * @return nothing
     */

    virtual void	SetInitVector(
        PtrDelegate<const Buffer *>     i_iv
    ) = 0;

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * @return nothing
     */

    virtual void	SetInitVector(
        const Buffer::t_ConstRegion     & i_iv
    ) = 0;

    // ======== SetInitVector =========================================
    /**
     * SetInitVector needs to be called with the initialization vector
     * on ciphers that require one.
     *
     * @param i_iv is the initialization vector
     * @return nothing
     */

    virtual void	SetInitVector(
        const std::string               & i_iv
    ) = 0;

    // ======== WriteTo ===============================================
    /**
     * WriteTo sets the the buffer to write (append) the output
     * of the encryption.  The contents of the buffer are incomplete
     * unless "Flush" is called.  However, if Flushed is called, the contents
     * the buffer can no longer be appended to hence WriteTo must be called
     * again with a new buffer to write the encryption contents.
     *
     * @param i_output  The buffer to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        PtrDelegate<Buffer *>         o_output
    ) = 0;


    // ======== WriteTo ===============================================
    /**
     * Overloaded version of WriteTo for a std::string writer.  The
     * string passed to WriteTo will be accessed by Cipher when
     * data is added.
     *
     * @param i_output  The std::string to write to.
     * @return nothing
     */

    virtual void	WriteTo(
        std::string                 & o_output
    ) = 0;


    // ======== Flush =================================================
    /**
     * Flush will finalize this cipher and pass the remaining data
     * to the output "WriteTo" buffer.
     *
     * @return nothing
     */

    virtual void Flush() = 0;
    

    // ======== AddData ===============================================
    /**
     * AddData will pass the added data through the Cipher
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        const Buffer::t_ConstRegion     & i_data
    ) = 0;

    // ======== AddData ===============================================
    /**
     * AddData will pass the added data through the Cipher
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        PtrView<const Buffer *>     i_data
    ) = 0;

    // ======== AddData ===============================================
    /**
     * Overload of AddData for std::string
     *
     *
     * @param i_data The data being added.
     * @return nothing
     */

    virtual void	AddData(
        const std::string     & i_key
    ) = 0;

    // ======== CipherIdentifier ======================================
    /**
     * CipherIdentifier returns the at::Factory register identifier
     * for this Cipher's key generator.
     *
     * @return nothing
     */

    virtual std::string CipherIdentifier() = 0;


    // ======== OnePass ===============================================
    /**
     * OnePass is a simplified helper class to use to encrypt or decrypt.
     *
     * @param i_key     The key to use.
     * @param i_data    The data to encrypt
     * @param i_mode    The type of Cipher wanted
     * @return nothing
     */

    std::string OnePass(
        const std::string           & i_key,
        const std::string           & i_data,
        Mode                          i_mode
    ) {
        std::string     l_result;
        
        SetKey( i_key, i_mode );

        WriteTo( l_result );

        AddData( i_data ); // this can be called multiple times

        Flush();

        return l_result;
    }    

    // ======== Encrypt ===============================================
    /**
     * Encrypt is a helper function that takes a single key and input
     * buffer and performs a encryption.
     *
     * @param i_key     The key to use.
     * @param i_data    The data to encrypt
     * @return nothing
     */

    std::string Encrypt(
        const std::string           & i_key,
        const std::string           & i_data
    ) {
        return OnePass( i_key, i_data, Scramble );
    }
    
    // ======== Decrypt ===============================================
    /**
     * Decrypt is a helper function that takes a single key and input
     * buffer and performs a decryption.
     *
     * @param i_key     The key to use.
     * @param i_data    The data to encrypt
     * @return nothing
     */

    std::string Decrypt(
        const std::string           & i_key,
        const std::string           & i_data
    ) {
        return OnePass( i_key, i_data, Unscramble );
    }
    
    // ======== OnePass ===============================================
    /**
     * OnePass is a simplified helper class to use to encrypt or decrypt.
     *
     * @param i_key     The key to use.
     * @param i_data    The data to encrypt
     * @param i_mode    The type of Cipher wanted
     * @return nothing
     */

    std::string OnePass(
        const std::string           & i_key,
        const std::string           & i_iv,
        const std::string           & i_data,
        Mode                          i_mode
    ) {
        std::string     l_result;
        
        SetKey( i_key, i_mode );

        SetInitVector( i_iv );

        WriteTo( l_result );

        AddData( i_data ); // this can be called multiple times

        Flush();

        return l_result;
    }    

    // ======== Encrypt ===============================================
    /**
     * Encrypt is a helper function that takes a single key and input
     * buffer and performs a encryption.
     *
     * @param i_key     The key to use.
     * @param i_iv      The initialization vector to use
     * @param i_data    The data to encrypt
     * @return nothing
     */

    std::string Encrypt(
        const std::string           & i_key,
        const std::string           & i_iv,
        const std::string           & i_data
    ) {
        return OnePass( i_key, i_iv, i_data, Scramble );
    }
    
    // ======== Decrypt ===============================================
    /**
     * Decrypt is a helper function that takes a single key and input
     * buffer and performs a decryption.
     *
     * @param i_key     The key to use.
     * @param i_iv      The initialization vector to use
     * @param i_data    The data to encrypt
     * @return nothing
     */

    std::string Decrypt(
        const std::string           & i_key,
        const std::string           & i_iv,
        const std::string           & i_data
    ) {
        return OnePass( i_key, i_iv, i_data, Unscramble );
    }
};


// ======== NewCipher =================================================
/**
 * NewCipher will look up the cipher registry for the cipher type in
 * question and then set up the cipher given the passed in key string.
 *
 * @param i_key_type_n_params
 * @return nothing
 */

extern AUSTRIA_EXPORT PtrDelegate<Cipher *> NewCipher(
    const std::string           & i_key_type_n_params
);

// ======== NewKeyGenSymmetric =================================================
/**
 * NewKeyGenSymmetric will look up the symmetric key generator registry for the
 * cipher type in question and then set up the cipher given the passed in key string.
 *
 * @param i_key_type_n_params
 * @return A symmetric cipher key generator
 */

extern AUSTRIA_EXPORT PtrDelegate<CipherSymmetricKey *> NewKeyGenSymmetric(
    const std::string           & i_key_type_n_params
);

// ======== NewKeyGenPublic =================================================
/**
 * NewKeyGenPublic will look up the symmetric key generator registry for the
 * cipher type in question and then set up the cipher given the passed in key string.
 *
 * @param i_key_type_n_params
 * @return A public cipher key generator
 */

extern AUSTRIA_EXPORT PtrDelegate<CipherPublicKey *> NewKeyGenPublic(
    const std::string           & i_key_type_n_params
);


} // end namespace


#endif // x_at_crypto_h_x

