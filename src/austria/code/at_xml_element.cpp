//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_element.cpp
 *
 * \author Guido Gherardi
 *
 */

#include "at_xml_element.h"
#include "at_exception.h"

#define AT_ThrowXmlHierarchyRequestErr(msg) AT_ThrowDerivation( at::XmlHierarchyRequestErr, msg )

// Austria namespace
namespace at
{
    std::string XmlElement::Name() const
    {
        return this->TagName();
    }

    unsigned short XmlElement::Type() const
    {
        return ELEMENT_NODE;
    }

    void XmlElement::CheckHierarchyRequest( PtrView< const XmlNode* > i_newChild ) const
    {
        switch ( i_newChild->Type() )
        {
        case ELEMENT_NODE: case TEXT_NODE: case COMMENT_NODE: case PROCESSING_INSTRUCTION_NODE:
        case CDATA_SECTION_NODE: case ENTITY_REFERENCE_NODE:
            break;
        default:
            AT_ThrowXmlHierarchyRequestErr( "i_newChild is of a type that is not allowed as a document child" );
        }
        XmlComposite::CheckHierarchyRequest( i_newChild );
    }

    PtrDelegate< XmlNode* > XmlElement::Clone( bool i_deep ) const
    {
        PtrDelegate< XmlElement* > l_clone( new XmlElement( OwnerDocument(), TagName() ) );
        l_clone->m_attributes = m_attributes;
        if ( i_deep )
        {
            PtrView< XmlNode* > l_child = FirstChild();
            while ( l_child )
            {
                l_clone->AppendChild( l_child->Clone( true ) );
                l_child = l_child->NextSibling();
            }
        }
        return l_clone;
    }

    XmlNodeList XmlElement::GetElementsByTagName( const std::string& i_tagName ) const
    {
        // Collect all the ELEMENT_NODE in the tree, starting with this element node.
        XmlNodeList l_elements;
        l_elements.push_back( const_cast< XmlElement* >( this ) );
        for ( size_t i = 0; i < l_elements.size(); i++ )
        {
            PtrView< XmlNode* > l_child = l_elements[i]->FirstChild();
            while ( l_child )
            {
                if ( l_child->Type() == ELEMENT_NODE )
                {
                    l_elements.push_back( l_child );
                }
                l_child = l_child->NextSibling();
            }
        }
        // If the tag name is *, we're done; return the list of all the element nodes.
        if ( i_tagName == "*" )
        {
            return l_elements;
        }
        // Otherwise, build the list of matching elements and return it.
        XmlNodeList l_result;
        const XmlNodeList::const_iterator l_end = l_elements.end();
        for ( XmlNodeList::const_iterator l_iter = l_elements.begin(); l_iter != l_end; l_iter++ )
        {
            if ( (*l_iter)->Name() == i_tagName )
            {
                l_result.push_back( *l_iter );
            }
        }
        return l_result;
    }

    bool XmlElement::HasAttributes() const
    {
        return m_attributes.size() > 0;
    }

    PtrView< XmlElement* > XmlElement::AsElement()
    {
        return PtrView< XmlElement* >( this );
    }

    PtrView< const XmlElement* > XmlElement::AsElement() const
    {
        return PtrView< const XmlElement* >( this );
    }

    void XmlElement::Accept( XmlNodeVisitor& i_op )
    {
        i_op.VisitElement( this );
    }

    void XmlElement::Accept( XmlNodeVisitor& i_op ) const
    {
        i_op.VisitElement( this );
    }
};
