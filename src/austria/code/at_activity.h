//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_activity.h
 *
 */

#ifndef x_at_activity_h_x
#define x_at_activity_h_x 1

#include "at_lifetime.h"
#include "at_lifetime_mt.h"
#include "at_thread.h"
#include <vector>

namespace at
{

class Activity;
class ActivityListOwner;

//
// Select a Medusa Traits class for ActivityListOwner
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< ActivityListOwner *, w_First, w_Second >
{
    public:

    typedef PtrTraitsMedusa<ActivityListOwner *>  type;
};

//
// Select a Medusa Traits class
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< const ActivityListOwner *, w_First, w_Second >
{
    public:

    typedef PtrTraitsMedusa<const ActivityListOwner *>  type;
};


// ======== ActivityPointers ==========================================
/**
 * Simple POD for managing pairs of pointers.
 */

struct ActivityPointers
{
    /**
     * This is either a set of forward/back pointers or
     * a beginning and end of list.
     */
    Activity                                    * m_links[ 2 ];
};


// ======== ActivityList ==============================================
/**
 * ActivityList can be used to derive various types of
 * thread management list types, including the thread pool and
 * ActivityList_Exclusive types.
 *
 * REV: Telling us what it can be used for only hints at what it does.
 * What does it do?
 *
 */

class AUSTRIA_EXPORT ActivityList
  : public PtrTarget_MT
{
    friend class Activity;
    friend class ActivityListOwner;
    friend class ThreadList;
    friend class ActivityTools;
    

private:

    /** Not implemented. */
    ActivityList( const ActivityList & );
    /** Not implemented. */
    ActivityList & operator= ( const ActivityList & );

    // ======== ActivityList ==========================================
    /**
     * ActivityList does simple initialization and should only be created
     * as part of an ActivityListOwner.
     */

    ActivityList();
    
protected:

    virtual ~ActivityList()
    {
    }
    
    // ======== PopNext ===============================================
    /**
     * PopNext will pop the next activity from the ActivityList and return it.
     * The reciever of the Activity returned by PopNext must either call
     * Activity::Cancelled or Activity::Perform.
     *
     * NOTE: PopNext requires m_mutex to be locked.
     *
     * @return A pointer to an Activity or 0 if no activities are
     *  enqueued.
     */

    Activity * PopNext();


    // ======== ProcessActivity =======================================
    /**
     * ProcessActivity attempts to process a single activity.
     *
     * NOTE: ProcessActivity requires m_mutex to be locked.
     *
     * @param i_activity The activity to be processed
     */

    void ProcessActivity( Activity * i_activity );
    

    // ======== WaitCompletionLocked ==================================
    /**
     * WaitCompletionLocked will wait until any associated activites are
     * completed processing or once Terminate() is called, WaitCompletionLocked
     * will await for termination of all outstanding processing activity.
     *
     * NOTE: WaitCompletionLocked requires m_mutex to be locked.
     */

    void WaitCompletionLocked();

    // ======== ProcessingStarted =====================================
    /**
     * This should be called whenever a thread commences processing
     * activities.
     *
     * NOTE: This method requires m_mutex to be locked.
     *
     * @param l_num_started is the number of threads reported
     *          as started.
     */

    void ProcessingStarted( int l_num_started = 1 );
    
    // ======== ProcessingEnded =======================================
    /**
     * This should be called whenever a thread stops processing
     * activities.
     *
     * NOTE: This method requires m_mutex to be locked.
     */

    void ProcessingEnded();

    // ======== ProcessingTerminated ==================================
    /**
     * ProcessingTerminated should be called when no further processing
     * will occur (due to Terminate() being called).  In a thread pool,
     * this would happen when all pool threads exit.
     *
     * NOTE: This method requires m_mutex to be locked.
     */

    void ProcessingTerminated();
    
    // ======== ProcessingStart_RAII ==================================
    /**
     * ProcessingStart_RAII is a helper class for managing thread
     * saftey of calling ProcessingStarted/ProcessingEnded().
     * REV: What does RAII stand for?
     */

    class ProcessingStart_RAII
    {

        PtrView< ActivityList * >       m_al;
        bool                            m_ok;

        public:
        
        inline ProcessingStart_RAII(
            PtrView< ActivityList * >   i_al
        )
          : m_al( i_al ),
            m_ok( false )
        {
            m_al->ProcessingStarted();
        }

        inline ~ProcessingStart_RAII()
        {
            if ( ! m_ok )
            {
                m_al->ProcessingEnded();
            }
        }

        inline void SetOk()
        {
            m_ok = true;
        }
    };
    
    // ======== ActivityEnqueuedNotification ==========================
    /**
     * ActivityEnqueuedNotification is intended to notify the
     * processor that a new activity has been added to the list.
     *
     * NOTE: The m_mutex is locked when ActivityEnqueuedNotification is
     * called.  When ActivityEnqueuedNotification returns, the m_mutex
     * must still be locked.
     *
     * @param i_count is the count of the number of elements that
     *      are being notified for.
     *
     * @return False if the notification failed because of
     *          the activity being enqueued having failed.
     */

    virtual bool ActivityEnqueuedNotification( unsigned i_count ) = 0;


    public:

    // ======== Close =================================================
    /**
     * Close will cancel all enqueued activities in this ActivityList
     * and no longer accept any new activities until Open is called.
     * Close may be called multiple times, Open must be called the
     * same number of times as Close to re-open the ActivityList.
     *
     * Close will wait for any activities to complete the processing.
     *
     * @return nothing
     */

    void Close();


    // ======== Open ==================================================
    /**
     * Open will re-open (accept enqueue requests).  This must be called
     * the same number of times that Close() is called to re-open the
     * list.
     */

    void Open();


    // ======== Terminate =============================================
    /**
     * Terminate will permenantly Close() the ActivityList.  No further
     * calls to Open will result in an open list.  Terminate will also
     * Wait for any (performing) activities to complete.
     */

    virtual void Terminate();
    

    // ======== Suspend ===============================================
    /**
     * Suspend will delay processing any activities until Resume is
     * called.  If Suspend is called multiple times, Resume must also 
     * be called the same number of times for processing to resume.
     * Activities may still be enqueued in a suspended ActivityList.
     *
     * Suspend will not wait for any current activity to complete.
     *
     * @return nothing
     */

    virtual void Suspend();
    

    // ======== Resume ================================================
    /**
     * Resume will contine to process activities after a Suspend().
     *
     * @return nothing
     */

    virtual void Resume();

    // ======== WaitCompletion ==================================
    /**
     * WaitCompletion will wait until any associated activites are
     * completed processing or once Terminate() is called, WaitCompletion
     * will await for termination of all processing activity.
     *
     */

    void WaitCompletion();

    // ======== GetQueueCount ==============================================
    /**
     * GetQueueCount returns the number of outstanding items in the queue
     */
    virtual unsigned GetQueueCount ();

    // ======== GetOwner ==============================================
    /**
     * GetOwner will return the owner of this activity list. 
     */

    virtual PtrDelegate< ActivityListOwner * > GetOwner() = 0;

    private:
    
    // ======== CloseAlreadyLocked ====================================
    /**
     * Close will permenantly cancel all remaining activities, suspend 
     * processing on the ActivityList and wait for any incomplete
     * activities to complete.
     *
     * @return nothing
     */

    void CloseAlreadyLocked();


    public:

    /**
     * m_mutex is the synchronization mechanism for this
     * ActivityList.
     */
    Mutex                                         m_mutex;

    protected:

    /**
     * m_cond is used to synchronize when all activities
     * being currently performed are complete.
     */
    Conditional                                   m_cond;

    /**
     * m_cond_waiters indicates the number of threads
     * currently waiting on m_cond.
     */
    int                                           m_cond_waiters;
    
    /**
     * m_cond_queued is used to synchronize when all activities
     * queued or performed are complete.
     */
    Conditional                                   m_cond_queued;

    /**
     * m_cond_queued_waiters is the count of waiters
     * on m_cond_queued.
     */
    int                                           m_cond_queued_waiters;

    /**
     * m_queue is the head and tail of the list of activities
     */
    ActivityPointers                              m_queue;

    /**
     * m_queue_count is the count of the number of elements
     * remaining in the queue.
     */
    unsigned                                      m_queue_count;

    /**
     * m_terminated indicates that the activity list is closed
     * and will no longer take Enqueue requests or supply
     * an owner via GetOwner.
     */

    bool                                          m_terminated;

    /**
     * m_closed_count is a positive number of the activity list is
     * currently closed.
     */
    
    int                                           m_closed_count;

    /**
     * m_close_in_progress indicates that closing is in progress.
     */
    bool                                          m_close_in_progress;

    /**
     * m_suspend_count indicates the number of times Suspend
     * has been called without a corresponding Resume
     */
    int                                           m_suspend_count;

    /**
     * m_perform_in_progress is the count of perform methods
     * in progress.  When this becomes 0, if there are any waiters,
     * it will cause a wakeup of all threads waiting on m_cond.
     */
    unsigned int                                  m_perform_in_progress;

};


// ======== ActivityListOwner =========================================
/**
 * ActivityListOwner is used to manage the proper shutdown of
 * an ActivityList.  Once all owners of an activity list are
 * released then the activity list is terminated.
 *
 */

class AUSTRIA_EXPORT ActivityListOwner
  : public ActivityList
{
    friend class ActivityList;

    template <typename, typename>
    friend class PtrTraitsMedusa;
    
    void MedusaInc( const ActivityListOwner * ) const
    {
        m_owner_count ++;
    }
    
    void MedusaDec( const ActivityListOwner * ) const
    {
        if ( 0 == -- m_owner_count )
        {
            const_cast< ActivityListOwner * >( this )->Terminate();
        }
    }

    public:

    virtual ~ActivityListOwner()
    {
    }

    // ======== GetOwner ==============================================
    /**
     * GetOwner will return the owner of this activity list. 
     */

    virtual PtrDelegate< ActivityListOwner * > GetOwner();

    // ======== GetActivityList =======================================
    /**
     * GetActivityList is a simple access routine to get access to the
     * managed ActivityList.
     *
     * @return The activity list held (being owned) by this ActivityListOwner
     */

    inline PtrDelegate< ActivityList * > GetActivityList()
    {
        return PtrView< ActivityList * >( this );
    }

    private:

    /**
     * m_owner_count is the number of owners of this
     * activity list. When the owner count goes from
     * 1 to 0 (edge triggered), the activity list 
     * Terminate method will be called.
     * Incrementing the owner count from there will
     * not "unterminate" the activity list.
     */
    mutable AtomicCount                 m_owner_count;
    
};


// ======== ActivityList_Cradle =========================================
/**
 * ActivityList_Cradle is intended as the base class for managing
 * an ActivityList in multiple classes.  I.e. if multiple classes
 * want to use the same activity list (especially the ActivityList_Exclusive
 * variant which guarentees that only a single thread is executing
 * activities) this class can be used to enforce this.  The Activity_Seated
 * variant of Activity will access the ActivityList_Cradle to define
 * it's activity list.
 */

class AUSTRIA_EXPORT ActivityList_Cradle
{
    public:

    /**
     * ActivityList_Cradle is intened to be virtially inherited
     * among a number of classes that use it.
     *
     */

    ActivityList_Cradle( const ActivityList_Cradle & i_alb )
      : m_activity_list_owner( i_alb.GetOwner() )
    {
        AT_Assert( m_activity_list_owner );
    }
    
    /**
     * ActivityList_Cradle is intened to be virtially inherited
     * among a number of classes that use it.
     *
     */

    ActivityList_Cradle( at::PtrDelegate< ActivityListOwner * > i_alo )
      : m_activity_list_owner( i_alo )
    {
        AT_Assert( m_activity_list_owner );
    }

    virtual ~ActivityList_Cradle()
    {
        m_activity_list_owner = NULL;
    }

    // ======== GetOwner ==============================================
    /**
     * GetOwner returns the owner object.
     *
     * @return ActivityListOwner associated with this base.
     */

    at::PtrView< ActivityListOwner * > GetOwner() const
    {
        return m_activity_list_owner;
    }

    // ======== GetActivityList =======================================
    /**
     * GetActivityList returns the associated activity list.
     *
     * @return nothing
     */

    inline PtrDelegate< ActivityList * > GetActivityList() const
    {
        return m_activity_list_owner->GetActivityList();
    }

    private:
    /**
     * m_activity_list_owner is the owner "proxy object" to the
     * activity list in question
     */
    
    Ptr< at::ActivityListOwner * >              m_activity_list_owner;

    private:
    // Assignment of the base is not allowed.
    ActivityList_Cradle operator=( const ActivityList_Cradle & );

    public:
    // In theory, this should never be called, however
    // we need to make this public since there will be a lot
    // of code to write otherwise. Actual calls to this
    // constructor should fail.
    ActivityList_Cradle() AT_DEFINE_VIRTINHR_CONSTRUCTOR; 
        // no default connstructor allowed
};


// ======== LibraryPool ===============================================
/**
 * LibraryPool will return the basic library activity list.
 *
 * @return The ActivityList pointer which is associated with
 *          the thread pool.
 */

extern PtrDelegate< ActivityList * > LibraryPool();


// ======== ActivityController ========================================
/**
 * ActivityController is used to manage a set of activities by providing
 * a mechanism to disable a set of activities from a single call.
 * When an object is being destroyed, all activities within the class
 * may require Disable()ing before destruction may commence.
 *
 * An ActivityController is intended to be inherited (using virtual
 * inheritance) in the same class as the Activity objects it is
 * managing.  Hence it is guarenteed that the ActivityController
 * will be destroyed last during object destruction (and constructed
 * first). The requirement is that in the most derived class.  It
 * must call DisableAll in the destructor; however, if a class
 * is derived from that is already calling DisableAll in its
 * destructor, then this may cause "undefined behaviour" because
 * this might call methods on activities that are already deleted.
 * The "inhibit" mechanism provided in this class should automatically
 * deal with this issue.
 *
 * Because ActivityController is intended to be inherited (using virtual
 * inheritance) in the same class as the Activity objects it is
 * managing, is is not necessary to provide multi-threaded re-entrant
 * guarentees on AddActivity or ActivityDestroyed because these
 * methods can only be called from a single thread (for each object).
 * (i.e. only one thread can be in the constructor of an object)
 *
 * Notes:
 * <li>This class is not thread safe.  i.e. No simultaneous calls to any of
 * the methods to an object of this class is supported.</li>
 * <li>ActivityController is intended to be inherited (using virtual
 * inheritance) in the same class as the Activity objects it is
 * managing.  This means that activities are destroyed in the
 * reverse order in which they are created and the ActivityController
 * is constructed before any of the added Activity objects and destroyed
 * after all Activity have been destroyed.<li>
 */

class AUSTRIA_EXPORT ActivityController
{
    public:

    inline ActivityController()
      : m_inhibit( false ),
        m_destruct_count( 0 )
    {
    }

    // ======== AddActivity ===========================================
    /**
     * AddActivity will add the activity to this ActivityController.
     *
     *
     * @param i_activity Is the activity added to the controller.
     */

    inline void AddActivity(
        Activity                    * i_activity
    ) {
        m_list_o_activities.push_back( i_activity );
    }


    // ======== ActivityDestroyed =====================================
    /**
     * ActivityDestroyed is simply to enable detection of programming
     * error.  Once activities are destroyed, they must not be
     * "enabled" or "disabled".
     *
     *
     * @param i_activity Is the activity notifying the controller of
     *          it's destruction.
     */

    inline void ActivityDestroyed(
        Activity                    * i_activity
    ) {
        ++ m_destruct_count;
    }

    // ======== DisableAll ============================================
    /**
     * DisableAll will disable all activities controlled by this
     * ActivityController.
     *
     */

    void DisableAll( bool i_wait = true, bool i_inhibit = true );


    // ======== EnableAll ============================================
    /**
     * EnableAll will enable all activities controlled by this
     * ActivityController.
     *
     */

    void EnableAll();

    private:

    /**
     * m_list_o_activities is the storage for all the list of
     * activities.
     */
    std::vector< Activity * >           m_list_o_activities;

    /**
     * m_inhibit indicates that calls to DisableAll
     * or EnableAll should be ignored.
     */
    bool                                m_inhibit;

    /**
     * m_destruct_count is a count of the number of calls
     * to ActivityDestroyed.  It is considered a bug to
     * call DisableAll or EnableAll
     * once activties on the m_list_o_activities are no
     * longer valid.
     */
    int                                 m_destruct_count;

};


// ======== Activity ==================================================
/**
 * An activity denotes a piece of work that can be sceduled to be
 * performed at some furture point in time.  An activity may be
 * inherited multiple times in a single class.  This is not unlike
 * an event, in the sense that an activity is usually associated
 * with some external (to the class) occurence but needs to be
 * processed in a particular thread or in a different stack frame.
 *
 */

class AUSTRIA_EXPORT Activity
{
    public:

    friend class ActivityList;
    friend class ThreadList;
    friend class ActivityTools;

    // ======== ActivityState ==========================================
    /**
     * ActivityState is the state of the activity.  This indicates the
     * what this activity is currently doing.  Changes to the state
     * should be made only while the activity list is locked.
     */

    class ActivityState
    {
        public:

        friend class ActivityState_Hidden;

        // ======== String ============================================
        /**
         * This is the string representation of the ActivityState
         *
         * @return A string representation of the state.
         */

        virtual const char * String() const = 0;


        // Only the activity can define new states
        private:
        
        ActivityState()
        {
        }
        
        ActivityState( const ActivityState & );
        ActivityState & operator = ( const ActivityState & );
        
    };

    /**
     * s_start indicates that the activity is not currently 
     * enqueued.
     */

    static const ActivityState                   & s_start;

    /**
     * s_enqueued - enqueued awaiting execution or cancelling
     * from either a close on the activity list.
     * REV: either a close on the activity list or a what?!
     */

    static const ActivityState                   & s_enqueued;
    
    /**
     * s_perform - currently executing the perform method.
     */

    static const ActivityState                   & s_perform;
    
    /**
     * s_perform_enqueue currently executing the perform
     * method and there has been an intervening equeue
     * called.
     */

    static const ActivityState                   & s_perform_enqueue;
    
    /**
     * s_cancelling currently executing the Cancelled method.
     */

    static const ActivityState                   & s_cancelling;
    
    /**
     * s_cancelling_enqueue currently executing the Cancelled
     * however there is an intervening enqueue.
     */

    static const ActivityState                   & s_cancelling_enqueue;
    
    /**
     * s_deleted indicates that the activity is no longer available.
     * Any access to the Activity class while in the deleted state
     * is likely an error.
     */

    static const ActivityState                   & s_deleted;



    // ======== EnqueueStatus =========================================
    /**
     * EnqueueStatus defines return values from Enqueue.
     *
     */

    enum EnqueueStatus
    {
        SuccessStatus = 1,
        ActivityAdded = SuccessStatus | ( 1 << 1 ),
        ActivityAlreadyInQueue = SuccessStatus | ( 2 << 1 ),
        ActivityDisabled = ( 4 << 1 ),
        ListClosed = ( 5 << 1 ),
        ThreadWakeupFailed = ( 6 << 1 ),

    };
    

    // ======== Activity ==============================================
    /**
     * An Activity requires an ActivityList to be associated with hence
     * the Activity constructor requires a pointer to it's associated
     * ActivityList.
     *
     * @param i_al Pointer to the associated activity list
     */

    Activity( PtrDelegate< ActivityList * > i_al );
    
    // ======== ~Activity =============================================
    /**
     * Destructor will wait for any pending activity 
     * ( calls to Cancelled() or Perform() ) to complete
     *
     */

    virtual ~Activity();

    // ======== Enqueue ===============================================
    /**
     * Enqueue will intsert this Activity into the ActivityList associated
     * with this action.
     *
     * Enqueue might fail if either:
     * 1. It is already enqueued
     * 2. The activity list is closed
     * 3. The activity is disabled
     * 4. Enqueue is being called in the context of
     *      of a Cancelled() call.
     *
     * @return True if the Activity was enqueued or false otherwise.
     */

    EnqueueStatus Enqueue();    

    // ======== Wait ==================================================
    /**
     * Wait will pause the current thread until this activity has completed
     * it's processing (either Cancelled or Perform routines) to complete.
     *
     */

    virtual void Wait();

    // ======== Disable ===============================================
    /**
     * Disable will disable this activity from being enqueued and remove
     * the activity from the queue and optionally wait for the activity to
     * complete the Perform() or Cancelled() methods (if currently within
     * these routines).
     *
     * @param i_wait If true wait for completion of Perform or Cancelled
     *      to return (only if currently within these routined).
     */

    void Disable( bool i_wait = true );

    // ======== Enable ================================================
    /**
     * Enable will will allow this activity to be queued after being
     * disabled (by calling Disable).
     *
     */

    void Enable();

    // ======== Perform ===============================================
    /**
     * Perform is defined by the deriving class.  The Perform method will
     * be called sometime after Enqueue succeeds.  Perform may also
     * complete before Enqueue returns (since the thread executing perform
     * may complete while the thread calling enable is still be in the
     * process of returning from Enable).
     *
     */

    virtual void Perform() = 0;


    // ======== Cancelled =============================================
    /**
     * Cancelled may be defined by the deriving class.  Usually called
     * by the thread that calls Disable() but may be called by the
     * same thread calling Perform or the thread calling
     * ActivityList::Close or ActivityList::Terminate.
     *
     * A default (no op) method is supplied.  However, Cancelled() can be
     * overridden if required.  This may be useful for cases where an
     * activity is stand-alone and requires notification or removal
     * from the activity list.
     *
     * @param i_count Is the total number of times that
     *          that the activity was dequeued (after being enqueued).
     *
     * REV: What does it mean for an activity to be dequeued multiple
     * times?  Also, the comment says by which thread the method is likely
     * to be called, but not the circumstances under which it will be
     * called.  What are these circumstances?  Will this method ever be
     * called while Perform is running in a different thread?
     */

    virtual void Cancelled(
        unsigned int                i_count
    );


    // ======== IsEnabled ============================================
    /**
     * IsEnabled returns the "m_enabled" status of this activity.
     *
     * REV: That is clear from the code, but what does the "'m_enabled'
     * status" of the activity MEAN?
     *
     * There may be other reasons for this activity failing an equeue
     * request however (like the activity list being closed or terminated
     * or already being enqueued).
     *
     * REV: What does failing an enqueue request have to do with the
     * "enabled status"?
     *
     * @return The enabled status
     */

    inline bool IsEnabled() const
    {
        return m_enabled;
    }

    private:

    /**
     * m_enabled indicates that the activity is currently
     * enabled.
     *
     * REV: Obviously.  But what does it mean to be "enabled" and to whom
     * does it mean that?
     */
    bool                                          m_enabled;

    /**
     * m_cancel_count is the number of times an enqueued
     * activity was disabled (dequeued) or cancelled by
     * an activity list close.
     */
    unsigned int                                  m_cancel_count;

    /**
     * m_al is the ActivityList associated with this
     * activity.
     */

    Ptr< ActivityList * >                         m_al;
    
    /**
     * m_link contains the links in the ActivityList
     */

    ActivityPointers                              m_link;

    /**
     * m_condz is the lazy condition variable used to
     * manage waiters.  This has the added benefit that
     * resources are not allocated until needed and that
     * it may be deleted yet waiting (or posting)
     * threads will not not be affected since they
     * hold a reference to the underlying at::Conditional
     * instance.
     */

    ConditionLazy                                 m_condz;

    /**
     * m_have_waiters indicates that there are threads waiting
     * for perform to complete.
     */

    int                                           m_have_waiters;

    /**
     * m_perform_ptr is assigned by the caller to perform
     * or cancel and is used to indicate self-deletion.
     */
    Activity                                   ** m_perform_ptr;

    /**
     * m_perform_task is the task id of the thread calling
     * the Peform or Cancelled methods.
     */
    Task::TaskID                                  m_perform_task;

    /**
     * m_state indicates the state of the activity
     */

    const ActivityState                          * m_state;

    private:
    // Activity can't be copied - these methods are private
    // and unimplimented for a reason.
    Activity( const Activity & );
    Activity & operator= ( const Activity & );    
};

// ======== operator== =================================================
/**
 * Compare equality of 2 ActivityState states.
 *
 * @param i_lhs The lhs operand
 * @param i_rhs The rhs operand
 * @return True if the states are the same
 */

inline bool operator==(
    const Activity::ActivityState    & i_lhs,
    const Activity::ActivityState    & i_rhs
) {
    return ( & i_lhs ) == ( & i_rhs );
}

// ======== operator!= =================================================
/**
 * Compare inequality of 2 ActivityState states.
 *
 * @param i_lhs The lhs operand
 * @param i_rhs The rhs operand
 * @return True if the states are the not the same
 */

inline bool operator!=(
    const Activity::ActivityState    & i_lhs,
    const Activity::ActivityState    & i_rhs
) {
    return ( & i_lhs ) != ( & i_rhs );
}


// ======== operator< ==================================================
/**
 * Compare less than of 2 ActivityState states.
 *
 * @param i_lhs The lhs operand
 * @param i_rhs The rhs operand
 * @return True if i_lhs is less than i_rhs
 */

inline bool operator<(
    const Activity::ActivityState    & i_lhs,
    const Activity::ActivityState    & i_rhs
) {
    return ( & i_lhs ) < ( & i_rhs );
}


// ======== ActivityList_Pool ============================================
/**
 * ActivityList_Pool implements the basic Austria thread pool.  This can
 * be used to create a stand-alone thread pool class or the standard
 * optimally use the Austria basic Thread Pool
 *
 */

class ThreadList;

class AUSTRIA_EXPORT ActivityList_Pool
  : public ActivityListOwner
{
    public:

    /**
     * ActivityList_Pool takes the number of threads that are
     * created.
     *
     * @param i_thread_count is the maximum number of threads to start,
     *          otherwise the library default is used if i_thread_count is 0.
     *
     */

    ActivityList_Pool( int i_thread_count = 0 );


    // ======== ~ActivityList_Pool ====================================
    /**
     * Destructor cleans up (waits for threads to complete)
     *
     */

    ~ActivityList_Pool();

    private:
    
    // ======== ActivityEnqueuedNotification ==========================

    virtual bool ActivityEnqueuedNotification( unsigned i_count );

    friend class ThreadList;

    private:
    
    ThreadList                              * m_thread_list;
};


// ======== ActivityList_Exclusive ====================================
/**
 * ActivityList_Exclusive is used to ensure that a single thread is used
 * to perform any of the activities that are enqueued on it.  This may
 * reduce the need for synchronization in various classes that co-operate
 * using the same ActivityList_Exclusive type ActivityList.
 *
 */

class AUSTRIA_EXPORT ActivityList_Exclusive
  : public ActivityListOwner
{
    protected:
    
    // ======== ProcessList ==========================================

    virtual void ProcessList();

    virtual ~ActivityList_Exclusive()
    {
    }
};


// ======== ActivityList_FedEx ========================================
/**
 * ActivityList_FedEx is an ActivityList_Exclusive but it is fed
 * from another activity.  Since an activity can only have a single
 * thread active at any one time, all the Activity-ies on this activity
 * list are only executed by a single thread.
 *
 */

class AUSTRIA_EXPORT ActivityList_FedEx
  : public ActivityList_Exclusive,
    protected Activity
{
    public:

    /**
     * ActivityList_FedEx feeds off another activity list and by default
     * uses the library's default thread pool.
     *
     */
    inline ActivityList_FedEx( PtrDelegate< ActivityList * > i_al = LibraryPool() )
      : Activity( i_al )
    {
    }

    virtual ~ActivityList_FedEx()
    {
    }

    private:
    
    // ======== Perform ===============================================
    /**
     * This is called when the activity is Perform()ed from the
     * feeding (parent) activity list.
     *
     */

    virtual void Perform();

    // ======== Cancelled =============================================
    /**
     * This is called when the activity is Cancelled() from the
     * parent activity list.
     *
     * @param i_count Is the total number of times that
     *          that the activity was dequeued (after being enqueued).
     */

    virtual void Cancelled(
        unsigned int                i_count
    );

    // ======== ActivityEnqueuedNotification ==========================
    /**
     * ActivityEnqueuedNotification will ensure that this in enqueued
     * when necessary.
     *
     * NOTE: The m_mutex is locked when ActivityEnqueuedNotification is
     * called.  When ActivityEnqueuedNotification returns, the m_mutex
     * must still be locked.
     *
     * @param i_count is the count of the number of elements that
     *      are being notified for.
     *
     * @return True if notification was successful
     */

    virtual bool ActivityEnqueuedNotification( unsigned i_count );

};


// ======== Activity_Seated ===========================================
/**
 * A seated activity uses the ActivityList_Cradle class to determine
 * it's associated acitvity list.  This class reduces the need to
 * explicitly assign each activity to a particular activity list.
 * Each class that inherits from Activity_Seated will also need
 * to appropriately construct the ActivityList_Cradle to determine
 * the activity list to use.
 */

class AUSTRIA_EXPORT Activity_Seated
  : public Activity,
    public virtual ActivityList_Cradle
{
    public:

    /**
     * Activity_Seated contructor takes no arguments and inherits 
     * from the a common ActivityList_Cradle structure to manage
     * the activity list.
     */
    inline Activity_Seated()
      : Activity( ActivityList_Cradle::GetActivityList() )
    {
    }

};

// ======== Activity_Controlled =======================================
/**
 * Activity_Controlled adds a controller to at::Activity.  This
 * enables a single call to disable all activtities managed by
 * the controller.
 *
 */

class AUSTRIA_EXPORT Activity_Controlled
  : public Activity,
    public virtual ActivityController
{
    public:

    /**
     * Activity_Controlled simply adds this to the
     * associated ActivityController.
     *
     * @param i_al Pointer to the associated activity list
     */

    Activity_Controlled( PtrDelegate< ActivityList * > i_al )
      : Activity( i_al )
    {
        ActivityController::AddActivity( this );
    }

    virtual inline ~Activity_Controlled()
    {
        ActivityController::ActivityDestroyed( this );
    }

};

// ======== Activity_ControlledSeated =======================================
/**
 * Activity_ControlledSeated adds a controller to at::Activity_Seated.  This
 * enables a single call to disable all activtities managed by the controller.
 *
 */

class AUSTRIA_EXPORT Activity_ControlledSeated
  : public Activity_Seated,
    public virtual ActivityController
{
    public:

    /**
     * Activity_ControlledSeated simply adds this to the
     * associated ActivityController.
     */
    inline Activity_ControlledSeated()
    {
        ActivityController::AddActivity( this );
    }

    virtual inline ~Activity_ControlledSeated()
    {
        ActivityDestroyed( this );
    }

    virtual void Perform() = 0;
};


// ======== AT_ActivityPerformBinder ==================================
/**
 * AT_ActivityPerformBinder is a macro that implements a wrapper
 * class that can be used to aid inheriting an activity multiple
 * times in a single class to handle multiple evens.  A method
 * named Perform ## z_name is creaded that needs to be overriden
 * by an application.
 *
 * @param z_name is the name of the class to create and the name of the
 * @param z_activity_type is one of Activity_Seated, Activity_Controlled
 *      or Activity_ControlledSeated.
 *
 */

#define AT_ActivityPerformBinder( z_name, z_activity_type ) \
class z_name                                                            \
  : public z_activity_type                                              \
{                                                                       \
                                                                        \
    virtual void Perform ## z_name() = 0;                               \
                                                                        \
    void Perform()                                                      \
    {                                                                   \
        Perform ## z_name();                                            \
    }                                                                   \
public:                                                                 \
    virtual ~z_name() {}                                                \
};                                                                      \
// end macro


}; // namespace


#endif // x_at_activity_h_x


