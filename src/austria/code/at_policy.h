/**
 *  \file at_policy.h
 *
 *  \author Bradley Austin
 *
 *  at_policy.h contains classes to support Policy-Based Class Design.
 *
 */

#ifndef x_at_policy_h_x
#define x_at_policy_h_x


namespace at
{


    template< class w_host_class >
    class PolicyCaster
    {

    public:

        template< class w_to_policy >
        class To
        {

        public:

            template< class w_from_policy >
            inline static w_to_policy & From( w_from_policy & i_from_policy )
            {
                return *static_cast< w_to_policy * >
                    ( static_cast< w_host_class * >( &i_from_policy ) );
            }

        private:

            /* Unimplemented */
            To();
            To( const To & );
            ~To();
            To & operator=( const To & );

        };

    private:

        /* Unimplemented */
        PolicyCaster();
        PolicyCaster( const PolicyCaster & );
        ~PolicyCaster();
        PolicyCaster & operator=( const PolicyCaster & );

    };


    template<
        class w_to_policy,
        class w_caster
    >
    class PolicyCast
    {

    public:

        template< class w_from_policy >
        inline static w_to_policy & From( w_from_policy & i_from_policy )
        {
            typedef  typename w_caster::template To< w_to_policy >  t_to;
            return t_to::From( i_from_policy );
        }

    private:

        /* Unimplemented */
        PolicyCast();
        PolicyCast( const PolicyCast & );
        ~PolicyCast();
        PolicyCast & operator=( const PolicyCast & );

    };


}


#endif
