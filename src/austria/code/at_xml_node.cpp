//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_node.cpp
 *
 * \author Guido Gherardi
 *
 */

#include "at_xml_node.h"
#include "at_xml_composite.h"
#include "at_exception.h"

#define AT_ThrowXmlHierarchyRequestErr(msg) AT_ThrowDerivation( at::XmlHierarchyRequestErr, msg )

// Austria namespace
namespace at
{
    // ========== Class XmlNode Implementation ===================================================================

    std::string XmlNode::Name() const
    {
        return std::string();
    }

    std::string XmlNode::Value() const
    {
        return std::string();
    }

    XmlNodeList XmlNode::ChildNodes() const
    {
        return XmlNodeList();
    }

    bool XmlNode::HasChildNodes() const
    {
        return false;
    }

    PtrView< XmlNode* > XmlNode::FirstChild() const
    {
        return 0;
    }

    PtrView< XmlNode* > XmlNode::LastChild() const
    {
        return 0;
    }

    PtrView< XmlNode* > XmlNode::InsertBefore( PtrDelegate< XmlNode* > i_newChild,
                                               PtrView< XmlNode* >     i_refChild )
    {
        AT_ThrowXmlHierarchyRequestErr( "this node is of a type that does not allow children" );
        return i_newChild;
    }

    PtrView< XmlNode* > XmlNode::AppendChild( PtrDelegate< XmlNode* > i_newChild )
    {
        AT_ThrowXmlHierarchyRequestErr( "this node is of a type that does not allow children" );
        return i_newChild;
    }

    PtrDelegate< XmlNode* > XmlNode::RemoveChild( PtrDelegate< XmlNode* > i_oldChild )
    {
        AT_ThrowXmlHierarchyRequestErr( "this node is of a type that does not allow children" );
        return i_oldChild;
    }

    PtrDelegate< XmlNode* > XmlNode::ReplaceChild( PtrDelegate< XmlNode* > i_newChild,
                                                   PtrDelegate< XmlNode* > i_oldChild )
    {
        AT_ThrowXmlHierarchyRequestErr( "this node is of a type that does not allow children" );
        return i_oldChild;
    }

    bool XmlNode::HasAttributes() const
    {
        return false;
    }

    PtrView< XmlDocument* > XmlNode::AsDocument()
    {
        return PtrView< XmlDocument* >();
    }

    PtrView< const XmlDocument* > XmlNode::AsDocument() const
    {
        return PtrView< const XmlDocument* >();
    }

    PtrView< XmlElement* > XmlNode::AsElement()
    {
        return PtrView< XmlElement* >();
    }

    PtrView< const XmlElement* > XmlNode::AsElement() const
    {
        return PtrView< const XmlElement* >();
    }

    bool XmlNode::IsAncestorOf( PtrView< const XmlNode* > i_node ) const
    {
        while ( i_node && i_node.Get() != this ) i_node = i_node->ParentNode();
        return i_node;
    }

    // ========== Default Visitor Methods ========================================================================

    void XmlNodeVisitor::VisitDocument( XmlDocument* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitDocument( const XmlDocument* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitElement( XmlElement* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitElement( const XmlElement* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitText( XmlText* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitText( const XmlText* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitComment( XmlComment* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitComment( const XmlComment* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitProcessingInstruction( XmlProcessingInstruction* )
    {
        // just do nothing!
    }

    void XmlNodeVisitor::VisitProcessingInstruction( const XmlProcessingInstruction* )
    {
        // just do nothing!
    }

}; // namespace
