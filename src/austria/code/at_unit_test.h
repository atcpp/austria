//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_unit_test.h
 *
 */

#ifndef x_at_unit_test_h_x
#define x_at_unit_test_h_x 1

#include "at_exports.h"
#include "at_factory.h"
#include "at_types.h"
#include "at_source_locator.h"
#include "at_start_up.h"

// Austria namespace
namespace at
{

/**
 * @defgroup UnitTestInterface Unit Testing
 *
<h2>Introduction</h2>

<p>Unit tests are perhaps the most under-utilized tool of software
development.  Unit tests not only allow you to verify the correct
operation of code, but also use properly can be used to optimize
your design process. Austria has a C++ framework for managing
unit tests that provides a uniform interface for running tests and
discovering unit test failures.

<p>The Austria unit test interface is quite simple
and leverages the <a href="group__GenericFactories.html">Generic Factories</a>
functionality.  Essentially, each unit test is a implementation of the
UnitTest class and the Austria unit test famework simply executes the
selected unit tests.  To make the task of defining unit tests simpler,
there are a set of macros that "simplify" the task.

<h2>How To Define A Unit Test</h2>
<p>A unit tests are grouped by "Area".  The first thing to do
is define the area by using the AT_TestArea macro.  This defines
a symbol that can be associated with any number of unit tests. A
single file may contain any number of test areas, although it is
usual to have only one test area per file.
<p>A unit test can only fail if it throws an exception, usually a
at::TestCase_Exception using the AT_TCAssert macro.  It may throw an
exception in the contructor, the Run() method, or the destructor.
Note that thowing an exception in the unit test destructor will lead to memory
leaks but this is inconsequential since this is just a unit test.
Some unit tests may be expected to throw as an indication of success
and hence the sense of failure is that if the test case does not
throw, it is considered to have failed.  A unit test can be defined
to "expect" to throw, which indicates that completion without failure
is considered a failure. These would be rare and it is reccomended
that your test case catch the appropriate exception and complete the
test case when applicable.
<p>Some unit tests may require exhasutive test runs which can take long
periods of time to complete.  It may be desirable to limit the frequency
of the longer tests since this will reduce the overall development
efficiency.  This can be done be assigning a "level" to a unit test.
The default level is set to 10, increasing the level of a test indicates
that it should run less frequently.
<p>The unit test is created by using the AT_DefineTest, AT_DefineTestLevel,
or AT_DefineTestExpect macros. AT_DefineTest will define a unit test
with the default of TestSuccess (no exception thrown) and a level of 10.
AT_DefineTestLevel allows the setting of a different expectation and
level while AT_DefineTestExpect will allow setting of a different
expected result.
<p>Once a unit test is defined, it must be registered with the unit
test factory.  This is done using the AT_RegisterTest macro.

<h2>Example Unit Tests</h2>

<p>There is nothing like an example to show how to create a unit test.
The first test is simple and always succeeds since it does not
throw.

<CODE>
<pre>
\#include "at_unit_test.h" // include the unit test header
using namespace at;
// an anonymous namespace will avoid collision between unit test
// modules.  This is optional although reccomended.
namespace {
// First define the area
AT_TestArea( ExampleArea, "This is an example of a Unit Test Area" );

AT_DefineTest( FirstExample, ExampleArea, "This is a simple example unit test" )
{
    void Run()
    {
        // The simplest test - this is where the significant part
        // of a test would reside.
    }
};
AT_RegisterTest( FirstExample, ExampleArea ); // register the unit test
} // end anonymous namespace

</pre>
</CODE>

<h2>Fixtures</h2> <p>Usually a number of unit tests will do a number of
"setup" activities that are common--common in the sense that the same
action must be performed prior to each test, not common in the sense that
an action must be performed exactly once prior to executing all the tests.
This can be accomplished by defining a "Fixture" class.  The example below
shows how this is done.  The two example test cases, ExampleA and ExampleB
inherit the "Fixture" class.</p>

<CODE>
<pre>
namespace {
AT_TestArea( Example2Area, "This is the second example of a Unit Test Area" );

struct Fixture
{
    int m_stuff;
    Fixture()
      : m_stuff(0)
    {}
};

// note that AT_DefineTest basically defines a class that inherits
// at::UnitTest so to inherit Fixture you need to separate it with
// a ",".
AT_DefineTest( ExampleA, Example2Area, "Fixture example A" ), public Fixture
{
    void Run()
    {
        AT_TCAssert( m_stuff == 0, "Shocker - m_stuff should be zero" );
    }
};
AT_RegisterTest( ExampleA, Example2Area ); // register the unit test

AT_DefineTest( ExampleB, Example2Area, "Fixture example B" ), public Fixture
{
    void Run()
    {
        AT_TCAssert( m_stuff == 0, "Shocker - m_stuff should be zero" );
    }
};
AT_RegisterTest( ExampleB, Example2Area ); // register the unit test
} // end namespace

</pre>
</CODE>

<h2>Test Level</h2>
<p>As described earlier, tests can be grouped into areas and levels.
For tests that can consume large amounts of system resources, it is
prudent to run them less frequently.  This can be accomplished by
associating a "level" with a unit test other than the default level.
The test level mechanism may also be used to indicate that a unit test
is not working so that unit test runs are not cluttered with known
test failures.  To define a unit test with a level other than the default
level, the AT_DefineTestLevel macro is used to define the test
as well as the expected result.</p>
<p>The following example demonstrates the use of the AT_DefineTestLevel
showing how to define a test at a different level.</p>

<CODE>
<pre>
namespace {

AT_DefineTestLevel(
    ExampleC,
    ExampleArea,
    "Example of test level other than 10",
    UnitTestTraits::TestSuccess,    // expect a Success result
    5                               // sets level 15 = ( 5 + default )
)
{
    void Run()
    {
        // This test will not run unless the run test level
        // is set to 15 or more
    }
};
AT_RegisterTest( ExampleC, ExampleArea ); // register the unit test
} // end anonymous namespace

</pre>
</CODE>

<h2>main()</h2>
<p>Tests are run by calling at::UnitTestExec.  There may, however,
be a need to perform a number of pre-main initializations, this
can be done using the at::MainInitializer as the start-up
initialization (and shut-down) manager.  The AT_MAINTEST macro defines
a minimal main() function to perform all the neccessary
initializations.</p>

<p>at::UnitTestExec takes command line parameters as follows.</p>
<UL> <b>--</b>area <i>list of areas</i> : Run tests in these areas</UL>
<UL> <b>--</b>test <i>list of tests</i> : Run these tests</UL>
<UL> <b>--</b>level <i>level</i> : Run only tests at or below the level e.g. --level 15</UL>
<UL> <b>--</b>assert_aborts : Failures to AT_Assert or calls to AT_Abort will terminate execution</UL>
<UL> <b>--</b>list : List all unit tests</UL>


 *
 * 
 *  @{
 */
    

// ======== UnitTestTraits =========================================
/**
 * Basic stuff about unit tests
 *
 */

class AUSTRIA_EXPORT UnitTestTraits
{
    public:

    /**
     * t_TestResult enumerates various test case results
     */
    
    enum t_TestResult
    {
        /**
         * DefaultResult is used by the unit test to indicate
         * that the unit test key contains the expected result
         */

        DefaultResult,

        /**
         * NotTested indicates that no test was performed
         */

        NotTested,

        /**
         * TestSuccess indicates that the test succeeded
         */

        TestSuccess,

        /**
         * TestConstructorFailure indicates that the constructor
         * for the unit test failed
         */

        TestConstructorFailure,

        /**
         * TestRunFailure indicates that the Run method failed.
         */

        TestRunFailure,

        /**
         * TestDestructorFailure indicates that the destructor
         * failed.
         */

        TestDestructorFailure

    };

};

/** @} */ // end of UnitTestInterface ( Doxygen group )

}; // namespace


namespace std {

// ======== basic_ostream<i_char_type, i_traits>& operator << =========
/**
 * Standard method for displaying a source locator
 *
 * &param i_ostream is the ostream
 * &param UnitTestKey_Basic & is the unit test key
 * &return reference to ostream
 */

template<
    typename        i_char_type,
    class           i_traits
>                   
basic_ostream<i_char_type, i_traits>& operator << (
    basic_ostream<i_char_type, i_traits>            & i_ostream,
    at::UnitTestTraits::t_TestResult                & i_value
) {

    switch ( i_value )
    {
        
        case at::UnitTestTraits::DefaultResult : {
            i_ostream << "No Result";
            break;
        }

        case at::UnitTestTraits::NotTested : {
            i_ostream << "Not tested";
            break;
        }

        case at::UnitTestTraits::TestSuccess : {
            i_ostream << "Success";
            break;
        }

        case at::UnitTestTraits::TestConstructorFailure : {
            i_ostream << "Test Constructor Failed";
            break;
        }

        case at::UnitTestTraits::TestRunFailure : {
            i_ostream << "Test Run Failed";
            break;
        }

        case at::UnitTestTraits::TestDestructorFailure : {
            i_ostream << "Test Destructor Failed";
            break;
        }

        default : {
            i_ostream << "INVALID TEST CODE";
            break;
        }
    }

    return i_ostream;
    
} // end basic_ostream<i_char_type, i_traits>& operator <<

} // namespace std

// Austria namespace
namespace at
{
/** @addtogroup UnitTestInterface */
/** @{*/

// ======== TestCase_Exception =====================================
/**
 * TestCase_Exception is thrown by a unit test when it fails.
 *
 */

class AUSTRIA_EXPORT TestCase_Exception
{
    public:

    TestCase_Exception(
        const AT_String                             & i_description,
        const char                                  * i_filename,
        int                                           i_lineno
    )
      : m_description( i_description ),
        m_locator( i_filename, i_lineno )
    {
        if ( s_abort )
        {
            AT_Abort();
        }
    }


    TestCase_Exception()
      : m_locator( 0, 0 )
    {
    }

    virtual ~TestCase_Exception()
    {
    }

    /**
     * m_description describes the unit test area.
     */
    
    AT_String                                       m_description;

    /**
     * m_locator describes the location of the unit test definition.
     */
    
    SourceLocator_Basic                             m_locator;

    /**
     * s_abort can be set if it is desired to abort instead of
     * throwing.
     */
    static bool                                     s_abort;
};


// ======== AT_TCAssert ===============================================
/**
 *  The AT_TCAssert macro throws a test case assert.
 *
 */

#define AT_TCAssert( x_expr, x_description )                            \
    if ( !( x_expr ) ) {                                                \
        throw at::TestCase_Exception(                                   \
            AT_String( x_description ),                                 \
            __FILE__,                                                   \
            __LINE__                                                    \
        );                                                              \
    }                                                                   \
// end of macro

}; // namespace

namespace std {

// ======== basic_ostream<i_char_type, i_traits>& operator << =========
/**
 * Standard method for displaying a source locator
 *
 * &param i_ostream is the ostream
 * &param UnitTestKey_Basic & is the unit test key
 * &return reference to ostream
 */

template<
    typename        i_char_type,
    class           i_traits
>                   
::std::basic_ostream<i_char_type, i_traits>& operator << (
    ::std::basic_ostream<i_char_type, i_traits>         & i_ostream,
    const at::TestCase_Exception                        & i_value
) {

    i_ostream << "Test Case Exception : " << i_value.m_locator << " - " << i_value.m_description;

    return i_ostream;
    
} // end basic_ostream<i_char_type, i_traits>& operator <<

} // namespace std

namespace at
{

/** @addtogroup UnitTestInterface */
/** @{*/

// ======== UnitTestArea_Basic =====================================
/**
 * A unit test area is used to describe an "Area" of tests as a
 * "group".
 */

class AUSTRIA_EXPORT UnitTestArea_Basic
{
    public:

    UnitTestArea_Basic(
        const char                                  * i_name,
        const char                                  * i_description,
        const char                                  * i_filename,
        int                                           i_lineno
    )
      : m_name( i_name ),
        m_description( i_description ),
        m_locator( i_filename, i_lineno )
    {
    }

    virtual ~UnitTestArea_Basic()
    {
    }

    //
    // member variables.
    //

    /**
     * m_name is the name of this area - this may be used to limit the
     * tests run to a particular area.
     */
    
    const char                                      * m_name;

    /**
     * m_description describes the unit test area.
     */
    
    const char                                      * m_description;

    /**
     * m_locator describes the location of the unit test definition.
     */
    
    SourceLocator_Basic                           m_locator;
};

/** @} */ // end of UnitTestInterface ( Doxygen group )

}; // namespace

namespace std {

// ======== basic_ostream<i_char_type, i_traits>& operator << =========
/**
 * Standard method for displaying a source locator
 *
 * &param i_ostream is the ostream
 * &param UnitTestKey_Basic & is the unit test key
 * &return reference to ostream
 */

template<
    typename        i_char_type,
    class           i_traits
>                   
::std::basic_ostream<i_char_type, i_traits>& operator << (
    ::std::basic_ostream<i_char_type, i_traits>         & i_ostream,
    const at::UnitTestArea_Basic                        & i_value
) {
    return i_ostream << i_value.m_name << " - " << i_value.m_locator << " - " << i_value.m_description;
    
} // end basic_ostream<i_char_type, i_traits>& operator <<

} // namespace std

namespace at
{

/** @addtogroup UnitTestInterface */
/** @{*/

// ======== UnitTestKey_Basic ======================================
/**
 * UnitTestKey_Basic gives a key in unit test factory map to use.
 *
 */

class AUSTRIA_EXPORT UnitTestKey_Basic
{
    public:

    UnitTestKey_Basic(
        const UnitTestArea_Basic                    * i_area,
        const char                                  * i_name,
        const char                                  * i_description,
        const char                                  * i_filename,
        int                                           i_lineno,
        UnitTestTraits::t_TestResult                  i_expected_result = UnitTestTraits::TestSuccess,
        int                                           i_level = m_level_default
    )
      : m_area( i_area ),
        m_name( i_name ),
        m_description( i_description ),
        m_locator( i_filename, i_lineno ),
        m_expected_result( i_expected_result ),
        m_level( i_level )
    {
    }

    virtual ~UnitTestKey_Basic()
    {
    }
    
    bool operator<( UnitTestKey_Basic const & i_value ) const;
    //
    // member variables.
    //

    /**
     * m_area is a pointer to the test area.
     */

    const UnitTestArea_Basic                        * m_area;

    /**
     * m_name is the name of this area - this may be used to limit the
     * tests run to a particular area.
     */
    
    const char                                      * m_name;

    /**
     * m_description describes the unit test area.
     */
    
    const char                                      * m_description;

    /**
     * m_locator describes the location of the unit test definition.
     */
    
    SourceLocator_Basic                           m_locator;

    /**
     * m_expected_result indicates the expected result.  Even though this
     * is the key, this is the way we communicate failure expectation
     * in the constructor.
     */

    UnitTestTraits::t_TestResult                  m_expected_result;

    /**
     * m_level defines the "level" of a test - tests can
     * be selected to run by level.  By default, any tests
     * of m_default_level or below are run.
     */

    int                                           m_level;   

    /**
     * m_level_default defined the default level.
     */
    static const unsigned                         m_level_default = 10;
};

/** @} */ // end of UnitTestInterface ( Doxygen group )

}; // namespace

namespace std {

// ======== basic_ostream<i_char_type, i_traits>& operator << =========
/**
 * Standard method for displaying a source locator
 *
 * &param i_ostream is the ostream
 * &param UnitTestKey_Basic & is the unit test key
 * &return reference to ostream
 */

template<
    typename        i_char_type,
    class           i_traits
>                   
::std::basic_ostream<i_char_type, i_traits>& operator << (
    ::std::basic_ostream<i_char_type, i_traits>         & i_ostream,
    const ::at::UnitTestKey_Basic                   & i_value
) {
    return i_ostream << i_value.m_name << " - " << i_value.m_locator;
    
} // end basic_ostream<i_char_type, i_traits>& operator <<

} // namespace std


namespace at
{

/** @addtogroup UnitTestInterface */
/** @{*/

// ======== UnitTestContext ===========================================
/**
 * UnitTestContext provides contextual information to test cases.
 *
 */

class AUSTRIA_EXPORT UnitTestContext
{
    public:


    // ======== GetValue ==============================================
    /**
     *  GetValue will return the pointer to the value of a named value
     *  that named value exists.
     *
     * @param i_name
     * @return pointer to string which contains the named value;
     */

    virtual const std::string * GetValue(
        const std::string       & i_name
    ) const = 0;

};


// ======== UnitTest ===============================================
/**
 * UnitTest defines the basic unit test interface.
 *
 * A unit test runs three separate tests: contruction, run and
 * destruction.
 *
 */

class AUSTRIA_EXPORT UnitTest
{
    public: 

    UnitTest( const UnitTestContext * i_context = 0 )
      : m_expected_result( UnitTestTraits::DefaultResult ),
        m_context( i_context )
    {
    }

    // ======== UnitTest ===========================================
    /**
     * Need a virtual destructor
     */
    virtual ~UnitTest() {}


    // ======== Run ===================================================
    /**
     * Run() performs the basic test - a return from the run method
     * indicates success.
     */

    virtual void Run() = 0;


    // ======== Locator ===============================================
    /**
     * A unit test locator indicates where it may be found.
     *
     */

    virtual UnitTestKey_Basic * Locator() = 0;


    // ======== GetArg ================================================
    /**
     *  GetArg returns a string parameter that was passed in the command
     *  line.
     *
     * @param i_name Name of the parameter.
     * @param i_default Default value of the paramter.
     * @return nothing
     */

    virtual const std::string & GetArg(
        const std::string       & i_name,
        const std::string       & i_default
    ) {
        if ( m_context )
        {
            const std::string * l_val = m_context->GetValue( i_name );

            if ( l_val )
            {
                return * l_val;
            }
        }
        return i_default;
    }

    

    /**
     * m_expected_result is set to the expected result.  A test is
     * deemed succeeded if the m_expected_result is the same as
     * the actual result.  This is usually set to TestSuccess.
     */

    UnitTestTraits::t_TestResult                        m_expected_result;

    /**
     * m_context points the the context of the framework
     */
    const UnitTestContext                             * m_context;
    
};



// ======== AT_TestArea ===============================================
/**
 * AT_TestArea creates a unit test area.
 *
 */

#define AT_TestArea( x_area, x_description )                            \
    static at::UnitTestArea_Basic   x_g_TestArea_ ## x_area(            \
        # x_area, x_description, __FILE__, __LINE__                     \
    )                                                                   \
// end of macro AT_TestArea


// ======== AT_DefineTest =============================================
/**
 * AT_DefineTest allows definition of the unit test class.
 *
 */

#define AT_DefineTest_( x_name, x_area, x_description, x_expect, x_level ) \
                                                                         \
    at::UnitTestKey_Basic   __g_TestKey_ ## x_area ## _ ## x_name(       \
        & x_g_TestArea_ ## x_area,                                       \
        # x_name,                                                        \
        x_description,                                                   \
        __FILE__,                                                        \
        __LINE__,                                                        \
        x_expect,                                                        \
        x_level                                                          \
    );                                                                   \
                                                                         \
    class __g_TestLocatorClass_ ## x_area ## _ ## x_name                 \
      : public at::UnitTest                                              \
    {                                                                    \
        public:                                                          \
                                                                         \
        at::UnitTestKey_Basic * Locator()                                \
        {                                                                \
            return & __g_TestKey_ ## x_area ## _ ## x_name;              \
        }                                                                \
    };                                                                   \
                                                                         \
                                                                         \
    class __g_TestClass_ ## x_area ## _ ## x_name                        \
      : public __g_TestLocatorClass_ ## x_area ## _ ## x_name            \
// end of macro

#define AT_DefineTestExpect( x_name, x_area, x_description, x_expect )  \
    AT_DefineTest_(                                                     \
        x_name,                                                         \
        x_area,                                                         \
        x_description,                                                  \
        x_expect,                                                       \
        at::UnitTestKey_Basic::m_level_default                          \
    )                                                                   \
// end of macro

#define AT_DefineTestLevel( x_name, x_area, x_description, x_expect, x_level )  \
    AT_DefineTest_(                                                     \
        x_name,                                                         \
        x_area,                                                         \
        x_description,                                                  \
        x_expect,                                                       \
        at::UnitTestKey_Basic::m_level_default + (x_level)              \
    )                                                                   \
// end of macro

#define AT_DefineTest( x_name, x_area, x_description )                  \
    AT_DefineTest_(                                                     \
        x_name,                                                         \
        x_area,                                                         \
        x_description,                                                  \
        at::UnitTestTraits::TestSuccess,                                \
        at::UnitTestKey_Basic::m_level_default                          \
    )                                                                   \
// end of macro 


// ======== AT_RegisterTest ===========================================
/**
 * AT_RegisterTest registers the unit test in the unit test
 * factory registry.
 */

#define AT_RegisterTest( x_name, x_area )                               \
                                                                        \
    at::FactoryImpl0P<                                                  \
        __g_TestClass_ ## x_area ## _ ## x_name,                        \
        at::UnitTest,                                                   \
        at::UnitTestKey_Basic                                           \
    >   __g_TestFactory_ ## x_area ## _ ## x_name(                      \
        __g_TestKey_ ## x_area ## _ ## x_name,                          \
        __FILE__,                                                       \
        __LINE__,                                                       \
        "Unit test " # x_area " " # x_name                              \
    )                                                                   \
// end of macro


// ======== UnitTestAssertModifier ====================================
/**
 * This class will modify the austria assert mechanism to allow
 * normally fatal asserts to continue.
 *
 */

class UnitTestAssertModifier;

// ======== UnitTestExec ===========================================
/**
 *  UnitTestExec is a method that executes all the unit tests.
 *
 *
 * &param i_argc - command line parameters
 * &param i_argv - command line parameters
 * &return success = 0 failure - other than 0
 */

extern AUSTRIA_EXPORT int UnitTestExec(
    int             i_argc,
    const char   ** i_argv
);


// ======== AT_MAINTEST ===============================================
/**
 * AT_MAINTEST is a shortcut macro for defining the basic main()
 * method for running the test.
 *
 */

#define AT_MAINTEST int main( int argc, const char ** argv )            \
{                                                                       \
    int retval = -1;                                                    \
    try {                                                               \
        MainInitializer     l_initializer( argc, argv );                \
        retval = at::UnitTestExec( argc, argv );                        \
    } catch (...) {                                                     \
    }                                                                   \
    return retval;                                                      \
}                                                                       \

/** @} */ // end of UnitTestInterface ( Doxygen group )

}; // namespace


#endif // x_at_unit_test_h_x


