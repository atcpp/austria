//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * \file at_lifetime.h
 *
 * \author Gianni Mariani, Richard Klein
 *
 * at_lifetime.h provides support for reference counting objects using smart pointers.
 */


#ifndef x_at_lifetime_h_x
#define x_at_lifetime_h_x 1

#include "at_exports.h"
#include "at_assert.h"
#include "at_types.h"

#include "at_pointers.h"

// Austria namespace
namespace at
{
    
// ======== Forward references =========================================
//

template <typename w_ClassRef>
    class PtrClassRefWrapper;

template < typename w_ClassRef, typename w_ClassRefWrapper = PtrClassRefWrapper<w_ClassRef> >
    class COMPtrTraits;
    
template <typename w_ClassRef, typename w_ClassRefWrapper = PtrClassRefWrapper<w_ClassRef> >
    class PtrTraits;
    
template <typename w_ClassRef, typename w_ClassRefWrapper = PtrClassRefWrapper<w_ClassRef> >
    class PtrTraitsMedusa;

template <
    typename w_ClassRef,
    typename w_First,
    typename w_Second
>
class PtrSelect
{
    public:

    /**
     * This should initialize the type traits for the first
     * class type.  In theory, this should select the "correct"
     * version (COM or regular) depending on type being detected.
     */
    typedef w_First         type;
};

template <
    typename w_ClassRef,
    typename w_RefTraits = typename PtrSelect<
        w_ClassRef,
        PtrTraits< w_ClassRef >,
        COMPtrTraits< w_ClassRef >
    >::type
> class PtrView;


#define AT_Friend_Ptrs  \
    template < \
        typename wa_ClassRef, \
        typename wa_RefTraits \
    > friend class Ptr; \
// end macro

template <
    typename w_ClassRef,
    typename w_RefTraits = typename PtrSelect<
        w_ClassRef,
        PtrTraits< w_ClassRef >,
        COMPtrTraits< w_ClassRef >
    >::type
> class Ptr;

#define AT_Friend_PtrView  \
    template < \
        typename wa_ClassRef, \
        typename wa_RefTraits \
    > friend class PtrView; \
// end macro

#define AT_Friend_PtrDelegate \
    template < \
        typename wa_ClassRef, \
        typename wa_RefTraits \
    > friend class PtrDelegate; \
// end macro


// ======== PtrTarget ===========================================
/**
 * PtrTarget is an abstract base class for any class whose lifetime
 * is managed by means of reference counting.  This is similar to the
 * interface that Win32/COM provides.
 *
 * It is not required to use this interface.  A substitute
 * PtrTraits class can allow the use of any reference counting
 * class.
 *
 */

class AUSTRIA_EXPORT PtrTarget
{
    public:

    virtual ~PtrTarget() {};  // Virtual destructor

    /**
     * The AddRef() method adds 1 to the reference count to this object.
     *
     * @return The current reference count.
     */

    virtual int AddRef() const = 0;

    /**
     * The Release() method decrements by 1 the reference count to this object.
     * Once all references have been released (the reference count is zero),
     * the PtrTarget implementation is expected to delete itself.
     * The act of "deleting itself" may be determinted to be different
     * for each kind of implementation.
     *
     * @return The current reference count.
     */

    virtual int Release() const = 0;
};


// ======== PtrStyle ==================================================
/**
 * PtrStyle defines a style of reference counted object that is
 * more natural than COM.
 *
 * REV: Please explain (1) how this works and (2) what "more natural than
 * COM" means, if that's relevant.
 */

class PtrStyle
{
    public:

    enum {

        // ======== InitCount =========================================
        /**
         * InitCount is the initial count setting.
         *
         */

        InitCount = 0,

        // ======== DeleteCheck =======================================
        /**
         * DeleteCheck may be 0 or 1 which indicates whether incrementing
         * a reference count on a deleted object is tested.
         */
        
        DeleteCheck = 1
    };


    // ======== BaseType ==============================================
    /**
     * BaseType allows the PtrStyle to define a base type (if needed).  
     *
     */

    struct BaseType
    {
    };


    // ======== LockStyleType ==============================================
    /**
     * LockStyleType is the lock used to protect the object during
     * an increment or decrement.  This is not normally needed
     * so this is a dummy implementation.
     *
     */

    struct LockStyleType
    {
        template <typename w_T>
        inline LockStyleType( w_T i_base )
        {
        }
    };

    // ======== ZeroReferenceAction ===================================
    /**
     * ZeroReferenceAction overrides what must happen when the
     * reference count drops to zero. This allows the
     * PtrTarget_Generic template to use other mechanisms.
     * 
     * @param i_obj is the this pointer of the reference-counted object
     * @param w_type is the type of the pointer being managed
     */

    template <typename w_type>
    inline static void ZeroReferenceAction( w_type i_obj )
    {
        delete i_obj;
    }
    
};


// ======== PtrCOMStyle ===============================================
/**
 * COMStyle reference counted objects start with a referece count
 * of 1 and are based on IUnknown which contains a QueryInterface
 * method.
 */

class PtrCOMStyle
{
    public:

    enum {

        // ======== InitCount =========================================
        /**
         * InitCount is the initial count setting.
         *
         */

        InitCount = 1,
        
        // ======== DeleteCheck =======================================
        /**
         * DeleteCheck may be 0 or 1 which indicates whether incrementing
         * a reference count on a deleted object is tested.  This does
         * not apply when InitCount = 1.
         */
        
        DeleteCheck = 0
    };


    // ======== BaseType ==============================================
    /**
     * BaseType allows the PtrStyle to define a base type (if needed).  
     *
     */

    struct BaseType
    {
        /**
         * PtrTargetMarker allows the detection of this type
         * in derived classes.
         */
        typedef int                             PtrTargetMarker;
    };
    
    // ======== LockStyleType ==============================================
    /**
     * LockStyleType is the lock used to protect the object during
     * an increment or decrement.  This is not normally needed
     * so this is a dummy implementation.
     *
     */

    struct LockStyleType
    {
        template <typename w_T>
        inline LockStyleType( w_T i_base )
        {
        }
    };

    // ======== ZeroReferenceAction ===================================
    /**
     * ZeroReferenceAction overrides what must happen when the
     * reference count drops to zero. This allows the
     * PtrTarget_Generic template to use other mechanisms.
     * 
     * @param i_obj is the this pointer of the reference-counted object
     * @param w_type is the type of the pointer being managed
     */

    template <typename w_type>
    inline static void ZeroReferenceAction( w_type i_obj )
    {
        delete i_obj;
    }
    
};


// ======== PtrVirtualStyle ===========================================
/**
 * PtrVirtualStyle defines a style that works for virtual functions.
 *
 */

class PtrVirtualStyle
{
    public:

    enum {

        // ======== InitCount =========================================
        /**
         * InitCount is the initial count setting.
         *
         */

        InitCount = 0,

        // ======== DeleteCheck =======================================
        /**
         * DeleteCheck may be 0 or 1 which indicates whether incrementing
         * a reference count on a deleted object is tested.
         */
        
        DeleteCheck = 1
    };


    // ======== BaseType ==============================================
    /**
     * BaseType allows the PtrStyle to define a base type (if needed).  
     *
     */

    struct BaseType
      : public virtual PtrTarget
    {
    };


    // ======== LockStyleType ==============================================
    /**
     * LockStyleType is the lock used to protect the object during
     * an increment or decrement.  This is not normally needed
     * so this is a dummy implementation.
     *
     */

    struct LockStyleType
    {
        template <typename w_T>
        inline LockStyleType( w_T i_base )
        {
        }
    };

    // ======== ZeroReferenceAction ===================================
    /**
     * ZeroReferenceAction overrides what must happen when the
     * reference count drops to zero. This allows the
     * PtrTarget_Generic template to use other mechanisms.
     * 
     * @param i_obj is the this pointer of the reference-counted object
     * @param w_type is the type of the pointer being managed
     */

    template <typename w_type>
    inline static void ZeroReferenceAction( w_type i_obj )
    {
        delete i_obj;
    }
    
};


// ======== PtrTarget_Helpers =========================================
/**
 * PtrTarget_Helpers contains helper methods.
 *
 */

template <typename w_count_t, typename w_ptr_style_traits = PtrStyle>
class PtrTarget_Generic;

template <typename w_count_t, typename w_ptr_style_traits>
class PtrTarget_Helpers
{
    public:

    // ======== IsRefCountOne =========================================
    /**
     * IsRefCountOne checks to see if there is a single reference to an
     * object.  This can be used in optimizations where objects can't
     * be shared but this check can eliminate the unnecessary creation
     * of a new object.
     *
     * @param i_ptr is the PtrTarget_Generic being checked
     * @return True if the reference count is currently 1
     */

    static bool IsRefCountOne(
        const PtrTarget_Generic<w_count_t,w_ptr_style_traits> * i_ptr
    )
    {
        return 1 == i_ptr->m_ref_count;
    }
};

// ======== PtrTarget_Generic =========================================
/**
 * PtrTarget_Generic is a generic reference counting class that
 * is suitable for multi-threaded or non multi-threaded use. This
 * is done by substitution of the w_count. For multi-threaded
 * use, make the w_count parameter an atomic count.
 *
 * @param w_count_t the count type - this can be any type that supports
 * pre-increment or pre-decrement and compares equal to or greater
 * than zero.
 */

template <typename w_count_t, typename w_ptr_style_traits>
class PtrTarget_Generic
  : public w_ptr_style_traits::BaseType
{
    public:

    /**
     * t_ptr_style defines the style type of this class.
     */

    typedef w_ptr_style_traits                  t_ptr_style;

    /**
     * t_count is the type of the reference count.
     */
    typedef w_count_t                           t_count;

    /**
     * PtrTarget_Helpers allows access to internal methods
     */
    
    friend class PtrTarget_Helpers<t_count,w_ptr_style_traits>;

    private:

    // for unit test only.
    friend class AT_TEST_LifeControlExp;
    
    enum {
        
        // ======== DoDeleteCheck =====================================
        /**
         * DoDeleteCheck is only used by the test suite and is
         * set to true if an extra decrement of the reference count
         * is done when the object is deleted.
         */
        
        DoDeleteCheck = 
            OSTraitsBase::DebugBuild
            && ( ! t_ptr_style::InitCount )
            && t_ptr_style::DeleteCheck
    };

    // m_ref_count is mutable because references may increment
    // or decrement on const versions of this object as well
    // as the non-const version.
    mutable t_count                             m_ref_count;

    // friend class declaration that allows the MPT_Finder
    // to find the PtrTargetMarker.
    template <
        typename wa_D,
        typename wa_TD
    > friend struct MPT_Finder;
    
    // ======== AssertGreater ==========================================
    /**
     * AssertGreater checks to see if the i_v > i_cmp.  The check is 
     * type independent.
     */

    template <typename w_count_basis, typename w_compare_t>
    static inline w_count_basis AssertGreater( w_count_basis i_v, w_compare_t i_cmp )
    {
        AT_Assert( i_v > i_cmp );
        return i_v;
    }
    
    public:
    //
    // this should force derived destructors to be virtual
    virtual ~PtrTarget_Generic() {};  // Virtual destructor
    
    /**
     * PtrTarget_Generic() initializes with a reference count of 1.
     * Upon creation, the object has a reference (the creator) and so
     * the reference count is set to InitCount.
     */
    
    PtrTarget_Generic()
      : m_ref_count( t_ptr_style::InitCount )
    {
    }

    /**
     * The copy constructor also initializes with a reference count of InitCount.
     * The reference count is a count of the references to this which has
     * nothing to do with the object that is being copied.  Hence the
     * reference count on copy construction is also set to InitCount.
     */
    
    PtrTarget_Generic( const PtrTarget_Generic & )
      : m_ref_count( t_ptr_style::InitCount )
    {
    }

    /**
     * Assignment operator.  The reference count of the parameter
     * has no bearing on the count of references to this.  This is why
     * the input parameter reference count is ignored.
     */

    PtrTarget_Generic & operator = ( const PtrTarget_Generic & )
    {
        return * this;
    }

    /**
     * The AddRef() method adds 1 to the reference count to this object.
     *
     * @return The current reference count.
     */

    int AddRef() const
    {
        // perform locking ... this is usually a no-op.
        typename t_ptr_style::LockStyleType       l_lock( this );
        
        return AssertGreater(
            ++ m_ref_count,
            static_cast<int>( t_ptr_style::InitCount )
        );
    }

    /**
     * The Release() method decrements by 1 the reference count to this object.
     * Once all references have been released (the reference count is zero),
     * the PtrTarget implementation is expected to delete itself.
     * The act of "deleting itself" may be determined to be different
     * for each kind of implementation.
     *
     * @return The current reference count.
     */

    int Release() const
    {
        // perform locking ... this is usually a no-op.
        typename t_ptr_style::LockStyleType       l_lock( this );

        int l_ret_val = AssertGreater( m_ref_count --, 0 ) - 1;
        
        if ( l_ret_val == 0 )
        {
            // Here we do an extra decrement if we are doing
            // assert checking.  This is because we want to
            // find cases where deleted pointers have their
            // reference count incremented.  There is no
            // guarantee that this check will work because the
            // allocated memory may be allocated.
            if ( DoDeleteCheck  )
            {
                -- m_ref_count;
            }

            // ZeroReferenceAction usually deletes the
            // pointer passed in.
            t_ptr_style::ZeroReferenceAction( this );
            return 0;
        }

        return l_ret_val;
    }

    // ======== GetRefCount ===========================================
    /**
     * GetRefCount is indented for debugging use only. Applicaton 
     * code should not need to know what the reference count is.
     * This returns a const reference to the count object.
     *
     * @return A const reference to the reference count
     */

    const t_count & GetRefCount() const
    {
        return m_ref_count;
    }    

};


// ======== IsRefCountOne =============================================
/**
 * IsRefCountOne will return true if the reference count of a
 * reference counted object is 1.
 *
 * @param i_ptr is the PtrTarget_Generic being checked
 * @return True if the reference count is currently 1
 */

template <typename w_count_t, typename w_ptr_style_traits>
bool IsRefCountOne(
    const PtrTarget_Generic<w_count_t, w_ptr_style_traits> * i_ptr
) {

    if ( i_ptr )
    {
        return PtrTarget_Helpers<w_count_t, w_ptr_style_traits>::IsRefCountOne( i_ptr );
    }
    else
    {
        return false;
    }
}

// ======== PtrTarget_Basic =========================================
/**
 * PtrTarget_Basic is a reference counting base class
 * implementation.
 *
 * This is useful for cases where an abstract class is not required.
 * With a good optimizing compiler, this class will create very lean code
 * because all calls to increment and decrement reference counts
 * may be inlined.
 */
AUSTRIA_TEMPLATE_EXPORT(PtrTarget_Generic, int)
typedef PtrTarget_Generic<int> PtrTarget_Basic;

// ======== PtrTarget_Virtual =========================================
/**
 * PtrTarget_Virtual is an implementation of the PtrTarget class 
 * and hence needs to inherit PtrTarget using virtual
 * inheritance.
 */

AUSTRIA_TEMPLATE_EXPORT2(PtrTarget_Generic, int, PtrVirtualStyle)
typedef PtrTarget_Generic<int, PtrVirtualStyle> PtrTarget_Virtual;


/**
 * @defgroup SmartPointerTrio Smart Pointer Support

<h2>Introduction</h2>

<p>Reference counted pointers are a useful idiom for simplification
of developing code.  Using reference counts, it can be determined when
an object is no longer needed and hence can be deleted without regard to
knowledge of complex interrelations within a system.  However, this
simple idiom leads to suprisingly complex code which
inevitably leads to errors in maintaining proper reference counts
which can be a source for many future errors.
</p>
<p>
The use of C++ exceptions also may create resource leaks and possible
logic errors which can be difficult to manage using try/catch blocks.
The "Initialization IS Acquisition" idiom provides
a handy way to eliminate errors from exceptions.  Exception safety is very
important for libraries as they are a common way to manage "exceptional"
cases.
</p>
<p>
The name "smart pointer" is used to describe a pointer-like object that
performs house-keeping.
A smart pointer for "reference counted" objects is able to greatly
simplify the programming tasks to maintain accurate reference counts.
While there are a small number of smart pointer implementations, the
design goals for this implementation are performance, legacy support
and support for class designs that are more appropriate for intrusive
reference counting. (Intrusive reference counting is where the reference
counting mechanism is provided within the class being managed).
</p>
<p>
There is no panacea however.  The reference counting idiom creates a significant
design issue, namely the circular reference.  Designs which have circular
references will leak and not perform as intended.  Hence designs that
require complex networks of pointers require non trivial design rules
to eliminate the conditions that cause cyclic referencing.  There are a
number of tools that may be adopted to manage the effects of circular
references and this library introduces the concept of the LeadTwin
and AideTwin interfaces that may be used also to manage the
circular reference class design issues.
</p>
<p>
A popular alternative library for managing shared pointers is boost::shared_ptr.
Boost::shared_ptr is an alternative implementation and approaches the problem of
circular references using the "weak_ptr" concept.  The design goals of
boost::shared_ptr are different from those of at::Ptr.
boost::shared_ptr in particular does not manage legacy support without
creating support classes and the weak_ptr support adds extra overhead that
does not mesh with the Ptr* performance goal. (In most cases, using at::PtrView or
at::PtrDelegate is as efficient as passing raw pointers).
</p>

<h2>at::Ptr* Smart Pointer Basics</h2>
<p>
The set of co-operative "smart" pointers
Ptr, PtrDelegate and PtrView simplify the task of 
maintaining reference counted pointers.  The intent is to create
a mechanism that makes it easier for a programmer to avoid inadvertent
errors, but is also efficient.
</p>

<p>PtrDelegate and PtrView
are intended to be used to specify pointer transfer policy (function call or
function return value) while
Ptr is intended to be instantiated as an object member, or automatic
or global variable to maintain the reference to the object being managed.
If used correctly, no explicit action to increment or decrement
reference counts is required.  In cases where PtrDelegate may not
be appropriate to pass (perhaps as an STL container specialization),
Ptr may be substituted in place of PtrDelegate.
</p>

<p>It is important to note that PtrDelegate is intended to be initialized once
and transferred into a Ptr or simply remain unreferenced.  Once a
PtrDelegate is assigned to a Ptr or another PtrDelegate, the internal
managed pointer contained in the PtrDelegate object is no longer valid.

<table>
<tr>
    <th colspan=5>Smart Pointer Conversion Semantics</th>
</tr>
<tr>
    <th>LHS =</th>
    <th colspan=4 align=left>RHS</th>
</tr>
<tr>
    <th>Assign To Below</th>
    <th>Ptr</th>
    <th>PtrDelegate</th>
    <th>PtrView</th>
    <th>Regular Pointer</th>
</tr>

<tr>
    <th>Ptr</th>
    <td>Increment</td>
    <td>Transfer</td>
    <td>Increment</td>
    <td>Assign **</td>
</tr>

<tr>
    <th>PtrDelegate</th>
    <td>Increment</td>
    <td>Transfer</td>
    <td>Increment</td>
    <td>Assign **</td>
</tr>

<tr>
    <th>PtrView</th>
    <td>Assign</td>
    <td>Assign</td>
    <td>Assign</td>
    <td>Assign **</td>
</tr>

</table>
<h5>Definitions of Semantics</h5>
<p>
<ul>
<li>\a Increment : assigned pointer's reference count
is incremented and the pointer is assigned to the destination.
<li>\a Transfer : pointer value is removed destructively from the smart pointer
being read from and placed in the pointer being written to.  No increment
takes place.  The PtrDelegate pointer being read will not be valid after the
assignment takes place.
<li>\a Assign means that the pointer is being copied and no increment
is taking place.
</ul>
</p>

<p>** It is important to note that assignment of a raw pointer to a PtrDelegate
or Ptr does not increment the reference count while assignment to a
PtrView followed by an assignment to a PtrDelegate or Ptr does increment
the reference count.  Regular pointers to reference counted objects should be rare
and only available from a new or legacy interface.
</p>

<p>Upon destruction of Ptr and PtrDelegate smart pointers, the reference
count of the managed object is decremented.  Note the omission of PtrView in
the prior statement.  This corresponds to the table above where 
assignment to PtrView performs no reference count increment.
</p>


<h3>How to Use Smart Pointers</h3>
<p>For any good craftsman, understanding the tools is the first imperative. The
smart pointers described here are a very thin layer over regular pointers and if
adopted correctly will eliminate virtually all leaks due to sloppy programming as
well as provide exception safety.

<p>Below are a set of guidelines for creating applications using smart pointers.
</p>

<ol>
<li>Never pass or return unmanaged reference counted pointers
unless it is required by legacy interfaces.  The pointers PtrView and PtrDelegate
are "policy" pointers indicating the intention to pass along the obligation to the
recipient to decrement the reference count. In some instances you may be required
to pass a Ptr (e.g. if instantiating an STL std::list of pointers) which
is not an issue. 

<li>Whenever
a reference counted object is created (the result of new or a factory request),
place the pointer immediately into a Ptr or PtrDelegate object.  When managing
a pointer in a legacy interface (e.g. Win32/COM) make sure to place the unmanaged
raw pointer directly into a managed pointer of appropriate type (if obligation to
release is passed, place into a Ptr or PtrDelegate or if no obligation
is passed, place into a PtrView followed by assignment to a Ptr
if you wish to maintain a reference to the object.

<li>
Since PtrView is the least expensive (i.e. no increment of
reference count is required) of the smart pointers, use this in preference to
PtrDelegate where the situation permits.

<li>
Be careful to note that once a PtrDelegate is assigned to another PtrDelegate or a
Ptr, its value is no longer valid and no further use of the smart pointer is
possible. If the intention is to maintain a reference to the pointer, place the pointer
in a Ptr and use the PtrDelegate smart pointer instead.

<li>
Avoid cyclic dependencies. (e.g. A->B B->A.)  Cyclic references will not be managed
correctly. Use a LeadTwin and AideTwin interface if appropriate or an
indirection class to remove the cyclic dependency.
</ol>

</p>


<h3>Example of Reference Counted Smart Pointers</h3>

<p>Below is a simple example of a class implemented with and without using smart pointers.

<CODE>
<pre>

// A reference counted class (without smart pointers)
class Node : public PtrTarget_Basic
{
    Node * left;
    Node * right;

    Node()
      : left(0), right(0)
    {}

    ~Node()
    {
        if ( left ) left->Release();
        if ( right ) right->Release();
    }
    
    // i_node is passed without a reference count
    void InsertLeft( Node * i_node ) 
    {
        Node * old_left = left;

        // Note the AddRef before the Release !
        // in complex systems a release may cause
        // errors (for example releasing itself)
        
        i_node->AddRef();
        left = i_node;
        
        if ( old_left ) old_left->Release();
    }

};

// A reference counted class (using smart pointers)
class Node : public PtrTarget_Basic
{
    Ptr<Node *> left;
    Ptr<Node *> right;

    void InsertLeft( PtrView<Node *> i_node ) 
    {
        left = i_node;
    }
};

</pre>
</CODE>

<p>
Note the simplification of the constructor and destructor between the
2 examples above. Also note the comment for InsertLeft in the first example
where no such comment is needed for the InsertLeft with smart pointers.
</p>

<p>As you can see above, the example is simple yet compelling. More complex
algorithms are also more compelling because the programming complexity for
dealing with maintaining reference counting manually can become difficult
to maintain.  Experience suggests that smart pointers almost eliminate
reference counting programming errors.
</p>

<h3>Unlimited Potential</h3>
<p>There are a number of other potential uses for Ptr. One is that
obviously the result of a 0 reference count need not delete the object
(deleting the object is simply the most common usage) and it may instead
be used as a way to trigger some other event e.g. read lock/write lock
semantics.

 *
 * 
 *  @{
 */


// ======== PtrClassRefWrapper ========================================
/**
 * PtrClassRefWrapper contains the state to manage access to the
 * reference counted pointer.  A different implementation of this
 * class would be needed to handle multi-threaded access.
 *
 */

template <typename w_ClassRef>
class PtrClassRefWrapper
{
    /**
     * m_ptr contains the pointer to the reference
     * counted object.
     */
    w_ClassRef          m_ptr;

    public:
    /**
     * t_ClassRefWrapper default constructor
     */
    inline PtrClassRefWrapper()
      : m_ptr()
    {
    }

    /**
     * t_ClassRefWrapper default constructor
     */
    inline PtrClassRefWrapper( w_ClassRef i_ptr )
      : m_ptr( i_ptr )
    {
    }

    /**
     * SingleThreadedAccess can be called when it is
     * guarenteed that only a single thread will access
     * the pointer.
     */
    inline w_ClassRef & SingleThreadedAccess()
    {
        return m_ptr;
    }

    /**
     * SafeAccess is usually used in the destructor.
     */
    inline w_ClassRef & SafeAccess()
    {
        return m_ptr;
    }

    /**
     * PurgePointer is used to clear the pointer for
     * instrumented pointers.  In this case it does
     * nothing.
     *
     */

    inline void PurgePointer()
    {
    }
    
    /**
     * IncRefCountTest will test that the object exists
     * before incrementing the reference count.
     */
    template <typename w_ClassRefLhs, typename w_Traits>
    inline w_ClassRefLhs IncRefCountTest()
    {
        w_ClassRefLhs           l_ptr( m_ptr );

        if ( l_ptr )
        {
            w_Traits::IncRefCount( l_ptr );
        }
        return l_ptr;
    }

    /**
     * SwapPtr will swap the i_ptr and the local pointer.
     * In a Ptr accessed in a multithreaded way, this must be
     * single threaded.
     */
    inline w_ClassRef SwapPtr( w_ClassRef i_ptr )
    {
        w_ClassRef          l_ptr( m_ptr );
        m_ptr = i_ptr;

        return l_ptr;
    }


    private :
    // PtrClassRefWrapper does not allow copy construction or
    //

    // no copy constructor
    PtrClassRefWrapper( const PtrClassRefWrapper & i_val );

    // no assignment
    PtrClassRefWrapper & operator = ( const PtrClassRefWrapper & i_val );

};


// ======== PtrTraits =========================================
/**
 * PtrTraits is a helper class that wraps 
 * AddRef() and Release() methods inside static methods having the
 * standard names IncRefCount() and DecRefCount().
 *
 * If you are using your own reference-counting scheme, i.e. one not
 * based on methods named AddRef() and Release(), then you will need to
 * define your own
 * PtrTraits class that implements the same interface as PtrTraits.
 *
 * Note that these methods will not work correctly with the COM IUnknown
 * interface. Use COMPtrTraits
 */

template <typename w_ClassRef, typename w_ClassRefWrapper>
class PtrTraits
{
    public:

    /**
     * t_ClassRefWrapper is the wrapper to use for PtrTraits.
     */
    typedef w_ClassRefWrapper       t_ClassRefWrapper;

    /**
     * FixRawPtrRefCount performs increments of reference 
     * counts when a raw pointer is passed to a
     * PtrDelegate<> or a Ptr<>. In COMPtrTraits, this
     * should perform a static assert and fail to compile.
     *
     * @param i_ptr  A pointer to the reference-counted object.
     */

    static inline w_ClassRef FixRawPtrRefCount( w_ClassRef i_ptr )
    {
        if ( i_ptr )
        {
            i_ptr->AddRef();
        }
        return i_ptr;
    }

    /**
     * IncRefCount() increments the reference count of an object.
     *
     * @param  i_ptr  A pointer to the reference-counted object.
     */

    static inline void IncRefCount( w_ClassRef i_ptr )
    {
        i_ptr->AddRef();
    }

    /**
     * DecRefCount() decrements the reference count of an object.
     * Once the reference count reaches zero, the object will
     * delete itself.
     *
     * @param  i_ptr  A pointer to the reference-counted object.
     */

    static inline void DecRefCount( w_ClassRef i_ptr )
    {
        i_ptr->Release();
    }

};

// ======== COMPtrTraits =========================================
/**
 * COMPtrTraits is a helper class that wraps 
 * AddRef() and Release() methods inside static methods having the
 * standard names IncRefCount() and DecRefCount().
 *
 * If you are using your own reference-counting scheme, i.e. one not
 * based on methods named AddRef() and Release(), then you will need to
 * define your own
 * PtrTraits class that implements the same interface as PtrTraits.
 *
 * Note that these methods will work correctly with the COM IUnknown
 * interface.
 */

template <typename w_ClassRef, typename w_ClassRefWrapper>
class COMPtrTraits
{
    public:

    /**
     * t_ClassRefWrapper is the wrapper to use for PtrTraits.
     */
    typedef w_ClassRefWrapper       t_ClassRefWrapper;

    /**
     * FixRawPtrRefCount performs increments of reference 
     * counts when a raw pointer is passed to a
     * PtrDelegate<> or a Ptr<>. In COMPtrTraits, this
     * should perform a static assert and fail to compile.
     *
     * @param i_ptr  A pointer to the reference-counted object.
     */

    // DO NOT DEFINE THIS - This detects erroneous usage
    // static inline w_ClassRef FixRawPtrRefCount( w_ClassRef i_ptr )

    /**
     * IncRefCount() increments the reference count of an object.
     *
     * @param  i_ptr  A pointer to the reference-counted object.
     */

    static inline void IncRefCount( w_ClassRef i_ptr )
    {
        i_ptr->AddRef();
    }

    /**
     * DecRefCount() decrements the reference count of an object.
     * Once the reference count reaches zero, the object will
     * delete itself.
     *
     * @param  i_ptr  A pointer to the reference-counted object.
     */

    static inline void DecRefCount( w_ClassRef i_ptr )
    {
        i_ptr->Release();
    }

};

// ======== PtrTraitsMedusa ===================================
/**
 * PtrTraitsMedusa is a PtrTraits class that allows management
 * of 2 types of reference counts within the one class.
 *
 * Not only does it manage the usual AddRef() and Release() methods
 * but also the number of references of a particular interface.
 * The managed class must provide overloaded MedusaInc and
 * MedusaDec methods that will be called as well as the regular
 * AddRef and Release methods.
 *
 * To have a PtrTraitsMedusa traits type used, you may
 * make a partial specialization of the PtrSelect class
 * (right next to the definition of the class in question
 * would be best) that defines "type" as a PtrTraitsMedusa<T>.
 * e.g.
<CODE>
<pre>

class X
{
    ...
};

//
// Select a Medusa Traits class
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< X *, w_First, w_Second >
{
    public:

    typedef PtrTraitsMedusa<X *>  type;
};

//
// Select a Medusa Traits class
template <
    typename w_First,
    typename w_Second
>
class PtrSelect< const X *, w_First, w_Second >
{
    public:

    typedef PtrTraitsMedusa<X *>  type;
};


</pre>
</CODE>
 * 
 */

template <typename w_ClassRef, typename w_ClassRefWrapper>
class PtrTraitsMedusa
{
    public:

    /**
     * t_ClassRefWrapper is the wrapper to use for PtrTraitsMedusa
     */
    typedef w_ClassRefWrapper       t_ClassRefWrapper;

    /**
     * FixRawPtrRefCount performs increments of reference 
     * counts when a raw pointer is passed to a
     * PtrDelegate<> or a Ptr<>.
     *
     * @param i_ptr  A pointer to the reference-counted object.
     */

    static inline w_ClassRef FixRawPtrRefCount( w_ClassRef i_ptr )
    {
        if ( i_ptr )
        {
            i_ptr->AddRef();
            i_ptr->MedusaInc( i_ptr );
        }
        return i_ptr;
    }

    /**
     * IncRefCount() increments the reference count of an object.
     *
     * @param  i_ptr  A pointer to the reference-counted object.
     */

    static inline void IncRefCount( w_ClassRef i_ptr )
    {
        i_ptr->AddRef();
        i_ptr->MedusaInc( i_ptr );
    }

    /**
     * DecRefCount() decrements the reference count of an object.
     * Once the reference count reaches zero, the object will
     * delete itself.
     *
     * @param  i_ptr  A pointer to the reference-counted object.
     */

    static inline void DecRefCount( w_ClassRef i_ptr )
    {
        i_ptr->MedusaDec( i_ptr );
        i_ptr->Release();
    }
    
};

    
// ======== PtrDelegate ================================================
/**
 *
 * PtrDelegate is a member of the set of co-operative "smart" pointers
 * Ptr, PtrDelegate and PtrView.  PtrDelegate and PtrView
 * are intended to be used to specify pointer passing policy while
 * Ptr is intended to be instantiated as a member or automatic
 * variable to maintain the reference to the object being managed.
 *
 * PtrDelegate indicates that the obligation to decrement the reference
 * count is being passed along with the pointer.  In essence, the
 * action of a PtrDelegate is very similar to that of a Ptr
 * except that there is an implicit Transfer() upon assignment.
 * The pointer contained within the PtrDelegate is no longer valid
 * after the transfer occurs.  The value of the smart pointer after
 * the assignment is not defined and may cause an exception if used.
 *
 * Below is an example showing how to use a PtrDelegate and the
 * implications of the implicit transfer.
 *
<CODE>
<pre>
// an example of using AT_LineLine

    class Stuff {
        at::Ptr<Interface *> m_ptr;
        
        void StashPointer( PtrDelegate<Interface *> ptr )
        {
            if ( ptr->IsGood() ) {
                m_ptr = ptr; // ptr transfers to member m_ptr **
            }

            ptr->Done(); // ERROR - ptr may be undefined

            m_ptr->Done(); // This is correct.

            // NOTE - if ptr was not transferred in the line ** then
            // it will cause the reference count to be decremented
            // on the exit of this function
            
        }

        void DoStuff( PtrDelegate<Interface *>  ptr )
        {
            if ( ptr != 0 ) {
                StashPointer( ptr ); // ptr is transferred to parameter
                // ptr may no longer be used from this point
            }
        }
        
        void DoOther()
        {
            DoStuff( new InterfaceImpl ); // passing obligation to release

            DoMore( m_ptr );
        }

        void DoMore( PtrView<Interface *>  ptr ) )
        {
            StashPointer( ptr );  // View->Line conversion
                                  // * reference count incremented

            ptr->More(); // ptr is unchanged - assumption that
                         // reference count does not go to 0.
                         // It is the obligation of the developer to
                         // ensure that a reference is maintained.
        }

    };

</pre>
</CODE>
 *
 * \warning The current implementation of PtrDelegate may change in such a way
 * that may assert if a PtrDelegate pointer is used after a
 * transfer (implied with assignment to a Ptr or a PtrView).
 * This would be to ensure that the semantics described above are not
 * inadvertently ignored hence causing mysterious problems.
 *
 */


template <
    typename w_ClassRef,
    typename w_RefTraits = typename PtrSelect<
        w_ClassRef,
        PtrTraits< w_ClassRef >,
        COMPtrTraits< w_ClassRef >
    >::type
>
class PtrDelegate
{
    AT_Friend_PtrView
    AT_Friend_Ptrs
    AT_Friend_PtrDelegate

    /**
     * m_ptrVal is the data member that contains the actual pointer value,
     * i.e. the address of the object whose lifetime is
     * being managed by reference counting.
     */

    mutable typename w_RefTraits::t_ClassRefWrapper m_ptrVal;

    /**
     * This method releases the existing managed pointer, i.e.
     * decrements its reference count, and substitutes a
     * new one.
     *
     * @param  i_ptrVal  A regular pointer to the new managed pointer.
     * @return           A regular pointer to the new managed pointer.
     */

    inline void ReleasePointee( w_ClassRef i_ptrVal )
    {
        //
        // The order in which this happens is critical.
        // The new pointer must be in place before decrementing
        // the reference count.
        // 
        
        // Save a pointer to the old managed pointer.
        w_ClassRef l_ptrVal = m_ptrVal.SwapPtr( i_ptrVal );

        // Decrement the old managed pointer's reference count.
        if ( l_ptrVal ) {
            w_RefTraits::DecRefCount( l_ptrVal );
        }

    }

public:

    /**
     * Copy constructor for PtrDelegate.
     *
     * @param  i_ptr  A reference to the PtrDelegate pointer
     *                being copied. 
     */
    
    inline PtrDelegate(
        const PtrDelegate & i_ptr
    )
      : m_ptrVal( i_ptr.Transfer() )
    {
    }

    /**
     * Construct a PtrDelegate from another
     * PtrDelegate which may be based on a different under-
     * lying pointer type.
     *
     * @param  i_ptr  A reference to the PtrDelegate pointer
     *                being copied.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrDelegate(
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.Transfer() )
    {
    }

    /**
     * Construct a PtrDelegate from a Ptr.
     * The object being pointed to will have its
     * reference count incremented.
     *
     * @param  i_ptr  A reference to the Ptr pointer
     *                being copied.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrDelegate(
        const Ptr < w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() )
    {
    }

    /**
     * This method constructs a PtrDelegate from a PtrView.
     *
     * @param  i_ptr  A reference to the PtrView pointer
     *                being copied.
     */


    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrDelegate(
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() )
    {
    }


    /**
     * This method constructs a PtrDelegate from a regular pointer.
     * This is only allowed if the traits class allows assignment
     * from regular pointers.
     *
     * @param  i_ptr  The pointer being copied (optional).
     */

    inline PtrDelegate( w_ClassRef i_ptr = 0 )
      : m_ptrVal( w_RefTraits::FixRawPtrRefCount( i_ptr ) )
    {
    }

    /**
     * Constructs a PtrDelegate from a regular pointer,
     * optionally transferring ownership of the managed pointer to the
     * PtrDelegate pointer.
     *
     * @param  i_ptr            The regular pointer being copied.
     *
     * @param  i_takeOwnership  A flag indicating whether owner-
     *                          ship of the managed pointer should be
     *                          transferred to the PtrDelegate
     *                          pointer.
     */

    inline PtrDelegate( w_ClassRef i_ptr, bool i_takeOwnership )
      : m_ptrVal( i_ptr )
    {
        if ( i_takeOwnership ) {
            if ( i_ptr ) {
                w_RefTraits::IncRefCount( i_ptr );
            }
        }
    }

    /**
     * Upon destruction, the managed pointer will have its
     * reference count decremented.
     */

    inline ~PtrDelegate()
    {
        w_ClassRef  i_ptrVal = m_ptrVal.SafeAccess();
        // Releases the managed pointer
        if ( i_ptrVal ) {
            m_ptrVal.PurgePointer();
            w_RefTraits::DecRefCount( i_ptrVal );
        }
    }

    /**
     * Assignment operator for the
     * case where a PtrDelegate is being set equal to a
     * regular pointer.  This will only compile for
     * cases where the traits class contains a
     * FixRawPtrRefCount method.  Hence to disable
     * assigning of regular pointers use a PtrTraits
     * class that does not have a FixRawPtrRefCount.
     *
     * @param  i_ptr  The regular pointer on the right-hand side
     *                of the '=' sign.
     *
     * @return        A reference to the PtrDelegate on
     *                the left-hand side of the '=' sign.
     */

    inline PtrDelegate & operator= ( const w_ClassRef i_ptr )
    {

        // Perform assignment and use the FixRawPtrRefCount
        // RefTraits method to fix the reference count if
        // needed.
        ReleasePointee( w_RefTraits::FixRawPtrRefCount( i_ptr ) );

        // Return a reference to the PtrDelegate, so that
        // assignment operations can be chained together.
        return *this;
    }

    /**
     * Assignment operator for the
     * case where a PtrDelegate is being set equal to another
     * PtrDelegate.
     *
     * @param  i_ptr  A reference to the PtrDelegate on the
     *                right-hand side of the '=' sign.
     *
     * @return        A reference to the PtrDelegate on the
     *                left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrDelegate & operator= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        // Perform assignment by transferring responsibility.
        //
        ReleasePointee( i_ptr.Transfer() );

        // Return a reference to the destination PtrDelegate,
        // so that assignment operations can be chained together.
        return * this;
    }

    /**
     * Copy assignment.  If this is not provided, there will be one generated
     * that will do the wrong thing.
     *
     * @param  i_ptr  A reference to the PtrDelegate on the
     *                right-hand side of the '=' sign.
     *
     * @return        A reference to the PtrDelegate on the
     *                left-hand side of the '=' sign.
     */

    inline PtrDelegate & operator= (
        const PtrDelegate & i_ptr
    )
    {
        // Perform assignment by transferring responsibility.
        //
        ReleasePointee( i_ptr.Transfer() );

        // Return a reference to the destination PtrDelegate,
        // so that assignment operations can be chained together.
        return * this;
    }

    /**
     * Assignment operator for the
     * case where a PtrDelegate is being set equal to a Ptr.
     *
     * @param  i_ptr  A reference to the Ptr on the
     *                right-hand side of the '=' sign.
     *
     * @return        A reference to the PtrDelegate on the
     *                left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrDelegate & operator= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        // Perform assignment
        //
        ReleasePointee( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() );

        // Return a reference to the PtrDelegate, so that
        // assignment operations can be chained together.
        return *this;
    }

    /**
     * Assignment operator for the
     * case where a PtrDelegate is being set equal to a
     * PtrView.
     *
     * @param  i_ptr  A reference to the PtrView on the
     *                right-hand side of the '=' sign.
     *
     * @return        A reference to the PtrDelegate on the
     *                left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrDelegate & operator= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {

        // Perform assignment
        //
        ReleasePointee( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() );

        // Return a reference to the PtrDelegate, so that
        // assignment operations can be chained together.
        return *this;
    }

    /**
     * This helper method returns the pointer being managed by this
     * PtrDelegate, and erases it from the PtrDelegate to indicate
     * responsibility for decrementing the reference count has been
     * transferred back to Transfer()'s caller.
     *
     * @return       The value of the PtrDelegate pointer,
     *               i.e. the address of the managed pointer.
     */

    inline w_ClassRef Transfer() const
    {
        return m_ptrVal.SwapPtr( 0 );
    }

    /**
     * This helper method returns a PtrView version of
     * this PtrDelegate pointer.
     *
     * @return       A PtrView version of this PtrDelegate
     *               pointer.
     */

    inline PtrView< w_ClassRef, w_RefTraits > LifeView() const
    {
        return PtrView< w_ClassRef, w_RefTraits >( m_ptrVal.SingleThreadedAccess() );
    }

    /**
     * This method overloads the member access operator.
     *
     * @return       The value of the PtrDelegate smart pointer,
     *               i.e. the address of the object whose lifetime
     *               is being managed by reference counting.
     */

    inline typename PointerTo<w_ClassRef>::value_type * operator-> () const
    {
        return & ( * m_ptrVal.SingleThreadedAccess() );
    }

    /**
     * This method overloads the dereference operator - *( ptr )
     *
     * @return       The object being pointed to.
     */

    inline typename PointerTo<w_ClassRef>::value_type & operator* () const
    {
        return * m_ptrVal.SingleThreadedAccess();
    }

    /**
     * Get() allows access to the managed pointer.  Use of this method is
     * discouraged;  only use this method if interfacing to a legacy library.
     *
     *
     * @return       The value of the PtrDelegate smart pointer,
     *               i.e. the address of the object whose lifetime
     *               is being managed by reference counting.
     */

    inline w_ClassRef Get() const
    {
        return m_ptrVal.SingleThreadedAccess();
    }

    /**
     * This method overloads the conversion to bool operator.
     *
     * @return       A boolean indicating whether (true) or not
     *               (false) the PtrDelegate pointer is non-NULL.
     */

    inline operator bool () const
    {
        return m_ptrVal.SingleThreadedAccess() != 0;
    }

    /**
     * This method overloads operator !.
     *
     * @return       A boolean indicating whether (true) or not
     *               (false) the PtrDelegate pointer is NULL.
     */

    inline bool operator! () const
    {
        return m_ptrVal.SingleThreadedAccess() == 0;
    }

    // operator ==
    /**
     * These methods overload the == operator for the cases where
     * a PtrDelegate is being tested for equality to a PtrDelegate,
     * a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '==' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the two pointers on either
     *                side of the '==' sign are equal, i.e.
     *                point to the same object.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator !=
    /**
     * These methods overload the != operator for the cases
     * where a PtrDelegate is being tested for inequality
     * to a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '!=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the two pointers on either
     *                side of the '!=' sign are unequal, i.e.
     *                point to different objects.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator <
    /**
     * These methods overload the < operator for the cases
     * where a PtrDelegate is being tested to see if it is
     * less than a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '<' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is less
     *                than the right-hand pointer, i.e. whether
     *                the address the left-hand managed pointer
     *                points to is less than the address the
     *                right-hand managed pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator >
    /**
     * These methods overload the > operator for the cases
     * where a PtrDelegate is being tested to see if it is
     * greater than a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '>' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is
     *                greater than the right-hand pointer,
     *                i.e. whether the address the lefthand
     *                managed pointer points to is greater
     *                than the address the right-hand managed
     *                pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator <=
    /**
     * These methods overload the <= operator for the cases
     * where a PtrDelegate is being tested to see if it is
     * less than or equal to a PtrDelegate, an Ptr,
     * or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '<=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is
     *                less than or equal to the right-hand
     *                pointer, i.e. whether the address the
     *                left-hand managed pointer points to is
     *                less than or equal to the address the
     *                right-hand managed pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator >=
    /**
     * These methods overload the >= operator for the cases
     * where a PtrDelegate is being tested to see if it is
     * greater than or equal to a PtrDelegate, a Ptr
     * or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '>=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is
     *                greater than or equal to the right-hand
     *                pointer, i.e. whether the address the
     *                left-hand managed pointer points to is
     *                greater than or equal to the address the
     *                righthand managed pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }
};


}


namespace std
{

    template< typename w_ClassRef, class w_RefTraits >
    inline void swap(
        at::Ptr< w_ClassRef, w_RefTraits > & io_first,
        at::Ptr< w_ClassRef, w_RefTraits > & io_second
    );

}


namespace at
{


// ======== Ptr ================================================
/**
 * Ptr is a member of the set of co-operative "smart" pointers
 * Ptr, PtrDelegate and PtrView.  PtrDelegate and PtrView
 * are intended to be used to specify pointer passing policy while
 * Ptr is intended to be instantiated as a member or automatic
 * variable to maintain the reference to the object being managed.
 *
 * The Ptr smart pointer is designed to make it easier to
 * work with an object whose lifetime is managed by means of
 * reference counting.
 *
 * With an ordinary pointer, you must remember to increment a reference
 * count when you start using the object, and decrement a reference
 * count when you are done with the object. (e.g. implemented with methods
 * AddRef() and Release() as in COM or add() and dec() as with alternative
 * implemenatations).
 *
 * The problem is that it is very easy to forget to do an AddRef()
 * or a Release(), especially when errors occur.
 *
 * A Ptr smart pointer solves this problem, by doing the
 * AddRef() and Release() for you automatically.
 *
 * How is this accomplished?  Very simply:  When a Ptr
 * pointer is copied or assigned to another one, the constructor
 * or overloaded assignment operator automatically does an AddRef().
 * Similarly, when a Ptr pointer is deleted or goes out of
 * scope, its destructor automatically does a Release().
 *
 * @see at_lifetime.h
 * @see PtrDelegate
 * @see PtrView
 *
 */

template < typename w_ClassRef, typename w_RefTraits >
class Ptr
{
    AT_Friend_PtrView
    AT_Friend_Ptrs
    AT_Friend_PtrDelegate

    friend void std::swap< w_ClassRef, w_RefTraits >(
        Ptr< w_ClassRef, w_RefTraits > &,
        Ptr< w_ClassRef, w_RefTraits > &
    );

    /**
     * m_ptrVal contains the actual managed pointer value,
     * i.e. the address of the object whose lifetime is
     * being managed by reference counting.
     */

    mutable typename w_RefTraits::t_ClassRefWrapper m_ptrVal;

private:

    /**
     * ReleasePointee releases the existing managed pointer, i.e.
     * decrements its reference count, and replaces it with the
     * one being passed in i_ptrVal.
     *
     * @param  i_ptrVal  A regular pointer to the replacement
     * @return           A regular pointer to the replacement
     */

    inline void ReleasePointee( w_ClassRef i_ptrVal )
    {
        //
        // The order in which this happens is critical.
        // The new pointer must be in place before decrementing
        // the reference count.
        // 
        
        // Save a pointer to the old managed pointer.
        w_ClassRef l_ptrVal = m_ptrVal.SwapPtr( i_ptrVal );

        // Decrement the old managed pointer's reference count.
        if ( l_ptrVal ) {
            w_RefTraits::DecRefCount( l_ptrVal );
        }

    }

public:


    /**
     * Default contructor for a Ptr.  This sets the
     * managed pointer to null.
     *
     */

    inline Ptr()
      : m_ptrVal()
    {
    }

    /**
     * This method contructs a Ptr from a regular pointer.
     *
     * @param  i_ptr  The regular pointer to be copied (optional).
     */

    inline Ptr( w_ClassRef i_ptr )
      : m_ptrVal( w_RefTraits::FixRawPtrRefCount( i_ptr ) )
    {
    }

    /**
     * Constructs a Ptr smart pointer from
     * a regular pointer, optionally taking ownership of the managed pointer
     * (by bumping his reference count).
     *
     * @param  i_ptr            The regular pointer to be copied.
     *
     * @param  i_takeOwnership  A flag indicating whether or not
     *                          to take ownership of the managed pointer.
     */

    inline Ptr( w_ClassRef i_ptr, bool i_takeOwnership )
      : m_ptrVal( i_ptr )
    {
        if ( i_takeOwnership ) {
            if ( m_ptrVal ) {
                w_RefTraits::IncRefCount( m_ptrVal );
            }
        }
    }

    /**
     * Constructs a Ptr from another
     * Ptr which may be based on a different underlying
     * pointer type.
     *
     * @param  i_ptr  A reference to the Ptr being
     *                copied.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline Ptr(
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() )
    {
    }

    /**
     * This method provides an explicit copy constructor for
     * Ptr.  Apparently, without such a constructor,
     * the compiler is unable (or unwilling) to use the above
     * templated constructor, and instead generates its own
     * code that does the wrong thing.
     *
     * @param  i_ptr  A reference to the Ptr pointer
     *                being copied.
     */

    inline Ptr(
        const Ptr & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() )
    {
    }

    /**
     * Contructs a Ptr from a PtrView.
     *
     * @param  i_ptr  A reference to the PtrView being copied.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline Ptr(
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() )
    {
    }

    /**
     * Contructs a Ptr from a PtrDelegate.
     *
     * @param  i_ptr  The PtrDelegate smart pointer being copied.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline Ptr(
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.Transfer() )
    {
    }

    /**
     * The Ptr destructor must make sure that the reference
     * count of the pointer that is being managed is decremented.
     */

    inline ~Ptr()
    {
        w_ClassRef  i_ptrVal = m_ptrVal.SafeAccess();
        // Releases the managed pointer
        if ( i_ptrVal ) {
            m_ptrVal.PurgePointer();
            w_RefTraits::DecRefCount( i_ptrVal );
        }
    }

    /**
     * Ptr ( regular pointer ) assignment
     * This method overloads the assignment operator for the
     * case where a Ptr is being set equal to a regular
     * pointer.
     *
     * @param  i_ptr  The regular pointer on the right-hand side
     *                of the '=' sign.
     *
     * @return    A reference to the Ptr pointer
     *                on the left-hand side of the '=' sign.
     */

    inline Ptr & operator= (
        w_ClassRef              i_ptr
    )
    {
        // Release the old managed pointer, and adopt the new one.
        ReleasePointee( w_RefTraits::FixRawPtrRefCount( i_ptr ) );

        // Return a reference to the Ptr pointer on
        // the left-hand side of the '=' sign, so that assign-
        // ment operations can be chained together.
        return *this;
    }

    /**
     * Assignment operator for the
     * case where a Ptr is being set equal to another
     * Ptr which may be based on a different underlying
     * pointer type.
     *
     * @param  i_ptr  A reference to the Ptr pointer
     *                on the right-hand side of the '=' sign.
     *
     * @return        A reference to the Ptr pointer
     *                on the left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >

    inline Ptr & operator=(
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        // this will increment the reference count
        // of the pointer passed in and replace the
        // pointer currently being managed by this.
        ReleasePointee( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() );

        // Return a reference to the Ptr pointer on
        // the left-hand side of the '=' sign, so that assign-
        // ment operations can be chained together.
        return *this;
    }

    /**
     * Copy assignment.
     *
     * If such a method is not provided, the compiler
     * is unable to use the above template version
     * of overloaded assignment, and instead generates its own
     * code that does the wrong thing.
     *
     * @param  i_ptr  A reference to the Ptr pointer on
     *                the right-hand side of the '=' sign.
     *
     * @return        A reference to the Ptr pointer on
     *                the left-hand side of the '=' sign.
     */

    inline Ptr & operator= (
        const Ptr & i_ptr
    )
    {
        // this will increment the reference count
        // of the pointer passed in and replace the
        // pointer currently being managed by this.
        ReleasePointee( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() );
        
        return *this;
    }

    /**
     * This method overloads the assignment operator for the
     * case where a Ptr is being set equal to a PtrView.
     *
     * @param  i_ptr  A reference to the PtrView pointer
     *                on the right-hand side of the '=' sign.
     *
     * @return        A reference to the Ptr pointer
     *                on the left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline Ptr & operator=(
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        // this will increment the reference count
        // of the pointer passed in and replace the
        // pointer currently being managed by this.
        
        ReleasePointee( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() );
        
        // Return a reference to the Ptr pointer on
        // the left-hand side of the '=' sign, so that assign-
        // ment operations can be chained together.
        return *this;
    }

    /**
     * This method overloads the assignment operator for the
     * case where a PtrDelegate is being assigned to a
     * Ptr.  This will set the PtrDelegate
     * passed in to null.  The concept is that the job of a
     * PtrDelegate is to describe the policy of passing and
     * returning pointers hence once the pointer is placed in
     * its final resting place, the PtrDelegate is no longer
     * needed (and should not be used).
     *
     * @param  i_ptr  A reference to the PtrDelegate pointer
     *                on the right-hand side of the '=' sign.
     *
     * @return        A reference to the Ptr pointer
     *                on the left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline Ptr & operator=(
        PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        // Transfer the PtrDelegate to this.  The optimization
        // for passing PtrDelegate is that the 
        
        ReleasePointee( i_ptr.Transfer() );

        // Return a reference to the Ptr pointer on
        // the left-hand side of the '=' sign, so that assign-
        // ment operations can be chained together.
        return *this;
    }


    // ======== SwapPtrCompare ========================================
    /**
     * SwapPtrCompare will swap 2 pointers atomically.  This will
     * only work if this specialization's t_ClassRefWrapper class
     * supports the swap compare feature.  The atomicity of this
     * operation is within THIS class, not to the passed in
     * i_ptr value.
     *
     * @param i_ptr  The smart pointer containing the value to
     *               replace the pointer in this class.
     * @param i_compare The value of the pointer in the class.
     * @return nothing
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    PtrView< w_ClassRef, w_RefTraitsRhs > SwapPtrCompare(
        Ptr< w_ClassRefRhs, w_RefTraitsRhs >    & i_ptr,
        w_ClassRef                                i_compare
    ) {

        w_ClassRef l_result = m_ptrVal.SwapPtrCompare(
            i_ptr.m_ptrVal.SingleThreadedAccess(),
            i_compare
        );

        if ( l_result == i_compare )
        {
            i_ptr.m_ptrVal.SwapPtr( l_result );
        }

        return l_result;
    }
    
    /**
     * Operator -> allows the access to the managed pointer
     * -> operator.
     *
     * @return       The address of the managed pointer.
     */

    inline typename PointerTo<w_ClassRef>::value_type * operator-> () const
    {
        return & ( * m_ptrVal.SingleThreadedAccess() );
    }

    /**
     * This method overloads the dereference operator - *( ptr )
     *
     * @return       The object being pointed to.
     */

    inline typename PointerTo<w_ClassRef>::value_type & operator* () const
    {
        return * m_ptrVal.SingleThreadedAccess();
    }

    /**
     * InnerReference() releases the managed pointer, and returns the
     * address of the private data member that is used to
     * store the address of the managed pointer.  This is intended for
     * use in legacy API's that require the address of a pointer
     * that will get filled in.  Use of this pointer for other
     * circumstances is discouraged.
     *
     * @return       The address of the private data member
     *               used to store the address of the managed pointer.
     */

    inline w_ClassRef * InnerReference()
    {
        // Release the managed pointer.
        ReleasePointee( 0 );

        // Return the address of the private data member used
        // to store the address of the managed pointer.
        return & m_ptrVal.SingleThreadedAccess();
    }

    /**
     * Get() allows access to the inner pointer value.
     *
     * @return       The address the inner pointer points to.
     */

    inline w_ClassRef Get() const
    {
        return m_ptrVal.SingleThreadedAccess();
    }

    /**
     * This defines the bool conversion.
     *
     * @return       A flag indicating whether (true) or not
     *               (false) the managed pointer points to a
     *               non-NULL address.
     */

    inline operator bool () const
    {
        return m_ptrVal.SingleThreadedAccess() != 0;
    }

    /**
     * Reset() will increment the reference count of
     * the pointer passed into \a i_ptr and will also 
     * replace the managed pointer in this with \a i_ptr.
     *
     * @param  i_ptr  The address of the new managed pointer.
     * @return        The value of \a i_ptr
     */

    inline w_ClassRef Reset( const w_ClassRef i_ptr )
    {
        // Add a reference to the regular pointer's managed pointer.
        if ( i_ptr ) {
            w_RefTraits::IncRefCount( i_ptr );
        }

        // Set this Ptr equal to the regular pointer.
        ReleasePointee( i_ptr );

        return i_ptr;
    }

    /**
     * This helper method increments the reference count
     * of the object managed by the PtrDelegate parameter.
     * This is used in the rare case where the PtrDelegate
     * smart pointer is required later in execution.
     *
     * @param  i_ptr  A reference to the PtrDelegate pointer
     *                whose managed pointer is to be "adopted".
     *
     * @return        The address of the "adopted" managed pointer.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline Ptr & Reset(
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        ReleasePointee( i_ptr.m_ptrVal.template IncRefCountTest< w_ClassRef, w_RefTraits >() );
        
        return * this;
    }

    /**
     * This helper method returns the pointer being managed by this
     * and sets this managed pointer to null.  This effectively
     * passes back to the caller the responsibility of decrementing
     * the reference count.
     *
     * @return       The address of the managed pointer.
     */

    inline w_ClassRef Transfer()
    {
        return m_ptrVal.SwapPtr( 0 );
    }

    /**
     * This helper method returns a PtrView version of
     * this Ptr pointer.
     *
     * @return       A PtrView version of this Ptr
     *              pointer.
     */

    inline PtrView< w_ClassRef, w_RefTraits > LifeView() const
    {
        return PtrView< w_ClassRef, w_RefTraits >( m_ptrVal.SingleThreadedAccess() );
    }

    // operator !
    /**
     * This method overloads operator !.
     *
     * @return       A boolean indicating whether (true) or
     *               not (false) this Ptr pointer
     *               is zero.
     */

    inline bool operator ! () const
    {
        return m_ptrVal.SingleThreadedAccess() == 0;
    }

    // operator ==
    /**
     * These methods overload the '==' operator for the cases
     * where a Ptr is being tested for equality to a
     * PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '==' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the two pointers on either
     *                side of the '==' sign are equal, i.e.
     *                point to the same object.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator !=
    /**
     * These methods overload the '!=' operator for the cases
     * where a Ptr is being tested for inequality to
     * a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '!=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the two pointers on either
     *                side of the '!=' sign are unequal, i.e.
     *                point to different objects.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator <
    /**
     * These methods overload the < operator for the cases
     * where a Ptr is being tested to see if it is
     * less than a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '<' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is less
     *                than the right-hand pointer, i.e. whether
     *                the address the left-hand managed pointer
     *                points to is less than the address the
     *                right-hand managed pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /**
     * These methods overload the > operator for the cases
     * where a Ptr is being tested to see if it is
     * greater than a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '>' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is
     *                greater than the right-hand pointer,
     *                i.e. whether the address the left-hand
     *                managed pointer points to is greater
     *                than the address the right-hand managed
     *                pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator<=
    /**
     * These methods overload the <= operator for the cases
     * where a Ptr is being tested to see if it is
     * less than or equal to a PtrDelegate, a Ptr,
     * or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '<=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is less
     *                than or equal to the right-hand pointer,
     *                i.e. whether the address the left-hand
     *                managed pointer points to is less than or
     *                equal to the address the right-hand
     *                managed pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator>=
    /**
     * These methods overload the >= operator for the cases
     * where a Ptr is being tested to see if it is
     * greater than or equal to a PtrDelegate, a Ptr,
     * or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '>=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is
     *                greater than or equal to the righthand
     *                pointer, i.e. whether the address the
     *                left-hand managed pointer points to is
     *                greater than or equal to the address the
     *                right-hand managed pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }
};


}


namespace std
{

    // ======== swap ===============================================
    /**
     *  Specialization of std::swap for Ptr objects.  By-passes the
     *  intermediate ref count adjustments that would occur with the
     *  default implementation (as swapping two Ptr objects leaves
     *  both referenced objects with ref counts unchanged).
     *
     *  @param io_first One of the Ptr objects to swap.
     *  @param io_second The other Ptr object to swap.
     *  @return nothing.
     */
    template< typename w_ClassRef, class w_RefTraits >
    inline void swap(
        at::Ptr< w_ClassRef, w_RefTraits > & io_first,
        at::Ptr< w_ClassRef, w_RefTraits > & io_second
    )
    {
        at::PtrDelegate< w_ClassRef, w_RefTraits >  l_val( io_first );
        io_first = io_second;
        io_second = l_val;
    }

}


namespace at
{


// ======== PtrView ================================================
/**
 *
 * PtrView is a member of the set of co-operative "smart" pointers
 * Ptr, PtrDelegate and PtrView.  PtrDelegate and PtrView
 * are intended to be used to specify pointer passing policy while
 * Ptr is intended to be instantiated as a member or automatic
 * variable to maintain the reference to the object being managed.
 *
 * The PtrView smart pointer is intended to specify a policy
 * of passing a pointer without any obligation to decrement the
 * reference count.  If your pointer is currently stored within
 * a Ptr and will remain available for the life of the call
 * (or the return) you may safely pass a PtrView.  This will
 * eliminate any unnecessary increments and decrements of the
 * reference count and hence is the preferred mechanism for passing
 * pointers.
 *
 * A Life-"View" is intended to indicate that a "view" into the pointer
 * is being transferred with no other obligation on the recepient.
 *
 * The policy demands however that if the recipient requires the object
 * being managed to persist after the recipient returns, it must
 * increment the reference count.  This can be done by using the PtrView
 * object to initialize a Ptr object.
 *
 * Hence assignment of a PtrView to a Ptr or PtrDelegate
 * will increment the reference count while assignment of a Ptr
 * or a PtrDelegate to a PtrView will not increment the reference
 * count.
 *
 * @see at_lifetime.h
 * @see PtrDelegate
 * @see Ptr
 *
 */

template < typename w_ClassRef, typename w_RefTraits >

class PtrView
{
    AT_Friend_PtrView
    AT_Friend_Ptrs
    AT_Friend_PtrDelegate

    /**
     * m_ptrVal contains the managed pointer value,
     * i.e. the address of the object whose lifetime is
     * being managed by reference counting.
     */

    mutable typename w_RefTraits::t_ClassRefWrapper m_ptrVal;

public:

    /**
     * Construct a PtrView smart pointer
     * from a regular pointer.
     *
     * @param  i_ptr  The regular pointer (optional).
     */

    inline PtrView( w_ClassRef i_ptr = 0 )
      : m_ptrVal( i_ptr )
    {
    }

    /**
     * Constructs a PtrView from another
     * PtrView which may be based on a different
     * managed pointer type.
     *
     * @param  i_ptr  A reference to the PtrView from which
     *                this one is being constructed.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrView(
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.SingleThreadedAccess() )
    {
    }

    /**
     * Copy constructor for PtrView.
     * Apparently, without such a constructor,
     * the compiler is unable (or unwilling)
     * to use the above templated constructor, and instead
     * generates its own code that does the wrong thing.
     *
     * @param  i_ptr  A reference to the PtrView pointer
     *                being copied.
     */

    inline PtrView(
        const PtrView & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.SingleThreadedAccess() )
    {
    }

    /**
     * Constructs a PtrView from a Ptr.
     *
     * @param  i_ptr  A reference to the Ptr that
     *                is to be copied.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrView(
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr )
      : m_ptrVal( i_ptr.m_ptrVal.SingleThreadedAccess() )
    {
    }

    /**
     * Constructs a PtrView from a PtrDelegate.
     *
     * @param  i_ptr  A reference to the PtrDelegate that
     *                is to be copied.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrView(
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
      : m_ptrVal( i_ptr.m_ptrVal.SingleThreadedAccess() )
    {
    }

    /**
     * Assignment operator for the
     * case where a PtrView is being set equal to a regular
     * pointer.
     *
     * @param  i_ptr  The regular pointer on the right-hand
     *                side of the '=' sign.
     *
     * @return        A reference to the PtrView pointer
     *                on the left-hand side of the '=' sign.
     */

    inline PtrView< w_ClassRef, w_RefTraits > & operator= (
        const w_ClassRef i_ptr
    )
    {
        m_ptrVal.SingleThreadedAccess() = i_ptr;
        return *this;
    }

    /**
     * Assignment operator for the
     * case where a PtrView is being set equal to another
     * PtrView.
     *
     * @param  i_ptr  The PtrView on the right-hand side
     *                of the '=' sign.
     *
     * @return        A reference to the PtrView on the
     *                left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrView & operator= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        m_ptrVal.SingleThreadedAccess() = i_ptr.m_ptrVal.SingleThreadedAccess();
        return *this;
    }

    /**
     * Copy assignment.
     *
     * @param  i_ptr  The PtrView on the right-hand side
     *                of the '=' sign.
     *
     * @return        A reference to the PtrView on the
     *                left-hand side of the '=' sign.
     */

    inline PtrView & operator= (
        const PtrView & i_ptr
    )
    {
        m_ptrVal.SingleThreadedAccess() = i_ptr.m_ptrVal.SingleThreadedAccess();
        return *this;
    }

    /**
     * Assignment operator for the
     * case where a Ptr is being assigned to a
     * PtrView.
     *
     * @param  i_ptr  A reference to the Ptr on the
     *                right-hand side of the '=' sign.
     *
     * @return        A reference to the PtrView on the
     *                left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrView & operator= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        m_ptrVal.SingleThreadedAccess() = i_ptr.m_ptrVal.SingleThreadedAccess();
        return *this;
    }

    /**
     * This method overloads the assignment operator for the
     * case where a PtrView is being set equal to a
     * PtrDelegate.
     *
     * @param  i_ptr  The PtrDelegate on the right-hand side
     *                of the '=' sign.
     *
     * @return        A reference to the PtrView on the
     *                left-hand side of the '=' sign.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline PtrView & operator= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        m_ptrVal.SingleThreadedAccess() = i_ptr.m_ptrVal.SingleThreadedAccess();
        return *this;
    }

    /**
     * member access operator, allows use of "->" against the
     * underlying type.
     *
     * @return       The address of the pointed-to object.
     */

    inline typename PointerTo<w_ClassRef>::value_type * operator-> () const
    {
        return & ( * m_ptrVal.SingleThreadedAccess() );
    }

    /**
     * This method overloads the dereference operator - *( ptr )
     *
     * @return       The object being pointed to.
     */

    inline typename PointerTo<w_ClassRef>::value_type & operator* () const
    {
        return * m_ptrVal.SingleThreadedAccess();
    }

    /**
     * Get() allows access to the inner pointer.
     *
     * @return       The address of the pointed-to object.
     */

    inline w_ClassRef Get() const
    {
        return m_ptrVal.SingleThreadedAccess();
    }

    /**
     * This method returns a PtrView version of this
     * pointer.
     *
     * @return       A PtrView version of this pointer.
     */

    inline PtrView< w_ClassRef > LifeView() const
    {
        return *this;
    }

    /**
     * Conversion from PtrView to bool returns true if
     * the managed pointer is not null.
     *
     * @return       A boolean indicating whether (true) or not
     *               (false) the managed pointer's address is non-zero.
     */

    inline operator bool() const
    {
        return m_ptrVal.SingleThreadedAccess() != 0;
    }

    /**
     * This method overloads the ! operator - returns true if
     * managed pointer is null.
     *
     * @return       A boolean indicating whether (true) or not
     *               (false) the managed pointer's address is zero.
     */
    inline bool operator! () const
    {
        return m_ptrVal.SingleThreadedAccess() == 0;
    }

    // operator ==
    /**
     * These methods overload the '==' operator for the cases
     * where a PtrView is being tested for equality to a
     * PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '==' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the two pointers on either
     *                side of the '==' sign are equal, i.e.
     *                point to the same object.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator== (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() == i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator !=
    /**
     * These methods overload the '!=' operator for the cases
     * where a PtrView is being tested for inequality to
     * a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '!=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the two pointers on either
     *                side of the '!=' sign are unequal, i.e.
     *                point to different objects.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator!= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() != i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator <
    /**
     * These methods overload the < operator for the cases
     * where a PtrView is being tested to see if it is
     * less than a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '<' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is less
     *                than the right-hand pointer, i.e. whether
     *                the address the left-hand managed pointer
     *                points to is less than the address the
     *                right-hand managed pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator< (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() < i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator >
    /**
     * These methods overload the > operator for the cases
     * where a PtrView is being tested to see if it is
     * greater than a PtrDelegate, a Ptr, or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '>' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is
     *                greater than the right-hand pointer,
     *                i.e. whether the address the lefthand
     *                managed pointer points to is greater
     *                than the address the right-hand managed
     *                pointer points to.
     */

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator> (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() > i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    // operator <=
    /**
     * These methods overload the <= operator for the cases
     * where a PtrView is being tested to see if it is
     * less than or equal to a PtrDelegate, a Ptr,
     * or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '<=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is less
     *                than or equal to the right-hand pointer,
     *                i.e. whether the address the left-hand
     *                managed pointer points to is less than or
     *                equal to the address the right-hand
     *                managed pointer points to.
     */
    //@{

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs> & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator<= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() <= i_ptr.m_ptrVal.SingleThreadedAccess();
    }
    //@}

    // operator >=
    /**
     * These methods overload the >= operator for the cases
     * where a PtrView is being tested to see if it is
     * greater than or equal to a PtrDelegate, a Ptr,
     * or a PtrView.
     *
     * @param  i_ptr  A reference to the smart pointer on the
     *                right-hand side of the '>=' operator.
     *
     * @return        A boolean indicating whether (true) or
     *                not (false) the left-hand pointer is
     *                greater than or equal to the right-
     *                hand pointer, i.e. whether the address
     *                the left-hand managed pointer points to
     *                is greater than or equal to the address
     *                the right-hand managed pointer points to.
     */
    //@{

    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const PtrDelegate< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const Ptr< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }

    /** \overload */
    template < typename w_ClassRefRhs, typename w_RefTraitsRhs >
    inline bool operator>= (
        const PtrView< w_ClassRefRhs, w_RefTraitsRhs > & i_ptr
    )
    {
        return m_ptrVal.SingleThreadedAccess() >= i_ptr.m_ptrVal.SingleThreadedAccess();
    }
    //@}
};

/** @} */ // end of SmartPointerTrio ( Doxygen group )


// ======== Pointer =================================================
/**
 * The Pointer smart pointer works essentially like the auto_ptr
 * of the Standard Template Library (STL).
 *
 * Unlike the other reference counting smart pointers, a Pointer
 * never adds or releases a reference to its managed object.  Therefore, one should use
 * a Pointer only if the managed object is not capable of reference
 * counting.  Otherwise, use a Ptr.
 */

template < typename w_ClassRef >
class Pointer
{
    public:

        /**
         * m_ptrVal contains the actual managed pointer value,
         * i.e. the address of the managed object.
         */

        mutable w_ClassRef m_ptrVal;

    private:

        /**
         * This method deletes the managed object.
         */

        inline void DeletePointee()
        {
            if ( m_ptrVal ) {
                delete m_ptrVal;
            }
        }

        /**
         * DeletePointee( new regular pointer )
         * ---------------------------------
         * This method deletes the existing managed object, and
         * "adopts" a new one.
         *
         * @param  i_ptrVal  A regular pointer to the new managed object.
         * @return           A regular pointer to the new managed object.
         */
    
        inline w_ClassRef DeletePointee( w_ClassRef i_ptrVal )
        {
            // Save a pointer to the old managed object.
            // ----------------------------------
            w_ClassRef l_ptrVal = m_ptrVal;

            // Point to the new managed object.
            // -------------------------
            m_ptrVal = i_ptrVal;
        
            // Delete the old managed object.
            // -----------------------
            if ( l_ptrVal ) {
                delete l_ptrVal;
            }
        
            // Return a regular pointer to the new managed object.
            // -----------------------------------------
            return i_ptrVal;
        }
    
    public:

        /**
         * Pointer( regular pointer )
         * --------------------------
         * This method constructs a Pointer from a regular
         * pointer.
         *
         * @param  i_ptr  The regular pointer from which this
         *                Pointer is to be constructed.
         *                (If no regular pointer is specified,
         *                then zero is used.)
         */

        inline Pointer( w_ClassRef i_ptr = 0 )
          : m_ptrVal( i_ptr )
        {
        }

        /**
         * Pointer( Pointer )
         * ------------------------
         * Copy constructor.  This method constructs a
         * Pointer from another Pointer.
         *
         * @param  i_ptr  A reference to the Pointer to be
         *                copied.
         */

        inline Pointer(
            const Pointer< w_ClassRef > & i_ptr )

          : m_ptrVal( i_ptr.m_ptrVal )
        {
            // Zero out the managed pointer address in the Pointer
            // being copied, so that it is impossible to delete
            // the managed pointer twice accidentally.
            // ------------------------------------------------
            i_ptr.m_ptrVal = 0;
        }

        /**
         * ~Pointer()
         * -------------
         * Destructor.
         */

        inline ~Pointer()
        {
            DeletePointee();
        }

        /**
         * Pointer = regular pointer
         * -------------------------
         * This method overloads the assignment operator for the
         * case where a Pointer is being set equal to a regular
         * pointer.
         *
         * @param   i_ptr  The regular pointer on the right-hand
         *                 side of the '=' sign.
         *
         * @return         A reference to this Pointer (the
         *                 one on the left-hand side of the
         *                 '=' sign).
         */

        inline Pointer & operator= ( const w_ClassRef i_ptr )
        {
            // Delete the old managed object, and make the
            // Pointer point to the new one.
            // -----------------------------------------------
            DeletePointee( i_ptr );

            // Return a reference to the Pointer on the left-
            // hand side of the '=' sign, so that assignment
            // operations can be chained together.
            // ----------------------------------------------
            return *this;
        }

        /**
         * Pointer = Pointer
         * -----------------------
         * This method overloads the assignment operator for the
         * case where a Pointer is being set equal to another
         * Pointer.
         *
         * @param   i_ptr  A reference to the Pointer on the
         *                 right-hand side of the '=' sign.
         *
         * @return         A reference to this Pointer (the
         *                 one on the left-hand side of the '='
         *                 sign).
         */

        inline Pointer & operator= (
            const Pointer< w_ClassRef > & i_ptr )
        {
            // If the Pointer is being set equal to itself,
            // then just return.
            // ---------------------------------------------
            if ( & m_ptrVal == & i_ptr.m_ptrVal ) {
                return *this;
            }

            // Save the address of the new managed object.
            // ------------------------------------
            w_ClassRef l_ptrVal = i_ptr.m_ptrVal;

            // Zero out the Pointer on the right-hand side of the '='
            // sign, so that the new managed object can't be deleted
            // twice accidentally.
            // ------------------------------------------------
            i_ptr.m_ptrVal = 0;

            // Delete the old managed object, and store the new managed
            // object's address in the Pointer on the left-hand side of
            // the '=' sign.
            // ---------------------------------------------------
            DeletePointee( l_ptrVal );

            // Return a reference to the Pointer on the left-
            // hand side of the '=' sign, so that assignment
            // operations can be chained together.
            // ----------------------------------------------
            return *this;
        }

        /**
         * Pointer->
         * ------------
         * This method overloads the member access operator.
         *
         * @return       The address of the managed object.
         */

        inline typename PointerTo<w_ClassRef>::value_type * operator->() const
        {
            return & ( * m_ptrVal );
        }

        /**
         * This method overloads the dereference operator - *( ptr )
         *
         * @return       The object being pointed to.
         */
    
        inline typename PointerTo<w_ClassRef>::value_type & operator* () const
        {
            return * m_ptrVal;
        }

        /**
         * w_ClassRef( Pointer )
         * ------------------------
         * This method overloads the cast-to-w_ClassRef operator.
         *
         * @return       The address of the managed object.
         */

        inline operator w_ClassRef () const
        {
            return m_ptrVal;
        }

        /**
         * bool( Pointer )
         * ------------------
         * This method overloads the cast-to-bool operator.
         *
         * @return       A boolean indicating whether (true) or
         *               not (false) the managed pointer is
         *               non-zero.
         */

        inline operator bool () const
        {
            return m_ptrVal != 0;
        }

        /**
         * Transfer()
         * ----------
         * This helper method returns the managed object's address,
         * and then zeroes out the Pointer.
         *
         * @return       The managed object's address.
         */

        inline w_ClassRef Transfer()
        {
            w_ClassRef l_ptrVal = m_ptrVal;

            m_ptrVal = 0;
        
            return l_ptrVal;
        }

        // operator !
        /**
         * This method overloads operator !.
         *
         * @return       A boolean indicating whether (true) or
         *               not (false) this Pointer pointer
         *               is zero.
         */

        inline bool operator ! () const
        {
            return m_ptrVal == 0;
        }

};


// ======== IsRefCountOne =============================================
/**
 * IsRefCountOne will return true if the reference count of a
 * reference counted object is 1.
 *
 * @param i_ptr A reference to a Ptr to the reference-counted object
 * @return nothing
 */

template < typename w_ClassRef, typename w_RefTraits >
inline bool IsRefCountOne(
    const Ptr<w_ClassRef,w_RefTraits> & i_ptr
) {

    if ( i_ptr )
    {
        return IsRefCountOne( i_ptr.Get() );
    }
    else
    {
        return false;
    }
}



#undef AT_Friend_Ptrs
#undef AT_Friend_PtrView
#undef AT_Friend_PtrDelegate

}; // namespace
#endif // x_at_lifetime_h_x

