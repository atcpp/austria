/**
 * at_http_lexer.cpp is the basic HTTP parser functionality.
 *
 */

#include "at_os.h"
#include "at_http_response.h"
#include "at_http_request.h"
#include "at_http_lexer.h"

namespace at
{

//
// Define the "standard" keys"
//
const std::string HttpLexerTraits::s_HTTPVersionStrKey = ":HTTPVersionStr";
const std::string HttpRequestLexer::s_MethodKey = ":Method";
const std::string HttpRequestLexer::s_RequestURIKey = ":RequestURI";
const std::string HttpResponseLexer::s_StatusCodeKey = ":StatusCode";
const std::string HttpResponseLexer::s_StatusTextKey = ":StatusText";


// ======== HttpLexerBase =============================================
/**
 * This provides basic functions for the base lexer
 *
 */

template <
    typename w_lexer_info
>
class HttpLexerBase
  : public w_lexer_info
{
    public:

    typedef PtrView< Buffer * >   BufferType;
    typedef void                  ParseReturn;
    typedef Buffer::t_BaseType    ElementType;
    typedef SizeMem               SizeType;
    typedef const char          & ElementLocal;
    typedef const ElementType   * IteratorType;

    typedef typename w_lexer_info::OutputValues t_OutputValues;
    typedef typename w_lexer_info::ParserState  t_ParserState;
    typedef typename w_lexer_info::Acceptor     t_Acceptor;

    
    static IteratorType GetData( BufferType i_buffer )
    {
        return i_buffer->Region().m_mem;
    }

    static SizeType GetSize( BufferType i_buffer )
    {
        return i_buffer->Region().m_allocated;
    }
    
    static const ElementLocal GetNext( const ElementType * & io_val, SizeType & o_len )
    {
        -- o_len;
        return * ( io_val ++ );
    }
    
    static unsigned char GetValue( ElementLocal l_val )
    {
        return static_cast<unsigned char>( l_val );
    }
    
    virtual void Collect( t_OutputValues, ElementLocal l_val ) = 0;
    
    virtual void StartCollect( t_OutputValues, IteratorType i_val ) = 0;
    
    virtual void EndCollect( t_OutputValues i_ov, IteratorType i_val ) = 0;
    
    static void TransitionState( t_ParserState, t_ParserState i_next )
    {
    }
    
    virtual void InvalidInput( BufferType i_buffer, const ElementType * l_pos ) = 0;
    
    static void Consume( BufferType i_buffer, SizeType i_size )
    {
        i_buffer->Erase( 0, i_size );
    }
    
    virtual void ParsingFromError() = 0;
    
    virtual void NothingMoreToScan() = 0;
    
    virtual void Accept( t_Acceptor i_val ) = 0;
    
    virtual void PrematureEnd() = 0;
    
    virtual void BaseReset() = 0;
    
};


// ======== HttpLexer_Basic ===========================================
/**
 * HttpLexer_Basic contains the basic lexer and map stuff.
 *
 */

template <
    typename w_lexer_info
>
class HttpLexer_Basic
  : public w_lexer_info
{
    public:

    typedef typename w_lexer_info::IteratorType t_IteratorType;
    typedef typename w_lexer_info::BufferType   t_BufferType;
    typedef typename w_lexer_info::ElementType  t_ElementType;
    typedef typename w_lexer_info::ElementLocal t_ElementLocal;
    typedef typename w_lexer_info::OutputValues t_OutputValues;
    typedef typename w_lexer_info::ParserState  t_ParserState;
    typedef typename w_lexer_info::Acceptor     t_Acceptor;

    /**
     * HttpLexer_Basic
     *
     */
    HttpLexer_Basic(
        HttpLexerTraits::t_MapType      * o_map
    )
      : m_map( o_map ),
        m_has_accepted(),
        m_has_premature_end(),
        m_has_error()
    {
        AT_Assert( o_map );
    }

    virtual ~HttpLexer_Basic() {}

    /**
     * m_map contains the location where parsed data is placed
     */
    HttpLexerTraits::t_MapType          * m_map;

    /**
     * m_str contains contents of the string currently being parsed.
     */
    std::string                           m_str;

    /**
     * m_field_name is used to temporarily hold the field name
     * while the value is being parsed
     */
    std::string                           m_field_name;

    /**
     * m_has_accepted indicates that the request has completed
     * successfully.
     */
    bool                    m_has_accepted;
    
    /**
     * m_has_premature_end indicates that not enough data is available.
     * successfully.
     */
    bool                    m_has_premature_end;
    bool                    m_has_error;

    
    virtual void BaseReset()
    {
        m_map->clear();
        m_str = "";
        m_has_accepted = false;
        m_has_premature_end = false;
        m_has_error = false;
    }

    virtual void Collect( t_OutputValues i_ov, t_ElementLocal l_val )
    {
        // Nuke leading white space on v_FieldValue
        //
        if ( ( l_val == ' ' ) && ( i_ov == w_lexer_info::v_FieldValue ) && ! m_str.size() )
        {
            return;
        }
        m_str.push_back( l_val );
    }
    
    virtual void StartCollect( t_OutputValues i_ov, t_IteratorType i_val )
    {
    }

    virtual void InvalidInput( t_BufferType i_buffer, const t_ElementType * l_pos )
    {
        m_has_error = true;
    }
    
    virtual void ParsingFromError()
    {
        m_has_error = true;
    }
    
    virtual void NothingMoreToScan()
    {
    }
    
    virtual void Accept( t_Acceptor i_val )
    {
        m_has_accepted = true;
    }
    
    virtual void PrematureEnd()
    {
        m_has_premature_end = true;
    }
    
};


// ======== HttpRequestLexer_Basic ====================================
/**
 * HttpRequestLexer_Basic is the private implementation of the lexical
 * scanner for HTTP requests
 */

typedef HttpLexer_Basic<
    at_http::HttpRequest_Lexer<
        HttpLexerBase<
            at_http::HttpRequest_LexerInfo
        >
    >
>   HttpRequestLexer_Base;

class AUSTRIA_EXPORT HttpRequestLexer_Basic
  : public HttpRequestLexer_Base
{
    public:

    /**
     * HttpRequestLexer_Basic
     *
     */
    HttpRequestLexer_Basic(
        HttpRequestLexer::t_MapType     * o_map
    )
      : HttpRequestLexer_Base( o_map )
    {
    }

    virtual ~HttpRequestLexer_Basic() {}

    virtual void EndCollect( OutputValues i_ov, IteratorType i_val )
    {
        switch ( i_ov )
        {
            case NullOutputValue : {
                AT_Abort(); // this should never happen
                break;
            }
            case v_FieldName : {
                m_field_name = m_str;
                break;
            }
            case v_Method : {
                ( * m_map )[ HttpRequestLexer::s_MethodKey ] = m_str;
                break;
            }
            case v_FieldValue : {
                std::size_t l_size = m_str.size();

                // Trim the trailing CRLF
                if ( l_size > 0 )
                {
                    -- l_size;
                    if ( m_str[ l_size ] == '\n' )
                    {
                        if ( l_size > 0 && m_str[ l_size -1 ] == '\r' )
                        {
                            m_str.resize( l_size -1 );
                        }
                        else
                        {
                            m_str.resize( l_size );
                        }
                    }
                }
                    
                ( * m_map )[ m_field_name ] = m_str;
                break;
            }
            case v_HTTPVersionStr : {
                ( * m_map )[ HttpRequestLexer::s_HTTPVersionStrKey ] = m_str;
                break;
            }
            case v_RequestURI : {
                ( * m_map )[ HttpRequestLexer::s_RequestURIKey ] = m_str;
                break;
            }
        }

        m_str.erase();
    }

};


// ======== HttpRequestLexer ======================================
HttpRequestLexer::HttpRequestLexer(
    HttpRequestLexer::t_MapType * o_map
)
  : m_lexer( new HttpRequestLexer_Basic( o_map ) )
{
}


// ======== ParseSome =============================================
HttpLexerTraits::ParseResult HttpRequestLexer::ParseSome(
    PtrView< Buffer * >               i_buffer,
    bool                              i_is_last
) {
    m_lexer->ParseChunk( i_buffer, i_is_last );

    if ( m_lexer->m_has_accepted )
    {
        return Success;
    }
    
    if ( m_lexer->m_has_error )
    {
        return Invalid;
    }
    
    return Incomplete;
}

    

// ======== Parse =================================================
HttpLexerTraits::ParseResult HttpRequestLexer::Parse(
    HttpRequestLexer::t_MapType     * o_map,
    PtrView< Buffer * >               i_buffer
) {

    AT_Assert( o_map );

    HttpRequestLexer            l_req_lexer( o_map );

    return l_req_lexer.ParseSome( i_buffer, true );

}

// ============= HttpRequestLexer::~HttpRequestLexer ==============
HttpRequestLexer::~HttpRequestLexer()
{
    delete m_lexer;
}


// ======== HttpResponseLexer_Basic ====================================
/**
 * HttpResponseLexer_Basic is the private implementation of the lexical
 * scanner for HTTP requests
 */

typedef HttpLexer_Basic<
    at_http::HttpResponse_Lexer<
        HttpLexerBase<
            at_http::HttpResponse_LexerInfo
        >
    >
>   HttpResponseLexer_Base;

class AUSTRIA_EXPORT HttpResponseLexer_Basic
  : public HttpResponseLexer_Base
{
    public:

    /**
     * HttpResponseLexer_Basic
     *
     */
    HttpResponseLexer_Basic(
        HttpResponseLexer::t_MapType     * o_map
    )
      : HttpResponseLexer_Base( o_map )
    {
    }

    virtual void EndCollect( OutputValues i_ov, IteratorType i_val )
    {
        switch ( i_ov )
        {
            case NullOutputValue : {
                AT_Abort(); // this should never happen
                break;
            }
            case v_FieldName : {
                m_field_name = m_str;
                break;
            }
            case v_FieldValue : {
                std::size_t l_size = m_str.size();

                // Trim the trailing CRLF
                if ( l_size > 0 )
                {
                    -- l_size;
                    if ( m_str[ l_size ] == '\n' )
                    {
                        if ( l_size > 0 && m_str[ l_size -1 ] == '\r' )
                        {
                            m_str.resize( l_size -1 );
                        }
                        else
                        {
                            m_str.resize( l_size );
                        }
                    }
                }
                    
                ( * m_map )[ m_field_name ] = m_str;
                break;
            }
            case v_HTTPVersionStr : {
                ( * m_map )[ HttpResponseLexer::s_HTTPVersionStrKey ] = m_str;
                break;
            }
            case v_StatusCode : {
                ( * m_map )[ HttpResponseLexer::s_StatusCodeKey ] = m_str;
                break;
            }
            case v_StatusText : {
                ( * m_map )[ HttpResponseLexer::s_StatusTextKey ] = m_str;
                break;
            }
            
        }

        m_str.erase();
    }

};

// ======== HttpResponseLexer ======================================
HttpResponseLexer::HttpResponseLexer(
    HttpResponseLexer::t_MapType * o_map
)
  : m_lexer( new HttpResponseLexer_Basic( o_map ) )
{
}


// ======== ParseSome =============================================
HttpLexerTraits::ParseResult HttpResponseLexer::ParseSome(
    PtrView< Buffer * >               i_buffer,
    bool                              i_is_last
) {
    m_lexer->ParseChunk( i_buffer, i_is_last );

    if ( m_lexer->m_has_accepted )
    {
        return Success;
    }
    
    if ( m_lexer->m_has_error )
    {
        return Invalid;
    }
    
    return Incomplete;
}

    

// ======== Parse =================================================
HttpLexerTraits::ParseResult HttpResponseLexer::Parse(
    HttpResponseLexer::t_MapType     * o_map,
    PtrView< Buffer * >               i_buffer
) {

    AT_Assert( o_map );

    HttpResponseLexer            l_req_lexer( o_map );

    return l_req_lexer.ParseSome( i_buffer, true );

}

// ============= HttpResponseLexer::~HttpResponseLexer ==============
HttpResponseLexer::~HttpResponseLexer()
{
    delete m_lexer;
}



} // end namespace
