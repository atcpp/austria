//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * \file at_xml_parser.cpp
 *
 * \author Guido Gherardi
 *
 * The Austria XML Parser implementation.
 *
 * The implementation of Austria's XML Parser is based on libxml2's C translation of the SAX interface.
 * @See http://xmlsoft.org/html/libxml-parser.html for details about the C interface.
 */

#include "at_xml_parser.h"
#include "at_exception.h"
#include "at_file.h"
#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <stdio.h>
#include <stdarg.h>
#include <vector>
#include <sstream>

#ifdef _WIN32
#define snprintf  _snprintf
#define vsnprintf _vsnprintf
#endif

// Austria namespace
namespace at
{

/**
 * XmlParserContext.
 */
class XmlParserContext
{
    /**
     *  The SAX Parser Handler.
     */
    static xmlSAXHandler s_saxHandler;

    /**
     *  The pointer to the SAX Parser Context.
     */
    xmlParserCtxtPtr m_ctxt;

    /**
     *  The XML document being constructed by the parser.
     */
    PtrDelegate< XmlDocument* > m_currentDocument;

    /**
     *  The vector of XmlElement objects.
     */
    std::vector< Ptr< XmlComposite* > > m_compositeVector;

    /**
     *  The list of parsing errors/warnings.
     */
    std::list< FileStatusReport_Basic >* m_errorListRef;

public:

    /**
     *  Creates an XML document parser context.
     */
    XmlParserContext()
    {
        Initialize();
    }

    /**
     *  Destroys an XML parser context. 
     */
    ~XmlParserContext()
    {
    }

    /**
     *  Parses an in-file XML document.
     *  @return A pointer to the resulting XmlDocument.
     *  @note This method must be called only once.
     */
    PtrDelegate< XmlDocument* > ParseFile(
        const char*                          i_filename,
        std::list< FileStatusReport_Basic >* o_errlist
        )
    {
        try
        {
            at::RFile l_file;
            if ( ! l_file.Open( at::FilePath( i_filename ) ) )
            {
                throw at::FileErrorBasic( l_file.GetError() );
            }
        }
        catch ( const at::FileError & l_error )
        {
            if ( o_errlist )
            {
                o_errlist->push_back(
                    FileStatusReport_Basic(
                        i_filename,
                        0,
                        StatusReport::ErrorStatus,
                        0,
                        l_error.What()
                        )
                    );
            }
            return 0;
        }
        m_ctxt = xmlCreateFileParserCtxt( i_filename );
        return ParseDocument( o_errlist );
    }

    /**
     *  Parses an in-memory XML document.
     *  @return A pointer to the resulting XmlDocument.
     *  @note This method must be called only once.
     */
    PtrDelegate< XmlDocument* > ParseMemory(
        const char*                          i_buffer,
        int                                  i_size,
        std::list< FileStatusReport_Basic >* o_errlist
        )
    {
        m_ctxt = xmlCreateMemoryParserCtxt( i_buffer, i_size );
        return ParseDocument( o_errlist );
    }

protected:

    /**
     *  Parses an XML document.
     *  @return A pointer to the resulting XmlDocument.
     *  @note This method must be called only once.
     */
    PtrDelegate< XmlDocument* > ParseDocument(
        std::list< FileStatusReport_Basic >* o_errlist
        )
    {
        if ( m_ctxt )
        {
            const xmlSAXHandlerPtr l_oldsax = m_ctxt->sax;
            m_ctxt->sax = &s_saxHandler;
            m_ctxt->userData = (void*)this;

            m_errorListRef = o_errlist;

            xmlParseDocument( m_ctxt );

            m_errorListRef = 0;

            if ( ! m_ctxt->wellFormed )
            {
                m_currentDocument = 0;

                if ( m_ctxt->errNo == 0 && o_errlist )
                {
                    o_errlist->push_back(
                        FileStatusReport_Basic(
                            StatusReport::ErrorStatus,
                            0,
                            "xmlParseDocument failed"
                            )
                        );
                }
            }
            m_ctxt->sax = l_oldsax;
            if ( m_ctxt->myDoc )
            {
                xmlFreeDoc( m_ctxt->myDoc );
                m_ctxt->myDoc = 0;
            }
            xmlFreeParserCtxt( m_ctxt );
            m_ctxt = 0;
            return m_currentDocument;
        }
        return 0;
    }

    /**
     * @brief  Called when the beginning of a document is detected.
     * @param  i_ctx  A pointer to the XmlParserContext.
     */
    static void StartDocument( XmlParserContext* i_ctx );

    /**
     * @brief  Called when the end of the document has been detected.
     * @param  i_ctx  A pointer to the XmlParserContext.
     */
    static void EndDocument( XmlParserContext* i_ctx );

    /**
     * @brief  Called when an opening tag has been parsed.
     * @param  i_ctx  A pointer to the XmlParserContext.
     * @param  i_name The element name, including namespace prefix.
     * @param  i_atts An array of name/value attributes pairs, NULL terminated.
     */
    static void StartElement( XmlParserContext* i_ctx, const xmlChar* i_name, const xmlChar** i_atts );

    /**
     * @brief  Called when an ending tag has been parsed.
     * @param  i_ctx  A pointer to the XmlParserContext.
     * @param  i_name The element name.
     */
    static void EndElement( XmlParserContext* i_ctx, const xmlChar* i_name );

    /**
     * @brief  Called whenever characters outside tags get parsed.
     * @param  i_ctx  A pointer to the XmlParserContext.
     * @param  i_str  A xmlChar string.
     * @param  i_len  The number of xmlChar.
     */
    static void Characters( XmlParserContext* i_ctx, const xmlChar* i_str, int i_len );

    /**
     * @brief  Called whenever a comment has been parsed.
     * @param  i_ctx     A pointer to the XmlParserContext.
     * @param  i_content The content of the comment.
     */
    static void Comment( XmlParserContext* i_ctx, const xmlChar* i_content );

    /**
     * @brief  Called whenever a processing instruction has been parsed.
     * @param  i_ctx     A pointer to the XmlParserContext.
     * @param  i_target  The target name of the processing instruction.
     * @param  i_data    The content of the processing instruction.
     */
    static void ProcessingInstruction( XmlParserContext* i_ctx,
                                       const xmlChar*    i_target,
                                       const xmlChar*    i_data );

    /**
     * @brief  Called whenever an entity is encountered.
     * @param  i_ctx  A pointer to the XmlParserContext.
     * @param  i_name The entity name.
     * @return The xmlEntityPtr if found.
     *
     * An entity (eg &lt;) is handled by the parser by calling this method, which returns
     * a pointer to an xmlEntity structure containing some information about the entity.
     * This structure will not be freed by the parser, so it makes sense to create all the
     * structures once, and have GetEntity return a pointer to the appropriate one when it
     * is called. After calling GetEntity, the expansion of the entity is passed to the
     * Characters callback method. This way, there is no need to worry about decoding
     * entities anywhere else in callback methods.
     */
    static xmlEntityPtr GetEntity( XmlParserContext* i_ctx, const xmlChar* i_name );

    /**
     * @brief  Called whenever an error or warning is encountered.
     * @param  i_ctx  A pointer to the XmlParserContext.
     * @param  i_msg  The message to display.
     * @param  ...    Extra parameters for the message display.
     */
    static void Error( XmlParserContext* i_ctx, const char* i_msg, ... );

    /**
     * @brief  Class Initializer.
     * This is like a static constructor, except we have to call it explicitely.
     */
    static void Initialize()
    {
        if ( ! s_saxHandler.initialized )
        {
            s_saxHandler.getEntity =
                (getEntitySAXFunc)XmlParserContext::GetEntity;
            s_saxHandler.startDocument =
                (startDocumentSAXFunc)XmlParserContext::StartDocument;
            s_saxHandler.endDocument =
                (endDocumentSAXFunc)XmlParserContext::EndDocument;
            s_saxHandler.startElement =
                (startElementSAXFunc)XmlParserContext::StartElement;
            s_saxHandler.endElement =
                (endElementSAXFunc)XmlParserContext::EndElement;
            s_saxHandler.characters =
                (charactersSAXFunc)XmlParserContext::Characters;
            s_saxHandler.comment =
                (commentSAXFunc)XmlParserContext::Comment;
            s_saxHandler.processingInstruction =
                (processingInstructionSAXFunc)XmlParserContext::ProcessingInstruction;
            s_saxHandler.warning =
                (warningSAXFunc)XmlParserContext::Error;
            s_saxHandler.error =
                (errorSAXFunc)XmlParserContext::Error;
            s_saxHandler.fatalError =
                (errorSAXFunc)XmlParserContext::Error;
            s_saxHandler.initialized = 1;
        }
    }
};

xmlSAXHandler XmlParserContext::s_saxHandler;

void XmlParserContext::StartDocument( XmlParserContext* i_ctx )
{
    if ( i_ctx )
    {
        PtrView< XmlDocument* > l_document( new XmlDocument() );
        i_ctx->m_currentDocument = l_document;
        i_ctx->m_compositeVector.push_back( l_document );
    }
}

void XmlParserContext::EndDocument( XmlParserContext* i_ctx )
{
    if ( i_ctx && ! i_ctx->m_compositeVector.empty() )
    {
        i_ctx->m_compositeVector.pop_back();
    }
}

void XmlParserContext::StartElement( XmlParserContext* i_ctx, const xmlChar* i_name, const xmlChar** i_atts )
{
    if ( i_ctx && i_ctx->m_currentDocument )
    {
        PtrDelegate< XmlElement* > l_element = i_ctx->m_currentDocument->CreateElement( (const char*)i_name );
        if ( i_atts ) {
            while ( i_atts[ 0 ] && i_atts[ 1 ] )
            {
                l_element->SetAttribute( (const char*)i_atts[ 0 ], (const char*)i_atts[ 1 ] );
                i_atts += 2;
            }
            if ( *i_atts )
            {
                l_element->SetAttribute( (const char*)*i_atts++, "" );
            }
        }
        i_ctx->m_compositeVector.push_back( l_element );
    }
}

void XmlParserContext::EndElement( XmlParserContext* i_ctx, const xmlChar* i_name )
{
    if ( i_ctx && i_ctx->m_compositeVector.size() > 1 )
    {
        PtrDelegate< XmlComposite* > l_element = i_ctx->m_compositeVector.back();
        i_ctx->m_compositeVector.pop_back();
        i_ctx->m_compositeVector.back()->AppendChild( l_element );
    }
}

void XmlParserContext::Characters( XmlParserContext* i_ctx, const xmlChar* i_str, int i_len )
{
    if ( i_ctx && i_ctx->m_currentDocument && ! i_ctx->m_compositeVector.empty() )
    {
        const std::string l_data( (const char*)i_str, i_len );
        PtrDelegate< XmlText* > l_text = i_ctx->m_currentDocument->CreateTextNode( l_data );
        i_ctx->m_compositeVector.back()->AppendChild( l_text );
    }
}

void XmlParserContext::Comment( XmlParserContext* i_ctx, const xmlChar* i_content )
{
    if ( i_ctx && i_ctx->m_currentDocument && ! i_ctx->m_compositeVector.empty() )
    {
        const std::string l_data( (const char*)i_content );
        PtrDelegate< XmlComment* > l_comment = i_ctx->m_currentDocument->CreateComment( l_data );
        i_ctx->m_compositeVector.back()->AppendChild( l_comment );
    }
}

void XmlParserContext::ProcessingInstruction( XmlParserContext* i_ctx,
                                              const xmlChar*    i_target,
                                              const xmlChar*    i_data )
{
    if ( i_ctx && i_ctx->m_currentDocument && ! i_ctx->m_compositeVector.empty() )
    {
        const std::string l_target( (const char*)i_target );
        const std::string l_data( (const char*)i_data );
        PtrDelegate< XmlProcessingInstruction* > l_procInstr =
            i_ctx->m_currentDocument->CreateProcessingInstruction( l_target, l_data );
        i_ctx->m_compositeVector.back()->AppendChild( l_procInstr );
    }
}

xmlEntityPtr XmlParserContext::GetEntity( XmlParserContext* i_ctx, const xmlChar* i_name )
{
    return xmlGetPredefinedEntity( i_name );
}

void XmlParserContext::Error( XmlParserContext* i_ctx, const char* i_msg, ... )
{
    if ( i_ctx && i_ctx->m_errorListRef )
    {
        if ( const xmlErrorPtr l_err = xmlCtxtGetLastError( i_ctx->m_ctxt ) )
        {
            size_t l_msglen = 0;
            for ( char c; ( c = l_err->message[ l_msglen ] ) != '\0' && c != '\n'; )
            {
                ++l_msglen;
            }
            FileStatusReport_Basic l_report(
                l_err->file ? l_err->file : "",
                l_err->line,
                l_err->level == XML_ERR_WARNING ? StatusReport::WarningStatus : StatusReport::ErrorStatus,
                l_err->code,
                std::string( l_err->message, l_msglen )
                );
            if ( i_ctx->m_errorListRef->empty() || l_report != i_ctx->m_errorListRef->back() )
            {
                i_ctx->m_errorListRef->push_back( l_report );
            }
            xmlResetLastError();
        }
    }
}

PtrDelegate< XmlDocument* > XmlParseFile(
    const char*                             i_filename,
    std::list< FileStatusReport_Basic >*    o_errlist
    )
{
    return XmlParserContext().ParseFile( i_filename, o_errlist );
}

PtrDelegate< XmlDocument* > XmlParseMemory(
    const char*                             i_buffer,
    size_t                                  i_size,
    std::list< FileStatusReport_Basic >*    o_errlist
    )
{
    return XmlParserContext().ParseMemory( i_buffer, i_size, o_errlist );
}

PtrDelegate< XmlDocument* > XmlParseFile( const char* i_filename )
{
    std::list< FileStatusReport_Basic > l_errlist;
    if ( PtrDelegate< XmlDocument* > l_result =
             XmlParserContext().ParseFile( i_filename, &l_errlist ) )
    {
        return l_result;
    }
    if ( ! l_errlist.empty() )
    {
        std::ostringstream l_oss;
        l_errlist.front().Print( l_oss );
        AT_ThrowDerivation( XmlParserErr, l_oss.str().c_str() );
    }
    AT_ThrowDerivation( XmlParserErr, "unknown parser error" );
    return 0;
}

PtrDelegate< XmlDocument* > XmlParseMemory( const char* i_buffer, size_t i_size )
{
    std::list< FileStatusReport_Basic > l_errlist;
    if ( PtrDelegate< XmlDocument* > l_result =
             XmlParserContext().ParseMemory( i_buffer, i_size, &l_errlist ) )
    {
        return l_result;
    }
    if ( ! l_errlist.empty() )
    {
        std::ostringstream l_oss;
        l_errlist.front().Print( l_oss );
        AT_ThrowDerivation( XmlParserErr, l_oss.str().c_str() );
    }
    AT_ThrowDerivation( XmlParserErr, "unknown parser error" );
    return 0;
}

} // end of namespace
