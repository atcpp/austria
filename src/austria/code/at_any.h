//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_any.h
 *
 */

#pragma warning ( disable : 4800 )

#ifndef x_at_any_h_x
#define x_at_any_h_x 1

#include "at_lifetime.h"
#include "at_exports.h"
#include "at_exception.h"

#include <string>
#include <cstring>
#include <sstream>
#include <typeinfo>


// Austria namespace
namespace at
{
    
/**
 * @defgroup AustriaAny Any Object Wrapper

<h2>Introduction</h2>

<p>Run-time generic programming requires a run-time type that may
encompass any object.  The Austria at::Any class is able to wrap any
non-array type as long as it provides a copy constructor, destructor,
self-assignment and equality and less than comparisons.

<h3>How to Use The at::Any Class</h3>

<p>The Any class is actually a template in which the only parameter is
the base class for the instantiated Any class.  This allows for
providing more functionality to an Any object if required.  An Any
class can interoperate with all other Any instantiations.

<h5>Example for Creating and Reading an Any Object</h5>
<CODE>
<pre>

    struct Base { virtual ~Base() {} };

    Any<>       any1( short( 44 ), at::AnyInit );
    Any<Base>   any2 = ToAny( short( 45 ) );
    Any<>       any3( ToAny( short( 46 ) ) );

    Any<>       anyE;
    
    any2 = any1; // any2( short(45) ) is assigned short(44)

    std::cout << any2; // should print "44"

    short   value = any1.Refer(); // makes a copy

    short   & ref = any2.Refer();

    short     val;
    any2.Read( val ); // Returns reference to "val"
    // Read assigns the value in an Any object to the variable passed in
    // and returns a reference to the variable passed in. Read will throw
    // if the type of the variable passed to it is not the same as the
    // type stored in the Any object.

    int     value = any1.Refer(); // THROWS - no conversion from short to int
    // ( const at::ExceptionDerivation<Any_ConversionFailure> & ) exception thrown here ...
    
    int         valint;
    any2.Read( valint ); // THROWS - any2 contains short but passed an int reference

</pre>
</CODE>

<p>In the preceding example, the at::Any objects named any1, any2 and any3 are initialized
with an object of type short( 44 ), short( 45 ) and short( 46 ) respectively. The any2
object is of a different base type (has a virtual destructor), however it may be assigned
a value from any other Any<> type object ( as shown - any2 = any1 ).  The anyE Any variable
has no value.  It is initialized to "Empty".  Empty Any objects maintain no storage.

<p>An at::Any object can be output directly using the std::ostream::operator <<.  A
function specific to the type will be used to format the string representing the object.

<p>The easiest way to refer to the object is to know what object it is.  The Any class
has a member function at::Any::Refer() that can be used to expose the object within the
Any instance.  at::Any::Refer() returns a proxy class that detects the type being read or written
to it and translates the assignment to the destination (or source type).

<h5>Example for Writing Values to an Any Object</h5>
<CODE>
<pre>

    at::Any<Base>   any2 = at::ToAny( short( 45 ) );

    short   & ref = any2.Refer();

    ref = 3; // changes the value within the any2 Any object.

    any2.Refer() = short(4); // Assign 4 to any2

    any2.Assign( short(5) ); // Assign 5 to any2

    any2.Refer() = std::string("Hello"); // replace short(5) with std::string.

    std::string     * str( 0 );

    if ( any2.ReadPointer( str ) )
    {
        // any2 is a string !
        * str = "Change it to this";
    }
    
</pre>
</CODE>

<p>An Any object may be modified using the at::Any::Refer() returned proxy using the
assignment operator ( operator = ). If the source object type being assigned to an Any object is
different from the type of object contained within the Any instance it will be replaced with
a copy of the source object.

<p>In the first two lines of the previous example, a new Any object is created and a reference to
the value within the Any variable is created.  The value within the Any object may be assigned
with the "ref" reference variable.  However, the Refer() method returns a proxy class that
may also be assigned to which will result in either an assignment only if the type being assigned
is the same as the type stored within the Any object, otherwise a new copy is made of the
value being assigned and the previous value within the Any object is deleted.
The sixth line in the example shows a std::string being assigned. As described earlier, the
previous value (short(5)) is deleted and replaced with a copy of the std::string("Hello") value.

<p>It is possible to determine if the value stored within an Any object is of a particular
type using the at::Any::ReadPointer method.  ReadPointer will return true if the type
pointed to by the pointer passed into ReadPointer is the same as the type stored within the
object and if so will set the passed in pointer to the address of the object within the Any instance.


<h5>Example of Using at::Any in STL Containers </h5>
<CODE>
<pre>

    std::map< at::Any<>, at::Any<> > anymap; // create a map of at::Any objects keyed by at::Any objects

    // as an associative array, assign anymap[ "Hello" ] with "World"
    anymap[ at::Any<>( std::string( "Hello" ), at::AnyInit ) ] = at::Any<>( std::string( "World" ), at::AnyInit );

    // in the same std::map, assign anymap[ 6 ] with a std::vector
    anymap[ at::Any<>( int(6), at::AnyInit ) ] = at::Any<>( std::vector< at::Any<> >( 5 ), at::AnyInit );

    // int(6) is different from short(6) to at::Any
    anymap[ at::Any<>( short(6), at::AnyInit ) ] = at::Any<>( short(), at::AnyInit );


</pre>
</CODE>

<p>An at::Any object can be stored inside STL template library containers. In the preceding
example is an example of at::Any being used as both a data value and key of an std::map instance.
The order in which at::Any keys of different types are stored is implementation dependant and may
change on different runs of the same binary but will be consistent in a single process.

<h5>at::Any types and Relational Operators</h5>
<CODE>
<pre>
    
    Any<>       any1( ToAny( short( 1 ) ) );
    Any<>       any2( ToAny( short( 2 ) ) );
    Any<>       anyo( ToAny( short( 1 ) ) );

    assert( any1 < any2 ); // comparison of same types OK
    assert( any1 == anyo ); 
    assert( any1 == short( 1 ) );
    assert( short( 1 ) == any1 );
    assert( unsigned( 1 ) != any1 ); // NOTE: unsigned(1) != Any( short(1) )

</pre>
</CODE>

<p>at::Any implements the <, >, == and != relational operators. Objects of different types may
be compared and follow the following rules.
<ul>Objects of different types are not equal.  This may produce some surprises.  Only objects of
the same type may compare as equal.

<h5>at::Ptr, at::PtrView and at::PtrDelegate with at::Any</h5>
<CODE>
<pre>

    struct A : PtrTarget_MT { };

    Ptr< A * > p = new A;
    PtrView< A * > pv = p;
    PtrDelegate< A * > pd;

    // Any objects convert PtrView or PtrDelegate smart pointers
    // to Ptr smart pointers. Hence only Ptr pointers are stored within
    // Any objects.
    
    Any<> a0 = ToAny( pv ); // PtrView assignments to an Any are converted to Ptr

    Ptr< A * >  * pp;

    a0.ReadPointer( pp ) == true; // get a pointer to the internal Ptr;

    PtrDelegate< A * >  * ppd;
    
    a0.ReadPointer( ppd ) == true; // THROWS - internal object is a Ptr
    // Can't make a pointer from a Ptr to a PtrDelegate

    pd = a0.ReferPtr(); // PtrRefer works for assignment to at::Ptr* types

</pre>
</CODE>

<p>at::Any stores only at::Ptr objects of at::PtrView or at::PtrDelegate.
This is done by conversion.

<h5>Changing the Internal Any Representation</h5>
<CODE>
<pre>

    struct A : { };
    struct B : A { };


    // Make it so any pointers to B are converted to
    // an A pointer within the Any pointer.
    template <>
    struct AnyAssignType< B * >
    {
        typedef A * t_AssignType;
    };

    B * b = new B;
    A * a;
    Any<>   val( b, at::AnyInit ); // This really stores a type A *

    B * b = val.Refer(); // OOPS Throws.
    

</pre>
</CODE>

<p>at::Any stores only Ptr objects.

 *
 * 
 *  @{
 */

template <typename T>
class AnyConverter;

template <typename w_AnyBase>
class AnyPtrConverter;


// ======== AnyNull ===================================================
/**
 * AnyNull is a Null base class for the Any class which can be
 * used in cases where there is no need for a base class.
 */

struct AUSTRIA_EXPORT AnyNull {};


template <typename w_Base = AnyNull>
class Any;

template <typename w_type>
class AnyManager_Red;

template <typename w_cv_type>
class AnyManager_Maker;

template <typename w_Type>
class ToAnyConvert;

// ======== AnyManager ================================================
/**
 * AnyManager describes the interface for managing any class using
 * the Any object.  In particular there are interfaces to manage an
 * object's life (delete), assignment and comparison.
 *
 */

class AUSTRIA_EXPORT AnyManager
{
    template <typename w_type>
    friend class AnyManager_Red;
    
    protected:

    /**
     * AnyManager constructor requires just the type_info.
     *
     * @param i_type_info is the type info for this object.
     */
    AnyManager(
        const std::type_info        & i_type_info
    )
      : m_type_info( i_type_info )
    {
    }

    public:

    /**
     * m_type_info is the type info of this AnyManager.
     */

    const std::type_info                  & m_type_info;


    // ======== DeleteObject ==========================================
    /**
     * DeleteObject will perform deletion of an object.
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be deleted.
     */

    virtual void DeleteObject(
        void                              * i_object
    ) const = 0;

    // ======== CopyObject ============================================
    /**
     * CopyObject will create a copy of an object of type
     * m_type_info
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be copied.
     * @return A void * pointer to the new object.
     */

    virtual void * CopyObject(
        const void                        * i_object
    ) const = 0;
    

    // ======== AssignObject ==========================================
    /**
     * AssignObject will perform an assignment from one object to
     * another given the source and destination values.  Both
     * void * pointers must point to the same object type.
     *
     * @param i_dest A void pointer to the object being written.
     * @param i_source A void pointer to the object being read.
     */

    virtual void AssignObject(
        void                              * i_dest,
        const void                        * i_source
    ) const = 0;

    // ======== CompareLess ===========================================
    /**
     * CompareLess will perform the operator < on two objects of type
     * m_type_info i.e.
     *    i_lhs < i_rhs
     *
     * @param i_lhs A void pointer to the left hand side object
     * @param i_rhs A void pointer to the right hand side object
     * @return True if i_lhs < i_rhs
     */

    virtual bool CompareLess(
        const void                        * i_lhs,
        const void                        * i_rhs
    ) const = 0;

    // ======== CompareEqual ==========================================
    /**
     * CompareEqual tests for object equality.
     *
     * @param i_lhs A void pointer to the left hand side object
     * @param i_rhs A void pointer to the right hand side object
     * @return True if i_lhs == i_rhs
     */

    virtual bool CompareEqual(
        const void                        * i_lhs,
        const void                        * i_rhs
    ) const = 0;

    // ======== ToString ==============================================
    /**
     * ToString converts the object to a string mainly for logging and
     * display purposes.
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be converted to string.
     * @return nothing
     */

    virtual std::string ToString(
        const void                        * i_object
    ) const = 0;


    // ======== ToStream ==============================================
    /**
     * ToStream performs a std::ostream << on the underlying object.
     *
     * @param o_ostream The stream to perform the << on
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be streamed.
     * @return nothing
     */

    virtual void ToStream(
        std::ostream                        & o_ostream,
        const void                        * i_object
    ) const = 0;

    
};

// ======== AnyToStr ===============================================
/**
 *  Convert an object to a string for human readability purposes.
 *
 * @param i_val is the object to convert to a string
 * @return the string representation of the object
 */

template< typename  w_Type >
std::string AnyToStr( const w_Type & i_val )
{
    std::ostringstream  l_ostream;

    l_ostream << i_val;
    
    return l_ostream.str();

} // end AnyToStr


// ======== AnyToStream ============================================
/**
 * Output an object to a stream. This is the default for an Any
 * object; it can however be changed for a specific type by
 * overloading the AnyToStream function with a new definition
 * specific to particular needs.
 *
 * NOTE: To ensure no surprises, make sure to keep the
 * overloaded AnyToStream declaration just after the type being
 * overloaded for. Since there is only one "Manager" class
 * defined for any particular type it may be possible to
 * create different behaviour with different instantiations
 * of the manager object and at link time only one is used
 * (per DLL) which may cause different behaviour with different
 * linking scenarios.  This note also applies to at::AnyToStr.
 *
 * @param o_ostream The stream to perform the << on
 * @param i_val is the object to be streamed
 */

template< typename  w_Type >
void AnyToStream(
    std::ostream                        & o_ostream,
    const w_Type                        & i_val
)
{
    o_ostream << i_val;
    
} // end AnyToStream


// ======== AnyManager_Empty ==========================================
/**
 * AnyManager_Empty describes an "empty" object.
 *
 */

class AUSTRIA_EXPORT AnyManager_Empty
  : public AnyManager
{
    private: struct Empty {};
    
    public:

    AnyManager_Empty()
      : AnyManager( typeid( Empty ) )
    {
    }
    
    // ======== DeleteObject ==========================================

    virtual void DeleteObject(
        void                              * i_object
    ) const {
    }

    // ======== CopyObject ============================================
    /**
     * CopyObject will create a copy of an object of type
     * m_type_info
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be copied.
     * @return A void pointer to the new object.
     */

    virtual void * CopyObject(
        const void                        * i_object
    ) const {
        return 0;
    }


    // ======== AssignObject ==========================================
    /**
     * AssignObject will perform an assignment from one object to
     * another given the source and destination values.  Both
     * void pointers must point to the same object type.
     *
     * @param i_dest A void pointer to the object being written.
     * @param i_source A void pointer to the object being read.
     */

    virtual void AssignObject(
        void                              * i_dest,
        const void                        * i_source
    ) const {
    }

    // ======== CompareLess ===========================================
    /**
     * CompareLess will perform the operator < on two objects of type
     * m_type_info i.e.
     *    i_lhs < i_rhs
     *
     * @param i_lhs A void pointer to the left hand side object
     * @param i_rhs A void pointer to the right hand side object
     * @return True if i_lhs < i_rhs
     */

    virtual bool CompareLess(
        const void                        * i_lhs,
        const void                        * i_rhs
    ) const {
        return false;
    }

    // ======== CompareEqual ==========================================
    /**
     * CompareEqual tests for object equality.
     *
     * @param i_lhs A void pointer to the left hand side object
     * @param i_rhs A void pointer to the right hand side object
     * @return True if i_lhs == i_rhs
     */

    virtual bool CompareEqual(
        const void                        * i_lhs,
        const void                        * i_rhs
    ) const {
        return true;
    }

    // ======== ToString ==============================================
    /**
     * ToString converts the object to a string mainly for logging and
     * display purposes.
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be converted to string.
     * @return The string representation of the object.
     */

    virtual std::string ToString(
        const void                        * i_object
    ) const {
        return "at::AnyManager_Empty";
    }

    // ======== ToStream ==============================================
    /**
     * ToStream performs a std::ostream << on the underlying object.
     *
     * @param o_ostream The stream to perform the << on
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be streamed.
     * @return nothing
     */

    virtual void ToStream(
        std::ostream                        & o_ostream,
        const void                        * i_object
    ) const {
        o_ostream << "at::AnyManager_Empty";
    }

    public:
    static const AnyManager_Empty & GetMgr()
    {
        static const AnyManager_Empty           s_manager;
        return s_manager;
    }
    
    static const std::type_info         & s_empty_type;
};


// ======== AnyManager_Red ============================================
/**
 * AnyManager_Red implements the AnyManager for a particular class.
 *
 */

template <typename w_type>
class AnyManager_Red
  : public AnyManager
{
    public:

    template <typename w_cv_type>
    friend class AnyManager_Maker;

    /**
     * AnyManager_Red
     *
     */
    AnyManager_Red()
      : AnyManager( typeid( w_type ) )
    {
    }


    // ======== DeleteObject ==========================================
    /**
     * DeleteObject will perform deletion of an object.
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be deleted.
     */

    virtual void DeleteObject(
        void                              * i_object
    ) const {
        delete static_cast<w_type *>( i_object );
    }

    // ======== CopyObject ============================================
    /**
     * CopyObject will create a copy of an object of type
     * m_type_info
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be copied.
     * @return A void * pointer to the new object.
     */

    virtual void * CopyObject(
        const void                        * i_object
    ) const {
        return static_cast< void * >(
            new w_type( * static_cast<const w_type *>( i_object ) )
        );
    }


    // ======== AssignObject ==========================================
    /**
     * AssignObject will perform an assignment from one object to
     * another given the source and destination values.  Both
     * void pointers must point to the same object type.
     *
     * @param i_dest A void pointer to the object being written.
     * @param i_source A void pointer to the object being read.
     */

    virtual void AssignObject(
        void                              * i_dest,
        const void                        * i_source
    ) const {
        * static_cast< w_type * >( i_dest ) =
            * static_cast< const w_type * >( i_source );
    }

    // ======== CompareLess ===========================================
    /**
     * CompareLess will perform the operator < on two objects of type
     * m_type_info i.e.
     *    i_lhs < i_rhs
     *
     * @param i_lhs A void pointer to the left hand side object
     * @param i_rhs A void pointer to the right hand side object
     * @return True if i_lhs < i_rhs
     */

    virtual bool CompareLess(
        const void                        * i_lhs,
        const void                        * i_rhs
    ) const {
        return
            * static_cast< const w_type * >( i_lhs ) <
            * static_cast< const w_type * >( i_rhs );
    }

    // ======== CompareEqual ==========================================
    /**
     * CompareEqual tests for object equality.
     *
     * @param i_lhs A void pointer to the left hand side object
     * @param i_rhs A void pointer to the right hand side object
     * @return True if i_lhs == i_rhs
     */

    virtual bool CompareEqual(
        const void                        * i_lhs,
        const void                        * i_rhs
    ) const {
        return
            * static_cast< const w_type * >( i_lhs ) ==
            * static_cast< const w_type * >( i_rhs );
    }

    // ======== ToString ==============================================
    /**
     * ToString converts the object to a string mainly for logging and
     * display purposes.
     *
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be converted to string.
     * @return The string representation of the object.
     */

    virtual std::string ToString(
        const void                        * i_object
    ) const {
        return AnyToStr( * static_cast< const w_type * >( i_object ) );
    }

    // ======== ToStream ==============================================
    /**
     * ToStream performs a std::ostream << on the underlying object.
     *
     * @param o_ostream The stream to perform the << on
     * @param i_object A void pointer to the object of type
     *                  m_type_info to be streamed.
     * @return nothing
     */

    virtual void ToStream(
        std::ostream                        & o_ostream,
        const void                        * i_object
    ) const {
        AnyToStream( o_ostream, * static_cast< const w_type * >( i_object ) );
    }

    protected:
    
    static const AnyManager_Red & GetMgr()
    {
        static const AnyManager_Red             s_manager;
        return s_manager;
    }
        
};

// ======== AnyManager_Maker ==========================================
/**
 * AnyManager_Maker contains the resources associated with a particular
 * type.
 */

template <typename w_cv_type>
class AnyManager_Maker
{
    public:

    /**
     * t_Type is the real type that we're going to use for the one
     * passed in.  The type is removed of const-ness and volatile-ity.
     */

    typedef typename NonConstVolatile< w_cv_type >::t_Type  t_Type;

    /**
     * The type of the Manager for this AnyManager_Maker
     */
    typedef AnyManager_Red< t_Type >                        t_Manager;

    /**
     * s_any_manager is the AnyManager implementation for the non-const
     * non-volatile AnyManager_Red version of this type. This ensures that
     * if an Any object is created from a const T or non-const T, the
     * object will be the same kind of Any object.  Constness becomes
     * a facet of the Any object itself.
     */

    static const AnyManager & GetMgr()
    {
        const AnyManager & s_manager = t_Manager::GetMgr();
        return s_manager;
    }
        
};


// ======== Any_ConversionFailure =====================================
/**
 * Any_ConversionFailure is used with the AT_ThrowDerivation macro
 * to create a specific exception type for conversion failure.
 *
 */

struct Any_ConversionFailure {};


// ======== Any_ThrowConversionFailure =================================
/**
 * Any_ThrowConversionFailure simply throws a
 * ExceptionDerivation<Any_ConversionFailure> exception.
 *
 *
 * @return nothing
 */

extern void Any_ThrowConversionFailure();



//
// These specializations are so that we don't
// inadvertently create Any objects of AnyConverter
// objects.
//
template <typename T>
class AnyConverter<AnyConverter<T> >;

template <typename T>
class AnyConverter<AnyPtrConverter<T> >;

template <typename T>
class AnyPtrConverter<AnyConverter<T> >;

template <typename T>
class AnyPtrConverter<AnyPtrConverter<T> >;

template <typename T>
class AnyManager_Red<AnyConverter<T> >;

template <typename T>
class AnyManager_Maker<AnyConverter<T> >;

template <typename T>
class AnyManager_Maker<AnyPtrConverter<T> >;


// ======== AnyAssignType ====================================
/**
 * AnyAssignType determines the storage type for an Any
 * object.
 */

template <typename w_Type>
struct AnyAssignType
{
    typedef w_Type                      t_AssignType;
};

template <typename w_Type, typename w_Traits>
struct AnyAssignType< PtrView< w_Type, w_Traits > >
{
    typedef Ptr< w_Type, w_Traits >     t_AssignType;
};

template <typename w_Type, typename w_Traits>
struct AnyAssignType< PtrDelegate< w_Type, w_Traits > >
{
    typedef Ptr< w_Type, w_Traits >     t_AssignType;
};


// ======== AnyInit ===================================================
/**
 * AnyInitType is used as a marker for using the Any value initializer.
 * It serves no other purpose.
 */

struct AnyInitType {};

extern const AnyInitType AnyInit;

// ======== Any =======================================================
/**
 * Any is a wrapper for any type. It uses std::type_info to determine
 * the real type of the class.
 * 
 * Any is reference counted which allows passing of values by pointer.
 */

template <typename w_Base>
class Any
  : public w_Base
{
    friend class AnyReference;

    // ======== ConversionCheck =======================================
    /**
     * ConversionCheck is used to perfom a check that the template
     * parameter is the same as the type of this class.
     *
     * @param w_Type is the destination type.
     * @return nothing
     */

    template <typename w_Type>
    inline void ConversionCheck()
    {
        if ( m_manager->m_type_info != typeid( typename AnyManager_Maker< w_Type >::t_Type ) )
        {
            Any_ThrowConversionFailure();
        }

    }
    
    public:

    friend class AnyConverter<w_Base>;
    friend class AnyPtrConverter<w_Base>;

    template <typename w_OtherBase>
    friend class Any;

    // ======== Any ===================================================
    /**
     * Any constructor - convert a non-Any object to an Any object.
     *
     * Usage: Any( value, AnyInit )
     *
     * @param i_value is the value being assigned
     * @param i_unused is a marker to avoid confusion with other conversion
     *                  operators.
     */
    
    template <typename w_Type>
    Any(
        const w_Type                & i_value,
        const AnyInitType           &
    )
      : m_manager( & AnyManager_Maker< w_Type >::GetMgr() ),
        m_object( m_manager->CopyObject( static_cast< const void* >( & i_value ) ) )
    {
    }


    // ======== Assign ================================================
    /**
     * Assignment operator - assign this Any object from a non Any
     * object.
     *
     * @param i_value is the object being assigned to this
     * @return A reference to the assigned Any object
     */

    template <typename w_Type>
    Any & Assign(
        const w_Type                        & i_value
    ) {
        typedef typename AnyAssignType< w_Type >::t_AssignType t_assign_type;

        if ( m_manager->m_type_info == typeid( typename AnyManager_Maker< t_assign_type >::t_Type ) )
        {
            // do assignment
            * static_cast< t_assign_type * >( m_object ) = i_value;
        }
        else
        {
            const AnyManager * const l_new_manager = & AnyManager_Maker< w_Type >::GetMgr();
            
            // objects are different; must create a new object
            // and replace the old (in that order).

            void * l_new_obj = new t_assign_type( i_value );

            void * l_old_obj = m_object;
            const AnyManager * l_old_manager = m_manager;

            // before deletion - must set up this object
            // consistently otherwise if the destructor
            // of the old object in some way causes this to be
            // deleted,
            m_object = l_new_obj;
            m_manager = l_new_manager;

            l_old_manager->DeleteObject( l_old_obj );
        }

        return * this;
    }

    // ======== Default ===============================================
    /**
     * Default will "default initialize" a value of a particular type.
     *
     * @param w_Type The type of object to default initialize
     * @return A reference to the default initialized Any object
     */

    template <typename w_Type>
    Any & Default()
    {
        typedef typename AnyAssignType< w_Type >::t_AssignType t_assign_type;

        const AnyManager * const l_new_manager = & AnyManager_Maker< w_Type >::GetMgr();
        
        // must create a new object
        // and replace the old (in that order).

        void * l_new_obj = new t_assign_type(); // create a default object of w_Type

        void * l_old_obj = m_object;
        const AnyManager * l_old_manager = m_manager;

        // before deletion - must set up this object
        // consistently otherwise if the destructor
        // of the old object in some way causes this to be
        // deleted,
        
        m_object = l_new_obj;
        m_manager = l_new_manager;

        l_old_manager->DeleteObject( l_old_obj );

        return * this;
    }
    

    // ======== Read ==================================================
    /**
     * Read will read a value from an Any type.
     *
     * @return A reference to the newly stored value.
     */

    public:
    template <typename w_Type>
    w_Type & Read(
        w_Type                              & i_value
    ) {
        
        typedef typename AnyAssignType< w_Type >::t_AssignType t_assign_type;

        ConversionCheck< t_assign_type >();
        
        return i_value = * static_cast< t_assign_type * >( m_object );
    }
    
    // ======== operator= =============================================
    /**
     * Assignment operator is able to accept assignment from
     * the result of a ToAny<> call.
     *
     * @param i_value is the object being assigned to this
     * @return A reference to the assigned Any object
     */

    template <typename w_Type>
    inline Any & operator=(
        const ToAnyConvert<w_Type>              & i_value
    ) {
        return Assign( i_value.m_value );
    }

    // ======== Any ===================================================
    /**
     * Any default constructor - this is an empty Any.
     *
     */

    Any()
      : m_manager( & AnyManager_Empty::GetMgr() ),
        m_object( 0 )
    {
    }

    // ======== Any ===================================================
    /**
     * Any copy constructor.
     *
     */
    Any(
        const Any           & i_any
    )
      : m_manager( i_any.m_manager ),
        m_object( m_manager->CopyObject( i_any.m_object ) )
    {
    }


    // ======== operator= =============================================
    /**
     * Assignment operator - assign this Any object from another
     * Any object.
     *
     * @param i_any is the object being assigned to this
     * @return A reference to the assigned Any object
     */

    Any & operator=(
        const Any           & i_any
    ) {
        if ( m_manager->m_type_info == i_any.m_manager->m_type_info )
        {
            // do assignment
            m_manager->AssignObject( m_object, i_any.m_object );
        }
        else
        {
            // objects are different; must create a new object
            // and replace the old (in that order).

            void * l_new_obj = i_any.m_manager->CopyObject(
                i_any.m_object
            );

            void * l_old_obj = m_object;
            const AnyManager * l_old_manager = m_manager;

            // before deletion - must set up this object
            // consistently otherwise if the destructor
            // of the old object in some way causes this to be
            // deleted,
            m_object = l_new_obj;
            m_manager = i_any.m_manager;

            l_old_manager->DeleteObject( l_old_obj );
        }

        return * this;
    }

    // ======== Any ===================================================
    /**
     * Any copy constructor from a different base type
     *
     */
    template <typename w_OtherBase>
    Any(
        const Any<w_OtherBase>           & i_any
    )
      : m_manager( i_any.m_manager ),
        m_object( m_manager->CopyObject( i_any.m_object ) )
    {
    }

    // ======== operator= =============================================
    /**
     * Assignment operator - assign this Any object from another
     * Any object.
     *
     * @param i_any is the object being assigned to this
     * @return A reference to the assigned Any object
     */

    template <typename w_OtherBase>
    Any & operator=(
        const Any<w_OtherBase>           & i_any
    ) {
        if ( m_manager->m_type_info == i_any.m_manager->m_type_info )
        {
            // do assignment
            m_manager->AssignObject( m_object, i_any.m_object );
        }
        else
        {
            // objects are different; must create a new object
            // and replace the old (in that order).

            void * l_new_obj = i_any.m_manager->CopyObject(
                i_any.m_object
            );

            void * l_old_obj = m_object;
            const AnyManager * l_old_manager = m_manager;

            // before deletion - must set up this object
            // consistently otherwise if the destructor
            // of the old object in some way causes this to be
            // deleted,
            m_object = l_new_obj;
            m_manager = i_any.m_manager;

            l_old_manager->DeleteObject( l_old_obj );
        }

        return * this;
    }

    // ======== CompareLess ===========================================
    /**
     * CompareLess will perform the operator < on this and another Any object
     * i.e.
     *    *this < i_rhs
     *    
     * @param i_rhs The right hand side object
     * @return True if *this < i_rhs
     */

    template <typename w_OtherBase>
    inline bool CompareLess(
        const Any<w_OtherBase>            & i_rhs
    ) const {
        
        if ( m_manager->m_type_info == i_rhs.m_manager->m_type_info )
        {
            return m_manager->CompareLess( m_object, i_rhs.m_object );
        }

        return m_manager->m_type_info.before( i_rhs.m_manager->m_type_info );
    }

    // ======== CompareEqual ==========================================
    /**
     * CompareEqual tests for object equality.
     *
     * @param i_rhs The right hand side object
     * @return True if *this == i_rhs
     */

    template <typename w_OtherBase>
    inline bool CompareEqual(
        const Any<w_OtherBase>            & i_rhs
    ) const {
        
        if ( m_manager->m_type_info == i_rhs.m_manager->m_type_info )
        {
            return m_manager->CompareEqual( m_object, i_rhs.m_object );
        }

        return false;
    }


    // ======== CompareEqualType ======================================
    /**
     * CompareEqualType compares equality given a value.
     *
     *
     * @param w_Type the type being compared to
     * @param i_value The value being compared
     * @return True if the comparison between this and i_value is Equal
     */

    template <typename w_Type>
    inline bool CompareEqualType(
        const w_Type                        & i_value
    ) const {
        if ( m_manager->m_type_info == typeid( typename AnyManager_Maker< w_Type >::t_Type ) )
        {
            return m_manager->CompareEqual( m_object, & i_value );
        }
        return false;
    }

    // ======== CompareGreaterType ======================================
    /**
     * CompareGreaterType compares greater than given a value.
     *
     *
     * @param w_Type the type being compared to
     * @param i_value The value being compared
     * @return True if this is greater than i_value
     */

    template <typename w_Type>
    inline bool CompareGreaterType(
        const w_Type                        & i_value
    ) const {
        if ( m_manager->m_type_info == typeid( typename AnyManager_Maker< w_Type >::t_Type ) )
        {
            return m_manager->CompareLess( & i_value, m_object );
        }

        return typeid( typename AnyManager_Maker< w_Type >::t_Type ).before( m_manager->m_type_info );
    }

    // ======== CompareLessType ======================================
    /**
     * CompareLessType compares less than given a value.
     *
     *
     * @param w_Type the type being compared to
     * @param i_value The value being compared
     * @return True if this is less than i_value
     */

    template <typename w_Type>
    inline bool CompareLessType(
        const w_Type                        & i_value
    ) const {
        if ( m_manager->m_type_info == typeid( typename AnyManager_Maker< w_Type >::t_Type ) )
        {
            return m_manager->CompareLess( m_object, & i_value );
        }

        return m_manager->m_type_info.before( typeid( typename AnyManager_Maker< w_Type >::t_Type ) );
    }

    
    // ======== ToString ==============================================
    /**
     * ToString converts the object to a string mainly for logging and
     * display purposes.
     *
     * @return The string representation of the object.
     */

    inline std::string ToString() const
    {
        return m_manager->ToString( m_object );
    }

    // ======== ToStream ==============================================
    /**
     * ToStream performs a std::ostream << on the underlying object.
     *
     * @param o_ostream The stream to perform the << on
     * @return nothing
     */

    virtual void ToStream(
        std::ostream                        & o_ostream
    ) const {
        m_manager->ToStream( o_ostream, m_object );
    }

    // ======== IsSame ================================================
    /**
     * IsSame returns true if the object passed in is of the same type
     * as the value stored in this Any object.
     *
     * @param i_value
     * @return True if i_value is of the same type as the stored value.
     */

    template <typename w_Type>
    inline bool IsSame(
        const w_Type                        & i_value
    ) {
        const AnyManager * const l_new_manager = & AnyManager_Maker< w_Type >::GetMgr();
            
        return ( m_manager->m_type_info == l_new_manager->m_type_info );
    }


    // ======== Refer ===============================================
    /**
     * When converting this Any object back to a regular type, the
     * Refer method will allow the conversion and automatically
     * determine the destination type.
     *
     * @return A converter proxy that is used to perform the actual conversion.
     */

    inline AnyConverter< w_Base > Refer()
    {
        AnyConverter< w_Base >  l_non_const( * this );
        return l_non_const;
    }

    // ======== Refer =(const)=======================================
    /**
     * When converting this Any object back to a regular type, the
     * Refer method will allow the conversion and automatically
     * determine the destination type.
     *
     * @return A converter proxy that is used to perform the actual conversion.
     */

    inline const AnyConverter< w_Base > Refer() const
    {
        AnyConverter< w_Base >  l_non_const( * const_cast< Any * >( this ) );
        return l_non_const;
    }


    // ======== ReferPtr ============================================
    /**
     * When converting this Any object back to a at::Ptr<T,T> type, the
     * ReferPtr method will allow the conversion and automatically
     * determine the destination type.
     *
     * @return A converter proxy that is used to perform the actual conversion.
     */

    inline AnyPtrConverter< w_Base > ReferPtr()
    {
        AnyPtrConverter< w_Base >  l_non_const( * this );
        return l_non_const;
    }

    // ======== ReferPtr =(const)===================================
    /**
     * When converting this Any object back to a Ptr* type, the
     * ReferPtr method will allow the conversion and automatically
     * determine the destination type.
     *
     * @return A converter proxy that is used to perform the actual conversion.
     */

    inline const AnyPtrConverter< w_Base > ReferPtr() const
    {
        AnyPtrConverter< w_Base >  l_non_const( * const_cast< Any * >( this ) );
        return l_non_const;
    }

    // ======== ReadPointer ===========================================
    /**
     * Retrieves the pointer to the Any object and checks type
     * consistency.
     *
     * @param o_pointer_to_pointer is the location to place the
     *          pointer to this object.
     * @return True if the Any object is of the same type
     */

    template <typename w_Type>
    bool ReadPointer(
        w_Type          * & o_pointer_to_pointer
    ) {
        const AnyManager * const l_new_manager = & AnyManager_Maker< w_Type >::GetMgr();

        if ( m_manager->m_type_info != l_new_manager->m_type_info )
        {
            return false;
        }

        o_pointer_to_pointer = static_cast< w_Type * >( m_object );

        return true;
    }

    // ======== ReadPointer ===========================================
    /**
     * Retrieves the pointer to the Any object and checks type
     * consistency.
     *
     * @param o_pointer_to_pointer is the location to place the
     *          pointer to this object.
     * @return True if the Any object is of the same type
     */

    template <typename w_Type>
    bool ReadPointer(
        const w_Type    * & o_pointer_to_pointer
    ) const {
        const AnyManager * const l_new_manager = & AnyManager_Maker< w_Type >::GetMgr();

        if ( m_manager->m_type_info != l_new_manager->m_type_info )
        {
            return false;
        }

        o_pointer_to_pointer = static_cast< const w_Type * >( m_object );

        return true;
    }

    // ======== MakeRef ===========================================
    /**
     * This simply makes a reference to the internal object.  Needs
     * to be referenced like MakeRef<T>().
     *
     * @return A reference to the object
     */

    template <typename w_Type>
    w_Type & MakeRef()
    {
        ConversionCheck<w_Type>();
        
        return * static_cast< w_Type * >( m_object );
    }

    template <typename w_Type>
    const w_Type & MakeRef() const
    {
        ConversionCheck<w_Type>();
        
        return * static_cast< const w_Type * >( m_object );
    }


    // ======== ~Any ==================================================
    /**
     * Any destructor - this will destroy the inner object as well.
     *
     */

    virtual ~Any()
    {
        // even if this is an empty object - the destructor here
        // is a no-op.
        m_manager->DeleteObject( m_object );
    }

    // ======== Any ===================================================
    /**
     * Any special constructor
     *
     */
    template <typename w_Type>
    Any(
        const ToAnyConvert<w_Type>      & i_toany
    )
      : m_manager( & AnyManager_Maker< w_Type >::GetMgr() ),
        m_object( m_manager->CopyObject( & i_toany.m_value ) )
    {
    }


    // ======== TypeId ================================================
    /**
     * TypeId returns the std::type_info of the object currently
     * stored within this at::Any object.
     *
     * @return std::type_info of object stored in this.
     */

    inline const std::type_info & TypeId() const
    {
        return m_manager->m_type_info;
    }
    
    
    private:

    /**
     * m_manager is the manager for the object that
     * is managed by this Any object.
     */
    const AnyManager                    * m_manager;

    private:
    
    /**
     * m_object is a pointer to the object being managed.
     */
    void                                * m_object;

};


// ======== operator== ================================================
/**
 * operator == for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side Any object
 * @return True if the objects compare as equal
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator==( const Any<w_BaseLhs> & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return i_lhs.CompareEqual( i_rhs );
}

// ======== operator== ================================================
/**
 * operator == for Any objects
 *
 * @param i_lhs the left hand side of any type
 * @param i_rhs the right hand side Any object
 * @return True if the objects compare as equal
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator==( const w_BaseLhs & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return i_rhs.CompareEqualType( i_lhs );
}

// ======== operator== ================================================
/**
 * operator == for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side of any type
 * @return True if the objects compare as equal
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator==( const Any<w_BaseLhs> & i_lhs, const w_BaseRhs & i_rhs )
{
    return i_lhs.CompareEqualType( i_rhs );
}

// ======== operator!= ================================================
/**
 * operator != for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side Any object
 * @return True if the objects compare as not equal
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator!=( const Any<w_BaseLhs> & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return ! i_lhs.CompareEqual( i_rhs );
}

// ======== operator!= ================================================
/**
 * operator != for Any objects
 *
 * @param i_lhs the left hand side of any type
 * @param i_rhs the right hand side Any object
 * @return True if the objects compare as not equal
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator!=( const w_BaseLhs & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return ! i_rhs.CompareEqualType( i_lhs );
}

// ======== operator!= ================================================
/**
 * operator != for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side of any type
 * @return True if the objects compare as not equal
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator!=( const Any<w_BaseLhs> & i_lhs, const w_BaseRhs & i_rhs )
{
    return ! i_lhs.CompareEqualType( i_rhs );
}

// ======== operator< ================================================
/**
 * operator < for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side Any object
 * @return true if i_lhs < i_rhs
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator<( const Any<w_BaseLhs> & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return i_lhs.CompareLess( i_rhs );
}

// ======== operator< ================================================
/**
 * operator < for Any objects
 *
 * @param i_lhs the left hand side of any type
 * @param i_rhs the right hand side Any object
 * @return True if i_lhs < i_rhs
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator<( const w_BaseLhs & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return i_rhs.CompareGreaterType( i_lhs );
}

// ======== operator< ================================================
/**
 * operator < for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side of any type
 * @return True if i_lhs < i_rhs
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator<( const Any<w_BaseLhs> & i_lhs, const w_BaseRhs & i_rhs )
{
    return i_lhs.CompareLessType( i_rhs );
}

// ======== operator> ================================================
/**
 * operator > for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side Any object
 * @return True if the objects compare as greater i.e. i_lhs > i_rhs
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator>( const Any<w_BaseLhs> & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return i_rhs.CompareLess( i_lhs );
}

// ======== operator> ================================================
/**
 * operator > for Any objects
 *
 * @param i_lhs the left hand side of any type
 * @param i_rhs the right hand side Any object
 * @return True if the objects compare as greater i.e. i_lhs > i_rhs
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator>( const w_BaseLhs & i_lhs, const Any<w_BaseRhs> & i_rhs )
{
    return i_rhs.CompareLessType( i_lhs );
}

// ======== operator> ================================================
/**
 * operator > for Any objects
 *
 * @param i_lhs the left hand side Any object
 * @param i_rhs the right hand side of any type
 * @return True if the objects compare as greater i.e. i_lhs > i_rhs
 */

template <typename w_BaseLhs, typename w_BaseRhs>
bool operator>( const Any<w_BaseLhs> & i_lhs, const w_BaseRhs & i_rhs )
{
    return i_lhs.CompareGreaterType( i_rhs );
}

}; // namespace at


// ======== operator<< ================================================
/**
 * This is the std::ostream::operator<< for Any objects.
 *
 *
 */

namespace std {

template<
    typename        w_char_type,
    class           w_traits,
    typename        w_Base
>                   
basic_ostream<w_char_type, w_traits>& operator << (
    basic_ostream<w_char_type, w_traits>        & o_ostream,
    const at::Any<w_Base>                       & i_value
) {
    i_value.ToStream( o_ostream );
    return o_ostream;
    
} // end basic_ostream<w_char_type, w_traits>& operator <<

} // namespace std

namespace at {

// ======== ToAnyConvert =====================================================
/**
 * This is used to construct Any objects
 *
 */

template <typename w_Type>
class ToAnyConvert
{
    const w_Type            & m_value;

    template <typename w_OtherBase>
    friend class Any;
    
    public:
    /**
     * ToAnyConvert constructor 
     *
     */
    ToAnyConvert(
        const w_Type    & i_value
    )
      : m_value( i_value )
    {
    }

};

// ======== AnyConverter ==============================================
/**
 * AnyConverter can be used to help conversion to a different type
 *
 */

template <typename w_AnyBase>
class AnyConverter
{
    public:

    friend class Any<w_AnyBase>;

    Any<w_AnyBase>            & m_any;
    
    /**
     * AnyConverter constructor for non-const 
     * converter.
     */
    inline AnyConverter(
        Any<w_AnyBase>        & i_any
    )
      : m_any( i_any )
    {
    }


    // ======== AnyConverter ==========================================
    /**
     * AnyConverter copy constructor
     *
     * @param i_rhs
     */

    AnyConverter( const AnyConverter & i_rhs )
      : m_any( i_rhs.m_any )
    {
    }


    // ======== operator w_Type =======================================
    /**
     * Conversion operator for an Any type to any other type.  If the
     * destination type is not the same as the type stored in the Any object,
     * then throw a bad conversion exception.
     *
     * @param w_Type The type to convert to.
     * @return A reference to the converted stored object.
     */

    template <typename w_Type>
    operator w_Type & ()
    {
        m_any.template ConversionCheck<w_Type>();
        
        return * static_cast< w_Type * >( m_any.m_object );
    }
    
    // ======== operator w_Type =======================================
    /**
     * Conversion operator for an Any type to any other type.  If the
     * destination type is not the same as the type stored in the Any object,
     * then throw a bad conversion exception.
     *
     * @param w_Type The type to convert to.
     * @return A reference to the converted stored object.
     */

    template <typename w_Type>
    operator const w_Type & () const
    {
        m_any.template ConversionCheck<w_Type>();
        
        return * static_cast< const w_Type * >( m_any.m_object );
    }


    // ======== operator= ============================================
    /**
     * Assignment operator - assign this Any object from a non Any
     * object.  This allows syntax like : Any<>.Refer() = 2
     *
     * @param i_value is the object being assigned to this
     * @return Reference to the assigned object.
     */

    template <typename w_Type>
    w_Type & operator= (
        const w_Type                        & i_value
    )
    {
        m_any.Assign( i_value );
        return * static_cast< w_Type * >( m_any.m_object );
    }
};



// ======== AnyPtrConverter ==============================================
/**
 * AnyPtrConverter can be used to help conversion to a different type
 *
 */

template <typename w_AnyBase>
class AnyPtrConverter
{
    public:

    friend class Any<w_AnyBase>;

    Any<w_AnyBase>            & m_any;
    
    /**
     * AnyPtrConverter constructor for non-const 
     * converter.
     */
    inline AnyPtrConverter(
        Any<w_AnyBase>        & i_any
    )
      : m_any( i_any )
    {
    }


    // ======== AnyPtrConverter ==========================================
    /**
     * AnyPtrConverter copy constructor
     *
     * @param i_rhs
     */

    AnyPtrConverter( const AnyPtrConverter & i_rhs )
      : m_any( i_rhs.m_any )
    {
    }


    // ======== operator Ptr< w_Type, w_Traits > ======================
    /**
     * Conversion operator for an Any type to a Ptr type.  If the
     * destination type is not the same as the type stored in the Any object,
     * then throw a bad conversion exception.
     *
     * @param w_Type The type to convert to.
     * @return A reference to the converted value.
     */

    template <typename w_Type, typename w_Traits >
    operator Ptr< w_Type, w_Traits > & ()
    {
        m_any.template ConversionCheck< Ptr< w_Type, w_Traits > >();
        
        return * static_cast< Ptr< w_Type, w_Traits > * >( m_any.m_object );
    }
    
    // ======== operator PtrView< w_Type, w_Traits > ==================
    /**
     * Conversion operator for an Any type to a PtrView type.  If the
     * destination type is not the same as the type stored in the Any object,
     * then throw a bad conversion exception.
     *
     * @param w_Type The type to convert to.
     * @return The converted value.
     */

    template <typename w_Type, typename w_Traits >
    operator const PtrView< w_Type, w_Traits > ()
    {
        m_any.template ConversionCheck< Ptr< w_Type, w_Traits > >();

        Ptr< w_Type, w_Traits > & l_ptr = * static_cast< Ptr< w_Type, w_Traits > * >( m_any.m_object );
        
        return l_ptr;
    }
    
    // ======== operator w_Type =======================================
    /**
     * Conversion operator for an Any type to a PtrDelegate type.  If the
     * destination type is not the same as the type stored in the Any object,
     * then throw a bad conversion exception.
     *
     * @param w_Type The type to convert to.
     * @return A reference to the Any object.
     */

    template <typename w_Type, typename w_Traits >
    operator const PtrDelegate< w_Type, w_Traits > ()
    {
        m_any.template ConversionCheck< Ptr< w_Type, w_Traits > >();
        
        Ptr< w_Type, w_Traits > & l_ptr = * static_cast< Ptr< w_Type, w_Traits > * >( m_any.m_object );
        
        return l_ptr;
    }
    
    // ======== operator w_Type =======================================
    /**
     * Conversion operator for an Any type to any other type.  If the
     * destination type is not the same as the type stored in the Any object,
     * then throw a bad conversion exception.
     *
     * @param w_Type The type to convert to.
     * @return A reference to the Any object.
     */

    template <typename w_Type, typename w_Traits >
    operator const Ptr< w_Type, w_Traits > & () const
    {
        m_any.template ConversionCheck< Ptr< w_Type, w_Traits > >();
        
        return * static_cast< const Ptr< w_Type, w_Traits > * >( m_any.m_object );
    }

    // ======== operator= ============================================
    /**
     * Assignment operator - assign this an at::Ptr object
     * object.
     *
     * @param i_value is the object being assigned to this
     * @return Reference to the assigned object.
     */

    template <typename w_Type, typename w_Traits >
    Ptr< w_Type, w_Traits > & operator= (
        const Ptr< w_Type, w_Traits >           & i_value
    )
    {
        m_any.Assign( i_value );
        return * static_cast< Ptr< w_Type, w_Traits > * >( m_any.m_object );
    }

    // ======== operator= ============================================
    /**
     * Assignment operator - assign this an at::PtrView object
     * object.
     *
     * @param i_value is the object being assigned to this
     * @return Reference to the assigned object.
     */

    template <typename w_Type, typename w_Traits >
    Ptr< w_Type, w_Traits > & operator= (
        const PtrView< w_Type, w_Traits >           & i_value
    )
    {
        m_any.Assign( i_value );
        return * static_cast< Ptr< w_Type, w_Traits > * >( m_any.m_object );
    }

    // ======== operator= ============================================
    /**
     * Assignment operator - assign this an at::PtrDelegate object
     * object.
     *
     * @param i_value is the object being assigned to this
     * @return Reference to the assigned object.
     */

    template <typename w_Type, typename w_Traits >
    Ptr< w_Type, w_Traits > & operator= (
        const PtrDelegate< w_Type, w_Traits >           & i_value
    )
    {
        m_any.Assign( i_value );
        return * static_cast< Ptr< w_Type, w_Traits > * >( m_any.m_object );
    }

};


// ======== AnyReference ==============================================
/**
 * Anyreference allows taking a reference of a regular object and pass
 * it to a generic routine to perform assignment in a type free context.
 * This will only work when the types being assigned are the same.
 */

class AnyReference
{
    public:

    /**
     * AnyReference is contructed with a reference from any non const type.
     *
     */
    template <typename w_Type>
    AnyReference(
        w_Type                      & i_value
    )
      : m_manager( AnyManager_Maker< w_Type >::GetMgr() ),
        m_object( & i_value )
    {
    }

    /**
     * AnyReference is contructed with an Any object
     *
     */
    template <typename w_OtherBase>
    AnyReference(
        Any<w_OtherBase>           & i_any
    )
      : m_manager( * i_any.m_manager ),
        m_object( i_any.m_object )
    {
    }

    /**
     * const references are not allowed.
     */
    template <typename w_type>
    AnyReference(
        const w_type                & i_value
    )
    {
        AT_StaticAssert( sizeof( *this ), ReferenceMustBeNonConst );
    }



    // ======== operator= ==============================================
    /**
     * Assignment operator
     *
     * @param i_rhs assignment
     * @return nothing
     */

    AnyReference & operator=( const AnyReference & i_rhs )
    {
        // make sure the manager types are the same
        if ( & m_manager != & i_rhs.m_manager )
        {
            Any_ThrowConversionFailure();
        }

        m_manager.AssignObject( m_object, i_rhs.m_object );
        return * this;
    }
    
    // ======== operator= ==============================================
    /**
     * Assignment operator
     *
     * @param i_rhs assignment
     * @return nothing
     */

    template <typename w_OtherBase>
    AnyReference & operator=(
        const Any<w_OtherBase>           & i_any
    )
    {
        // make sure the manager types are the same
        if ( & m_manager != i_any.m_manager )
        {
            Any_ThrowConversionFailure();
        }

        m_manager.AssignObject( m_object, i_any.m_object );
        return * this;
    }
    

    private:
    
    /**
     * This contains the information related to this reference
     */
    const AnyManager                    & m_manager;
    
    /**
     * m_value_ptr is a pointer to the value to be assigned
     */
    void * const                          m_object;

};



// ======== ToAny =======================================================
/**
 * ToAny is a function that peforms conversion to a ToAnyConvert which
 * is constructible or assignable to an Any type.
 *
 * @param i_value Is the value being converted to a ToAny<> type
 * @return nothing
 */

template <typename w_Type>
inline const ToAnyConvert< w_Type > ToAny( const w_Type & i_value )
{
    return ToAnyConvert< w_Type >( i_value );
}

/** @} */ // end of AustriaAny ( Doxygen group )


}; // namespace

#endif // x_at_any_h_x




