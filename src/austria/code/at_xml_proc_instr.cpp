//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_proc_instr.cpp
 *
 * \author Guido Gherardi
 *
 */

#include "at_xml_proc_instr.h"

// Austria namespace
namespace at
{
    std::string XmlProcessingInstruction::Name() const
    {
        return Target();
    }

    std::string XmlProcessingInstruction::Value() const
    {
        return Data();
    }

    unsigned short XmlProcessingInstruction::Type() const
    {
        return PROCESSING_INSTRUCTION_NODE;
    }

    PtrDelegate< XmlNode* > XmlProcessingInstruction::Clone( bool i_deep ) const
    {
        PtrDelegate< XmlProcessingInstruction* > l_copy (
            new XmlProcessingInstruction( OwnerDocument(), Target(), Data() )
            );
        return l_copy;
    }

    void XmlProcessingInstruction::Accept( XmlNodeVisitor& i_op )
    {
        i_op.VisitProcessingInstruction( this );
    }

    void XmlProcessingInstruction::Accept( XmlNodeVisitor& i_op ) const
    {
        i_op.VisitProcessingInstruction( this );
    }
};
