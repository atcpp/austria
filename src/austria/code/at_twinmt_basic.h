//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//      http://www.gnu.org/copyleft/lesser.txt
// 


/**
 *  at_twinmt_basic.h
 *
 */


#ifndef x_at_twinmt_basic_h_x
#define x_at_twinmt_basic_h_x 1


#include "at_exports.h"
#include "at_twinmt.h"
#include "at_assert.h"
#include "at_exception.h"

// Austria namespace
namespace at
{



/**
 *  This is the "Twin" basic implementation for multi-threaded use.
 *
 */



// ======== DeleteMarkerManager =======================================
/**
 * DeleteMarkerManager little class to manage the deleted status.
 *
 */

struct DeleteMarkerManager
{
    public:

    // reference to location
    bool                           * ( & m_delete_marker );

    bool                                 m_is_deleted;
    /**
     * DeleteMarkerManager
     *
     */
    DeleteMarkerManager(
        bool                           * ( & i_delete_marker )
    )
      : m_delete_marker( i_delete_marker ),
        m_is_deleted( false )
    {
        AT_Assert( 0 == m_delete_marker );
        m_delete_marker = & m_is_deleted;
    }

    ~DeleteMarkerManager()
    {
        if ( ! m_is_deleted )
        {
            AT_Assert( & m_is_deleted == m_delete_marker );
            m_delete_marker = 0;
        }
    }

    operator bool()
    {
        return m_is_deleted;
    }

    private:
    DeleteMarkerManager( const DeleteMarkerManager & );
    DeleteMarkerManager & operator=( const DeleteMarkerManager & );
};


// ======== LockingCaller =============================================
/**
 *  LockingCaller is a proxy class used to ensure the locking of the
 *  common aide/lead mutex before performing a function call.
 */
template< class w_type >
class LockingCaller
{
private:
    w_type                  & m_caller; // ref to caller

    // not implemented
    LockingCaller & operator=(const LockingCaller& doesNotAssign);

public:

    // take the lock on construction -- typically this is a compiler temporary object
    // as in lead.CallAide().VoidCall(&Aide::foo,arg1)
    // it is not intended for a caller to hold a "LockingCaller" object for very long
    LockingCaller( w_type  &i_lead_or_aide )
      : m_caller(i_lead_or_aide)
    {
        AT_Assert(&m_caller);
    }

    LockingCaller(const LockingCaller& orig)
      : m_caller( orig.m_caller )
    {
    }

    ~LockingCaller()
    {
    }

    // ======== ReturnCall ============================================
    /**
     *  ReturnCall will call a function and take the lock - ReturnCall
     *  will take the return value and store it to the first parameter.
     *  ReturnCall returns true if the call was performed, otherwise
     *  false (currently not associated).
     *
     *  @param o_retval The place to write the return value.
     *  @return True if the call is performed (twin is associated), false
     *          if there is no association.
     */
    template<
        typename w_return_type,
        typename w_pointer_to_member
    >
    bool ReturnCall(
        w_return_type  & o_retval,
        w_pointer_to_member  i_func
    )
    {
        
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        o_retval = ( m_caller.m_twin->* i_func )();
        return true;
    }

    template<
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1
    >
    bool ReturnCall(
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        o_retval = ( m_caller.m_twin->* i_func )( i_param1 );
        return true;
    }

    template<
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2
    >
    bool ReturnCall(
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );
        
        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        o_retval = ( m_caller.m_twin->* i_func )( i_param1, i_param2 );
        return true;
    }

    template<
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3
    >
    bool ReturnCall(
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );
        
        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        o_retval = ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3 );
        return true;
    }

    template<
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4
    >
    bool ReturnCall(
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );
        
        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        o_retval = ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3, i_param4 );
        return true;
    }

    // ======== VoidCall ==============================================
    /**
     *  VoidCall will call a function and take the lock and nothing is
     *  done with the return value.
     *
     *  @return True if the call is performed (twin is associated), false
     *          if there is no association.
     */
    template< typename w_pointer_to_member >
    bool VoidCall( w_pointer_to_member  i_func )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );
        
        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )();
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );
        
        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )( i_param1 );
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )( i_param1, i_param2 );
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3 );
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3, i_param4 );
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4,
        typename w_param5
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4,
        w_param5  & i_param5
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3, i_param4, i_param5 );
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4,
        typename w_param5,
        typename w_param6
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4,
        w_param5  & i_param5,
        w_param6  & i_param6
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3, i_param4, i_param5, i_param6 );
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4,
        typename w_param5,
        typename w_param6,
        typename w_param7
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4,
        w_param5  & i_param5,
        w_param6  & i_param6,
        w_param7  & i_param7
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3, i_param4, i_param5, i_param6, i_param7 );
        return true;
    }

    template<
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4,
        typename w_param5,
        typename w_param6,
        typename w_param7,
        typename w_param8
    >
    bool VoidCall(
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4,
        w_param5  & i_param5,
        w_param6  & i_param6,
        w_param7  & i_param7,
        w_param8  & i_param8
    )
    {
        ThreadSafePtrTryLock< typename w_type::t_OtherType * >  l_lock( m_caller.m_mutexPtr, m_caller.m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return false;
        }
        
        if ( ! m_caller.m_twin )
        {
            return false;
        }
        ( m_caller.m_twin->* i_func )(
                i_param1, i_param2, i_param3, i_param4,
                i_param5, i_param6, i_param7, i_param8
        );
        return true;
    }

    // ======== ReturnCallRMutex ============================================
    /**
     *  ReturnCallRMutex will call a function and swap the lock with the one
     *  specified by the caller - ReturnCallRMutex
     *  will take the return value and store it to the first parameter
     *  after the caller's mutex
     *  ReturnCall returns true if the call was performed, otherwise
     *  false.
     *
     *  @param i_mutex
     *  @param o_retval The place to write the return value.
     *  @return True if the call is performed (twin is associated), false
     *          if there is no association.
     */
    template<
        typename w_mutex_type,
        typename w_return_type,
        typename w_pointer_to_member
    >
    bool ReturnCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_return_type  & o_retval,
        w_pointer_to_member  i_func
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            o_retval = ( m_caller.m_twin->* i_func )();
            return false;
        }

        return true;
    }
 
    template<
        typename w_mutex_type,
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1
    >
    bool ReturnCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            o_retval = ( m_caller.m_twin->* i_func )( i_param1 );
            return false;
        }

        return true;
    }
 
    template<
        typename w_mutex_type,
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2
    >
    bool ReturnCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            o_retval = ( m_caller.m_twin->* i_func )( i_param1, i_param2 );
            return false;
        }

        return true;
    }
 
    template<
        typename w_mutex_type,
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3
    >
    bool ReturnCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            o_retval = ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3 );
            return false;
        }

        return true;
    }
 
    template<
        typename w_mutex_type,
        typename w_return_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4
    >
    bool ReturnCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_return_type  & o_retval,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            o_retval = ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3, i_param4 );
            return false;
        }

        return true;
    }
 
    // ======== VoidCallRMutex ==============================================
    /**
     *  VoidCallRMutex will call a function and swap the lock with one specified
     *  by the caller
     *  VoidCallRMutex returns true if the call was performed, otherwise
     *  false.
     *
     *  @param i_mutex
     *  @return True if the call is performed (twin is associated), false
     *          if there is no association.
     */
    template<
        typename w_mutex_type,
        typename w_pointer_to_member
    >
    bool VoidCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_pointer_to_member  i_func
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            ( m_caller.m_twin->* i_func )();
            return false;
        }

        return true;

    }
 
    template<
        typename w_mutex_type,
        typename w_pointer_to_member,
        typename w_param1
    >
    bool VoidCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_pointer_to_member  i_func,
        w_param1  & i_param1
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            ( m_caller.m_twin->* i_func )( i_param1);
            return false;
        }

        return true;
    }
 
    template<
        typename w_mutex_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2
    >
    bool VoidCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            ( m_caller.m_twin->* i_func )( i_param1, i_param2 );
            return false;
        }

        return true;
    }
 
    template<
        typename w_mutex_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3
    >
    bool VoidCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3 );
            return false;
        }

        return true;
    }
 
    template<
        typename w_mutex_type,
        typename w_pointer_to_member,
        typename w_param1,
        typename w_param2,
        typename w_param3,
        typename w_param4
    >
    bool VoidCallRMutex(
        w_mutex_type   & i_caller_mutex,
        w_pointer_to_member  i_func,
        w_param1  & i_param1,
        w_param2  & i_param2,
        w_param3  & i_param3,
        w_param4  & i_param4
    )
    {
        ThreadSafePtrSwapLock<w_mutex_type> l_lock( i_caller_mutex, m_caller.m_mutexPtr );
        
        if ( m_caller.m_twin ) {
            ( m_caller.m_twin->* i_func )( i_param1, i_param2, i_param3, i_param4 );
            return false;
        }

        return true;
    }
};

// ======== AppLeadTwinMT_EmptyTraits =================================
/**
 *  AppLeadTwinMT_EmptyTraits is an application end of a Lead.  The
 *  application will provide various method of notification.
 */
template<
    class w_aide_twin_interface,
    class w_lead_interface
>
class AppLeadTwinMT_EmptyTraits
{

  public:

    typedef
        LeadTwinMT< w_aide_twin_interface, w_lead_interface >
            t_LeadTwinInterface;


    // ======== AppLeadCompletedNotify ================================
    /**
     *  AppLeadCompletedNotify does nothing in this version.
     *
     *  @param i_lead
     *  @return nothing
     */
    static inline void AppLeadCompletedNotify( t_LeadTwinInterface * i_lead )
    {

        //
        // DO NOTHING HERE : the EmptyTraits version does not
        // inform an application.
        //

    } // end AppLeadCompletedNotify

    // ======== AppLeadAssociatedNotify ================================
    /**
     *  AppLeadAssociatedNotify does nothing in this version.
     *
     *  @param i_lead
     *  @return nothing
     */
    static inline void AppLeadAssociatedNotify( t_LeadTwinInterface * i_lead )
    {

        //
        // DO NOTHING HERE : the EmptyTraits version does not
        // inform an application.
        //

    } // end AppLeadAssociatedNotify

};


// ======== AppLeadTwinMT_ForwardTraits ===============================
/**
 *  AppLeadTwinMT_ForwardTraits is an application end of a Lead.  The
 *  application is provided various method of notification.
 */
template<
    typename w_aide_twin_interface,
    typename w_lead_interface
>
class AppLeadTwinMT_ForwardTraits
{

  public:

    // ======== t_LeadTwinInterface ===================================
    /**
     *  t_LeadTwinInterface supplies the interface for a
     *  LeadTwinMT_Basic to manage the notification to the application
     *  of various events like LeadCompleted events.
     */
    class t_LeadTwinInterface
      : public LeadTwinMT< w_lead_interface, w_aide_twin_interface >
    {

      public:

        // ======== AppLeadCompleted ==================================
        /**
         *  AppLeadCompleted is provided by the application to get
         *  notified when the associated interface completes.
         *
         *  @param i_completion_code indicates the completion reason
         *  @return nothing
         */
        virtual void AppLeadCompleted(
            TwinTraits::TwinCode i_completion_code
        )
        {
        }


        // ======== AppLeadAssociated =================================
        /**
         * AppLeadAssociated can be provided by the application to
         * get notified when a lead is associated.
         *
         * @return nothing
         */

        virtual void AppLeadAssociated()
        {
        }

        
    };

    // ======== AppLeadCompletedNotify ================================
    /**
     *  This method is called by LeadCompleted to perform application
     *  notification.
     *
     *  @param i_lead
     *  @return nothing
     */
    static inline void AppLeadCompletedNotify(
        t_LeadTwinInterface  * i_lead,
        TwinTraits::TwinCode  i_completion_code
    )
    {

        /*  WARNING! - AppLeadCompleted is allowed to delete the lead
         *  object under certain circumstances.  We must not access the
         *  lead object after this call.
         */
        i_lead->AppLeadCompleted( i_completion_code );

    } // end AppLeadCompletedNotify

    // ======== AppLeadAssociatedNotify ================================
    /**
     *  AppLeadAssociatedNotify does nothing in this version.
     *
     *  @param i_lead
     *  @return nothing
     */
    static inline void AppLeadAssociatedNotify( t_LeadTwinInterface * i_lead )
    {

        /*  WARNING! - AppLeadAssociated is allowed to delete the lead
         *  object under certain circumstances.  We must not access the
         *  lead object after this call.
         */
        i_lead->AppLeadAssociated();

    } // end AppLeadAssociatedNotify

};


// ======== LeadTwinMT_Basic ==========================================
/**
 *  LeadTwinMT_Basic provides a basic implementation of the LeadTwinMT
 *  interface.
 */
template<
    class w_lead_interface,
    class w_aide_twin_interface = AideTwinMT< TwinMTNullAide >,
    class w_app_lead_twin_interface =
        AppLeadTwinMT_ForwardTraits<
            w_aide_twin_interface,
            w_lead_interface
        >
>
class LeadTwinMT_Basic
  : public w_app_lead_twin_interface::t_LeadTwinInterface
{
public:
    /**
     *  The type of the aide pointer (m_twin).
     */
    typedef  w_aide_twin_interface *  t_AidePointer;

    /**
     *  The type of the aide, from the point of view of the lead.
     *  (The real type of the aide object will probably be something
     *  derived from this.)
     */
    typedef  w_aide_twin_interface  t_OtherType;

    /**
     *  m_mutexPtr is a smart pointer to the reference-counted,
     *  *recursive* mutex shared between the lead and the aide.  This
     *  mutex:
     *
     *      1) Protects (serializes access to) the m_twin members of
     *         the lead and the aide
     *
     *      2) Atomicizes the breaking of the link, and
     *
     *      3) Prevents LeadCancel and AideCancel from returning before
     *         all method calls across the link have completed.
     *
     *  m_mutexPtr is a thread safe smart pointer.  This meads that the
     *  only valid operation is to copy the value to another smart
     *  pointer.  Once you have it locked, then you need to check
     *  that this is still the current pointer.  ThreadSafePtrTryLock
     *  is the locking (RAII) class that performs this operation.
     *
     *  Note that there may be additional pointers to the mutex in
     *  addition to m_mutex in the lead and aide, through which the
     *  mutex may also get locked.  In particular, multiple lead-aide
     *  pairs may all share the same mutex.  It may be necessary to
     *  take avantage of this in some cases, in order to avoid
     *  deadlocks, or to ensure that a thread that accesses an object
     *  through a lead-twin interface has exclusive access to that
     *  object.  However this opens up additional potential for even
     *  more deadlocks, so this technique should be used only with
     *  great care.
     *
     *  To avoid deadlocks, you can use the Activity mechanism to
     *  register an "action" to be called from a pool thread.
     *
     */
    private:
    ThreadSafeMutexPtr     m_mutexPtr;

    // ======== GetMutex ==============================================
    /**
     * GetMutex returns a copy of the mutex pointer.
     *
     *
     * @return A copy of the mutex pointer
     */

    public:
    inline at::PtrDelegate<MutexRefCount *> GetMutex() const
    {
        return m_mutexPtr;
    }

    
     /**
     *  m_twin is the lead object's pointer to the aide object.
     *  This will be null before AideAssociate is called, and again
     *  after the association is broken by a call to LeadCancel or
     *  AideCancel.  Whenever it's non-null, the association is
     *  considered to be active.
     *
     *  The mutex pointed to by m_mutex must be locked before accessing
     *  m_twin for either reading or writing.
     *  When m_twin is zero-ed out to break the
     *  association, the aide object's m_twin must also be zero-ed
     *  out in the same critical section, in order to make the breaking
     *  of the link an atomic operation.
     */
    private:
    volatile t_AidePointer  m_twin;

    /**
     * m_delete_marker is used to manage the clean destruction
     * of a twin object.
     */
    bool            * m_delete_marker;

protected:
    // ======== LeadAssociate =========================================
    /**
     *  This is LeadTwinMT_Basic's implemetation of LeadAssociate,
     *  which is a pure virtual method in LeadTwinMT.  It should not be
     *  overridden by classes deriving from LeadTwinMT_Basic unless
     *  they call back to this implementation prior to communicating
     *  with the Aide. It should only be called by the aide in
     *  AideAssociate, via the wrapper method WrapLeadAssociate.
     *
     *  @param i_aide is the aide to associate with
     *  @param i_mutex Mutex to protect this association.  THIS MUTEX MUST BE RECURSIVE.
     *  @return nothing
     */
    virtual bool LeadAssociate(
        w_aide_twin_interface               * i_aide,
        PtrView< MutexRefCount * >            i_mutex
    )
    {
        AT_Assert( i_aide != 0 );
        AT_Assert( i_mutex != 0 );

        Ptr< MutexRefCount * >                l_mutex( i_mutex );

        do {
            if ( m_twin )
            {
                LeadCancel();
            }
    
            // acquire the lock before setting the member variable
            // to assure we have exclusive access to the member variables
            // If this deadlocks here it means that you have a problem
            // with your design.
            {
                Lock<Mutex> l_lock(*l_mutex);
    
                // do an atomic compare exchange
                if ( 0 == m_mutexPtr.SwapPtrCompare( l_mutex, 0 ) )
                {
                    // This should now be impossible.
                    AT_Assert( m_twin == 0 );
                    m_twin = i_aide;

                    // lead has been associated so we can now notify the
                    // application
                    w_app_lead_twin_interface::AppLeadAssociatedNotify( this );

                    break;
                }
            }
            OSTraitsBase::SchedulerYield();
        } while ( 1 );

        return true;
    }

public:
    // ======== LeadCancel ============================================
    /**
     *  This is LeadTwinMT_Basic's implemetation of LeadCancel, which
     *  is a pure virtual method in LeadTwinMT.  It should not be
     *  overridden by classes deriving from LeadTwinMT_Basic.
     *
     *  LeadCancel is called by anyone to break the connection between
     *  the lead and the aide
     *
     *  DESIGN PHILOSOPHY -- the link can be broken AT ANY TIME
     *  by ANYONE, and clients must deal with it.  They must invalidate
     *  or revalidate all pointers to the other side of the link
     *  CallAide/CallLead do this for clients automatically,
     *  and reflect calls to broken links with an error status, and they
     *  are the only sanctioned means of calling across the link
     *
     *  LeadCancel is thread-safe, and can be called multiple times on
     *  the same lead, serially or concurrently, without conflict.
     *
     *  It is permitted for a thread to call LeadCancel while already
     *  holding a lock on the shared mutex.  This is necessary for
     *  certain techniques in which multiple lead-aide pairs share the
     *  same mutex.
     *
     *  @return nothing
     */
    virtual void LeadCancel()
    {
        ThreadSafePtrTryLock< t_AidePointer >  l_lock( m_mutexPtr, m_twin );
        
        if ( ! l_lock.IsAcquired() )
        {
            return;
        }

        // make sure cancel is not already in progress.
        if ( 0 != m_delete_marker )
        {
            return;
        }
        
        DeleteMarkerManager l_is_deleted( m_delete_marker );
        
        t_AidePointer l_pointer = m_twin;

        if ( l_pointer )
        {
            m_twin = 0;
            // cancellation notification
            l_pointer->AideCancelPrivate();
            w_app_lead_twin_interface::AppLeadCompletedNotify( this, TwinTraits::CancelRequested );
            
            if ( ( ! l_is_deleted ) && l_lock.IsAcquired() )
            {
                l_lock.ReplaceMutex( 0 );
            }
        }

    }

    // MUST ONLY BE CALLED BY AideCancel
    //
    virtual void LeadCancelPrivate()
    {
        // The m_mutexPtr is assumed to be locked by the aide
        
        DeleteMarkerManager l_is_deleted( m_delete_marker );
        
        m_twin = 0;
        w_app_lead_twin_interface::AppLeadCompletedNotify( this, TwinTraits::CancelRequested );
        
        if ( ! l_is_deleted )
        {
            m_mutexPtr = 0;
        }
    }


    /*  GetLeadRequireLock can be used when the lead or aide
     *  require to call multiple methods whith the mutex locked.
     *  This allows the lead implementation to get access to
     *  the aide.  The mutex MUST be locked before calling
     *  GetLeadRequireLock.
     */
    inline void GetAideRequireLock( t_AidePointer & o_aide_ptr )
    {
        o_aide_ptr = m_twin;

    } // end GetAideRequireLock


    friend class LockingCaller< LeadTwinMT_Basic >;

    // ======== CallAide ==============================================
    /**
     *  CallAide returns a proxy object that can be used to make one or
     *  more thread-safe, serialized accesses to the aide object
     *  (methods and/or data members in the interface class).  In
     *  particular, the accesses are serialized with LeadCancel and
     *  AideCancel, so that if an access succeeds (i.e. the association
     *  is still active when the access begins), the aide is guaranteed
     *  not to destroy itself before the access is complete.  (If the
     *  association has already been broken before the access attempt,
     *  then the attempt fails.)
     *
     *  The accesses are incidentally also serialized with other
     *  accesses made using proxy objects returned by either CallLead
     *  or CallAide on the same lead-aide pair.  Significantly, the
     *  accesses are *not* serialized with operations performed on the
     *  aide object by other threads, that do not go through the
     *  at_twinmt interface.  (In other words, a thread that
     *  successfully accesses the aide object through a proxy returned
     *  by CallAide should not assume it has exclusive access to the
     *  aide object, unless that is guaranteed by other means.)
     *  Protecting against conflicts with operations that do not go
     *  through the at_twinmt interface is beyond the scope of
     *  at_twinmt.
     *
     *  All calls to CallAide must finish before the LeadTwinMT_Basic destructor is called.
     *
     *  @return A LockingCaller object which can be used to access the aide from the lead.
     */
    inline LockingCaller< LeadTwinMT_Basic > CallAide()
    {
        AT_Assert(this);
        return LockingCaller< LeadTwinMT_Basic >( *this );
    }


    // ======== LeadTwinMT_Basic ======================================
    /**
     *  LeadTwinMT_Basic constructor.
     */
    inline LeadTwinMT_Basic() :
        m_twin(),
        m_delete_marker()
    {
    }


    // ======== ~LeadTwinMT_Basic =====================================
    /**
     *  LeadTwinMT_Basic destructor.
     *
     *  should ONLY ever be called by a managed ref-counted Ptr object.
     *  Do not ever call destructor yourself or even "delete" on the
     *  object since those mechanisms bypass all safe MT design principles.
     */
    inline ~LeadTwinMT_Basic()
    {
        if ( m_delete_marker )
        {
            * m_delete_marker = true;
            return;
        }
        
        // we MUST call cancel, it is the only way to inform the
        // aide of our premature disappearance during exception handling.
        LeadCancel();
    }

private:
    /*  Unimplemented.  */
    LeadTwinMT_Basic( const LeadTwinMT_Basic & );
    LeadTwinMT_Basic & operator=( const LeadTwinMT_Basic & );
};


// ======== AppAideTwinMTTraits_EmptyTraits ===========================
/**
 *  AppAideTwinMTTraits_EmptyTraits provides no new interface points.
 */
template< typename w_aide_twin_interface >
class AppAideTwinMTTraits_EmptyTraits
{

  public:

    typedef w_aide_twin_interface  t_AideTwinInterface;


    // ======== AppAideCloseNotify_Internal ===========================
    /**
     *  AppAideCloseNotify performs notification of the AideTwinMT
     *  application layer.
     *
     *  @return nothing
     */
    inline static void AppAideCloseNotify_Internal(
        t_AideTwinInterface  * i_aide_ptr,
        TwinTraits::TwinCode  i_completion_code
    )
    {

        //
        // EmptyTraits does no notifications
        //

        return;

    } // end AppAideCloseNotify_Internal


};


// ======== AppAideTwinMTTraits_ForwardTraits =========================
/**
 *  AppAideTwinMTTraits_ForwardTraits provides no new interface points.
 */
template< typename w_aide_twin_interface >
class AppAideTwinMTTraits_ForwardTraits
{

  public:

    // ======== t_AideTwinInterface ===================================
    /**
     *  This adds a few more methods to the aide interface
     *  implementation.
     *
     *
     */
    class t_AideTwinInterface
      : public w_aide_twin_interface
    {

      public:

        // ======== AppAideCloseNotify ================================
        /**
         *  AppAideCloseNotify is overridden by the deriving classes
         *  and will notify the closing of the Aide.
         *
         *  @param i_completion_code is the completion code.
         *  @return nothing
         */
        virtual void AppAideCloseNotify(
            TwinTraits::TwinCode  i_completion_code
        )
        {
        }

    };

    // ======== AppAideCloseNotify_Internal ===========================
    /**
     *  AppAideCloseNotify performs notification of the AideTwinMT
     *  application layer.
     *
     *  @param i_completion_code is the completion code.
     *  @param i_aide_ptr is the interface this this
     *  @return nothing
     */
    inline static void AppAideCloseNotify_Internal(
        t_AideTwinInterface  * i_aide_ptr,
        TwinTraits::TwinCode  i_completion_code
    )
    {

        /*  WARNING! - AppAideCloseNotify is allowed to delete the aide
         *  object under certain circumstances.  We must not access the
         *  aide object after this call.
         */
        i_aide_ptr->AppAideCloseNotify( i_completion_code );

    } // end AppAideCloseNotify_Internal

};


// ======== AideTwinMT_Basic ==========================================
/**
 *  AideTwinMT_Basic implements a basic version of the AideTwinMT part
 *  of the system.
 */
template<
    class w_lead_interface,
    class w_aide_interface = TwinMTNullAide,
    class w_app_aide_twin_traits =
        AppAideTwinMTTraits_ForwardTraits< AideTwinMT< w_aide_interface > >,
    class w_lead_twin_interface =
        LeadTwinMT<
            w_lead_interface,
            AideTwinMT< w_aide_interface >
        >
>
class AideTwinMT_Basic
  : public w_app_aide_twin_traits::t_AideTwinInterface
{
public:
    /**
     *  The type of the lead pointer (m_twin).
     */
    typedef  w_lead_twin_interface *  t_LeadPointer;

    /**
     *  The type of the lead, from the point of view of the aide.
     *  (The real type of the aide object will probably be something
     *  derived from this.)
     */
    typedef  w_lead_twin_interface  t_OtherType;

    /**
     *  m_mutexPtr is a smart pointer to the reference-counted,
     *  *recursive* mutex shared between the lead and the aide.  This
     *  mutex:
     *
     *      1) Protects (serializes access to) the m_twin members of
     *         the lead and the aide
     *
     *      2) Atomicizes the breaking of the link, and
     *
     *      3) Prevents LeadCancel and AideCancel from returning before
     *         all method calls across the link have completed.
     *
     *  m_mutexPtr is a thread safe smart pointer.  This meads that the
     *  only valid operation is to copy the value to another smart
     *  pointer.  Once you have it locked, then you need to check
     *  that this is still the current pointer.
     *
     *  Note that there may be additional pointers to the mutex in
     *  addition to m_mutex in the lead and aide, through which the
     *  mutex may also get locked.  In particular, multiple lead-aide
     *  pairs may all share the same mutex.  It may be necessary to
     *  take avantage of this in some cases, in order to avoid
     *  deadlocks, or to ensure that a thread that accesses an object
     *  through a lead-twin interface has exclusive access to that
     *  object.  However this opens up additional potential for even
     *  more deadlocks, so this technique should be used only with
     *  great care.
     *
     */
    private:
    ThreadSafeMutexPtr     m_mutexPtr;

    // ======== GetMutex ==============================================
    /**
     * GetMutex returns a copy of the mutex pointer.
     *
     *
     * @return A copy of the mutex pointer
     */

    public:
    inline at::PtrDelegate<MutexRefCount *> GetMutex() const
    {
        return m_mutexPtr;
    }

    /**
     *  m_twin is the lead object's pointer to the aide object.
     *  This will be null before AideAssociate is called, and again
     *  after the association is broken by a call to LeadCancel or
     *  AideCancel.  Whenever it's non-null, the association is
     *  considered to be active.
     *
     *  The mutex pointed to by m_mutex must be locked before accessing
     *  m_twin for either reading or writing.
     *  When m_twin is zero-ed out to break the
     *  association, the aide object's m_twin must also be zero-ed
     *  out in the same critical section, in order to make the breaking
     *  of the link an atomic operation.
     */
    private:
    volatile t_LeadPointer  m_twin;


    /**
     * m_delete_marker is used to manage the clean destruction
     * of a twin object.
     */
    bool            * m_delete_marker;

public:
    // ======== AideAssociate =========================================
    /**
     *  AideAssociate is called by the aide application layer to
     *  associate an aide with a lead.
     *
     *  AideAssociate is *NOT* threadsafe!  The thread calling
     *  AideAssociate must have exclusive access to both the aide.
     *
     *  The user of AideAssociate must provide it with a reference-
     *  counted, *recursive* mutex, which will be used to make
     *  subsequent operations on the lead and aide thread-safe.
     *
     *  @param i_lead is the lead to associate with
     *  @param i_mutex Mutex to protect this association.  THIS MUTEX MUST BE RECURSIVE.
     *  @return nothing
     */
    void AideAssociate(
        t_LeadPointer  i_lead,
        Ptr< MutexRefCount * >  i_mutex
    )
    {
        AT_Assert( i_lead != 0 );
        AT_Assert( i_mutex != 0 );

        // if already associated - cancel it.
        if ( m_twin )
        {
            AideCancel();
        }

        // assume we have exclusive access to the member variables

        m_mutexPtr = i_mutex;
        m_twin = i_lead;

        /*  This makes the necessary state changes on the lead side.
         */
        WrapLeadAssociate( i_lead, i_mutex );

    } // end AideAssociate


    // ======== AideCancel ============================================
    /**
     *  This is AideTwinMT_Basic's implemetation of AideCancel, which
     *  is a pure virtual method in AideTwinMT.  It should not be
     *  overridden by classes deriving from AideTwinMT_Basic.
     *
     *  AideCancel is called by anyone to break the connection between
     *  the lead and the aide
     *
     *  DESIGN PHILOSOPHY -- the link can be broken AT ANY TIME
     *  by ANYONE, and clients must deal with it.  They must invalidate
     *  or revalidate all pointers to the other side of the link
     *  CallAide/CallLead do this for clients automatically,
     *  and reflect calls to broken links with an error status, and they
     *  are the only sanctioned means of calling across the link
     *
     *  AideCancel is thread-safe, and can be called multiple times on
     *  the same lead, serially or concurrently, without conflict.
     *
     *  It is permitted for a thread to call AideCancel while already
     *  holding a lock on the shared mutex.  This is necessary for
     *  certain techniques in which multiple lead-aide pairs share the
     *  same mutex.
     *
     *  @return nothing
     */
    virtual void AideCancel()
    {
        ThreadSafePtrTryLock< t_LeadPointer >  l_lock( m_mutexPtr, m_twin );

        if ( ! l_lock.IsAcquired() )
        {
            return;
        }
        
        // make sure cancel is not already in progress.
        if ( 0 != m_delete_marker )
        {
            return;
        }
        
        DeleteMarkerManager l_is_deleted( m_delete_marker );        
        
        t_LeadPointer l_pointer = m_twin;

        if ( l_pointer )
        {
            m_twin = 0;
            // cancellation notification
            l_pointer->LeadCancelPrivate();
            w_app_aide_twin_traits::AppAideCloseNotify_Internal( this, TwinTraits::CancelRequested );

            // could be deleted - don't really 
            if ( ( ! l_is_deleted ) && l_lock.IsAcquired() )
            {
                l_lock.ReplaceMutex( 0 );
            }
        }

    }

    // MUST ONLY BE CALLED BY LeadCancel
    //
    virtual void AideCancelPrivate()
    {
        // The m_mutexPtr is assumed to be locked by the aide
        DeleteMarkerManager l_is_deleted( m_delete_marker );
        
        m_twin = 0;
        w_app_aide_twin_traits::AppAideCloseNotify_Internal( this, TwinTraits::CancelRequested );

        if ( ! l_is_deleted )
        {
            m_mutexPtr = 0;
        }
    }

    /*  GetLeadRequireLock can be used when the lead or aide
     *  require to call multiple methods whith the mutex locked.
     *  This allows the lead implementation to get access to
     *  the aide.  The mutex MUST be locked before calling
     *  GetLeadRequireLock.
     */
    inline void GetLeadRequireLock( t_LeadPointer &  o_lead_ptr )
    {
        o_lead_ptr = m_twin;
    }


    friend class LockingCaller< AideTwinMT_Basic >;


    // ======== CallLead ==============================================
    /**
     *  CallLead returns a proxy object that can be used to make one or
     *  more thread-safe, serialized accesses to the lead object
     *  (methods and/or data members in the interface class).  In
     *  particular, the accesses are serialized with LeadCancel and
     *  AideCancel, so that if an access succeeds (i.e. the association
     *  is still active when the access begins), the lead is guaranteed
     *  not to destroy itself before the access is complete.  (If the
     *  association has already been broken before the access attempt,
     *  then the attempt fails.)
     *
     *  The accesses are incidentally also serialized with other
     *  accesses made using proxy objects returned by either CallLead
     *  or CallAide on the same lead-aide pair.  Significantly, the
     *  accesses are *not* serialized with operations performed on the
     *  lead object by other threads, that do not go through the
     *  at_twinmt interface.  (In other words, a thread that
     *  successfully accesses the lead object through a proxy returned
     *  by CallLead should not assume it has exclusive access to the
     *  lead object, unless that is guaranteed by other means.)
     *  Protecting against conflicts with operations that do not go
     *  through the at_twinmt interface is beyond the scope of
     *  at_twinmt.
     *
     *  All calls to CallLead must finish before the AideTwinMT_Basic destructor is called.
     *
     *
     *  @return A LockingCaller object which can be used to access the lead from the aide.
     */
    inline LockingCaller< AideTwinMT_Basic > CallLead()
    {
        AT_Assert(this);
        return LockingCaller< AideTwinMT_Basic >( *this );
    }


    // ======== AideTwinMT_Basic ======================================
    /**
     *  AideTwinMT_Basic constructor.
     */
    inline AideTwinMT_Basic() :
        m_twin(),
        m_delete_marker()
    {
    }


    // ======== ~AideTwinMT_Basic =====================================
    /**
     *  AideTwinMT_Basic destructor.
     *
     *  should ONLY ever be called by a managed ref-counted Ptr object.
     *  Do not ever call destructor yourself or even "delete" on the
     *  object since those mechanisms bypass all safe MT design principles.
     */
    inline ~AideTwinMT_Basic()
    {
        if ( m_delete_marker )
        {
            * m_delete_marker = true;
            return;
        }
        
        // we MUST call cancel, it is the only way to inform the
        // aide of our premature disappearance during exception handling.
        AideCancel();
    }


private:
    /*  Unimplemented.  */
    AideTwinMT_Basic( const AideTwinMT_Basic & );
    AideTwinMT_Basic & operator=( const AideTwinMT_Basic & );
};


}; // namespace


#endif // x_at_twinmt_basic_h_x
