/*
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 */
#include "at_net.h"
#include "at_net_ipv4.h"
#include "at_net_http.h"
#include "at_factory.h"
#include "at_assert.h"

using namespace at;

const NetError NetError::s_bind_failure("Bind failure");
const NetError NetError::s_unexpected_failure("Unexpected failure");
const NetError NetError::s_connection_refused("Connection refused");
const NetError NetError::s_timed_out("Operation timed out");
const NetError NetError::s_network_unreachable("Network unreachable");

const NetResolverError NetResolverError::s_fail("Permanent name lookup failure");
const NetResolverError NetResolverError::s_nodata("No address associated with na");
const NetResolverError NetResolverError::s_noname("No address associated with na");
const NetResolverError NetResolverError::s_gaveup("Temporary name resolution failure");

const NetDatagramError NetDatagramError::s_address_unavailable("Invalid address");
const NetDatagramError NetDatagramError::s_unexpected_failure("Unexpected failure");

const NetConnectionError NetConnectionError::s_peer_reset("Peer reset connection");
const NetConnectionError NetConnectionError::s_unexpected_failure("Unexpected failure");

bool
NetAddress::operator==(const NetAddress &rhs) const
{
    return &rhs == this;
}

namespace
{
    /**
     * SwitchNet figures out what kind of address it has received and
     * hands it off to the appropriate implementation.
     */
    class SwitchNet : public Net
    {
    public:
        SwitchNet();
        virtual ~SwitchNet();

        virtual void Resolve(
            NetResolverLead *i_lead,
            PtrDelegate<const NetAddress*> i_address);

        void ListenDatagram(
            NetDatagramLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters,
            PtrDelegate<Pool*> i_buffer_pool = 0);

        void Listen(
            NetConnectLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters);

        void Connect(
            NetConnectLead *i_lead,
            PtrDelegate<const NetAddress*> i_address,
            PtrDelegate<const NetParameters*> i_parameters);

        PtrDelegate<at::ActivityListOwner*> GetActivityListOwner();

    private:
        Net* GetNet(Ptr<const NetAddress*>);

        Ptr<Net*> m_ipv4_net;
        Ptr<Net*> m_http_net;

        /** Not implemented */
        SwitchNet(const SwitchNet&);
        /** Not implemented */
        SwitchNet& operator=(const SwitchNet&);
    };

    SwitchNet::SwitchNet() :
        m_ipv4_net(FactoryRegister<at::Net, DKy>::Get().Create("net_ipv4")()),
        m_http_net(FactoryRegister<at::Net, DKy>::Get().Create("net_http")())
    {
    }

    SwitchNet::~SwitchNet()
    {
        m_ipv4_net = NULL;
        m_http_net = NULL;
    }

    void
    SwitchNet::Resolve(
        NetResolverLead *i_lead,
        PtrDelegate<const NetAddress*> i_address)
    {
        Ptr<const NetAddress*> a = i_address;
        m_ipv4_net->Resolve(i_lead, a);
    }

    void
    SwitchNet::ListenDatagram(
        NetDatagramLead *i_lead,
        PtrDelegate<const NetAddress*> i_address,
        PtrDelegate<const NetParameters*> i_parameters,
        PtrDelegate<Pool*> i_buffer_pool)
    {
        Ptr<const NetAddress*> a = i_address;
        Ptr<const NetParameters*> p = i_parameters;
        this->GetNet(a)->ListenDatagram(
            i_lead, a, p, i_buffer_pool);
    }

    void
    SwitchNet::Listen(
        NetConnectLead *i_lead,
        PtrDelegate<const NetAddress*> i_address,
        PtrDelegate<const NetParameters*> i_parameters)
    {
        Ptr<const NetAddress*> a = i_address;
        Ptr<const NetParameters*> p = i_parameters;
        this->GetNet(a)->Listen(i_lead, a, p);
    }

    void
    SwitchNet::Connect(
        NetConnectLead *i_lead,
        PtrDelegate<const NetAddress*> i_address,
        PtrDelegate<const NetParameters*> i_parameters)
    {
        Ptr<const NetAddress*> a = i_address;
        Ptr<const NetParameters*> p = i_parameters;
        this->GetNet(a)->Connect(i_lead, a, p);
    }

    PtrDelegate<at::ActivityListOwner*>
    SwitchNet::GetActivityListOwner()
    {
        AT_Abort();
        return 0;
    }

    Net*
    SwitchNet::GetNet(Ptr<const NetAddress*> ptr)
    {
        const NetAddress *a = ptr.Get();
        Net *n = 0;

        if (dynamic_cast<const IPv4Address*>(a))
            n = m_ipv4_net.Get();
        else if (dynamic_cast<const HTTPAddress*>(a))
            n = m_http_net.Get();
        else
            AT_Abort();

        return n;
    }
}

// @@ TODO this is not thread-safe
AT_MakeFactory0P( "Net", SwitchNet, Net, DKy);

#define AT_HardLinkSymbolName Austria_NetHardLinkSymbol
#define HardLink(A)         \
    A(HTTPNet,0)            \
    A(IP,0)                 \
//end
#include "at_factory_link.h"


