//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_os.cpp
 *
 * This contains the implementation of at_os.h.
 *
 */

#include "at_os.h"
#include "at_unit_test.h"

// Austria namespace
namespace at
{

// Init static member variable.
// ----------------------------
char  * OSTraitsBase::m_accessViolationAddr = 0;
bool    OSTraitsBase::m_running_test_case = false;

// ======== TestCaseAssert ========================================
void OSTraitsBase::TestCaseAssert( const char * i_file, int i_line )
{
    throw TestCase_Exception(
        "Assertion from AT_Assert()",
        i_file,
        i_line
    );
}

// ======== TestCaseAbort ========================================
void OSTraitsBase::TestCaseAbort( const char * i_file, int i_line )
{
    throw TestCase_Exception(
        "AT_Abort() called",
        i_file,
        i_line
    );
}


#if defined(WIN32) || defined(_WIN32)

const char * OSTraitsBase::m_default_temp_dir = "C:";

#else

const char * OSTraitsBase::m_default_temp_dir = "/tmp";

#endif

#define AT_HardLinkSymbolName Austria_FactoryHardLinkSymbol
#define HardLink(A)         \
    A(Pool_Basic,0)         \
    A(TimerService_Basic,0) \
//end

#include "at_factory_link.h"

}; // namespace


