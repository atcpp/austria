//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_source_locator.h
 *
 * This contains a basic interface for reporting source file locations
 * used for reporting errors or logs.
 *
 */

#ifndef x_at_source_locator_h_x
#define x_at_source_locator_h_x 1

#include "at_exports.h"
#include <iostream>

// Austria namespace
namespace at
{

// ======== SourceLocator_Basic ===========================================
/**
 * A source locator record is simply a collection of source file name and
 * line number.
 *
 */

class AUSTRIA_EXPORT SourceLocator_Basic
{
    public:

    /**
     * This will constuct a source locator which is simply
     * a file name and a line number.
     *
     * @param i_filename a string that will exist for the life
     *                   of this object (a literal) that
     *                   represents the file name.
     * @param i_lineno   the line number.
     */

    SourceLocator_Basic(
        const char                                  * i_filename,
        int                                           i_lineno
    )
      : m_filename( i_filename ),
        m_lineno( i_lineno )
    {
    }

    /**
     * The stored file name.
     */
    const char                                      * m_filename;

    /**
     * The line number.
     */
    int                                               m_lineno;
};

}; // namespace

namespace std {

using namespace at;

// ======== basic_ostream<w_char_type, w_traits>& operator << =========
/**
 * Standard method for displaying a source locator
 *
 * @param w_char_type The character type
 * @param w_traits The traits type
 * @param i_ostream is the ostream
 * @param SourceLocator_Basic & is the source locator
 * @return reference to ostream
 */

template<
    typename        w_char_type,
    class           w_traits
>                   
basic_ostream<w_char_type, w_traits>& operator << (
    basic_ostream<w_char_type, w_traits>            & i_ostream,
    const SourceLocator_Basic                    & i_value
) {
    return i_ostream << i_value.m_filename << ':' << i_value.m_lineno;
    
} // end basic_ostream<w_char_type, w_traits>& operator <<

} // namespace std


#endif // x_at_source_locator_h_x


