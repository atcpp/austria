//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * \file at_lifetime_instr.h
 *
 * \author Gianni Mariani
 *
 * at_lifetime_instr.h is an instrumented reference counted pointer that can
 * be queried for the stack trace of all the remaining addref counts.
 */


#ifndef x_at_lifetime_instr_h_x
#define x_at_lifetime_instr_h_x 1

#include "at_stack_trace.h"
#include "at_lifetime_mt.h"
#include "at_thread.h"
#include <list>
#include <ostream>

// Austria namespace
namespace at
{

//

// ======== PtrStackTrace =============================================
/**
 * PtrStackTrace contains the stack trace for the instrumented
 * PtrTarget.
 *
 */

struct AUSTRIA_EXPORT PtrStackTrace
{
    public:

    /**
     * m_stack_trace is the stack trace to when the
     * m_ptr value became set.
     */
    at::Ptr< StackTrace * >     m_stack_trace;

    /**
     * t_WrapList is the type of the list that manages
     * this.
     */
    typedef std::list< PtrStackTrace * > t_WrapList;

    /**
     * m_location is the location of the wrapper
     */
    t_WrapList::iterator        m_location;
    
};


// ======== PtrClassRefWrapperInstrumented ========================================
/**
 * PtrClassRefWrapperInstrumented contains the state to manage access to the
 * reference counted pointer.  A different implementation of this
 * class would be needed to handle multi-threaded access.
 *
 */

template <typename w_ClassRef>
class PtrClassRefWrapperInstrumented
  : public PtrStackTrace
{
    
    /**
     * m_ptr contains the pointer to the reference
     * counted object.
     */
    w_ClassRef                  m_ptr;

    public:

    /**
     * t_ClassRefWrapper default constructor
     */
    inline PtrClassRefWrapperInstrumented()
      : m_ptr()
    {
    }

    /**
     * t_ClassRefWrapper default constructor
     */
    inline PtrClassRefWrapperInstrumented( w_ClassRef i_ptr )
      : m_ptr( i_ptr )
    {
        if ( i_ptr )
        {
            m_stack_trace = NewStackTrace();
            i_ptr->AddReferer( this );
        }
    }

    ~PtrClassRefWrapperInstrumented()
    {
        if ( m_ptr )
        {
            m_ptr->RemoveReferer( this );
        }
    }

    /**
     * SingleThreadedAccess can be called when it is
     * guarenteed that only a single thread will access
     * the pointer.
     */
    inline w_ClassRef & SingleThreadedAccess()
    {
        return m_ptr;
    }

    /**
     * SafeAccess is usually used in the destructor.
     */
    inline w_ClassRef & SafeAccess()
    {
        return m_ptr;
    }

    /**  
     * PurgePointer is used to clear the pointer for
     * instrumented pointers. 
     * 
     */
    
    inline void PurgePointer()
    {
        if ( m_ptr )
        {
            m_ptr->RemoveReferer( this );
            m_ptr = w_ClassRef();
        }
    }

    /**
     * IncRefCountTest will test that the object exists
     * before incrementing the reference count.
     */
    template <typename w_ClassRefLhs, typename w_Traits>
    inline w_ClassRefLhs IncRefCountTest()
    {
        w_ClassRefLhs           l_ptr( m_ptr );

        if ( l_ptr )
        {
            w_Traits::IncRefCount( l_ptr );
        }
        return l_ptr;
    }

    /**
     * SwapPtr will swap the i_ptr and the local pointer.
     * In a Ptr accessed in a multithreaded way, this must be
     * single threaded.
     */
    inline w_ClassRef SwapPtr( w_ClassRef i_ptr )
    {
        w_ClassRef          l_ptr( m_ptr );

        if ( l_ptr )
        {
            l_ptr->RemoveReferer( this );
        }

        m_ptr = i_ptr;

        if ( i_ptr )
        {
            m_stack_trace = NewStackTrace();
            i_ptr->AddReferer( this );
        }
        else
        {
            m_stack_trace = 0;
        }

        return l_ptr;
    }


    private :
    // PtrClassRefWrapperInstrumented does not allow copy construction or
    //

    // no copy constructor
    PtrClassRefWrapperInstrumented( const PtrClassRefWrapperInstrumented & i_val );

    // no assignment
    PtrClassRefWrapperInstrumented & operator = ( const PtrClassRefWrapperInstrumented & i_val );

};


// ======== PtrTarget_Instrumented =========================================
/**
 * PtrTarget_Instrumented is an instrumented reference counting class that
 * is suitable for multi-threaded or non multi-threaded use.  Used with the
 * appropriate Ptr trait type smart pointers, this can be used to
 * find all the pointers that are currently pointing to objects of
 * this type.  These are heavy objects in that they contain a mutex
 * and a std::list of all the objects pointing to this.
 *
 * @param w_count_t the count type - this can be any type that supports
 * pre-increment or pre-decrement and compares equal to or greater
 * than zero.
 */

template <
    typename        w_base,
    typename        w_referrer = PtrStackTrace,
    typename        w_lock_type = at::Mutex
>
class PtrTarget_Instrumented
  : public w_base
{
    public:

    /**
     * t_base is the base type
     */
    typedef w_base                              t_base;

    /**
     * t_ptr_lock_type is the lock type
     */
    typedef w_lock_type                         t_ptr_lock_type;

    /**
     * t_ptr_referrer is the referer.
     */
    typedef w_referrer                          t_ptr_referrer;

    // for unit test only.
    friend class AT_TEST_LifeControlExp;
    
    /**
     * m_ptr_mutex is the mutex type that locks access to the list
     */
    mutable t_ptr_lock_type                     m_ptr_mutex;

    /**
     * m_reflist contains a reverse reference of the list
     * of w_referrer objects that point to this.
     */
    mutable typename t_ptr_referrer::t_WrapList m_reflist;


    // ======== RemoveReferer =========================================
    /**
     * RemoveReferer will remove the referer object from the list.
     *
     * @param i_refer A pointer to the referring object
     */

    virtual void RemoveReferer( w_referrer * i_referer ) const
    {
        at::Lock<w_lock_type>           l_lock( m_ptr_mutex );

        m_reflist.erase( i_referer->m_location );
    }
    
    // ======== AddReferer ============================================
    /**
     * AddReferer will insert a referer object into the list.
     *
     * @param i_refer A pointer to the referring object
     */

    virtual void AddReferer( w_referrer * i_referer ) const
    {
        at::Lock<w_lock_type>           l_lock( m_ptr_mutex );

        m_reflist.push_front( i_referer );
        i_referer->m_location = m_reflist.begin();
    }


    // ======== GetRefCount ===========================================
    /**
     * This returns a const reference to the count object.
     *
     * @return A const reference to the reference count
     */

    const typename t_base::t_count & GetRefCount() const
    {
        return t_base::GetRefCount();
    }    

};

//AUSTRIA_TEMPLATE_EXPORT(PtrTarget_Instrumented, AtomicCount)
typedef PtrTarget_Instrumented<PtrTarget_MT> PtrTarget_InstrMT;


// ======== PrintPointerOwners ========================================
/**
 * PrintPointerOwners may be used on a PtrTarget_Instrumented
 * object with the w_referrer = PtrStackTrace.
 *
 * @param o_stream  The output stream
 * @param i_ptr     A reference to the object of a PtrTarget_Instrumented
 *                  base type.
 * @return o_stream
 */

template <typename t_ptr_type>
std::ostream & PrintPointerOwners(
    std::ostream        & o_stream,
    t_ptr_type          & i_ptr
) {

    at::Lock<typename t_ptr_type::t_ptr_lock_type>   l_lock(i_ptr.m_ptr_mutex);

    typedef typename t_ptr_type::t_ptr_referrer::t_WrapList::iterator   t_iterator;
    typedef typename t_ptr_type::t_ptr_referrer::t_WrapList::size_type  t_size_type;

    t_size_type l_count = i_ptr.m_reflist.size();
    t_size_type l_refcount = t_size_type( i_ptr.GetRefCount().Get() );
    
    if ( ( ! l_count ) && ( ! l_refcount ) )
    {
        o_stream << "Object has no references\n";
        return o_stream;
    }

    if ( ( ! l_count ) && ( l_refcount ) )
    {
        o_stream << "Object reference count is " << l_refcount << " however no listed references\n";
        return o_stream;
    }

    if ( l_count != l_refcount )
    {
        o_stream << "Object difference in the reference count(" << l_refcount
            << ") and the list count(" << l_count << ")\n";
        o_stream << "Stack traces (most recent first)\n";
    }
    else
    {
        o_stream << "Pointer reference count is " << l_count << "\n";
    }

    int     l_it = 1;
    for ( t_iterator l_i = i_ptr.m_reflist.begin(); l_i != i_ptr.m_reflist.end(); ++ l_i )
    {
        o_stream << "Stack trace " << ( l_it ++ ) << "\n";
        o_stream << * ( ( * l_i )->m_stack_trace );
    }

    return o_stream;
}


    
}; // namespace
#endif // x_at_lifetime_instr_h_x

