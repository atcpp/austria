// -*- c++ -*-
//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_file_status_report_basic.h
 *
 */

#ifndef x_at_file_status_report_basic_h_x
#define x_at_file_status_report_basic_h_x 1

#include "at_file_status_report.h"

// Austria namespace
namespace at
{

// ======== FileStatusReport_Basic =============================================
/**
 *  FileStatusReport_Basic is a basic implementation of the FileStatusReport
 *  interface.
 */

class AUSTRIA_EXPORT FileStatusReport_Basic
    : public FileStatusReport
{
    std::string                 m_filename;
    unsigned int                m_linenum;
    StatusTypeCode              m_typecode;
    int                         m_usercode;
    std::string                 m_description;

public:

    FileStatusReport_Basic()
        : m_linenum( 0 ),
          m_typecode( StatusReport::NoStatus ),
          m_usercode( 0 )
    {
    }

    FileStatusReport_Basic(
        StatusTypeCode          i_typecode,
        int                     i_usercode,
        const std::string     & i_description
        )
        : m_linenum( 0 ),
          m_typecode( i_typecode ),
          m_usercode( i_usercode ),
          m_description( i_description )
    {
    }

    FileStatusReport_Basic(
        const std::string     & i_filename,
        unsigned int            i_linenum,
        StatusTypeCode          i_typecode,
        int                     i_usercode,
        const std::string     & i_description
        )
        : m_filename( i_filename ),
          m_linenum( i_linenum ),
          m_typecode( i_typecode ),
          m_usercode( i_usercode ),
          m_description( i_description )
    {
    }

    ~FileStatusReport_Basic()
    {
    }

    void ReportStatus(
        const std::string     & i_filename,
        unsigned int            i_linenum,
        StatusTypeCode          i_typecode,
        int                     i_usercode,
        const std::string     & i_description
        )
    {
        // just store the info
        m_filename = i_filename;
        m_linenum = i_linenum;
        m_typecode = i_typecode;
        m_usercode = i_usercode;
        m_description = i_description;
    }

    void ReportStatus(
        const FileStatusReport& i_sreport
        )
    {
        i_sreport.GetStatus( *this );
    }

    void ClearStatus()
    {
        m_filename.clear();
        m_linenum = 0;
        m_typecode = StatusReport::NoStatus;
        m_usercode = 0;
        m_description.clear();
    }

    bool GetStatus() const
   {
       return m_typecode != StatusReport::NoStatus;
   }

    bool GetStatus(
        std::string           & o_filename,
        unsigned int          & o_linenum,
        StatusTypeCode        & o_typecode,
        int                   & o_usercode,
        std::string           & o_description
        ) const
    {
        if ( m_typecode == StatusReport::NoStatus )
        {
            return false;
        }
        o_filename = m_filename;
        o_linenum = m_linenum;
        o_typecode = m_typecode;
        o_usercode = m_usercode;
        o_description = m_description;
        return true;
    }

    bool GetStatus(
        FileStatusReport      & o_sreport
        ) const
    {
        if ( m_typecode == StatusReport::NoStatus )
        {
            return false;
        }
        o_sreport.ReportStatus(
            m_filename,
            m_linenum,
            m_typecode,
            m_usercode,
            m_description
            );
        return true;
    }

    void Print( std::ostream& i_os ) const
    {
        i_os << m_filename;
        if ( m_linenum )
        {
            i_os << ':' << m_linenum;
        }
        switch ( m_typecode )
        {
        case StatusReport::WarningStatus:
            i_os << ": warning";
            break;
        case StatusReport::ErrorStatus:
            i_os << ": error";
            break;
        case StatusReport::UnrecoverableStatus:
            i_os << ": fatal error";
            break;
        }
        if ( m_usercode )
        {
            i_os << " " << m_usercode;
        }
        if ( ! m_description.empty() )
        {
            i_os << ": " << m_description;
        }
    }

    bool operator==( const FileStatusReport_Basic & i_other ) const
    {
        return
            ( this == &i_other ) ||
            ( m_filename == i_other.m_filename &&
              m_linenum == i_other.m_linenum &&
              m_typecode == i_other.m_typecode &&
              m_usercode == i_other.m_usercode &&
              m_description == i_other.m_description );
    }

    bool operator!=( const FileStatusReport_Basic & i_other ) const
    {
        return ! ( i_other == *this );
    }

    StatusTypeCode Type() const
    {
        return m_typecode;
    }

};

inline bool IsErrorStatusReport( const at::FileStatusReport_Basic & i_sreport )
{
    return
        i_sreport.Type() == at::StatusReport::ErrorStatus ||
        i_sreport.Type() == at::StatusReport::UnrecoverableStatus;
}

} // namespace

#endif // x_at_file_status_report_basic_h_x
