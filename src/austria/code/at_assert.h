//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_assert.h
 *
 */

#ifndef x_at_assert_h_x
#define x_at_assert_h_x 1

#include "at_os.h"

// Austria namespace
namespace at
{

// ======== AT_Assert ==================================================
/**
 * The AT_Assert macro aborts execution of the running process if the
 * asserted condition is not true.
 */

#ifdef AT_USE_OS_ASSERT

#include <cassert>
#include <cstdlib>

#define AT_Assert(A)  os_assert(A)
#define AT_Abort()    os_abort(A)
#else // #ifdef AT_USE_OS_ASSERT

// ======== AT_Assert =================================================
/**
 * AT_Assert performs the same operations as <cassert>'s  assert()
 * macro, however, this will throw a test case exception when
 * running test cases.
 */

#if AT_DebugBuild

#define AT_Assert(A)                                                    \
    do {                                                                \
        if ( ! (A) ) {                                                  \
            if ( ::at::OSTraitsBase::GetRunningTestCase() ) {           \
                ::at::OSTraitsBase::TestCaseAssert( __FILE__, __LINE__ );\
            }                                                           \
            /* access violation - prefer this to abort */               \
            * ::at::OSTraitsBase::GetAccVioAddr() = 0;                  \
        }                                                               \
    } while ( false )                                                   \
// end macro AT_Assert
                
#else // #if AT_DebugBuild
#define AT_Assert(A)    do {} while (false)
#endif // #if AT_DebugBuild

#define AT_Abort()                                                      \
    do {                                                                \
        if ( ::at::OSTraitsBase::GetRunningTestCase() ) {               \
            ::at::OSTraitsBase::TestCaseAbort( __FILE__, __LINE__ );    \
        }                                                               \
        * ::at::OSTraitsBase::GetAccVioAddr() = 0;                      \
    } while ( true )                                                    \
// end macro AT_Abort

#endif // #ifdef AT_USE_OS_ASSERT


#define x_AT_CAT_A_x(i_a, i_b) i_a ## i_b
#define x_AT_CAT_x(i_a, i_b) x_AT_CAT_A_x(i_a, i_b)
#define AT_SuffixLneNumber( i_a ) x_AT_CAT_x( i_a, __LINE__ )


// ======== AT_StaticAssert ===========================================
/**
 * AT_StaticAssert will cause a compilation error if the "cond" value
 * is not true.  The name should be descriptive.
 *
 * @param i_condional a constant expression that evaluates to false
 *          when a compilation error is needed.
 * @param i_name Is a valid C++ identifer which contains a name.
 */

template <int w_v>
struct AT_StaticAssert_HelperArray
{
    enum {
        m_value = (w_v) ? 1 : -1
    };
};

#define AT_StaticAssert( i_condional, i_name )                          \
    typedef char                                                        \
        AT_SuffixLneNumber( x_StaticAssert___ ## i_name ## ___ )[       \
            ::at::AT_StaticAssert_HelperArray<(0!=(int)(i_condional))>::m_value \
        ]                                                               \
// end macro

} // namespace at

#endif // #if x_at_assert_h_x

