//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_exception.h
 *
 */

#ifndef x_at_exception_h_x
#define x_at_exception_h_x 1

#include "at_exports.h"
#include "at_source_locator.h"
#include "at_lifetime.h"
#include "at_stack_trace.h"
#include <string>

// Austria namespace
namespace at
{


class Full_Exception;

// ======== Exception =============================================
/**
 * This is the basic low-cost exception interface.
 *
 */

class AUSTRIA_EXPORT Exception
  : public std::exception
{

    /**
     * This contains a description of the error.
     */
    const char                      * m_what;
    
    public:

    virtual ~Exception() throw ()
    {
    }


    // ======== Exception =============================================
    /**
     * Construct an Exception.
     *
     * @param i_what The reason for the exception
     */

    Exception(
        const char                  * i_what
    )
      : m_what( i_what )
    {
        if ( s_abort_on_assert )
        {
            AT_Abort();
        }
    }

    /**
     * This returns the m_what value - usually a description
     * of the reason for the exception.
     */
    
    virtual const char * what() const throw ()
    {
        return m_what;
    }

    /**
     * Full returns a pointer to the Exception_Full interface
     * if this Exception_Full is a child of this.  Null otherwise.
     *
     * @return A pointer to the Full_Exception class.
     */
    virtual const Full_Exception * Full() const
    {
        return 0;
    }

    static bool s_abort_on_assert;
};


// ======== Full_Exception ============================================
/**
 * Full_Exception is a full-on exception that contains much more
 * information regarding the reason for the exception, including a
 * stack trace.
 *
 */

class AUSTRIA_EXPORT Full_Exception
  : public Exception
{
    public:

    virtual ~Full_Exception() throw ()
    {
    }

    Full_Exception(
        const char                          * i_what,
        const char                          * i_filename,
        int                                   i_lineno,
        PtrDelegate< const StackTrace * >     i_trace
    )
      : Exception( i_what ),
        m_sl( i_filename, i_lineno ),
        m_trace( i_trace )
    {
    }
    
    // ======== Trace =================================================
    /**
     * Trace returns the stack-trace at the point at which this
     * exception is created.
     *
     * @return A pointer to a StackTrace interface
     */

    PtrView< const StackTrace * > Trace() const
    {
        return m_trace;
    }

    /**
     * Full returns a pointer to the Full_Exception interface
     * if this Full_Exception is a child of this.  Null otherwise.
     *
     * @return A pointer to the Full_Exception class.
     */
    virtual const Full_Exception * Full() const
    {
        return this;
    }

    // ======== Location ==============================================
    /**
     * Location returns the location that caused the exception.
     *
     *
     * @return A "SourceLocator_Basic".
     */

    const SourceLocator_Basic & Location() const
    {
        return m_sl;
    }

    static const char                           s_default_file[];

    private:
    /**
     * m_sl contains the source locator.
     */
    const SourceLocator_Basic                   m_sl;

    /**
     * m_trace contains the stack trace.
     */
    Ptr< const StackTrace * >                   m_trace;
};

#define AT_ThrowFull( i_classprefix, i_why ) \
    throw i_classprefix ## _Exception( (i_why), __FILE__, __LINE__, NewStackTrace() )

    
// ======== ExceptionDerivation =================================
/**
 * ExceptionDerivation is a template that derives from an exception
 * class ( Full_Exception by default ) and allows the creation of a
 * 
 */

template <typename w_Type, typename w_ExceptionType = Full_Exception>
class ExceptionDerivation
  : public w_ExceptionType
{
    public:

    /**
     * ConversionFailure_Exception constructor
     *
     */
    inline ExceptionDerivation( 
        const char                          * i_what,
        const char                          * i_filename,
        int                                   i_lineno,
        PtrDelegate< const StackTrace * >     i_trace = 0
    )
      : w_ExceptionType( i_what, i_filename, i_lineno, i_trace )
    {
    }

};

#define AT_ThrowDerivation( i_class, i_why ) \
    throw at::ExceptionDerivation<i_class>( (i_why), __FILE__, __LINE__, at::NewStackTrace() )
    
#define AT_ThrowDerivationBase( i_class, i_derive, i_why ) \
    throw at::ExceptionDerivation<i_class, i_derive>( (i_why), __FILE__, __LINE__, at::NewStackTrace() )
    
    
// ======== ExceptionDerivBase =================================
/**
 * ExceptionDerivBase is a template that derives from an exception
 * class nominated within the template parameter.
 * 
 */

template <typename w_Type, typename w_ExceptionType = Full_Exception>
class ExceptionDerivBase
  : public ExceptionDerivation<typename w_Type::t_Base, w_ExceptionType>
{
    public:

    /**
     * ConversionFailure_Exception constructor
     *
     */
    inline ExceptionDerivBase( 
        const char                          * i_what,
        const char                          * i_filename,
        int                                   i_lineno,
        PtrDelegate< const StackTrace * >     i_trace = 0
    )
      : ExceptionDerivation<typename w_Type::t_Base, w_ExceptionType>(
            i_what, i_filename, i_lineno, i_trace
        )
    {
    }

};

#define AT_ThrowDerivBase( i_class, i_why ) \
    throw at::ExceptionDerivBase<i_class>( (i_why), __FILE__, __LINE__, at::NewStackTrace() )
    
#define AT_ThrowDerivBaseBase( i_class, i_derive, i_why ) \
    throw at::ExceptionDerivBase<i_class, i_derive>( (i_why), __FILE__, __LINE__, at::NewStackTrace() )
    
}; // namespace

#endif // x_at_exception_h_x


