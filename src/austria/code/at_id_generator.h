//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_id_generator.h
 *
 */

#ifndef x_at_id_generator_h_x
#define x_at_id_generator_h_x 1

#include "at_exports.h"
#include <string>

// Austria namespace
namespace at
{


// ======== NewIdent =============================================
/**
 * NewIdent will return a string form of an Ident.  This is
 * equivalent to a GUID or a UUID.
 *
 * @return A string representing the identifier.
 */

extern AUSTRIA_EXPORT std::string NewIdent();


// ======== BinaryRandomSequence =============================================
/**
 * BinaryRandomSequence will return a string with a number of binary 
 *
 * @param i_length The length of the binary random sequence to generate
 * @return A string representing the identifier.
 */

extern AUSTRIA_EXPORT std::string BinaryRandomSequence( unsigned i_length );


} // namespace

#endif // x_at_id_generator_h_x



