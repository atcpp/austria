//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_status_report.h
 *
 * 
 */

#ifndef x_at_status_report_h_x
#define x_at_status_report_h_x 1

#include "at_exports.h"
#include <string>

// Austria namespace
namespace at
{

// ======== StatusReport =================================================
/**
 * StatusReport is a simple status report interface meant to be used
 * as a generic extended form of error/status reporting.
 */

class AUSTRIA_EXPORT StatusReport
{
    public:

    virtual ~StatusReport();

    /**
     * The StatusTypeCode indicates the nature of the status being
     * reported.
     *
     */

    enum StatusTypeCode
    {
        /**
         * NoStatus indicates that no status is reported.
         */
        
        NoStatus,

        /**
         * ErrorStatus is usually used to indicate a failure but not of
         * a nature that causes the system reporting the failure to
         * cease to peform other operations.
         */
        
        ErrorStatus,

        /**
         * WarningStatus indicates that a problem was encountered but
         * the requested operation was able to complete.  It is usually
         * good to log such information but it does not cause unexpected
         * behaviour.
         */
        
        WarningStatus,

        /**
         * UnrecoverableStatus indicates that the error is of a severe
         * nature and the object reporting the status is unable to 
         * continue.
         */
        
        UnrecoverableStatus
    };

    /**
     * ReportStatus is used by the interface to report the status of 
     * a request - usually the act of calling a method on an object.
     *
     * @param i_typecode is a StatusTypeCode.
     * @param i_status is an object specific code.
     * @param i_description describes the status in a human readable form.
     */

    virtual void ReportStatus(
        StatusTypeCode                                i_typecode,
        int                                           i_status,
        const std::string                           & i_description
    ) = 0;

    /**
     * ReportStatus is used by the interface to report the status of 
     * a request - usually the act of calling a method on an object.
     * This allows a report from an existing report.
     *
     * @param i_sreport
     */

    virtual void ReportStatus(
        const StatusReport                      * i_sreport
    ) = 0;

    /**
     * ClearStatus is used to clear the status.
     */

    virtual void ClearStatus() = 0;

    /**
     * GetStatus is used to get a status report - if any.
     * @return true if an status was reported.
     */

    virtual bool GetStatus() const = 0;

    /**
     * GetStatus( StatusTypeCode &, int &, std::string & )
     * is used to get a full description of the status.
     *
     * @param i_typecode is a StatusTypeCode.
     * @param i_status is an object specific code.
     * @param i_description describes the status in a human readable form.
     * @return true if a status was reported.
     */

    virtual bool GetStatus(
        StatusTypeCode                              & i_typecode,
        int                                         & i_status,
        std::string                                 & i_description
    ) const = 0;

    /**
     * GetStatus( StatusReport * )
     * is used to get a full description of the status.
     *
     * @return true if a status was reported.
     */

    virtual bool GetStatus(
        StatusReport                                * o_srep
    ) const = 0;
    
    
};

}; // namespace
#endif // x_at_status_report_h_x


