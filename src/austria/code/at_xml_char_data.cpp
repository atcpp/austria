//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 

/**
 * \file at_xml_char_data.cpp
 *
 * \author Guido Gherardi
 *
 */

#include "at_xml_char_data.h"
#include "at_exception.h"
#include "at_xml_text.h"
#include "at_xml_comment.h"

#define AT_ThrowXmlIndexSizeErr(msg)        AT_ThrowDerivation( at::XmlIndexSizeErr, msg )

// Austria namespace
namespace at
{
    std::string XmlCharacterData::Value() const
    {
        return Data();
    }

    void XmlCharacterData::DeleteData( std::string::size_type i_offset,
                                       std::string::size_type i_count )
    {
        if ( i_offset > m_data.size() )
        {
            AT_ThrowXmlIndexSizeErr( "i_offset is out of range" );
        }
        m_data.erase( i_offset, i_count );
    }

    void XmlCharacterData::InsertData( std::string::size_type i_offset,
                                       const std::string& i_str )
    {
        if ( i_offset > m_data.size() )
        {
            AT_ThrowXmlIndexSizeErr( "i_offset is out of range" );
        }
        m_data.insert( i_offset, i_str );
    }

    void XmlCharacterData::ReplaceData( std::string::size_type i_offset,
                                        std::string::size_type i_count,
                                        const std::string&     i_str )
    {
        if ( i_offset > m_data.size() )
        {
            AT_ThrowXmlIndexSizeErr( "i_offset is out of range" );
        }
        m_data.replace( i_offset, i_count, i_str );
    }

    std::string XmlCharacterData::SubstringData( std::string::size_type i_offset,
                                                 std::string::size_type i_count ) const
    {
        if ( i_offset > m_data.size() )
        {
            AT_ThrowXmlIndexSizeErr( "i_offset is out of range" );
        }
        return std::string( m_data, i_offset, i_count );
    }

    std::string XmlText::Name() const
    {
        static std::string l_name( "#text" );
        return l_name;
    }

    PtrDelegate< XmlNode* > XmlText::Clone( bool i_deep ) const
    {
        return PtrDelegate< XmlText* >( new XmlText( OwnerDocument(), Data() ) );
    }

    unsigned short XmlText::Type() const
    {
        return TEXT_NODE;
    }

    void XmlText::Accept( XmlNodeVisitor& i_op )
    {
        i_op.VisitText( this );
    }

    void XmlText::Accept( XmlNodeVisitor& i_op ) const
    {
        i_op.VisitText( this );
    }

    std::string XmlComment::Name() const
    {
        static std::string l_name( "#comment" );
        return l_name;
    }

    PtrDelegate< XmlNode* > XmlComment::Clone( bool i_deep ) const
    {
        return PtrDelegate< XmlComment* >( new XmlComment( OwnerDocument(), Data() ) );
    }

    unsigned short XmlComment::Type() const
    {
        return COMMENT_NODE;
    }

    void XmlComment::Accept( XmlNodeVisitor& i_op )
    {
        i_op.VisitComment( this );
    }

    void XmlComment::Accept( XmlNodeVisitor& i_op ) const
    {
        i_op.VisitComment( this );
    }
};
