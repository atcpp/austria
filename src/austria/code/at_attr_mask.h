//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_attr_mask.h
 *
 */

#ifndef x_at_attr_mask_h_x
#define x_at_attr_mask_h_x 1

#include "at_assert.h"

/**
 * @defgroup AttrMask Statically Checked Type Safe Attributes 

<h2>Introduction</h2>

<p>Some API's require the ability to set a number of different options.
This has been traditionally performed by using a mask of bits and the
bitwise "|" operator.  This can sometimes lead to problems where the
combination of options is invalid or with constants of different
purposes used incorrectly.  This interface allows the creation of
of a bit-mask set of options with optional compile-time checking for
masks that are not allowed (run-time checking is still required).

<h3>How to Use The AT_DefineAttribute</h3>

<p>This interface makes heavy use of macros and templates to generate classes
and constants that can be used to create all the definitions that
are required.

<h5>Example for Creating and Reading an Any Object</h5>
<CODE>
<pre>

// Descision 1.  Decide on the final attribute type class name.
// In this case we have chosen "WriteAttr".  It is required to define
// a pre-processor macro that contains a list of all the attributes
// that are required and it MUST BE NAMED with the chosen name
// suffixed by _List_x - like so:

// 
// definition of application specific attributes

#define WriteAttr_List_x( A )  \
        A( WriteAttr, WriteAppend, 0 )             \
        A( WriteAttr, NoExistCreate, 1 )           \
        A( WriteAttr, CreateExclusive, 2 )         \
        A( WriteAttr, Truncate, 3 )                \
        A( WriteAttr, Sync, 4 )                    \
// End                                                     //

//
// Next is to decide 

#define WriteAttr_Illegal                                  \
    AT_IllegalCombo( w_Attr, Truncate, WriteAppend )       \
// End                                                     //

AT_DefineAttribute( WriteAttr, WriteAttr_Illegal )

// this goes in the C++ file...
WriteAttr_List_x( AT_StaticVariableDefn )


/// definition of application specific attributes

#define ReadAttr_List_x( A )  \
        A( ReadAttr, NonBlock, 0 )                  \
        A( ReadAttr, Direct, 1 )                    \
// End                                                     //

#define ReadAttr_Illegal                                    \
// End                                                     //

AT_DefineAttribute( ReadAttr, ReadAttr_Illegal )

// this goes in the C++ file...
ReadAttr_List_x( AT_StaticVariableDefn )

ReadAttr        i_rattr = ReadAttr::NonBlock;

// application test code.
WriteAttr      i_xattr = 
        WriteAttr::WriteAppend | WriteAttr::NoExistCreate | WriteAttr::CreateExclusive;

WriteAttr      i_xattr_dynamic = WriteAttr( WriteAttr::e_WriteAppend ) | WriteAttr::NoExistCreate;

//WriteAttr      i_xattr_bad = 
//        WriteAttr::WriteAppend | WriteAttr::Truncate;


#if 1
WriteAttr Foo()
{
//    WriteAttr::Attribute     x(3 );
    
    return WriteAttr::WriteAppend | WriteAttr::NoExistCreate | WriteAttr::CreateExclusive;
}

#endif


 *
 * 
 *  @{
 */



#define AT_AttributeEnumDef( AttrType, Name, Num ) \
    e_ ## Name = 1 << (Num),

#define AT_StaticVariableDecl( AttrType, Name, Num ) \
    static const AttrBase< t_Type, t_Derived, unsigned(e_ ## Name) > Name;

#define AT_StaticVariableDefn( AttrType, Name, Num ) \
    const AttrBase< AttrType::t_Type, AttrType::t_Derived, unsigned(AttrType::e_ ## Name) > AttrType::Name;

#define AT_IllegalCombo( X, A, B )                                      \
    AT_StaticAssert(                                                    \
        ( X & ( e_ ## A | e_ ## B ) ) != ( e_ ## A | e_ ## B ),         \
        Cannot_have_ ## A ## _and_ ## B ## _in_the_same_attribute_set   \
    );                                                                  \
// end macro                                                            //

#define AT_DefineAttribute( x_AttributeName_x, x_Illegal_x )             \
                                                                         \
class x_AttributeName_x                                                  \
{                                                                        \
    public:                                                              \
                                                                         \
    enum AttributeEnumType                                               \
    {                                                                    \
        x_AttributeName_x ## _List_x( AT_AttributeEnumDef )              \
    };                                                                   \
                                                                         \
    typedef AttributeEnumType               t_Type;                      \
    typedef x_AttributeName_x               t_Derived;                   \
                                                                         \
    x_AttributeName_x () : m_attributes(0) {}                            \
                                                                         \
    template< unsigned w_Attr >                                          \
    inline x_AttributeName_x(                                            \
        const AttrBase< t_Type, t_Derived, w_Attr >     & i_attr         \
    )                                                                    \
      : m_attributes( w_Attr )                                           \
    {                                                                    \
    }                                                                    \
                                                                         \
    template< unsigned w_Attr >                                          \
    inline x_AttributeName_x(                                            \
        const AttrBase< t_Type, t_Derived, w_Attr >     & i_attr,        \
        const x_AttributeName_x                         & i_other        \
    )                                                                    \
      : m_attributes( w_Attr | i_other.m_attributes )                    \
    {                                                                    \
    }                                                                    \
                                                                         \
    const x_AttributeName_x operator|( const x_AttributeName_x & i_rhs ) const \
    {                                                                    \
        return x_AttributeName_x( *this, i_rhs );                        \
    }                                                                    \
                                                                         \
    const unsigned operator&( const x_AttributeName_x & i_rhs ) const    \
    {                                                                    \
        return m_attributes & i_rhs.m_attributes;                        \
    }                                                                    \
                                                                         \
    /**                                                                  \                      
     *     This could also be operator |=                                \
     */                                                                  \
    unsigned Add ( const x_AttributeName_x & i_attr )                    \
    {                                                                    \
        m_attributes |= i_attr.m_attributes;                             \
                                                                         \
        return m_attributes;                                             \
    }                                                                    \
                                                                         \
    /**                                                                  \
     *    This couls also be operator ~ and operator &=                  \
     */                                                                  \
    unsigned Remove ( const x_AttributeName_x & i_attr )                 \
    {                                                                    \
        m_attributes &= ~i_attr.m_attributes;                            \
                                                                         \
        return m_attributes;                                             \
    }                                                                    \
                                                                         \
    /**                                                                  \
     * m_attribute                                                       \
     */                                                                  \
    unsigned                                m_attributes;                \
                                                                         \
    template <unsigned w_Attr >                                          \
    struct CheckIllegal                                                  \
    {                                                                    \
        x_Illegal_x                                                      \
    };                                                                   \
                                                                         \
    x_AttributeName_x ## _List_x( AT_StaticVariableDecl );               \
                                                                         \
    private:                                                             \
    inline x_AttributeName_x(                                            \
        const x_AttributeName_x                 & i_lhs,                 \
        const x_AttributeName_x                 & i_rhs                  \
    )                                                                    \
      : m_attributes( i_lhs.m_attributes | i_rhs.m_attributes )          \
    {                                                                    \
    }                                                                    \
};                                                                       \
// End Macro                                                            //


// ======== AttrBase ============================================
/**
 * Attribute class - represents basic attributes
 *
 */

template <typename w_Type, typename w_Derived, unsigned w_Attr >
class AttrBase
{
    public:

    typedef w_Type              t_Type;
    typedef w_Derived           t_Derived;

    typename t_Derived::template CheckIllegal<w_Attr>     m_legal;
    
    inline AttrBase()
    {}
    
};

template <
    typename    w_Type,
    typename    w_Derived,
    unsigned    w_AttrLhs,
    unsigned    w_AttrRhs
>
inline AttrBase<
    w_Type,
    w_Derived,
    w_AttrLhs | w_AttrRhs
>
operator | (
    const AttrBase<w_Type, w_Derived, w_AttrLhs>        & i_lhs,
    const AttrBase<w_Type, w_Derived, w_AttrRhs>        & i_rhs
) {
    typedef AttrBase<
        w_Type,
        w_Derived,
        w_AttrLhs | w_AttrRhs
    > t_ReturnType;

    return t_ReturnType();
}

template <
    typename w_Type,
    typename w_Derived,
    w_Type   w_AttrLhs
>
const w_Derived operator | (
    const AttrBase<w_Type, w_Derived, w_AttrLhs>        & i_lhs,
    const w_Derived                                     & i_rhs
) {
    return w_Derived( i_lhs, i_rhs );
}

template <
    typename w_Type,
    typename w_Derived,
    w_Type   w_AttrRhs
>
const w_Derived operator | (
    const w_Derived                                     & i_lhs,
    const AttrBase<w_Type, w_Derived, w_AttrRhs>        & i_rhs
) {
    return w_Derived( i_rhs, i_lhs );
}


/** @} */ // end of AttrMask ( Doxygen group )

#endif // x_at_attr_mask_h_x



