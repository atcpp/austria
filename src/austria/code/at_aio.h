/*
 *  This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *  Any unauthorized use of this file is prohibited and will be prosecuted
 *  to the full extent of the law.
 *
 *  Herein lies a minimal wrapper for asynchronous I/O at the OS level.
 *
 *  This is a quasi-public interface: It should only be consumed by other
 *  portions of the Austria framework.
 *
 *  For the public interface to networking I/O, please see at_net.h.
 */

#ifndef x_at_aio_h_x
#define x_at_aio_h_x 1

#include "at_thread.h"
#include "at_atomic.h"
#include "at_twinmt.h"
#include "at_buffer.h"

#include <utility>
#include <map>
#include <list>
#include <vector>
#include <memory>

namespace at
{
    /** An IPv4 address and port. */
    typedef std::pair<unsigned long, unsigned short> aio_ip4_addr;

    struct aio_hub;

    /**
     * An endpoint (aka file descriptor or socket).  You should treat
     * these as opaque.
     *
     */
    struct aio_fd {
        static const aio_fd invalid;

        int m_opaque_handle;

        aio_fd(int handle = -1) : m_opaque_handle(handle) {}
        ~aio_fd() {}
    };

    struct aio_fd_internal;

    inline bool operator<(const aio_fd &lhs, const aio_fd &rhs)
    {
        return lhs.m_opaque_handle < rhs.m_opaque_handle;
    }
    inline bool operator==(const aio_fd &lhs,const aio_fd &rhs)
    {
        return lhs.m_opaque_handle == rhs.m_opaque_handle;
    }
    inline bool operator!=(const aio_fd &lhs, const aio_fd &rhs)
    {
        return !(lhs == rhs);
    }

    /**
     * This namespace contains only constants that signify flags on
     * endpoints.
     */
    namespace aio_flag
    {
        /** Data is available for reading. */
        extern const int data_available;

        /** A connection is available on a listening endpoint. */
        extern const int connection_waiting;

        /** The endpoint will be closed. */
        extern const int closing;

        /** data can be written to the endpoint **/
        extern const int writeable;

        /**
         * Return a string representation of all the flags that are set in
         * e.
         */
        std::string to_string(int e);
    }

    /**
     * This namespace contains only constants that signify endpoint states.
     */
    namespace aio_state
    {
        /** The endpoint is listening for connections. */
        extern const int listening;

        /** The endpoint is attempting to make a connection. */
        extern const int connecting;

        /** The endpoint is connected. */
        extern const int connected;

        /** The endpoint is listening for datagrams. */
        extern const int datagram;

        /**
         * The endpoint is closed.  Endpoints only exist in this state
         * during a call to aio_monitor::state_changed, which will be the
         * last call to that aio_monitor for the endpoint.
         */
        extern const int closed;

        /**
         * Return a statically allocated string representation of a state,
         * which will match the state's symbol name.
         */
        const char* to_string(int);
    }

    /**
     * This namespace contains only constants that signify failure modes.
     */
    namespace aio_error
    {
        /**
         * An unexpected error occurred.  Were this a debug build, the
         * program would have aborted.
         */
        extern const int unexpected;

        /**
         * The caller lost a race.  This error is returned, for example,
         * when read is called but the data_available flag is no longer
         * set, indicating that someone else read the data before the
         * caller could get to it.
         */
        extern const int loser;

        /**
         * The endpoint was not in the state mandated as a precondition for
         * the call.
         */
        extern const int state;

        /**
         * An invalid argument was passed, such as an endpoint not created
         * by the aio_hub to which the call was made.
         */
        extern const int invalid;

        /**
         * The request cannot be satisfied because the endpoint has been
         * scheduled to be closed.
         */
        extern const int closing;

        /** 
         * The aio_hub is in the process of shutting down.
         */
        extern const int shutdown;

        /**
         * The remote host is unreachable (or is not accepting connections).
         */
        extern const int unreachable;

        /**
         * A write failed because the peer reset the connection.
         */
        extern const int peer_reset;

        /**
         * A bind failed (probably because either we do not have privileges
         * to bind to the requested address or the address is already in
         * use).
         */
        extern const int bind_failure;

        /**
         * An invalid address structure was supplied.
         */
        extern const int invalid_address;

        /**
         * Return a statically allocated string representation of an error
         * code, which will match the error's symbol name.
         */
        const char* to_string(int);
    }

    /**
     * aio_monitor is an interface to be implemented by you.  It defines
     * callbacks that allow you to track the state of an endpoint.  No-op
     * implementations of all of the callbacks are given; you need only
     * implement methods that you care about.
     *
     * These callbacks are made from within a thread that is responsible
     * generating events and processing requests.  Therefore,
     *
     * DO NOT PERFORM TIME CONSUMING ACTIONS FROM WITHIN THESE METHODS.
     * also, the different interfaces defined herein may be called
     * asynchronously from different threads, so it is your responsibility
     * to protect your internal state with appropriate mutex objects
     */
    class aio_monitor_if
    {
    protected:
        /** Do nothing. */
        virtual ~aio_monitor_if();

    public:
        /**
         * Receive notification that you have become the monitor for an
         * endpoint.  This is the first call that will be made to you
         * pertaining to the endpoint, and it will always be made
         * synchronously as part of a call to a public aio_hub method
         * (listen, connect, accept, set_monitor).
         *
         * Postcondition: You will receive all monitor calls pertaining to
         * the enpoint until either the monitor is changed or the endpoint
         * is closed.
         *
         * It is very ill-advised to make calls into the source aio_hub
         * from within this method.
         *
         * @param fd The endpoint for which you are now the monitor
         * @param source The machine managing the endpoint
         */
        virtual void become_monitor(aio_fd fd, aio_hub *source);

        /**
         * Receive notification from an aio_hub that the state of an
         * endpoint has changed.  This implementation does nothing.
         *
         * Precondition: The state or flags associated with fd have changed.
         *
         * Postcondition: None.
         *
         * Note that if state is aio_state::closed, fd is not a valid
         * endpoint to pass to any aio_hub method.
         *
         * @param fd The subject of the call
         * @param source The hub managing fd and making the call
         * @param state A snapshot of the endpoint's state
         * @param state A snapshot of the endpoint's flags
         */
        virtual void state_changed(aio_fd fd, aio_hub *source, int state, int flags);

        /**
         * Receive notification from an aio_hub about a send or a
         * write.  This implementation does nothing.
         *
         * THE BUFFER YOU ARE PASSED MAY NOT PERSIST AFTER RETURN FROM THIS
         * CALL.
         *
         * @param fd The destination of the write, an endpoint in the
         * datagram state
         * @param source The hub managing fd and making the call
         * @param buffer The data that was written
         * @param sent_size The number of bytes written
         * @param unsent_size The number of bytes not written
         * @param dest The remote endpoint
         * @param dest The destination of the send
         * @param usr The usr value passed to aio_hub::write
         * @param err An error, which is only meaningful if
         * unsent_size > 0; in that case, it indicates why the send or
         * write did not complete.
         * @@ TODO rewrite dox
         */
        virtual void send_completed(
            aio_fd fd, aio_hub *source,
            Ptr<Buffer *> i_buffer,
            int i_sent_size,
            aio_ip4_addr dest,
            int err);
    };

/**
 * This typedef is what you should pass pointers to and inherit from,
 * rather than NetResolverLeadIf (which, by itself, lacks the lead-related
 * methods).
 * REV: @@ TODO -- there are ways to enforce this policy
 */
typedef LeadTwinMT<class aio_monitor_if> AioMonitorLead;

    /**
     * aio_hub handles asynchronous I/O (AIO).  It allows you to create
     * new endpoints on which AIO will occur, receive notifications
     * regarding those endpoints, and perform actions on them.
     *
     * Each aio_hub owns a thread that talks to the OS.  This thread is
     * created in the constructor and is terminated in the destructor.
     */
    struct aio_hub_internal;
    struct aio_system_handle;
    struct AsyncSocketContext;

    struct aio_hub {
        /**
         * Create a new asynchronous I/O hub.  Don't make too many.
         * One is ideal.
         *
         * @return A new aio_hub or null if none could be created.
         */
        static aio_hub* new_hub();

        /**
         * Attempt to connect to a remote endpoint over IPv4.
         *
         * Postcondition: On success, a connection attempt to addr is being
         * made.
         *
         * If the connection attempt fails, the monitor will receive a
         * state_changed call.  The state will be aio_state::closed.  No
         * further calls will be made to the monitor.
         *
         * If this method returns false, the monitor pointer will never be
         * dereferenced.  Otherwise, if the monitor pointer is not null, it
         * must remain valid until set_monitor is called with another
         * (possibly null) monitor pointer or an aio_monitor::state_changed
         * call is received and the state is aio_state::closed.
         *
         * @param addr An IPv4 address and port
         * @param monitor A valid aio_monitor or null
         * @param i_stateMutex A shared state mutex for entire lead/aide chain
         * @param fd_o On call, a valid endpoint pointer or null; on success, a
         * valid endpoint; on failure, untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the connect attempt was initiated
         */
        bool connect(   aio_ip4_addr addr, AioMonitorLead *monitor,
                        PtrView<MutexRefCount *> i_stateMutex,
                        aio_fd *fd_o, int *err_o);

        /**
         * Start listening for connections at an IPv4 endpoint.
         *
         * Postcondition: On success, a listening socket has been
         * established and the monitor (if not null) has received an
         * aio_monitor::become_monitor call.
         *
         * If the call fails, the monitor pointer will never be
         * dereferenced.  Otherwise, if the monitor pointer is not null, it
         * must remain valid until set_monitor is called with another
         * (possibly null) monitor pointer or an aio_monitor::state_changed
         * call is received and the state is aio_state::closed.
         *
         * @param addr An IPv4 address and port
         * @param monitor A valid aio_monitor or null
         * @param i_stateMutex A shared state mutex for entire lead/aide chain
         * @param fd_o On call, a valid endpoint pointer or null; on
         * success, a valid endpoint; on failure, untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         */
        bool listen(    aio_ip4_addr addr, AioMonitorLead *monitor,
                        PtrView<MutexRefCount *> i_stateMutex,
                        aio_fd *fd_o, int *err_o);

        /**
         * Start listening for datagrams at an IPv4 endpoint.
         *
         * Postcondition: On success, a listening socket has been
         * established and the monitor (if not null) has received an
         * aio_monitor::become_monitor call.
         *
         * If the call fails, the monitor pointer will never be
         * dereferenced.  Otherwise, if the monitor pointer is not null, it
         * must remain valid until set_monitor is called with another
         * (possibly null) monitor pointer or an aio_monitor::state_changed
         * call is received and the state is aio_state::closed.
         *
         * @param addr An IPv4 address and port
         * @param monitor A valid aio_monitor or null
         * @param i_stateMutex A shared state mutex for entire lead/aide chain
         * @param fd_o On call, a valid endpoint pointer or null; on
         * success, a valid endpoint; on failure, untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         */
        bool listen_datagram(   aio_ip4_addr addr, AioMonitorLead *monitor,
                                PtrView<MutexRefCount *> i_stateMutex,
                                aio_fd *fd_o, int *err_o);

        /**
         * Accept a connection on a listening IPv4 endpoint.
         *
         * Precondition: The connection_waiting flag is set for the
         * endpoint fd and the monitor (if not null) has received an
         * aio_monitor::become_monitor call.
         *
         * Postcondition: On success, a connection is accepted.
         * and the monitor (if not null) has received an
         * aio_monitor::become_monitor call.
         *
         * If the call fails, the monitor pointer will never be
         * dereferenced.  Otherwise, if the monitor pointer is not null, it
         * must remain valid until set_monitor is called with another
         * (possibly null) monitor pointer or an aio_monitor::state_changed
         * call is received and the state is aio_state::closed.
         *
         * @param fd An endpoint in the listening state
         * @param monitor A valid aio_monitor or null
         * @param i_stateMutex A shared state mutex for entire lead/aide chain
         * @param conn_o On call, a valid aio_fd or null; on success, the
         * connection's endpoint; on failure, untouched
         * @param addrp_o On call, a valid aio_ip4_addr or null; on
         * success, the address of the remote endpoint; on failure, untouched
         * @param err_o On call, a valid int or null; on failure, an error
         * code; on success, untouched
         * @return True iff the call succeeded
         */
        bool accept(    aio_fd fd, AioMonitorLead *monitor,
                        PtrView<MutexRefCount *> i_stateMutex,
                        aio_fd *conn_o, aio_ip4_addr *addrp_o, int *err_o);

        /**
         * Set the monitor for an endpoint.
         *
         * Postcondition: The new monitor receives any pending and all
         * future callbacks for fd.  No thread is executing a callback on
         * the old monitor for fd.
         *
         * @param fd An endpoint
         * @param monitor An aio_monitor or null
         * @param i_stateMutex A shared state mutex for entire lead/aide chain
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         */
        bool set_monitor(aio_fd fd, AioMonitorLead *monitor,
                        PtrView<MutexRefCount *> i_stateMutex,
                        int *err_o);

        /**
         * Return the address of the local side of an endpoint.
         *
         * @param fd An endpoint
         * @param addrp_o On call, a valid aio_ip4_addr pointer; on
         * success, the local address of the endpoint; on failure,
         * untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         */
        bool get_local_address(aio_fd fd, aio_ip4_addr *addrp_o, int *err_o);

        /**
         * Return the address of the remote side of an endpoint.
         *
         * @param fd An endpoint
         * @param addrp_o On call, a valid aio_ip4_addr pointer; on
         * success, the remote address of the endpoint; on failure,
         * untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         */
        bool get_remote_address(aio_fd fd, aio_ip4_addr *addrp_o, int *err_o);

        /**
         * Read data from a connected endpoint.
         *
         * Precondition: The data_available flag is set for fd.
         *
         * Postcondition: On success, up to *sizep_io bytes of data are
         * read into buffer_o.
         *
         * Note: If size is zero, the call will return success immediately
         * without evaluating any of its arguments.
         *
         * @param fd A connected endpoint
         * @param buffer_o On call, a valid region of memory; on success,
         * your data; on failure, untouched
         * @param sizep_io On call, the length of buffer_o to; on success,
         * the amount of data actually written to buffer_o; on failure,
         * untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @param True iff the call succeeded
         */
        bool read(aio_fd fd, char *buffer_o, int *sizep_io, int *err_o);

        /**
         * Return a snapshot of the state of an endpoint.  It is not
         * guaranteed to be correct, since it may change before the caller
         * can act upon the information returned.
         *
         * @param fd An endpoint
         * @param state_o On call, a valid int pointer or null; on success,
         * a snapshot of fd's state; on failure, untouched
         * @param flags_o On call, a valid int pointer or null; on success,
         * a snapshot of fd's flags; on failure, untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         */
        bool get_state(aio_fd fd, int *state_o, int *flags_o, int *err_o);

        /**
         * Schedule a write on a connected endpoint.
         *
         * Postcondition: On success, the data is queued for writing within
         * the aio_hub.  An attempt will be made to pass the data to
         * the operating system for writing.  If the monitor for this fd is
         * non-null after the attempt has been made, either a
         * write_succeeded or a write_failed call will be made to the
         * monitor.  On failure, there is no postcondition.  Specifically,
         * no call will be made to the monitor.
         *
         * If size is zero, the call will return success immediately
         * without evaluating any of its arguments.
         *
         * On succcessful return, the caller should not modify or delete
         * the buffer until aio_monitor::write_succeeded or
         * aio_monitor::write_failed has been called; however, the buffer
         * will not be deleted by aio_hub, so the monitor should
         * probably do something to that effect.
         *
         * @param fd A connected endpoint
         * @param buffer A region of memory of length indicated by the size
         * parameter
         * @param size The length of buffer
         * @param usr This will be passed to aio_monitor::write_succeeded
         * or aio_monitor::write_failed; you may place anything you wish
         * in this argument
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         * @@ TODO rewrite dox
         */
        bool write(aio_fd fd, Ptr<Buffer *> i_buffer, int *err_o);

        /**
         * Receive a datagram on an IPv4 endpoint listening for datagrams.
         *
         * Precondition: The data_available flag is set for the endpoint.
         *
         * Postcondition: On success, [part of] a datagram is read.  If the
         * buffer is too short for the datagram, it will be terminated.
         *
         * Note: If size is zero, the call will return success immediately
         * without evaluating any of its arguments.
         *
         * @param fd An endpoint in the datagram state
         * @param buffer_o On call, a region of memory of *sizep_io
         * length; on successful return, packet data of *sizep_io length
         * (*sizep_io is updated during the call to reflect the amount of
         * data read); on failure, untouched
         * @param sizep_io On call, a valid int pointer indicating the
         * maximum number of bytes to read; on success, the
         * length of the datagram; on failure, untouched
         * @param addrp_o On call, a valid aio_ip4_addr pointer or null; on
         * success, the address of the sender; on failure, untouched
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         */
        bool recv(aio_fd fd, char *buffer_o, int *sizep_io,aio_ip4_addr *addrp_o, int *err_o);

        /**
         * Schedule a send on an IPv4 datagram endpoint.
         *
         * Precondition: The endpoint is in the datagram state.
         *
         * Postcondition: The datagram is enqueued for sending in the
         * aio_hub.  An attempt will eventually be made to pass the
         * datagram to the OS.  Once that attempt has been made, if the
         * monitor for the endpoint is non-null, either write_succeeded or
         * write_failed will be called.  On failure, there is no
         * postcondition; notably, no call will be made on the monitor.
         *
         * If size is zero, the call will return success immediately
         * without evaluating any of its arguments.
         *
         * On succcessful return, the caller should not modify or delete
         * the buffer until aio_monitor::send_succeeded or
         * aio_monitor::send_failed has been called; however, the buffer
         * will not be deleted by aio_hub, so the monitor should
         * probably do something to that effect.
         *
         * @param fd An endpoint in the datagram state
         * @param buffer A region of memory of length indicated by the size
         * parameter
         * @param dest The destination address
         * @param usr This will be passed to aio_monitor::send_succeeded
         * or aio_monitor::send_failed; you may place anything you wish
         * in this argument
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succeeded
         * @@ TODO rewrite dox
         */
        bool send(aio_fd fd, Ptr<Buffer *> i_buffer, aio_ip4_addr dest, int *err_o);

        /**
         * Close an endpoint.
         *
         * Postcondition: On success, fd is scheduled to be closed.  All
         * pending requests will be processed and reads may succeed before
         * the close has occurred.  No further requests that would be
         * queued will succeed.  Once the endpoint has been closed, if its
         * monitor is non-null, a state_changed call will be made.
         *
         * Note that if fd is already in the closed state, the call will
         * succeed and no further action will be taken.
         *
         * @param fd An endpoint
         * @param err_o On call, a valid int pointer or null; on failure,
         * an error code; on success, untouched
         * @return True iff the call succceeded
         */
        bool close(aio_fd fd, int *err_o);

        /** Close all endpoints and die (in that order). */
        virtual ~aio_hub();

private:
        /** Not implemented. */
        aio_hub(const aio_hub&);
        /** Not implemented. */
        aio_hub& operator=(const aio_hub&);

        /** Set up shop. */
        aio_hub(void);

        virtual bool queueAsyncSocketContext(PtrView<AsyncSocketContext *> ac);
        virtual Ptr<AsyncSocketContext *> getAsyncSocketContext(aio_fd afd);

private:
        // state
        std::vector <aio_hub_internal *> m_internal_hubs;

        // state Mutex
        Mutex   m_stateMutex;

    };
} // namespace at

#endif // #ifndef x_at_aio_x
