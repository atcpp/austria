/**
 * \file at_log_ctime.h
 *
 * \author Bradley Austin
 *
 * at_log_ctime.h contains an extension to the Austria logging module to
 * support querying the system time using the standard C library time()
 * function.
 *
 */

#ifndef x_at_log_ctime_h_x
#define x_at_log_ctime_h_x 1


#include <ctime>
#include <string>


// Austria namespace
namespace at
{


    class LogManagerTimePolicy_CTime
    {

    public:

        typedef  time_t  t_time_type;

        static t_time_type GetTime()
        {
            return time( 0 );
        }

    protected:

        inline LogManagerTimePolicy_CTime() {}
        inline ~LogManagerTimePolicy_CTime() {}

    private:

        /* Unimplemented */
        LogManagerTimePolicy_CTime( const LogManagerTimePolicy_CTime & );
        LogManagerTimePolicy_CTime & operator=( const LogManagerTimePolicy_CTime & );

    };


    class LogWriterTimeFormatPolicy_CTime_NonThreadSafe
    {

    public:

        typedef  std::string  t_formatted_time_type;

        static t_formatted_time_type FormatTime( time_t i_time )
        {
            tm * l_tm;
            char l_s[26];

            if ( i_time == -1 || ( l_tm = localtime( &i_time ) ) == 0 )
                return "N/A";
            strftime( l_s, 26, "%H:%M:%S  %a %b %d %Y", l_tm );
            return l_s;
        }

    protected:

        inline LogWriterTimeFormatPolicy_CTime_NonThreadSafe() {}
        inline ~LogWriterTimeFormatPolicy_CTime_NonThreadSafe() {}

    private:

        /* Unimplemented */
        LogWriterTimeFormatPolicy_CTime_NonThreadSafe
            ( const LogWriterTimeFormatPolicy_CTime_NonThreadSafe & );
        LogWriterTimeFormatPolicy_CTime_NonThreadSafe &operator=
            ( const LogWriterTimeFormatPolicy_CTime_NonThreadSafe & );

    };


}


#include AT_LOG_CTIME_H


#endif
