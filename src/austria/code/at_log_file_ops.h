/**
 *  \file at_log_file_ops.h
 *
 *  \author Bradley Austin
 *
 */

#ifndef x_at_log_file_ops_h_x
#define x_at_log_file_ops_h_x 1


#include "at_log_rotating_files.h"
#include "at_file.h"
#include "at_types.h"

#include <string>


namespace at
{

    class LogWriterOutputFileOpsPolicy_ATFile
    {

    public:

        static bool FileExists( const std::string & i_path )
        {
            try
            {
                return BaseFile::Exists( FilePath( i_path ) );
            }
            catch ( ... )
            {
                throw LogWriterOutputFileOpsException();
            }
        }

        static Int64 FileSize( const std::string & i_path )
        {
            try
            {
                return BaseFile::GetLength( FilePath( i_path ) );
            }
            catch ( ... )
            {
                throw LogWriterOutputFileOpsException();
            }
        }

        static void DeleteFile( const std::string & i_path )
        {
            try
            {
                if ( ! BaseFile::Remove( FilePath( i_path ) ) )
                {
                    throw LogWriterOutputFileOpsException();
                }
            }
            catch ( ... )
            {
                throw LogWriterOutputFileOpsException();
            }
        }

        static void RenameFile(
            const std::string & i_from,
            const std::string & i_to
        )
        {
            try
            {
                if ( ! BaseFile::Rename(
                           FilePath( i_from ),
                           FilePath( i_to )
                       )
                   )
                {
                    throw LogWriterOutputFileOpsException();
                }
            }
            catch ( ... )
            {
                throw LogWriterOutputFileOpsException();
            }
        }

    };

}


#endif
