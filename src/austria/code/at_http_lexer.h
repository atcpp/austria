/**
 * at_http_lexer.h
 *
 */

#ifndef x_at_http_lexer_h_x
#define x_at_http_lexer_h_x 1

#include "at_exports.h"
#include "at_buffer.h"
#include <map>

// Austria namespace
namespace at
{

class HttpRequestLexer_Basic;
class HttpResponseLexer_Basic;


// ======== HttpLexerTraits ===========================================
/**
 * HttpLexerTraits contains contants that are used to access the
 * maps used to retrieve data from HTTP requests and responses.
 *
 */

class AUSTRIA_EXPORT HttpLexerTraits
{
    public:

    /**
     * s_HTTPVersionStrKey is the key used for the verion
     * string in the parsed maps.
     */

    static const std::string            s_HTTPVersionStrKey;

    enum ParseResult
    {
        /**
         * Invalid indicates that the buffer contents are
         * invalid request or result.
         */
        Invalid,

        /**
         * Incomplete indicates that the parse is incomplete.
         */
        Incomplete,
        
        /**
         * Success indicates that the parse was successful.
         */
        Success

    };

    /**
     * t_MapType is the type of the map used to store data
     * retrieved from the HTTP request string.
     */
    typedef std::map<const std::string, std::string>    t_MapType;

};


// ======== HttpRequestLexer ==========================================
/**
 * HttpRequestLexer can be used to parse all values from a http request.
 *
 */

class AUSTRIA_EXPORT HttpRequestLexer
  : public HttpLexerTraits
{
    public:

    /**
     * s_MethodKey is a string that is the "Method" key for the
     * map.
     */
     
    static const std::string            s_MethodKey;

    /**
     * s_RequestURIKey is a string that is the URI key.
     */
    
    static const std::string            s_RequestURIKey;
    
    // ======== HttpRequestLexer ======================================
    /**
     * HttpRequestLexer constructs a HttpRequestLexer.  The map
     * passed into the HttpRequestLexer will become invalidated
     * if there is an error parsing the buffer.
     *
     * @param o_map contains the map that will be populated
     *          with the parsed elements.
     *
     */
    HttpRequestLexer(
        t_MapType                       * o_map
    );

    ~HttpRequestLexer();

    // ======== ParseSome =============================================
    /**
     * ParseSome will read the contents of the buffer (i_buffer) and
     * place the result into the map passed into the constructor.  If the
     * buffer contents are incomplete, the buffer is cleared and
     * ParseSome may be called multiple times until a the scanning is
     * complete.
     *
     * @param i_buffer contains the contents of the http request
     *          to be parsed.
     * @param i_is_last indicates that the current buffer is the
     *          the last buffer to be parsed.
     * @return A ParseResult
     */

    ParseResult ParseSome(
        PtrView< Buffer * >               i_buffer,
        bool                              i_is_last
    );    

    // ======== Parse =================================================
    /**
     * Parse is a static method that will attempt to parse the
     * contents of the passed in buffer in a single pass.  If the buffer
     * contents are incomplete, the buffer is not modifed (unlike ParseSome).
     * However, the contents of the map becomes undefined if Parse
     * does not return at::HttpLexerTraits::Success.
     *
     * @param o_map contains the map that will be populated
     *          with the parsed elements.
     *
     * @param i_buffer contains the contents of the http request
     *          to be parsed.
     */
    
    static ParseResult Parse(
        t_MapType                       * o_map,
        PtrView< Buffer * >               i_buffer
    );

    static ParseResult Parse(
        t_MapType                       & o_map,
        PtrView< Buffer * >               i_buffer
    ) {
        return Parse( & o_map, i_buffer );
    }

    private:

    /**
     * m_lexer contains a pointer to an internal parser.
     */
    
    HttpRequestLexer_Basic              * m_lexer;
};


// ======== HttpResponseLexer ==========================================
/**
 * HttpResponseLexer can be used to parse all values from a http request.
 *
 */

class AUSTRIA_EXPORT HttpResponseLexer
  : public HttpLexerTraits
{
    public:

    /**
     * s_StatusCodeKey is a string that is the status code key for the
     * map.
     */
     
    static const std::string            s_StatusCodeKey;

    /**
     * s_StatusTextKey is a string that is the status text key
     */
    
    static const std::string            s_StatusTextKey;
    
    // ======== HttpResponseLexer ======================================
    /**
     * HttpResponseLexer constructs a HttpResponseLexer.  The map
     * passed into the HttpResponseLexer will become invalidated
     * if there is an error parsing the buffer.
     *
     * @param o_map contains the map that will be populated
     *          with the parsed elements.
     *
     */
    HttpResponseLexer(
        t_MapType                       * o_map
    );

    ~HttpResponseLexer();

    // ======== ParseSome =============================================
    /**
     * ParseSome will read the contents of the buffer (i_buffer) and
     * place the result into the map passed into the constructor.  If the
     * buffer contents are incomplete, the buffer is cleared and
     * ParseSome may be called multiple times until a the scanning is
     * complete.
     *
     * @param i_buffer contains the contents of the http request
     *          to be parsed.
     * @param i_is_last indicates that the current buffer is the
     *          the last buffer to be parsed.
     * @return A ParseResult
     */

    ParseResult ParseSome(
        PtrView< Buffer * >               i_buffer,
        bool                              i_is_last
    );    

    // ======== Parse =================================================
    /**
     * Parse is a static method that will attempt to parse the
     * contents of the passed in buffer in a single pass.  If the buffer
     * contents are incomplete, the buffer is not modifed (unlike ParseSome).
     * However, the contents of the map becomes undefined if Parse
     * does not return at::HttpLexerTraits::Success.
     *
     * @param o_map contains the map that will be populated
     *          with the parsed elements.
     *
     * @param i_buffer contains the contents of the http request
     *          to be parsed.
     */
    
    static ParseResult Parse(
        t_MapType                       * o_map,
        PtrView< Buffer * >               i_buffer
    );

    static ParseResult Parse(
        t_MapType                       & o_map,
        PtrView< Buffer * >               i_buffer
    ) {
        return Parse( & o_map, i_buffer );
    }

    private:

    /**
     * m_lexer contains a pointer to an internal parser.
     */
    
    HttpResponseLexer_Basic              * m_lexer;
};



}; // namespace

#endif // x_at_http_lexer_h_x


