/**
 *  \file at_preference_xml.h
 *
 *  \author Bradley Austin
 *
 */

#ifndef x_at_preference_xml_h_x
#define x_at_preference_xml_h_x 1

#include "at_preference.h"
#include "at_lifetime.h"
#include "at_xml_document.h"
#include "at_xml_element.h"
#include "at_xml_node.h"


namespace at
{


    class XmlPreferenceDocumentReaderErrorPolicy_Null;


    template<
        class w_error_policy = XmlPreferenceDocumentReaderErrorPolicy_Null
    >
    class XmlPreferenceDocumentReader
      : public w_error_policy
    {

        using w_error_policy::ReportMissingRootElementError;
        using w_error_policy::ReportRootElementWrongTagNameError;
        using w_error_policy::ReportRootElementUnknownAttributeError;
        using w_error_policy::ReportPreferenceElementWrongNodeTypeError;
        using w_error_policy::ReportPreferenceElementWrongTagNameError;
        using w_error_policy::ReportPreferenceElementMissingNameAttributeError;
        using w_error_policy::ReportPreferenceElementMissingContextAttributeError;
        using w_error_policy::ReportPreferenceElementMissingTypeAttributeError;
        using w_error_policy::ReportPreferenceElementUnknownAttributeError;
        using w_error_policy::ReportDuplicateNameAndContextError;
        using w_error_policy::ReportPreferenceMissingValueError;
        using w_error_policy::ReportValueElementWrongNodeTypeError;
        using w_error_policy::ReportPreferenceMultipleValuesError;
        using w_error_policy::ReportValueElementUnknownTagNameError;
        using w_error_policy::ReportAtomMissingValueAttributeError;
        using w_error_policy::ReportAtomUnknownAttributeError;
        using w_error_policy::ReportVectorUnknownAttributeError;


        bool ReadValue(
            PtrView< const XmlElement * > i_element,
            MultiString &o_ms
        )
        {
            MultiString l_ms;
            std::string l_tag_name = i_element->TagName();
            if ( l_tag_name == "atom" )
            {
                if ( ! i_element->HasAttribute( "value" ) )
                {
                    ReportAtomMissingValueAttributeError();
                    return false;
                }
                if ( i_element->Attributes().size() != 1 )
                {
                    ReportAtomUnknownAttributeError();
                    return false;
                }
                l_ms = i_element->GetAttribute( "value" );
            }
            else if ( l_tag_name == "vector" )
            {
                if ( i_element->HasAttributes() )
                {
                    ReportVectorUnknownAttributeError();
                    return false;
                }
                l_ms = std::vector< MultiString >();
                for ( PtrView< const XmlNode * > l_child =
                          i_element->FirstChild();
                      l_child;
                      l_child = l_child->NextSibling()
                    )
                {
                    switch ( l_child->Type() )
                    {
                        case XmlNode::ELEMENT_NODE :
                            l_ms.Vec().push_back( MultiString() );
                            if ( ! ReadValue(
                                       l_child->AsElement(),
                                       l_ms.Vec().back()
                                   )
                               )
                            {
                                return false;
                            }
                            break;

                        case XmlNode::COMMENT_NODE :
                        case XmlNode::PROCESSING_INSTRUCTION_NODE :
                            break;

                        case XmlNode::TEXT_NODE :

                            /*
                             *  TO DO: Verify text consists only of whitespace,
                             *  otherwise return error.
                             */

                            break;

                        default:
                            ReportValueElementWrongNodeTypeError();
                            return false;
                    }
                }
            }
            else
            {
                ReportValueElementUnknownTagNameError();
                return false;
            }
            o_ms.swap( l_ms );
            return true;
        }


    public:

        bool Read(
            PtrView< const XmlDocument * > i_document,
            PreferenceValueTable & o_table
        )
        {
            const PtrView< const XmlElement * > l_root =
                i_document->DocumentElement();
            if ( ! l_root )
            {
                ReportMissingRootElementError();
                return false;
            }
            if ( l_root->TagName() != "preferences" )
            {
                ReportRootElementWrongTagNameError();
                return false;
            }
            if ( l_root->HasAttributes() )
            {
                ReportRootElementUnknownAttributeError();
                return false;
            }
            PreferenceValueTable l_table;
            for ( PtrView< const XmlNode * > l_child = l_root->FirstChild();
                  l_child;
                  l_child = l_child->NextSibling()
                )
            {
                switch ( l_child->Type() )
                {
                    case XmlNode::ELEMENT_NODE :
                        {
                            const PtrView< const XmlElement * > l_element =
                                l_child->AsElement();
                            if ( l_element->TagName() != "preference" )
                            {
                                ReportPreferenceElementWrongTagNameError();
                                return false;
                            }
                            if ( ! l_element->HasAttribute( "name" ) )
                            {
                                ReportPreferenceElementMissingNameAttributeError();
                                return false;
                            }
                            if ( ! l_element->HasAttribute( "context" ) )
                            {
                                ReportPreferenceElementMissingContextAttributeError();
                                return false;
                            }
                            if ( ! l_element->HasAttribute( "type" ) )
                            {
                                ReportPreferenceElementMissingTypeAttributeError();
                                return false;
                            }
                            if ( l_element->Attributes().size() != 3 )
                            {
                                ReportPreferenceElementUnknownAttributeError();
                                return false;
                            }
                            const std::string l_name =
                                l_element->GetAttribute( "name" );
                            const std::string l_context =
                                l_element->GetAttribute( "context" );
                            {
                                PreferenceValueTable::LookupResult
                                    l_lookup_result = l_table.Lookup(
                                        l_name,
                                        l_context
                                    );
                                if ( l_lookup_result.Found() &&
                                     l_lookup_result.Context() == l_context
                                   )
                                {
                                    ReportDuplicateNameAndContextError();
                                    return false;
                                }
                            }
                            const std::string l_type_name =
                                l_element->GetAttribute( "type" );
                            MultiString l_ms;
                            int l_value_count = 0;
                            for ( PtrView< const XmlNode * > l_grandchild =
                                      l_element->FirstChild();
                                  l_grandchild;
                                  l_grandchild = l_grandchild->NextSibling()
                                )
                            {
                                switch ( l_grandchild->Type() )
                                {
                                    case XmlNode::ELEMENT_NODE :
                                        if ( ++l_value_count > 1 )
                                        {
                                            ReportPreferenceMultipleValuesError();
                                            return false;
                                        }
                                        if ( ! ReadValue(
                                                   l_grandchild->AsElement(),
                                                   l_ms
                                               )
                                           )
                                        {
                                            return false;
                                        }
                                        break;

                                    case XmlNode::COMMENT_NODE :
                                    case XmlNode::PROCESSING_INSTRUCTION_NODE :
                                        break;

                                    case XmlNode::TEXT_NODE :

                                        /*
                                         *  TO DO: Verify text consists only of whitespace,
                                         *  otherwise return error.
                                         */

                                        break;

                                    default:
                                        ReportValueElementWrongNodeTypeError();
                                        return false;
                                }
                            }
                            if ( l_value_count == 0 )
                            {
                                ReportPreferenceMissingValueError();
                                return false;
                            }
                            l_table.SetValue
                                ( l_name, l_context, l_type_name, l_ms );
                        }
                        break;

                    case XmlNode::COMMENT_NODE :
                    case XmlNode::PROCESSING_INSTRUCTION_NODE :
                        break;

                    case XmlNode::TEXT_NODE :

                        /*
                         *  TO DO: Verify text consists only of whitespace,
                         *  otherwise return error.
                         */

                        break;

                    default:
                        ReportPreferenceElementWrongNodeTypeError( l_child->Type() );
                        return false;
                }
            }
            o_table.swap( l_table );
            return true;
        }


    };


    class XmlPreferenceDocumentReaderErrorPolicy_Null
    {

    public:

        inline void ReportMissingRootElementError() {}

        inline void ReportRootElementWrongTagNameError() {}

        inline void ReportRootElementUnknownAttributeError() {}

        inline void ReportPreferenceElementWrongNodeTypeError( unsigned short i_type ) {}

        inline void ReportPreferenceElementWrongTagNameError() {}

        inline void ReportPreferenceElementMissingNameAttributeError() {}

        inline void ReportPreferenceElementMissingContextAttributeError() {}

        inline void ReportPreferenceElementMissingTypeAttributeError() {}

        inline void ReportPreferenceElementUnknownAttributeError() {}

        inline void ReportDuplicateNameAndContextError() {}

        inline void ReportPreferenceMissingValueError() {}

        inline void ReportValueElementWrongNodeTypeError() {}

        inline void ReportPreferenceMultipleValuesError() {}

        inline void ReportValueElementUnknownTagNameError() {}

        inline void ReportAtomMissingValueAttributeError() {}

        inline void ReportAtomUnknownAttributeError() {}

        inline void ReportVectorUnknownAttributeError() {}

    };


    void WriteXmlPreferenceDocumentMultiString(
        PtrView< XmlDocument * > i_doc,
        PtrView< XmlElement * > o_element,
        const MultiString & i_ms
    )
    {
        AT_Assert( i_ms.IsString() || i_ms.IsVector() );
        if ( i_ms.IsVector() )
        {
            Ptr< XmlElement * > l_vector = i_doc->CreateElement( "vector" );
            o_element->AppendChild( l_vector );
            const std::vector< MultiString > & l_vec = i_ms.Vec();
            for ( std::vector< MultiString >::const_iterator
                      l_iter = l_vec.begin(),
                      l_end = l_vec.end();
                  l_iter != l_end;
                  ++l_iter
                )
            {
                WriteXmlPreferenceDocumentMultiString
                    ( i_doc, l_vector, *l_iter );
            }
        }
        else
        {
            Ptr< XmlElement * > l_atom = i_doc->CreateElement( "atom" );
            o_element->AppendChild( l_atom );
            l_atom->SetAttribute( "value", i_ms.Str() );
        }
    }


    PtrDelegate< XmlDocument * > WriteXmlPreferenceDocument(
        const PreferenceValueTable & o_table
    )
    {
        Ptr< XmlDocument * > l_doc = new XmlDocument;
        Ptr< XmlElement * > l_root = l_doc->CreateElement( "preferences" );
        l_doc->AppendChild( l_root );
        for ( PreferenceValueTable::const_iterator
                  l_iter = o_table.begin(),
                  l_end = o_table.end();
              l_iter != l_end;
              ++l_iter
            )
        {
            Ptr< XmlElement * > l_pref = l_doc->CreateElement( "preference" );
            l_root->AppendChild( l_pref );
            l_pref->SetAttribute( "name", l_iter->Name() );
            l_pref->SetAttribute( "context", l_iter->Context() );
            l_pref->SetAttribute( "type", l_iter->Type() );
            WriteXmlPreferenceDocumentMultiString
                ( l_doc, l_pref, l_iter->Value() );
        }
        return l_doc;
    }


}


#endif
