//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *  at_gx86_file.cpp
 *
 *  This contains the gx86 implementation of at_file.h.
 *
 */

#include "at_exports.h"
#include "at_factory.h"
#include "at_source_locator.h"
#include "at_types.h"
#include "at_lifetime.h"
#include "at_file.h"
#include "at_thread.h"
#include "at_start_up.h"
#include "errno.h"
#include <sys/aio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


#include <map>
#include <string>

// Austria namespace
namespace at
{
    // Register the fileinitializer
    AT_MakeFactory2P( "FileInitializer", FileInitializer, Initializer, DKy, int & , const char ** & );
    
    //////////////////////////////////////////////////////////////
    //
    //  FileError for gx86 is implemented as FileErrorBasic
    //
    //////////////////////////////////////////////////////////////

    // ======== std::map<long,FileErrorBasic*> g_errormap ===================================================
    /**
     *      g_errormap contains the map of OS errorcodes to File Errors
     */
    static std::map<long,FileErrorBasic*> s_errormap;

    // Initialize all the Custom Errors with their error messages
    static FileErrorBasic s_success_basic               ( "Success", 0 ); 
    static FileErrorBasic s_end_of_file_basic           ( "End of File (EOF)" );
    static FileErrorBasic s_waiting_read_basic          ( "Waiting for an ASYNC Read to Finish" );
    static FileErrorBasic s_waiting_write_basic         ( "Waiting for an ASync Write to Finish" );
    static FileErrorBasic s_waiting_open_basic          ( "Waiting for an ASync Open to Complete" );
    static FileErrorBasic s_waiting_close_basic         ( "Waiting for an ASync Close to Complete" );
    static FileErrorBasic s_file_not_found_basic        ( "File not Found", ENOENT );
    static FileErrorBasic s_bad_path_basic              ( "Bad Path" );
    static FileErrorBasic s_too_many_open_files_basic   ( "Too many open files", EMFILE );
    static FileErrorBasic s_access_denied_basic         ( "Access Denied", EACCES );
    static FileErrorBasic s_invalid_file_basic          ( "Invalid File", EBADF );
    static FileErrorBasic s_remove_current_dir_basic    ( "Remove Current Directory Failed", ENOTDIR );
    static FileErrorBasic s_directory_full_basic        ( "Directory is Full" );
    static FileErrorBasic s_bad_seek_basic              ( "Negative Seek", ESPIPE );
    static FileErrorBasic s_hard_io_basic               ( "Hard Disk Error", EIO );
    static FileErrorBasic s_sharing_violation_basic     ( "Sharing Violation" );
    static FileErrorBasic s_lock_violation_basic        ( "Lock Violation" );
    static FileErrorBasic s_disk_full_basic             ( "Disk Full", ENOSPC );   
    static FileErrorBasic s_bad_param                   ( "Invalid Parameter", EINVAL );
    static FileErrorBasic s_overflow                    ( "Buffer Overflow", EMSGSIZE );
    static FileErrorBasic s_too_many_async_calls        ( "Too many Async Calls", EAGAIN );
    static FileErrorBasic s_user_aborted                ( "Canceled by User", ECANCELED );
    static FileErrorBasic s_already_exists              ( "File already exists", EEXIST );

    // Assign the Static FileErrorBasic members
    const FileError &FileError::s_success               = s_success_basic;
    const FileError &FileError::s_end_of_file           = s_end_of_file_basic;
    const FileError &FileError::s_waiting_read          = s_waiting_read_basic;
    const FileError &FileError::s_waiting_write         = s_waiting_write_basic;
    const FileError &FileError::s_waiting_open          = s_waiting_open_basic;
    const FileError &FileError::s_waiting_close         = s_waiting_close_basic;
    const FileError &FileError::s_file_not_found        = s_file_not_found_basic;
    const FileError &FileError::s_bad_path              = s_bad_path_basic;
    const FileError &FileError::s_too_many_open_files   = s_too_many_open_files_basic;
    const FileError &FileError::s_access_denied         = s_access_denied_basic;
    const FileError &FileError::s_invalid_file          = s_invalid_file_basic;
    const FileError &FileError::s_remove_current_dir    = s_remove_current_dir_basic;
    const FileError &FileError::s_directory_full        = s_directory_full_basic;
    const FileError &FileError::s_bad_seek              = s_bad_seek_basic;
    const FileError &FileError::s_hard_io               = s_hard_io_basic;
    const FileError &FileError::s_sharing_violation     = s_sharing_violation_basic;
    const FileError &FileError::s_lock_violation        = s_lock_violation_basic;
    const FileError &FileError::s_disk_full           	= s_disk_full_basic;
    const FileError &FileError::s_bad_param             = s_bad_param;
    const FileError &FileError::s_overflow              = s_overflow;
    const FileError &FileError::s_too_many_async_calls  = s_too_many_async_calls;
    const FileError &FileError::s_user_aborted          = s_user_aborted;
    const FileError &FileError::s_already_exists        = s_already_exists;

    // ======== FileErrorBasic FileErrorBasic( const char* i_name, long i_errorno ) ====================
    FileErrorBasic::FileErrorBasic ( const char* i_name, long i_errorno ) : m_errortext ( i_name ), m_errorno ( i_errorno )
    {
        if ( i_errorno != -1 )
        {
            s_errormap[i_errorno] = this;
        }
    }

    // ======== FileErrorBasic FileErrorBasic( long i_errorno ) ========================================
    FileErrorBasic::FileErrorBasic ( long i_errorno )
    {
        FileErrorBasic* fe = s_errormap[i_errorno];
        if ( fe )
        {
            *this = fe;
        }
        else
        {
            AT_Assert ( false ); // This OS error (i_errorno) needs to be added to the global list above. See Bill. For Gx86 Lookup Errno Description
        } 
    }

    FileErrorBasic::FileErrorBasic ( const FileError& i_fileerror )
    {
        m_errortext = i_fileerror.What ();
        m_errorno = i_fileerror.ErrorCode ();
    }

    // ======== FileErrorBasic & FileErrorBasic::operator = ( const FileErrorBasic* i_fileerror )================

    const FileErrorBasic & FileErrorBasic::operator = ( const FileErrorBasic* i_fileerror )
    { 
        m_errortext = i_fileerror->What ();
        m_errorno = i_fileerror->ErrorCode ();

        return *this; 
    }

    // ======== FileAttr_List_x( AT_StaticVariableDefn ) ========================================
    /**
     *      Define the static variables needed for attribute handling
     */
    FileAttr_List_x( AT_StaticVariableDefn )

    /////////////////////////////////////////////////////////////////////
    //
    // GX86 Implementation of BaseFile 
    //
    /////////////////////////////////////////////////////////////////////

    const int BaseFile::SeekBegin = SEEK_SET; 
    const int BaseFile::SeekCurrent = SEEK_CUR;
    const int BaseFile::SeekEnd = SEEK_END; 

    /**
    *  m_filelock is used to lock the BaseFile static functions
    */
    at::Mutex BaseFile::m_filelock;

    // ======== BaseFile::Exists ========================================
    bool BaseFile::Exists ( const FilePath & i_file_path )
    {
        Lock<Mutex> m_l ( m_filelock );

	    return access ( i_file_path.String (), F_OK ) == 0;
    }

    // ======== BaseFile::Rename ========================================
    bool BaseFile::Rename ( const FilePath & i_file_pathold, const FilePath & i_file_pathnew )
    {
        Lock<Mutex> m_l ( m_filelock );

        return -1 != std::rename ( i_file_pathold.String (), i_file_pathnew.String () );
    }

    // ======== BaseFile::Remove ========================================
    bool BaseFile::Remove ( const FilePath & i_file_path )
    {
        Lock<Mutex> m_l ( m_filelock );

        return -1 != unlink ( i_file_path.String () );	
    }

    // ======== BaseFile::Open ( i_file_path, i_attr ) =======================
    bool BaseFile::Open ( const FilePath & i_file_path, const FileAttr &i_attr )
    {
        // Assert if already open 
        AT_Assert ( m_filehandle.fildes == -1 );

        // Adjust the Mode to match this class type
        FileAttr attr = i_attr;
        if ( false == CheckAttributes ( &attr ) )
        {
            return false;
        }

        int openmode = 0;
        int mode = 0;

        // Read Access
        if ( attr & FileAttr::Read ) 
        {
            openmode = O_RDONLY;
            mode = S_IRUSR;
        }

        // Write and preserve
        if ( attr & FileAttr::Write  ) 
        {
            openmode = O_WRONLY;
            mode = S_IWUSR;
        }

        // Read/Write 
        if ( attr & FileAttr::ReadWrite ) 
        {
            openmode = O_RDWR;
            mode = S_IRUSR | S_IWUSR;
        }

        // Create 
        if ( attr & FileAttr::Create ) 
        {
            openmode |= O_CREAT;
        }

        // Preserve only if Write
        if ( attr & FileAttr::Preserve ) 
        {
            if ( attr & FileAttr::Write )
            {
                openmode |= O_APPEND;
            }
        }

        if ( attr & FileAttr::Mapped )
        {
            AT_Assert ( false );
        }

        if ( attr & FileAttr::DirectIO )
        {
            // REV: This flag does not exist under OS X.  Should we fail
            // or should we ignore it?
            AT_Assert ( false );
            //openmode |= O_DIRECT;
        }

        if ( attr & FileAttr::Async )
        {
        }

        int filedes = open ( i_file_path.String (), openmode, mode );

        if ( filedes != -1 )
        {
            m_filehandle.fildes = filedes;
            m_fullname = i_file_path;
            m_mode = i_attr;
            m_closeondelete = true;

            return true;
        }
        else
        {
#ifdef FILESTRICT
            // Throw an Exception, the file should be able to open
            throw FileErrorBasic ( errno );
#else
            // Record the Error
            SetError ( errno );
#endif
        }

        return false;
    }

    // ======== BaseFile::Open ===============================================
    bool BaseFile::Open ()
    {
        return Open ( m_fullname, m_mode ); 
    }

    // ======== Operations: BaseFile::IsOpen =================================
    bool BaseFile::IsOpen ()
    {
        return m_filehandle.fildes != -1;
    }

    // ======== BaseFile::AbortBlockingCall ==================================
    bool BaseFile::AbortBlockingCall ()
    {
        AT_Assert ( m_filehandle.fildes != -1 );

		struct aiocb aiodata; 
		bzero( &aiodata, sizeof (struct aiocb)); 
		aiodata.aio_fildes = m_filehandle.fildes; 
		aiodata.aio_offset = 0; 
		aiodata.aio_sigevent.sigev_notify = SIGEV_NONE; 
		
        return aio_cancel ( m_filehandle.fildes, &aiodata );
    }

    // ======== BaseFile::Flush () ==========================================
    bool BaseFile::Flush ()
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        if ( -1 == fsync ( m_filehandle.fildes ) )
        {
            SetError ( errno );

            return false;
        }
    
        return true;
    }

    // ======== BaseFile::Close  ()===========================================
    void BaseFile::Close () 
    {
        AT_Assert ( m_locked == 0 );

        int error = 0;
        // If we have a valid File handle then Close
        if ( m_filehandle.fildes != -1 )
        {
            error = close ( m_filehandle.fildes );
        }

        m_filehandle.fildes = -1;
        m_closeondelete = false;

        // There was an error trying to Close the File
        if ( error )
        {
            throw FileErrorBasic ( errno );
        }

    }

    // ======== BaseFile::GetError () ========================================
    const FileError& BaseFile::GetError () const 
    {
        AT_Assert ( m_lasterror != 0 );

        return *m_lasterror;
    }
    
    // ======== BaseFile::SetError ( long i_error ) ==========================
    void BaseFile::SetError ( long i_error )
    {
        if ( m_lasterror )
        {
            delete m_lasterror; 
        }

        m_lasterror = new FileErrorBasic ( i_error );
    }

    // ======== BaseFile::( const FileError& i_error ) =======================
    void BaseFile::SetError ( const FileError& i_error )
    {
        SetError ( i_error.ErrorCode () );
    }

    // ======== BaseFile::( long i_off, long i_from ) ========================
    Int64 BaseFile::Seek ( Int64 i_off, long i_from ) const 
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        AT_Assert ( i_from == SEEK_SET || i_from == SEEK_END || i_from == SEEK_CUR );

        const Int64 seekerror = -1;
        long whence = i_from;

        Int64 res = lseek ( m_filehandle.fildes, i_off, whence ); 
        if ( seekerror == res  )
        {
            throw FileErrorBasic ( errno );
        }

        return res;
    }

    // ======== BaseFile::GetPosition () =====================================
    Int64 BaseFile::GetPosition () const 
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const Int64 seekerror = -1;
        Int64 pos = lseek ( m_filehandle.fildes, 0, SEEK_CUR );

        if ( seekerror == pos )
        {
            throw FileErrorBasic ( errno );
        }
        
        return pos;
    }

    // ======== BaseFile::SetLength ( long i_newlen ) ========================
    bool BaseFile::SetLength ( Int64 i_newlen )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const Int64 seekerror = -1;
        
        if ( seekerror == ftruncate ( m_filehandle.fildes, i_newlen ) )
        {
            SetError ( errno );
            return false;
        }

        return true;
    }
    
    // ======== BaseFile::GetLength () =======================================
    Int64 BaseFile::GetLength () const 
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const Int64 seekerror = -1;
        struct stat info;

        if ( seekerror == fstat ( m_filehandle.fildes, &info ) )
        {
            throw FileErrorBasic ( errno );
        }
            
        return info.st_size;
    }

    // ======== BaseFile::LockRange( Int64 i_pos, INt64 i_count ) ==============
    void BaseFile::LockRange( Int64 i_pos, Int64 i_count )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        try 
        {
            Seek ( i_pos, SEEK_SET );
        }
        catch ( const FileErrorBasic& fe )
        {
            throw fe;
        }

        if ( -1 == lockf ( m_filehandle.fildes, F_LOCK, i_count ) )
        {
            throw FileErrorBasic ( errno );
        }

        m_locked++;

    }

    // ======== BaseFile::UnlockRange( Int64 i_pos, Int64 i_count ) ============
    void BaseFile::UnlockRange( Int64 i_pos, Int64 i_count )
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        AT_Assert ( m_locked > 0 );

        try 
        {
            Seek ( i_pos, SEEK_SET );
        }
        catch ( const FileErrorBasic& fe )
        {
            throw fe;
        }

        if ( -1 == lockf( m_filehandle.fildes, F_ULOCK, i_count ) )
        {
            throw FileErrorBasic ( errno );
        }

        m_locked--;
    }

/** 
 *  S_IRUSR   00400 read by owner
 *
 *  S_IWUSR   00200 write by owner
 *
 *  S_IXUSR   00100 execute/search by owner
 *
 *  S_IRGRP   00040 read by group
 *
 *  S_IWGRP   00020 write by group
 *
 *  S_IXGRP   00010 execute/search by group
 *
 *  S_IROTH   00004 read by others
 *
 *  S_IWOTH   00002 write by others
 *
 *  S_IXOTH   00001 execute/search by others
**/


    // ======== BaseFile::SetPermissions ( int i_permissions ) ===============
    void BaseFile::SetPermissions ( int i_permissions )
    {
        long attributes = 0;

        if ( i_permissions & FilePermission::UserWrite  ) 
        {
            attributes |= S_IWUSR; 
        }

        if ( i_permissions & FilePermission::GroupWrite ) 
        {
            attributes |= S_IWGRP; 
        }

        if ( i_permissions & FilePermission::WorldWrite ) 
        {
            attributes |= S_IWOTH; 
        }

	    if ( i_permissions & FilePermission::UserRead  ) 
        {
            attributes |= S_IRUSR; 
        }
	    if ( i_permissions & FilePermission::GroupRead ) 
        {
            attributes |= S_IRGRP; 
        }

	    if ( i_permissions & FilePermission::WorldRead ) 
        {
            attributes |= S_IROTH; 
        }

        if ( i_permissions & FilePermission::UserExecutable  ) 
        {
            attributes |= S_IXUSR; 
        }

        if ( i_permissions & FilePermission::GroupExecutable ) 
        {
            attributes |= S_IXGRP; 
        }

        if ( i_permissions & FilePermission::WorldExecutable ) 
        {
            attributes |= S_IXOTH; 
        }
        
        if ( m_filehandle.fildes != -1 ) // Not 0 or -1 
        {
        	if ( -1 == fchmod ( m_filehandle.fildes, attributes ) )
        	{
            	throw FileErrorBasic ( errno );
        	}
        }
        else
        {
        	if ( -1 == chmod ( m_fullname.String (), attributes ) )
        	{
        		throw ( FileErrorBasic ( errno ) );
        	}
        }
    }

    // ======== BaseFile::GetPermissions( ) ===============
    int BaseFile::GetPermissions ( )
    {
        struct stat * l_stats;
        if ( m_filehandle.fildes != -1) // Not 0 or -1
        {       
                if ( -1 == fstat ( m_filehandle.fildes, l_stats ) )
                {
                        throw FileErrorBasic( errno );
                }
        }
        else
        {
                if ( -1 == stat ( m_fullname.String (), l_stats ) )
                {
                        throw ( FileErrorBasic ( errno ) );
                }
        }

        if (! l_stats)
        {
          throw ( FileErrorBasic ( errno ) );
        }

        int l_attributes = l_stats->st_mode;

        long l_permissions = 0;

        if ( l_attributes & S_IWUSR )
        {
          l_permissions |= FilePermission::UserWrite;
        }

        if ( l_attributes & S_IWGRP )
        {
          l_permissions |= FilePermission::GroupWrite;
        }
        
        if ( l_attributes & S_IWOTH )
        {
          l_permissions |= FilePermission::WorldWrite;
        }

        if ( l_attributes & S_IRUSR )
        {
          l_permissions |= FilePermission::UserRead;
        }

        if ( l_attributes & S_IRGRP )
        {
          l_permissions |= FilePermission::GroupRead;
        }
        
        if ( l_attributes & S_IROTH )
        {
          l_permissions |= FilePermission::WorldRead;
        }

        if ( l_attributes & S_IXUSR )
        {
          l_permissions |= FilePermission::UserExecutable;
        }

        if ( l_attributes & S_IXGRP )
        {
          l_permissions |= FilePermission::GroupExecutable;
        }
        
        if ( l_attributes & S_IXOTH )
        {
          l_permissions |= FilePermission::WorldExecutable;
        }

        return l_permissions;       
    }


    // ======== BaseFile::GetDateTime ( int i_flag, TimeStamp* i_timestamp ) =
    bool BaseFile::GetDateTime ( int i_flag, TimeStamp* i_timestamp ) const
    {
        return false;
    }

    // ======== BaseFile::~BaseFile () =======================================
    BaseFile::~BaseFile ()
    {
        try
        {
            if ( true == m_closeondelete )
            {
                Close ();
	    	}

            if ( m_lasterror )
            {
				delete m_lasterror;
	    	}	
        }
        catch ( const FileErrorBasic& fe )
        {
            std::string error ( " Error occured during basfile object destruction - " );
            error += fe.What ();
            std::cerr << error; // Not Sure how handle this error
        }
    }

    // ======== BaseFile::BaseFile ( const FilePath&, int ) ==================
    BaseFile::BaseFile ( const FilePath & i_file_path, const FileAttr &i_attr )
    {
        m_filehandle.fildes = -1;
        m_fullname      	= i_file_path;
        m_mode          	= i_attr; // Mode is verified In Open
        m_closeondelete 	= false;
        m_lasterror     	= 0;
        m_locked        	= 0;
        // Create a recursive mutex for thread synchronization on lead/aide twins...
        m_mutex = new MutexRefCount( Mutex::Recursive );
    }

    // ======== BaseFile::BaseFile () ========================================
    BaseFile::BaseFile ()
    {
        m_mode          	= FileAttr::Read;
        m_filehandle.fildes = -1;
        m_closeondelete 	= false;
        m_lasterror     	= 0;
        m_locked        	= 0;
        // Create a recursive mutex for thread synchronization on lead/aide twins...
        m_mutex = new MutexRefCount( Mutex::Recursive );
    }

    // ======== BaseFile::CheckAttributes ( FileAttr *i_attr ) ===============
    bool BaseFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Should never be called so always fails
        return false;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Win32 Implementation of RFile
    // REV: ??!?!  This came from at_gx86_file.cpp.
    //
    /////////////////////////////////////////////////////////////////////

    // ======== RFile::RFile ( const FilePath&, int ) ==============
    RFile::RFile ( const FilePath & i_file_path, const FileAttr& i_attr ) : BaseFile ( i_file_path, i_attr )
    {
    }

    // ======== RFile::RFile () ========================================
    RFile::RFile () : BaseFile ()
    {
    }

    // ======== RFile::Read ( at::PtrDelegate< Buffer* > i_br ) =======
    bool RFile::Read ( at::PtrDelegate< Buffer * > i_br )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        if ( i_br.Get()->Region().m_allocated == 0 )
        {
            return 0;   // 0 buffer yah get nuthin
        }

        long bytesread = read ( m_filehandle.fildes, i_br.Get ()->Region().m_mem, 
				i_br.Get()->Region().m_allocated );

        if ( bytesread == 0 )
        {
            SetError ( errno );
            
            return -1;
        }

        i_br.Get()->SetAllocated ( bytesread );

        return bytesread;
    }

    // ======== RFile::Open ( const FilePath&, const int )================
    bool RFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr  )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== RFile::Open () ===========================================
    bool RFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== RFile::CheckAttributes ( int i_attr ) ====================
    bool RFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we only have Read attributes 

        FileAttr mode = *i_attr;

        // Write not Allowed
        if ( mode & FileAttr::Write ) 
        {
            return false;
        }

        // ReadWrite not Allowed
        if ( mode & FileAttr::ReadWrite ) 
        {
            return false;  
        }

        // Can't Create a read only file
        if ( mode & FileAttr::Create )
        {
            return false;
        }

        // Must Have Read Attribute
        if ( !( mode & FileAttr::Read ) )
        {
            mode.Add ( FileAttr::Read );        
        }

        *i_attr = mode;

        return true;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Win32 Implementation of WFile
    //
    /////////////////////////////////////////////////////////////////////

    // ======== WFile::WFile ( const FilePath&, int ) ====================
    WFile::WFile ( const FilePath & i_file_path, const FileAttr& i_attr  ) : BaseFile ( i_file_path, i_attr ) 
    {
    }

    // ======== WFile::WFile () ==========================================
    WFile::WFile () : BaseFile ()
    {
    }

    // ======== WFile::Write ( at::PtrDelegate< Buffer * > i_br ) ====================
    long WFile::Write ( at::PtrDelegate< Buffer * > i_br )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        // Zero bytes to write do nothing!
        if ( i_br.Get()->Region().m_allocated == 0 )
        {
            return 0;   
        }

        long byteswritten;

        // If write fails throw an exception
        byteswritten = write ( m_filehandle.fildes, static_cast<void*>(i_br.Get ()->Region().m_mem), 
				  i_br.Get()->Region().m_allocated );

        if ( byteswritten == 0 )
        {
            throw FileErrorBasic ( errno );
        }

        // If the number written does not match then the Disk must be full
        if ( byteswritten != i_br.Get()->Region().m_allocated )
        {
            throw FileErrorBasic ( FileError::s_disk_full );
        }

        return byteswritten;
    }

    // ======== WFile::Open ( const FilePath&, const int )================
    bool WFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== WFile::Open () ===========================================
    bool WFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== WFile::CheckAttributes ( int i_attr ) ====================
    bool WFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we only have Write attributes 

        FileAttr mode = *i_attr;

        // Read Not Allowed
        if ( mode & FileAttr::Read )
        {
            return false;       
        }

        // ReadWrite Not Allowed
        if ( mode & FileAttr::ReadWrite )
        {
            return false;       
        }

        // Create and Preserve not allowed together
        if ( mode & FileAttr::Create && mode & FileAttr::Preserve )
        {
            // Create has presedance over Preserve so remove Preserve
            mode.Remove ( FileAttr::Preserve );
        }

        // Must Have Write
        if ( ! (mode & FileAttr::Write ) )
        {
            mode.Add ( FileAttr::Write );
        }

        *i_attr =  mode;

        return true;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Win32 Implementation of RWFile
    //
    /////////////////////////////////////////////////////////////////////

    // ======== RWFile::RWFile ( const FilePath& ,int ) ==================
    RWFile::RWFile ( const FilePath & i_file_path, const FileAttr& i_attr  ) 
        : BaseFile ( i_file_path, i_attr ), RFile ( i_file_path, i_attr ), WFile ( i_file_path, i_attr )
    {
    }

    // ======== RWFile::RWFile () ========================================
    RWFile::RWFile () : BaseFile (), WFile (), RFile ()
    {
    }

    // ======== RWFile::Open ( const FilePath& , const int ) ============
    bool RWFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== RWFile::Open () =========================================
    bool RWFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== RWFile::CheckAttributes ( int *i_attr ) ================
    bool RWFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we have Read / Write attributes 
        FileAttr mode = *i_attr;

        // Read Not Allowed
        if ( mode & FileAttr::Read )
        {
            return false;       
        }

        // Write Not Allowed
        if ( mode & FileAttr::Write )
        {
            return false;       
        }

        // Create and Preserve not allowed together 
        if ( mode & FileAttr::Create && mode & FileAttr::Preserve )
        {
            // Create takes presedance remove Preserve 
            mode.Remove ( FileAttr::Preserve );
        }

        // Must Have ReadWrite
        if ( ! ( mode & FileAttr::ReadWrite ) )
        {
            mode.Add ( FileAttr::ReadWrite );
        }

        *i_attr = mode;

        return true;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // GX86 Callback Functions 
    //
    /////////////////////////////////////////////////////////////////////

    // ======== read_completed  =================================
 	static void read_completed ( union sigval sv )
 	{
		//at::Lock<at::Mutex> a_l ( at::BaseFile::m_filelock ); see below
		
 		at::FileContext* filecontext = (at::FileContext*)sv.sigval_ptr;
        AT_Assert ( filecontext); 
		at::Lock<at::Mutex> a_l ( *(filecontext->m_base_file_mutex) );

		int result = aio_return ( (aiocb*)filecontext );
        if ( result != -1 )
        {
        	filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, at::FileErrorBasic::s_success );
        }
        else
		{
			const FileErrorBasic fe ( errno );
			filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, fe );
		}
		filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::Cancel);
 	}

    // ======== write_completed =================================
 	void write_completed ( union sigval sv )
	{
		//at::Lock<at::Mutex> a_l ( at::BaseFile::m_filelock );
 		
 		at::FileContext* filecontext = (at::FileContext*)sv.sigval_ptr;
        AT_Assert ( filecontext ); 		
		at::Lock<at::Mutex> a_l ( *(filecontext->m_base_file_mutex) );

        int result = aio_return ( (aiocb*)filecontext );
        if ( result != -1 )
        {
         	filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, at::FileErrorBasic::s_success );
        }
        else
        {
        	const FileErrorBasic fe ( errno );
 			filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, fe );
        }
		filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::Cancel);
	}

    // ======== RFile::ReadAsync =================================
    bool RFile::ReadAsync ( at::Ptr< Buffer * > i_br, AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead ) 
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        AT_Assert ( i_br.Get () != NULL );
        AT_Assert ( i_br.Get()->Region().m_allocated != 0 );
        AT_Assert ( m_mode & FileAttr::Async );

        // Create a new Overlapped struct NOTE: This needs to be deleted - LifeTime!
        FileContext * filecontext = new FileContext;
        // Store the Buffer
        filecontext->SetBuffer ( i_br );
        // Store the This Pointer
        filecontext->SetThis ( this );
        // Store the Lead
        filecontext->SetLead ( i_lead, RFile::m_mutex );
		filecontext->SetBaseFileMutex( at::BaseFile::m_filelock );

		filecontext->aio_sigevent.sigev_notify = SIGEV_THREAD;
		filecontext->aio_sigevent.sigev_notify_function = read_completed;
		filecontext->aio_sigevent.sigev_notify_attributes = NULL;

        // Read Asynchronously
        if ( -1 == aio_read ( (aiocb*)filecontext ) )
        {
            SetError  ( errno );

            return false;
        }

        return true;
    }

    // ======== WFile::WriteAsync =================================
    bool WFile::WriteAsync ( const at::PtrView< Buffer * > i_br, AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead ) 
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        AT_Assert ( i_br.Get () != NULL );
        AT_Assert ( i_br.Get()->Region().m_allocated != 0 );
        AT_Assert ( m_mode & FileAttr::Async );

        // Create a new Overlapped struct should be deleted in completion routine
        FileContext * filecontext = new FileContext;
        // Store the buffer
        filecontext->SetBuffer ( i_br );
        // Store the This Pointer
        filecontext->SetThis ( this );
        // Store the Lead
        filecontext->SetLead ( i_lead, WFile::m_mutex );
		filecontext->SetBaseFileMutex( at::BaseFile::m_filelock );
		
        filecontext->aio_sigevent.sigev_notify = SIGEV_THREAD;
        filecontext->aio_sigevent.sigev_notify_function = write_completed;
        filecontext->aio_sigevent.sigev_notify_attributes = NULL;

        // Write  Asynchronously
        aiocb* aio_cb = (aiocb*)filecontext;
        if ( -1 == aio_write ( aio_cb ) )
        {
            SetError ( errno );
            
            return false;
        }
        
        return true;
    }

	// ======== FileContext::SetBuffer =================================
    void FileContext::SetBuffer  ( at::PtrDelegate<at::Buffer*> i_buffer ) 
    { 
    	m_buffer = i_buffer; 

		aio_buf = (volatile void*)GetRawBytes ();
		aio_nbytes = GetBufferSize ();
    }

	// ======== FileContext::SetThis =================================
    void FileContext::SetThis ( at::BaseFile* i_basefile ) 
    { 
    	m_basefile = i_basefile; 
    	aio_fildes = i_basefile->GetFileHandle ().fildes;
		aio_offset = i_basefile->GetPosition ();
    }
};
