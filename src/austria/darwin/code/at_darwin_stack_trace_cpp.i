//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_darwin_stack_trace_cpp.i
 *
 */

//
// This implements the darwin (Mac OS X) stack trace
// HACK/FIX/TODO: NO, IT DOES NOT.  This is a placeholder so that we can
// compile.  2005-02-23 (m) If you are reading this, the MoreIsBetter
// example library on Apple's developer web site includes a MoreDebug
// sub-library that has a MoreBacktrace functionality.  This is the code
// we need to lift.


#include "at_stack_trace.h"
#include "at_types.h"
#include "at_lifetime_mt.h"

#include <cstdlib>
#include <vector>

// Austria namespace
namespace at
{

// ======== StackTrace_darwin ===========================================

class AUSTRIA_EXPORT StackTrace_darwin
  : virtual public PtrTarget_MTVirtual,
    public StackTrace
{

    public:

    StackTrace_darwin()
      : m_addresses(),
        m_backtrace_symbols(0)
    {
    }

    ~StackTrace_darwin()
    {
        if ( m_backtrace_symbols )
        {
            std::free( m_backtrace_symbols );
        }
    }


    // ======== ProcessFrames ==========================================
    /**
     * ProcessFrames will process the stack trace into StackFrame
     * objects.
     *
     * @return nothing
     */

    virtual void ProcessFrames() const
    {
    }    

    // ======== Begin =================================================

    virtual PtrDelegate<StackIterator *> Begin() const;

    // ======== operator< =============================================

    virtual bool operator< ( const StackTrace & i_rhs ) const
    {
        const StackTrace_darwin * l_st_darwin = dynamic_cast< const StackTrace_darwin * >( & i_rhs );

        AT_Assert( l_st_darwin != 0 );

        return m_addresses < l_st_darwin->m_addresses;
    }
    
    // This contains the addresses collected from the
    // backtrace
    std::vector< void * >                   m_addresses;

    // Place to store all the stack frames
    //
    mutable std::vector< StackFrame >       m_frames;

    // Store all the backtrace symbols
    mutable char                         ** m_backtrace_symbols;


};

// ======== StackIterator_darwin ========================================

class AUSTRIA_EXPORT StackIterator_darwin
  : virtual public PtrTarget_MTVirtual,
    public StackIterator
{
    public:

    // ======== StackIterator_darwin ====================================
    
    StackIterator_darwin(
        at::PtrDelegate< const StackTrace_darwin * >    i_stace_trace
    )
      : m_stace_trace( i_stace_trace ),
        m_iterator( m_stace_trace->m_frames.begin() )
    {
    }
    

    // ======== Next ==================================================
    virtual bool Next()
    {
        if ( m_iterator == m_stace_trace->m_frames.end() )
        {
            return false;
        }
        
        return m_stace_trace->m_frames.end() != ++ m_iterator;

    }


    // ======== Previous ==============================================
    virtual bool Previous()
    {
        if ( m_iterator == m_stace_trace->m_frames.begin() )
        {
            return false;
        }

        -- m_iterator;
        return true;
    }


    // ======== Last ==================================================
    virtual void Last()
    {
        m_iterator = m_stace_trace->m_frames.end();
    }


    // ======== First =================================================
    virtual void First()
    {
        m_iterator = m_stace_trace->m_frames.begin();
    }


    // ======== IsValid ===============================================
    virtual bool IsValid() const
    {
        return m_stace_trace->m_frames.end() != m_iterator;
    }


    // ======== DeReference ===========================================
    const StackFrame & DeReference() const
    {
        AT_Assert( IsValid() );
        return * m_iterator;
    }
    
    // m_stace_trace points to the StackTrace_darwin associated with this
    // iterator.
    
    at::Ptr< const StackTrace_darwin * >      m_stace_trace;

    // m_iterator is the current iterator
    std::vector< StackFrame >::iterator     m_iterator;

};


// ======== StackTrace_darwin::Begin ====================================

PtrDelegate<StackIterator *> StackTrace_darwin::Begin() const
{
    ProcessFrames();
    
    return new StackIterator_darwin( at::PtrView< const StackTrace_darwin * >( this ) );
}


// ======== NewStackTrace =============================================
/**
 * NewStackTrace is implementation dependant, it will create a new
 * stack trace and record the stack trace at this point.
 *
 * @return A pointer to a new stack trace.
 */

PtrDelegate< StackTrace * > NewStackTrace()
{
    void        * l_addresses[ 1024 ];

    return new StackTrace_darwin();
}

}; // namespace at
