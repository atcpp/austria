//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_darwin_atomic.h
 *
 */

#ifndef x_at_darwin_atomic_h_x
#define x_at_darwin_atomic_h_x 1

// Austria namespace
namespace at
{

// ======== AtomicCountType ===========================================
/**
 * AtomicCountType is the fundamental type that is operated on for
 * atomic operations.
 */

typedef int AtomicCountType;



// ======== AtomicIncrement ===========================================
/**
 *	AtomicIncrement will atomically increment the value being pointed
 *  to in the parameter.
 *
 * @param io_val pointer to the value being incremented
 * @return The value contained in the pointer before being incremented.
 */

inline static AtomicCountType AtomicIncrement(
    volatile AtomicCountType    * io_val
) {
    int    tmp;

    asm volatile(
        "3:     lwarx   %0,0,%1\n"      // store the value at io_val into
                                        // tmp and make a resevation to
                                        // write at io_val
        "       addic   %0,%0,1\n"      // add 1 to tmp and store in tmp
        "       stwcx.  %0,0,%1\n"      // store tmp at io_val if no one
                                        // has written at io_val since we
                                        // made the reservation
        "       bne-    3b\n"           // loop if that failed
        "       addic   %0,%0,1\n"      // add 1 to tmp and store to tmp
                                        // so that we can return the old
                                        // value
        : "=&r" (tmp)
        : "r" ( io_val )
        : "cc", "memory");

    return tmp;
}

// ======== AtomicDecrement ===========================================
/**
 *	AtomicDecrement will atomically decrement the value being pointed
 *  to in the parameter.
 *
 * @param io_val pointer to the value being decremented
 * @return The value contained in the pointer before being decremented.
 */

inline static AtomicCountType AtomicDecrement(
    volatile AtomicCountType    * io_val
) {
    int    tmp;

    asm volatile(
        "3:     lwarx   %0,0,%1\n"      // store the value at io_val into
                                        // tmp and make a resevation to
                                        // write at io_val
        "       subic   %0,%0,1\n"      // subtract 1 from tmp and store
                                        // in tmp
        "       stwcx.  %0,0,%1\n"      // store tmp at io_val if no one
                                        // has written at io_val since we
                                        // made the reservation
        "       bne-    3b\n"           // loop if that failed
        "       subic   %0,%0,1\n"      // add 1 to tmp and store to tmp
                                        // so that we can return the old
                                        // value
        : "=&r" (tmp)
        : "r" (io_val)
        : "cc", "memory");

    return tmp;
}


// ======== AtomicBump ===========================================
/**
 *	AtomicBump will atomically add the value being pointed
 *  to in the parameter with the 
 *
 * @param io_val pointer to the value being decremented
 * @param i_add_val valie to add.
 * @return The value contained in the pointer before being added
 */

inline static AtomicCountType AtomicBump(
    volatile AtomicCountType    * io_val,
    AtomicCountType                i_add_val
) {
    int    tmp;

    asm volatile(
        "3:     lwarx   %0,0,%1\n"      // store the value at io_val into
                                        // tmp and make a resevation to
                                        // write at io_val
        "       addc    %0,%0,%2\n"     // add i_add_val to tmp and store
                                        // in tmp
        "       stwcx.  %0,0,%1\n"      // store tmp at io_val if no one
                                        // has written at io_val since we
                                        // made the reservation
        "       bne-    3b\n"           // loop if that failed
        "       subc    %0,%0,%2\n"     // subtract i_add_val from tmp and
                                        // store to tmp so that we can
                                        // return the old value
        : "=&r" (tmp)
        : "r" (io_val), "r" (i_add_val)
        : "cc", "memory");

    return tmp;
}


// ======== AtomicExchangeablePointer =================================
/**
 * AtomicExchangeablePointer is the pointer type that is the base for
 * the AtomicExchange_Ptr class.
 *
 */

typedef void * AtomicExchangeablePointer;


// ======== AtomicExchangeValue =======================================
/**
 *	AtomicExchangeValue will atomically exchange the value passed in
 *  with the one pointed to in memory.
 *
 * @param i_val the value to set the memory location to
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return the previous value pointed to by io_mem_loc
 */

inline static AtomicExchangeablePointer AtomicExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val
) {
    AtomicExchangeablePointer tmp;

    asm volatile(
        "3:     lwarx   %0,0,%1\n"      // store the value at io_mem_loc
                                        // to tmp and make a reservation
                                        // to write at io_mem_loc
        "       stwcx.  %2,0,%1\n"      // store i_val at io_mem_loc if no
                                        // one has written there since
                                        // we made the reservation
        "       bne-    3b\n"           // loop if that failed
        : "=&r" (tmp)
        : "r" (io_mem_loc), "r" (i_val)
        : "cc", "memory");

    return tmp;
}

// ======== AtomicExchangeableValue =================================
/**
 * AtomicExchangeableValue is the pointer type that is the base for
 * the AtomicExchange_Ptr class.
 *
 */

typedef int AtomicExchangeableValue;


// ======== AtomicExchangeValue =======================================
/**
 *	AtomicExchangeValue will atomically exchange the value passed in
 *  with the one pointed to in memory.
 *
 * @param i_val the value to set the memory location to
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return the previous value pointed to by io_mem_loc
 */

inline static AtomicExchangeableValue AtomicExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val
) {
    AtomicExchangeableValue tmp;

    // Observe that this is indeed exactly the same set of instructions
    // used for pointers in the pointer overload of AtomicExchange, above.
    // REV: does sizeof(AtomicExchangeableValue) ==
    // sizeof(AtomicExchangeablePointer) == word size for 64-bit PPC?
    asm volatile(
        "3:     lwarx   %0,0,%1\n"      // store the value at io_mem_loc
                                        // to tmp and make a reservation
                                        // to write at io_mem_loc
        "       stwcx.  %2,0,%1\n"      // store i_val at io_mem_loc if no
                                        // one has written there since
                                        // we made the reservation
        "       bne-    3b\n"           // loop if that failed
        : "=&r" (tmp)
        : "r" (io_mem_loc), "r" (i_val)
        : "cc", "memory");

    return tmp;
}

// ======== AtomicCompareExchange ================================
/**
 * AtomicCompareExchangeValue does a compare exchange.
 *
 * @param i_val the value to set the memory location to
 * @param i_cmp The value to compare with
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return The original value in the memory location
 */

inline static AtomicExchangeableValue AtomicCompareExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val,
    AtomicExchangeableValue                 i_cmp
) {
    AtomicExchangeableValue tmp;

    asm volatile(
        "3:     lwarx   %0,0,%1\n"      // store the value at io_mem_loc
                                        // to tmp and make a reservation
                                        // to write at io_mem_loc
        "       cmpw    cr7,%0,%3\n"    // compare tmp and i_cmp
        "       bne     cr7,4f\n"       // skip to 4: if they're not equal
        // REV: Do we care that we leave the RESERVE bit set if tmp!=i_cmp?
        // Presumably not, since lwarx and stwcx. instructions should be
        // paired but this seems vaguely trashy.
        "       stwcx.  %2,0,%1\n"      // store i_val at io_mem_loc if no
                                        // one has written there since
                                        // we made the reservation
        "       bne-    3b\n"           // loop if that failed
        "4:"
        : "=&r" (tmp)
        : "r" (io_mem_loc), "r" (i_val), "r" (i_cmp)
        : "cr7", "cc", "memory");

    return tmp;
}



// ======== AtomicCompareExchange (pointers) =========================
/**
 * AtomicCompareExchangePointer performs a compare/exchange on a
 * pointer value.
 *
 * @param i_val The value to set the memory location to
 * @param i_cmp The value to compare with
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return The original value in the memory location
 */

inline static AtomicExchangeablePointer AtomicCompareExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val,
    AtomicExchangeablePointer                 i_cmp
) {
    AtomicExchangeablePointer tmp;

    // Observe that this is indeed exactly the same set of instructions
    // used for values in the value overload of AtomicCompareExchange, above.
    // REV: does sizeof(AtomicExchangeableValue) ==
    // sizeof(AtomicExchangeablePointer) == word size for 64-bit PPC?
    asm volatile(
        "3:     lwarx   %0,0,%1\n"      // store the value at io_mem_loc
                                        // to tmp and make a reservation
                                        // to write at io_mem_loc
        "       cmpw    cr7,%0,%3\n"    // compare tmp and i_cmp
        "       bne     cr7,4f\n"       // skip to 4: if they're not equal
        // REV: Do we care that we leave the RESERVE bit set if tmp!=i_cmp?
        // Presumably not, since lwarx and stwcx. instructions should be
        // paired but this seems vaguely trashy.
        "       stwcx.  %2,0,%1\n"      // store i_val at io_mem_loc if no
                                        // one has written there since
                                        // we made the reservation
        "       bne-    3b\n"           // loop if that failed
        "4:"
        : "=&r" (tmp)
        : "r" (io_mem_loc), "r" (i_val), "r" (i_cmp)
        : "cr7", "cc", "memory");

    return tmp;
}



}; // namespace

#endif // x_at_darwin_atomic_h_x


