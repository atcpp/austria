//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_darwin_thread_cpp.i
 *
 */

//
// This implements the darwin (or posix) API for threads.
// The code is all posix specific.

#include "at_darwin_thread.h"

#include "at_posix_thread_cpp.i"
