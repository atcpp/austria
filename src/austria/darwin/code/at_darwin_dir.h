//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_darwin_dir.h
 *
 */

#include "at_exports.h"

#ifndef x_at_darwin_dir_h_x
#define x_at_darwin_dir_h_x 1

// Austria namespace
namespace at
{
}; // namespace

#endif // x_at_darwin_dir_h_x


