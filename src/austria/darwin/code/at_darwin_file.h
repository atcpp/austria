//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_darwin_file.h
 *
 */

#include "at_exports.h"
#include "at_twinmt_basic.h"
#include <sys/aio.h>

#ifndef x_at_darwin_file_h_x
#define x_at_darwin_file_h_x 1

// Austria namespace
namespace at
{
// ======== FileErrorBasic  =============================================
/**
*       Concrete implementation of FileError
*
*/
class AUSTRIA_EXPORT FileErrorBasic : public FileError
{
public:

    // ======== FileErrorBasic  =============================================
    /**
    *       @param i_name A description of the error
    *       @param i_errorno A number to associate with this error (Found in Winerror.h)
    */

    FileErrorBasic ( const char* i_name, long i_errorno = -1 );

    // ======== FileErrorBasic  =============================================
    /**
    *
    *       @param i_errorno The number of the error. Used for throwing.
    */

    FileErrorBasic ( long i_errorno );

    // ======== FileErrorBasic  =============================================
    /**
    *
    *       @param i_fileerror Create this object using the info form another 
    *                          FileError object
    */

    FileErrorBasic ( const FileError& i_fileerror );

public:

    // ======== What =============================================
    /**
    *   @return returns the Description of this error
    */

    const char * What () const { return m_errortext; }

    // ======== Operator =  =============================================
    /**
    *       Operator = Copies the contents of another FileErrorBasic to this one
    *
    */

    const FileErrorBasic & operator = ( const FileErrorBasic* fe );

    // ======== Operator ==  =============================================
    /**
    *       Operator == Compares two FileError's
    *
    */

    bool operator == ( const FileError& fe ) const { return fe.ErrorCode () == ErrorCode (); } 


protected:

    // ======== ErrorCode =============================================
    /**
    *       @return return the error code
    */

    long ErrorCode () const { return m_errorno; }

private:

    /**
    *   m_errortext The internal Error text
    */

    const char* m_errortext;

    /**
    *   m_errortext The internal Error code
    */

    long m_errorno;
};

// ======== FileContext =============================================
/**
*       
*       FileContext wraps the read buffer 
*       
*
*/
class FileContext : public aiocb
{
public:

    // ======== FileContext::FileContext =============================================
    /**
    *           Constructs aFileContext object
    *   
    */

    FileContext () 
    {
		aio_sigevent.sigev_value.sigval_ptr = (void*)this;

		aio_fildes = 0;
		aio_buf = 0;
		aio_nbytes = 0;
		aio_offset = 0;
		aio_reqprio = 0;
		aio_lio_opcode = 0;
    }

	Mutex* m_base_file_mutex;

	// ======== SetMutex ==============================================
	/**
	*		@param i_mutex is used to pass in the protected mutex from
	*		the RFile base class for use by the call back function
	*/

	void SetBaseFileMutex( Mutex &i_mutex )
	{
		m_base_file_mutex = &i_mutex;
	}

    // ======== Setposition =============================================
    /**
    *       @param i_pos Fills indicates
    *       the current position of the file pointer
    */

    void SetPosition ( long i_pos ) {}   

    // ======== GetRawBytes =============================================
    /**
    *       @return returns the raw bytes from inside the at::Buffer
    */

    void* GetRawBytes  () { return static_cast<void*>(m_buffer.Get ()->Region ().m_mem); }

    // ======== GetBufferSize =============================================
    /**
    *       @return returns the number of bytes that the buffer holds
    */

    long GetBufferSize () { return m_buffer.Get ()->Region ().m_allocated; }

    // ======== GetBufferSize =============================================
    /**
    *       @return returns the buffer 
    *       NOTE: The buffer is now invalid
    */

    at::PtrDelegate<at::Buffer*> GetBuffer  () { return m_buffer; }

    // ======== SetBuffer =============================================
    /**
    *       @param i_buffer Accepts a buffer and takes control of it
    *                       this buffer usually contains the result of a read
    */

    void SetBuffer  ( at::PtrDelegate<at::Buffer*> i_buffer ); 

    // ======== SetThis =============================================
    /**
    *       @param i_basefile The this pointer of th calling class
    */

    void SetThis ( at::BaseFile* i_basefile );
    
    // ======== GetThis =============================================
    /**
    *       @return return m_basefile
    */

    BaseFile* GetThis () { return m_basefile; }
    
    // ======== SetLead =============================================
    /**
    *       @param associate a lead with our aide
    */

    void SetLead ( AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer lead, Ptr< MutexRefCount * >  i_mutex )
    {
        lead->m_filecontext = this;
        m_aide.AideAssociate ( lead, i_mutex );
    }

    // ======== GetAide =============================================
    /**
    *       @return returns our Aide
    */

    AideTwinMT_Basic<FileNotifyLeadInterface>& GetAide () { return m_aide; }
    
    
private:

    /**
    *       m__buffer a delegate buffer used to store data during async reads
    */

    at::PtrDelegate< at::Buffer * > m_buffer;
    
    /**
    * 		m_basefile is a pointer to the this pointer of tha calling class
    */

    at::BaseFile* m_basefile;
    
    /**
    *     m_aide used to callback client 
    */

    AideTwinMT_Basic<FileNotifyLeadInterface> m_aide;
};
    
// ======== FileInitializer =============================================
/**
 *  FileInitializer sets initial conditions for the file subsusytem
 */
class FileInitializer : public Initializer
{
    public:
  
    FileInitializer ( int & argc, const char ** & argv )
    {
// The aio_init function does not exist under OS X, though it does under
// Linux.  Perhaps it's not actually POSIX?  Anyhow, its purpose is to
// allow users of the aio library to specify information used for
// optimizations (if the Linux header file tells the whole story) so
// it should not be a problem to not perform this.
#if 0
    	struct aioinit init;
    	memset ( &init, 0, sizeof(aioinit) );
    	init.aio_threads = 1024;
    	init.aio_num = 1024;
    	aio_init ( &init );
#endif
    }

    ~FileInitializer ()
    {
    }
};
    
}; // namespace

#endif // x_at_darwin_file_h_x


