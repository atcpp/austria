//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_gx86_thread.h
 *
 */

#ifndef x_at_gx86_thread_h_x
#define x_at_gx86_thread_h_x 1

#include <pthread.h>

// We define constants for a non-standard pthread mutex attribute.
#define AT_MUTEX_ATTR_FAST PTHREAD_MUTEX_FAST_NP
#define AT_MUTEX_ATTR_RECURSIVE PTHREAD_MUTEX_RECURSIVE_NP
#define AT_MUTEX_ATTR_ERRORCHECK PTHREAD_MUTEX_ERRORCHECK_NP

// Austria namespace
namespace at
{

typedef unsigned int SYS_TaskID;
    
//
// These types are system specific.
//


/**
 * SystemMutexContextType basically contains a pthread_mutex_t
 * This should be made large enough for holding at least a
 * pthread_mutex_t.  Don't include pthread.h here.
 */

typedef void * SystemMutexContextType[
    sizeof( void * ) == 4
        ? 6 * sizeof( void * )
        : 5 * sizeof( void * )
];

/**
 * SystemConditionalContextType basically contains a pthread_condattr_t
 * and a pointer to a pthread_mutex_t.
 * This should be made large enough for holding at least a
 * pthread_condattr_t and a pointer.  Don't include pthread.h here.
 */

typedef void * SystemConditionalContextType[
    sizeof( void * ) == 4
        ? 13 * sizeof( void * )
        : 7 * sizeof( void * )
];


}; // namespace

#endif // x_at_gx86_thread_h_x



