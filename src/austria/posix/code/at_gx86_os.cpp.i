//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_gx86_os.cpp.i
 *
 */


// #include "at_os.h" - already included

#include <sched.h>
#include <unistd.h>


// ======== ProcessorCount ============================================
/**
 * Return the number of available CPUs
 *
 *
 */

static unsigned ProcessorCount()
{
#ifdef _SC_NPROCESSORS_ONLN
    long l_val = sysconf( _SC_NPROCESSORS_ONLN );

    if ( l_val != -1L )
    {
        return l_val;
    }
    else
    {
        return 2;
    }
#else
    return 2;
#endif
}


namespace at
{
    
// ======== SchedulerYield =======================================
void OSTraitsBase::SchedulerYield()
{
    sched_yield();
}

// ======== s_cpu_count ==========================================
const unsigned OSTraitsBase::s_cpu_count = ProcessorCount();


// ======== s_smp_memory_latency =================================
const unsigned OSTraitsBase::s_smp_memory_latency =
    OSTraitsBase::s_cpu_count > 1 ? 2 : 0;



} // namespace
