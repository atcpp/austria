/**
 * \file at_gx86_log_ctime.h
 *
 * \author Bradley Austin
 *
 */

#ifndef x_at_gx86_log_ctime_h_x
#define x_at_gx86_log_ctime_h_x 1


namespace at
{


    class LogWriterTimeFormatPolicy_CTime_gx86
    {

    public:

        typedef  std::string  t_formatted_time_type;

        static t_formatted_time_type FormatTime( time_t i_time )
        {
            tm l_tm;
            char l_s[26];

            if ( i_time == -1 || localtime_r( &i_time, &l_tm ) == 0 )
                return "N/A";
            strftime( l_s, 26, "%H:%M:%S  %a %b %d %Y", &l_tm );
            return l_s;
        }

    protected:

        inline LogWriterTimeFormatPolicy_CTime_gx86() {}
        inline ~LogWriterTimeFormatPolicy_CTime_gx86() {}

    private:

        /* Unimplemented */
        LogWriterTimeFormatPolicy_CTime_gx86
            ( const LogWriterTimeFormatPolicy_CTime_gx86 & );
        LogWriterTimeFormatPolicy_CTime_gx86 & operator=
            ( const LogWriterTimeFormatPolicy_CTime_gx86 & );

    };


}


#endif
