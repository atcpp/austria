/*
 *
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *  at_gx86_dir.cpp
 *
 *  This contains the gx86 implementation of at_dir.h.
 *
 */

#include "at_exports.h"
#include "at_types.h"

#include "at_file.h"
#include "at_dir.h"

#include AT_FILE_H
#include AT_DIR_H

#include <errno.h>
#include "aio.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <map>
#include <string>
#include <dirent.h>

// Austria namespace
namespace at
{
    ////////////////////////////////////////////////////////////// 
    //
    //  DirEntry
    //
    //////////////////////////////////////////////////////////////

    // ======== DirEntry::IsDirectory ================================================
    bool DirEntry::IsDirectory () const
    {
        const char* name = m_entry_name.String ();

        struct stat path_stat;
        
        if ( ::stat ( name, &path_stat ) != 0 )
        {
            if ( ( errno == ENOENT ) || ( errno == ENOTDIR ) )
            {
                return false;  // stat failed because the path does not exist
            }
        }
        
        if ( ::stat ( name, &path_stat ) != 0 )
        {
            throw FileErrorBasic ( errno );
        }

        return S_ISDIR( path_stat.st_mode );
    }

    ////////////////////////////////////////////////////////////// 
    //
    //  DirIterator
    //
    //////////////////////////////////////////////////////////////

    // ======== DirIterator =============================================
    DirIterator::DirIterator ( const FilePath& i_dir_name ) : m_dir_name ( i_dir_name ) 
    {
        m_handle.dirhandle = 0;
    }

    // ======== Next ==================================================
    bool DirIterator::Next ()
    {
        if ( 0 == m_handle.dirhandle )
            return false; 

        struct dirent * dp;
        errno = 0;
        
        dp = readdir ( static_cast<DIR*>( m_handle.dirhandle ) );
        
        if ( dp == 0 )
        {
            if ( errno != 0 )
            {
                throw FileErrorBasic ( errno );
            }
            else 
            { 
                return false; 
            } // end reached
        }

        m_dir_entry.m_entry_name = std::string ( dp->d_name );

        return true;
    }

    // ======== First =================================================
    bool DirIterator::First () 
    {
        m_handle.dirhandle = opendir ( m_dir_name.String () );
        
        if ( 0 == m_handle.dirhandle )
        {
            return false;
        }
        
        if ( Next () )
        {
        	const char *name = m_dir_entry.m_entry_name.String ();
        	// Check for and skip over Dot, Dot Dot
        	bool dotdot = false;
	        do
	        {
	            dotdot = name[0] == '.' && ( name[1] == '\0' || (name[1] =='.' && name[2] == '\0' ) );
	            if ( dotdot )
	            {
	                if ( Next () )
	                    name = m_dir_entry.m_entry_name.String ();
	                else
	                    return false; // no files!
	            }
	        } while ( dotdot );
	        
	        return true;
        }
        
        return false;
    }

    // ======== GetEntry ===========================================
    const DirEntry & DirIterator::GetEntry () const
    {
        return m_dir_entry;
    }

    DirIterator::~DirIterator ()
    {
        if ( 0 != m_handle.dirhandle )
        {
        	closedir ( static_cast<DIR*>( m_handle.dirhandle ) );
        }
    }

    ////////////////////////////////////////////////////////////// 
    //
    //  Directory
    //
    //////////////////////////////////////////////////////////////

    // ======== Create =================================================
    bool Directory::Create ( const FilePath& i_dir_name )
    {
        if ( mkdir ( i_dir_name.String (), S_IRWXU | S_IRWXG | S_IRWXO ) == 0 )
        {
        	return true;
        }
        
        if ( errno != EEXIST )
        {
        	throw FileErrorBasic ( errno );
        }
        
        return false;
    }

    // ======== Remove =================================================
    bool Directory::Remove ( const FilePath& i_dir_name, bool i_delete_if_not_empty )
    {
        Directory dir ( i_dir_name );

        bool isdir = dir.IsDirectory ();
        if ( isdir == false )
            return false;

        if ( true == i_delete_if_not_empty )
        {
            dir.Clear ( true );
        }

        if ( std::remove( i_dir_name.String () ) != 0 )
        {
            if ( false == i_delete_if_not_empty && errno == EEXIST )
            {
                return false;
            }
            else
            {
                throw FileErrorBasic ( ENOTEMPTY ); // Error Support for ENOTEMPTY needed
            }
        }

        return true; 
    }
    
    // ======== ChangeCurrent =================================================
    bool Directory::ChangeCurrent ( const FilePath& i_dir_name )
    {
        return true;
    }

    // ======== Rename =================================================
    bool Directory::Rename ( const FilePath& i_dir_old_name, const FilePath& i_dir_new_name )
    {
        if ( Exists ( i_dir_new_name ) )
        {
            return false;
        }

		if ( !Exists ( i_dir_old_name ) )
		{
			return false;
		}
		
        if ( -1 == std::rename ( i_dir_old_name.String (), i_dir_new_name.String ()  ) )
        {
        	return false;
        }
        
        return true;
    }

    // ======== Exists =================================================
    bool Directory::Exists ( const FilePath& i_dir_name )
    {
        struct stat path_stat;
        if ( ::stat( i_dir_name.String (), &path_stat ) != 0 )
        {
            if ( ( errno == ENOENT ) || ( errno == ENOTDIR ) )
            {
                return false;  // stat failed because the path does not exist
            }
        }

        return S_ISDIR( path_stat.st_mode );
    }

    bool Directory::IsDirectory ()
    {
        return Exists( m_dir_name );
    }

    // ======== Directory::Directory==========================================
    Directory::Directory ( const FilePath& i_dir_name ) : m_dir_name ( i_dir_name )
    {
    }

    // ======== IsEmpty =================================================
    bool Directory::IsEmpty ()
    {
        at::PtrDelegate <DirIterator *> di = ListEntries ();

        return !di->First ();
    }

    // ======== Clear =================================================
    bool Directory::Clear  ( bool i_delete_sub_directories )
    {
        at::PtrDelegate <DirIterator *> di = ListEntries ();

        if ( di->First () )
        {
            do
            {
                DirEntry entry = di->GetEntry ();
                std::string file = m_dir_name.String ();
                file += "/";
                file += entry.m_entry_name.String ();
                entry.m_entry_name = file;

                if ( !entry.IsDirectory () )
                {
                    WFile wf ( entry.m_entry_name );
                    wf.SetPermissions ( FilePermission::UserWrite );
                    if ( false == BaseFile::Remove ( entry.m_entry_name ) )
                    {
                        return false;
                    }
                }
                else
                {
                    if ( i_delete_sub_directories )
                    {
                        Remove ( entry.m_entry_name, true );
                    }
                }
            }
            while ( di->Next () );
        }

        return true; // Directory was erased
    }

    // ======== List =================================================
    PtrDelegate<DirIterator *> Directory::ListEntries () const
    {
        PtrDelegate<DirIterator*> di = new DirIterator ( m_dir_name );

        return di;
    }

    // ======== operator == =============================================
    bool Directory::operator == ( const Directory & i_rhs ) const
    {
        AT_Assert ( false ); // Not Supported for 1.0
        return true;
    }

    // ======== ~Directory::Directory =============================================
    Directory::~Directory ()
    {
    }
};
