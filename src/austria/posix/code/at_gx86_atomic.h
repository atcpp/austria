//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_gx86_atomic.h
 *
 */

#ifndef x_at_gx86_atomic_h_x
#define x_at_gx86_atomic_h_x 1

// Austria namespace
namespace at
{

// ======== AtomicCountType ===========================================
/**
 * AtomicCountType is the fundamental type that is operated on for
 * atomic operations.
 */

typedef int AtomicCountType;



// ======== AtomicIncrement ===========================================
/**
 *	AtomicIncrement will atomically increment the value being pointed
 *  to in the parameter.
 *
 * @param io_val pointer to the value being incremented
 * @return The value contained in the pointer before being incremented.
 */

inline static AtomicCountType AtomicIncrement(
    volatile AtomicCountType    * io_val
) {
    register int        l_a_r_val __asm__ ("eax") = 1;

    __asm__ __volatile__ (
        "lock; xaddl %0,%1"
        : "+r" (l_a_r_val)
        : "m" (*io_val)
        : "memory"
    );

    return l_a_r_val;
}

// ======== AtomicDecrement ===========================================
/**
 *	AtomicDecrement will atomically decrement the value being pointed
 *  to in the parameter.
 *
 * @param io_val pointer to the value being decremented
 * @return The value contained in the pointer before being decremented.
 */

inline static AtomicCountType AtomicDecrement(
    volatile AtomicCountType    * io_val
) {
    register int        l_a_r_val __asm__ ("eax") = -1;

    __asm__ __volatile__ (
        "lock; xaddl %0,%1"
        : "+r" (l_a_r_val)
        : "m" (*io_val)
        : "memory"
    );

    return l_a_r_val;
}


// ======== AtomicBump ===========================================
/**
 *	AtomicBump will atomically add the value being pointed
 *  to in the parameter with the 
 *
 * @param io_val pointer to the value being decremented
 * @param i_add_val valie to add.
 * @return The value contained in the pointer before being added
 */

inline static AtomicCountType AtomicBump(
    volatile AtomicCountType    * io_val,
    AtomicCountType                i_add_val
) {
    register int        l_a_r_val __asm__ ("eax") = i_add_val;

    __asm__ __volatile__ (
        "lock; xaddl %0,%1"
        : "+r" (l_a_r_val)
        : "m" (*io_val)
        : "memory"
    );

    return l_a_r_val;
}


// ======== AtomicExchangeablePointer =================================
/**
 * AtomicExchangeablePointer is the pointer type that is the base for
 * the AtomicExchange_Ptr class.
 *
 */

typedef void * AtomicExchangeablePointer;


// ======== AtomicExchangeValue =======================================
/**
 *	AtomicExchangeValue will atomically exchange the value passed in
 *  with the one pointed to in memory.
 *
 * @param i_val the value to set the memory location to
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return the previous value pointed to by io_mem_loc
 */

inline static AtomicExchangeablePointer AtomicExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val
) {

#if defined( __x86_64__ ) || defined( __amd64__  )

    __asm__ __volatile__ (
        "xchgq %0,%1"
        : "+r" (i_val)
        : "m" (*io_mem_loc)
        : "memory"
    );

#else

    __asm__ __volatile__ (
        "xchgl %0,%1"
        : "+r" (i_val)
        : "m" (*io_mem_loc)
        : "memory"
    );

#endif

    return i_val;

}

// ======== AtomicExchangeableValue =================================
/**
 * AtomicExchangeableValue is the pointer type that is the base for
 * the AtomicExchange_Ptr class.
 *
 */

typedef int AtomicExchangeableValue;


// ======== AtomicExchangeValue =======================================
/**
 *	AtomicExchangeValue will atomically exchange the value passed in
 *  with the one pointed to in memory.
 *
 * @param i_val the value to set the memory location to
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return the previous value pointed to by io_mem_loc
 */

inline static AtomicExchangeableValue AtomicExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val
) {

    __asm__ __volatile__ (
        "xchgl %0,%1"
        : "+r" (i_val)
        : "m" (*io_mem_loc)
        : "memory"
    );

    return i_val;

}

// ======== AtomicCompareExchange ================================
/**
 * AtomicCompareExchangeValue does a compare exchange.  i.e.
 *
 * @param i_val the value to set the memory location to
 * @param i_cmp The value to compare with
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return The original value in the memory location
 */

inline static AtomicExchangeableValue AtomicCompareExchange(
    volatile AtomicExchangeableValue      * io_mem_loc,
    AtomicExchangeableValue                 i_val,
    AtomicExchangeableValue                 i_cmp
) {
    register AtomicExchangeableValue    l_ret_val;

    __asm__ __volatile__ (
        "lock; cmpxchgl %2,%3"
        : "=a" (l_ret_val)
        : "a" (i_cmp), "q"(i_val), "m" (*io_mem_loc)
        : "memory"
    );

    return l_ret_val;
}



// ======== AtomicCompareExchange (pointers) =========================
/**
 * AtomicCompareExchangePointer performs a compare/exchange on a
 * pointer value.
 *
 * @param i_val The value to set the memory location to
 * @param i_cmp The value to compare with
 * @param io_mem_loc the pointer to memory of the value
 *  to be exchanged.
 * @return The original value in the memory location
 */

inline static AtomicExchangeablePointer AtomicCompareExchange(
    volatile AtomicExchangeablePointer      * io_mem_loc,
    AtomicExchangeablePointer                 i_val,
    AtomicExchangeablePointer                 i_cmp
) {
    register AtomicExchangeablePointer    l_ret_val;

#if defined( __x86_64__ ) || defined( __amd64__  )
    
    __asm__ __volatile__ (
        "lock; cmpxchgq %2,%3"
        : "=a" (l_ret_val)
        : "a" (i_cmp), "q"(i_val), "m" (*io_mem_loc)
        : "memory"
    );

#else

    __asm__ __volatile__ (
        "lock; cmpxchgl %2,%3"
        : "=a" (l_ret_val)
        : "a" (i_cmp), "q"(i_val), "m" (*io_mem_loc)
        : "memory"
    );

#endif

    return l_ret_val;
}



}; // namespace

#endif // x_at_gx86_atomic_h_x


