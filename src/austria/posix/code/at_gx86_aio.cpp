/*
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 *  Herein lies a minimal wrapper for asynchronous I/O at the OS level
 */

#include "at_aio.h"
#include "at_twinmt_basic.h"

#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>


#include <memory>
#include <list>
#include <iostream>
#include <vector>

namespace {
	int foono;
}

namespace at
{

    
    typedef int SOCKET;
    typedef SOCKET HANDLE;
    #define MAXIMUM_WAIT_OBJECTS    (64)
    #define INVALID_SOCKET (-1)
    #define SOCKET_ERROR (INVALID_SOCKET)

    Mutex cerr_lock(Mutex::Recursive);
    #define PC_TRACE_BASIC_XXX(__pct_a)   //  { Lock<Mutex> l(cerr_lock); std::cerr << __pct_a ; std::cerr.flush();}
    #define PC_TRACE_BASIC(__pct_a)     // { Lock<Mutex> l(cerr_lock); std::cerr << __pct_a ; std::cerr.flush();}
    #define PC_TRACE(a) PC_TRACE_BASIC( "[" << errno << "] line:" << __LINE__ << ":" << (a) << std::endl;)
    #define OutputDebugString(a) foono = -1; PC_TRACE_BASIC(a)

    #define TRY_SOCKET_OP(a,b,c)    PC_TRACE( #b );                                 \
                                    while (INVALID_SOCKET == ((a) = (b))) {         \
                                        if (errno == EINTR)         { continue; }   \
                                        if (errno == c)             { continue; }   \
                                        PC_TRACE("error in socket op");             \
                                        break;                                      \
                                    }

    struct aio_system_handle {
        SOCKET  m_sockEPoll;
    };

    const int default_events = EPOLLIN|EPOLLOUT|EPOLLPRI|EPOLLERR|EPOLLHUP|EPOLLET;

    /**
     * An endpoint (aka file descriptor or socket).  You should treat
     * these as opaque.
     *
     */

    struct AsyncSocketContext :
        public AideTwinMT_Basic<aio_monitor_if>,
        public PtrTarget_MT
    {
        AsyncSocketContext(AioMonitorLead *monitor , PtrView<MutexRefCount *> i_stateMutex, aio_hub *hub, int stype = SOCK_STREAM);
        virtual ~AsyncSocketContext();

        // communications methods
        virtual bool connect(aio_ip4_addr &addr);
        virtual bool listen(aio_ip4_addr &addr, bool isDataGram = false);
        virtual bool accept(PtrView<AsyncSocketContext *> listening_asc, aio_fd *conn_o,aio_ip4_addr *addr);
        virtual bool recv(char *buffer_o, int *sizep_io,aio_ip4_addr *addrp_o, int *err_o);
        virtual bool close(int *err_o);

        // helpers
        virtual bool get_local_address(aio_ip4_addr *addrp_o, int *err_o);
        virtual bool get_remote_address(aio_ip4_addr *addrp_o, int *err_o);

        enum { isDatagram = true, isNotDatagram = false };
        virtual bool send_or_write(Ptr<Buffer *> i_buffer, aio_ip4_addr dest, int *err_o,bool datagram = isNotDatagram);
        virtual bool set_monitor(AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex, int *err_o);

        // accessors/status
        virtual int fd(void) { return m_fdx; }
        virtual int sys_fd(void) { return m_socket; }
        virtual int sys_fd_saved(void) { return m_socketFD; }
        virtual bool NotifyEvent(epoll_event & ev); // return false to delete "this" context
        virtual bool NotifyListenEvent(epoll_event & ev); // return false to delete "this" context
        virtual bool NotifyConnectEvent(epoll_event & ev); // return false to delete "this" context

        // socket events
        virtual bool notifyRead(int statusCode);
        virtual bool notifyWrite(int statusCode);
        virtual bool notifyOOB(int statusCode);
        virtual bool notifyAccept(int statusCode);
        virtual bool notifyConnect(int statusCode);
        virtual bool notifyClose(int statusCode);
        virtual bool notifyQOS(int statusCode);
        virtual bool notifyGroupQOS(int statusCode);
        virtual bool notifyRoutingInterfaceChange(int statusCode);
        virtual bool notifyAddressListChange(int statusCode);

        virtual bool tellMonitor( int state, int flags);
        virtual bool sendCompleted(void);

    private:
        AsyncSocketContext(const AsyncSocketContext &doesNotCopy);
        AsyncSocketContext& operator=(const AsyncSocketContext &doesNotAssign);

        /** Shared -- The number of endpoints we've created. */
        static AtomicCount m_n;

        // internal class
        struct write_params {
            int     m_sent_size;
            aio_ip4_addr m_dest_addr;
            int     m_err;
            Ptr<Buffer *>   m_buffer;
        };

        /** A number unique to this endpoint. */
        int         m_fdx;

        SOCKET      m_socket;
        SOCKET      m_socketFD; // save socket_fd registered with EPOLL so that we can delete fd from epoll set after closing
        int         m_type;

        bool        m_closed;
        bool        m_connectPending;
        bool        m_listening;

        bool        m_haveLocalAddress;
        socklen_t   m_local_addr_len;
        sockaddr_in m_local_addr;

        bool        m_havePeerAddress;
        socklen_t   m_peer_addr_len;
        sockaddr_in m_peer_addr;

        aio_hub     *m_hub;

        std::list<write_params> m_queued_write_params;
        // insert new members above here ^^^

        Ptr<MutexRefCount *>    m_stateMutex;

    };

    AtomicCount AsyncSocketContext::m_n = 0;

    /**
    *   CallbackTask  - this thread runs in the background. It waits for new contexts
    *   and adds them to an event map. When the context completes the thread fires
    *   off a completion notification.
    *
    *   Activities execute in the context of the CallbackTask thread, serially after
    *   their associated event fires
    */

    // @@ TODO this code is essentially identical to at_file CallbackTask -- reuse would be good

    struct aio_hub_internal : private at::Task
    {
    private:
        std::auto_ptr<aio_system_handle>  m_wsDataPtr;

        ConditionalMutex    m_condWork;         // wait for condition variable when no work pending

        // members to protect with the above mutex
        int                             m_hQueuePipe[2];   // write aborts the epoll_wait when a new event is queued
        bool                            m_done;             // flag to terminate the work loop
        bool                            m_working;          // flag that the worker thread is active

        std::vector<Ptr<AsyncSocketContext *> > m_asc;  // pending io
        std::vector<int>                        m_pendingEPollRemovals;
        std::vector<int>                        m_pendingCloses;

    public:

        aio_hub_internal () :
            m_condWork(Mutex::Recursive),
            m_done(false),
            m_working(false),
            m_asc(),
            m_pendingEPollRemovals(),
            m_pendingCloses()
        {
            m_hQueuePipe[0] = -1;
            m_hQueuePipe[1] = -1;
            int result = ::pipe(m_hQueuePipe);
            if (-1 == result) {
                // @@ TODO throw exception -- construction failed
            }
            m_wsDataPtr.reset(new aio_system_handle());
            m_wsDataPtr->m_sockEPoll = ::epoll_create(MAXIMUM_WAIT_OBJECTS);
            if (INVALID_SOCKET == m_wsDataPtr->m_sockEPoll) {
                ::close(m_hQueuePipe[0]);
                ::close(m_hQueuePipe[1]);
                m_hQueuePipe[0] = -1;
                m_hQueuePipe[1] = -1;
                // @@ TODO throw exception -- construction failed
            } else if (m_hQueuePipe[0] != -1) {
                ::fcntl(m_hQueuePipe[0],F_SETFL, O_NONBLOCK);
                epollRegister(m_hQueuePipe[0], EPOLLOUT); // used internally for signalling and breaking into an epoll_wait
            }
        }

        // typically does not execute in the thread context of the callback thread
        virtual ~aio_hub_internal()
        {
            Shutdown();
            if (m_wsDataPtr.get()) {
                PC_TRACE_BASIC_XXX( "closing epoll" << std::endl << std::flush; );
                int x;
                TRY_SOCKET_OP(x,(::close(m_wsDataPtr->m_sockEPoll)),EAGAIN);
                m_wsDataPtr->m_sockEPoll = INVALID_SOCKET;
                PC_TRACE_BASIC_XXX( "epoll closed" << std::endl << std::flush; );
            }
        }

	    // ======== Start =======================================================
        /**
        *  Starts the thread
        *  typically does not execute in the thread context of the callback thread
        */
        virtual void Start ()
        {
            Task::Start ();
        }

	    // ======== Shutdown =======================================================
        /**
        *  Cancels all activities and shuts down the thread,
        *  typically does not execute in the thread context of the callback thread
        *
        * @returns nothing
        */
        virtual void Shutdown()
        {
            Lock<ConditionalMutex>  l(m_condWork);
            PC_TRACE_BASIC_XXX( "shutting down internal hub" << std::endl << std::flush; );
            m_done = true;
            releaseWorker();
            if (m_working) {
                PC_TRACE_BASIC_XXX( "join thread internal hub" << std::endl << std::flush; );
                m_condWork.Wait();
            }
            for (size_t i = 0; i < m_asc.size(); ++i) {
                if (INVALID_SOCKET != m_asc[i]->sys_fd()) {
                    PC_TRACE_BASIC_XXX( "closing m_asc[i]->sys_fd()" << m_asc[i]->sys_fd() << std::endl << std::flush; );
                    m_asc[i]->close(NULL);
                }
            }
            if (-1 != m_hQueuePipe[0]) {
                PC_TRACE_BASIC_XXX( "removing pipe from epoll" << std::endl << std::flush; );
                int result = ::epoll_ctl(m_wsDataPtr->m_sockEPoll,EPOLL_CTL_DEL,m_hQueuePipe[0],NULL);
                ::close(m_hQueuePipe[0]);
            }
            if (-1 != m_hQueuePipe[1]) {
                ::close(m_hQueuePipe[1]);
            }
            PC_TRACE_BASIC_XXX( "async close requests" << std::endl << std::flush; );
            processAsyncCloseRequests();
            PC_TRACE_BASIC_XXX( "epoll remove requests" << std::endl << std::flush; );
            processPendingEPollRemovals();
            PC_TRACE_BASIC_XXX( "system socket close" << std::endl << std::flush; );
            processPendingCloses();
            PC_TRACE_BASIC_XXX( "shutdown complete" << std::endl << std::flush; );
        }

        virtual bool QueueAsyncSocketContext(PtrView<AsyncSocketContext *> ac) {
            Lock<ConditionalMutex>  l(m_condWork);
            if (m_asc.size() >= (MAXIMUM_WAIT_OBJECTS -1)) {
                // we can't handle more due to win32 api limitations
                return false;
            }
            m_asc.push_back(ac);
            epollRegister(ac->sys_fd());
            for (size_t i = 0; i < m_asc.size(); ++i) {
                PC_TRACE_BASIC_XXX( "adding - m_asc[" << i << "].sysfd=" << m_asc[i]->sys_fd() << std::endl; );
            }
            releaseWorker();
            return true;
        }

        Ptr<AsyncSocketContext *> GetAsyncSocketContext(aio_fd afd) {
            Lock<ConditionalMutex>  l(m_condWork);
            Ptr<AsyncSocketContext *> retval = NULL;
            for (size_t i = 0; i < m_asc.size(); ++i) {
                if (afd == m_asc[i]->fd()) {
                    retval = m_asc[i];
                }
            }
            return retval;
        }

    private:
        // ======== Work =======================================================
        //  sleeps until there is work to do
        //  then retrieves and posts it
        virtual void Work ()
        {
            Lock<ConditionalMutex>  l(m_condWork);
            m_working = true;
            do {
                PC_TRACE("work");
                if (m_asc.empty()) {
                    PC_TRACE("wt-sema");
                    m_condWork.Wait();
                } else {
                    PC_TRACE("wt-epev");
                    doEPollEventWait();
                }
            } while (!m_done);
            PC_TRACE("wt-done");
            m_working = false;
            m_condWork.PostAll();
        }

        void releaseWorker(void)
        {
            m_condWork.PostAll();
            if (-1 != m_hQueuePipe[1]) {
                static char  eventChar = 't';
                ::write(m_hQueuePipe[1],&eventChar,sizeof(eventChar));
            }
        }

        virtual bool epollRegister(int fd, int ignoreFlags = 0) {
            bool retval = false;
            if (m_wsDataPtr.get()) {
                epoll_event l_epollEvent = epoll_event();
                    l_epollEvent.events = default_events & ~ignoreFlags;
                    l_epollEvent.data.fd = fd;      // @@ TODO to optimize, eliminate lookup from fd to ac on event processing
                int result = ::epoll_ctl(m_wsDataPtr->m_sockEPoll,EPOLL_CTL_ADD,fd,&l_epollEvent);
                if (!result) {
                    retval = true;
                } else {
                    retval = false; // only included for debug trace purposes -- redundant otherwise.
                }
            }
            return retval;
        }

        void doEPollEventWait(void) {
            if (m_wsDataPtr.get()) {
                epoll_event events[128];    // arbitrary maximum -- could use tuning later
                memset(events,0,sizeof(events));
                int timeout = 1000;         // arbitrary value -- could use tuning later
                int result;
                {
                    PC_TRACE("epevw-ulck");
                    Unlock<ConditionalMutex> m_ul(m_condWork);
                    // synchronous call to epoll, to see if all pending events have been retrieved
                    result = ::epoll_wait(m_wsDataPtr->m_sockEPoll,&events[0],sizeof(events) / sizeof(epoll_event),0);
                    if (!result) {
                        {
                            Lock<ConditionalMutex> m_ul(m_condWork);
                            processPendingCloses();
                        }
                        result = ::epoll_wait(m_wsDataPtr->m_sockEPoll,&events[0],sizeof(events) / sizeof(epoll_event),timeout);
                    }
                }
                if (result > 0) {
                    for (int n=0; n < result; ++n) {
                        dispatchEPollEvent(events[n]);
                    }
                }
                processAsyncCloseRequests();
                processPendingEPollRemovals();
            }
        }

        void dispatchEPollEvent(epoll_event & ev) {
            if (ev.data.fd == m_hQueuePipe[0]) {
                // our private wakeup event
                char buffer[1];
                while (::read(m_hQueuePipe[0],buffer,sizeof(buffer)) > 0) {
                    ;
                }
                return;
            }
            Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(ev);   // hold an owned reference
            if (ac) {
                bool bKeep = true;
                {
                    Unlock<ConditionalMutex> l_ul(m_condWork);
                    PC_TRACE("depv-ntfy");
                    bKeep = ac->NotifyEvent(ev);
                }
                if (!bKeep) {
                    PC_TRACE_BASIC( "remove fd=" << ev.data.fd << " " << ev.events << std::endl; );
                    m_pendingEPollRemovals.push_back(ev.data.fd);
                }
            } else {
                PC_TRACE_BASIC( "epoll event returned invalid fd=" << ev.data.fd << " " << ev.events << std::endl; );
                for (size_t i = 0; i < m_asc.size(); ++i) {
                    PC_TRACE_BASIC( "m_asc[" << i << "].sysfd=" << m_asc[i]->sys_fd() << std::endl; );
                }
                for (size_t i = 0; i < m_pendingEPollRemovals.size(); ++i) {
                    PC_TRACE_BASIC( "m_pendingEPollRemovals[" << i << "].sysfd=" << m_pendingEPollRemovals[i] << std::endl; );
                }
            }
            PC_TRACE("depv-ulck");
            Unlock<ConditionalMutex> l_ul(m_condWork);
            ac = NULL;
        }

        PtrView<AsyncSocketContext *> getAsyncSocketContext(epoll_event & ev)
        {
            PtrView<AsyncSocketContext *> retval = NULL;
            // @@ REV TODO get rid of O(N) search by storing Ptr in the ev.data.ptr field
            for (size_t i = 0; i < m_asc.size(); ++i) {
                if (ev.data.fd == m_asc[i]->sys_fd()) {
                    retval = m_asc[i];
                }
            }
            return retval;
        }

        void processPendingEPollRemovals(void) {
            PC_TRACE("processPendingEPollRemovals");
            for (size_t i = 0; i < m_pendingEPollRemovals.size(); ++i) {
                PC_TRACE_BASIC_XXX( "m_pendingEPollRemovals[" << i << "].sysfd=" << m_pendingEPollRemovals[i] << std::endl; );
                int result = ::epoll_ctl(m_wsDataPtr->m_sockEPoll,EPOLL_CTL_DEL,m_pendingEPollRemovals[i],NULL);
                if (0 != result) {
                    PC_TRACE("epoll delete failed");
                }
                removeAsyncSocketContext(m_pendingEPollRemovals[i]);
                m_pendingCloses.push_back(m_pendingEPollRemovals[i]);
            }
            m_pendingEPollRemovals.clear();
        }

        void processPendingCloses(void) {
            PC_TRACE("processPendingCloses");
            for (size_t i = 0; i < m_pendingCloses.size(); ++i) {
                PC_TRACE_BASIC_XXX( "processPendingCloses[" << i << "].sysfd=" << m_pendingCloses[i] << std::endl; );
                int x;
                TRY_SOCKET_OP(x,(::close(m_pendingCloses[i])),EAGAIN);
                PC_TRACE_BASIC_XXX( "processPendingCloses[" << i << "].sysfd=" << m_pendingCloses[i] << " result=" << x << std::endl; );
            }
            m_pendingCloses.clear();
        }

        void processAsyncCloseRequests(void) {
            PC_TRACE("processAsyncCloseRequests");
            for (size_t i = 0; i < m_asc.size(); ++i) {
                if (INVALID_SOCKET == m_asc[i]->sys_fd()) {
                    PC_TRACE_BASIC_XXX( "processAsyncCloseRequests[" << i << "].sysfd=" << m_asc[i]->sys_fd_saved() << std::endl; );
                    m_pendingEPollRemovals.push_back(m_asc[i]->sys_fd_saved());
                }
            }
            return;
        }

        void removeAsyncSocketContext(int fd)
        {
            PC_TRACE("removeAsyncSocketContext");
            for (size_t i = 0; i < m_asc.size(); ++i) {
                if (fd == m_asc[i]->sys_fd_saved()) {
                    PC_TRACE_BASIC_XXX( "remove async ctx sysfd=" << fd << std::endl << std::flush; );
                    Ptr<AsyncSocketContext *> ac = m_asc[i];   // hold an owned reference
                    m_asc.erase(m_asc.begin() + i);            // does not erase pointee
                    PC_TRACE_BASIC_XXX( "removed async ctx sysfd=" << fd << std::endl << std::flush; );
                    Unlock<ConditionalMutex> l_ul(m_condWork);
                    ac = NULL;
                    break;
                }
            }
            return;
        }
    };
}

using namespace at;

namespace
{
    /** Make an OS-level address structure. */
    void
    to_os_address(aio_ip4_addr addr, sockaddr_in *o)
    {
        o->sin_family = AF_INET;
        o->sin_addr.s_addr = htonl(addr.first);
        o->sin_port = htons(addr.second);
    }

    /** Make an aio-level address structure. */
    bool
    to_aio_address(sockaddr_in addr, aio_ip4_addr *o)
    {
        o->first = ntohl(addr.sin_addr.s_addr);
        o->second = ntohs(addr.sin_port);
        return true;
    }
}

const int aio_flag::data_available = (1 << 0);
const int aio_flag::connection_waiting = (1 << 1);
const int aio_flag::closing = (1 << 2);
const int aio_flag::writeable = (1 << 3);

const int at::aio_state::listening = 1;
const int at::aio_state::connecting = 2;
const int at::aio_state::connected = 3;
const int at::aio_state::datagram = 4;
const int at::aio_state::closed = 5;

const int at::aio_error::unexpected = 1;
const int at::aio_error::loser = 2;
const int at::aio_error::state = 3;
const int at::aio_error::invalid = 4;
const int at::aio_error::closing = 5;
const int at::aio_error::shutdown = 6;
const int at::aio_error::unreachable = 7;
const int at::aio_error::peer_reset = 8;
const int at::aio_error::bind_failure = 9;
const int at::aio_error::invalid_address = 10;

const aio_fd aio_fd::invalid;

AsyncSocketContext::~AsyncSocketContext()
{
    AideCancel();
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (INVALID_SOCKET != m_socket) {
        int x;
        TRY_SOCKET_OP(x,(::close(m_socket)),EAGAIN);
        m_socket = INVALID_SOCKET;
    }
}

const char* at::aio_error::to_string(int e)
{
    switch (e)
    {
    case unexpected:
        return "unexpected";
    case loser:
        return "loser";
    case state:
        return "state";
    case invalid:
        return "invalid";
    case closing:
        return "closing";
    case shutdown:
        return "shutdown";
    case unreachable:
        return "unreachable";
    case peer_reset:
        return "peer_reset";
    default:
        return "<unrecognized error code>";
    }
}

const char* at::aio_state::to_string(int e)
{
    switch (e)
    {
    case aio_state::listening:
        return "listening";
    case aio_state::connecting:
        return "connecting";
    case aio_state::connected:
        return "connected";
    case aio_state::datagram:
        return "datagram";
    case aio_state::closed:
        return "closed";
    default:
        return "<unrecognized state>";
    }
}

std::string at::aio_flag::to_string(int e)
{
    std::string s;
    if (e & aio_flag::data_available) s += "data_available";
    if (e & aio_flag::connection_waiting)
    {
        if (s.size()) s += ",";
        s += "connection_waiting";
    }
    if (e & aio_flag::closing)
    {
        if (s.size()) s += ",";
        s += "closing";
    }
    return s;
}

aio_monitor_if::~aio_monitor_if()
{
}

void
aio_monitor_if::become_monitor(aio_fd fd, aio_hub *source)
{
}

void
aio_monitor_if::state_changed(
    aio_fd fd, aio_hub *source, int state, int flags)
{
}

void
aio_monitor_if::send_completed(
    aio_fd fd,
    aio_hub *source,
    Ptr<Buffer *> i_buffer,
    int i_sent_size,
    aio_ip4_addr dest,
    int err)
{
}

aio_hub*
aio_hub::new_hub()
{
    // singleton for now @@ TODO
    static aio_hub theHub;
    return &theHub;
}

Ptr<AsyncSocketContext *> aio_hub::getAsyncSocketContext(aio_fd afd)
{
    Lock<Mutex> lock(m_stateMutex);
    Ptr<AsyncSocketContext *> ac;
    for (size_t i=0; i < m_internal_hubs.size(); ++i) {
        // @@ TODO -- currently this is O(N) with number of sockets -- consider O(log(N))
        // data structures and algorithms
        ac = m_internal_hubs[i]->GetAsyncSocketContext(afd);    // try to get context
        if (ac) {
            break;
        }
    }
    return ac;
}

bool aio_hub::queueAsyncSocketContext(PtrView<AsyncSocketContext *> ac)
{
    Lock<Mutex> lock(m_stateMutex);
    bool retval = false;
    for (size_t i=0; i < m_internal_hubs.size(); ++i) {
        retval = m_internal_hubs[i]->QueueAsyncSocketContext(ac);   // try to enqueue request
        if (retval) {
            break;
        }
    }
    if (!retval) {  // all existing hubs are full, create a new one
        aio_hub_internal *ah = new aio_hub_internal();
        retval = ah->QueueAsyncSocketContext(ac);                   // try to enqueue request
        m_internal_hubs.push_back(ah);
        ah->Start();
    }
    return retval;
}

bool
aio_hub::connect(
    aio_ip4_addr dest, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex,
    aio_fd *fd_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    if (monitor || fd_o ) { PC_TRACE("connecting");
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_STREAM);
        retval = sock->connect(dest);           // initiate client request on socket
        if (retval) {
            retval = queueAsyncSocketContext(sock);     // enqueue request
            if (retval) {
                fd_o? (*fd_o = sock->fd()) : false;
                err = 0;
                retval = true;
            } else {
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {
            err = aio_error::unexpected;
        }
    }
    err_o? (*err_o = err) : false;
    return retval;
}

bool
aio_hub::listen(
    aio_ip4_addr addr, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex,
    aio_fd *fd_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    if (monitor || fd_o ) {
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_STREAM);
        retval = sock->listen(addr);            // initiate client request on socket
        if (retval) {
            retval = queueAsyncSocketContext(sock);     // enqueue request
            if (retval) {
                fd_o? (*fd_o = sock->fd()) : false;
                err = 0;
                retval = true;
            } else {
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {
            err = aio_error::bind_failure;
        }
    }
    err_o? (*err_o = err) : false;
    return retval;
}

bool
aio_hub::listen_datagram(
    aio_ip4_addr addr, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex,
    aio_fd *fd_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    if (monitor || fd_o ) {
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_DGRAM);
        retval = sock->listen(addr, true);      // initiate client request on socket
        if (retval ) {
            retval = queueAsyncSocketContext(sock);     // enqueue request
            if (retval) {
                fd_o? (*fd_o = sock->fd()) : false;
                err = 0;
                retval = true;
            } else {
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {
            err = aio_error::bind_failure;
        }
    }
    err_o? (*err_o = err) : false;
    return retval;
}

bool
aio_hub::accept(
    aio_fd listening_fd, AioMonitorLead *monitor,
    PtrView<MutexRefCount *> i_stateMutex, 
    aio_fd *conn_o, aio_ip4_addr *addrp_o, int *err_o)
{
    // @@ TODO this function body should be a template with a replacable parameter for the socket.func
    int err = aio_error::invalid;
    bool retval = false;
    Ptr<AsyncSocketContext *> listening_ac = getAsyncSocketContext(listening_fd);
    if (listening_ac && (monitor || conn_o )) {
        PC_TRACE_BASIC( "accepting on pubfd=" << listening_fd.m_opaque_handle << " sysfd=" << listening_ac->sys_fd() << std::endl; );
        Ptr<AsyncSocketContext *> sock = new AsyncSocketContext(monitor,i_stateMutex,this,SOCK_DGRAM);
        retval = sock->accept(listening_ac,conn_o,addrp_o); // initiate client request on socket
        if (retval) {
            retval = queueAsyncSocketContext(sock);     // enqueue request
            if (retval) {
                PC_TRACE_BASIC( "accept enqueued sysfd=" << sock->sys_fd() << std::endl; );
                err = 0;
                retval = true;
            } else {   PC_TRACE("accept::shutdown");
                err = aio_error::shutdown;              // @@ TODO -- or out of memory!
            }
        } else {   PC_TRACE("accept::unexpected");
            err = aio_error::unexpected;
        }
    }
    if (!retval) {
        err_o? (*err_o = err) : 0;
    }
    return retval;
}

bool
aio_hub::set_monitor(aio_fd fd, AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->set_monitor(monitor,i_stateMutex,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}


bool
aio_hub::get_local_address(aio_fd fd, aio_ip4_addr *addrp_o, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->get_local_address(addrp_o,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::get_remote_address(aio_fd fd, aio_ip4_addr *addrp_o, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->get_remote_address(addrp_o,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::read(aio_fd fd, char *buffer_o, int *sizep_io, int *err_o)
{
    return this->recv(fd, buffer_o, sizep_io, 0, err_o);
}

bool
aio_hub::write(aio_fd fd, Ptr<Buffer *> i_buffer, int *err_o)
{   PC_TRACE("write");
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) { PC_TRACE("writing");
        retval = ac->send_or_write(i_buffer, aio_ip4_addr(), err_o, AsyncSocketContext::isNotDatagram);
    } else { PC_TRACE("no writing");
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::recv(aio_fd fd, char *buffer_o, int *sizep_io,aio_ip4_addr *addrp_o, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->recv(buffer_o,sizep_io,addrp_o,err_o);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::send(aio_fd fd, Ptr<Buffer *> i_buffer, aio_ip4_addr dest, int *err_o)
{   PC_TRACE("send");
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->send_or_write(i_buffer, dest, err_o, AsyncSocketContext::isDatagram);
    } else {
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

bool
aio_hub::close(aio_fd fd, int *err_o)
{
    bool retval = false;
    Ptr<AsyncSocketContext *> ac = getAsyncSocketContext(fd);
    if (ac) {
        retval = ac->close(err_o);  // queues an event request
    } else {
        retval = true;
        err_o? (*err_o = aio_error::unexpected) : 0;
    }
    return retval;
}

aio_hub::~aio_hub()
{
    Lock<Mutex> lock(m_stateMutex);
    PC_TRACE_BASIC_XXX( "dtor aio_hub" << std::endl << std::flush; );
    while (!m_internal_hubs.empty()){
        PC_TRACE_BASIC_XXX( "begin shutdown aio_hub_internal" << std::endl << std::flush; );
        m_internal_hubs[0]->Shutdown();
        PC_TRACE_BASIC_XXX( "shutdown aio_hub_internal complete" << std::endl << std::flush; );
        delete m_internal_hubs[0];
        PC_TRACE_BASIC_XXX( "deleted aio_hub_internal" << std::endl << std::flush; );
        m_internal_hubs.erase(m_internal_hubs.begin());
    }
    PC_TRACE_BASIC_XXX( "dtor aio_hub COMPLETE" << std::endl << std::flush; );
}

aio_hub::aio_hub(void) :
    m_internal_hubs(),
    m_stateMutex(Mutex::Recursive)
{
    PC_TRACE_BASIC_XXX( "ctor aio_hub COMPLETE" << std::endl << std::flush; );
}

// message creation methods
bool AsyncSocketContext::connect(aio_ip4_addr &addr)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    if (m_socket != INVALID_SOCKET) {
        sockaddr_in os_addr;
        to_os_address(addr, &os_addr);
        m_connectPending = true;
        if (SOCKET_ERROR == ::connect(m_socket,(sockaddr *)(&os_addr),sizeof(os_addr))) {
            // try asynchronous completion proc
            if (EINPROGRESS == errno) {
                errno = 0;
                retval = true;
            }
        } else {
            // synchronous completion
            retval = true;
        }
    }
    return retval;
}

bool AsyncSocketContext::listen(aio_ip4_addr &addr, bool isDataGram)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    if (m_socket != INVALID_SOCKET) {
        sockaddr_in os_addr;
        to_os_address(addr, &os_addr);
        int result;
        TRY_SOCKET_OP(result,(::bind(m_socket,(sockaddr *)(&os_addr),sizeof(os_addr))),EAGAIN);
        PC_TRACE_BASIC_XXX( "bound sysfd=" << m_socket << " addr=" << addr.second << std::endl; );
        if (SOCKET_ERROR != result) {
            int result2 = 0;
            if (!isDataGram) {
                m_listening = true;
                TRY_SOCKET_OP(result2,(::listen(m_socket,SOMAXCONN)),EAGAIN);
                PC_TRACE_BASIC_XXX( "listening sysfd=" << m_socket << std::endl; );
            }
            if (SOCKET_ERROR != result2) {
                retval = true;
            } else {
                int result3 = errno;
            }
        } else {
        }
    }
    return retval;
}

AsyncSocketContext::AsyncSocketContext(AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex,
                                       aio_hub *hub, int stype) :
    m_fdx(m_n++),
    m_socket(INVALID_SOCKET),
    m_socketFD(INVALID_SOCKET),
    m_type(stype),
    m_closed(false),
    m_connectPending(false),
    m_listening(false),
    m_haveLocalAddress(false),
    m_local_addr_len(sizeof(m_local_addr)),
    m_havePeerAddress(false),
    m_peer_addr_len(sizeof(m_peer_addr)),
    m_hub(hub),
    m_queued_write_params(),
    m_stateMutex(i_stateMutex)
{
    if (m_fdx == -1) {  // disallow reserved invalid value
        m_fdx = m_n++;
    }
    ::memset(&m_local_addr,0,sizeof(m_local_addr));
    ::memset(&m_peer_addr,0,sizeof(m_peer_addr));
    // done static init

    if (monitor) {
        AideAssociate(monitor, m_stateMutex);
        CallLead().VoidCall(&AioMonitorLead::become_monitor,m_fdx,m_hub);
    }

    TRY_SOCKET_OP(m_socket,(::socket(PF_INET,m_type,(m_type == SOCK_STREAM)? IPPROTO_TCP : IPPROTO_UDP)),EAGAIN);
    m_socketFD = m_socket;
    PC_TRACE("create socket");
    if (INVALID_SOCKET != m_socket) {
        int x;
        TRY_SOCKET_OP(x,(::fcntl(m_socket, F_SETFL, O_NONBLOCK)),EAGAIN);
        if (x != -1) {
            PC_TRACE_BASIC( "created socket pubfd=" << m_fdx << " sys_fd=" << m_socket << std::endl; );
            return; // succeed in opening non-blocking socket
        }
        PC_TRACE("create socket - fcntl failed");
    }
    if (INVALID_SOCKET != m_socket) {
        PC_TRACE("create socket - cleanup");
        int x;
        TRY_SOCKET_OP(x,(::close(m_socket)),EAGAIN);
        m_socket = INVALID_SOCKET;
    }
    PC_TRACE("create socket - done");
    if (monitor) {
        tellMonitor(aio_state::closed,0);
    }
    return;
}

bool AsyncSocketContext::accept(PtrView<AsyncSocketContext *> listening_asc, aio_fd *conn_o,aio_ip4_addr *addr)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    sockaddr_in l_peer_addr;
    socklen_t l_peer_addr_len = sizeof(l_peer_addr);

    SOCKET l_socket;
    TRY_SOCKET_OP(l_socket,(::accept(listening_asc->m_socket,(sockaddr *)&l_peer_addr, &l_peer_addr_len)),EAGAIN);
    if ((SOCKET_ERROR != l_socket) && (l_peer_addr_len == sizeof(l_peer_addr))) {
        int x;
        if (INVALID_SOCKET != m_socket) {   //@@ TODO prevent constructor from creating wasted socket
            TRY_SOCKET_OP(x,(::close(m_socket)),EAGAIN);
            m_socket = INVALID_SOCKET;
        }
        m_socketFD = l_socket;
        TRY_SOCKET_OP(x,(::fcntl(l_socket, F_SETFL, O_NONBLOCK)),EAGAIN);
        if (x != -1) {
            m_socket = l_socket;
            m_peer_addr = l_peer_addr;
            m_peer_addr_len = l_peer_addr_len;
            m_havePeerAddress = true;
            retval = true;
        } else {
            TRY_SOCKET_OP(x,(::close(l_socket)),EAGAIN);
            m_socket = INVALID_SOCKET;
        }
    }
    
    if ( retval ) {
        
        if ( conn_o )
        {
            *conn_o = this->fd();
        }
        
        if ( addr )
        {
            to_aio_address(m_peer_addr, addr);
        }
        
    } else {
        tellMonitor(aio_state::closed,0);
    }
    return retval;
}

bool AsyncSocketContext::recv(char *buffer_o, int *sizep_io,aio_ip4_addr *addrp_o, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    int err = aio_error::unexpected;
    if (buffer_o && sizep_io) {
        // in winsock, os_addr is not returned for connection oriented sockets, so we
        // must initialize it to something meaningful
        sockaddr_in os_addr;
        socklen_t os_addr_size = sizeof(os_addr);
        if (m_havePeerAddress) {
            os_addr = m_peer_addr;
        } else {
            // @@ TODO -- this is questionable, but the real question is what is the
            // representation of an "invalid" aio_ip4_addr ...
            ::memset(&os_addr,0,sizeof(os_addr));
        }

        int bytes;
        TRY_SOCKET_OP(bytes,(::recvfrom(m_socket,buffer_o,*sizep_io,0,(sockaddr*)&os_addr,&os_addr_size)),0);
        if (SOCKET_ERROR != bytes)
        {
            if ( addrp_o )
            {
                to_aio_address( os_addr, addrp_o );
            }
            *sizep_io = bytes;
            retval = true;
        } else {
            int error = errno;
            switch (error) {
                case EWOULDBLOCK:
                    err = aio_error::loser;     // @@ TODO -- this is a terrible error name, replace
                    break;
                default:
                // @@ there are certain errors we may want to quantify as a courtesy to the caller
                    break;
            }
        }
    }
    if (!retval) {
        err_o? (*err_o = err): 0;
    }
    PC_TRACE("asc::recv::ret");
    return retval;
}

bool AsyncSocketContext::close(int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = true;
    PC_TRACE_BASIC_XXX( "trigger async close on sysfd=" << m_socket << std::endl << std::flush; );
    m_socket = INVALID_SOCKET;  // trigger async close on hub worker thread
    m_closed = true;
    err_o? (*err_o = 0): 0;
    return retval;
}

bool AsyncSocketContext::get_local_address(aio_ip4_addr *addrp_o, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (addrp_o) {
        if (m_havePeerAddress) {
            to_aio_address(m_local_addr, addrp_o);
        } else {
            m_local_addr_len = sizeof(m_local_addr);
            if (SOCKET_ERROR != ::getsockname(m_socket, (sockaddr*)&m_local_addr, &m_local_addr_len)) {
                to_aio_address(m_local_addr, addrp_o);
                m_haveLocalAddress = true;
            }
        }
    }
    (!m_haveLocalAddress && err_o)? (*err_o = aio_error::unexpected): 0;
    return m_haveLocalAddress;
}

bool AsyncSocketContext::get_remote_address(aio_ip4_addr *addrp_o, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (addrp_o) {
        if (m_havePeerAddress) {
            to_aio_address(m_peer_addr, addrp_o);
        } else {
            m_peer_addr_len = sizeof(m_peer_addr);
            if (SOCKET_ERROR != ::getpeername(m_socket, (sockaddr*)&m_peer_addr, &m_peer_addr_len)) {
                to_aio_address(m_peer_addr, addrp_o);
                m_havePeerAddress = true;
            }
        }
    }
    (!m_havePeerAddress && err_o)? (*err_o = aio_error::unexpected): 0;
    return m_havePeerAddress;
}



// @@ TODO -- overload for datagram vs. connected sockets
bool AsyncSocketContext::send_or_write(Ptr<Buffer *> i_buffer, aio_ip4_addr dest, int *err_o,bool datagram)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex); PC_TRACE("send or write");

    char *buffer = i_buffer->Region().m_mem;
    int size = i_buffer->Region().m_allocated;

    bool retval = false;
    retval &= !size;
    if (!retval) {
        sockaddr_in os_addr;
        to_os_address(dest, &os_addr);
        // sendto, with flags = 0, os_addr ignored on non-datagram sockets
        int result;
        TRY_SOCKET_OP(result,(::sendto(m_socket, buffer, size, 0, (sockaddr*)&os_addr,sizeof(os_addr))),0);
        if (SOCKET_ERROR != result) {
            write_params l_write_params;

            l_write_params.m_sent_size = result;
            l_write_params.m_dest_addr = dest;
            l_write_params.m_err = (datagram && (result != size))? aio_error::unexpected: 0;
            l_write_params.m_buffer = i_buffer;

            m_queued_write_params.push_back(l_write_params);

            retval = (l_write_params.m_err == 0);

        } else {
            err_o? (*err_o = aio_error::unexpected): 0;
        }
    }
    return retval;
}

bool AsyncSocketContext::set_monitor(AioMonitorLead *monitor, PtrView<MutexRefCount *> i_stateMutex, int *err_o)
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    bool retval = false;
    AideCancel();
    if (monitor) {
        m_stateMutex = i_stateMutex;
        AideAssociate(monitor, m_stateMutex);
        retval = CallLead().VoidCall(&AioMonitorLead::become_monitor,m_fdx,m_hub);
    }

    err_o? (*err_o = 0): 0;
    return retval;
}

// socket events {return "false" only to terminate this context for final state
/* reference should match aio.h

namespace aio_flag
    extern const int data_available;
    extern const int connection_waiting;
    extern const int closing;
namespace aio_state
    extern const int listening;
    extern const int connecting;
    extern const int connected;
    extern const int datagram;
    extern const int closed;
*/

bool AsyncSocketContext::tellMonitor( int state, int flags)
{
    bool retval = false;
    if (flags) {
        PC_TRACE_BASIC( "tellMonitor::" << state << ":" << flags << " fd=" << m_socketFD << std::endl; );
    }
    PC_TRACE_BASIC( "tellMonitor::" << state << ":" << flags << " fd=" << m_socketFD << std::endl; );
    retval = CallLead().VoidCall(&AioMonitorLead::state_changed, m_fdx, m_hub, state, flags);
    PC_TRACE("told monitor");
    return retval;
}

bool AsyncSocketContext::sendCompleted(void)
{
    bool retval = false;
    write_params l_write_params = m_queued_write_params.front();
    Ptr<Buffer *> l_buffer = l_write_params.m_buffer;
    aio_ip4_addr dest;
    dest.first = l_write_params.m_dest_addr.first;
    dest.second = l_write_params.m_dest_addr.second;

    if (!l_write_params.m_err) {
        if (l_write_params.m_sent_size == l_buffer->Region().m_allocated) {
            // the buffer was completed, remove and report
            m_queued_write_params.pop_front();

            retval = CallLead().VoidCall(
                &AioMonitorLead::send_completed,
                m_fdx,
                m_hub,
                l_buffer,
                l_write_params.m_sent_size,
                dest,
                l_write_params.m_err
            );
        } else {
            // partial buffer remains, leave on front, try writing more
            char *buffer = l_buffer->Region().m_mem + l_write_params.m_sent_size;
            int size = l_buffer->Region().m_allocated - l_write_params.m_sent_size;
            sockaddr_in os_addr;
            to_os_address(dest, &os_addr);
            // sendto, with flags = 0, os_addr ignored on non-datagram sockets
            int result;
            TRY_SOCKET_OP(result,(::sendto(m_socket, buffer, size, 0, (sockaddr*)&os_addr,sizeof(os_addr))),0);
            if (SOCKET_ERROR != result) {
                m_queued_write_params.front().m_sent_size += result;
                m_queued_write_params.front().m_err = 0;

                retval = (m_queued_write_params.front().m_err == 0);

            } else {
                m_queued_write_params.front().m_err = aio_error::unexpected;
            }
        }
    } else {
        // there was an error on the previous write, remove and report
        m_queued_write_params.pop_front();
        retval = CallLead().VoidCall(
            &AioMonitorLead::send_completed,
            m_fdx,
            m_hub,
            l_buffer,
            l_write_params.m_sent_size,
            dest,
            l_write_params.m_err
        );
    }
    return retval;
}

bool AsyncSocketContext::notifyRead(int statusCode)
{
    bool retval = false;
    if (!statusCode) {
        retval = true;
        tellMonitor(aio_state::connected, aio_flag::data_available);
    } else {
        tellMonitor(aio_state::closed, 0);
    }
    return retval;
}

bool AsyncSocketContext::notifyWrite(int statusCode)
{
    bool retval = false;
    if (!statusCode) {
        retval = true;
        // in the documentation for EventSelect, it is indicated that the FD_WRITE event
        // means "you can write now", as opposed to "your write has completed"
        // which means that it is also set upon initial "connect/accept" socket functions
        if (!m_queued_write_params.empty()) {
            sendCompleted();
        }
        tellMonitor(aio_state::connected, aio_flag::writeable);
    } else {
        tellMonitor(aio_state::closed, 0);
    }
    return retval;
}

bool AsyncSocketContext::notifyOOB(int statusCode)
{
    bool retval = true;
    ::OutputDebugString("AsyncSocketContext::notifyOOB\n");
    // @@ TODO -- need to eat the incoming data, and need monitor API/state/flags
    return retval;
}

bool AsyncSocketContext::notifyAccept(int statusCode)
{
    bool retval = false;
    switch (statusCode) {
        case 0:
            tellMonitor(aio_state::listening,aio_flag::connection_waiting);
            retval = true;
            break;
        case EAFNOSUPPORT:       // Addresses in the specified family cannot be used with this socket.
        case ENETUNREACH:        // The network cannot be reached from this host at this time.
        case ENOBUFS:            // No buffer space is available. The socket cannot be connected.
        case ETIMEDOUT:          // An attempt to connect timed out without establishing a connection
        case ECONNREFUSED:       // The attempt to connect was forcefully rejected.
        default:
            ::OutputDebugString("AsyncSocketContext::notifyAccept - BAD\n");
            tellMonitor(aio_state::closed,0);
            break;
    }
    return retval;
}

bool AsyncSocketContext::notifyConnect(int statusCode)
{
    bool retval = false;
    switch (statusCode) {
        case 0:
            tellMonitor(aio_state::connected,0);
            retval = true;
            break;
        case EAFNOSUPPORT:       // Addresses in the specified family cannot be used with this socket.
        case ENETUNREACH:        // The network cannot be reached from this host at this time.
        case ENOBUFS:            // No buffer space is available. The socket cannot be connected.
        case ETIMEDOUT:          // An attempt to connect timed out without establishing a connection
        case ECONNREFUSED:       // The attempt to connect was forcefully rejected.
        default:
            ::OutputDebugString("AsyncSocketContext::notifyConnect - BAD\n");
            tellMonitor(aio_state::closed,0);
            break;
    }
    return retval;
}

bool AsyncSocketContext::notifyClose(int statusCode)
{
    bool retval = false;
    if (m_closed || !m_listening) { // epoll returns an EPOLLHUP when a socket becomes bound and listening ...? workaround
        m_socket = INVALID_SOCKET;  // this is to prevent an undocumented race condition in some winsock implementations
                                    // wherein the WS reissues the same socket number to another client app before we get the
                                    // async close notification -- we need to prevent our client app from doing any
                                    // further socket operations (such as "close") which invalidate the other client's state.
                                    // good idea anyway, since our client should not really be doing anything after being told
                                    // that the session is closed
        if (!m_connectPending) {
            tellMonitor(aio_state::connected, aio_flag::closing);
        }
        tellMonitor(aio_state::closed, 0);
    } else {
        retval = true;
    }
    return retval;
}

bool AsyncSocketContext::notifyQOS(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyQOS\n");
    return retval;
}

bool AsyncSocketContext::notifyGroupQOS(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyGroupQOS\n");
    return retval;
}

bool AsyncSocketContext::notifyRoutingInterfaceChange(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyRoutingInterfaceChange\n");
    return retval;
}

bool AsyncSocketContext::notifyAddressListChange(int statusCode)
{
    bool retval = true;
    // @@ TODO -- need monitor API/state/flags
    ::OutputDebugString("AsyncSocketContext::notifyAddressListChange\n");
    return retval;
}

bool AsyncSocketContext::NotifyEvent(epoll_event & ev)  // return false to delete "this" context
{
    Lock<Ptr<MutexRefCount *> > lock(m_stateMutex);
    if (m_closed) {
        PC_TRACE_BASIC( "at_aio::NotifyEvent::closed already!" << std::endl; );
        notifyClose(0);
        return false;
    }
    PC_TRACE_BASIC( "at_aio::NotifyEvent ev=" << ev.events << " listening=" << m_listening << " connecting=" << m_connectPending << " sys_fd=" << sys_fd() << std::endl; );
    bool retval = true;
    retval &= (ev.events & EPOLLERR)? notifyClose(0): true; // process error first @@ TODO how to get error code from fd ?
    // remaining flags have overloaded meanings during different connection states -- see "man 7 socket"
    if (m_listening) {
        retval &= NotifyListenEvent(ev);
    } else if (m_connectPending) {
        retval &= NotifyConnectEvent(ev);
    } else {
        // the order of the following event checks matters to the client
        retval &= (ev.events & EPOLLPRI)? notifyOOB(0): true;
        retval &= (ev.events & EPOLLIN)? notifyRead(0): true;
        retval &= (ev.events & EPOLLHUP)? notifyClose(0): true; // process close before writing
        retval &= (ev.events & EPOLLOUT)? notifyWrite(0): true;
    }
    return retval;
}

bool AsyncSocketContext::NotifyListenEvent(epoll_event & ev)  // return false to delete "this" context
{
    bool retval = true;
    if (ev.events & (EPOLLOUT | EPOLLIN)) {
        retval = notifyAccept(0);
    }
    return retval;
}

bool AsyncSocketContext::NotifyConnectEvent(epoll_event & ev)  // return false to delete "this" context
{
    bool retval = true;
    if (ev.events & (EPOLLOUT | EPOLLIN)) {
        PC_TRACE_BASIC( "at_aio::NotifyConnectEvent ev=" << ev.events << " listening=" << m_listening << " connecting=" << m_connectPending << " sys_fd=" << sys_fd() << std::endl; );
        m_connectPending = false;
        retval = notifyConnect(0);
    }
    return retval;
}
