/**
 * \file at_gx86_log_time.h
 *
 * \author Bradley Austin
 *
 */


#ifndef x_at_gx86_log_time_h_x
#define x_at_gx86_log_time_h_x 1


#include "at_log_ctime.h"
#include "at_gx86_log_ctime.h"


namespace at
{

    typedef  LogManagerTimePolicy_CTime  LogManagerTimePolicy_Default;

    typedef  LogWriterTimeFormatPolicy_CTime_gx86  LogWriterTimeFormatPolicy_Default;

}


#endif
