/*
 *
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
//

/**
 * at_gx86_file.h
 *
 */

#include "at_exports.h"
#include "at_twinmt_basic.h"

#ifndef x_at_gx86_file_h_x
#define x_at_gx86_file_h_x 1

// Austria namespace
namespace at
{
// ======== FileErrorBasic  =============================================
/**
*       Concrete implementation of FileError
*
*/
class AUSTRIA_EXPORT FileErrorBasic : public FileError
{
public:

    // ======== FileErrorBasic  =============================================
    /**
    *       @param i_name A description of the error
    *       @param i_errorno A number to associate with this error (Found in Winerror.h)
    */

    FileErrorBasic ( const char* i_name, long i_errorno = -1 );

    // ======== FileErrorBasic  =============================================
    /**
    *
    *       @param i_errorno The number of the error. Used for throwing.
    */

    FileErrorBasic ( long i_errorno );

    // ======== FileErrorBasic  =============================================
    /**
    *
    *       @param i_fileerror Create this object using the info form another 
    *                          FileError object
    */

    FileErrorBasic ( const FileError& i_fileerror );

public:

    // ======== What =============================================
    /**
    *   @return returns the Description of this error
    */

    const char * What () const { return m_errortext; }

    // ======== Operator =  =============================================
    /**
    *       Operator = Copies the contents of another FileErrorBasic to this one
    *
    */

    const FileErrorBasic & operator = ( const FileErrorBasic* fe );

    // ======== Operator ==  =============================================
    /**
    *       Operator == Compares two FileError's
    *
    */

    bool operator == ( const FileError& fe ) const { return fe.ErrorCode () == ErrorCode (); } 


protected:

    // ======== ErrorCode =============================================
    /**
    *       @return return the error code
    */

    long ErrorCode () const { return m_errorno; }

private:

    /**
    *   m_errortext The internal Error text
    */

    const char* m_errortext;

    /**
    *   m_errortext The internal Error code
    */

    long m_errorno;
};

// ======== FileInitializer =============================================
/**
 *  FileInitializer sets initial conditions for the file subsusytem
 */
class FileInitializer : public Initializer
{
    public:
  
    FileInitializer ( int & argc, const char ** & argv );
    ~FileInitializer ();
};
    
}; // namespace

#endif // x_at_gx86_file_h_x


