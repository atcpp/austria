//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_posix_time.h
 *
 */

#ifndef x_at_posix_time_h_x
#define x_at_posix_time_h_x 1

#include "at_exports.h"
#include "at_types.h"

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>


// Austria namespace
namespace at_posix
{


// ======== PosixTimeval ==============================================
/**
 * Create a timeval from a TimeInterval
 *
 * @return The timeval that corresponds to i_time_interval
 */
inline timeval PosixTimeval( const at::TimeInterval & i_time_interval )
{
    timeval l_val;
    at::TimeInterval l_remainder;

    l_val.tv_sec = i_time_interval.Secs( &l_remainder );
    l_val.tv_usec = l_remainder.MicroSecs();

    return l_val;
}


// ======== PosixTimespec =============================================
/**
 * Create a timespec from a TimeInterval
 *
 * @return The timespec that corresponds to i_time_interval
 */
inline timespec PosixTimespec( const at::TimeInterval & i_time_interval )
{
    timespec l_val;
    at::TimeInterval l_remainder;

    l_val.tv_sec = i_time_interval.Secs( &l_remainder );
    l_val.tv_nsec = l_remainder.NanoSecs();

    return l_val;
}


// ======== ToTimeInterval ============================================
/**
 * Create a TimeInterval from a timeval
 *
 * @return The TimeInterval that corresponds to i_timeval
 */
inline at::TimeInterval ToTimeInterval( const timeval & i_timeval )
{
    return
        at::TimeInterval::Secs( i_timeval.tv_sec ) +
        at::TimeInterval::MicroSecs( i_timeval.tv_usec );
}


// ======== ToTimeInterval ============================================
/**
 * Create a TimeInterval from a timespec
 *
 * @return The TimeInterval that corresponds to i_timespec
 */
inline at::TimeInterval ToTimeInterval( const timespec & i_timespec )
{
    return
        at::TimeInterval::Secs( i_timespec.tv_sec ) +
        at::TimeInterval::NanoSecs( i_timespec.tv_nsec );
}


inline at::TimeStamp PosixEpoch()
{

    /*
     *  The difference between the Posix epoch (Midnight 1-1-1970) and
     *  the Austria epoch (Midnight 1-1-1601), in units of 100
     *  nanoseconds.
     */
    static const long long l_offset =
        (  ( 369 * 365 )       // 369 years between 1-1-1601 and 1-1-1970
         + 89                  // 89 leap years between 1-1-1601 and 1-1-1970
                               // (1700, 1800, and 1900 were not leap years!)
        ) * 864000000000LL;    // number of 100 nanosecond intervals in a day
                               // (leap seconds not considered!)

    return at::TimeStamp( l_offset );
}


// ======== PosixTimeval ==============================================
/**
 * Create a timeval from a TimeStamp
 *
 * @return The timeval that corresponds to i_time_stamp
 */
inline timeval PosixTimeval( const at::TimeStamp & i_time_stamp )
{
    return PosixTimeval( i_time_stamp - PosixEpoch() );
}


// ======== PosixTimespec =============================================
/**
 * Create a timespec from a TimeStamp
 *
 * @return The timespec that corresponds to i_time_stamp
 */
inline timespec PosixTimespec( const at::TimeStamp & i_time_stamp )
{
    return PosixTimespec( i_time_stamp - PosixEpoch() );
}


// ======== ToTimeStamp ===============================================
/**
 * Create a TimeStamp from a timeval
 *
 * @return The TimeStamp that corresponds to i_timeval
 */
inline at::TimeStamp ToTimeStamp( const timeval & i_timeval )
{
    return PosixEpoch() + ToTimeInterval( i_timeval );
}


// ======== ToTimeStamp ===============================================
/**
 * Create a TimeStamp from a timespec
 *
 * @return The TimeStamp that corresponds to i_timespec
 */
inline at::TimeStamp ToTimeStamp( const timespec & i_timespec )
{
    return PosixEpoch() + ToTimeInterval( i_timespec );
}


}; // namespace

#endif // x_at_posix_time_h_x



