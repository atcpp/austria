//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_posix_atomic_cpp.i
 *
 */


//
// This is the posix version of the threading interface
//

//
// System dependant at_thread.h has been included here.
// Nothing more to include except for platform headers
//

#include "at_os.h"
#include "at_assert.h"

#include "at_thread.h"

#include "at_posix_time.h"

#include <pthread.h>
#include <errno.h>
#include <iostream>
#include <sstream>

#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>


//
// Tracing support
//
static const bool g_tracing = false;

// at_trace.h must be included after defining g_tracing
#include "at_trace.h"

#define F(A) & A, # A, __LINE__




// ======== PrivateConditionalContext =================================
/*
 * PrivateConditionalContext contains information pertaining to the
 * Conditional implementation.
 */

struct PrivateConditionalContext
{
    public:

    pthread_mutex_t               * m_mutex;
    pthread_cond_t                  m_cond[ 1 ];

};



//
// make sure the sizes of opaque objects are the appropriate size.
// corrent the sizes.
AT_StaticAssert(
    sizeof( at::Mutex::MutexContext ) >= sizeof( pthread_mutex_t ),
    Mutex__MutexContext_size_must_be_greater_or_equal_to_size_of_pthread_mutex_t
);

AT_StaticAssert(
    sizeof( at::Conditional::ConditionalContext ) >= sizeof( PrivateConditionalContext ),
    Conditional__ConditionalContext_size_must_be_greater_or_equal_to_size_of_pthread_cond_t
);

namespace at
{

// ======== TaskContext ===============================================
/**
 * TaskContext is the data contained within this context.
 *
 */

struct TaskContext
{

    TaskContext()
      : m_started( false ),
        m_completed( false ),
        m_is_joined( false )
    {
    }
    
    /**
     * m_thread_id contains the ID of this thread
     */
    
    pthread_t               m_thread_id;

    /**
     * m_started indicates wether the thread has been "started"
     */

    volatile bool           m_started;

    /**
     * m_completed indicates wether the thread has finished.
     */
    volatile bool           m_completed;

    /**
     * m_is_joined indicates wether the thread is joined
     */
    volatile bool           m_is_joined;

    /**
     * This is to manage this thread.
     */
    ConditionalMutex        m_thread_cond_mutex;

    /**
     * This is to manage this thread.
     */
    ConditionalMutex        m_wait_cond_mutex;


    static void * start_routine( void * i_task );
    
};

// ======== Start =================================================
void Task::Start()
{
    if ( ! m_task_context->m_started )
    {
        // Wake this thread 
        Lock<ConditionalMutex>  l_lock( m_task_context->m_thread_cond_mutex );

        m_task_context->m_started = true;
        l_lock.Post();
    }
}

// ======== Wait ==================================================
void Task::Wait()
{
    if ( ! m_task_context->m_is_joined )
    {
        // Wait here to be started
        Lock<ConditionalMutex>  l_lock( m_task_context->m_wait_cond_mutex );

        while ( ! m_task_context->m_completed )
        {
            l_lock.Wait();
        }

        // Need to call join here ...

        if ( ! m_task_context->m_is_joined )
        {
            m_task_context->m_is_joined = true;

            void * l_return_value;

            int l_result = Trace(
                F(pthread_join),
                m_task_context->m_thread_id,
                & l_return_value
            );

            if ( 0 != l_result )
            {
                AT_Abort();
            }
        }
        
    } // l_lock is unlocked here

}

// ======== Wait ==================================================
bool Task::Wait( const TimeInterval & i_time )
{
    if ( ! m_task_context->m_is_joined )
    {
        // Wait here to be started
        Lock<ConditionalMutex>  l_lock( m_task_context->m_wait_cond_mutex );

		TimeStamp l_tst_start = ThreadSystemTime (); 
		TimeStamp l_tst = l_tst_start;
		while ( ! m_task_context->m_completed && ( l_tst-l_tst_start < i_time  ) )
		{
			l_lock.Wait( i_time - (l_tst-l_tst_start) );
			l_tst = ThreadSystemTime (); 
		}

        // Need to call join here ...

        if ( m_task_context->m_completed && ! m_task_context->m_is_joined )
        {
            m_task_context->m_is_joined = true;

            void * l_return_value;

            int l_result = Trace(
                F(pthread_join),
                m_task_context->m_thread_id,
                & l_return_value
            );

            if ( 0 != l_result )
            {
                AT_Abort();
            }
        }
        
        return m_task_context->m_completed;
        
    } // l_lock is unlocked here

	return true;
}


// ======== GetThisId =============================================

Task::TaskID Task::GetThisId()
{
    return m_task_context->m_thread_id;
}


// ======== GetSelfId =============================================

Task::TaskID Task::GetSelfId()
{
    return ::pthread_self();
}


// ======== start_routine =============================================

void * TaskContext::start_routine( void * i_task )
{
    Task    * l_this_task = static_cast<Task *>( i_task );

    {
        // Wait here to be started
        Lock<ConditionalMutex>  l_lock( l_this_task->m_task_context->m_thread_cond_mutex );

        while ( ! l_this_task->m_task_context->m_started )
        {
            l_lock.Wait();
        }
    }

    // do the work ...
    l_this_task->Work();

    {
        // Wake all the waiters.
        Lock<ConditionalMutex>  l_lock( l_this_task->m_task_context->m_wait_cond_mutex );

        l_this_task->m_task_context->m_completed = true;
        l_lock.PostAll();
    }
    
    return 0;
}


// ======== Task ======================================================

Task::Task()
  : m_task_context( new TaskContext() )
{

    int l_result = Trace(
        F( pthread_create ),
        & m_task_context->m_thread_id,
        static_cast<const pthread_attr_t *>( 0 ),
        & TaskContext::start_routine,
        static_cast<void *>( this )
    );

    if ( 0 != l_result )
    {
        AT_Abort();
    }
}

// ======== ~Task =================================================
Task::~Task()
{
    // must not try to delete thread that is still executing
    Wait();
    
    delete m_task_context;
}


#define ReferenceMutexContext(i_name)                                   \
    pthread_mutex_t * i_name = reinterpret_cast<pthread_mutex_t *>( & m_mutex_context )


// ======== Mutex =====================================================


// ======== MutexAttr =================================================
/*
 * This is to create static initialized mutex attr's
 *
 */

class MutexAttr
{
    public:

    MutexAttr( int i_kind )
    {
        pthread_mutexattr_init( m_attr );
        if ( pthread_mutexattr_settype( m_attr, i_kind ) != 0 )
        {
            AT_Abort();
        }
    }

    ~MutexAttr()
    {
        pthread_mutexattr_destroy( m_attr );
    }
    
    pthread_mutexattr_t * GetAttr()
    {
        return m_attr;
    }
    
    pthread_mutexattr_t                 m_attr[ 1 ];

};

// AT_MUTEX_ATTR_* is defined in at_*_thread.h because some of these
// attributes are non-POSIX (or we want to use a non-POSIX variant).
MutexAttr       g_MA_Fast( AT_MUTEX_ATTR_FAST );
MutexAttr       g_MA_Recursive( AT_MUTEX_ATTR_RECURSIVE );
MutexAttr       g_MA_Check( AT_MUTEX_ATTR_ERRORCHECK );

// ======== Mutex =================================================
Mutex::Mutex( MutexType i_type )
{
    ReferenceMutexContext( l_mutex );

    switch ( i_type )
    {
        case NonRecursive :
        {
            int l_result = Trace( F(pthread_mutex_init), l_mutex, g_MA_Fast.GetAttr() );

            if ( l_result != 0 )
            {
                AT_Abort();
            }
            
            break;
        }
        case Recursive :
        {
            int l_result = Trace( F(pthread_mutex_init), l_mutex, g_MA_Recursive.GetAttr() );
            
            if ( l_result != 0 )
            {
                AT_Abort();
            }
            
            break;
        }
        case Checking :
        {            
            int l_result = Trace( F(pthread_mutex_init), l_mutex, g_MA_Check.GetAttr() );
            
            if ( l_result != 0 )
            {
                AT_Abort();
            }
            
            break;
        }
    }
}


// ======== Mutex =================================================

Mutex::~Mutex()
{
    ReferenceMutexContext( l_mutex );

    int l_result = Trace( F(pthread_mutex_destroy), l_mutex );
    
    if ( l_result != 0 )
    {
        // trying to destroy a mutex that is locked
        AT_Abort();
    }
}

// ======== Lock ==================================================

void Mutex::Lock()
{
    ReferenceMutexContext( l_mutex );
    
    int l_result = Trace( F(pthread_mutex_lock), l_mutex );

    if ( l_result != 0 )
    {
        if ( l_result == EINVAL )
        {
            AT_Abort();
        }
        
        if ( l_result == EDEADLK )
        {
            AT_Abort();
        }

        AT_Abort();
    }
}


// ======== TryLock ===============================================

bool Mutex::TryLock()
{
    ReferenceMutexContext( l_mutex );
    
    int l_result = Trace( F(pthread_mutex_trylock), l_mutex );

    if ( EBUSY == l_result )
    {
        return false;
    }
    
    if ( l_result != 0 )
    {
        if ( l_result == EINVAL )
        {
            AT_Abort();
        }
        
        AT_Abort();
    }

    return true;
}


// ======== Unlock ================================================

void Mutex::Unlock()
{
    ReferenceMutexContext( l_mutex );
    
    int l_result = Trace( F(pthread_mutex_unlock), l_mutex );

    if ( l_result != 0 )
    {
        if ( l_result == EINVAL )
        {
            AT_Abort();
        }
        
        if ( l_result == EPERM )
        {
            AT_Abort();
        }

        AT_Abort();
    }
}


// ======== Conditional ===============================================

#define ReferenceConditionalContext(i_name)                             \
    PrivateConditionalContext * i_name =                                \
        reinterpret_cast<PrivateConditionalContext *>( & m_conditional_context )


// ======== Conditional ===========================================

Conditional::Conditional( Mutex & i_mutex )
{
    pthread_mutex_t * l_mutex = reinterpret_cast<pthread_mutex_t *>( & i_mutex.m_mutex_context );
    ReferenceConditionalContext( l_conditional );

    l_conditional->m_mutex = l_mutex;

    int l_result = Trace(
        F(pthread_cond_init),
        l_conditional->m_cond,
        static_cast<const pthread_condattr_t *>( 0 )
    );
    
    if ( l_result != 0 )
    {
        AT_Abort();
    }

}

// ======== ~Conditional ==========================================

Conditional::~Conditional()
{
    ReferenceConditionalContext( l_conditional );
    
    int l_result = Trace(
        F(pthread_cond_destroy),
        l_conditional->m_cond
    );
    
    if ( l_result != 0 )
    {
        AT_Abort();
    }
}


// ======== Post ==================================================
void Conditional::Post()
{
    ReferenceConditionalContext( l_conditional );

    int l_result = Trace( F(pthread_cond_signal), l_conditional->m_cond );
    
    if ( l_result != 0 )
    {
        AT_Abort();
    }
}


// ======== PostAll ===============================================
void Conditional::PostAll()
{
    ReferenceConditionalContext( l_conditional );
    
    int l_result = Trace( F(pthread_cond_broadcast), l_conditional->m_cond );
    
    if ( l_result != 0 )
    {
        AT_Abort();
    }
}

// ======== Wait ==================================================
void Conditional::Wait()
{
    ReferenceConditionalContext( l_conditional );

    int l_result = Trace( F(pthread_cond_wait), l_conditional->m_cond, l_conditional->m_mutex );
    
    if ( l_result != 0 )
    {
        AT_Abort();
    }
}

// ======== Wait ==================================================
bool Conditional::Wait( const TimeInterval & i_time )
{
    return Wait( ThreadSystemTime() + i_time );
}


// ======== Wait ==================================================

bool Conditional::Wait( const TimeStamp & i_time )
{
    ReferenceConditionalContext( l_conditional );

    timespec tspec = at_posix::PosixTimespec( i_time );

    int l_result = Trace( F(pthread_cond_timedwait),
    l_conditional->m_cond, l_conditional->m_mutex, &tspec );

    if ( l_result == ETIMEDOUT) return false;
    else if ( l_result != 0 ) AT_Abort();
    else return true;
}


// ======== ConditionalMutex ======================================
ConditionalMutex::ConditionalMutex( MutexType i_type )
  : Mutex( i_type ),
    Conditional( * static_cast< Mutex * >( this ) )
{
}

ConditionalMutexRefCount::ConditionalMutexRefCount( MutexType i_type)
  : MutexRefCount( i_type ),
    Conditional( * static_cast< Mutex * >( this ) )
{
}


// ======== Sleep =================================================
bool Task::Sleep ( const TimeInterval & i_time )
{
    timespec l_ts = at_posix::PosixTimespec( i_time );
    
    if ( -1 == nanosleep( & l_ts, 0 ) )
    {
  if ( errno == EINTR )
   return true; // Signal interrupted us
  else
   return false; // invalid time  
    }
    
    return false;
}

// ======== Sleep =================================================
bool Task::Sleep ( const TimeStamp & i_time )
{
    return Sleep( i_time - ThreadSystemTime() );
}


// ======== ThreadSystemTime ==========================================
TimeStamp ThreadSystemTime()
{
    timeval l_tv;
    int l_error = gettimeofday( &l_tv, 0 );
    if ( l_error )
    {
        AT_Abort();
    };
    return at_posix::ToTimeStamp( l_tv );
}



// ======== SystemCommandContext ======================================
/**
 * This contains the system specific system command context.
 *
 */

class SystemCommandContext
{
    public:

    SystemCommandContext(
        const char                  * i_command_name,
        const char                  * const i_command_argv[],
        const SystemCommandAttr     & i_attr = SystemCommandAttr()
    )
      : m_pid(),
        m_errno(),
        m_status(),
        m_state( SystemCommand::Running )
    {
        
        m_pid = fork();

        if ( ! m_pid )
        {
            // this is the child process !!!

            execv( i_command_name, (char* const*)i_command_argv );

            exit( errno );
        }
        else if ( -1 == m_pid )
        {
            // fork failed
            m_state = SystemCommand::Failed;
            m_errno = errno;
        }
    }
    
    ~SystemCommandContext()
    {
        // TODO BAD - in theory we need to wait here but we
        // can't because it can cause a deadlock
    }

    // ======== InitStatus ============================================

    bool InitStatus( std::string * o_error )
    {
        if ( -1 == m_pid )
        {
            if ( o_error )
            {
                * o_error = strerror( m_errno );
            }
            return false;
        }
        return true;
    }

    // ======== ExitStatus ============================================

    SystemCommand::ProcessState ExitStatus(
        int             & o_info,
        bool              i_wait = false
    ) {
        
        switch ( m_state )
        {
            case SystemCommand::Failed :
            {
                return SystemCommand::Failed;
            }

            case SystemCommand::Stopped :
            case SystemCommand::Running :
            {
                int l_wait_status;

                pid_t   l_pid;
                do {
                    l_pid = ::waitpid( m_pid, & l_wait_status, i_wait ? 0 : WNOHANG );
                } while ( ( i_wait ) && ( 0 == l_pid ) && ( EINTR == errno ) );
        
                if ( ! l_pid )
                {
                    return SystemCommand::Running;
                }
        
                if ( -1 == l_pid )
                {
                    // very strange - set to Interrupted
                    o_info = m_status = -1;
                    return m_state = SystemCommand::Interrupted;
                }
    
                AT_Assert( l_pid == m_pid );

                if ( WIFSTOPPED( l_wait_status ) )
                {
                    o_info = WSTOPSIG( l_wait_status );
                    return m_state = SystemCommand::Stopped;
                }
                
                if ( WIFEXITED( l_wait_status ) )
                {
                    o_info = m_status = WEXITSTATUS( l_wait_status );
                    return m_state = SystemCommand::Exited;
                }

                // the next if statement should be true
                AT_Assert( WIFSIGNALED( l_wait_status ) );
                
                if ( WIFSIGNALED( l_wait_status ) )
                {
                    o_info = m_status = WTERMSIG( l_wait_status );
                    return m_state = SystemCommand::Exited;
                }

                // should never get here - failsafe only
                o_info = m_status = -1;
                return m_state = SystemCommand::Interrupted;

            }
        }

        o_info = m_status;
        return m_state;
    }

    pid_t                                   m_pid;
    int                                     m_errno;
    int                                     m_status;
    SystemCommand::ProcessState             m_state;

};

SystemCommand::SystemCommand(
    const char                  * i_command_name,
    const char                  * const i_command_argv[],
    const SystemCommandAttr     & i_attr
)
  : m_command_context(
        new SystemCommandContext( i_command_name, i_command_argv, i_attr )
    )
{}

SystemCommand::~SystemCommand()
{
    delete m_command_context;
}

// ======== InitStatus ============================================

bool SystemCommand::InitStatus( std::string * o_error )
{
    return m_command_context->InitStatus( o_error );
}


// ======== ExitStatus ============================================

SystemCommand::ProcessState SystemCommand::ExitStatus(
    int             & o_info,
    bool              i_wait
) {
    return m_command_context->ExitStatus( o_info, i_wait );
}


}; // namespace

