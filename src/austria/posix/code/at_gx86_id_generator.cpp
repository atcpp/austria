

#include "at_id_generator.h"
#include "at_base64.h"

#include "at_types.h"
#include "at_assert.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdlib>
#include <vector>

    
namespace
{


struct UUID_Basic
{
    at::Uint64  m_uuid[2];
};

inline at::Uint64 rdtsc()
{
    at::Uint64 l_ticks;
    __asm__ __volatile__("rdtsc" : "=A" (l_ticks));
    return l_ticks;
} 

const char s_filename[] = "/dev/urandom";

struct RandomFile
{
    int         m_fd;

    RandomFile()
      : m_fd( ::open( s_filename, O_RDONLY ) )
    {
        AT_Assert( m_fd >= 0 );
    }

    ~RandomFile()
    {
        if ( m_fd >= 0 )
        {
            ::close( m_fd );
        }
    }

    int Read( void * o_buf, int i_len )
    {
        return ::read( m_fd, o_buf, i_len );
    }

    static RandomFile & GetStatic()
    {
        static RandomFile   s_rf;

        return s_rf;
    }
        
    private:

    RandomFile( const RandomFile & );
    RandomFile & operator=( const RandomFile & );
};


// ======== UuidGenerate ==============================================
/**
 * UuidGenerate will create a new UUID
 *
 *
 * @param o_uuid The location of the uuid
 * @return nothing
 */

void UuidGenerate( UUID_Basic & o_uuid )
{
    RandomFile & s_rf = RandomFile::GetStatic();

    s_rf.Read( o_uuid.m_uuid, sizeof( o_uuid.m_uuid ) );

    o_uuid.m_uuid[0] += rdtsc();

    void * l_memaddr = std::malloc( unsigned( o_uuid.m_uuid[0] & 0x3ff ) );
    if ( l_memaddr )
    {
        std::free( l_memaddr );

        o_uuid.m_uuid[1] ^= ( at::Uint64 ) l_memaddr;
    }

    o_uuid.m_uuid[1] += ::getpid();
}


} // anon namespace

// Austria namespace
namespace at
{

// ======== NewIdent =============================================

std::string NewIdent()
{
    UUID_Basic      l_uuid;

    UuidGenerate( l_uuid );

    BaseNEncoder<6, BaseNEndianLittle>          l_encoder( g_urlsafe_base64_key );

    std::string     l_result;

    std::back_insert_iterator< std::string >    l_inserter( l_result );

    l_encoder.Encode(
        ( char * ) l_uuid.m_uuid,
        ( sizeof l_uuid ) + ( char * ) l_uuid.m_uuid,
        l_inserter
    );

    l_encoder.Flush( l_inserter );
    
    return l_result;
}

// ======== BinaryRandomSequence =============================================
/**
 * BinaryRandomSequence will return a string with a number of binary 
 *
 * @param i_length The length of the binary random sequence to generate
 * @return A string representing the identifier.
 */

AUSTRIA_EXPORT std::string BinaryRandomSequence( unsigned i_length )
{
    std::vector<char>   l_str( i_length );

    RandomFile & s_rf = RandomFile::GetStatic();

    s_rf.Read( & l_str[0], i_length );

    return std::string( l_str.begin(), l_str.end() );
}



} // namespace

