/*
 *
 *	This file is protected by trade secret and copyright (c) 2005 NetCableTV.
 *	Any unauthorized use of this file is prohibited and will be prosecuted
 *	to the full extent of the law.
 *
 */

//
// The Austria library is copyright (c) Gianni Mariani 2005.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
//  http://www.gnu.org/copyleft/lesser.txt
// 
/**
 *  at_gx86_file.cpp
 *
 *  This contains the gx86 implementation of at_file.h.
 *
 */

#include "at_exports.h"
#include "at_factory.h"
#include "at_source_locator.h"
#include "at_types.h"
#include "at_lifetime.h"
#include "at_file.h"
#include "at_thread.h"
#include "at_start_up.h"
#include "at_posix_time.h"
#include "errno.h"

#include AT_FILE_H

#include <aio.h>
#include <unistd.h>
#include <fcntl.h>


#include <map>
#include <string>

// Austria namespace
namespace at
{

// ======== GetHostName ===========================================
/**
 * GetHostName returns the machine name of the local host.
 *
 * @return a std::string containing the host name.
 */
    
std::string GetHostName() 
{   
    char l_buf[ 256 ];
    * l_buf = 0;
    gethostname( l_buf, sizeof( l_buf ) );
 
    return l_buf;
}


    // Register the fileinitializer
    AT_MakeFactory2P( "ZFileInitializer", FileInitializer, Initializer, DKy, int & , const char ** & );
    
    //////////////////////////////////////////////////////////////
    //
    //  FileError for gx86 is implemented as FileErrorBasic
    //
    //////////////////////////////////////////////////////////////

    // ======== std::map<long,FileErrorBasic*> g_errormap ===================================================
    /**
     *      g_errormap contains the map of OS errorcodes to File Errors
     */
    static std::map<long,FileErrorBasic*> s_errormap;

    // Initialize all the Custom Errors with their error messages
    static FileErrorBasic s_success_basic               ( "Success", 0 ); 
    static FileErrorBasic s_end_of_file_basic           ( "End of File (EOF)" );
    static FileErrorBasic s_waiting_read_basic          ( "Waiting for an ASYNC Read to Finish" );
    static FileErrorBasic s_waiting_write_basic         ( "Waiting for an ASync Write to Finish" );
    static FileErrorBasic s_waiting_open_basic          ( "Waiting for an ASync Open to Complete" );
    static FileErrorBasic s_waiting_close_basic         ( "Waiting for an ASync Close to Complete" );
    static FileErrorBasic s_file_not_found_basic        ( "File not Found", ENOENT );
    static FileErrorBasic s_bad_path_basic              ( "Bad Path" );
    static FileErrorBasic s_too_many_open_files_basic   ( "Too many open files", EMFILE );
    static FileErrorBasic s_access_denied_basic         ( "Access Denied", EACCES );
    static FileErrorBasic s_invalid_file_basic          ( "Invalid File", EBADF );
    static FileErrorBasic s_remove_current_dir_basic    ( "Remove Current Directory Failed", ENOTDIR );
    static FileErrorBasic s_directory_full_basic        ( "Directory is Full" );
    static FileErrorBasic s_bad_seek_basic              ( "Negative Seek", ESPIPE );
    static FileErrorBasic s_hard_io_basic               ( "Hard Disk Error", EIO );
    static FileErrorBasic s_sharing_violation_basic     ( "Sharing Violation" );
    static FileErrorBasic s_lock_violation_basic        ( "Lock Violation" );
    static FileErrorBasic s_disk_full_basic             ( "Disk Full", ENOSPC );   
    static FileErrorBasic s_bad_param_basic             ( "Invalid Parameter", EINVAL );
    static FileErrorBasic s_overflow_basic              ( "Buffer Overflow", EMSGSIZE );
    static FileErrorBasic s_too_many_async_calls_basic  ( "Too many Async Calls", EAGAIN );
    static FileErrorBasic s_user_aborted_basic          ( "Canceled by User", ECANCELED );
    static FileErrorBasic s_already_exists_basic        ( "File already exists", EEXIST );

    // Assign the Static FileErrorBasic members
    const FileError &FileError::s_success               = s_success_basic;
    const FileError &FileError::s_end_of_file           = s_end_of_file_basic;
    const FileError &FileError::s_waiting_read          = s_waiting_read_basic;
    const FileError &FileError::s_waiting_write         = s_waiting_write_basic;
    const FileError &FileError::s_waiting_open          = s_waiting_open_basic;
    const FileError &FileError::s_waiting_close         = s_waiting_close_basic;
    const FileError &FileError::s_file_not_found        = s_file_not_found_basic;
    const FileError &FileError::s_bad_path              = s_bad_path_basic;
    const FileError &FileError::s_too_many_open_files   = s_too_many_open_files_basic;
    const FileError &FileError::s_access_denied         = s_access_denied_basic;
    const FileError &FileError::s_invalid_file          = s_invalid_file_basic;
    const FileError &FileError::s_remove_current_dir    = s_remove_current_dir_basic;
    const FileError &FileError::s_directory_full        = s_directory_full_basic;
    const FileError &FileError::s_bad_seek              = s_bad_seek_basic;
    const FileError &FileError::s_hard_io               = s_hard_io_basic;
    const FileError &FileError::s_sharing_violation     = s_sharing_violation_basic;
    const FileError &FileError::s_lock_violation        = s_lock_violation_basic;
    const FileError &FileError::s_disk_full           	= s_disk_full_basic;
    const FileError &FileError::s_bad_param             = s_bad_param_basic;
    const FileError &FileError::s_overflow              = s_overflow_basic;
    const FileError &FileError::s_too_many_async_calls  = s_too_many_async_calls_basic;
    const FileError &FileError::s_user_aborted          = s_user_aborted_basic;
    const FileError &FileError::s_already_exists        = s_already_exists_basic;
	const FileError &FileError::s_file_exists           = s_already_exists_basic;

    // ======== FileErrorBasic FileErrorBasic( const char* i_name, long i_errorno ) ====================
    FileErrorBasic::FileErrorBasic ( const char* i_name, long i_errorno ) : m_errortext ( i_name ), m_errorno ( i_errorno )
    {
        if ( i_errorno != -1 )
        {
            s_errormap[i_errorno] = this;
        }
    }

    // ======== FileErrorBasic FileErrorBasic( long i_errorno ) ========================================
    FileErrorBasic::FileErrorBasic ( long i_errorno )
    {
        FileErrorBasic* fe = s_errormap[i_errorno];
        if ( fe )
        {
            *this = fe;
        }
        else
        {
            AT_Assert ( false ); // This OS error (i_errorno) needs to be added to the global list above. See Bill. For Gx86 Lookup Errno Description
        } 
    }

    FileErrorBasic::FileErrorBasic ( const FileError& i_fileerror )
    {
        m_errortext = i_fileerror.What ();
        m_errorno = i_fileerror.ErrorCode ();
    }

    // ======== FileErrorBasic & FileErrorBasic::operator = ( const FileErrorBasic* i_fileerror )================

    const FileErrorBasic & FileErrorBasic::operator = ( const FileErrorBasic* i_fileerror )
    { 
        m_errortext = i_fileerror->What ();
        m_errorno = i_fileerror->ErrorCode ();

        return *this; 
    }

    // ======== FileAttr_List_x( AT_StaticVariableDefn ) ========================================
    /**
     *      Define the static variables needed for attribute handling
     */
    FileAttr_List_x( AT_StaticVariableDefn )

    /////////////////////////////////////////////////////////////////////
    //
    // GX86 Implementation of BaseFile 
    //
    /////////////////////////////////////////////////////////////////////

    const int BaseFile::SeekBegin = SEEK_SET; 
    const int BaseFile::SeekCurrent = SEEK_CUR;
    const int BaseFile::SeekEnd = SEEK_END; 

    // ======== BaseFile::Exists ========================================
    bool BaseFile::Exists ( const FilePath & i_file_path )
    {
	    return access ( i_file_path.String (), F_OK ) == 0;
    }

    // ======== BaseFile::Rename ========================================
    bool BaseFile::Rename ( const FilePath & i_file_pathold, const FilePath & i_file_pathnew )
    {
        return -1 != std::rename ( i_file_pathold.String (), i_file_pathnew.String () );
    }

    // ======== BaseFile::HardLink ======================================
    bool BaseFile::HardLink ( const FilePath & i_file_pathold, const FilePath & i_file_pathnew )
    {
        return -1 != link( i_file_pathold.String (), i_file_pathnew.String () );
    }

    // ======== SymLink =============================================

    bool BaseFile::SymLink ( const FilePath & i_file_path, const FilePath & i_link_path )
    {
        return -1 != symlink( i_file_path.String(), i_link_path.String() );
    }

    // ======== ReadSymLink =============================================

    bool BaseFile::ReadSymLink ( const FilePath & i_link_path, std::string & o_symlink_val )
    {
        char        l_path[ PATH_MAX ];

        int l_retval = readlink( i_link_path.String(), l_path, sizeof( l_path ) );

        if ( -1 == l_retval )
        {
            return false;
        }

        o_symlink_val.assign( l_path, l_retval );

        return true;
    }
    
    // ======== RemoveSymLink =============================================
    
    bool BaseFile::RemoveSymLink ( const FilePath & i_link_path )
    {
        struct stat l_stat;

        if ( -1 == lstat( i_link_path.String(), & l_stat ) )
        {
            return false;
        }

        if ( S_IFLNK & l_stat.st_mode )
        {
            return Remove( i_link_path );
        }

        errno = ENOTDIR;
        return false;
        
    }
    
    // ======== BaseFile::Remove ========================================
    bool BaseFile::Remove ( const FilePath & i_file_path )
    {
        return -1 != unlink ( i_file_path.String () );	
    }

    // ======== BaseFile::Open ( i_file_path, i_attr ) =======================
    bool BaseFile::Open ( const FilePath & i_file_path, const FileAttr &i_attr )
    {
        // Assert if already open 
        AT_Assert ( m_filehandle.fildes == -1 );

        // Adjust the Mode to match this class type
        FileAttr attr = i_attr;
        if ( false == CheckAttributes ( &attr ) )
        {
            return false;
        }

        int openmode = 0;

        // Read Access
        if ( attr & FileAttr::Read ) 
        {
            openmode = O_RDONLY;
        }

        // Write and preserve
        if ( attr & FileAttr::Write  ) 
        {
            openmode = O_WRONLY;
        }

        // Read/Write 
        if ( attr & FileAttr::ReadWrite ) 
        {
            openmode = O_RDWR;
        }

		// NOTE: the mode flag is only used when the file is openned for O_CREAT
		//       in fact for all other cases it is ignored.
		//       in fact the flags used in FileAttr really don't speak to mode which
		//       sets the file's access permissions. better to fix the mode to 0666
		//       and let the umask(2) customize the access.
        int mode = 0;

        // Create 
        if ( attr & FileAttr::Create ) 
        {
            openmode |= O_CREAT;
			mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;	// 0666 for you unix geeks out there...
        }
        if ( attr & FileAttr::New ) 
        {
            openmode |= O_CREAT | O_EXCL;
			mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;	// 0666 for you unix geeks out there...
        }

#if 0
        // Preserve only if Write
        if ( attr & FileAttr::Preserve ) 
        {
            if ( attr & FileAttr::Write )
            {
                openmode |= O_APPEND;
            }
        }
#else
		// The code above fails because O_APPEND is not the same as preserve
		// we must employ some negative logic to determine when to truncate
		// a file open for write
		if ( !((attr & FileAttr::Preserve) || (attr & FileAttr::New)) && ( attr& FileAttr::Write ) )
		{
			openmode |= O_TRUNC;
		}
#endif

        if ( attr & FileAttr::Mapped )
        {
            AT_Assert ( false );
        }

        if ( attr & FileAttr::DirectIO )
        {
            openmode |= O_DIRECT;
        }

        if ( attr & FileAttr::Async )
        {
        }

        if ( attr & FileAttr::SyncWrite )
        {
            openmode |= O_SYNC;
        }

        int filedes = open ( i_file_path.String (), openmode, mode );

        if ( filedes != -1 )
        {
            m_filehandle.fildes = filedes;
            m_fullname = i_file_path;
            m_mode = i_attr;
            m_closeondelete = true;

            return true;
        }
        else
        {
#ifdef FILESTRICT
            // Throw an Exception, the file should be able to open
            throw FileErrorBasic ( errno );
#else
            // Record the Error
            SetError ( errno );
#endif
        }

        return false;
    }

    // ======== BaseFile::Open ===============================================
    bool BaseFile::Open ()
    {
        return Open ( m_fullname, m_mode ); 
    }

    // ======== Operations: BaseFile::IsOpen =================================
    bool BaseFile::IsOpen ()
    {
        return m_filehandle.fildes != -1;
    }

    // ======== BaseFile::AbortAllBlockingCalls ==================================
    bool BaseFile::AbortAllBlockingCalls ()
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        return aio_cancel ( m_filehandle.fildes, 0 );
    }

    // ======== BaseFile::Flush () ==========================================
    bool BaseFile::Flush ()
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        if ( -1 == fsync ( m_filehandle.fildes ) )
        {
            SetError ( errno );

            return false;
        }
    
        return true;
    }

    // ======== BaseFile::Close  ()===========================================
    void BaseFile::Close () 
    {
        int error = 0;
        // If we have a valid File handle then Close
        if ( m_filehandle.fildes != -1 )
        {
            error = close ( m_filehandle.fildes );
        }

        m_filehandle.fildes = -1;
        m_closeondelete = false;

        // There was an error trying to Close the File
        if ( error )
        {
            throw FileErrorBasic ( errno );
        }

    }

    // ======== BaseFile::GetError () ========================================
    const FileError& BaseFile::GetError () const 
    {
        AT_Assert ( m_lasterror != 0 );

        return *m_lasterror;
    }
    
    // ======== BaseFile::SetError ( long i_error ) ==========================
    void BaseFile::SetError ( long i_error )
    {
        if ( m_lasterror )
        {
            delete m_lasterror; 
        }

        m_lasterror = new FileErrorBasic ( i_error );
    }

    // ======== BaseFile::SetError( const FileError& i_error ) =======================
    void BaseFile::SetError ( const FileError& i_error )
    {
        SetError ( i_error.ErrorCode () );
    }

    // ======== BaseFile::Seek( long i_off, int i_from ) ========================
    Int64 BaseFile::Seek ( Int64 i_off, int i_from ) const 
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        AT_Assert ( i_from == SEEK_SET || i_from == SEEK_END || i_from == SEEK_CUR );

        const Int64 seekerror = -1;
        long whence = (long)i_from;

        Int64 res = lseek64 ( m_filehandle.fildes, i_off, whence ); 
        if ( seekerror == res  )
        {
            throw FileErrorBasic ( errno );
        }

        return res;
    }

    // ======== BaseFile::GetPosition () =====================================
    Int64 BaseFile::GetPosition () const 
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const Int64 seekerror = -1;
        Int64 pos = lseek64 ( m_filehandle.fildes, 0, SEEK_CUR );

        if ( seekerror == pos )
        {
            throw FileErrorBasic ( errno );
        }
        
        return pos;
    }

    // ======== BaseFile::SetLength ( long i_newlen ) ========================
    bool BaseFile::SetLength ( Int64 i_newlen )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const Int64 seekerror = -1;
        
        if ( seekerror == ftruncate64 ( m_filehandle.fildes, i_newlen ) )
        {
            SetError ( errno );
            return false;
        }

        return true;
    }
    
    // ======== BaseFile::GetLength () =======================================
    Int64 BaseFile::GetLength () const 
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const Int64 seekerror = -1;
        struct stat64 info;

        if ( seekerror == fstat64 ( m_filehandle.fildes, &info ) )
        {
            throw FileErrorBasic ( errno );
        }
            
        return info.st_size;
    }

    // ======== BaseFile::GetLength () =======================================
    Int64 BaseFile::GetLength( const FilePath & i_file_path )
    {
        struct stat l_stat;

        if ( stat( i_file_path.String(), &l_stat ) != 0 )
        {
            throw FileErrorBasic ( errno );
        }

        return l_stat.st_size;
    }

    // ======== BaseFile::LockRange( Int64 i_pos, Int64 i_count ) ==============
    bool BaseFile::LockRange( Int64 i_pos, Int64 i_count, bool i_wait )
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        
        struct flock64        a_flock[1];
        int                   l_retval;
     
        a_flock->l_type= F_WRLCK;
        a_flock->l_whence = SEEK_SET;
        a_flock->l_start= i_pos;
        a_flock->l_len= i_count;

        l_retval = fcntl( m_filehandle.fildes, (i_wait ? F_SETLKW : F_SETLK), a_flock);

        if ( -1 == l_retval )
        {
            if ( ( EACCES == errno ) || ( EAGAIN == errno ) )
            {
                return false;
            }

            throw FileErrorBasic ( errno );
        }

        return true;

    }

    // ======== BaseFile::UnlockRange( Int64 i_pos, Int64 i_count ) ============
    bool BaseFile::UnlockRange( Int64 i_pos, Int64 i_count )
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        
        struct flock64        a_flock[1];
        int                   l_retval;
     
        a_flock->l_type= F_UNLCK;
        a_flock->l_whence = SEEK_SET;
        a_flock->l_start= i_pos;
        a_flock->l_len= i_count;

        l_retval = fcntl( m_filehandle.fildes, F_SETLKW, a_flock);

        if ( -1 == l_retval )
        {
            if ( ( EACCES == errno ) || ( EAGAIN == errno ) )
            {
                return false;
            }

            throw FileErrorBasic ( errno );
        }

        return true;

    }

/** 
 *  S_IRUSR   00400 read by owner
 *
 *  S_IWUSR   00200 write by owner
 *
 *  S_IXUSR   00100 execute/search by owner
 *
 *  S_IRGRP   00040 read by group
 *
 *  S_IWGRP   00020 write by group
 *
 *  S_IXGRP   00010 execute/search by group
 *
 *  S_IROTH   00004 read by others
 *
 *  S_IWOTH   00002 write by others
 *
 *  S_IXOTH   00001 execute/search by others
**/


    // ======== BaseFile::SetPermissions ( int i_permissions ) ===============
    void BaseFile::SetPermissions ( int i_permissions )
    {
        long attributes = 0;

        if ( i_permissions & FilePermission::UserWrite  ) 
        {
            attributes |= S_IWUSR; 
        }

        if ( i_permissions & FilePermission::GroupWrite ) 
        {
            attributes |= S_IWGRP; 
        }

        if ( i_permissions & FilePermission::WorldWrite ) 
        {
            attributes |= S_IWOTH; 
        }

	    if ( i_permissions & FilePermission::UserRead  ) 
        {
            attributes |= S_IRUSR; 
        }
	    if ( i_permissions & FilePermission::GroupRead ) 
        {
            attributes |= S_IRGRP; 
        }

	    if ( i_permissions & FilePermission::WorldRead ) 
        {
            attributes |= S_IROTH; 
        }

        if ( i_permissions & FilePermission::UserExecutable  ) 
        {
            attributes |= S_IXUSR; 
        }

        if ( i_permissions & FilePermission::GroupExecutable ) 
        {
            attributes |= S_IXGRP; 
        }

        if ( i_permissions & FilePermission::WorldExecutable ) 
        {
            attributes |= S_IXOTH; 
        }
        
        if ( m_filehandle.fildes != -1 ) // Not 0 or -1 
        {
        	if ( -1 == fchmod ( m_filehandle.fildes, attributes ) )
        	{
            	throw FileErrorBasic ( errno );
        	}
        }
        else
        {
        	if ( -1 == chmod ( m_fullname.String (), attributes ) )
        	{
        		throw ( FileErrorBasic ( errno ) );
        	}
        }
    }

    // ======== BaseFile::GetPermissions( ) ===============
    int BaseFile::GetPermissions ( )
    {
        struct stat * l_stats;
        if ( m_filehandle.fildes != -1) // Not 0 or -1
        {       
                if ( -1 == fstat ( m_filehandle.fildes, l_stats ) )
                {
                        throw FileErrorBasic( errno );
                }
        }
        else
        {
                if ( -1 == stat ( m_fullname.String (), l_stats ) )
                {
                        throw ( FileErrorBasic ( errno ) );
                }
        }

        if (! l_stats)
        {
          throw ( FileErrorBasic ( errno ) );
        }

        int l_attributes = l_stats->st_mode;

        long l_permissions = 0;

        if ( l_attributes & S_IWUSR )
        {
          l_permissions |= FilePermission::UserWrite;
        }

        if ( l_attributes & S_IWGRP )
        {
          l_permissions |= FilePermission::GroupWrite;
        }
        
        if ( l_attributes & S_IWOTH )
        {
          l_permissions |= FilePermission::WorldWrite;
        }

        if ( l_attributes & S_IRUSR )
        {
          l_permissions |= FilePermission::UserRead;
        }

        if ( l_attributes & S_IRGRP )
        {
          l_permissions |= FilePermission::GroupRead;
        }
        
        if ( l_attributes & S_IROTH )
        {
          l_permissions |= FilePermission::WorldRead;
        }

        if ( l_attributes & S_IXUSR )
        {
          l_permissions |= FilePermission::UserExecutable;
        }

        if ( l_attributes & S_IXGRP )
        {
          l_permissions |= FilePermission::GroupExecutable;
        }
        
        if ( l_attributes & S_IXOTH )
        {
          l_permissions |= FilePermission::WorldExecutable;
        }

        return l_permissions;       
    }

    // ======== BaseFile::GetDateTime ( int i_flag, TimeStamp & o_timestamp ) =
    bool BaseFile::GetDateTime ( int i_flag, TimeStamp & o_timestamp ) const
    {
        return false;
    }

    // ======== BaseFile::GetDateTime( const FilePath & i_file_path, int i_flag, TimeStamp& o_timestamp ) =
    bool BaseFile::GetDateTime(
        const FilePath & i_file_path,
        int i_flag,
        TimeStamp & o_timestamp
    )
    {
        bool l_ret = false;
        struct stat l_stat;

        if ( stat( i_file_path.String(), &l_stat ) == 0 )
        {
            time_t * l_t;

            switch( i_flag )
            {
                case LastWrite:
                    l_t = &( l_stat.st_mtime );
                    break;

                case Accessed:
                    l_t = &( l_stat.st_atime );
                    break;

                default:
                    return false;
            }

            o_timestamp = at_posix::PosixEpoch() + TimeInterval::Secs( *l_t );

            l_ret = true;
        }

        return l_ret;
    }

    // ======== BaseFile::~BaseFile () =======================================
    BaseFile::~BaseFile ()
    {
        try
        {
            if ( true == m_closeondelete )
            {
                Close ();
	    	}

            if ( m_lasterror )
            {
				delete m_lasterror;
	    	}	
        }
        catch ( const FileErrorBasic& fe )
        {
            // Not much we can do if there's an error on closing the file.
            // Can't throw an exception, since allowing a destructor to emit
            // exceptions is evil.  No point in recording the error state in
            // the object itself, since it's in the course of being
            // destructed.  And writing a message to stderr or something would
            // be more than a little presumtuous.  So we just drop it on the
            // floor...
        }
    }

    // ======== BaseFile::BaseFile ( const FilePath&, int ) ==================
    BaseFile::BaseFile ( const FilePath & i_file_path, const FileAttr &i_attr )
    {
        m_filehandle.fildes = -1;
        m_fullname      	= i_file_path;
        m_mode          	= i_attr; // Mode is verified In Open
        m_closeondelete 	= false;
        m_lasterror     	= 0;
        m_locked        	= 0;
        // Create a recursive mutex for thread synchronization on lead/aide twins...
        m_mutex = new MutexRefCount( Mutex::Recursive );
    }

    // ======== BaseFile::BaseFile () ========================================
    BaseFile::BaseFile ()
    {
        m_mode          	= FileAttr::Read;
        m_filehandle.fildes = -1;
        m_closeondelete 	= false;
        m_lasterror     	= 0;
        m_locked        	= 0;
        // Create a recursive mutex for thread synchronization on lead/aide twins...
        m_mutex = new MutexRefCount( Mutex::Recursive );
    }

    // ======== BaseFile::CheckAttributes ( FileAttr *i_attr ) ===============
    bool BaseFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Should never be called so always fails
        return false;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Gx86 Implementation of RFile
    //
    /////////////////////////////////////////////////////////////////////

    // ======== RFile::RFile ( const FilePath&, int ) ==============
    RFile::RFile ( const FilePath & i_file_path, const FileAttr& i_attr ) : BaseFile ( i_file_path, i_attr )
    {
    }

    // ======== RFile::RFile () ========================================
    RFile::RFile () : BaseFile ()
    {
    }

    // ======== RFile::Read ( at::PtrDelegate< Buffer* > i_br ) ==========
    bool RFile::Read ( at::PtrView< Buffer * > i_br )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        if ( i_br.Get()->Region().m_allocated == 0 )
        {
            return true;   // 0 buffer yah get nuthin
        }

        long bytesread = read ( m_filehandle.fildes, i_br.Get ()->Region().m_mem, 
				i_br.Get()->Region().m_allocated );

        if ( bytesread == -1 )
        {
            SetError ( errno );
            
            return false;
        }

        i_br.Get()->SetAllocated ( bytesread );

        return true;
    }

    // ======== RFile::Read ( Buffer::t_Region& i_br ) ===================
    bool RFile::Read ( Buffer::t_Region& i_br )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const size_t  l_count = i_br.m_max_available - i_br.m_allocated;
        const ssize_t l_bytesread = read ( m_filehandle.fildes,
                                           i_br.m_mem + i_br.m_allocated,
                                           l_count > SSIZE_MAX ? SSIZE_MAX : l_count );
        if ( l_bytesread == -1 )
        {
            SetError( errno );
            return false;
        }
        i_br.m_allocated += l_bytesread;
        return true;
    }

    // ======== RFile::Open ( const FilePath&, const int )================
    bool RFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr  )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== RFile::Open () ===========================================
    bool RFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== RFile::CheckAttributes ( int i_attr ) ====================
    bool RFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we only have Read attributes 

        FileAttr mode = *i_attr;

        // Write not Allowed
        if ( mode & FileAttr::Write ) 
        {
            return false;
        }

        // ReadWrite not Allowed
        if ( mode & FileAttr::ReadWrite ) 
        {
            return false;  
        }

        // Can't Create a read only file
        if ( mode & FileAttr::Create )
        {
            return false;
        }
        if ( mode & FileAttr::New )
        {
            return false;
        }

        if ( mode & FileAttr::SyncWrite )
        {
            return false;
        }

        // Must Have Read Attribute
        if ( !( mode & FileAttr::Read ) )
        {
            mode.Add ( FileAttr::Read );        
        }

        *i_attr = mode;

        return true;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Gx86 Implementation of WFile
    //
    /////////////////////////////////////////////////////////////////////

    // ======== WFile::WFile ( const FilePath&, int ) ====================
    WFile::WFile ( const FilePath & i_file_path, const FileAttr& i_attr  ) : BaseFile ( i_file_path, i_attr ) 
    {
    }

    // ======== WFile::WFile () ==========================================
    WFile::WFile () : BaseFile ()
    {
    }

    // ======== WFile::Write ( at::PtrDelegate< Buffer * > i_br ) ====================
    long WFile::Write ( at::PtrView< Buffer * > i_br )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        // Zero bytes to write do nothing!
        if ( i_br.Get()->Region().m_allocated == 0 )
        {
            return 0;   
        }

        long byteswritten;

        // If write fails throw an exception
        byteswritten = write ( m_filehandle.fildes, static_cast<void*>(i_br.Get ()->Region().m_mem), 
				  i_br.Get()->Region().m_allocated );

        if ( byteswritten == 0 )
        {
            throw FileErrorBasic ( errno );
        }

        // If the number written does not match then the Disk must be full
        if ( byteswritten != i_br.Get()->Region().m_allocated )
        {
            throw FileErrorBasic ( FileError::s_disk_full );
        }

        return byteswritten;
    }

    // ======== WFile::Write ( const Buffer::t_Region& i_br ) ============
    bool WFile::Write ( const Buffer::t_Region& i_br )
    {
        return Write( Buffer::t_ConstRegion( i_br ) );
    }

    // ======== WFile::Write ( const Buffer::t_Region& i_br ) ============
    bool WFile::Write ( const Buffer::t_ConstRegion& i_br )
    {
        AT_Assert ( m_filehandle.fildes != -1 );

        const char* l_buf = i_br.m_mem;
        size_t l_count = i_br.m_allocated;
        while ( l_count > 0 )
        {
            const ssize_t l_byteswritten = write ( m_filehandle.fildes, l_buf, l_count );
            if ( l_byteswritten == -1 )
            {
                SetError( errno );
                return false;
            }
            if ( l_byteswritten == 0 )
            {
                SetError( FileErrorBasic ( FileError::s_disk_full ) );
                return false;
            }
            l_count -= l_byteswritten;
            l_buf += l_byteswritten;
        }
        return true;
    }

    // ======== WFile::Open ( const FilePath&, const int )================
    bool WFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== WFile::Open () ===========================================
    bool WFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== WFile::CheckAttributes ( int i_attr ) ====================
    bool WFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we only have Write attributes 

        FileAttr mode = *i_attr;

        // Read Not Allowed
        if ( mode & FileAttr::Read )
        {
            return false;       
        }

        // ReadWrite Not Allowed
        if ( mode & FileAttr::ReadWrite )
        {
            return false;       
        }

        // Create and Preserve not allowed together
        if ( mode & FileAttr::Create && mode & FileAttr::Preserve )
        {
            // Create has presedance over Preserve so remove Preserve
            mode.Remove ( FileAttr::Preserve );
        }

        if ( mode & FileAttr::New && mode & FileAttr::Create )
        {
            return false;
        }
        if ( mode & FileAttr::New && mode & FileAttr::Preserve )
        {
            mode.Remove ( FileAttr::Preserve );
        }

        // Must Have Write
        if ( ! (mode & FileAttr::Write ) )
        {
            mode.Add ( FileAttr::Write );
        }

        *i_attr =  mode;

        return true;
    }

    /////////////////////////////////////////////////////////////////////
    //
    // Gx86 Implementation of RWFile
    //
    /////////////////////////////////////////////////////////////////////
static int numConstructors = 0;

    // ======== RWFile::RWFile ( const FilePath& ,int ) ==================
    RWFile::RWFile ( const FilePath & i_file_path, const FileAttr& i_attr  ) 
        : BaseFile ( i_file_path, i_attr ), RFile ( i_file_path, i_attr ), WFile ( i_file_path, i_attr )
    {
      //printf("**************** In RWFile Constructor # %d\n",++numConstructors);
    }

    // ======== RWFile::RWFile () ========================================
    RWFile::RWFile () : BaseFile (), WFile (), RFile ()
    {
    }

    // ======== RWFile::Open ( const FilePath& , const int ) ============
    bool RWFile::Open ( const FilePath & i_file_path, const FileAttr& i_attr )
    {
        return BaseFile::Open ( i_file_path, i_attr );
    }

    // ======== RWFile::Open () =========================================
    bool RWFile::Open ()
    {
        return BaseFile::Open ();
    }

    // ======== RWFile::CheckAttributes ( int *i_attr ) ================
    bool RWFile::CheckAttributes ( FileAttr *i_attr )
    {
        // Make sure we have Read / Write attributes 
        FileAttr mode = *i_attr;

        // Read Not Allowed
        if ( mode & FileAttr::Read )
        {
            return false;       
        }

        // Write Not Allowed
        if ( mode & FileAttr::Write )
        {
            return false;       
        }

        // Create and Preserve not allowed together 
        if ( mode & FileAttr::Create && mode & FileAttr::Preserve )
        {
            // Create takes presedance remove Preserve 
            mode.Remove ( FileAttr::Preserve );
        }

        if ( mode & FileAttr::New && mode & FileAttr::Create )
        {
            return false;
        }
        if ( mode & FileAttr::New && mode & FileAttr::Preserve )
        {
            mode.Remove ( FileAttr::Preserve );
        }

        // Must Have ReadWrite
        if ( ! ( mode & FileAttr::ReadWrite ) )
        {
            mode.Add ( FileAttr::ReadWrite );
        }

        *i_attr = mode;

        return true;
    }

// ======== SystemFileContext =============================================
/**
*       
*       SystemFileContext wraps the read buffer 
*       
*
*/
class SystemFileContext : public aiocb, public FileContext
{
public:

    // ======== SystemFileContext::SystemFileContext =============================================
    /**
    *           Constructs aSystemFileContext object
    *   
    */

    SystemFileContext () 
    {
		aio_sigevent.sigev_value.sival_ptr = (void*)this;

		aio_fildes = 0;
		aio_buf = 0;
		aio_nbytes = 0;
		aio_offset = 0;
		aio_reqprio = 0;
		aio_lio_opcode = 0;
    }

	Mutex* m_base_file_mutex;

	// ======== SetMutex ==============================================
	/**
	*		@param i_mutex is used to pass in the protected mutex from
	*		the RFile base class for use by the call back function
	*/

	void SetBaseFileMutex( Mutex &i_mutex )
	{
		m_base_file_mutex = &i_mutex;
	}

    // ======== Setposition =============================================
    /**
    *       @param i_pos Fills indicates
    *       the current position of the file pointer
    */

    void SetPosition ( long i_pos ) {}   

    // ======== SetBuffer =============================================
    /**
    *       @param i_buffer Accepts a buffer and takes control of it
    *                       this buffer usually contains the result of a read
    */

    virtual void SetBuffer  ( at::PtrDelegate<at::Buffer*> i_buffer ); 

    // ======== SetLead =============================================
    /**
    *       @param associate a lead with our aide
    */

    void SetLead ( AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer lead, Ptr< MutexRefCount * >  i_mutex )
    {
        lead->m_filecontext = this;
        m_aide.AideAssociate ( lead, i_mutex );
    }

    // ======== GetAide =============================================
    /**
    *       @return returns our Aide
    */

    AideTwinMT_Basic<FileNotifyLeadInterface>& GetAide () { return m_aide; }
    
    // ======== SetThis =============================================
    /**
    *       @param i_basefile The this pointer of the calling class
    */
    virtual void SetThis ( at::BaseFile* i_basefile );
    
private:

    /**
    *     m_aide used to callback client 
    */

    AideTwinMT_Basic<FileNotifyLeadInterface> m_aide;
};

    /////////////////////////////////////////////////////////////////////
    //
    // GX86 Callback Functions 
    //
    /////////////////////////////////////////////////////////////////////

    // ======== read_completed  =================================
 	void read_completed ( sigval_t sigval )
 	{
 		at::SystemFileContext* filecontext = (at::SystemFileContext*)sigval.sival_ptr;
        AT_Assert ( filecontext); 

		int result = aio_return ( (aiocb*)filecontext );
        if ( result != -1 )
        {
        	filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, at::FileErrorBasic::s_success );
        }
        else
		{
			const FileErrorBasic fe ( errno );
			filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, fe );
		}
		filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::Cancel);

        delete filecontext;
 	}

    // ======== write_completed =================================
 	void write_completed ( sigval_t sigval )
	{
 		at::SystemFileContext* filecontext = (at::SystemFileContext*)sigval.sival_ptr;
        AT_Assert ( filecontext ); 		

        int result = aio_return ( (aiocb*)filecontext );
        if ( result != -1 )
        {
         	filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, at::FileErrorBasic::s_success );
        }
        else
        {
        	const FileErrorBasic fe ( errno );
 			filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::IOCompleted, filecontext, fe );
        }
		filecontext->GetAide ().CallLead ().VoidCall ( &at::FileNotifyLeadInterface::Cancel);

        delete filecontext;
	}

    // ======== RFile::ReadAsync =================================
    bool RFile::ReadAsync ( at::PtrDelegate< Buffer * > i_br, 
        AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead, Int64 i_off, int i_from ) 
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        AT_Assert ( i_br.Get () != NULL );
        AT_Assert ( i_br.Get()->Region().m_allocated != 0 );
        AT_Assert ( m_mode & FileAttr::Async );

        // Create a new Overlapped struct NOTE: This needs to be deleted - LifeTime!
        SystemFileContext * filecontext = new SystemFileContext;
        // Store the Buffer
        filecontext->SetBuffer ( i_br );
        // Store the This Pointer
        filecontext->SetThis ( this );
        // Store the Lead
        filecontext->SetLead ( i_lead, RFile::m_mutex );

		filecontext->aio_sigevent.sigev_notify = SIGEV_THREAD;
		filecontext->aio_sigevent.sigev_notify_function = read_completed;
		filecontext->aio_sigevent.sigev_notify_attributes = NULL;

        // If requested perform a seek (-1 means no seek)
        if ( i_off > -1 ) 
        {
            try
            {
                Seek ( i_off, i_from );
            }
            catch ( FileErrorBasic & fe )
            {
                SetError ( fe );

                return false;
            }
        }
        
        // Read Asynchronously
        if ( -1 == aio_read ( (aiocb*)filecontext ) )
        {
            SetError  ( errno );

            return false;
        }

        return true;
    }

    // ======== WFile::WriteAsync =================================
    bool WFile::WriteAsync ( const at::PtrDelegate< Buffer * > i_br, 
        AideTwinMT_Basic<FileNotifyLeadInterface>::t_LeadPointer i_lead ) 
    {
        AT_Assert ( m_filehandle.fildes != -1 );
        AT_Assert ( i_br.Get () != NULL );
        AT_Assert ( i_br.Get()->Region().m_allocated != 0 );
        AT_Assert ( m_mode & FileAttr::Async );

        // Create a new Overlapped struct should be deleted in completion routine
        SystemFileContext * filecontext = new SystemFileContext;
        // Store the buffer
        filecontext->SetBuffer ( i_br );
        // Store the This Pointer
        filecontext->SetThis ( this );
        // Store the Lead
        filecontext->SetLead ( i_lead, WFile::m_mutex );
		
        filecontext->aio_sigevent.sigev_notify = SIGEV_THREAD;
        filecontext->aio_sigevent.sigev_notify_function = write_completed;
        filecontext->aio_sigevent.sigev_notify_attributes = NULL;

        // Write  Asynchronously
        aiocb* aio_cb = (aiocb*)filecontext;
        if ( -1 == aio_write ( aio_cb ) )
        {
            SetError ( errno );
            
            return false;
        }
        
        return true;
    }

	// ======== FileContext::SetBuffer =================================
    void SystemFileContext::SetBuffer  ( at::PtrDelegate<at::Buffer*> i_buffer ) 
    { 
    	m_buffer = i_buffer; 

		aio_buf = (volatile void*)GetRawBytes ();
		aio_nbytes = GetBufferSize ();
    }

	// ======== FileContext::SetThis =================================
    void SystemFileContext::SetThis ( at::BaseFile* i_basefile ) 
    { 
    	m_basefile = i_basefile; 
    	aio_fildes = i_basefile->GetFileHandle ().fildes;
		aio_offset = i_basefile->GetPosition ();
    }

#if 0
    // @@ REV -- need to fully document protocol and expected behaviors
    // ======== FileNotifyLeadInterface::Cancel ============================
    void FileNotifyLeadInterface::Cancel ()
    {
        AT_Assert ( m_filecontext );
        aio_cancel ( m_filecontext->GetThis ()->GetFileHandle ().fildes, (aiocb*)m_filecontext ); 
    }
#endif

    FileInitializer::FileInitializer ( int & argc, const char ** & argv )
    {
    	struct aioinit init;
    	memset ( &init, 0, sizeof(aioinit) );
    	init.aio_threads = 1024;
    	init.aio_num = 1024;
    	aio_init ( &init );
    }

    FileInitializer::~FileInitializer ()
    {
    }

// ======== CurrentDirectory ==========================================
/**
 * CurrentDirectory returns a filepath of the current directory
 *
 * @return The current directory.
 */

FilePath CurrentDirectory()
{

    char l_buf[ 1024 ];
    char * l_bufp = getcwd( l_buf, sizeof( l_buf ) );

    AT_Assert( l_bufp );
    
    return FilePath( std::string( l_buf ) );
}

// ======== SetBinaryMode =========================================

bool SetBinaryMode( int i_filedes, bool i_set_binary )
{
    return true;
}


} // namespace at
