//
// The Austria library is copyright (c) Gianni Mariani 2004.
// 
// Grant Of License.  Grants to LICENSEE the non-exclusive right to use the Austria
// library subject to the terms of the LGPL.
// 
// A copy of the license is available in this directory or one may be found at this URL:
// 	http://www.gnu.org/copyleft/lesser.txt
// 
/**
 * at_gx86_stack_trace_cpp.i
 *
 */

//
// This implements the gx86 (Linux) stack trace

#include "at_stack_trace.h"
#include "at_types.h"
#include "at_lifetime_mt.h"

#include <execinfo.h>
#include <cstdlib>
#include <vector>

// Austria namespace
namespace at
{

// ======== StackTrace_Gx86 ===========================================

class AUSTRIA_EXPORT StackTrace_Gx86
  : virtual public PtrTarget_MTVirtual,
    public StackTrace
{

    public:

    StackTrace_Gx86(
        void                           ** i_addresses,
        int                               i_count
    )
      : m_addresses( i_addresses, i_addresses + i_count ),
        m_backtrace_symbols( 0 )
    {
    }

    ~StackTrace_Gx86()
    {
        if ( m_backtrace_symbols )
        {
            std::free( m_backtrace_symbols );
        }
    }


    // ======== ProcessFrames ==========================================
    /**
     * ProcessFrames will process the stack trace into StackFrame
     * objects.
     *
     * @return nothing
     */

    virtual void ProcessFrames() const
    {
        // if the processing is already done, don't do it again
        
        if ( 0 != m_backtrace_symbols )
        {
            return;
        }

        // go get symbolds
        m_backtrace_symbols = backtrace_symbols(
            & m_addresses[0],
            m_addresses.size()
        );

        // make sure we got them
        AT_Assert( m_backtrace_symbols );

        // create the frames

        m_frames.reserve( m_addresses.size() );
        
        for ( unsigned i = 0; i < m_addresses.size(); ++ i )
        {
            StackFrame      l_frame = {
                m_addresses[ i ],
                m_backtrace_symbols[ i ]
            };
            
            m_frames.push_back( l_frame );
        }
        
    }    

    // ======== Begin =================================================

    virtual PtrDelegate<StackIterator *> Begin() const;

    // ======== operator< =============================================

    virtual bool operator< ( const StackTrace & i_rhs ) const
    {
        const StackTrace_Gx86 * l_st_gx86 = dynamic_cast< const StackTrace_Gx86 * >( & i_rhs );

        AT_Assert( l_st_gx86 != 0 );

        return m_addresses < l_st_gx86->m_addresses;
    }
    
    // This contains the addresses collected from the
    // backtrace
    std::vector< void * >                   m_addresses;

    // Place to store all the stack frames
    //
    mutable std::vector< StackFrame >       m_frames;

    // Store all the backtrace symbols
    mutable char                         ** m_backtrace_symbols;


};

// ======== StackIterator_Gx86 ========================================

class AUSTRIA_EXPORT StackIterator_Gx86
  : virtual public PtrTarget_MTVirtual,
    public StackIterator
{
    public:

    // ======== StackIterator_Gx86 ====================================
    
    StackIterator_Gx86(
        at::PtrDelegate< const StackTrace_Gx86 * >    i_stace_trace
    )
      : m_stace_trace( i_stace_trace ),
        m_iterator( m_stace_trace->m_frames.begin() )
    {
    }
    

    // ======== Next ==================================================
    virtual bool Next()
    {
        if ( m_iterator == m_stace_trace->m_frames.end() )
        {
            return false;
        }
        
        return m_stace_trace->m_frames.end() != ++ m_iterator;

    }


    // ======== Previous ==============================================
    virtual bool Previous()
    {
        if ( m_iterator == m_stace_trace->m_frames.begin() )
        {
            return false;
        }

        -- m_iterator;
        return true;
    }


    // ======== Last ==================================================
    virtual void Last()
    {
        m_iterator = m_stace_trace->m_frames.end();
    }


    // ======== First =================================================
    virtual void First()
    {
        m_iterator = m_stace_trace->m_frames.begin();
    }


    // ======== IsValid ===============================================
    virtual bool IsValid() const
    {
        return m_stace_trace->m_frames.end() != m_iterator;
    }


    // ======== DeReference ===========================================
    const StackFrame & DeReference() const
    {
        AT_Assert( IsValid() );
        return * m_iterator;
    }
    
    // m_stace_trace points to the StackTrace_Gx86 associated with this
    // iterator.
    
    at::Ptr< const StackTrace_Gx86 * >      m_stace_trace;

    // m_iterator is the current iterator
    std::vector< StackFrame >::iterator     m_iterator;

};


// ======== StackTrace_Gx86::Begin ====================================

PtrDelegate<StackIterator *> StackTrace_Gx86::Begin() const
{
    ProcessFrames();
    
    return new StackIterator_Gx86( at::PtrView< const StackTrace_Gx86 * >( this ) );
}


// ======== NewStackTrace =============================================
/**
 * NewStackTrace is implementation dependant, it will create a new
 * stack trace and record the stack trace at this point.
 *
 * @return A pointer to a new stack trace.
 */

PtrDelegate< StackTrace * > NewStackTrace()
{
    void        * l_addresses[ 1024 ];

    int l_count = backtrace( l_addresses, CountElements( l_addresses ) );
    
    return new StackTrace_Gx86( l_addresses, l_count );
}

}; // namespace at
