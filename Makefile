
# This is the top level Makefile
# - public merged

#
# MXI_ROOT is different for different places.  This forces the
# MXI_ROOT to be re-evaluated.
#
MXI_ROOT=
IN_LINK_REMOVE_COMMAND=true

IN_DOX_SUBDIR=src

IN_SEPTICIDE_ENVIRON=
IN_SEPTICIDE_OPTIONS=-b '$(MAKE) all' -b '$(MAKE) test' -t '$(MAKE) runtest'
IN_SEPTICIDE_DATABASE='host=min port=5433'
IN_SEPTICIDE_MAKEFILES=$(wildcard septicide/Makefile.*)
IN_SEPTICIDE_TARGETS=$(IN_SEPTICIDE_MAKEFILES:septicide/Makefile.%=%)

IN_WIN32_DEVENV_SLN=win32/vssln/vssln.sln

#
# include septicide configuration target (if any)
#
IN_SEPTICIDE_FILE=$(wildcard septicide/Makefile.$(IN_SEPTICIDE_PLATFORM))
ifneq ($(IN_SEPTICIDE_FILE),)
include $(IN_SEPTICIDE_FILE)
endif

IN_TARGETS_INSTALL=post_install

include Makefile.xsi

make/distclean : $(subst make/distclean,,$(MXS_MAKEDIRS:%=%/distclean))

post_install : $(subst post_install,,$(MXS_MAKEDIRS:%=%/install))

.PHONY: official
official :
	export INL_RELEASE_BUILD=1; \
	export INL_OFFICIAL_BUILD=1; \
	export INL_OFFICIAL_BUILD_VERS=1; \
	$(MAKE) all


.PHONY : $(IN_SEPTICIDE_TARGETS)
$(IN_SEPTICIDE_TARGETS) :
	( export IN_SEPTICIDE_PLATFORM=$@; $(MAKE) septicide )

.PHONY: septicide
ifeq ($(IN_SEPTICIDE_PLATFORM),)
septicide :
	echo "septicide targets are $(IN_SEPTICIDE_TARGETS)"
else
septicide :
	$(IN_SEPTICIDE_ENVIRON) \
	sp_execute \
		-d $(IN_SEPTICIDE_DATABASE) \
		-r $(shell svn info | grep Revision: | cut -d' ' -f 2) \
		-p $(IN_SEPTICIDE_PLATFORM) \
		-l septicide.log -b 'stty -onlcr; true' \
		$(IN_SEPTICIDE_OPTIONS)
	echo Done septicide
endif

#
# Rule to generate a raw septicide config.
#
septicide/Makefile.% :
	( \
		echo "# Septicide config for $(*)"; \
		echo "# This file is included in the top level Makefile"; \
		echo "# for a particular septicide build."; \
		echo "#"; \
		echo "# Uncomment required options."; \
		echo "#"; \
		echo "#IN_SEPTICIDE_ENVIRON="; \
		echo "#IN_SEPTICIDE_ENVIRON+=INL_M32_BUILD=1; export INL_M32_BUILD; MXI_ARCH=gx86_32; export MXI_ARCH; make make_Includedirs;"; \
		echo "#IN_SEPTICIDE_ENVIRON+=INL_RELEASE_BUILD=1; export INL_RELEASE_BUILD;"; \
		echo "#IN_SEPTICIDE_OPTIONS=-b 'make all' -b 'make test' -t 'make runtest'"; \
	) > septicide/$@


#
# visual studio 2003 builds
#
win32_all : $(MXS_OBJDIR_DEP) src/all
	#cmd /c 'devenv /useenv /build $(MXS_DEVENV_BUILD_TYPE) $(IN_WIN32_DEVENV_SLN)'

win32_clean : $(MXS_OBJDIR_DEP)
	#cmd /c 'devenv /clean $(MXS_DEVENV_BUILD_TYPE) $(IN_WIN32_DEVENV_SLN)'
	rm $(MXI_ROOT)/win32/vssln/vssln.ncb || true;
	rm $(MXI_ROOT)/win32/vssln/vssln.suo || true;

src/all : fix_link_exe_bug

.PHONY: fix_link_exe_bug
fix_link_exe_bug :
	$(IN_LINK_REMOVE_COMMAND) || true;

#
# nctv_shuttle post-install bundle creator
#
.PHONY: post_install_shuttle_bundler
post_install_shuttle_bundler:
	echo "Creating nctv_installer.exe ..."
	$(MXI_ROOT)/src/inno_setup/iscc "$(MXS_BUILDPREFIX)install\nctv\shuttle\nctv_installer.iss"
	echo "...done creating nctv_installer.exe"
	echo "Creating nctv_bootstrap.exe ..."
	$(MXI_ROOT)/src/inno_setup/iscc "$(MXS_BUILDPREFIX)install\nctv\shuttle\nctv_bootstrap.iss"
	echo "...done creating nctv_bootstrap.exe"
	echo "Creating nctv_bootstrap_new_mm.exe ..."
	$(MXI_ROOT)/src/inno_setup/iscc "$(MXS_BUILDPREFIX)install\nctv\shuttle\nctv_bootstrap_new_mm.iss"
	echo "...done creating nctv_bootstrap_new_mm.exe"


.PHONY: post_install
post_install:
	$(MAKE) post_install_shuttle_bundler

#
# copy the official bits over to the "official builds" location
#
IN_OFFICIAL_BUILDS=//min/public/official_builds

IN_THIS_BUILD_VER:=$(IN_OFFICIAL_BUILDS)/$(INL_OFFICIAL_BUILD_VERS)
IN_THIS_BUILD:=$(IN_THIS_BUILD_VER)/$(MXS_BUILDPREFIX)

IN_DEC_PACKAGE_DIRENAME=dec_package
IN_DEC_PACKAGE=$(MXI_ROOT)/$(MXS_BUILDPREFIX)/install/nctv/$(IN_DEC_PACKAGE_DIRENAME)
IN_DEC_PACKAGE_BUNDLE=$(IN_DEC_PACKAGE).tar.gz

r_official_publish :
	export INL_RELEASE_BUILD=1; $(MAKE) official_publish

official_publish : $(IN_DEC_PACKAGE_BUNDLE)
	mkdir "$(IN_THIS_BUILD_VER)"
	mkdir "$(IN_THIS_BUILD)"
	cp "$(MXS_BUILDPREFIX)install/nctv/shuttle/Output/nctv_installer.exe" "$(IN_THIS_BUILD)"
	cp "$(MXS_BUILDPREFIX)install/nctv/shuttle/Output/nctv_bootstrap.exe" "$(IN_THIS_BUILD)"
	cp "$(MXS_BUILDPREFIX)install/nctv/shuttle/Output/nctv_bootstrap_new_mm.exe" "$(IN_THIS_BUILD)"
	cp "src/nctv_shuttle_win/win32/projects/nctv_shuttle_prog/$(MXS_BUILDPREFIX)nctv_shuttle_prog.exe" "$(IN_THIS_BUILD)nctv_shuttle.exe"
	cp "$(IN_DEC_PACKAGE_BUNDLE)" "$(IN_THIS_BUILD)"
	pwd > "$(IN_THIS_BUILD)build_dir.txt"
	svn info > "$(IN_THIS_BUILD)svn_info.txt"


r_dec_bundle :
	export INL_RELEASE_BUILD=1; $(MAKE) dec_bundle

.PHONY: dec_bundle
dec_bundle : $(IN_DEC_PACKAGE_BUNDLE)

# The dec package is dependant on all the files in the directory being bundled.
$(IN_DEC_PACKAGE_BUNDLE) : $(wildcard $(IN_DEC_PACKAGE)/*)
	cd $(IN_DEC_PACKAGE)/.. && tar czf $(IN_DEC_PACKAGE_BUNDLE) $(IN_DEC_PACKAGE_DIRENAME)

