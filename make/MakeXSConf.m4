#
# MakeXSConf.m4
#
# This contains application specific defines.
#
#*************************************************************************
# This program or document is free software; you can redistribute it     *
# and/or modify it under the terms of the GNU General Public License as  *
# published by the Free Software Foundation ; either version 2 of the    *
# License, or (at your option) any later version.                        *
#                                                                        *
# This document is distributed in the hope that it will be useful, but   *
# without any warranty; without even the implied warranty of             *
# merchantability or fitness for a particular purpose. See the GNU       *
# General Public License for more details.                               *
#                                                                        *
# No Warranty                                                            *
# Because the program or document is licensed free of charge, there is no*
# warranty for the program, to the extent permitted by applicable law.   *
# Except when otherwise stated in writing the copyright holders and/or   *
# other parties provide the program "as is" without warranty of any kind,*
# either expressed or implied, including, but not limited to, the implied*
# warranties of merchantability and fitness for a particular purpose. The*
# entire risk as to the quality and performance of the program is with   *
# you. Should the program prove defective, you assume the cost of all    *
# necessary servicing, repair or correction.                             *
#                                                                        *
# In no event unless required by applicable law or agreed to in writing  *
# will any copyright holder, or any other party who may modify and/or    *
# redistribute the program or document as permitted above, be liable to  *
# you for damages, including any general, special, incidental or         *
# consequential damages arising out of the use or inability to use the   *
# program or document (including but not limited to loss of data or data *
# being rendered inaccurate or losses sustained by you or third parties  *
# or a failure of the program to operate with any other programs), even  *
# if such holder or other party has been advised of the possibility of   *
# such damages.                                                          *
#*************************************************************************
#

#
# Naming convention.
#
#	BUILDM_	defined in Makefiles
#	BUILD_	defined in environment (defaults in MakeXSConf)
#
dnl
dnl install_directories
dnl For each target directory in the installation, an entry needs to be made
dnl in the install_directories list. This will create MXS_INSTABLE_<uppercase_dir>
dnl gmake macros.
dnl
define(`install_directories',dnl
`(dnl
`test_dir/base'dnl
)'dnl
)
dnl
dnl
dnl uuid_files
dnl For each targettable uuid place an entry into uuid_files.  This will create a
dnl gmake macro that uses can be used to locate a file containing the guid to be used.
dnl
define(`uuid_files',`(`hello_world_guid')')

define(`IN4_APPLICATION_VARIABLES',`

M4forech(Ix,install_directories,`MIN_BASE_VARIABLE(INSTABLE_`'upcase(Ix),`Installable files for 'Ix)
')

M4forech(Ix,uuid_files,`MIN_BASE_OPTION_VARIABLE(GUID_`'upcase(Ix),`GUID for 'Ix)
')

M4forech(Ix,uuid_files,`MIN_BASE_OPTION_VARIABLE(GUID_FILE_`'upcase(Ix),`GUID for 'Ix)
')

M4forech(Ix,uuid_files,`MIN_BASE_OPTION_VARIABLE(FILEVERSION_`'upcase(Ix),`GUID for 'Ix)
')

MIN_BASE_OPTION_VARIABLE(SIGNCODE_PARAMS,`Paramaters for signcode')

')

#
# IN4_APPLICATION_DEFINES must define RMRF, CC, and MAKE
#
define(`IN4_APPLICATION_DEFINES',`

ifeq ($(MXI_ROOT),)
x:=$(shell exec echo "------------------------------------------------------------" 1>&2)
x:=$(shell exec echo "ERROR - ERROR - ERROR - ERROR - ERROR - ERROR - ERROR" 1>&2)
x:=$(shell exec echo "MXI_ROOT is undefined, MXI_ROOT is the top level directory" 1>&2)
x:=$(shell exec echo "for this tree. Set this environment variable and try again" 1>&2)
x:=$(shell exec echo "------------------------------------------------------------" 1>&2)
# syntax error here
$(error errors encountered - aborting)
.exit
x:=$(shell exec echo "MXI_ROOT=$(MXI_ROOT)" 1>&2)
else

endif

ifeq ($(MXI_REALARCH),)
#
# Try to automatically define MXI_REALARCH
#

BUILDM_UNAME_P:=$(shell uname -p)
BUILDM_UNAME_S:=$(shell uname -s)
BUILDM_UNAME_M:=$(shell uname -m)

ifeq ($(BUILDM_UNAME_P),sparc)
MXI_REALARCH=sparc
export MXI_REALARCH
else

ifeq ($(BUILDM_UNAME_S),Darwin)
MXI_REALARCH=darwin
else
ifeq ($(BUILDM_UNAME_P),powerpc)
MXI_REALARCH=ppc
export MXI_REALARCH
else

ifeq ($(BUILDM_UNAME_S),Linux)
ifeq ($(BUILDM_UNAME_M),i686)
MXI_REALARCH=gx86_32
export MXI_REALARCH
endif
endif

ifeq ($(BUILDM_UNAME_S),Linux)
ifeq ($(BUILDM_UNAME_M),x86_64)
ifeq ($(INL_M32_BUILD),1)
MXI_REALARCH=gx86_32
export MXI_REALARCH
else
MXI_REALARCH=gx86_64
export MXI_REALARCH
endif
endif
endif

ifneq ($(findstring CYGWIN,$(BUILDM_UNAME_S)),)
MXI_REALARCH=win32
export MXI_REALARCH
endif

endif #ifeq ($(BUILDM_UNAME_S),Darwin)
endif #ifeq ($(BUILD_UNAME_P),ppc)
endif #ifeq ($(BUILD_UNAME_P),sparc)
endif #ifeq ($(MXI_ARCH),)

ifeq ($(MXI_ARCH),)
MXI_ARCH=$(MXI_REALARCH)
endif

# Special defines for yasql
BUILDM_SUPPORTED_ARCHS=gx86_32 gx86_64 mipsl mipsb sspc win32 ppc sparc darwin

#
# Pick the build architecture

ifeq (,$(findstring $(MXI_ARCH),$(BUILDM_SUPPORTED_ARCHS)))
x:=$(shell exec echo "------------------------------------------------------------" 1>&2)
x:=$(shell exec echo "ERROR - ERROR - ERROR - ERROR - ERROR - ERROR - ERROR" 1>&2)
x:=$(shell exec echo "MXI_ARCH is set to $(MXI_ARCH) which is not a supported" 1>&2)
x:=$(shell exec echo "architecture.  Set MXI_ARCH to one of :" 1>&2)
x:=$(shell exec echo "	$(BUILDM_SUPPORTED_ARCHS)" 1>&2)
x:=$(shell exec echo "You can correct this by adding auto detection in " 1>&2)
x:=$(shell exec echo "	$(MXI_ROOT)/make/MakeXSConf.m4" 1>&2)
x:=$(shell exec echo "or setting MXI_ARCH explicitly" 1>&2)
x:=$(shell exec echo "------------------------------------------------------------" 1>&2)
x:=$(shell echo .exit; exit 1  )
# syntax error here
.exit
endif

#
# BUILDM_OTHER_ARCH is all architectures but the current one.
# This is useful for building list of exclude files or dirs
#
BUILDM_OTHER_ARCH=$(filter-out $(MXI_ARCH),$(BUILDM_SUPPORTED_ARCHS))


XXX_EMPTY=
XXX_SPACE=$(XXX_EMPTY) $(XXX_EMPTY)

ING_DEFAULT_USERNAME=$(subst $(XXX_SPACE),_,$(if $(USERNAME),$(USERNAME),$(USER)))

#
# Include official build definitions file.
#

ING_OFFICIAL_BUILD_VERSION_DEFS=$(MXI_ROOT)/official_build_version.defs
ifneq ($(wildcard $(ING_OFFICIAL_BUILD_VERSION_DEFS)),)
`include' $(MXI_ROOT)/official_build_version.defs
endif

#
# INL_OFFICIAL_BUILD_VERS contains a build number
#

INL_OFFICIAL_BUILD_VERS=$(if $(BUILD_OFFICIAL_VERSION),$(BUILD_OFFICIAL_VERSION),$(shell date +%y%m%d%H%M)-$(shell uname -m|sed -e "s/\ /_/g")-$(shell uname -n)-$(MXI_ARCH)-$(MXS_USERNAME))

#
# INL_OFFICIAL_BUILD_SUFFIX contains a suffix for build related targets
#

INL_OFFICIAL_BUILD_SUFFIX=_$(if $(BUILD_OFFICIAL_NUMBER),$(BUILD_OFFICIAL_NUMBER),$(MXS_USERNAME))

#
# INL_OFFICIAL_BUILD_VERSION_NUMERIC contains a numeric version number
#

INL_OFFICIAL_BUILD_VERSION_NUMERIC=$(if $(BUILD_OFFICIAL_NUMBER),$(shell echo $(BUILD_OFFICIAL_NUMBER) | bc),$(shell date +%y%j | bc))

BUILDM_CFLAGS=\
	-Wreturn-type -W -Wpointer-arith -pipe -ggdb3 -fcheck-new -fPIC $(BUILDM_RELEASE_FLAGS) \
	-D_POSIX_THREADS -D_POSIX_THREAD_SAFE_FUNCTIONS -D_REENTRANT -DACE_HAS_AIO_CALLS \
	-DBUILD_VERSION=$(INL_OFFICIAL_BUILD_VERS) -DBUILD_ARCH=$(MXI_ARCH) \
	$(if $(filter-out %64,$(MXI_ARCH)),-m32)

ING_SHELL_COMMAND= \
	HT_SUFFIX=$(INL_OFFICIAL_BUILD_SUFFIX); export HT_SUFFIX; \
	HT_OFFICIAL_VERSION=$(INL_OFFICIAL_BUILD_VERS); export HT_OFFICIAL_VERSION;

ING_DEFAULT_CFLAGS=$(BUILDM_CFLAGS)
ING_DEFAULT_CXXFLAGS=$(BUILDM_CFLAGS)
ING_DEFAULT_LDFLAGS=-lpthread -rdynamic
ING_DEFAULT_FILES_CLEAN=*.BAK core.[0-9]* jnk *.o *.plg $(MXS_BUILDDIR)
ING_DEFAULT_MIDL_INCLUDE_PATH=$(shell ( dirname "$(BC)which midl$(BC)" > $(TEMP)/midl_inc_1.tmp; dirname "$(BC)cat $(TEMP)/midl_inc_1.tmp$(BC)" > $(TEMP)/midl_inc_2.tmp ) 2>&1 ; sed -e s/\$$/\\/Include/ $(TEMP)/midl_inc_2.tmp )

#
# Ant executable default path
#

ING_DEFAULT_ANT=$(MXI_ROOT)/src/apache_ant/code/$(MXS_BUILDPREFIX)install/bin/ant

#
# Default post-build command for packaged builds
#

ING_DEFAULT_EX_POST_BUILD_COMMAND=$(MAKE) make_Includedirs

#
# Use prebuild bundles if they exist
#
ING_DEFAULT_USE_PREBUILT_BUNDLES=$(wildcard $(MXS_EX_PREBUILT_FILE))


#
# As per Verisigns customer support response, they no longer support
# the :commercial", "individual" attribute.
#
#ING_DEFAULT_SIGNCODE_PARAMS=-$$ commercial
#
ING_DEFAULT_SIGNCODE_PARAMS=

ifeq ($(MXI_ARCH),win32)
ING_DEFAULT_SKIP_INCLUDE_DIR_BUILD=1
else
ING_DEFAULT_SKIP_INCLUDE_DIR_BUILD=0
endif

#
# mostlyclean should perform all that clean does and more
ING_DEFAULT_FILES_MOSTLYCLEAN=$(MXS_FILES_CLEAN)
ING_DEFAULT_TARGETS_MOSTLYCLEAN=$(MXS_TARGETS_CLEAN)

#
# distclean should perform all that mostlyclean does and more
ING_DEFAULT_FILES_DISTCLEAN=$(MXS_FILES_MOSTLYCLEAN)
ING_DEFAULT_TARGETS_DISTCLEAN=$(MXS_TARGETS_MOSTLYCLEAN)

#
# try to execute subdirectories in parallel for ALL.

ING_DEFAULT_PARALLEL_JOBS_ALL=2
ING_DEFAULT_PARALLEL_JOBS_TEST=2
ifeq ($(MXI_ARCH),win32)
ING_DEFAULT_PARALLEL_JOBS_ALL=1
ING_DEFAULT_PARALLEL_JOBS_TEST=1
endif


ING_DEFAULT_OFFICIAL_BUILD=
ING_DEFAULT_RELEASE_BUILD=

#
# Defailt extension of c++ file names
ING_DEFAULT_CPP_EXTENSION=.cpp

ING_DEFAULT_CXXFILES=$(MXS_ALL_CXXFILES)
ING_DEFAULT_CFILES=$(MXS_ALL_CFILES)
ING_DEFAULT_LINKOBJS=$(MXS_CXXOBJS) $(MXS_COBJS)

#
# find all Makefile files in subdirectories - causes automatic
# traversal of all subdirectoris containing Makefile.  This
# excludes all directories that are architecture dependant.

ING_DEFAULT_MAKESUBFILES_ALL=$(subst XXXXX,,$(filter-out $(BUILDM_OTHER_ARCH:%=%/Makefile),XXXXX $(wildcard */Makefile) XXXXX))
ING_DEFAULT_MAKESUBFILES_SETUP=$(ING_DEFAULT_MAKESUBFILES_ALL)
ING_DEFAULT_SUBMAKEFILES=$(wildcard */Makefile)

ING_BASIC_TARGETS_ALL=$(MXS_CXXOBJS) $(MXS_COBJS)\
	$(MXS_CXX_EXECUTABLE) $(MXS_C_EXECUTABLE) \
	$(MXS_STATIC_LIBRARY_NAME) $(MXS_CXX_DSO_NAME) $(MXS_C_DSO_NAME) \
	$(MXS_STATIC_RELOCATABLE_NAME)

ifeq ($(MXI_ARCH),win32)
ING_DEFAULT_TARGETS_ALL=$(if $(MXS_USE_CYGWIN_GCC),$(ING_BASIC_TARGETS_ALL),) $(if $(MXS_MSVS_PROJECT_NAME),$(if $(filter-out TRUE,$(MXS_MSVS_TEST_PROJECT)),devenv_build,),)
ING_DEFAULT_TARGETS_TEST=$(if $(MXS_USE_CYGWIN_GCC),$(ING_BASIC_TARGETS_TEST),) $(if $(MXS_MSVS_PROJECT_NAME),$(if $(filter-out FALSE,$(MXS_MSVS_TEST_PROJECT)),devenv_build,),)
else
ING_DEFAULT_TARGETS_ALL=$(ING_BASIC_TARGETS_ALL)
endif

#
# files get built into a subdirectory
ING_RELEASE_PREFIX=release
ING_DEVEL_PREFIX=work
ING_RELEASE_BUILDDIR=$(ING_RELEASE_PREFIX).$(MXI_ARCH)
ING_DEVEL_BUILDDIR=$(ING_DEVEL_PREFIX).$(MXI_ARCH)
ING_DEFAULT_BUILDDIR=$(if $(MXS_RELEASE_BUILD),$(ING_RELEASE_BUILDDIR),$(ING_DEVEL_BUILDDIR))
ING_DEFAULT_PREBUILD_SUFFIX=$(if $(MXS_RELEASE_BUILD),$(ING_RELEASE_PREFIX),$(ING_DEVEL_PREFIX))-$(MXI_ARCH)
ING_ALL_DEVEL_DIRS=$(BUILDM_SUPPORTED_ARCHS:%=$(ING_DEVEL_PREFIX).%)
ING_ALL_RELEASE_DIRS=$(BUILDM_SUPPORTED_ARCHS:%=$(ING_RELEASE_PREFIX).%)
ING_BUILD_EXCLUDE_LIST=$(subst $(MXS_BUILDDIR),,$(ING_ALL_RELEASE_DIRS) $(ING_ALL_DEVEL_DIRS))

M4forech(Ix,install_directories,`MXS_INSTALL_`'upcase(Ix)=$(MXS_INSTALL_HOME)/$(MXS_BUILDPREFIX)install/Ix/
')
M4forech(Ix,install_directories,`MXS_INSTALL_TARGET_`'upcase(Ix)=$(foreach insable,$(MXS_INSTABLE_`'upcase(Ix)),$(MXS_INSTALL_`'upcase(Ix))$(notdir $(insable)))
')

M4forech(Ix,uuid_files,`ING_DEFAULT_GUID_FILE_`'upcase(Ix)=$(MXI_ROOT)/build/uuid/uuid$(INL_OFFICIAL_BUILD_SUFFIX).Ix
')

ING_REPO_ADD_CMD=svn add
ING_REPO_CI_CMD=svn ci

dnl
dnl This complicated looking macro evaluates to a shell script that attempts
dnl to pull the uuid from a file and create it if nessasary and place it in
dnl the cvs repository.
dnl
M4forech(Ix,uuid_files,`ING_DEFAULT_GUID_`'upcase(Ix)=dnl
$(shell if test -f $(MXS_GUID_FILE_`'upcase(Ix)); then cat $(MXS_GUID_FILE_`'upcase(Ix)); dnl
elif uuidgen > $(MXS_GUID_FILE_`'upcase(Ix)) ; then cat $(MXS_GUID_FILE_`'upcase(Ix)); dnl
(cd $(dir $(MXS_GUID_FILE_`'upcase(Ix))); $(ING_REPO_ADD_CMD) $(notdir $(MXS_GUID_FILE_`'upcase(Ix))); dnl
$(ING_REPO_CI_CMD) -m "Automatically added uuid file by $(MXS_USERNAME)" $(notdir $(MXS_GUID_FILE_`'upcase(Ix))) dnl
) >> $(MXI_ROOT)/build_uuid_gen.log 2>&1; dnl
else echo "error:could not create guid file" 1>&2; exit 1; fi)
')

ING_BASE_FILEVERSION=3,0,0,

M4forech(Ix,uuid_files,`ING_DEFAULT_FILEVERSION_`'upcase(Ix)=$(ING_BASE_FILEVERSION)$(INL_OFFICIAL_BUILD_VERSION_NUMERIC)
')

#


ING_DEFAULT_TARGETS_INSTALL=M4forech(Ix,install_directories,`$(MXS_INSTALL_TARGET_`'upcase(Ix)) ')

define(`IN4_APP_RULES',`
M4forech(Ix,install_directories,`

#
`#' Install rule for Ix
#
`$('MXS_INSTALL_`'upcase(Ix)`)'% : $(MXS_BUILDPREFIX)%
	@echo "Installing $`'@"
	-$(if $(wildcard $(dir $`'@)),$(RMF) $`'@,$(MKDIR) -p $(dir $`'@))
	$(LN) $< $`'@

`$('MXS_INSTALL_`'upcase(Ix)`)'% : %
	@echo "Installing $`'@"
	-$(if $(wildcard $(dir $`'@)),$(RMF) $`'@,$(MKDIR) -p $(dir $`'@))
	$(LN) $< $`'@

')
')

ING_DEFAULT_INSTALL_HOME=$(MXI_ROOT)

ING_DEFAULT_INSTALL_HOME_DIRS=M4forech(Ix,install_directories,Ix )

ING_gx86_IDIRS=/usr/kerberos/include

ING_DEFAULT_IDIRS=./ $(MXS_BUILDDIR) $(MXS_GEN_INCLUDEDIRS) $(ING_$(MXI_ARCH)_IDIRS)

ING_DEFAULT_INCL_EXCLUDE_PATTERNS=$(filter-out /$(MXI_ARCH)/,$(BUILDM_SUPPORTED_ARCHS:%=/%/)) ACE_wrappers/apps ACE_wrappers/ASNMP \
	ACE_wrappers/PACE ACE_wrappers/docs ACE_wrappers/examples \
	ACE_wrappers/performance-tests ACE_wrappers/tests \
	ACE_wrappers/netsvcs $(INL_USE_ACE) \
	$(patsubst $(MXI_ROOT)/%,%,$(sort $(dir $(shell find $(MXI_ROOT) -follow -name mxs_excl_incl -type f )))) \
	$(ING_BUILD_EXCLUDE_LIST)


#
# Java setup
#

ING_DEFAULT_JAVA_HOME=/usr/java/j2sdk1.4.2_08/
export JAVA_HOME=$(MXS_JAVA_HOME)

#
# Doxygen setup
#
ING_DEFAULT_DOX_PROJECT_NAME=netcableTV
ING_DEFAULT_DOX_PROJECT_NUMBER=$(INL_OFFICIAL_BUILD_VERS)

MXS_INCL_EXCLUDE_FIND_OPTION=$(MXS_INCL_EXCLUDE_PATTERNS:%=$(FC)!$(FC) -path $(MXI_ROOT)/\*%\* )

ifeq ($(MXI_ARCH),darwin)
CCX_EXE=c++
endif

ifeq ($(CC_EXE),)
CC_EXE=/usr/local/bin/gcc
endif
#CC_EXE=gcc
ifeq ($(CXX_EXE),)
CXX_EXE=g++
endif
CC=$(CC_EXE)
CXX=$(CXX_EXE)
INSTALL=/usr/bin/install
MAKE=make
RMF=/bin/rm -f
RMRF=/bin/rm -rf

ifeq ($(MXI_ARCH),win32)
LN=/bin/cp -f
else
LN=/bin/ln -f
endif

MKDIR=/bin/mkdir
BUNZIP2=/usr/bin/bunzip2
TAR=/bin/tar
CHMOD=/bin/chmod
TEXI2HTML=/usr/bin/texi2html
CP=/bin/cp
GUNZIP=/bin/gunzip
FIND=/usr/bin/find
TOUCH=/bin/touch
M4=/usr/bin/m4
CVS=/usr/bin/cvs
BISON=/usr/bin/bison
MV=/bin/mv
MSDEV=msdev
LEX=flex


')

#
# IN4_APPLICATION_FLAGS
#
define(`IN4_APPLICATION_FLAGS',`

# Global defaults for various variables.
#
ifeq ($(MXS_RELEASE_BUILD),1)
ifeq ($(BUILDM_RELEASE_FLAGS),)
BUILDM_RELEASE_FLAGS=-O3 -finline-functions -DRELEASE_BUILD=1
endif
endif

.PRECIOUS: $(MXS_BUILDPREFIX)%.so $(MXS_BUILDPREFIX)%.o $(MXS_BUILDPREFIX)%.dep \
 $(MXS_BUILDPREFIX)%$(MXS_CPP_EXTENSION)


')

#
# IN4_APPLICATION_RULES
#
define(`IN4_APPLICATION_RULES',`

#
# show what we think we are
.PHONY: whatami
whatami :
	@echo MXI_ROOT=$(MXI_ROOT);\
	echo MXI_ARCH=$(MXI_ARCH)


#
# gui target - bring up a directory browser
#
ifeq ($(MXI_ARCH),win32)
.PHONY: gui
gui:
	-$(MXS_FILE_BROWSER) $(subst \,\\,$(shell cygpath -w $$PWD)) &

gui_dir/% :
	-$(MXS_FILE_BROWSER) $(subst \,\\,$(shell cygpath -w $(subst gui_dir/,,$(@)))) &

else

.PHONY: gui
gui:
	$(MXS_FILE_BROWSER) $$PWD &

gui_dir/% :
	$(MXS_FILE_BROWSER) $(subst gui_dir/,,$(@))

endif

.PHONY: shell
shell :
	MAKELEVEL=; export MAKELEVEL; tcsh


.PHONY : win32_projects
win32_projects: $(MXS_WIN32_PROJECTS)

.PHONY : win32/projects/%/vcproj_lib
win32/projects/%/vcproj_lib : $(INL_VCPROJ_FORCE_BUILD)
	( \
		INS_MSVS_BUILD_TYPE=LIBRARY ; export INS_MSVS_BUILD_TYPE ;\
		INS_MSVS_TEST_PROJECT=FALSE ; export INS_MSVS_TEST_PROJECT ;\
		$(MAKE) $(dir $(@))vcproj \
	)

.PHONY : win32/projects/%/vcproj_exe
win32/projects/%/vcproj_exe : $(INL_VCPROJ_FORCE_BUILD)
	( \
		INS_MSVS_BUILD_TYPE=CONSOLE ; export INS_MSVS_BUILD_TYPE ;\
		INS_MSVS_TEST_PROJECT=FALSE ; export INS_MSVS_TEST_PROJECT ;\
		$(MAKE) $(dir $(@))vcproj \
	)

.PHONY : win32/projects/%/vcproj_test
win32/projects/%/vcproj_test : $(INL_VCPROJ_FORCE_BUILD)
	( \
		INS_MSVS_BUILD_TYPE=CONSOLE ; export INS_MSVS_BUILD_TYPE ;\
		INS_MSVS_TEST_PROJECT=TRUE ; export INS_MSVS_TEST_PROJECT ;\
		$(MAKE) $(dir $(@))vcproj \
	)


.PHONY : win32/projects/%/vcproj
win32/projects/%/vcproj : $(INL_VCPROJ_FORCE_BUILD)
	if [ \! -d win32 ]; then \
		mkdir -p win32 ; \
		cp Makefile.xsi win32 ; \
		echo "include Makefile.xsi" > win32/Makefile ; \
	fi
	#	
	if [ \! -d win32/projects ]; then \
		mkdir -p win32/projects ; \
		cp Makefile.xsi win32/projects ; \
		echo "include Makefile.xsi" > win32/projects/Makefile ; \
	fi
	#		
	if [ \! -d $(dir $(@)) ]; then \
		mkdir -p $(dir $(@)) ; \
		mkdir -p $(dir $(@)) ; \
		cp Makefile.xsi $(dir $(@)) ; \
		echo "include Makefile.xsi" > $(dir $(@))Makefile ; \
	fi
	#		
	( cd $(dir $(@)) ; $(MAKE) build_type/$(*).vcproj_type )
	( \
		cd $(dir $(@)) ; \
		MXI_REALARCH=$(MXI_ARCH); export MXI_REALARCH; \
		MXI_ARCH=win32; export MXI_ARCH; \
		$(MAKE) $(*).vcproj \
	)


ifeq ($(MXI_ARCH),win32)
ING_DEFAULT_CXXLIBS=$(MXS_GEN_LIBFILES)
INS_CXXLIBS=wininet.lib ws2_32.lib
IN_MSVS_USE_ATL=2
IN_MSVS_USE_MFC=0
endif

.PHONY: vcproj_incpath
vcproj_incpath :
	@echo "$(foreach XINC,$(MXS_IDIRS),-I $(shell $(MXI_ROOT)/make/$(if $(MXI_REALARCH),$(MXI_REALARCH),$(MXI_ARCH))/relpath $(shell pwd) $(XINC) $(MXI_ROOT)))"

vcproj_incpath_file/% :
	@echo "$(foreach XINC,$(MXS_IDIRS),$(shell $(MXI_ROOT)/make/$(if $(MXI_REALARCH),$(MXI_REALARCH),$(MXI_ARCsvn H))/relpath "$(shell pwd)" "$(XINC)" "$(MXI_ROOT)" $(*)))"
	touch $(*)
	
vcproj_libpath_file/% :
	@echo "$(foreach XINC,$(MXS_GEN_LIBDIRS),$(shell $(MXI_ROOT)/make/$(if $(MXI_REALARCH),$(MXI_REALARCH),$(MXI_ARCH))/relpath "$(shell pwd)" "$(XINC)" "$(MXI_ROOT)" $(*)))"
	touch $(*)
	
vcproj_libfiles_file/% :
	@echo -n $(strip $(MXS_CXXLIBS)) > $(*)
	
vcproj_incpath_quot :
	@echo "$(foreach XINC,$(MXS_IDIRS), -I $(FC)$(shell $(MXI_ROOT)/make/$(if $(MXI_REALARCH),$(MXI_REALARCH),$(MXI_ARCH))/relpath $(shell pwd) $(XINC) $(MXI_ROOT))$(FC))"

MXS_VCPROJ_DIRPATH=$(shell \
	true $(foreach XINC,$(MXS_GEN_LIBDIRS), -I $(FC)$(shell $(MXI_ROOT)/make/$(if $(MXI_REALARCH),$(MXI_REALARCH),$(MXI_ARCH))/relpath "$(shell pwd)" "$(XINC)" "$(MXI_ROOT)" $(MXS_BUILDDIR).LibPath )$(FC)); cat $(MXS_BUILDDIR).LibPath; rm $(MXS_BUILDDIR).LibPath)

.PHONY : $(INL_VCPROJ_FORCE_BUILD) vcproj_force_build
vcproj_force_build $(INL_VCPROJ_FORCE_BUILD) :
	true

.PHONY : vcproj_force_build_phony
vcproj_force_build_phony :
	true

MXS_GENERAL_CODE_PATH=$(if $(filter-out TRUE, $(MXS_MSVS_TEST_PROJECT)),../../../code ../../code .,../../../test ../../test)
export MXS_MSVS_TEST_PROJECT

ifeq ($(wildcard $(MXS_TOOLS)*),)

%.vcproj : $(INL_VCPROJ_FORCE_BUILD)
	rm -f incl_devel_$(*) incl_release_$(*)
	rm -f lib_devel_$(*) lib_release_$(*)
	rm -f libfiles_devel_$(*) libfiles_release_$(*)
	#
	INL_RELEASE_BUILD=; export INL_RELEASE_BUILD; \
	MAKELEVEL=; export MAKELEVEL; \
	$(if $(MXI_REALARCH),,MXI_REALARCH=$(MXI_ARCH); export MXI_REALARCH; \
	MXI_ARCH=win32; export MXI_ARCH; ) \
	$(MAKE) vcproj_incpath_file/incl_devel_$(*)
	#
	INL_RELEASE_BUILD=1; export INL_RELEASE_BUILD; \
	MAKELEVEL=; export MAKELEVEL; \
	$(if $(MXI_REALARCH),,MXI_REALARCH=$(MXI_ARCH); export MXI_REALARCH; \
	MXI_ARCH=win32; export MXI_ARCH; ) \
	$(MAKE) vcproj_incpath_file/incl_release_$(*)
	#
	INL_RELEASE_BUILD=; export INL_RELEASE_BUILD; \
	MAKELEVEL=; export MAKELEVEL; \
	$(if $(MXI_REALARCH),,MXI_REALARCH=$(MXI_ARCH); export MXI_REALARCH; \
	MXI_ARCH=win32; export MXI_ARCH; ) \
	$(MAKE) vcproj_libpath_file/lib_devel_$(*)
	#
	INL_RELEASE_BUILD=1; export INL_RELEASE_BUILD; \
	MAKELEVEL=; export MAKELEVEL; \
	$(if $(MXI_REALARCH),,MXI_REALARCH=$(MXI_ARCH); export MXI_REALARCH; \
	MXI_ARCH=win32; export MXI_ARCH; ) \
	$(MAKE) vcproj_libpath_file/lib_release_$(*)
	#
	INL_RELEASE_BUILD=; export INL_RELEASE_BUILD; \
	MAKELEVEL=; export MAKELEVEL; \
	$(if $(MXI_REALARCH),,MXI_REALARCH=$(MXI_ARCH); export MXI_REALARCH; \
	MXI_ARCH=win32; export MXI_ARCH; ) \
	$(MAKE) vcproj_libfiles_file/libfiles_devel_$(*)
	#
	INL_RELEASE_BUILD=1; export INL_RELEASE_BUILD; \
	MAKELEVEL=; export MAKELEVEL; \
	$(if $(MXI_REALARCH),,MXI_REALARCH=$(MXI_ARCH); export MXI_REALARCH; \
	MXI_ARCH=win32; export MXI_ARCH; ) \
	$(MAKE) vcproj_libfiles_file/libfiles_release_$(*)
	#
	m4 \
	-DVC_BUILD_TYPE="$(if $(MXS_MSVS_BUILD_TYPE),$(MXS_MSVS_BUILD_TYPE),LIBRARY)" \
	-DVC_PROJ_NAME="$(*)" \
	-DVC_PATH="$(MXS_GENERAL_CODE_PATH)" \
	-DVC_WIN_PATH="UNUSED_PARAMETER" \
	-DVC_GUID="$(shell if [ \! -f $(*).uuid ]; then uuidgen > $(*).uuid; fi; cat $(*).uuid; )" \
	-DVC_DEVEL_BUILDDIR="$(ING_DEVEL_BUILDDIR)" \
	-DVC_RELEASE_BUILDDIR="$(ING_RELEASE_BUILDDIR)" \
	-DVC_DEVEL_INCLPATH="incl_devel_$(*)" \
	-DVC_RELEASE_INCLPATH="incl_release_$(*)" \
	-DVC_DEVEL_LIBPATH="lib_devel_$(*)" \
	-DVC_RELEASE_LIBPATH="lib_release_$(*)" \
	-DVC_DEVEL_LIBRARIES="libfiles_devel_$(*)" \
	-DVC_RELEASE_LIBRARIES="libfiles_release_$(*)" \
	-DVC_USE_ATL="$(IN_MSVS_USE_ATL)" \
	-DVC_USE_MFC="$(IN_MSVS_USE_MFC)" \
	-DVC_USEPRECOMPILEDHEADER="$(if $(IN_USEPRECOMPILEDHEADER),$(IN_USEPRECOMPILEDHEADER),0)" \
	$(MXI_ROOT)/make/win32/vcproj_templ.m4 > $(@).new
	mv -f $(@).new $(@)
	rm -f incl_devel_$(*) incl_release_$(*)
	rm -f lib_devel_$(*) lib_release_$(*)
	rm -f libfiles_devel_$(*) libfiles_release_$(*)

else

ifeq ($(MXI_RELATIVE_ROOT),)
MXI_RELATIVE_ROOT:=$(shell if [ -f ./make/MakeXS.m4 ]; then echo "."; else d=".."; while [ "$(BC)cd $$d; /bin/pwd$(BC)" \!= "/" -a \! -f "$$d/make/MakeXS.m4" ]; do d="$$d/.."; done ; echo $$d; fi)
ifeq ($(MXI_RELATIVE_ROOT),/)
$(error MXI_RELATIVE_ROOT is not defined or cannot be found)
endif
endif

ifeq ($(MXS_MSVS_BUILD_TYPE),CONSOLE)
MXS_MSVS_VCPROJ_TEMPLATE:=$(MXI_ROOT)/make/win32/vcproj_exe.template
else
MXS_MSVS_VCPROJ_TEMPLATE:=$(MXI_ROOT)/make/win32/vcproj_lib.template
endif
ifeq ($(MXI_REALARCH),win32)
MXS_MSVS_VCPROJ_TEMPLATE:=$(shell cygpath -m "$(MXS_MSVS_VCPROJ_TEMPLATE)")
endif

%.vcproj : $(INL_VCPROJ_FORCE_BUILD)
	@$(MXS_TOOLS) create_vcproj \
	    -R "$(MXI_RELATIVE_ROOT)" \
	    -T "$(MXS_MSVS_VCPROJ_TEMPLATE)" \
	    -D "VC_PROJ_NAME=$(*)" \
	    -D "VC_GUID=$(shell if [ \! -f $(*).uuid ]; then uuidgen > $(*).uuid; fi; cat $(*).uuid; )" \
	    -D "VC_USE_ATL=$(IN_MSVS_USE_ATL)" \
	    -D "VC_USE_MFC=$(IN_MSVS_USE_MFC)" \
	    -D "VC_USEPRECOMPILEDHEADER=$(if $(IN_USEPRECOMPILEDHEADER),$(IN_USEPRECOMPILEDHEADER),0)" \
	    $(MXS_MXS_TOOL_OVERRIDE) \
	    $(MXS_GENERAL_CODE_PATH:%=-S "%") \
	    -o "$(@)"

endif

MXS_LOAD_FOREIGN_MAKEFILE=$(wildcard $(INL_LOAD_FOREIGN_MAKEFILE))

ifneq ($(MXS_LOAD_FOREIGN_MAKEFILE),)
include $(MXS_LOAD_FOREIGN_MAKEFILE)
endif

.PHONY: build_type/%.vcproj_type
build_type/%.vcproj_type :
	$(MAKE) build_defs_by_type/$(*)

build_defs_by_type/% :
	if [ \! -f Makefile.win32.defs ]; then \
		echo "IN_MSVS_BUILD_TYPE=$(if $(filter-out CONSOLE, $(MXS_MSVS_BUILD_TYPE)),LIBRARY,CONSOLE)" > Makefile.win32.defs ; \
		echo "IN_MSVS_PROJECT_NAME=$(*)" >> Makefile.win32.defs ; \
		echo "IN_MSVS_TEST_PROJECT=$(MXS_MSVS_TEST_PROJECT)" >> Makefile.win32.defs ; \
	fi

devenv :
	cd $(MXI_ROOT)/win32/vssln; devenv vssln.sln &

ifeq ($(MXI_ARCH),win32)
APPDATA_PATH = $(shell exec echo -n "$(BC)cygpath -u "$$APPDATA"$(BC)" )
endif

TSUBVERSION_CONFIG_DIR=$(APPDATA_PATH)/Subversion
TSUBVERSION_CONFIG=$(TSUBVERSION_CONFIG_DIR)/config

SUBVERSION_CONFIG_DIR=$(HOME)/.subversion
SUBVERSION_CONFIG=$(SUBVERSION_CONFIG_DIR)/config

svnconfig : tsvnconfig 
	if cmp "$(MXI_ROOT)/make/subversion_conf/config" "$(SUBVERSION_CONFIG)"; then \
		echo "Installed config file is the same" ; \
	else \
		mv "$(SUBVERSION_CONFIG)" "$(SUBVERSION_CONFIG).$(BC)date +%s$(BC)" ; \
		cp "$(MXI_ROOT)/make/subversion_conf/config" "$(SUBVERSION_CONFIG)" ; \
	fi

IN_CERT_TGZ_FILENAME=netcabletv_cert.tgz.aes-256-cbc
IN_CERT_LOCAL_DIR=/cygdrive/c/

install_cert :
	stty -echo && \
	echo -n "Passphrase:" && read PASS && \
	stty echo && \
	openssl enc -aes-256-cbc -pass pass:$$PASS -d -in $(MXI_ROOT)/make/certs/$(IN_CERT_TGZ_FILENAME) | \
	( cd $(IN_CERT_LOCAL_DIR)&& tar xzvf - )


ifeq ($(MXI_ARCH),win32)
tsvnconfig :
	if cmp "$(MXI_ROOT)/make/subversion_conf/config" "$(TSUBVERSION_CONFIG)"; then \
		echo "Installed config file is the same" ; \
	else \
		mv "$(TSUBVERSION_CONFIG)" "$(TSUBVERSION_CONFIG).$(BC)date +%s$(BC)" ; \
		cp "$(MXI_ROOT)/make/subversion_conf/config" "$(TSUBVERSION_CONFIG)" ; \
	fi
else
tsvnconfig :
endif

')


