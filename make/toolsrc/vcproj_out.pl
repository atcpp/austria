#!/usr/bin/perl -w

use strict;
use File::Basename;

my $line;

my $file = $ARGV[0];

defined $file || die "Expected file name as first arg";

sub process
{
    ( my $file ) = @_;
    
    open( INFILE, "<$file" ) || die "Can't open $file";

    my $OUTPUTFILE;

    while ( $line = <INFILE> )
    {
        if ( $line =~ m/.*OutputFile="\$\(OutDir\)\/(.*)".*/ )
        {
            my $x = $1;
            if ( ! defined $OUTPUTFILE )
            {
                $OUTPUTFILE = $x;
            }
            elsif ( ! ($OUTPUTFILE eq $x) )
            {
                print STDERR "$file has different output file names\n";
            }
        }
    }

    if ( !defined $OUTPUTFILE )
    {
        print STDERR "$file has no output file names\n";
    }
    else
    {
        my $dirname = dirname($file);

        my $makexs_file = $dirname . "/Makefile.win32.defs";

        print STDOUT "$makexs_file\n";
        
        if ( -f $makexs_file )
        {
            open( IN_MXS, "<$makexs_file" ) || die "Can't open $makexs_file for reading";

            my $mxs_outfile;

            while ( $line = <IN_MXS> )
            {
                if ( $line =~ m/.*IN_OUTPUTFILE=(.*)\n?/ )
                {
                    $mxs_outfile = $1;

                    if ( $mxs_outfile eq $OUTPUTFILE )
                    {
                        print STDERR "$file IN_OUTPUTFILE in ($makexs_file) correct\n";
                        return 1;
                    }
                    else
                    {
                        print STDERR "$file IN_OUTPUTFILE in Makefile.win32.defs ($mxs_outfile) differs to OutputFile ($OUTPUTFILE) in .vcproj file";
                        return 2;
                    }
                }
            }

            close(IN_MXS);

            if ( ! defined $mxs_outfile )
            {
                open( OUT_MXS, ">>$makexs_file" ) || die "Can't open $makexs_file for append";

                print OUT_MXS "IN_OUTPUTFILE=" . $OUTPUTFILE . "\n";

                close( OUT_MXS );
                print STDERR "$file Added IN_OUTPUTFILE to ($makexs_file)\n";
            }
        }
    }
    
}

process( $file );


