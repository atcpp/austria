
#include <iostream>
#include <fstream>
#include <string>

/**
 * clean_path removes all ".." and "." elements in a path.
 *
 * @param path is the path being cleaned.
 */

void clean_path( std::string & path )
{

	if ( path.length() == 0 ) {
		return;
	}

	std::string::iterator ostr = path.begin() - 1;
	std::string::iterator istr = path.begin();

	std::string::iterator iend = path.end();

	bool is_abs = ( * istr ) == '/';
	
	// copy the string over itself - removing junk
	while ( istr != iend ) {
		
		// if we have a /./ or a /../ string then do some fixing
redo:
		if ( * istr == '/' ) {
			if ( ( iend != ( istr + 1 ) ) )
			{
				if ( ( * ( istr + 1 ) == '.' ) )
				{
					if ( ( iend == ( istr + 2 ) ) )
					{
						// we have a /.<end>
						istr += 2;
						break;
					}
					else
					{
						if ( * ( istr + 2 ) == '/' )
						{
							istr += 2;
							goto redo;
						}
						else if ( * ( istr + 2 ) == '.' )
						{
							if ( ( iend == ( istr + 3 ) ) || ( * ( istr + 3 ) == '/' ) )
							{
								// we have a /../ or a /..<end>
								istr += 3;
								while ( ostr >= path.begin() )
								{ 
									if ( * ostr == '/' )
									{
										-- ostr;
										if ( istr == iend )
										{
											break;
										}
										goto redo;
									}
									-- ostr;
								}
								if ( istr == iend )
								{
									break;
								}
							}
						}
					}
				}
				else if ( ( * ( istr + 1 ) == '/' ) )
				{
					// we have a '//'
					++ istr;
					goto redo;
				}
			}
		}
	
		* ( ++ ostr ) = * ( istr ++ );
	}

	if ( * ostr == '/' )
	{
		-- ostr;
	}
	
	path.erase( ostr + 1, path.end() );

	if ( is_abs )
	{
		if ( path.length() == 0 )
		{
			path = '/';
		}
		else if ( path[ 0 ] != '/' )
		{
			path = std::string( "/" ) + path;
		}
	}
		
	return;
	
}


std::string relative_path_clean(
	const std::string		& path_from,
	const std::string		& path_to,
	const std::string		& path_stem
)
{

	if ( path_to.length() == 0 ) {
		return ".";
	}

	if ( path_to[0] != '/' )
	{
		return path_to;
	}
	
	if ( path_from.length() == 0 ) {
		return path_to;
	}
	
	std::string::const_iterator fitr = path_from.begin();
	std::string::const_iterator titr = path_to.begin();
	std::string::const_iterator sitr = path_stem.begin();

	std::string::const_iterator last_slash_fitr = path_from.begin();
	std::string::const_iterator last_slash_titr = path_to.begin();

	std::string::const_iterator efitr = path_from.end();
	std::string::const_iterator etitr = path_to.end();
	std::string::const_iterator esitr = path_stem.end();

	// process the stem - all three strings need to be identical
	// up to the end of the stem.
	
	if ( sitr != esitr )
	{
		while ( ( titr != etitr ) && ( fitr != efitr ) && ( sitr != esitr ) )
		{
			if ( * titr != * fitr )
			{
				return path_to;
			}
			
			if ( * titr != * sitr )
			{
				return path_to;
			}
	
			++ titr;
			++ fitr;
			++ sitr;
		}

		-- sitr;

		if ( * sitr != '/' )
		{
			if ( ( titr != etitr ) && ( * titr != '/' ) )
			{
				return path_to;
			}

			++ titr;
				
			if ( ( fitr != efitr ) && ( * fitr != '/' ) )
			{
				return path_to;
			}

			++ fitr;
		}

		last_slash_fitr = fitr;
		last_slash_titr = titr;

	}
	
	// copy the string over itself - removing junk
	while ( ( titr != etitr ) && ( fitr != efitr ) )
	{

		if ( * titr != * fitr )
		{
			break;
		}

		++ fitr;
		if ( * ( titr ++ ) == '/' )
		{
			last_slash_fitr = fitr;
			last_slash_titr = titr;
		}

	}

	if ( titr == etitr )
	{
		if ( fitr == efitr )
		{
			return ".";
		}
		else if ( '/' == * fitr )
		{
			++ fitr;
			last_slash_fitr = fitr;
			last_slash_titr = titr;
		}
	}
	else
	{
		if ( '/' == * titr )
		{
			++ titr;
			last_slash_fitr = fitr;
			last_slash_titr = titr;
		}
	}

	std::string rel_path;

	titr = last_slash_titr;
	fitr = last_slash_fitr;

	if ( fitr != efitr )
	{
		rel_path += "..";

		++ fitr;
		
		while ( fitr != efitr )
		{
			if ( * ( fitr ++ ) == '/' )
			{
				if ( fitr != efitr )
				{
					rel_path += "/..";
				}
			}
		}
	}

	if ( last_slash_titr != etitr )
	{
		if ( rel_path.length() != 0 )
		{
			rel_path += '/';
		}
		rel_path.append( last_slash_titr, etitr );
	}

	if ( rel_path.length() == 0 )
	{
		return ".";
	}
	
	return rel_path;
	
}

std::string relative_path(
	std::string			& path_from,
	std::string			& path_to,
	std::string			& path_stem
)
{
	clean_path( path_from );
	clean_path( path_to );
	clean_path( path_stem );

	return relative_path_clean( path_from, path_to, path_stem );
}


char usage[] =
"Usage relpath <from_path> <to_path> <stem_path> [<append_file>]\n"
"relpath creates a relative path from an absolute path\n"
" <from_path> is the directory where the relative path is targetted for\n"
" <to_path> is the target path\n"
" <stem_path> is the minimum path to be considered for creating a relative path\n"
" <append_file> is an optional parameter, this file will contain a ';' separated list\n"
"\n"
"Example relpath /var/log/http/errors/ /var/log/http/nonerror/ /var/log\n"
"	results in '../nonerror'\n";

int main( int argc, char ** argv )
{

	if ( argc != 4 && argc != 5 )
	{
		std::cerr << usage;
		exit( 1 );
	}

	std::ostream		* p_file = & std::cout;

	std::ofstream		ofile;
	bool out_file = false;

	if ( argc == 5 )
	{
		out_file = true;
		ofile.open( argv[4], std::ofstream::out | std::ofstream::app);

		if ( !ofile )
		{
			std::cerr << "can't open file '" << argv[4] << "'\n";
			exit( 1 );
		}
		ofile.seekp( 0, std::ios_base::end );
		if ( ofile.tellp() != std::ostream::pos_type( 0 ) )
		{
			ofile << ';';
		}
		
		p_file = & ofile;
	}

	std::string	from_str( argv[1] );
	std::string	to_str( argv[2] );
	std::string	stem_str( argv[3] );

	( * p_file ) << relative_path( from_str, to_str, stem_str );

	if ( ! out_file )
	{
		( * p_file ) << "\n";
	}

	return 0;
}
