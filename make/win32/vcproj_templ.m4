divert(-1)
#
# parameters
#
# VC_BUILD_TYPE - LIBRARY, CONSOLE, WINDOWS
#
# VC_PROJ_NAME - the name of the project
# VC_PATH - the path to search for sources
# VC_WIN_PATH - the path to search for win32 specific sources
# VC_GUID - the project guid to use
# VC_DEVEL_BUILDDIR - Debug directory
# VC_RELEASE_BUILDDIR - Release directory
# VC_DEVEL_INCLPATH - The file that contains the include path for debug builds.
# VC_RELEASE_INCLPATH - The file that contains the include path for release builds.
# VC_DEVEL_LIBPATH - The file that contains the library path for debug builds.
# VC_RELEASE_LIBPATH - The file that contains the library path for release builds.
# VC_DEVEL_LIBRARIES - The file that contains the library path for debug builds.
# VC_RELEASE_LIBRARIES - The file that contains the library path for release builds.
# VC_USEPRECOMPILEDHEADER - 0/1 for value of precompiled header option
# VC_USE_ATL - Empty or digit value
# VC_USE_MFC - Empty or digit value
#

changequote(+@+,-@-)

define(+@+VX_HDR_FILES-@-,translit((shift(esyscmd(+@+find -@-VC_PATH+@+ \( -name \*.hpp -o -name \*.h \) -printf ",[%%_%p_%%]"-@-))),/,+@+\-@-))
define(+@+VX_SRC_FILES-@-,translit((shift(esyscmd(+@+find -@-VC_PATH+@+ \( -name \*.cpp -o -name \*.c \) -printf ",[%%_%p_%%]"-@-))),/,+@+\-@-))

define(+@+VX_HDR_WIN_FILES-@-,translit((shift(esyscmd(+@+if [ -d -@-VC_WIN_PATH+@+ ]; then find -@-VC_WIN_PATH+@+ \( -name \*.hpp -o -name \*.h \) -printf ",[%%_%p_%%]"; fi-@-))),/,+@+\-@-))
define(+@+VX_SRC_WIN_FILES-@-,translit((shift(esyscmd(+@+if [ -d -@-VC_WIN_PATH+@+ ]; then find -@-VC_WIN_PATH+@+ \( -name \*.cpp -o -name \*.c \) -printf ",[%%_%p_%%]"; fi-@-))),/,+@+\-@-))

changequote([%_,_%])

dnl M4forech(x, (item_1, item_2, ..., item_n), stmt)
define([%_M4forech_%], [%_pushdef([%_$1_%], [%__%])_M4forech([%_$1_%], [%_$2_%], [%_$3_%])popdef([%_$1_%])_%])dnl
define([%__Xarg1_%], [%_$1_%])
define([%__M4forech_%],dnl
[%_ifelse([%_$2_%], [%_()_%], ,
[%_define([%_$1_%], _Xarg1$2)$3[%__%]_M4forech([%_$1_%], (shift$2), [%_$3_%])_%])_%])dnl
dnl

define([%_VX_FILE_%],[%_dnl
			<File
				RelativePath="$1"/>
_%])

define([%_VX_USE_MFC_%],[%_ifelse(VC_USE_MFC,,,[%_			UseOfMFC="_%]VC_USE_MFC[%_"
_%])_%])
define([%_VX_USE_ATL_%],[%_ifelse(VC_USE_ATL,,,[%_			UseOfATL="_%]VC_USE_ATL[%_"
_%])_%])

define([%_VX_CONFTYPE_CONSOLE_%],[%_1_%])
define([%_VX_CONFTYPE_LIBRARY_%],[%_4_%])
define([%_VX_CONFTYPE_WINDOWS_%],[%_1_%])

define([%_VX_PREPROC_CONSOLE_%],[%__CONSOLE_%])
define([%_VX_PREPROC_LIBRARY_%],[%__LIB_%])
define([%_VX_PREPROC_WINDOWS_%],[%__WINDOWS_%])

define([%_VX_LINKTOOL_LIBRARY_%],[%__%])
define([%_VX_LINKTOOL_CONSOLE_%],[%_[%_
			<Tool
				Name="VCLinkerTool"
				AdditionalDependencies="_%]include(VX_LIBRARIES)[%_"
				AdditionalLibraryDirectories="_%]include(VX_LIBPATH)[%_"
				OutputFile="$(OutDir)/_%]VC_PROJ_NAME[%_.exe"
				LinkIncremental="2"
				GenerateDebugInformation="TRUE"
				ProgramDatabaseFile="$(OutDir)/_%]VC_PROJ_NAME[%_.pdb"
				SubSystem="1"
				TargetMachine="1"/>
_%]_%])
define([%_VX_LINKTOOL_WINDOWS_%],[%_[%_
			<Tool
				Name="VCLinkerTool"
				AdditionalDependencies="_%]include(VX_LIBRARIES)[%_"
				AdditionalLibraryDirectories="_%]include(VX_LIBPATH)[%_"
				OutputFile="$(OutDir)/_%]VC_PROJ_NAME[%_.exe"
				LinkIncremental="2"
				GenerateDebugInformation="TRUE"
				ProgramDatabaseFile="$(OutDir)/_%]VC_PROJ_NAME[%_.pdb"
				SubSystem="2"
				TargetMachine="1"/>
_%]_%])

define([%_VX_LIBRTOOL_LIBRARY_%],[%_dnl
			<Tool
				Name="VCLibrarianTool"
				OutputFile="$(OutDir)/_%]VC_PROJ_NAME[%_.lib"/>
_%])
define([%_VX_LIBRTOOL_CONSOLE_%],[%__%])
define([%_VX_LIBRTOOL_WINDOWS_%],[%__%])

define([%_VX_WEBDEPTOOL_LIBRARY_%],[%__%])
define([%_VX_WEBDEPTOOL_CONSOLE_%],[%_dnl
			<Tool
				Name="VCWebDeploymentTool"/>
_%])
define([%_VX_WEBDEPTOOL_WINDOWS_%],[%_dnl
			<Tool
				Name="VCWebDeploymentTool"/>
_%])

define([%_VX_SHOW_%],[%_VX_$1__%]VC_BUILD_TYPE)

_%])

#
divert(0)dnl
[%_<?xml version="1.0" encoding="Windows-1252"?>
<VisualStudioProject
	ProjectType="Visual C++"
	Version="7.10"
	Name="_%]VC_PROJ_NAME[%_"
	ProjectGUID="{_%]VC_GUID[%_}"
	Keyword="Win32Proj">
	<Platforms>
		<Platform
			Name="Win32"/>
	</Platforms>
	<Configurations>
		<Configuration
			Name="Debug|Win32"
			OutputDirectory="_%]VC_DEVEL_BUILDDIR[%_"
			IntermediateDirectory="_%]VC_DEVEL_BUILDDIR[%_"
			ConfigurationType="_%]VX_SHOW(CONFTYPE)[%_"
_%]dnl
VX_USE_MFC()dnl
VX_USE_ATL()dnl
[%_			CharacterSet="2">
			<Tool
				Name="VCCLCompilerTool"
				AdditionalOptions="/wd4267 /wd4244"
				Optimization="0"
				AdditionalIncludeDirectories="_%]include(VC_DEVEL_INCLPATH)[%_"
				PreprocessorDefinitions="WIN32;_DEBUG;_%]VX_SHOW(PREPROC)[%_;WINVER=0x0400"
				MinimalRebuild="TRUE"
				BasicRuntimeChecks="3"
				RuntimeLibrary="3"
				UsePrecompiledHeader="_%]VC_USEPRECOMPILEDHEADER[%_"
				RuntimeTypeInfo="TRUE"
				WarningLevel="3"
				Detect64BitPortabilityProblems="TRUE"
				DebugInformationFormat="4"/>
			<Tool
				Name="VCCustomBuildTool"/>
_%]define([%_VX_LIBRARIES_%],defn([%_VC_DEVEL_LIBRARIES_%]))dnl
define([%_VX_LIBPATH_%],defn([%_VC_DEVEL_LIBPATH_%]))dnl
VX_SHOW(LIBRTOOL)[%__%]VX_SHOW(LINKTOOL)[%__%]dnl
[%_
			<Tool
				Name="VCMIDLTool"/>
			<Tool
				Name="VCPostBuildEventTool"/>
			<Tool
				Name="VCPreBuildEventTool"/>
			<Tool
				Name="VCPreLinkEventTool"/>
			<Tool
				Name="VCResourceCompilerTool"/>
			<Tool
				Name="VCWebServiceProxyGeneratorTool"/>
			<Tool
				Name="VCXMLDataGeneratorTool"/>
			<Tool
				Name="VCManagedWrapperGeneratorTool"/>
_%]VX_SHOW(WEBDEPTOOL)[%__%]dnl
[%_
			<Tool
				Name="VCAuxiliaryManagedWrapperGeneratorTool"/>
		</Configuration>
		<Configuration
			Name="Release|Win32"
			OutputDirectory="_%]VC_RELEASE_BUILDDIR[%_"
			IntermediateDirectory="_%]VC_RELEASE_BUILDDIR[%_"
			ConfigurationType="_%]VX_SHOW(CONFTYPE)[%_"
_%]dnl
VX_USE_MFC()dnl
VX_USE_ATL()dnl
[%_			CharacterSet="2">
			<Tool
				Name="VCCLCompilerTool"
				AdditionalOptions="/wd4267 /wd4244"
				AdditionalIncludeDirectories="_%]include(VC_RELEASE_INCLPATH)[%_"
				PreprocessorDefinitions="WIN32;NDEBUG;_%]VX_SHOW(PREPROC)[%_;WINVER=0x0400"
				RuntimeLibrary="2"
				UsePrecompiledHeader="_%]VC_USEPRECOMPILEDHEADER[%_"
				RuntimeTypeInfo="TRUE"
				WarningLevel="3"
				Detect64BitPortabilityProblems="TRUE"
				DebugInformationFormat="3"/>
			<Tool
				Name="VCCustomBuildTool"/>
_%]define([%_VX_LIBRARIES_%],defn([%_VC_RELEASE_LIBRARIES_%]))dnl
define([%_VX_LIBPATH_%],defn([%_VC_RELEASE_LIBPATH_%]))dnl
VX_SHOW(LIBRTOOL)[%__%]VX_SHOW(LINKTOOL)[%__%]dnl
[%_
			<Tool
				Name="VCMIDLTool"/>
			<Tool
				Name="VCPostBuildEventTool"/>
			<Tool
				Name="VCPreBuildEventTool"/>
			<Tool
				Name="VCPreLinkEventTool"/>
			<Tool
				Name="VCResourceCompilerTool"/>
			<Tool
				Name="VCWebServiceProxyGeneratorTool"/>
			<Tool
				Name="VCXMLDataGeneratorTool"/>
			<Tool
				Name="VCManagedWrapperGeneratorTool"/>
			<Tool
				Name="VCAuxiliaryManagedWrapperGeneratorTool"/>
		</Configuration>
	</Configurations>
	<References>
	</References>
	<Files>
		<Filter
			Name="Source Files"
			Filter="cpp;c;cxx;def;odl;idl;hpj;bat;asm;asmx"
			UniqueIdentifier="{4FC737F1-C7A5-4376-A066-2A32D752A2FF}">
_%]M4forech(VX_F,defn([%_VX_SRC_FILES_%]),[%_VX_FILE(VX_F)_%])dnl
M4forech(VX_F,defn([%_VX_SRC_WIN_FILES_%]),[%_VX_FILE(VX_F)_%])dnl
[%_		</Filter>
		<Filter
			Name="Header Files"
			Filter="h;hpp;hxx;hm;inl;inc;xsd"
			UniqueIdentifier="{93995380-89BD-4b04-88EB-625FBE52EBFB}">
_%]M4forech(VX_F,defn([%_VX_HDR_FILES_%]),[%_VX_FILE(VX_F)_%])dnl
M4forech(VX_F,defn([%_VX_HDR_WIN_FILES_%]),[%_VX_FILE(VX_F)_%])dnl
[%_		</Filter>
		<Filter
			Name="Resource Files"
			Filter="rc;ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe;resx"
			UniqueIdentifier="{67DA6AB6-F800-4c08-8B7A-83BB121AAD01}">
		</Filter>
	</Files>
	<Globals>
	</Globals>
</VisualStudioProject>
_%]dnl
